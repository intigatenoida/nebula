package com.nibula.costbreakdown.modal
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse
import kotlinx.android.parcel.Parcelize

@Keep
data class CostBreakdownResponse(

    @SerializedName("Response")
    var response: Response? = Response()
):BaseResponse(){
    @Parcelize
    @Keep
    data class Response(
        @SerializedName("CurrencyType")
        var currencyType: String? = "",
        @SerializedName("CurrencyTypeSymbol")
        var currencyTypeSymbol: String? = "",
        @SerializedName("EthereumTransactionCost")
        var ethereumTransactionCost: Double? = 0.0,
        @SerializedName("EthereumTransactionCostLabel")
        var ethereumTransactionCostLabel: String? = "",
        @SerializedName("PayPalFixedFeesLabel")
        var payPalFixedFeesLabel: String? = "",
        @SerializedName("PaypalTransactionFees")
        var paypalTransactionFees: Double? = 0.0,
        @SerializedName("PriceInitialMusicOffering")
        var priceInitialMusicOffering: Double? = 0.0,
        @SerializedName("PriceInitialMusicOfferingLabel")
        var priceInitialMusicOfferingLabel: String? = "",
        @SerializedName("PriceMusicExclusiveOfTax")
        var priceMusicExclusiveOfTax: Double? = 0.0,
        @SerializedName("PriceMusicExclusiveOfTaxLabel")
        var priceMusicExclusiveOfTaxLabel: String? = "",
        @SerializedName("PriceMusicInclusiveOfTax")
        var priceMusicInclusiveOfTax: Double? = 0.0,
        @SerializedName("PriceMusicInclusiveOfTaxLabel")
        var priceMusicInclusiveOfTaxLabel: String? = "",
        @SerializedName("PriceMusicRightCoOwnership")
        var priceMusicRightCoOwnership: Double? = 0.0,
        @SerializedName("PriceMusicRightCoOwnershipLabel")
        var priceMusicRightCoOwnershipLabel: String? = "",
        @SerializedName("PricePerRoyaltyShareRecording")
        var pricePerRoyaltyShareRecording: Double? = 0.0,
        @SerializedName("PricePerRoyaltyShareRecordingLabel")
        var pricePerRoyaltyShareRecordingLabel: String? = "",
        @SerializedName("PricePerRoyaltyShareWork")
        var pricePerRoyaltyShareWork: Double? = 0.0,
        @SerializedName("PricePerRoyaltyShareWorkLabel")
        var pricePerRoyaltyShareWorkLabel: String? = "",
        @SerializedName("StripeFixedFeesLabel")
        var stripeFixedFeesLabel: String? = "",
        @SerializedName("StripeTransactionFees")
        var stripeTransactionFees: Double? = 0.0,
        @SerializedName("TaxCharged")
        var taxCharged: Double? = 0.0,
        @SerializedName("TaxChargedLabel")
        var taxChargedLabel: String? = "",
        var recordType : Int = 0
    ):Parcelable


}