package com.nibula.costbreakdown.fragment

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.databinding.CostBreakDownActivityBinding
import com.nibula.databinding.CostbreakdownFragmentBinding
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils

class CostBreakDownFragment : BaseFragment() {

    companion object {
        fun newInstance() = CostBreakDownFragment()
    }

    lateinit var rootView: View
    lateinit var costBreakdownResponse: CostBreakdownResponse.Response
    lateinit var binding:CostbreakdownFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
            binding= CostbreakdownFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            //rootView = inflater.inflate(R.layout.costbreakdown_fragment, container, false)
        }

        //--> Album Response
        costBreakdownResponse =
            requireArguments().getParcelable<CostBreakdownResponse.Response>(AppConstant.COST_BREAKDOWN)!! as CostBreakdownResponse.Response

        initView()
        return rootView
    }

    private fun initView() {
        with(rootView) {

            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.priceMusicExclusiveOfTax?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPriceUMusic.text = it }

            binding.tvMusicPriceNote.text =
                costBreakdownResponse.priceMusicExclusiveOfTaxLabel.toString()

            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.taxCharged?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPriceWorkVat.text = it }

            binding.tvPriceVatNote.text = costBreakdownResponse.taxChargedLabel.toString()

            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.priceMusicInclusiveOfTax?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPriceMusicInclusive.text = it }

            binding.tvPricePerRoyaltyRecordingNote.text =
                costBreakdownResponse.priceMusicInclusiveOfTaxLabel.toString()

            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.pricePerRoyaltyShareRecording?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPricePerRoyaltyRecording.text = it }

            binding.tvPricePerRoyaltyRecordingNote.text =
                costBreakdownResponse.pricePerRoyaltyShareRecordingLabel.toString()

            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.pricePerRoyaltyShareWork?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPricePerRoyaltyWork.text = it }

            binding.tvPricePerRoyaltyWorkNote.text =
                costBreakdownResponse.pricePerRoyaltyShareWorkLabel.toString()

            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.priceInitialMusicOffering?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPriceInitialOffering.text = it }


            binding.tvPriceInitialOfferingNote.text =
                costBreakdownResponse.priceInitialMusicOfferingLabel.toString()


            if (costBreakdownResponse.recordType != AppConstant.released) {
                binding.tvPriceEtherumNote.visibility = View.VISIBLE
                binding.tvPriceEtherum.visibility = View.VISIBLE

                binding.viewBottom.visibility = View.GONE
                binding.tvPriceEtherumNote.text =
                    costBreakdownResponse.ethereumTransactionCostLabel.toString()
                (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.ethereumTransactionCost?.let {
                    CommonUtils.trimDecimalToThreePlaces(
                        it
                    )
                }).also { binding.tvPriceEtherum.text = it }
            } else {
                binding.releaseGroup.visibility = View.GONE
                binding.viewBottom.visibility = View.VISIBLE
                binding.tvPriceEtherumNote.visibility = View.VISIBLE
                binding.tvPriceEtherum.visibility = View.VISIBLE
                binding.tvPriceEtherumNote.text =  setBoldPrice(costBreakdownResponse.priceMusicInclusiveOfTaxLabel.toString())
            }

            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.ethereumTransactionCost?.let {
                CommonUtils.trimDecimalToThreePlaces(it)
            }).also { binding.tvPriceEtherum.text = it }
            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.paypalTransactionFees?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPricePaypal.text = it }

            binding.tvPricePaypalNote.text = costBreakdownResponse.payPalFixedFeesLabel.toString()
            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.stripeTransactionFees?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPriceStripe.text = it }


            binding.tvPriceStripeNote.text = costBreakdownResponse.stripeFixedFeesLabel.toString()
        }
    }

    private fun setBoldPrice(str: String): Spannable {
        val spannable = SpannableString(str)
        val typeface = ResourcesCompat.getFont(requireContext(), R.font.pt_sans_bold)
        if (typeface != null) {
            spannable.setSpan(
                StyleSpan(typeface.style),
                0,
                str.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
        }
        return spannable
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.ivBack.setOnClickListener {
            try {
                navigate.manualBack()
            } catch (e: Exception) {
            }
        }
    }

}