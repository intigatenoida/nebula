package com.nibula.response.myinvestmentresponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class Myinvestments(
    @SerializedName("Response")
    var response: Any?,
    @SerializedName("ResponseCollection")
    var responseCollection: MutableList<ResponseCollection>?,
    @SerializedName("TotalRecords")
    var totalRecords: Int?
):BaseResponse()