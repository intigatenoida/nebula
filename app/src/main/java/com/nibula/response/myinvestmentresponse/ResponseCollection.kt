package com.nibula.response.myinvestmentresponse

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.counter.TimerParams

@Keep
data class ResponseCollection(
    @SerializedName("AmountAvailableToInvest")
    var amountAvailableToInvest: Double = 0.0,
    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Boolean = false,
    @SerializedName("ArtistsId")
    var artistsId: Any? = Any(),
    @SerializedName("CreatedOn")
    var createdOn: Any? = Any(),
    @SerializedName("Description")
    var description: String = "",
    @SerializedName("GenreType")
    var genreType: String = "",
    @SerializedName("Id")
    var id: String = "",

    @SerializedName("RecordId")
    var recordId: String = "",

    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double = 0.0,
    @SerializedName("Labels")
    var labels: String = "",
    @SerializedName("OfferingEndDate")
    var offeringEndDate: String = "",
    @SerializedName("OfferingStartDate")
    var offeringStartDate: String = "",
    @SerializedName("PAdvisoryType")
    var pAdvisoryType: String = "",
    @SerializedName("PAdvisoryTypeId")
    var pAdvisoryTypeId: Int = 0,
    @SerializedName("PerformanceCopyrightNo")
    var performanceCopyrightNo: String = "",
    @SerializedName("PriceAfterRelease")
    var priceAfterRelease: Double = 0.0,
    @SerializedName("PricePerRoyaltyShare")
    var pricePerRoyaltyShare: Double = 0.0,
    @SerializedName("RecordImage")
    var recordImage: String = "",
    @SerializedName("RecordStatusType")
    var recordStatusType: Any? = Any(),
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int = 0,
    @SerializedName("RecordTitle")
    var recordTitle: String = "",
    @SerializedName("RecordType")
    var recordType: String = "",
    @SerializedName("RecordTypeId")
    var recordTypeId: Int = 0,
    @SerializedName("RecordingCopyrightNo")
    var recordingCopyrightNo: String = "",
    @SerializedName("ReleaseDate")
    var releaseDate: String = "",
    @SerializedName("SharesAvailable")
    var sharesAvailable: Double = 0.0,
    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Long = 0,
    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double?,
    @SerializedName("TotalPaidAmount")
    var totalPaidAmount: Double?,

    @SerializedName("TotalNoOfInvesters")
    var totalNoOfInvesters: Int?,
    @SerializedName("TotalRecords")
    var totalRecords: Int = 0,
    @SerializedName("ValuePerShareInDollars")
    var valuePerShareInDollars: Double = 0.0,
    @SerializedName("ArtistsName")
    var artistsName: String = "",

    @SerializedName("UploadingPerson")
    var uploadingPerson: String = "",
    @SerializedName("InvestmentStatusId")
    var investmentStatusId: Int?,
    @SerializedName("InvestmentStatus")
    var investmentStatus: String?,
    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Boolean?,
    @SerializedName("CurrencyType")
    var currencyType: String?,
    @SerializedName("CurrencySymbol")
    var currencySymbol: String?,
    @SerializedName("ShowToPublic")
    var showToPublic: Boolean = false,
    @SerializedName("InvestmentId")
    var investmentId: Int?,

    @SerializedName("PurchasedShare")
    var purchasedShare: Int?,

    @SerializedName("TotalOwnedTokens")
    var totalOwnedTokens: Int?,

    //MyCollectorsResponse
    @SerializedName("InvestMentId")
    var investMentId: String?,

    @SerializedName("Shares")
    var shares: String?,
    @SerializedName("Amount")
    var amount: String?,

    @SerializedName("InvestorId")
    var investorId: String?,
    @SerializedName("InvestorName")
    var investorName: String?,
    @SerializedName("InvestorUserName")
    var investorUserName: String?,
    @SerializedName("InvestorImage")
    var investorImage: String?,
    @SerializedName("TotalShares")
    var TotalShares: String?
) : TimerParams()