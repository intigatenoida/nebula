package com.nibula.response.myinvestmentresponse


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class MyInvestmentResponse(
    @SerializedName("MyInvestments")
    var myinvestments: Myinvestments?,
    @SerializedName("TotalInvestment")
    var totalInvestment: String?,
    @SerializedName("MyInvestmentsTotal")
    var myInvestmentsTotals: String?=null
)