package com.nibula.response.uploadrecord

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.request.upload.UploadProgressRequestBody

@Keep
data class RecordGetDataUpdate(
    @SerializedName("Id")
    var id: Int?,
    @SerializedName("RecordId")
    var recordId: String?,
    @SerializedName("SongFilePath")
    var songFilePath: String?,
    @SerializedName("RecordDurationInSeconds")
    val recordDurationInSeconds: Int?,
    var uploadStatus: String = UploadProgressRequestBody.STOP,
    var progress: Int = 0,
    var isSongRecordFileChnaged: Boolean = false,
    var isRecordImageFileChanged: Boolean = false
)