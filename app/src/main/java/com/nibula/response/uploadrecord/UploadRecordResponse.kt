package com.nibula.response.uploadrecord

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class UploadRecordResponse(
	
	@SerializedName("Id")
	val id:String?,
	@SerializedName("RecordsImage")
	val recordsImage:String?,
	@SerializedName("RecordsTitle")
	val recordsTitle:String?,
	@SerializedName("ArtistName")
	val artistName:String?
	
):BaseResponse()