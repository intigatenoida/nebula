package com.nibula.response.uploadrecord


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class GenereData(
    @SerializedName("Value")
    var value: Int?,
    @SerializedName("Text")
    var text: String?
)