package com.nibula.response.uploadrecord

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class UploadRecordFileResponse(

    @SerializedName("Id")
    var id: String?,
    @SerializedName("PId")
    var pId: Int?,
    @SerializedName("RecordFileId")
    var recordFileId: Int?,

    @SerializedName("RecordsImage")
    var songCover: String?,

    @SerializedName("RecordsTitle")
    var recordsTitle: String?

) : BaseResponse()