package com.nibula.response.uploadrecord

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class GetRecordIdResponse(
    @SerializedName("Id")
    var recordId: String?,
    @SerializedName("PId")
    var recordFileID: String?
) : BaseResponse()