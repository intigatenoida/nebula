package com.nibula.response.uploadrecord


import android.net.Uri
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.request.upload.UploadProgressRequestBody
import com.nibula.response.artists.ArtistData

@Keep
data class RecordGetData(
	@SerializedName("Id")
	var id: Int?,
	@SerializedName("RecordId")
	val recordId: String?,
	@SerializedName("SongTitle")
	var songTitle: String?,
	@SerializedName("SongFilePath")
	val songFilePath: String?,
	@SerializedName("RecordDurationInSeconds")
	val recordDurationInSeconds: Int?,
	@SerializedName("SortingOrder")
	val sortingOrder: Int?,
	@SerializedName("StatusTypeId")
	val statusTypeId: Int?,
	@SerializedName("Artists")
	var artists: MutableList<ArtistData?>?,

	//new params
	var currencyType : Int = 0,
	@SerializedName("ISWCCode")
	var iswcCodeWork : String = "",
	@SerializedName("ISCRCode")
	var iscrCodeRecording : String = "",
	@SerializedName("SocietyNameWork")
	var collectingSocietyWork : String = "",
	@SerializedName("CopyrightSocietyWork")
	var collectingSocietyWorkId : Int = 0,
	@SerializedName("SocietyNameRecording")
	var collectingSocietyRecording : String ="",
	@SerializedName("CopyrightSocietyRecording")
	var collectingSocietyRecordingId : Int =0,

	var isFirstSong : Boolean = false,


	var isFromOnSave:Boolean=false,

	var isRefreshRecord:Boolean=false,



	var vType: Int = 0,
	var recordFileUri: Uri?,
	var isUploadRequired: Boolean = false,
	var uploadStatus: String = UploadProgressRequestBody.STOP,
	var progress: Int = 0,
	var isExpand:Boolean = true,
	var retryCount:Int = 2,
	var stableId:Long = -1,
	var randomStuck:Int = -1
)