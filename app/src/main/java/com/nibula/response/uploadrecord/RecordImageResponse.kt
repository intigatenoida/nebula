package com.nibula.response.uploadrecord

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class RecordImageResponse(

	@SerializedName("RecordsImage")
	var recordImage:String?
	
):BaseResponse()