package com.nibula.response.HistoryResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("ActionTakenOn")
    var actionTakenOn: String,
    @SerializedName("BalanceTypeId")
    var balanceTypeId: Int,
    @SerializedName("CreditAmount")
    var creditAmount: Double,
    @SerializedName("DebitAmount")
    var debitAmount: Double,
    @SerializedName("Description")
    var description: String="",
    @SerializedName("Id")
    var id: Int,
    @SerializedName("NetAmount")
    var netAmount: Double,
    @SerializedName("TotalRecords")
    var totalRecords: Int,
    @SerializedName("UserId")
    var userId: String,
    @SerializedName("CurrencyTypeSymbol")
    var currencyTypeSymbol: String
)