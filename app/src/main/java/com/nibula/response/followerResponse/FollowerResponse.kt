package com.nibula.response.followerResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class FollowerResponse(
    @SerializedName("ResponseMessage")
    var responseMessage: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int = 0,
    @SerializedName("TotalRecords")
    var totalRecords: Int = 0,
    @SerializedName("Users")
    var users: List<User> = listOf()
)