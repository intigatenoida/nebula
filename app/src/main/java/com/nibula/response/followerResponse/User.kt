package com.nibula.response.followerResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class User(
    @SerializedName("IsArtist")
    var isArtist: Boolean = false,
    @SerializedName("Name")
    var name: String = "",
    @SerializedName("ROWINDEX")
    var rOWINDEX: Int = 0,
    @SerializedName("UserBio")
    var userBio: String = "",
    @SerializedName("UserId")
    var userId: String = "",
    @SerializedName("UserImage")
    var userImage: String = "",
    @SerializedName("UserName")
    var userName: String = ""
)