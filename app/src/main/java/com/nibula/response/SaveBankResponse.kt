package com.nibula.response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class SaveBankResponse(
    @SerializedName("custom")
    var custom: Any,
    @SerializedName("Id")
    var id: String,
    @SerializedName("PId")
    var pId: Int,
    @SerializedName("ResponseMessage")
    var responseMessage: String,
    @SerializedName("ResponseStatus")
    var responseStatus: Int
)