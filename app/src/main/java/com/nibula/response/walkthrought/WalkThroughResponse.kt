package com.nibula.response.walkthrought


import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class WalkThroughResponse(
    @SerializedName("ResponseCollection")
    var responseCollection: ArrayList<ResponseCollection>? = null
): BaseResponse(), Parcelable