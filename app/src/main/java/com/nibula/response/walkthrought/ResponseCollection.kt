package com.nibula.response.walkthrought


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class ResponseCollection(
    @SerializedName("Description")
    var description: String? = null,
    @SerializedName("Id")
    var id: Int? = null,
    @SerializedName("Title")
    var title: String? = null
):Parcelable