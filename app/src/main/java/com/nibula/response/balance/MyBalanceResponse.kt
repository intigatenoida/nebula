package com.nibula.response.balance


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class MyBalanceResponse(
    @SerializedName("Response")
    var response: Response?
) : BaseResponse()