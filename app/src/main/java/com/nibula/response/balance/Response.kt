package com.nibula.response.balance


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Response(
    @SerializedName("CashBackAmount")
    var cashBackAmount: Double?,
    @SerializedName("CurrentBalanceAmount")
    var currentBalanceAmount: Double?,
    @SerializedName("RoyaltyAmount")
    var royaltyAmount: Double?,
    @SerializedName("TotalEarningsAmount")
    var totalEarningsAmount: Double?,
    @SerializedName("TotalRaisedAmount")
    var totalRaisedAmount: Float?,
    @SerializedName("PendingInvestmentRequestTotalAmout")
    var pendingInvestmentRequestTotalAmout: String?,
    @SerializedName("IsDefaultAddressAvailable")
    var isDefaultAddressAvailable: Boolean,
    @SerializedName("CurrencyType")
    var currencyType: String?

)