package com.nibula.response.home.recentuploaded


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class TopArtist(
    @SerializedName("Id")
    val id: String?,
    @SerializedName("UserName")
    val userName: String?,
    @SerializedName("UserPic")
    val userPic: String?,

    var viewType: Int? = 1
)