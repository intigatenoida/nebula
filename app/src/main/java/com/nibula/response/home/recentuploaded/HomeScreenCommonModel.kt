package com.nibula.response.home.recentuploaded


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class HomeScreenCommonModel(
    @SerializedName("AppVersion")
    val appVersion: String?,
    @SerializedName("Response")
    val response: Response?,
    @SerializedName("ResponseCollection")
    val responseCollection: List<Any?>?,
    @SerializedName("ResponseMessage")
    val responseMessage: String?,
    @SerializedName("ResponseStatus")
    val responseStatus: Int?,
    @SerializedName("TotalRecords")
    val totalRecords: Int?
)