package com.nibula.response.home.recentuploaded


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class RecentUploadedResponse(
    @SerializedName("ResponseCollection")
    var responseCollection: ArrayList<ResponseCollection> = arrayListOf()
): BaseResponse()