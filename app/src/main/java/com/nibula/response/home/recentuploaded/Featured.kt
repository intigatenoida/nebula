package com.nibula.response.home.recentuploaded


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Featured(
    @SerializedName("AvailableTokens")
    val availableTokens: Double?,
    @SerializedName("CurrencyType")
    val currencyType: String?,
    @SerializedName("Id")
    val id: String?,
    @SerializedName("PricePerToken")
    val pricePerToken: Double?,
    @SerializedName("RecordArtist")
    val recordArtist: String?,
    @SerializedName("RecordImage")
    val recordImage: String?,
    @SerializedName("RecordStatus")
    val recordStatus: String?,
    @SerializedName("RecordStatusTypeId")
    val recordStatusTypeId: Int?,
    @SerializedName("RecordTitle")
    val recordTitle: String?,
    @SerializedName("RoyalityOwnershipPercentagePerToken")
    val royalityOwnershipPercentagePerToken: Double?,
    @SerializedName("TimeLeftToRelease")
    val timeLeftToRelease: Int?,
    @SerializedName("TotalTokens")
    val totalTokens: Double?
)