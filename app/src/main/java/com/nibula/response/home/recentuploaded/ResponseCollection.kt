package com.nibula.response.home.recentuploaded
/*


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.counter.TimerParams

@Keep
data class ResponseCollection(
    @SerializedName("Id")
    var id: String? = null,
    @SerializedName("RecordTypeId")
    var recordTypeId: Int? = null,
    @SerializedName("RecordType")
    var recordType: String? = null,
    @SerializedName("RecordTitle")
    var recordTitle: String? = null,
    @SerializedName("RecordImage")
    var recordImage: String? = null,

    @SerializedName("RecordArtist")
    var recordArtist: String? = null,
    @SerializedName("Description")
    var description: String? = null,
    @SerializedName("GenreTypeId")
    var genreTypeId: Any? = null,
    @SerializedName("GenreType")
    var genreType: String? = null,
    @SerializedName("PAdvisoryTypeId")
    var pAdvisoryTypeId: Any? = null,
    @SerializedName("PAdvisoryType")
    var pAdvisoryType: String? = null,
    @SerializedName("PriceAfterRelease")
    var priceAfterRelease: Double? = null,
    @SerializedName("Labels")
    var labels: String? = null,
    @SerializedName("PerformanceCopyrightNo")
    var performanceCopyrightNo: String? = null,
    @SerializedName("RecordingCopyrightNo")
    var recordingCopyrightNo: String? = null,
    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double? = null,
    @SerializedName("PricePerRoyaltyShare")
    var pricePerRoyaltyShare: Double? = null,
    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Boolean? = null,
    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Boolean? = null,
    @SerializedName("OfferingStartDate")
    var offeringStartDate: String? = null,
    @SerializedName("OfferingEndDate")
    var offeringEndDate: String? = null,
    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Long? = null,
    @SerializedName("ReleaseDate")
    var releaseDate: String? = null,
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int? = null,
    @SerializedName("RecordStatusType")
    var recordStatusType: String? = null,
    @SerializedName("ArtistsId")
    var artistsId: String? = null,
    @SerializedName("ArtistsName")
    var artistsName: String? = null,
    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double? = null,
    @SerializedName("AmountAvailableToInvest")
    var amountAvailableToInvest: Double? = null,
    @SerializedName("SharesAvailable")
    var sharesAvailable: Double? = null,
    @SerializedName("ValuePerShareInDollars")
    var valuePerShareInDollars: Double? = null,
    @SerializedName("TotalNoOfInvesters")
    var totalNoOfInvesters: Int? = null,
    @SerializedName("viewType")
    var viewType: Int = 2,


    @SerializedName("PricePerToken")
    var pricePerToken: Double,
    @SerializedName("TotalTokens")
    var totalTokens: Int,

    @SerializedName("AvailableTokens")
    var availableTokens: Int,

    @SerializedName("SoldTokens")
    var soldTokens: Int,


    @SerializedName("RecordStatus")
    var recordStatus: String,

    @SerializedName("CurrencyType")
    var currencyType: String,
    @SerializedName("RecordsCreatedBy")
    var RecordsCreatedBy: String,


    @SerializedName("RecordArtistImage")
    var recordArtistImage: String,

    @SerializedName("RoyalityOwnershipPercentagePerToken")
    var royalityOwnershipPercentagePerToken: Double
): TimerParams()
*/





import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.counter.TimerParams

@Keep
class ResponseCollection : TimerParams() {
    @SerializedName("Id")
    var id: String? = null
    @SerializedName("RecordTypeId")
    var recordTypeId: Int? = null
    @SerializedName("RecordType")
    var recordType: String? = null
    @SerializedName("RecordTitle")
    var recordTitle: String? = null
    @SerializedName("RecordImage")
    var recordImage: String? = null

    @SerializedName("RecordArtist")
    var recordArtist: String? = null
    @SerializedName("Description")
    var description: String? = null
    @SerializedName("GenreTypeId")
    var genreTypeId: Any? = null
    @SerializedName("GenreType")
    var genreType: String? = null
    @SerializedName("PAdvisoryTypeId")
    var pAdvisoryTypeId: Any? = null
    @SerializedName("PAdvisoryType")
    var pAdvisoryType: String? = null
    @SerializedName("PriceAfterRelease")
    var priceAfterRelease: Double? = null
    @SerializedName("Labels")
    var labels: String? = null
    @SerializedName("PerformanceCopyrightNo")
    var performanceCopyrightNo: String? = null
    @SerializedName("RecordingCopyrightNo")
    var recordingCopyrightNo: String? = null
    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double? = null
    @SerializedName("PricePerRoyaltyShare")
    var pricePerRoyaltyShare: Double? = null
    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Boolean? = null
    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Boolean? = null
    @SerializedName("OfferingStartDate")
    var offeringStartDate: String? = null
    @SerializedName("OfferingEndDate")
    var offeringEndDate: String? = null
    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Long? = null
    @SerializedName("ReleaseDate")
    var releaseDate: String? = null
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int? = null
    @SerializedName("RecordStatusType")
    var recordStatusType: String? = null
    @SerializedName("ArtistsId")
    var artistsId: String? = null
    @SerializedName("ArtistsName")
    var artistsName: String? = null
    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double? = null
    @SerializedName("AmountAvailableToInvest")
    var amountAvailableToInvest: Double? = null
    @SerializedName("SharesAvailable")
    var sharesAvailable: Double? = null
    @SerializedName("ValuePerShareInDollars")
    var valuePerShareInDollars: Double? = null
    @SerializedName("TotalNoOfInvesters")
    var totalNoOfInvesters: Int? = null
    @SerializedName("viewType")
    var viewType: Int = 2


    @SerializedName("PricePerToken")
    var pricePerToken: Double=0.0
    @SerializedName("TotalTokens")
    var totalTokens: Int=0

    @SerializedName("AvailableTokens")
    var availableTokens: Int=0

    @SerializedName("SoldTokens")
    var soldTokens: Int=0


    @SerializedName("RecordStatus")
    var recordStatus: String?=null

    @SerializedName("CurrencyType")
    var currencyType: String?=null
    @SerializedName("RecordsCreatedBy")
    var RecordsCreatedBy: String?=null


    @SerializedName("RecordArtistImage")
    var recordArtistImage: String?=null

    @SerializedName("RoyalityOwnershipPercentagePerToken")
    var royalityOwnershipPercentagePerToken: Double=0.0

}