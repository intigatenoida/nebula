package com.nibula.response.home.recentuploaded

import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.artists.FreaturedArtistResponse
import com.nibula.response.topoffertopartist.ArtistInfoData
import com.nibula.response.topoffertopartist.TrendingOfferingData

@Keep
data class Response(
    @SerializedName("FeaturedList")
    val featuredList: MutableList<FreaturedArtistResponse>?,
    @SerializedName("TopArtists")
    val topArtists: MutableList<ArtistInfoData>?,
    @SerializedName("TrendingList")
    val trendingList: MutableList<TrendingOfferingData>?,

    @SerializedName("IsShowFirebaseBanner")
    val isShowFirebaseBanner: Boolean?


)