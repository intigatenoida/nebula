package com.nibula.response.mycollectorresponse

import com.nibula.response.myinvestmentresponse.ResponseCollection

import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class MyCollecrtorsResponse(
    @SerializedName("Response")
    var response: Any?,
    @SerializedName("ResponseCollection")
    var responseCollection: MutableList<ResponseCollection>?,
    @SerializedName("TotalRecords")
    var totalRecords: Int?
):BaseResponse()