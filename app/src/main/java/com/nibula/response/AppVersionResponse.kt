package com.nibula.response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class AppVersionResponse(
    @SerializedName("ForceUpdate")
    var forceUpdate: Boolean = false,
    @SerializedName("ResponseMessage")
    var responseMessage: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int = 0,
    @SerializedName("AndroidVersion")
    var androidVersion: String = ""


)