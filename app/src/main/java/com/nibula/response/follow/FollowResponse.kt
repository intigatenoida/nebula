package com.nibula.response.follow

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class FollowResponse (

    @SerializedName("IsSelfUser")
    val isSelfUser:Boolean?

):BaseResponse()