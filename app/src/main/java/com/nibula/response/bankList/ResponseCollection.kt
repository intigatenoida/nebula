package com.nibula.response.bankList


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("BankId")
    var bankId: Int,
    @SerializedName("BankName")
    var bankName: String
)