package com.nibula.response.bankList


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class BankListResponse(
    @SerializedName("AppVersion")
    var appVersion: String,
    @SerializedName("Response")
    var response: Any,
    @SerializedName("ResponseCollection")
    var responseCollection: List<ResponseCollection>,
    @SerializedName("ResponseMessage")
    var responseMessage: String,
    @SerializedName("ResponseStatus")
    var responseStatus: Int,
    @SerializedName("TotalRecords")
    var totalRecords: Int
)