package com.nibula.response.carificationResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Email(
    @SerializedName("Message")
    var message: String,
    @SerializedName("Status")
    var status: Int,
    @SerializedName("RegisteredValue")
    var registeredValue: String

)