package com.nibula.response.carificationResponse


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Phone(
    @SerializedName("Message")
    var message: String,
    @SerializedName("Status")
    var status: Int,
    @SerializedName("RegisteredValue")
    var registeredValue: String
)