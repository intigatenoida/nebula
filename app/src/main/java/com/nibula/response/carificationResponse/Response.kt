package com.nibula.response.carificationResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("Email")
    var email: Email,
    @SerializedName("Phone")
    var phone: Phone
)