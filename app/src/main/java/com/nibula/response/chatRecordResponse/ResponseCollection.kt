package com.nibula.response.chatRecordResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("RecordId")
    var recordId: String = "",
    @SerializedName("RecordImage")
    var recordImage: String = "",
    @SerializedName("PartnerId")
    var partnerId: String = "",
    @SerializedName("isSelected")
    var isSelected:Boolean=false
)