package com.nibula.response.chatRecordResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ChatRecordResponse(
    @SerializedName("recordDetail")
    var recordDetail: RecordDetail = RecordDetail()
)