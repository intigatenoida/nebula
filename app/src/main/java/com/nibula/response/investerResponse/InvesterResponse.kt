package com.nibula.response.investerResponse


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class InvesterResponse(

    @SerializedName("ArtistInvestors")
    var artistInvestors: List<ArtistInvestor>,
    @SerializedName("ResponseMessage")
    var responseMessage: String? = null,
    @SerializedName("ResponseStatus")
    var responseStatus: Int? = null,
    @SerializedName("TotalRecords")
    var TotalRecords: Int? = null

)