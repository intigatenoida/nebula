package com.nibula.response.investerResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ArtistInvestor(
    @SerializedName("CurrencyType")
    var currencyType: Any,
    @SerializedName("Id")
    var id: Int,
    @SerializedName("InvestmentRequestDate")
    var investmentRequestDate: String,
    @SerializedName("InvestorId")
    var investorId: String,
    @SerializedName("InvestorImage")
    var investorImage: String,
    @SerializedName("InvestorName")
    var investorName: String,
    @SerializedName("RecordId")
    var recordId: String,
    @SerializedName("RecordImage")
    var recordImage: String,
    @SerializedName("RecordTitle")
    var recordTitle: String,
    @SerializedName("RecordType")
    var recordType: String,
    @SerializedName("RequestedShares")
    var requestedShares: Int,
    @SerializedName("TotalRequestedAmount")
    var totalRequestedAmount: Double
)