package com.nibula.response.chatHistoryResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ChatHistoryResponse(
    @SerializedName("recordDetail")
    var recordDetail: RecordDetail = RecordDetail()
)