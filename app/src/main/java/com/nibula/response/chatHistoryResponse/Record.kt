package com.nibula.response.chatHistoryResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Record(
    @SerializedName("Artist")
    var artist: String? = "",
    @SerializedName("ArtistId")
    var artistId: String? = "",
    @SerializedName("ArtistImage")
    var artistImage: String? = "",
    @SerializedName("Id")
    var id: String = "",
    @SerializedName("Title")
    var title: String? = "",
    @SerializedName("RecordImage")
    var recordImage: String? = ""


)