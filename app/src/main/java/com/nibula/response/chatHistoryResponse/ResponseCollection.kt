package com.nibula.response.chatHistoryResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("ActionDate")
    var actionDate: String = "",
    @SerializedName("IsRead")
    var isRead: Boolean = false,
    @SerializedName("IsRight")
    var isRight: Boolean = false,
    @SerializedName("MediaFile")
    var mediaFile: String = "",
    @SerializedName("MediaFileType")
    var mediaFileType: Int = 0,
    @SerializedName("MediaFileTypeId")
    var mediaFileTypeId: Int = 0,
    @SerializedName("Message")
    var message: String="",
    @SerializedName("MessageId")
    var messageId: Int = 0,
    @SerializedName("Record")
    var record: Record = Record(),
    @SerializedName("RecordId")
    var recordId: String ?= null,
    @SerializedName("TotalRecords")
    var totalRecords: Int = 0,
    @SerializedName("time")
    var time: String = "",
    @SerializedName("viewType")
    var viewType: Int = 0
)