package com.nibula.response.signup


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class SignupResponse(
    @SerializedName("Response")
    var response: Response = Response(),
    @SerializedName("ResponseCollection")
    var responseCollection: List<Any> = listOf(),
    @SerializedName("TotalRecords")
    var totalRecords: Any? = Any()
):BaseResponse()