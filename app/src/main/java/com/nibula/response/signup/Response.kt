package com.nibula.response.signup


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("token")
    var token: Token = Token(),
    @SerializedName("userInfo")
    var userInfo: UserInfo = UserInfo()
)