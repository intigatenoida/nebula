package com.nibula.response.signup


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class UserInfo(
    @SerializedName("email")
    var email: String = "",
    @SerializedName("given_name")
    var givenName: String = "",
    @SerializedName("sub")
    var sub: String = "",
    @SerializedName("verifiedEmail")
    var verifiedEmail: String = "",
    @SerializedName("verifiedPhone")
    var verifiedPhone: String = ""
)