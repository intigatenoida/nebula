package com.nibula.response.signup


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Token(
    @SerializedName("access_token")
    var accessToken: String = "",
    @SerializedName("expires_in")
    var expiresIn: Int = 0,
    @SerializedName("refresh_token")
    var refreshToken: String = "",
    @SerializedName("token_type")
    var tokenType: String = ""
)