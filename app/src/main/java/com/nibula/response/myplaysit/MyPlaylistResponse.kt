package com.nibula.response.myplaysit

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class MyPlaylistResponse (

    @SerializedName("ResponseCollection")
    var playListData:MutableList<MyPlayListData> = mutableListOf()

):BaseResponse()