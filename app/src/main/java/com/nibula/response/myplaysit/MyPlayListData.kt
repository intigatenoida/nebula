package com.nibula.response.myplaysit

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.artists.ArtistData

@Keep
data class MyPlayListData(

    @SerializedName("StartWith")
    var startWith:String?,
    @SerializedName("TotalRecords")
    var totalRecords:Int?,
    @SerializedName("ArtistList")
    var artistData:MutableList<ArtistData>?

)