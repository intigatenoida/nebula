package com.nibula.response.login


import com.google.gson.annotations.SerializedName

data class Token(
    @SerializedName("access_token")
    var accessToken: String = "",
    @SerializedName("expires_in")
    var expiresIn: Int = 0,
    @SerializedName("token_type")
    var tokenType: String = "",
    @SerializedName("refresh_token")
    var refresh_token: String = "",
    @SerializedName("blockchain_wallet_address")
    var blockchain_wallet_address: String? = ""
)