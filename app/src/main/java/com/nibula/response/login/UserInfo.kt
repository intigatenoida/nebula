package com.nibula.response.login


import com.google.gson.annotations.SerializedName

data class UserInfo(
    @SerializedName("email")
    var email: String = "",
    @SerializedName("given_name")
    var givenName: String = "",
    @SerializedName("sub")
    var sub: String = "",
    @SerializedName("verifiedEmail")
    var verifiedEmail: String = "",
    @SerializedName("verifiedPhone")
    var verifiedPhone: String = "",
    @SerializedName("PhoneNumber")
    var phoneNumber: String = "",
    @SerializedName("CountryCode")
    var countryCode: String = "",
    @SerializedName("blockchain_wallet_address")
    var blockchain_wallet_address: String = ""
)