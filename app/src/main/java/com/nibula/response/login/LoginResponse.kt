package com.nibula.response.login


import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

data class LoginResponse(
    @SerializedName("token")
    var token: Token = Token(),
    @SerializedName("userInfo")
    var userInfo: UserInfo = UserInfo()
): BaseResponse()