package com.nibula.response.recorddetail

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class GetInvestorStatusData(

	@SerializedName("MyStatusTypeOnRecord")
	var mystatustypeonrecord:Int? = null,
	@SerializedName("MyStatusOnRecord")
	var mystatusonrecord:String? = null ,
	@SerializedName("TotalPendingInvestmentRequests")
	var totalPendingInvestmentRequests:Int? = null,
	@SerializedName("RecommendationMessage")
	var recommendationMessage:String? = null,
	@SerializedName("SharesAllowedToMe")
var SharesAllowedToMe:Int? = 0
)