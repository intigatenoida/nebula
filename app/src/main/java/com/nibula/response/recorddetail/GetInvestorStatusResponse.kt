package com.nibula.response.recorddetail

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class GetInvestorStatusResponse(

	@SerializedName("Response")
	var response:GetInvestorStatusData? = null,
	@SerializedName("PendingInvestments")
	var pendinginvestments:Int? = null
):BaseResponse()