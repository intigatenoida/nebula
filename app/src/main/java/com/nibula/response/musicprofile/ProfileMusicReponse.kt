package com.nibula.response.musicprofile


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ProfileMusicReponse(
    @SerializedName("myMusic")
    var myMusic: MyMusic? = null
)