package com.nibula.response.musicprofile


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class MyMusic(
    @SerializedName("TotalRecords")
    var totalRecords: Int?,
    @SerializedName("ResponseCollection")
    var responseCollection: MutableList<ResponseCollection>?
):BaseResponse()