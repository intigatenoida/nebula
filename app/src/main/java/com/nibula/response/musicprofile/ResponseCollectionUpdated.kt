package com.nibula.response.musicprofile


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollectionUpdated(
    @SerializedName("AmountAvailableToInvest")
    var amountAvailableToInvest: Double? = 0.0,
    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Boolean? = false,
    @SerializedName("ArtistsId")
    var artistsId: String? = "",
    @SerializedName("ArtistsName")
    var artistsName: String? = "",
    @SerializedName("CopyrightSocietyRecording")
    var copyrightSocietyRecording: Any? = Any(),
    @SerializedName("CopyrightSocietyWork")
    var copyrightSocietyWork: Any? = Any(),
    @SerializedName("CreatedOn")
    var createdOn: String? = "",
    @SerializedName("CurrencyType")
    var currencyType: String? = "",
    @SerializedName("CurrencyTypeId")
    var currencyTypeId: Int? = 0,
    @SerializedName("GenreType")
    var genreType: String? = "",
    @SerializedName("GenreTypeIds")
    var genreTypeIds: String? = "",
    @SerializedName("ISCRCode")
    var iSCRCode: String? = "",
    @SerializedName("ISWCCode")
    var iSWCCode: String? = "",
    @SerializedName("Id")
    var id: String? = "",
    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double? = 0.0,
    @SerializedName("IsSelfUser")
    var isSelfUser: Boolean? = false,
    @SerializedName("MessageToCoOwners")
    var messageToCoOwners: String? = "",
    @SerializedName("OfferingEndDate")
    var offeringEndDate: String? = "",
    @SerializedName("OfferingStartDate")
    var offeringStartDate: String? = "",
    @SerializedName("OneOffPaymentForMusicStreamAccess")
    var oneOffPaymentForMusicStreamAccess: Double? = 0.0,
    @SerializedName("PreviewDurationInSec")
    var previewDurationInSec: Int? = 0,
    @SerializedName("PricePerRoyaltyShare")
    var pricePerRoyaltyShare: Double? = 0.0,
    @SerializedName("PricePerRoyaltyShareRecording")
    var pricePerRoyaltyShareRecording: Any? = Any(),
    @SerializedName("PricePerRoyaltyShareWork")
    var pricePerRoyaltyShareWork: Any? = Any(),
    @SerializedName("RecordImage")
    var recordImage: String? = "",
    @SerializedName("RecordStatusType")
    var recordStatusType: String? = "",
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int? = 0,
    @SerializedName("RecordTitle")
    var recordTitle: String? = "",
    @SerializedName("RecordType")
    var recordType: String? = "",
    @SerializedName("RecordTypeId")
    var recordTypeId: Int? = 0,
    @SerializedName("ReleaseDate")
    var releaseDate: String? = "",
    @SerializedName("ScreenType")
    var screenType: Int? = 0,
    @SerializedName("SharesAvailable")
    var sharesAvailable: Double? = 0.0,
    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Boolean? = false,
    @SerializedName("SocietyNameRecording")
    var societyNameRecording: Any? = Any(),
    @SerializedName("SocietyNameWork")
    var societyNameWork: Any? = Any(),
    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Int? = 0,
    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double? = 0.0,
    @SerializedName("TotalNoOfInvesters")
    var totalNoOfInvesters: Int? = 0,
    @SerializedName("TotalRecords")
    var totalRecords: Int? = 0,
    @SerializedName("ValueOfAShare")
    var valueOfAShare: Double? = 0.0
)