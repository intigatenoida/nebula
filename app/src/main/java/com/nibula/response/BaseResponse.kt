package com.nibula.response

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
open class BaseResponse(

    @SerializedName("ResponseMessage")
    var responseMessage: String? = null,
    @SerializedName("ResponseStatus")
    var responseStatus: Int? = null,
    @SerializedName("AppVersion")
    var appVersion: String? = null,
    @SerializedName("WithdrawalFundRequestsId")
    var withdrawalFundRequestsId: Int = 0
)