package com.nibula.response.defaultListResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class DefaultUserResponse(
    @SerializedName("DefaultSharing")
    var defaultSharing: DefaultSharing
)