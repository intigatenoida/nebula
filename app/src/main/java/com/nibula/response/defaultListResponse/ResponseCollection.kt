package com.nibula.response.defaultListResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("Bio")
    var bio: String,
    @SerializedName("Name")
    var name: String,
    @SerializedName("UserId")
    var userId: String,
    @SerializedName("UserImage")
    var userImage: String,
    @SerializedName("UserName")
    var userName: String,
    @SerializedName("isChecked")
    var isChecked: Boolean


)