package com.nibula.response.artists


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.details.File

@Keep
data class ArtistData(
    @SerializedName("ArtistId")
    val artistId: String?,
    @SerializedName("ArtistImage")
    val artistImage: String?,
    @SerializedName("ArtistName")
    val artistName: String?,
    @SerializedName("Verified")
    val verified: Boolean?,
    @SerializedName("RecordId")
    val recordId: String? = "",
    @SerializedName("RecordImage")
    val recordImage: String? = "",
    @SerializedName("RecordTitle")
    val recordTitle: String? = "",
    var isEditTextEnabled: Boolean = true,
    var key: String = "",
    var viewType: Int = 1,

    @SerializedName("RecordType")
    val RecordType: String? = "",
    @SerializedName("RecordTypeId")
    val RecordTypeId: Int = 0,

    @SerializedName("FileId")
    val FileId: String = "",


    @SerializedName("IsFavourite") var IsFavourite: Boolean = false,

    @SerializedName("Files")
    var files: MutableList<File>? = null,

    var isPlaying: Boolean = false,
)