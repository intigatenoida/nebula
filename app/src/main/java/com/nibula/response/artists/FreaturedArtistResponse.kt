package com.nibula.response.artists

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.counter.TimerParams

@Keep
class FreaturedArtistResponse : TimerParams() {
    @SerializedName("Id")
    var Id: String? = null

    @SerializedName("RecordTitle")
    var RecordTitle: String? = null

    @SerializedName("RecordArtist")
    var RecordArtist: String? = null

    @SerializedName("RecordImage")
    var RecordImage: String? = null

    @SerializedName("PricePerToken")
    var PricePerToken: String? = null

    @SerializedName("TimeLeftToRelease")
    var TimeLeftToRelease: Long? = 0

    @SerializedName("AvailableTokens")
    var AvailableTokens: Int? = 0


    @SerializedName("RecordTypeId")
    val RecordTypeId: Int = 0
    @SerializedName("RecordStatus")
    var RecordStatus: String? = null

    @SerializedName("RecordStatusTypeId")
    var RecordStatusTypeId: String? = null
    var viewType: Int? = 1

}