package com.nibula.response.artists

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class ArtistListResponse (

	@SerializedName("Artists")
	var artistList:MutableList<ArtistData> = mutableListOf()

):BaseResponse()