package com.nibula.response.artists;

public class PlayMusic {
   public  Boolean isPlaying;

    public Boolean getPlaying() {
        return isPlaying;
    }

    public void setPlaying(Boolean playing) {
        isPlaying = playing;
    }


    // default constructor
    public PlayMusic() {
    }

    public PlayMusic(Boolean playing) {
        this.isPlaying = playing;
    }


}