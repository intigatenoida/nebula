package com.nibula.response.recordshares

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class RequestToInvestResponse(

    @SerializedName("Id")
    var id:String?,
    @SerializedName("PId")
    var pId:String?,
    @SerializedName("custom")
    var custom:String?


):BaseResponse()
