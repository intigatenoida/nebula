package com.nibula.response.mp3Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class mp3Response(
    @SerializedName("AppVersion")
    var appVersion: String = "",
    @SerializedName("Response")
    var response: Response = Response(),
    @SerializedName("ResponseCollection")
    var responseCollection: List<Any> = listOf(),
    @SerializedName("ResponseMessage")
    var responseMessage: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int = 0,
    @SerializedName("TotalRecords")
    var totalRecords: Any? = Any()
)