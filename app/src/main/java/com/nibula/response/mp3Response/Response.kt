package com.nibula.response.mp3Response


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Response(
	@SerializedName("ArtistName")
	var artistName: String = "",
	@SerializedName("RecordDurationInSeconds")
	var recordDurationInSeconds: Int = 0,
	@SerializedName("RecordId")
	var recordId: String = "",
	@SerializedName("SongFilePath")
	var songFilePath: String = "",
	@SerializedName("SongTitle")
	var songTitle: String = "",
	@SerializedName("RecordImagePath")
	var RecordImagePath: String?=null

)