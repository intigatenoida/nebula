package com.nibula.response.registerResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class RegisterResponse(
    @SerializedName("MobileVerificationRequired")
    var mobileVerificationRequired: Boolean = false,
    @SerializedName("RequestCode")
    var requestCode: String = "",
    @SerializedName("UserId")
    var userId: String = ""
):BaseResponse()