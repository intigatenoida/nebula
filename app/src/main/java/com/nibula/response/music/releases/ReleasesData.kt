package com.nibula.response.music.releases

import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ReleasesData(
    @SerializedName("Id")
    var id: String?,
    @SerializedName("RecordTypeId")
    var recordTypeId: Int?,
    @SerializedName("RecordType")
    var recordType: String?,
    @SerializedName("RecordTitle")
    var recordTitle: String?,
    @SerializedName("RecordImage")
    var recordImage: String?,
    @SerializedName("Description")
    var description: String?,
    @SerializedName("GenreTypeId")
    var genreTypeId: Any?,
    @SerializedName("GenreType")
    var genreType: String?,
    @SerializedName("PAdvisoryTypeId")
    var pAdvisoryTypeId: Any?,
    @SerializedName("PAdvisoryType")
    var pAdvisoryType: String?,
    @SerializedName("PriceAfterRelease")
    var priceAfterRelease: Double?,
    @SerializedName("Labels")
    var labels: String?,
    @SerializedName("PerformanceCopyrightNo")
    var performanceCopyrightNo: String?,
    @SerializedName("RecordingCopyrightNo")
    var recordingCopyrightNo: String?,
    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double?,
    @SerializedName("PricePerRoyaltyShare")
    var pricePerRoyaltyShare: Double?,
    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Boolean?,
    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Boolean?,
    @SerializedName("OfferingStartDate")
    var offeringStartDate: String?,
    @SerializedName("OfferingEndDate")
    var offeringEndDate: String?,
    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Long?,
    @SerializedName("ReleaseDate")
    var releaseDate: String?,
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int?,
    @SerializedName("RecordStatusType")
    var recordStatusType: String?,
    @SerializedName("ArtistsId")
    var artistsId: String?,
    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double?,
    @SerializedName("AmountAvailableToInvest")
    var amountAvailableToInvest: Double?,
    @SerializedName("SharesAvailable")
    var sharesAvailable: Double?,
    @SerializedName("ValuePerShareInDollars")
    var valuePerShareInDollars: Double?,
    @SerializedName("TotalNoOfInvesters")
    var totalNoOfInvesters: Int?
)