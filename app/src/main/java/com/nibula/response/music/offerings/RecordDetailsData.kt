package com.nibula.response.music.offerings


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.counter.TimerParams

@Keep
data class RecordDetailsData(
    @SerializedName("Id")
    var id: String?,
    @SerializedName("RecordType")
    var recordType: String?,
    @SerializedName("RecordTypeId")
    var recordTypeId: Int?,
    @SerializedName("GenreType")
    var genreType: String?,
    @SerializedName("RecordTitle")
    var recordTitle: String?,
    @SerializedName("RecordImage")
    var recordImage: String?,
    @SerializedName("Description")
    var description: String?,
    @SerializedName("PAdvisoryType")
    var pAdvisoryType: String?,
    @SerializedName("PriceAfterRelease")
    var priceAfterRelease: Double?,
    @SerializedName("Labels")
    var labels: String?,
    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double?,
    @SerializedName("MaxRoyaltyPerInvestor")
    var maxRoyaltyPerInvestor: Double?,
    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Int?,
    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Int?,
    @SerializedName("OfferingStartDate")
    var offeringStartDate: String?,
    @SerializedName("OfferingEndDate")
    var offeringEndDate: String?,
    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Long?,
    @SerializedName("ReleaseDate")
    var releaseDate: String?,
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int?,
    @SerializedName("RecordStatusType")
    var recordStatusType: String?,
    @SerializedName("ArtistId")
    var artistId: String?,
    @SerializedName("ArtistName")
    var artistName: String?,
    @SerializedName("CurrencyType")
    var currencyType: String?,
    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double?,
    @SerializedName("AmountAvailableToInvest")
    var amountAvailableToInvest: Double?,
    @SerializedName("SharesAvailable")
    var sharesAvailable: Double?,
    @SerializedName("ValuePerShareInDollars")
    var valuePerShareInDollars: Double?,
    @SerializedName("TotalNoOfInvesters")
    var totalNoOfInvesters: Int?,
    @SerializedName("Files")
    var files: MutableList<Any>?
):TimerParams()