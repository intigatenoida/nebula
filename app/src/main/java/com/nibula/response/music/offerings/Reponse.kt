package com.nibula.response.music.offerings

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Reponse(

    @SerializedName("RecordDetail")
    var recordsList: MutableList<RecordDetailsData> = mutableListOf(),
    @SerializedName("TotalRecords")
    var totalRecords: Int? = null

)