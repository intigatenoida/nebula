package com.nibula.response.music.offerings

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class OfferingResponses (

    @SerializedName("Response")
    var response:Reponse? = null

): BaseResponse()