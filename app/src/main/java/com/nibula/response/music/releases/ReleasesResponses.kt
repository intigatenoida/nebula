package com.nibula.response.music.releases

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class ReleasesResponses(
    @SerializedName("ResponseCollection")
    var responseDataList: MutableList<ReleasesData> = mutableListOf(),
    @SerializedName("TotalRecords")
    var totalRecords:Int
) : BaseResponse()