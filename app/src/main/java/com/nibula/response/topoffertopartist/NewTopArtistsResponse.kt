package com.nibula.response.topoffertopartist


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

import com.nibula.response.BaseResponse

@Keep
data class NewTopArtistsResponse(

    @SerializedName("ResponseCollection")
    var responseCollection: MutableList<ArtistInfoData> = mutableListOf()

) : BaseResponse()