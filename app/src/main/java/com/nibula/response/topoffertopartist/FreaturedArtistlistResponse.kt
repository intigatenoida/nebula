package com.nibula.response.topoffertopartist


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse
import com.nibula.response.artists.FreaturedArtistResponse

@Keep
data class FreaturedArtistlistResponse(
    @SerializedName("ResponseCollection")
    var responseCollection: MutableList<FreaturedArtistResponse>
):BaseResponse()