package com.nibula.response.topoffertopartist


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class TrandingOfferingsData(
    @SerializedName("Id")
    var id: String? = null,
    @SerializedName("RecordTypeId")
    var recordTypeId: Int?= null,
    @SerializedName("RecordType")
    var recordType: String?= null,
    @SerializedName("RecordTitle")
    var recordTitle: String?= null,
    @SerializedName("RecordImage")
    var recordImage: String?= null,
    @SerializedName("Description")
    var description: String?= null,
    @SerializedName("GenreTypeId")
    var genreTypeId: String?= null,
    @SerializedName("GenreType")
    var genreType: String?= null,
    @SerializedName("PAdvisoryTypeId")
    var pAdvisoryTypeId: Int?= null,
    @SerializedName("PAdvisoryType")
    var pAdvisoryType: String?= null,
    @SerializedName("PriceAfterRelease")
    var priceAfterRelease: Double?= null,
    @SerializedName("Labels")
    var labels: String?= null,
    @SerializedName("PerformanceCopyrightNo")
    var performanceCopyrightNo: String?= null,
    @SerializedName("RecordingCopyrightNo")
    var recordingCopyrightNo: String?= null,
    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double?= null,
    @SerializedName("MaxRoyaltyPerInvestor")
    var maxRoyaltyPerInvestor: Double?= null,
    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Boolean?= null,
    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Boolean?= null,
    @SerializedName("OfferingStartDate")
    var offeringStartDate: String?= null,
    @SerializedName("OfferingEndDate")
    var offeringEndDate: String?= null,
    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Long?= null,
    @SerializedName("ReleaseDate")
    var releaseDate: String?= null,
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int?= null,
    @SerializedName("RecordStatusType")
    var recordStatusType: String?= null,
    @SerializedName("ArtistsId")
    var artistsId: String?= null,
    @SerializedName("SharesAvailable")
    var sharesAvailable:Int?= null,
    @SerializedName("Investors")
    var investors: Any?= null,
    @SerializedName("AmountRaised")
    var amountRaised: Any?= null,
    @SerializedName("AmountEarned")
    var amountEarned: Any?= null
)