package com.nibula.response.topoffertopartist

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.counter.TimerParams

@Keep
class ArtistInfoData {

    @SerializedName("Id")
    var id:String?= null

    @SerializedName("UserName")
    var userName:String?= null

    @SerializedName("UserPic")
    var userPic:String?= null

    @SerializedName("IsSelfUser")
    var isSelfUser:Boolean? = null

    var viewType: Int? = 1

}