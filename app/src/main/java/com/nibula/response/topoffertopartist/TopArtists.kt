package com.nibula.response.topoffertopartist


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class TopArtists(
    @SerializedName("ResponseCollection")
    var responseCollection: MutableList<TopArtistData> = mutableListOf()
): BaseResponse()