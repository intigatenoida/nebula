package com.nibula.response.topoffertopartist

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.counter.TimerParams

@Keep
class TrendingOfferingData : TimerParams() {
    @SerializedName("Id")
    var id: String? = null

    @SerializedName("RecordTypeId")
    var recordTypeId: Int? = null

    @SerializedName("CurrencyType")
    var currencyType: String? = null

    @SerializedName("RecordType")
    var recordType: String? = null

    @SerializedName("RecordTitle")
    var recordTitle: String? = null

    @SerializedName("RecordArtist")
    var recordArtist: String? = null

    @SerializedName("RecordImage")
    var recordImage: String? = null

    @SerializedName("Description")
    var description: String? = null

    @SerializedName("GenreTypeId")
    var genreTypeId: String? = null

    @SerializedName("GenreType")
    var genreType: String? = null

    @SerializedName("PAdvisoryTypeId")
    var pAdvisoryTypeId: Int? = null

    @SerializedName("PAdvisoryType")
    var pAdvisoryType: String? = null

    @SerializedName("PriceAfterRelease")
    var priceAfterRelease: Double? = null

    @SerializedName("Labels")
    var labels: String? = null

    @SerializedName("PerformanceCopyrightNo")
    var performanceCopyrightNo: String? = null

    @SerializedName("RecordingCopyrightNo")
    var recordingCopyrightNo: String? = null

    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double? = null

    @SerializedName("MaxRoyaltyPerInvestor")
    var maxRoyaltyPerInvestor: Double? = null

    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Boolean? = null

    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Boolean? = null

    @SerializedName("OfferingStartDate")
    var offeringStartDate: String? = null

    @SerializedName("OfferingEndDate")
    var offeringEndDate: String? = null

    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Long? = null

    @SerializedName("ReleaseDate")
    var releaseDate: String? = null

    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int? = null

    @SerializedName("RecordStatusType")
    var recordStatusType: String? = null

    @SerializedName("ArtistsId")
    var artistsId: String? = null


    @SerializedName("RecordStatus")
    var recordStatus: String? = null


    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double? = null

    @SerializedName("PricePerToken")
    var pricePerToken: Double? = null



    @SerializedName("TotalTokens")
    var totalTokens: Int? = 0

    @SerializedName("AvailableTokens")
    var availableTokens: Int? = 0


    @SerializedName("AmountAvailableToInvest")
    var amountAvailableToInvest: Double? = null



    @SerializedName("RoyalityOwnershipPercentagePerToken")
    var royalityOwnershipPercentagePerToken: Double? = null

    @SerializedName("SharesAvailable")
    var sharesAvailable: Double? = null

    @SerializedName("ValuePerShareInDollars")
    var valuePerShareInDollars: Double? = null

    @SerializedName("TotalNoOfInvesters")
    var totalNoOfInvesters: Int? = null
    var viewType: Int? = 1

}