package com.nibula.response.topoffertopartist


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollectionX(
    @SerializedName("amountEarned")
    var amountEarned: Any? = Any(),
    @SerializedName("amountRaised")
    var amountRaised: Any? = Any(),
    @SerializedName("approveEachInvestor")
    var approveEachInvestor: Boolean = false,
    @SerializedName("artistsId")
    var artistsId: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("genreType")
    var genreType: String = "",
    @SerializedName("genreTypeId")
    var genreTypeId: Int = 0,
    @SerializedName("id")
    var id: String = "",
    @SerializedName("investmentAmountNeeded")
    var investmentAmountNeeded: Double = 0.0,
    @SerializedName("investors")
    var investors: Any? = Any(),
    @SerializedName("labels")
    var labels: String = "",
    @SerializedName("maxRoyaltyPerInvestor")
    var maxRoyaltyPerInvestor: Double = 0.0,
    @SerializedName("offeringEndDate")
    var offeringEndDate: String = "",
    @SerializedName("offeringStartDate")
    var offeringStartDate: String = "",
    @SerializedName("pAdvisoryType")
    var pAdvisoryType: String = "",
    @SerializedName("pAdvisoryTypeId")
    var pAdvisoryTypeId: Int = 0,
    @SerializedName("performanceCopyrightNo")
    var performanceCopyrightNo: String = "",
    @SerializedName("priceAfterRelease")
    var priceAfterRelease: Double = 0.0,
    @SerializedName("recordImage")
    var recordImage: String = "",
    @SerializedName("recordStatusType")
    var recordStatusType: String = "",
    @SerializedName("recordStatusTypeId")
    var recordStatusTypeId: Int = 0,
    @SerializedName("recordTitle")
    var recordTitle: String = "",
    @SerializedName("recordType")
    var recordType: String = "",
    @SerializedName("recordTypeId")
    var recordTypeId: Int = 0,
    @SerializedName("recordingCopyrightNo")
    var recordingCopyrightNo: String = "",
    @SerializedName("releaseDate")
    var releaseDate: String = "",
    @SerializedName("sharesAvailable")
    var sharesAvailable: Int = 0,
    @SerializedName("showInvestorsToPublic")
    var showInvestorsToPublic: Boolean = false,
    @SerializedName("timeLeftToRelease")
    var timeLeftToRelease: Long = 0
)