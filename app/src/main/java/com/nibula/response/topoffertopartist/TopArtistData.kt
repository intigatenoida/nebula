package com.nibula.response.topoffertopartist


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class TopArtistData(
    @SerializedName("ArtistId")
    var artistId: String?= null,
    @SerializedName("artistImage")
    var artistImage: String?= null,
    @SerializedName("artistName")
    var artistName: String?= null
)