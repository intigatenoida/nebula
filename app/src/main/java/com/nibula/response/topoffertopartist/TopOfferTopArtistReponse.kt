package com.nibula.response.topoffertopartist


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class TopOfferTopArtistReponse(
    @SerializedName("trendingOfferings")
    var trendingOffering: TrendingOffering? = null,
    @SerializedName("topArtists")
    var topArtists: MutableList<ArtistInfoData> = mutableListOf()
)