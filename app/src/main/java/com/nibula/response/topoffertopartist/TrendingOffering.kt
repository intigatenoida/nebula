package com.nibula.response.topoffertopartist

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class TrendingOffering(

    @SerializedName("ResponseCollection")
    var responseCollection: MutableList<TrendingOfferingData> = mutableListOf()

): BaseResponse()