package com.nibula.response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class VerifyOtpResponse(
    @SerializedName("ResponseMessage")
    var responseMessage: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int = 0,
    @SerializedName("token")
    var token: String = ""
)