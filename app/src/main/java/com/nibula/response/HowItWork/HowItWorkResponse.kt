package com.nibula.response.HowItWork


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class HowItWorkResponse(
    @SerializedName("AppVersion")
    var appVersion: String = "",
    @SerializedName("Response")
    var response: Any? = Any(),
    @SerializedName("ResponseCollection")
    var responseCollection: List<ResponseCollection> = listOf(),
    @SerializedName("ResponseMessage")
    var responseMessage: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int = 0,
    @SerializedName("TotalRecords")
    var totalRecords: Any? = Any()
)