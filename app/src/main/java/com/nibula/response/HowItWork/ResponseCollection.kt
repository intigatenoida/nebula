package com.nibula.response.HowItWork


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("Content")
    var content: String = "",
    @SerializedName("Id")
    var id: Int = 0,
    @SerializedName("Title")
    var title: String = ""
)