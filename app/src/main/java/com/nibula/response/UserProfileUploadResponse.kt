package com.nibula.response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class UserProfileUploadResponse(
    @SerializedName("ResponseMessage")
    var responseMessage: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int = 0,
    @SerializedName("UserImage")
    var userImage: String = ""
)