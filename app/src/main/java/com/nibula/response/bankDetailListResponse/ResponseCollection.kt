package com.nibula.response.bankDetailListResponse


import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class ResponseCollection(
    @SerializedName("AccountHolderName")
    var accountHolderName: String,
    @SerializedName("AddedOn")
    var addedOn: String,
    @SerializedName("BankAccountNumber")
    var bankAccountNumber: String,
    @SerializedName("BankId")
    var bankId: Int,
    @SerializedName("BankName")
    var bankName: String,
    @SerializedName("IFSCCode")
    var iFSCCode: String,
    @SerializedName("Id")
    var id: Int,
    @SerializedName("IsDefault")
    var isDefault: Boolean,
    @SerializedName("TotalRecords")
    var totalRecords: Int,
    @SerializedName("UserId")
    var userId: String,
    @SerializedName("isShowOption")
    var isShowOption: Boolean = false,
    @SerializedName("BankIcon")
    var bankIcon: String

) : Parcelable