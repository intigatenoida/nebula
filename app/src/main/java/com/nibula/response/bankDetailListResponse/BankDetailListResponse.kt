package com.nibula.response.bankDetailListResponse


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class BankDetailListResponse(
    @SerializedName("AppVersion")
    var appVersion: String,
    @SerializedName("Response")
    var response: String,
    @SerializedName("ResponseCollection")
    var responseCollection: List<ResponseCollection>,
    @SerializedName("ResponseMessage")
    var responseMessage: String,
    @SerializedName("ResponseStatus")
    var responseStatus: Int,
    @SerializedName("TotalRecords")
    var totalRecords: Int
): Parcelable