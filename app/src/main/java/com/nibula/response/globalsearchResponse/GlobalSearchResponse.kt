package com.nibula.response.globalsearchResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

class GlobalSearchResponse(
    @SerializedName("ArtistsIncluded")
    var artistsIncluded: Boolean? = false,
    @SerializedName("FriendsIncluded")
    var friendsIncluded: Boolean? = false,
    @SerializedName("MusicIncluded")
    var musicIncluded: Boolean? = false,
    @SerializedName("Result")
    var result: ArrayList<GlobalSearchResponseItem> = arrayListOf()
)