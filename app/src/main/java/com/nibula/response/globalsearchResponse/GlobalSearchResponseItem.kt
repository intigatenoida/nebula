package com.nibula.response.globalsearchResponse


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class GlobalSearchResponseItem(
    @SerializedName("Id")
    var id: String = "",
    @SerializedName("RecordArtistName")
    var recordArtistName: String? = "",
    @SerializedName("RecordCurrencyType")
    var recordCurrencyType: String? = "",
    @SerializedName("RecordOrUserImage")
    var recordOrUserImage: String = "",
    @SerializedName("RecordOrUserName")
    var recordOrUserName: String = "",
    @SerializedName("RecordReleaseDate")
    var recordReleaseDate: String = "",
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int = 0,
    @SerializedName("Score")
    var score: Int = 0,
    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double = 0.0,
    @SerializedName("TotalInvestors")
    var totalInvestors: Int = 0,
    @SerializedName("TypeId")
    var typeId: Int = 0,
    @SerializedName("UserBio")
    var userBio: String? = "",
    @SerializedName("UserIsArtist")
    var userIsArtist: Boolean = false,
    @SerializedName("UserName")
    var userName: String? = "",


    @SerializedName("isUserSelectArtist")
    var isUserSelectArtist: Boolean? =false
):Parcelable