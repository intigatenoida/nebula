package com.nibula.response.details


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class Investments(
    @SerializedName("Investments")
    var investments: MutableList<Investment>? = null,
    @SerializedName("Investors")
    var investors: MutableList<Investor>? = null
):Parcelable