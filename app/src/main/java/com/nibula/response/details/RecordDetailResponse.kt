package com.nibula.response.details


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class RecordDetailResponse(
    @SerializedName("AppVersion")
    val appVersion: String?,
    @SerializedName("Response")
    val response: ResponseX?,
    @SerializedName("ResponseCollection")
    val responseCollection: List<Any>?,
    @SerializedName("ResponseMessage")
    val responseMessage: String?,
    @SerializedName("ResponseStatus")
    val responseStatus: Int?,
    @SerializedName("TotalRecords")
    val totalRecords: Int?
)