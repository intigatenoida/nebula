package com.nibula.response.details.investments


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class MyInvestment(
    @SerializedName("Amount")
    var amount: Double? = null,
    @SerializedName("Id")
    var id: Int? = null,
    @SerializedName("InvestedBy")
    var investedBy: String? = null,
    @SerializedName("InvestedOn")
    var investedOn: String? = null,
    @SerializedName("InvestorImage")
    var investorImage: String? = null,
    @SerializedName("InvestorName")
    var investorName: String? = null,
    @SerializedName("RecordId")
    var recordId: String? = null,
    @SerializedName("Shares")
    var shares: Int? = null,
    @SerializedName("TypeId")
    var typeId: Int? = null
)