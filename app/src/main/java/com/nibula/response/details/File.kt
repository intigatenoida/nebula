package com.nibula.response.details
import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/*
@Parcelize
@Keep
data class File(
    @SerializedName("Id")
    var id: Int? = null,
    @SerializedName("MusicDurationInSeconds")
    var musicDurationInSeconds: Int? = null,
    @SerializedName("SongTitle")
    var songTitle: String? = null,
    @SerializedName("isPlaying")
    var isPlaying: Boolean = false,
    @SerializedName("artistName")
    var artistName: String = ""
):Parcelable*/

@Parcelize
@Keep
data class File(
    @SerializedName("Id") var id: Int? = null,
    @SerializedName("MusicDurationInSeconds") var musicDurationInSeconds: Int? = null,
    @SerializedName("SongTitle") var songTitle: String? = null,
    @SerializedName("isPlaying") var isPlaying: Boolean = false,

    @SerializedName("IsFavourite") var IsFavourite: Boolean = false,
    @SerializedName("artistName") var artistName: String = "",

    @SerializedName("Artists") var artistsList: MutableList<ArtistData>?=null,

    ) : Parcelable

@Parcelize
@Keep
data class ArtistData(
    @SerializedName("ArtistId") val artistId: String?,
    @SerializedName("ArtistImage") val artistImage: String?,
    @SerializedName("ArtistName") val artistName: String?,
    @SerializedName("Verified") val verified: Boolean?,
    @SerializedName("RecordId") val recordId: String? = "",
    @SerializedName("RecordImage") val recordImage: String? = "",
    @SerializedName("RecordTitle") val recordTitle: String? = "",
) : Parcelable


/*

"Id": 639,
"SongTitle": "He Reigns",
"MusicDurationInSeconds": 174,
"IsFavourite": false,
"Artists": [
{
    "Id": 714,
    "ArtistId": "e31cc7f1-b6f0-4acf-ef90-08dac0dc8a84",
    "ArtistName": "Ashu Madhu",
    "ArtistUserName": "DJAshu",
    "ArtistImage": "https://imageresizer.nebu.la/s3/nebula-store/media/icon/UP49886aa6eae14d418bbb3dad34f19251.jpeg"
},*/
