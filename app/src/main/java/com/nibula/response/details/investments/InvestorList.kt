package com.nibula.response.details.investments


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class InvestorList(
    @SerializedName("ResponseCollection")
    var myInvestments: MutableList<MyInvestment>? = null,
    @SerializedName("TotalRecords")
    var totalRecords: Int? = null
): BaseResponse()