package com.nibula.response.details.investments


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("MyInvestments")
    var myInvestments: MutableList<MyInvestment>? = null
)