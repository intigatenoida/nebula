package com.nibula.response.details


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class Investment(
    @SerializedName("Amount")
    var amount: Double? = null,
    @SerializedName("InvestedBy")
    var investedBy: String? = null,
    @SerializedName("InvestedOn")
    var investedOn: String? = null)
:Parcelable