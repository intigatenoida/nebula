package com.nibula.response.details


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseX(
    @SerializedName("ApproveEachInvestor")
    val approveEachInvestor: Boolean?,
    @SerializedName("ArtistId")
    val artistId: String?,
    @SerializedName("ArtistImage")
    val artistImage: String?,
    @SerializedName("ArtistName")
    val artistName: String?,
    @SerializedName("CurrencyType")
    val currencyType: String?,
    @SerializedName("Files")
    val files: List<File>?,
    @SerializedName("GenreType")
    val genreType: String?,
    @SerializedName("Id")
    val id: String?,
    @SerializedName("MessageToCoOwners")
    val messageToCoOwners: String?,
    @SerializedName("MyStatusOnRecord")
    val myStatusOnRecord: String?,
    @SerializedName("MyStatusTypeOnRecord")
    val myStatusTypeOnRecord: Int?,
    @SerializedName("RecordImage")
    val recordImage: String?,
    @SerializedName("RecordStatusType")
    val recordStatusType: String?,
    @SerializedName("RecordStatusTypeId")
    val recordStatusTypeId: Int?,
    @SerializedName("RecordTitle")
    val recordTitle: String?,
    @SerializedName("RoyalityOwnershipPercentagePerToken")
    val royalityOwnershipPercentagePerToken: Double?,
    @SerializedName("SharesAvailable")
    val sharesAvailable: Double?,
    @SerializedName("SharingUrl")
    val sharingUrl: Any?,
    @SerializedName("ShowInvestorsToPublic")
    val showInvestorsToPublic: Boolean?,
    @SerializedName("TimeLeftToRelease")
    val timeLeftToRelease: Int?,
    @SerializedName("TotalPendingInvestmentRequests")
    val totalPendingInvestmentRequests: Int?,
    @SerializedName("TotalShares")
    val totalShares: Double?,
    @SerializedName("ValuePerShareInDollars")
    val valuePerShareInDollars: Double?
)