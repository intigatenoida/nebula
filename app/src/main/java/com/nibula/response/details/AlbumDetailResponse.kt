package com.nibula.response.details


import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class AlbumDetailResponse(
	@SerializedName("Response")
	var response: Response? = null
) : BaseResponse(), Parcelable