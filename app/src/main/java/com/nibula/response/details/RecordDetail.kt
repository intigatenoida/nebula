package com.nibula.response.details


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class RecordDetail(
    @SerializedName("Response")
    var response: Response? = null
): BaseResponse(),Parcelable