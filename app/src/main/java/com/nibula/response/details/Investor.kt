package com.nibula.response.details


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class Investor(
    @SerializedName("Id")
    var id: String? = null,
    @SerializedName("UserName")
    var userName: String? = null,
    @SerializedName("UserPic")
    var userPic: String? = null
):Parcelable