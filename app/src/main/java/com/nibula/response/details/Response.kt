package com.nibula.response.details


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class Response(
    @SerializedName("AmountAvailableToInvest")
    var amountAvailableToInvest: Double? = null,
    @SerializedName("ApproveEachInvestor")
    var approveEachInvestor: Int? = null,
    @SerializedName("ArtistId")
    var artistId: String? = null,
    @SerializedName("ArtistImage")
    var artistImage: String? = null,
    @SerializedName("ArtistName")
    var artistName: String? = null,
    @SerializedName("Files")
    var files: MutableList<File>? = null,
    @SerializedName("GenreType")
    var genreType: String? = null,
    @SerializedName("Id")
    var id: String? = null,
    @SerializedName("InvestmentAmountNeeded")
    var investmentAmountNeeded: Double? = null,
    @SerializedName("OfferingEndDate")
    var offeringEndDate: String? = null,
    @SerializedName("OfferingStartDate")
    var offeringStartDate: String? = null,
    @SerializedName("CurrencyType")
    var currencyType: String? = null,
    @SerializedName("PricePerRoyaltyShare")
    var pricePerRoyaltyShare: Double? = null,
    @SerializedName("RecordImage")
    var recordImage: String? = null,
    @SerializedName("RecordStatusType")
    var recordStatusType: String? = null,
    @SerializedName("RecordStatusTypeId")
    var recordStatusTypeId: Int? = null,
    @SerializedName("RecordTitle")
    var recordTitle: String? = null,
    @SerializedName("RecordType")
    var recordType: String? = null,
    @SerializedName("RecordTypeId")
    var recordTypeId: Int?,
    @SerializedName("SharesAvailable")
    var sharesAvailable: Double? = null,
    @SerializedName("ShowInvestorsToPublic")
    var showInvestorsToPublic: Int? = null,
    @SerializedName("TimeLeftToRelease")
    var timeLeftToRelease: Long? = null,
    @SerializedName("TotalInvestedAmount")
    var totalInvestedAmount: Double? = null,
    @SerializedName("TotalNoOfInvesters")
    var totalNoOfInvesters: Int? = null,

    @SerializedName("TotalShares")
    var totalNoOfShares: Int? = null,
    @SerializedName("ValuePerShareInDollars")
    var valuePerShareInDollars: Double? = null,
    @SerializedName("IsNotified")
    var isNotified: Boolean? = false,

    @SerializedName("IsFavourite")
    var isFavourite: Boolean? = false,


    @SerializedName("IsRaffle")
    var IsRaffle: Boolean? = false,


    @SerializedName("RaffleRegistrationCloseDate")
    var RaffleRegistrationCloseDate: String? = null,

    @SerializedName("RaffleStatus")
    var RaffleStatus: String? = null,

    @SerializedName("RaffleStatusId")
    var raffleStatusId: Int? = 0,


    @SerializedName("RaffleLandingPage")
    var RaffleLandingPage: String? = null,


    @SerializedName("Rewards")
    var rewards: String? = null,

    @SerializedName("RoyalityOwnershipPercentagePerToken")
    var royalityOwnershipPercentagePerToken: Double? = null,




//    @SerializedName("ShareValue")
//    var shareValue: Double? = null,
//    @SerializedName("PriceAfterRelease")
//    var priceAfterRelease: Double? = null,
//    @SerializedName("PAdvisoryType")
//    var pAdvisoryType: String? = null,
//    @SerializedName("Labels")
//    var labels: String? = null,
//    @SerializedName("Description")
//    var description: String? = null,
    @SerializedName("MessageToCoOwners") var messageToCoOwners: String? = null,
    @SerializedName("ISWCCode") var iSWCCode: String? = null,
    @SerializedName("OneOffPaymentForMusicStreamAccess") var oneOffPaymentForMusicStreamAccess: Double? = null,
    @SerializedName("ISCRCode") var iSCRCode: String? = null,
    @SerializedName("PricePerRoyaltyShareWork") var pricePerRoyaltyShareWork: Double? = null,
    @SerializedName("PricePerRoyaltyShareRecording") var pricePerRoyaltyShareRecording: Double? = null,
    @SerializedName("SocietyNameWork") var societyNameWork: String? = null,
    @SerializedName("CopyrightSocietyWork") var copyrightSocietyWork: String? = null,
    @SerializedName("SocietyNameRecording") var societyNameRecording: String? = null,
    @SerializedName("CopyrightSocietyRecording") var copyrightSocietyRecording: String? = null,
    @SerializedName("CurrencyTypeAbbreviation") var currencyTypeAbbreviation: String? = null,
    @SerializedName("SharingUrl") var sharingUrl: String? = null,

    var costBreakdownResponse: CostBreakdownResponse.Response? = null


) : Parcelable