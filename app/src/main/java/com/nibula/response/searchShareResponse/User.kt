package com.nibula.response.searchShareResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class User(
    @SerializedName("IsArtist")
    var isArtist: Boolean,
    @SerializedName("Name")
    var name: String,
    @SerializedName("UserName")
    var UserName: String,
    @SerializedName("TotalRecords")
    var totalRecords: Int,
    @SerializedName("UserId")
    var userId: String,
    @SerializedName("UserImage")
    var userImage: String,
    @SerializedName("isChecked")
    var isChecked: Boolean
)