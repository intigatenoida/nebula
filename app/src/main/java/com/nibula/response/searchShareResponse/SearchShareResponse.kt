package com.nibula.response.searchShareResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class SearchShareResponse(
    @SerializedName("ResponseMessage")
    var responseMessage: String,
    @SerializedName("ResponseStatus")
    var responseStatus: Int,
    @SerializedName("TotalRecords")
    var totalRecords: Int,
    @SerializedName("Users")
    var users: List<User>
)