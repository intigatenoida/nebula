package com.nibula.response.referalresponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("ChatId")
    var chatId: Int,
    @SerializedName("CreatedOn")
    var createdOn: String,
    @SerializedName("IsMessage")
    var isMessage: Boolean,
    @SerializedName("IsRead")
    var isRead: Boolean,
    @SerializedName("Message")
    var message: String,
    @SerializedName("PartnerId")
    var partnerId: String,
    @SerializedName("PartnerImage")
    var partnerImage: String,
    @SerializedName("PartnerName")
    var partnerName: String,
    @SerializedName("RecordId")
    var recordId: String,
    @SerializedName("RecordImage")
    var recordImage: String,
    @SerializedName("headerType")
    var headerType: Int=1,
    @SerializedName("RecordType")
    var RecordType: String,
    @SerializedName("TimePeriod")
    var timePeriod: String,
    @SerializedName("RecordTypeId")
    var RecordTypeId: Int



)