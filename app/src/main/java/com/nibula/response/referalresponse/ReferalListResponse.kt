package com.nibula.response.referalresponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ReferalListResponse(
    @SerializedName("recordDetail")
    var recordDetail: RecordDetail
)