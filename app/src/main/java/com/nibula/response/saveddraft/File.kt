package com.nibula.response.saveddraft


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class File(
    @SerializedName("Artists")
    val artists: List<Artist?>?,
    @SerializedName("Id")
    val id: Int?,
    @SerializedName("MusicDurationInSeconds")
    val musicDurationInSeconds: Int?,
    @SerializedName("SongTitle")
    val songTitle: String?
):Parcelable