package com.nibula.response.saveddraft


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class Response(
    @SerializedName("AmountAvailableToInvest")
    val amountAvailableToInvest: Double?,
    @SerializedName("ApproveEachInvestor")
    val approveEachInvestor: Int?,
    @SerializedName("ApprovedRefferalCode")
    val approvedRefferalCode: String?,
    @SerializedName("ArtistId")
    val artistId: String?,
    @SerializedName("ArtistImage")
    val artistImage: String?,
    @SerializedName("ArtistName")
    val artistName: String?,
    @SerializedName("ArtistOwnershipPercentage")
    val artistOwnershipPercentage: Double?,
    @SerializedName("AutoApproveInvestments")
    val autoApproveInvestments: Boolean?,
    @SerializedName("CopyrightSocietyRecording")
    val copyrightSocietyRecording: Int?,

    @SerializedName("CopyrightSocietyWork")
    val copyrightSocietyWork: Int?,
    @SerializedName("CurrencyType")
    val currencyType: String?,
    @SerializedName("CurrencyTypeAbbreviation")
    val currencyTypeAbbreviation: String?,
    @SerializedName("Files")
    val files: List<File?>?,
    @SerializedName("GenreType")
    val genreType: String?,

    @SerializedName("GenreTypeId")
    val genreTypeId: String?,

    @SerializedName("ISCRCode")
    val iSCRCode: String?,
    @SerializedName("ISWCCode")
    val iSWCCode: String?,
    @SerializedName("Id")
    val id: String?,
    @SerializedName("InvestmentAmountNeeded")
    val investmentAmountNeeded: Double?,
    @SerializedName("IsNotified")
    val isNotified: Boolean?,

    @SerializedName("IsSongPreviouslyReleased")
    val isSongPreviouslyReleased: Boolean?,

    @SerializedName("IsNotifiedMessageStatus")
    val isNotifiedMessageStatus: String?,
    @SerializedName("MessageToCoOwners")
    val messageToCoOwners: String?,
    @SerializedName("OfferingEndDate")
    val offeringEndDate: String?,
    @SerializedName("OfferingStartDate")
    val offeringStartDate: String?,
    @SerializedName("OneOffPaymentForMusicStreamAccess")
    val oneOffPaymentForMusicStreamAccess: Double?,
    @SerializedName("PercentageToSell")
    val percentageToSell: Double?,
    @SerializedName("PricePerRoyaltyShare")
    val pricePerRoyaltyShare: Double?,
    @SerializedName("PricePerRoyaltyShareRecording")
    val pricePerRoyaltyShareRecording: Double?,
    @SerializedName("PricePerRoyaltyShareWork")
    val pricePerRoyaltyShareWork: Double?,
    @SerializedName("RecordImage")
    val recordImage: String?,
    @SerializedName("RecordStatusType")
    val recordStatusType: String?,
    @SerializedName("RecordStatusTypeId")
    val recordStatusTypeId: Int?,
    @SerializedName("RecordTitle")
    val recordTitle: String?,
    @SerializedName("RecordType")
    val recordType: String?,
    @SerializedName("RecordTypeId")
    val recordTypeId: Int?,
    @SerializedName("RoyalityOwnershipPercentagePerToken")
    val royalityOwnershipPercentagePerToken: Double?,
    @SerializedName("SharesAvailable")
    val sharesAvailable: Double?,
    @SerializedName("SharingUrl")
    val sharingUrl: String?,
    @SerializedName("CountryName")
    val countryName: String?,

    @SerializedName("ShowInvestorsToPublic")
    val showInvestorsToPublic: Int?,
    @SerializedName("SocietyNameRecording")
    val societyNameRecording: String?,
    @SerializedName("SocietyNameWork")
    val societyNameWork: String?,
    @SerializedName("StepNumber")
    val stepNumber: Int?,
    @SerializedName("TimeLeftToRelease")
    val timeLeftToRelease: Int?,
    @SerializedName("TotalInvestedAmount")
    val totalInvestedAmount: Double?,
    @SerializedName("TotalNoOfInvesters")
    val totalNoOfInvesters: Int?,
    @SerializedName("TotalShares")
    val totalShares: Double?,
    @SerializedName("TotalSoldTokens")
    val totalSoldTokens: Int?,
    @SerializedName("ValuePerShareInDollars")
    val valuePerShareInDollars: Double?
):Parcelable