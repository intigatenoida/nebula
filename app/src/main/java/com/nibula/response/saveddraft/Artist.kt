package com.nibula.response.saveddraft


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class Artist(
    @SerializedName("ArtistId")
    val artistId: String?,
    @SerializedName("ArtistImage")
    val artistImage: String?,
    @SerializedName("ArtistName")
    val artistName: String?,
    @SerializedName("Id")
    val id: Int?
):Parcelable