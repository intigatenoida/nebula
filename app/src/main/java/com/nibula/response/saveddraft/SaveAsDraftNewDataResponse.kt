package com.nibula.response.saveddraft


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class SaveAsDraftNewDataResponse(
    @SerializedName("AppVersion")
    val appVersion: String?,
    @SerializedName("Response")
    val response: Response?,
    @SerializedName("ResponseMessage")
    val responseMessage: String?,
    @SerializedName("ResponseStatus")
    val responseStatus: Int?,
    @SerializedName("TotalRecords")
    val totalRecords: Int?
) : Parcelable