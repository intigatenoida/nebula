package com.nibula.response.saveddraft

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class SavedDraftRecordResponse(
	
	@SerializedName("ResponseCollection")
	var dataList: MutableList<SavedDraftRecordData>,
	@SerializedName("TotalRecords")
	var totalRecords: Int? = null

) : BaseResponse()