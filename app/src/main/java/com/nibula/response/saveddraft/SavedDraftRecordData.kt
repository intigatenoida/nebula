package com.nibula.response.saveddraft

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class SavedDraftRecordData(
    @SerializedName("AmountAvailableToInvest")
    val amountAvailableToInvest: Double?,
    @SerializedName("ApproveEachInvestor")
    val approveEachInvestor: Boolean?,
    @SerializedName("ArtistsId")
    val artistsId: String?,
    @SerializedName("ArtistsName")
    val artistsName: String?,
    @SerializedName("CreatedOn")
    val createdOn: String?,
    @SerializedName("CurrencyType")
    val currencyType: String?,
    @SerializedName("Description")
    val description: String?,
    @SerializedName("GenreType")
    val genreType: String?,
    @SerializedName("GenreTypeIds")
    val genreTypeIds: String?,
    @SerializedName("Id")
    val id: String?,
    @SerializedName("InvestmentAmountNeeded")
    val investmentAmountNeeded: Double?,
    @SerializedName("Labels")
    val labels: String?,
    @SerializedName("OfferingEndDate")
    val offeringEndDate: String?,
    @SerializedName("OfferingStartDate")
    val offeringStartDate: String?,
    @SerializedName("PAdvisoryType")
    val pAdvisoryType: String?,
    @SerializedName("PAdvisoryTypeId")
    val pAdvisoryTypeId: Int?,
    @SerializedName("PerformanceCopyrightNo")
    val performanceCopyrightNo: String?,
    @SerializedName("PriceAfterRelease")
    val priceAfterRelease: Double?,
    @SerializedName("PricePerRoyaltyShare")
    val pricePerRoyaltyShare: Double?,
    @SerializedName("ShowInvestorsToPublic")
    val showInvestorsToPublic:Boolean?,
    @SerializedName("RecordImage")
    val recordImage: String?,
    @SerializedName("RecordStatusType")
    val recordStatusType: String?,
    @SerializedName("RecordStatusTypeId")
    val recordStatusTypeId: Int?,
    @SerializedName("RecordTitle")
    val recordTitle: String?,
    @SerializedName("RecordType")
    val recordType: String?,
    @SerializedName("RecordTypeId")
    val recordTypeId: Int?,
    @SerializedName("RecordingCopyrightNo")
    val recordingCopyrightNo: String?,
    @SerializedName("ReleaseDate")
    val releaseDate: String?,
    @SerializedName("ScreenType")
    val screenType: Int?,
    @SerializedName("SharesAvailable")
    val sharesAvailable: Int?,
    @SerializedName("TimeLeftToRelease")
    val timeLeftToRelease: Long?,
    @SerializedName("TotalInvestedAmount")
    val totalInvestedAmount: Double?,
    @SerializedName("TotalNoOfInvesters")
    val totalNoOfInvesters: Int?,
    @SerializedName("TotalRecords")
    val totalRecords: Int?,
    @SerializedName("ValuePerShareInDollars")
    val valuePerShareInDollars: Double?,
    @SerializedName("PreviewDurationInSec")
    val previewDurationInSec: Int?,
    @SerializedName("CurrencyTypeId")
    val currencyTypeId: Int?,
    @SerializedName("CopyrightSocietyRecording")
    var copyrightSocietyRecording: Int? ,
    @SerializedName("CopyrightSocietyWork")
    var copyrightSocietyWork:  Int? ,
    @SerializedName("ISCRCode")
    var iSCRCode: String? = "",
    @SerializedName("ISWCCode")
    var iSWCCode: String? = "",
    @SerializedName("MessageToCoOwners")
    var messageToCoOwners: String? = "",
    @SerializedName("OneOffPaymentForMusicStreamAccess")
    var oneOffPaymentForMusicStreamAccess: Double? = 0.0,
    @SerializedName("PricePerRoyaltyShareRecording")
    var pricePerRoyaltyShareRecording:  Double? = 0.0,
    @SerializedName("PricePerRoyaltyShareWork")
    var pricePerRoyaltyShareWork: Double? = 0.0,
    @SerializedName("SocietyNameRecording")
    var societyNameRecording:String? = "",
    @SerializedName("SocietyNameWork")
    var societyNameWork: String? = "",
    @SerializedName("ValueOfAShare")
    var valueOfAShare: Double? = 0.0
): Parcelable