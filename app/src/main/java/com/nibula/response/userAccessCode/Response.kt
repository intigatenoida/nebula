package com.nibula.response.userAccessCode


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("AccessCode")
    var accessCode: Int = 0
)