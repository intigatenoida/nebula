package com.nibula.response.userAccessCode


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class UserAccessCodeResponse(
    @SerializedName("AppVersion")
    var appVersion: String = "",
    @SerializedName("Response")
    var response: Response = Response(),
    @SerializedName("ResponseMessage")
    var responseMessage: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int = 0
)