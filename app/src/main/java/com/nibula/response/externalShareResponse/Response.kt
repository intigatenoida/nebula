package com.nibula.response.externalShareResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("Url")
    var url: String
)