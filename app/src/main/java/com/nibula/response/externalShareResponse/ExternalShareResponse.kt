package com.nibula.response.externalShareResponse


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ExternalShareResponse(
    @SerializedName("AppVersion")
    var appVersion: String,
    @SerializedName("Response")
    var response: Response,
    @SerializedName("ResponseCollection")
    var responseCollection: List<Any>,
    @SerializedName("ResponseMessage")
    var responseMessage: String,
    @SerializedName("ResponseStatus")
    var responseStatus: Int,
    @SerializedName("TotalRecords")
    var totalRecords: Any
)