package com.nibula.response.notifiaction


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ResponseCollection(
    @SerializedName("DependentRowId")
    var dependentRowId: String,
    @SerializedName("Description")
    var description: String,
    @SerializedName("Id")
    var id: Int,
    @SerializedName("IsRead")
    var isRead: Boolean,
    @SerializedName("NotificationDate")
    var notificationDate: String,
    @SerializedName("NotificationType")
    var notificationType: String,
    @SerializedName("TimePeriod")
    var timePeriod: String,
    @SerializedName("TotalRecords")
    var totalRecords: Int,
    @SerializedName("viewType")
    var viewType: Int,
    @SerializedName("headerData")
    var headerDate: String,
    @SerializedName("NotificationTypeId")
    var notificationTypeId : Int


)