package com.nibula.response.cryptowalletresponse
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class SaveCryptoWalletResponse(
    @SerializedName("ResponseMessage")
    var responseMessage: String,
    @SerializedName("ResponseStatus")
    var responseStatus: Int,
    @SerializedName("Id")
    var Id: String,
    @SerializedName("custom")
    var custom: String
)

