package com.nibula

import android.annotation.TargetApi
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.webkit.*
import androidx.appcompat.app.AlertDialog
import com.nibula.base.BaseActivity
import com.nibula.databinding.TermsConditionActivityBinding
import com.nibula.utils.AppConstant


class TermsConditionActivity : BaseActivity() {
    private lateinit var termsConditionActivityBinding: TermsConditionActivityBinding
    var url = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.terms_condition_activity)
        termsConditionActivityBinding = TermsConditionActivityBinding.inflate(layoutInflater)
        if (intent.getIntExtra("VALUE", 0) == 1) {
            url = AppConstant.TERMS_CONDITION
        } else {
            termsConditionActivityBinding.headerLayout.titleTextView.text =
                getString(R.string.privacy_policy)
            //termsConditionActivityBinding.titleTextView.text = getString(R.string.privacy_policy)
            url = AppConstant.PRIVACY_POLICY

        }
        termsConditionActivityBinding.headerLayout.ivBack.setOnClickListener { finish() }
//        if (savedInstanceState == null) {
//            supportFragmentManager.beginTransaction()
//                .replace(R.id.container, TermsConditionFragment.newInstance())
//                .commitNow()
//        }
        setWebView()
    }


    private fun setWebView() {
        showProcessDialog()
        termsConditionActivityBinding.webView.webViewClient = ServiceWebViewClient()
        val settings = termsConditionActivityBinding.webView.settings
        settings.domStorageEnabled = true
        termsConditionActivityBinding.webView.settings.javaScriptEnabled = true
        termsConditionActivityBinding.webView.settings.javaScriptCanOpenWindowsAutomatically = true
        termsConditionActivityBinding.webView.isHorizontalScrollBarEnabled = false
        termsConditionActivityBinding.webView.loadUrl(
            url

        )
    }

    private inner class ServiceWebViewClient : WebViewClient() {
        override fun onLoadResource(view: WebView, url: String) {
            super.onLoadResource(view, url)
            if (view.progress > 30) {
                hideProcessDailog()
            }
        }

        @Deprecated("Deprecated in Java")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            if (url!!.startsWith("http:") || url.startsWith("https:")) {
                return false
            }

            // Otherwise allow the OS to handle it
            // Otherwise allow the OS to handle it
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            view!!.context.startActivity(intent)
            return true
        }


        @TargetApi(Build.VERSION_CODES.M)
        override fun onReceivedError(
            view: WebView,
            request: WebResourceRequest,
            error: WebResourceError
        ) {
            super.onReceivedError(view, request, error)
            Log.e("*********", "******************************${error.description.toString()}")
            hideProcessDailog()

        }

        override fun onReceivedHttpError(
            view: WebView,
            request: WebResourceRequest,
            errorResponse: WebResourceResponse
        ) {
            super.onReceivedHttpError(view, request, errorResponse)
            Log.i("*********", errorResponse.toString())
            hideProcessDailog()
        }

        override fun onReceivedSslError(
            view: WebView?,
            handler: SslErrorHandler?,
            error: SslError?
        ) {
            var message = "SSL Certificate error."
            when (error?.primaryError ?: -1) {
                SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                SslError.SSL_EXPIRED -> message = "The certificate has expired."
                SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
            }
            message += " Do you want to continue anyway?"
            val builder: AlertDialog.Builder = AlertDialog.Builder(this@TermsConditionActivity)
            builder.setMessage(message)
            builder.setPositiveButton("continue",
                DialogInterface.OnClickListener { _, _ -> handler!!.proceed() })
            builder.setNegativeButton("cancel",
                DialogInterface.OnClickListener { _, _ -> handler!!.cancel() })
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }
}
