package com.nibula.addBank.helper

import com.nibula.R
import com.nibula.addBank.fragment.BankListFragment
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.request.BankMakeDefaultRequest
import com.nibula.response.BaseResponse
import com.nibula.response.bankDetailListResponse.BankDetailListResponse
import com.nibula.response.bankDetailListResponse.ResponseCollection
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError

class BankListHelper(val fg: BankListFragment) : BaseHelperFragment() {

    var nfNextPage: Int = 1
    var nfMaxPage: Int = 0
    fun getBankList() {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }


        fg.showProcessDialog()
        fg.isLoading = true

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getBankList(nfNextPage)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BankDetailListResponse>() {
            override fun onSuccess(any: BankDetailListResponse?, message: String?) {
                fg.hideProcessDialog()
                fg.isLoading = false
                nfMaxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = nfNextPage < nfMaxPage ||
                        (any?.totalRecords ?: 0) > (nfNextPage * 10)
                val respone = any as BankDetailListResponse
                respone?.responseCollection?.let {
                    if (nfNextPage == 1) {
                        fg.adapter.clearBankList()
                    }
                    fg.adapter.addData(it as ArrayList<ResponseCollection>)
                    nfNextPage++
                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
                fg.isLoading = false
                fg.isDataAvailable = false
            }

        }))
    }

    fun makeBankDefault(id: Int) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }


        fg.showProcessDialog()
        fg.isLoading = true

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val request = BankMakeDefaultRequest(id)
        val observable = helper.updateBankDefault(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String?) {
                val response = any as BaseResponse
                if (response.responseStatus ?: 0 == 1) {
                    fg.hideProcessDialog()
                    fg.adapter.clearBankList()
                    nfNextPage = 1
                    getBankList()
                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }
        }))
    }

    fun deleteBankAccount(id: Int, position: Int) {

        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }

        fg.showProcessDialog()
        fg.isLoading = true

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.deleteUserAccounts(id)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String?) {
                val response = any as BaseResponse
                if (response.responseStatus ?: 0 == 1) {
                    fg.hideProcessDialog()
                    fg.adapter.removeAccount(position)

                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }
        }))
    }
}