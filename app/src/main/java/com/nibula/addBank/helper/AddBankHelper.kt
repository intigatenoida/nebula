package com.nibula.addBank.helper

import com.nibula.R
import com.nibula.addBank.fragment.AddBankFragment
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.request.AddUpdasteBankRequest
import com.nibula.response.SaveBankResponse
import com.nibula.response.bankList.BankListResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.utils.PrefUtils

class AddBankHelper(val fg: AddBankFragment) : BaseHelperFragment() {

    //to save the bank detail
    fun saveBank() {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideKeyboard()
            return
        }
        fg.showProcessDialog()
        val request = AddUpdasteBankRequest(
            fg.fragmentAddBankBinding.edtAccpuntHolderName.text.toString().trim(),
            fg.fragmentAddBankBinding.edtAccountNumber.text.toString().trim(),
            fg.selectedBankId,
            fg.fragmentAddBankBinding.edtIfscCode.text.toString().trim(),
            fg.bankId)

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.saveUpdateAccountNumber(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<SaveBankResponse>() {

            override fun onSuccess(any: SaveBankResponse?, message: String?) {
                fg.hideProcessDialog()
                if (any?.responseStatus ?: 0 == 1) {
                    CustomToast.showToast(
                        fg.requireContext(),
                        any?.responseMessage ?: "",
                        CustomToast.ToastType.SUCCESS
                    )
                    fg.navigate.manualBack()
                    if (fg.fromBankList) {
                        PrefUtils.saveValueInPreference(
                            fg.requireContext(),
                            PrefUtils.BANKLIST_REFRESH,
                            true
                        )
                    } else {
                        PrefUtils.saveValueInPreference(
                            fg.requireContext(),
                            PrefUtils.BANKLIST_REFRESH,
                            false
                        )
                    }
                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }

        }))

    }

    // to get the bank list
    fun getBankList() {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideKeyboard()
            return
        }

        fg.showProcessDialog()

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getBankList()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BankListResponse>() {

            override fun onSuccess(any: BankListResponse?, message: String?) {
                fg.hideProcessDialog()
                any?.responseCollection?.let { fg.updateBankListSpinner(it) }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()

            }

        }))

    }


}