package com.nibula.addBank.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.addBank.adapter.BankListAdapter
import com.nibula.addBank.helper.BankListHelper
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.FragmentBankListBinding
import com.nibula.response.bankDetailListResponse.ResponseCollection
import com.nibula.utils.AppConstant


class BankListFragment : BaseFragment(), BankListAdapter.onBankSelectListener {

    lateinit var rootView: View
    var bankList = ArrayList<ResponseCollection>()
    lateinit var adapter: BankListAdapter
    var isLoading = false
    var isDataAvailable = true

    companion object {
        fun getInstance() = BankListFragment()
    }

    lateinit var helper: BankListHelper
private lateinit var fragmetnBankListBinding: FragmentBankListBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //rootView = inflater.inflate(R.layout.fragment_bank_list, container, false)
        fragmetnBankListBinding=FragmentBankListBinding.inflate(layoutInflater)
        fragmetnBankListBinding.txtAddBank.setOnClickListener {
            navigate.addBank(Bundle().apply {
                putBoolean(AppConstant.FROM_BANK_LIST, true)
                putBoolean(AppConstant.ADD_BANK, true)
            })
        }
        //fragmetnBankListBinding.tv_title.text = getString(R.string.select_bank_account)
        fragmetnBankListBinding.layoutTbMain.tvTitle.text=getString(R.string.select_bank_account)
        initHelper()
        initUI()

        return rootView
    }

    private fun initHelper() {
        helper = BankListHelper(this)
    }

    override fun onResume() {
        super.onResume()
       /* if (PrefUtils.getBooleanValue(requireContext(), PrefUtils.BANKLIST_REFRESH)) {
            PrefUtils.saveValueInPreference(
                requireContext(),
                PrefUtils.BANKLIST_REFRESH,
                false
            )
            adapter.clearBankList()
            helper.nfNextPage = 1
            helper.getBankList()
        }*/
    }

    private fun initUI() {



        fragmetnBankListBinding.layoutTbMain.imgHome.setOnClickListener {
            navigate.manualBack()

        }
        val selectedId = arguments?.getInt(AppConstant.BANK_DETAIL) ?: -1
        adapter = BankListAdapter(requireContext(), selectedId, this)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        fragmetnBankListBinding.bankList.layoutManager = layoutManager
        fragmetnBankListBinding.bankList.adapter = adapter
        adapter.addData(bankList)

        fragmetnBankListBinding.bankList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.getBankList()
                }
            }
        })
        adapter.clearBankList()
        helper.nfNextPage = 1
        helper.getBankList()

        initTopHeader()
    }

    private fun initTopHeader() {

        fragmetnBankListBinding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        fragmetnBankListBinding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        fragmetnBankListBinding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }
    }

    override fun onEditBank(bankData: ResponseCollection) {
        val bundle = Bundle().apply {
            putInt(AppConstant.BANK_ID, bankData.bankId)
            putInt(AppConstant.ID, bankData.id)
            putString(AppConstant.BANK_ACCOUNT_NAME, bankData.accountHolderName)
            putString(AppConstant.BANK_ACCOUNT_NUMBER, bankData.bankAccountNumber)
            putString(AppConstant.BANK_IFSC, bankData.iFSCCode)
            putBoolean(AppConstant.FROM_BANK_LIST, true)
            putBoolean(AppConstant.ADD_BANK, false)
        }
        navigate.addBank(bundle)
    }

    override fun onSetDefault(bankData: ResponseCollection) {
        if (bankData.isDefault) {
            CustomToast.showToast(
                requireContext(),
                "This bank account is already default",
                CustomToast.ToastType.FAILED
            )
            return
        }
        helper.makeBankDefault(bankData.id)
    }

    override fun onDeleteAccount(bankData: ResponseCollection, position: Int) {
        helper.deleteBankAccount(bankData.id,position)
    }

    override fun goback() {
        navigate.manualBack()
    }


}