package com.nibula.addBank.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.addBank.helper.AddBankHelper
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.FragmentAddBankBinding
import com.nibula.response.bankList.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.SpinnerAdapterBank


class AddBankFragment : BaseFragment() {

    companion object {
        fun newInstance() = AddBankFragment()
    }

    var selectedBankId = -1
    lateinit var helper: AddBankHelper
    lateinit var rootView: View
    private var bankIdForSpinner = -1
    var bankId = 0
    var fromBankList = false
    var addNewBank = false
     lateinit var fragmentAddBankBinding: FragmentAddBankBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        // rootView = inflater.inflate(R.layout.fragment_add_bank, container, false)
        fragmentAddBankBinding = FragmentAddBankBinding.inflate(layoutInflater)
        initUI()
        return rootView
    }

    private fun initUI() {
        fragmentAddBankBinding.addNewAccount.setOnClickListener {
            if (validateData()) {
                helper.saveBank()
            }
        }
        fragmentAddBankBinding.cancel.setOnClickListener {

            navigate.manualBack()
        }

        initTopHeader()
        arguments?.let {
            addNewBank = it.getBoolean(AppConstant.ADD_BANK, false)
            fragmentAddBankBinding.edtIfscCode.setText(it.getString(AppConstant.BANK_IFSC) ?: "")
            fragmentAddBankBinding.edtAccpuntHolderName.setText(
                it.getString(AppConstant.BANK_ACCOUNT_NAME) ?: ""
            )
            fragmentAddBankBinding.edtAccountNumber.setText(
                it.getString(AppConstant.BANK_ACCOUNT_NUMBER) ?: ""
            )
            bankIdForSpinner = it.getInt(AppConstant.BANK_ID, -1) ?: -1
            bankId = it.getInt(AppConstant.ID) ?: 0
            fromBankList = it.getBoolean(AppConstant.FROM_BANK_LIST)
        }
        if (fragmentAddBankBinding.edtIfscCode.text.toString().isEmpty()) {
            //fragmentAddBankBinding.layoutTbMain.tv_title.text = getString(R.string.add_bank)

            fragmentAddBankBinding.layoutTbMain.tvTitle.text = getString(R.string.add_bank)
            fragmentAddBankBinding.addNewAccount.text = getString(R.string.add_bank_details)
        } else {
            fragmentAddBankBinding.layoutTbMain.tvTitle.text = getString(R.string.edit_bank)
            fragmentAddBankBinding.addNewAccount.text = getString(R.string.update_bank_detail)
        }

        helper = AddBankHelper(this)
        helper.getBankList()
    }

    private fun validateData(): Boolean {
        if (fragmentAddBankBinding.edtAccountNumber.text.toString().trim().isEmpty()) {
            CustomToast.showToast(
                requireContext(),
                getString(R.string.account_number_empty),
                CustomToast.ToastType.FAILED
            )
            return false
        }

        if (selectedBankId == -1) {
            CustomToast.showToast(
                requireContext(),
                getString(R.string.bank_name_empty),
                CustomToast.ToastType.FAILED
            )
            return false
        }
        if (fragmentAddBankBinding.edtAccpuntHolderName.text.toString().trim().isEmpty()) {
            CustomToast.showToast(
                requireContext(),
                getString(R.string.account_holder_name_empty),
                CustomToast.ToastType.FAILED
            )
            return false
        }
        if (fragmentAddBankBinding.edtIfscCode.text.toString().trim().isEmpty()) {
            CustomToast.showToast(
                requireContext(),
                getString(R.string.swift_code_empty),
                CustomToast.ToastType.FAILED
            )
            return false
        }

        return true
    }

    private fun initTopHeader() {

        fragmentAddBankBinding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        fragmentAddBankBinding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        fragmentAddBankBinding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        /*val header = requireActivity().findViewById<View>(R.id.header)
        header.visibility = GONE*/

        fragmentAddBankBinding.header.imgSearch.setOnClickListener {
            navigate.manualBack()
            // requireActivity().supportFragmentManager.popBackStack()
        }
    }

    override fun onResume() {
        super.onResume()
        fragmentAddBankBinding.header.ivNotification.setColorFilter(
            ContextCompat.getColor(
                requireContext(),
                R.color.violet_shade_100per
            )
        )
        CommonUtils.setNotificationDot(
            requireContext(),
            fragmentAddBankBinding.header.imgNotificationCount
        )
    }

    fun updateBankListSpinner(responseCollection: List<ResponseCollection>) {
        val spinnerData = mutableListOf<String>()
        spinnerData.add(getString(R.string.bank_name))

        var selectedIndex = -1
        for (i in 0.until(responseCollection.size)) {
            val bankData = responseCollection[i]
            if (bankIdForSpinner != -1 && bankIdForSpinner == bankData.bankId && !addNewBank) {
                selectedIndex = i
            }
            spinnerData.add(bankData.bankName)
        }
        val bankAdapter = SpinnerAdapterBank(requireActivity(), spinnerData)
        fragmentAddBankBinding.spnGenre.adapter = bankAdapter
        if (selectedIndex != -1) {
            fragmentAddBankBinding.spnGenre.setSelection(selectedIndex + 1)
        }
        fragmentAddBankBinding.spnGenre.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position == 0) return
                    selectedBankId = responseCollection[position - 1].bankId
                }

            }
    }

}