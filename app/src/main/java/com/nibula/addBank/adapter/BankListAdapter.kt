package com.nibula.addBank.adapter

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.FrameLayout
import android.widget.ListAdapter
import androidx.appcompat.widget.ListPopupWindow
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemBankListBinding
import com.nibula.response.bankDetailListResponse.ResponseCollection
import com.nibula.user.profile.adapters.BankOptionAdapter
import com.nibula.utils.PrefUtils


class BankListAdapter(
    val context: Context,
    private val selectedId: Int,
    val listener: BankListAdapter.onBankSelectListener
) :
    RecyclerView.Adapter<BankListAdapter.ViewHolder>() {

    var lastVisiblePosition = -1
    var bankList = ArrayList<ResponseCollection>()
    private lateinit var itemBankOptionBinding: ItemBankListBinding

    class ViewHolder(item: View) : RecyclerView.ViewHolder(item)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_bank_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return bankList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bankData = bankList[position]
        itemBankOptionBinding.bankName.text = bankData.bankName
        itemBankOptionBinding.accountNumber.text =
            "${context.getString(R.string.account_number)} : ${bankData.bankAccountNumber}"
        itemBankOptionBinding.ifsc.text =
            "${context.getString(R.string.swift_code)} : ${bankData.iFSCCode}"
        itemBankOptionBinding.accountHolderName.text =
            "${context.getString(R.string.account_holder_name)} : ${bankData.accountHolderName}"

        itemBankOptionBinding.chkBank.setOnCheckedChangeListener(null)
        itemBankOptionBinding.chkBank.isChecked = bankData.isDefault
        itemBankOptionBinding.imgOption.setOnClickListener {
            showPopUpMenu(itemBankOptionBinding.imgOption, bankData,position)
        }
        itemBankOptionBinding.txtDefault.visibility = if (bankData.isDefault)
            View.VISIBLE
        else
            View.GONE
        itemBankOptionBinding.chkBank.setOnCheckedChangeListener { compoundButton: CompoundButton, isChecked: Boolean ->
            if (isChecked) {
                PrefUtils.saveValueInPreference(context, PrefUtils.SELECTED_BANK_ID, bankData.id)
                listener.goback()
            } else {
                PrefUtils.saveValueInPreference(context, PrefUtils.SELECTED_BANK_ID, 0)
            }

        }
    }

    fun addData(bankList: ArrayList<ResponseCollection>) {
        this.bankList.addAll(bankList)
        notifyDataSetChanged()
    }

    private fun showPopUpMenu(
        view: View,
        bankData: ResponseCollection,itemPostion:Int
    ) {

        val listPopUp = ListPopupWindow(context)
        listPopUp.setBackgroundDrawable(
            ContextCompat.getDrawable(
                context,
                R.drawable.bank_option_drawable
            )
        )
        val data = ArrayList<String>()

        data.add(context.getString(R.string.edit))
        if (!bankData.isDefault) {
            data.add(context.getString(R.string.set_default))
            data.add(context.getString(R.string.remove))
        }

        val listPopupWindowAdapter =
            BankOptionAdapter(context, R.layout.item_bank_option, data)
        listPopUp.anchorView = view
        listPopUp.setAdapter(listPopupWindowAdapter)
        listPopUp.setContentWidth(measureContentWidth(listPopupWindowAdapter))
        listPopUp.setDropDownGravity(Gravity.END)
        listPopUp.horizontalOffset =
            -view.width
        listPopUp.verticalOffset =
            -/*context.resources.getDimension(R.dimen.dimen_10dp).toInt()*/view.height / 2
        listPopUp.setOnItemClickListener { parent, view, position, id ->
            when (data[position]) {
                context.getString(R.string.set_default) -> {
                    listener.onSetDefault(bankData)
                    listPopUp.dismiss()
                }
                context.getString(R.string.edit) -> {
                    listener.onEditBank(bankData)
                    listPopUp.dismiss()
                }
                context.getString(R.string.remove) -> {
                    listener.onDeleteAccount(bankData, itemPostion)
                    listPopUp.dismiss()
                }
            }
        }
        listPopUp.show()
    }

    interface onBankSelectListener {
        fun onEditBank(bankData: ResponseCollection)
        fun onSetDefault(bankData: ResponseCollection)
        fun onDeleteAccount(bankData: ResponseCollection, position: Int)
        fun goback()
    }


    private fun measureContentWidth(listAdapter: ListAdapter): Int {
        var mMeasureParent: ViewGroup? = null
        var maxWidth = 0
        var itemView: View? = null
        var itemType = 0
        val adapter: ListAdapter = listAdapter
        val widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        val heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        val count: Int = adapter.count
        for (i in 0 until count) {
            val positionType: Int = adapter.getItemViewType(i)
            if (positionType != itemType) {
                itemType = positionType
                itemView = null
            }
            if (mMeasureParent == null) {
                mMeasureParent = FrameLayout(context)
            }
            itemView = adapter.getView(i, itemView, mMeasureParent)
            itemView.measure(widthMeasureSpec, heightMeasureSpec)
            val itemWidth = itemView.measuredWidth
            if (itemWidth > maxWidth) {
                maxWidth = itemWidth
            }
        }
        return maxWidth
    }


    fun clearBankList() {
        bankList.clear()
        notifyDataSetChanged()
    }

    fun removeAccount(position: Int) {
        bankList.removeAt(position)
        notifyItemRemoved(position)
    }


}