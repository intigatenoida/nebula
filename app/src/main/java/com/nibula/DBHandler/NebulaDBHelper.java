package com.nibula.DBHandler;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.nibula.response.globalsearchResponse.GlobalSearchResponseItem;
import java.util.ArrayList;
public class NebulaDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "nebula";
    private static final String TABLE_PLAT_LIST = "PlayList";
    private static final String TABLE_RECENT_SEARCH = "RECENT_SEARCH";
    private static final String KEY_RECORD_ID = "Id";
    private static final String KEY_ARTIST_NAME = "artistName";
    private static final String KEY_ARTIST_IMAGE = "artistImage";
    private static final String KEY_ARTIST_USER_NAME = "artishUserName";
    private static final String KEY_ARTIST_ID = "artistId";
    private static final String KEY_RECORD_TITLE = "recordTitle";
    private static final String KEY_START_WITH = "startWith";
    private static final String KEY_RECORD_IMAGE = "recordImage";
    private static final String KEY_RECORD_TYPE_ID = "recordTypeId";
    private static final String KEY_FILE_ID = "fileId";
    private static final String KEY_RECORD_TYPE = "recordType";
    private static final String KEY_RecordCurrencyType = "RecordCurrencyType";
    private static final String KEY_RecordReleaseDate = "RecordReleaseDate";
    private static final String KEY_RecordStatusTypeId = "RecordStatusTypeId";
    private static final String KEY_Score = "Score";
    private static final String KEY_TotalInvestedAmount = "TotalInvestedAmount";
    private static final String KEY_TotalInvestors = "TotalInvestors";
    private static final String KEY_UserBio = "UserBio";
    private static final String KEY_UserIsArtist = "UserIsArtist";
    private static final String KEY_UserName = "UserName";
    public NebulaDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_RECENT_SEARCH_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_RECENT_SEARCH + " ( "
                + KEY_ARTIST_ID + " TEXT  NOT NULL , "
                + KEY_ARTIST_NAME + " TEXT  NOT NULL , "
                + KEY_UserName + " TEXT NOT NULL  ,"
                + KEY_RECORD_IMAGE + " TEXT NOT NULL  "
                /*+ KEY_RecordCurrencyType + " TEXT , "
                + KEY_RECORD_IMAGE + " TEXT , "
                + KEY_RECORD_TITLE + " TEXT , "
                + KEY_RecordReleaseDate + " TEXT , "
                + KEY_RecordStatusTypeId + " INTEGER , "
                + KEY_Score + " INTEGER , "
                + KEY_TotalInvestedAmount + " DOUBLE NOT NULL default 0.0 , "
                + KEY_TotalInvestors + " INTEGER , "
                + KEY_RECORD_TYPE_ID + " INTEGER , "
                + KEY_UserBio + " TEXT  , "
                + KEY_UserIsArtist + " INTEGER NOT NULL default 0 , "
                + KEY_UserName + " TEXT  "*/
                + ")";
        String CREATE_PLAYLIST_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_PLAT_LIST + " ( "
                + KEY_RECORD_ID + " TEXT  NOT NULL , "
                + KEY_ARTIST_ID + " TEXT NOT NULL , "
                + KEY_ARTIST_NAME + " TEXT NOT NULL , "
                + KEY_START_WITH + " TEXT NOT NULL , "
                + KEY_RECORD_TITLE + " TEXT NOT NULL , "
                + KEY_RECORD_IMAGE + " TEXT NOT NULL, "
                + KEY_RECORD_TYPE_ID + " INTEGER NOT NULL, "
                + KEY_RECORD_TYPE + " TEXT NOT NULL, "
                + KEY_FILE_ID + " TEXT NOT NULL "
                + ")";
        db.execSQL(CREATE_PLAYLIST_TABLE);
        db.execSQL(CREATE_RECENT_SEARCH_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAT_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_SEARCH);
        onCreate(db);
    }


    /**
     * Insert new PlatListData
     *
     * @param platListData
     * @return
     */
    public void InsertPlayListData(PlayListData platListData) {
        if (!isPlayListAlreadyExists(platListData.getRecordID())) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_RECORD_ID, platListData.getRecordID());
            values.put(KEY_ARTIST_ID, platListData.getArtistId());
            values.put(KEY_ARTIST_NAME, platListData.getArtistName());
            values.put(KEY_RECORD_TITLE, platListData.getRecordTitle());
            values.put(KEY_START_WITH, platListData.getStartWith());
            values.put(KEY_RECORD_IMAGE, platListData.getRecordImage());
            values.put(KEY_RECORD_TYPE, platListData.getRecordType());
            values.put(KEY_RECORD_TYPE_ID, platListData.getRecordTypeId());
            values.put(KEY_RECORD_TYPE_ID, platListData.getRecordTypeId());
            values.put(KEY_FILE_ID, platListData.getFileId());
            long r = db.insert(TABLE_PLAT_LIST, null, values);
            db.close();
        }
    }


    public void insertSearchData(String search) {
        if (!isSearchAlreadyExists(search)) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            /* values.put(KEY_RECORD_ID, platListData.getId());*/
            values.put(KEY_ARTIST_ID, "");
            values.put(KEY_ARTIST_NAME, search);
            values.put(KEY_UserName, "");
            values.put(KEY_RECORD_IMAGE, "");
            /*values.put(KEY_RecordCurrencyType, platListData.getRecordCurrencyType());
            values.put(KEY_RECORD_IMAGE, platListData.getRecordOrUserImage());
            values.put(KEY_RECORD_TITLE, platListData.getRecordOrUserImage());
            values.put(KEY_RecordReleaseDate, platListData.getRecordReleaseDate());
            values.put(KEY_RecordStatusTypeId, platListData.getRecordStatusTypeId());
            values.put(KEY_Score, platListData.getScore());
            values.put(KEY_TotalInvestedAmount, platListData.getTotalInvestedAmount());
            values.put(KEY_TotalInvestors, platListData.getTotalInvestors());
            values.put(KEY_RECORD_TYPE_ID, platListData.getTypeId());
            values.put(KEY_UserBio, platListData.getUserBio());
            values.put(KEY_UserIsArtist, platListData.getUserIsArtist() ? 1 : 0);
            values.put(KEY_UserName, platListData.getUserName());*/
            long r = db.insert(TABLE_RECENT_SEARCH, null, values);
            db.close();
        }
    }

    public void insertSearchDataNew(String artistId, String search, String userName, String artistImage) {
        if (!isSearchAlreadyExists(search)) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            /* values.put(KEY_RECORD_ID, platListData.getId());*/
            values.put(KEY_ARTIST_ID, artistId);
            values.put(KEY_ARTIST_NAME, search);
            values.put(KEY_UserName, userName);
            values.put(KEY_RECORD_IMAGE, artistImage);
            /*values.put(KEY_RecordCurrencyType, platListData.getRecordCurrencyType());
            values.put(KEY_RECORD_IMAGE, platListData.getRecordOrUserImage());
            values.put(KEY_RECORD_TITLE, platListData.getRecordOrUserImage());
            values.put(KEY_RecordReleaseDate, platListData.getRecordReleaseDate());
            values.put(KEY_RecordStatusTypeId, platListData.getRecordStatusTypeId());
            values.put(KEY_Score, platListData.getScore());
            values.put(KEY_TotalInvestedAmount, platListData.getTotalInvestedAmount());
            values.put(KEY_TotalInvestors, platListData.getTotalInvestors());
            values.put(KEY_RECORD_TYPE_ID, platListData.getTypeId());
            values.put(KEY_UserBio, platListData.getUserBio());
            values.put(KEY_UserIsArtist, platListData.getUserIsArtist() ? 1 : 0);
            values.put(KEY_UserName, platListData.getUserName());*/
            long r = db.insert(TABLE_RECENT_SEARCH, null, values);
            db.close();
        }
    }


    public ArrayList<PlayListData> getPlayListAllData() {
        ArrayList<PlayListData> dataList = new ArrayList<>();
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String select = "SELECT * FROM " + TABLE_PLAT_LIST + " ORDER BY " + KEY_START_WITH + " ASC";
            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                if (mCursor.moveToFirst()) {
                    do {
                        PlayListData data = new PlayListData();

                        data.setRecordID(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_ID)));
                        data.setArtistId(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_ID)));
                        data.setArtistName(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_NAME)));
                        data.setStartWith(mCursor.getString(mCursor.getColumnIndex(KEY_START_WITH)));
                        data.setRecordTitle(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_TITLE)));
                        data.setRecordImage(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_IMAGE)));
                        data.setRecordType(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_TYPE)));
                        data.setRecordTypeId(mCursor.getInt(mCursor.getColumnIndex(KEY_RECORD_TYPE_ID)));
                        data.setFileId(mCursor.getString(mCursor.getColumnIndex(KEY_FILE_ID)));
                        dataList.add(data);
                    } while (mCursor.moveToNext());
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return dataList;
        }
    }

    public ArrayList<String> getSearchData() {
        ArrayList<String> dataList = new ArrayList<>();
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String select = "SELECT * FROM " + TABLE_RECENT_SEARCH  /*+" ORDER BY " + KEY_ARTIST_NAME + " ASC"*/;
            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                if (mCursor.moveToFirst()) {
                    do {
                        dataList.add(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_NAME)));
                        /*GlobalSearchResponseItem data = new GlobalSearchResponseItem();
                        data.setId(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_ID)));
                        data.setRecordArtistName(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_NAME)));
                        data.setRecordCurrencyType(mCursor.getString(mCursor.getColumnIndex(KEY_RecordCurrencyType)));
                        data.setRecordOrUserImage(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_IMAGE)));
                        data.setRecordOrUserName(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_TITLE)));
                        data.setRecordReleaseDate(mCursor.getString(mCursor.getColumnIndex(KEY_RecordReleaseDate)));
                        data.setRecordStatusTypeId(mCursor.getInt(mCursor.getColumnIndex(KEY_RecordStatusTypeId)));
                        data.setScore(mCursor.getInt(mCursor.getColumnIndex(KEY_Score)));
                        data.setTotalInvestedAmount(mCursor.getDouble(mCursor.getColumnIndex(KEY_TotalInvestedAmount)));
                        data.setTotalInvestors(mCursor.getInt(mCursor.getColumnIndex(KEY_TotalInvestors)));
                        data.setTypeId(mCursor.getInt(mCursor.getColumnIndex(KEY_RECORD_TYPE_ID)));
                        data.setUserBio(mCursor.getString(mCursor.getColumnIndex(KEY_UserBio)));
                        data.setUserIsArtist(mCursor.getInt(mCursor.getColumnIndex(KEY_UserIsArtist)) == 1);
                        data.setUserName(mCursor.getString(mCursor.getColumnIndex(KEY_UserName)));
                        dataList.add(data);*/
                    } while (mCursor.moveToNext());
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return dataList;
        }
    }

    public ArrayList<GlobalSearchResponseItem> getSearchDataNew() {
        ArrayList<GlobalSearchResponseItem> dataList = new ArrayList<>();
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String select = "SELECT * FROM " + TABLE_RECENT_SEARCH + " ORDER BY " + KEY_ARTIST_NAME + " ASC";
            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                if (mCursor.moveToFirst()) {
                    do {

                        GlobalSearchResponseItem artistModel = new GlobalSearchResponseItem();
                        artistModel.setId(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_ID)));
                        artistModel.setRecordOrUserName(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_NAME)));
                        artistModel.setUserName(mCursor.getString(mCursor.getColumnIndex(KEY_UserName)));
                        artistModel.setRecordOrUserImage(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_IMAGE)));

                        dataList.add(artistModel);
                        /*GlobalSearchResponseItem data = new GlobalSearchResponseItem();
                        data.setId(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_ID)));
                        data.setRecordArtistName(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_NAME)));
                        data.setRecordCurrencyType(mCursor.getString(mCursor.getColumnIndex(KEY_RecordCurrencyType)));
                        data.setRecordOrUserImage(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_IMAGE)));
                        data.setRecordOrUserName(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_TITLE)));
                        data.setRecordReleaseDate(mCursor.getString(mCursor.getColumnIndex(KEY_RecordReleaseDate)));
                        data.setRecordStatusTypeId(mCursor.getInt(mCursor.getColumnIndex(KEY_RecordStatusTypeId)));
                        data.setScore(mCursor.getInt(mCursor.getColumnIndex(KEY_Score)));
                        data.setTotalInvestedAmount(mCursor.getDouble(mCursor.getColumnIndex(KEY_TotalInvestedAmount)));
                        data.setTotalInvestors(mCursor.getInt(mCursor.getColumnIndex(KEY_TotalInvestors)));
                        data.setTypeId(mCursor.getInt(mCursor.getColumnIndex(KEY_RECORD_TYPE_ID)));
                        data.setUserBio(mCursor.getString(mCursor.getColumnIndex(KEY_UserBio)));
                        data.setUserIsArtist(mCursor.getInt(mCursor.getColumnIndex(KEY_UserIsArtist)) == 1);
                        data.setUserName(mCursor.getString(mCursor.getColumnIndex(KEY_UserName)));
                        dataList.add(data);*/
                    } while (mCursor.moveToNext());
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return dataList;
        }
    }

    public Boolean isPlayListAlreadyExists(String recordId) {
        boolean idFound = false;
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String select = "SELECT * FROM " + TABLE_PLAT_LIST + " WHERE " + KEY_RECORD_ID + " = '" + recordId + "'";
            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                idFound = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return idFound;
        }
    }

    public Boolean isSearchAlreadyExists(String name) {
        boolean idFound = false;
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();

            Cursor mCursor = db.query(TABLE_RECENT_SEARCH, new String[]{KEY_ARTIST_NAME}, KEY_ARTIST_NAME + "=?",
                    new String[]{name}, null, null, null, null);
//            String select = "SELECT * FROM " + TABLE_RECENT_SEARCH + " WHERE " + KEY_ARTIST_NAME + " = '" + recordId + "'";
//            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                idFound = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return idFound;
        }
    }


    public ArrayList<PlayListData> getAllDataByUserId(int userid) {
        ArrayList<PlayListData> dataList = new ArrayList<>();
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String select = "SELECT * FROM " + TABLE_PLAT_LIST + " WHERE " + KEY_RECORD_ID + " = '" + userid + "'";
            Cursor mCursor = db.rawQuery(select, null);
            if (mCursor.getCount() > 0) {
                if (mCursor.moveToFirst()) {
                    do {
                        PlayListData data = new PlayListData();

                        data.setRecordID(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_ID)));
                        data.setArtistId(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_ID)));
                        data.setArtistName(mCursor.getString(mCursor.getColumnIndex(KEY_ARTIST_NAME)));
                        data.setStartWith(mCursor.getString(mCursor.getColumnIndex(KEY_START_WITH)));
                        data.setRecordTitle(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_TITLE)));
                        data.setRecordImage(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_IMAGE)));
                        data.setRecordType(mCursor.getString(mCursor.getColumnIndex(KEY_RECORD_TYPE)));
                        data.setRecordTypeId(mCursor.getInt(mCursor.getColumnIndex(KEY_RECORD_TYPE_ID)));
                        dataList.add(data);
                    } while (mCursor.moveToNext());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            return dataList;
        }
    }

    public void clearPlayListDatabase() {
        this.getWritableDatabase().execSQL("DELETE FROM " + TABLE_PLAT_LIST);
    }

    public void clearDataBase() {
        this.getWritableDatabase().execSQL("DELETE FROM " + TABLE_RECENT_SEARCH);
        this.getWritableDatabase().execSQL("DELETE FROM " + TABLE_PLAT_LIST);
    }

    public void deleteUsingID(int id) {
        getWritableDatabase().execSQL("delete from " + TABLE_PLAT_LIST + " where " + KEY_RECORD_ID + "= '" + id + "'");
    }

}