package com.nibula.useractivity.helper
import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.request.UpdateNotificationStatus
import com.nibula.response.BaseResponse
import com.nibula.response.notifiaction.NotificationResponse
import com.nibula.response.notifiaction.ResponseCollection
import com.nibula.retrofit.*
import com.nibula.useractivity.ui.NotificationFragment
import com.nibula.utils.AppConstant

class NotificationHelper(val fg: NotificationFragment) : BaseHelperFragment() {

    var nfNextPage: Int = 1
    var nfMaxPage: Int = 0
    val unreadList = ArrayList<ResponseCollection>()
    val readList = ArrayList<ResponseCollection>()

    fun requestNotificationsList(
        isProgressDialog: Boolean,
        fromPullToRefresh: Boolean
    ) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.spNotif.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }
        fg.isLoading = true
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.requestNotificationsList(nfNextPage)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<NotificationResponse>() {

            override fun onSuccess(any: NotificationResponse?, message: String?) {
                fg.isLoading = false
                /*Load More*/
                nfMaxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = nfNextPage < nfMaxPage ||
                        (any?.totalRecords ?: 0) > (nfNextPage * 10)

                if (any?.responseCollection != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.responseCollection.isNullOrEmpty()) {
                        if (fromPullToRefresh) {
                            fg.adapter.clearData()
                        }
                        fg.adapter.addData(any.responseCollection)
/*
                        updateData(any.responseCollection)
*/
                        nfNextPage++

                    } else {
                        if (nfNextPage == 1) {
                            fg.adapter.clearData()
                        }
                    }
                } else {
                    if (nfNextPage == 1) {
                        fg.adapter.clearData()
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
                fg.isLoading = false
            }

        }))

    }

/*
    private fun updateData(responseCollection: List<ResponseCollection>) {

        for (element in responseCollection) {
            if (element.isRead) {
                if (readList.isEmpty()) {
                    val header = ResponseCollection(
                        "",
                        "",
                        -1,
                        true,
                        "",
                        "",
                        "",
                        1,
                        NotificationAdapter.NOTIFY_HEADER,
                        CommonUtils.dateFormatterNotification(element.notificationDate), -1
                    )
                    readList.add(header)
                }
                element.viewType = NotificationAdapter.NOTIFY_DETAIL
                readList.add(element)
            } else {
                if (unreadList.isEmpty()) {
                    val header = ResponseCollection(
                        "",
                        "",
                        -1,
                        true,
                        "",
                        "",
                        "",
                        1,
                        NotificationAdapter.NOTIFY_HEADER,
                        "New", -1
                    )
                    unreadList.add(header)
                }
                element.viewType = NotificationAdapter.NOTIFY_DETAIL
                unreadList.add(element)
            }
        }

        val totalNotification = ArrayList<ResponseCollection>()
        totalNotification.addAll(unreadList)
        totalNotification.addAll(readList)
        fg.adapter.setData(totalNotification)
    }
*/

    private fun dismiss(isProgressDialog: Boolean) {
        fg.isLoading = false
        if (fg.binding.spNotif.isRefreshing) {
            fg.binding.spNotif.isRefreshing = false
        } else if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        noData()
    }

    private fun noData() {
        if (fg.adapter.dataList.isEmpty()) {
            fg.binding.gdNoNotif.visibility = View.VISIBLE
            fg.binding.rvNotifications.visibility = View.GONE
        } else {
            fg.binding.gdNoNotif.visibility = View.GONE
            fg.binding.rvNotifications.visibility = View.VISIBLE
        }
    }


    fun updateNotificationStatus(notificationTypeId: Int?) {

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val request = UpdateNotificationStatus()
        request.notificaationId = notificationTypeId!!.toString()

        val call = helper.updateNotificationStatus(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {

                val response = any as BaseResponse
                if (response.responseStatus == 1) {

                    fg.redirectedScreen()
                } else {
                    fg.showToast(
                        fg.requireContext(),
                        response.responseMessage ?: "",
                        CustomToast.ToastType.FAILED
                    )
                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()
            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })
    }

}