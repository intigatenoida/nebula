package com.nibula.useractivity.helper

import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.HowItWork.HowItWorkResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.useractivity.ui.HowItWorkFragment

class HowItWorkHelper(val fg:HowItWorkFragment):BaseHelperFragment() {

    fun getContent(){
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }

            fg.showProcessDialog()

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getHowItWork()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<HowItWorkResponse>() {

            override fun onSuccess(any: HowItWorkResponse?, message: String?) {
                fg.hideProcessDialog()
                if (any?.responseStatus==1?:0){
                    fg.updateUI(any?.responseCollection)
                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }

        }))

    }
}