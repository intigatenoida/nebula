package com.nibula.useractivity.helper

import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.chatHistoryResponse.ChatHistoryResponse
import com.nibula.response.chatRecordResponse.ChatRecordResponse
import com.nibula.response.chatRecordResponse.ResponseCollection
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.useractivity.adapters.ChatHistoryAdapter
import com.nibula.useractivity.ui.ChatFragment
import com.nibula.utils.CommonUtils

class ChatHelper(val fg: ChatFragment) : BaseHelperFragment() {
    var nfNextPage: Int = 1
    var nfMaxPage: Int = 0
    var lastSelectedData = ""
    var totalRecord = 0


    fun getChatRecords(chatId: Int, recordId: String) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss()
            return
        }


        fg.showProcessDialog()

        val helper =
            ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        val call = helper.getChatRecord(chatId)
        call.enqueue(object : CallBackManager<ChatRecordResponse>() {
            override fun onSuccess(any: ChatRecordResponse?, message: String) {
                dismiss()
                val response = any as ChatRecordResponse
                response?.recordDetail?.responseCollection?.let {
                    val recordDetailList = it as ArrayList<ResponseCollection>
                    if (recordDetailList.size == 1) {
                        recordDetailList[0].isSelected = true
                    } else {
                        for (record in recordDetailList) {
                            if (record.recordId == recordId) {
                                recordDetailList.remove(record)
                                recordDetailList.add(0, record)
                                break
                            }
                        }
                    }
                    fg.albumAdapter.addData(recordDetailList)
                }

            }

            override fun onFailure(message: String) {
                dismiss()
            }

            override fun onError(error: RetroError) {
                dismiss()
            }
        })
    }

    fun getChatHistory(chatId: Int, recordId: String, refreshRecord: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss()
            return
        }
        if (nfMaxPage == 1) {
            fg.showProcessDialog()
        }

        fg.isLoading = true
        val helper =
            ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        val call = helper.getChatHistory(chatId, recordId, nfNextPage, totalRecord)
        call.enqueue(object : CallBackManager<ChatHistoryResponse>() {
            override fun onSuccess(any: ChatHistoryResponse?, message: String) {

                fg.isLoading = false
                if(any?.recordDetail?.responseCollection?.isNotEmpty() == true) {
                    totalRecord = any.recordDetail.responseCollection[0].totalRecords ?: 0
                }
                nfMaxPage = (totalRecord / 20.0).toInt()
                fg.isDataAvailable = nfNextPage < nfMaxPage ||
                        (totalRecord ?: 0) > (nfNextPage * 10)
                val response = any as ChatHistoryResponse
                if (response.recordDetail?.responseStatus == 1) {
                    response.recordDetail?.responseCollection?.let { parseChatData(it, refreshRecord) }
                }
                nfNextPage++
                dismiss()
            }

            override fun onFailure(message: String) {
                dismiss()
            }

            override fun onError(error: RetroError) {
                dismiss()
            }
        })
    }

    private fun parseChatData(
        responseCollection: List<com.nibula.response.chatHistoryResponse.ResponseCollection>,
        refreshRecord: Boolean
    ) {
        var chatList = ArrayList<com.nibula.response.chatHistoryResponse.ResponseCollection>()

        for (index in responseCollection.indices) {
            val tempDate = responseCollection[index].actionDate.toLowerCase()
                .substring(0, responseCollection[index].actionDate.lastIndexOf("T"))
            if (lastSelectedData != tempDate) {
                lastSelectedData = tempDate
                val temp = com.nibula.response.chatHistoryResponse.ResponseCollection()
                temp.viewType = ChatHistoryAdapter.VIEW_TYPE_DATE
                temp.messageId = responseCollection[index].messageId
                temp.message = CommonUtils.chatDateFormatter(
                    fg.requireContext(),
                    responseCollection[index].actionDate
                )
                chatList.add(temp)
            }
            var isSameTime = false
            val presentTime = CommonUtils.chatTimeFormatter(responseCollection[index].actionDate)
            val futureTime = if (index < responseCollection.size - 1) {
                CommonUtils.chatTimeFormatter(responseCollection[index + 1].actionDate)
            } else {
                ""
            }

            val isSameSide =  if (index < responseCollection.size - 1) {
                responseCollection[index + 1].isRight == responseCollection[index].isRight
            } else {
               false
            }

            val pasttime = if (index > 0) {
                CommonUtils.chatTimeFormatter(responseCollection[index - 1].actionDate)
            } else {
                ""
            }

            isSameTime = !(presentTime != futureTime)


        responseCollection[index].time = presentTime
        responseCollection[index].viewType =
            if (responseCollection[index].recordId != "00000000-0000-0000-0000-000000000000" && !responseCollection[index].recordId.isNullOrEmpty()) {
                if (responseCollection[index].isRight) {
                    if (isSameTime && isSameSide) {
                        ChatHistoryAdapter.VIEW_RECORD_WITHOUT_TIME
                    } else {
                        ChatHistoryAdapter.VIEW_TYPE_RECORD_RIGHT
                    }
                } else {
                    if (isSameTime && isSameSide) {
                        ChatHistoryAdapter.RECEIVED_RECORD_WITHOUT_TIME
                    } else {
                        ChatHistoryAdapter.VIEW_TYPE_RECORD_LEFT
                    }

                }
            } else {
                if (responseCollection[index].isRight) {
                    if (isSameTime && isSameSide) {
                        ChatHistoryAdapter.VIEW_CHAT_MESSAGE_WITHOUT_TIME
                    } else {
                        ChatHistoryAdapter.VIEW_TYPE_SENT
                    }
                } else {
                    if (isSameTime && isSameSide) {
                        ChatHistoryAdapter.RECEIVED_CHAT_MESSAGE_WITHOUT_TIME
                    } else {
                        ChatHistoryAdapter.VIEW_TYPE_RECEIVED
                    }
                }
            }

        chatList.add(responseCollection[index])
    }


/*
        for (chatData in responseCollection) {
            val tempDate = chatData.actionDate.toLowerCase().substring(0, chatData.actionDate.lastIndexOf("T"))
            if (lastSelectedData != tempDate) {
                lastSelectedData = tempDate
                val temp = com.nibula.response.chatHistoryResponse.ResponseCollection()
                temp.viewType = ChatHistoryAdapter.VIEW_TYPE_DATE
                temp.messageId = chatData.messageId
                temp.message = CommonUtils.chatDateFormatter(fg.requireContext(),chatData.actionDate)
                chatList.add(temp)
            }

            val recordTime = CommonUtils.chatTimeFormatter(chatData.actionDate)
            var isSameTime  = false
            if(time != recordTime){
                time = recordTime
                isSameTime = false
            }else{
                isSameTime= true
            }
            chatData.time= recordTime
            chatData.viewType = if (chatData.recordId != "00000000-0000-0000-0000-000000000000" && !chatData.recordId.isNullOrEmpty()) {
                    if (chatData.isRight) {
                        ChatHistoryAdapter.VIEW_TYPE_RECORD_RIGHT
                    } else {
                        ChatHistoryAdapter.VIEW_TYPE_RECORD_LEFT
                    }
                } else {
                    if (chatData.isRight) {
                        if(isSameTime){
                            ChatHistoryAdapter.VIEW_CHAT_MESSAGE_WITHOUT_TIME
                        }else{
                            ChatHistoryAdapter.VIEW_TYPE_SENT
                        }
                    } else {
                        ChatHistoryAdapter.VIEW_TYPE_RECEIVED
                    }
                }

            chatList.add(chatData)
        }
*/

    if (refreshRecord)
    {
        fg.chatHistoryAdapter.clearAddChat(chatList)
    } else
    {
        fg.chatHistoryAdapter.addChat(chatList)
    }
    if (fg.chatHistoryAdapter.getFileSize() <= 20)
    {
        fg.layoutManager.scrollToPositionWithOffset(fg.chatHistoryAdapter.getFileSize() - 1, 0)
    }
//        fg.rootView.rv_messages.smoothScrollToPosition(fg.chatHistoryAdapter.getFileSize())
}


private fun dismiss() {
    fg.hideProcessDialog()
}


}