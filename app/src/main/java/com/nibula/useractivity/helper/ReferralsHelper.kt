package com.nibula.useractivity.helper

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.request.DeleteReferralRequest
import com.nibula.response.BaseResponse
import com.nibula.response.referalresponse.ReferalListResponse
import com.nibula.response.referalresponse.ResponseCollection
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.useractivity.ui.ReferralsFragment
import com.nibula.utils.CommonUtils

class ReferralsHelper(val fg: ReferralsFragment) : BaseHelperFragment() {

    val newList = ArrayList<ResponseCollection>()
    val earlirList = ArrayList<ResponseCollection>()
    var nfNextPage: Int = 1
    var nfMaxPage: Int = 0
    var isNewHeaderAdded = false
    var isEarlierHeaderAdded = false


    fun getReferalList(showDialog: Boolean) {


        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(showDialog)
            return
        }

        if (!fg.binding.spReferal.isRefreshing && showDialog
        ) {
            fg.showProcessDialog()
        }
        fg.isLoading = true
        val helper =
            ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getReferalList(nfNextPage)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<ReferalListResponse>() {

            override fun onSuccess(any: ReferalListResponse?, message: String?) {
                fg.isLoading = false
                /*Load More*/
                nfMaxPage = ((any?.recordDetail?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = nfNextPage < nfMaxPage ||
                        (any?.recordDetail?.totalRecords ?: 0) > (nfNextPage * 10)
                if (any?.recordDetail != null &&
                    any.recordDetail.responseStatus == 1
                ) {
                    if (!any.recordDetail.responseCollection.isNullOrEmpty()) {
                        updateData(any.recordDetail.responseCollection)
                        nfNextPage++
                    }
                }
                dismiss(showDialog)
            }

            override fun onError(error: RetroError?) {
                fg.isLoading = false
                dismiss(showDialog)
            }

        }))

    }

    private fun updateData(responseCollection: List<ResponseCollection>) {
        if (fg.binding.spReferal.isRefreshing) {
            newList.clear()
            earlirList.clear()
        }
        for (referal in responseCollection) {
            if (CommonUtils.isDateToday(referal.createdOn)) {
                newList.add(element = referal)
            } else {
                earlirList.add(element = referal)
            }
        }
        val tempList = ArrayList<ResponseCollection>()
        if (newList.isNotEmpty() && !isNewHeaderAdded) {
            val tempCollection = ResponseCollection(
                0,
                "",
                false,
                false,
                fg.getString(R.string.new_),
                "",
                "",
                "",
                "",
                "",
                2,
                "",
                "",
                0
            )
          newList.add(0,tempCollection)
            isNewHeaderAdded=true
        }

        if (newList.isNotEmpty()) {
            tempList.addAll(newList)
        }

        if (earlirList.isNotEmpty() && !isEarlierHeaderAdded && isNewHeaderAdded) {
            val tempCollection = ResponseCollection(
                0,
                "",
                false,
                false,
                fg.getString(R.string.earlier),
                "",
                "",
                "",
                "",
                "",
                2,
                "",
                "",
                0
            )
            earlirList.add(0,tempCollection)
            isEarlierHeaderAdded=true
        }

        if (earlirList.isNotEmpty()) {
            tempList.addAll(earlirList)
        }

        if (fg.binding.spReferal.isRefreshing) {
            fg.adapter.clearAndAddData(tempList)
        } else {
            fg.adapter.addData(tempList)
        }
    }


    private fun dismiss(showDialog: Boolean) {
        if (fg.binding.spReferal.isRefreshing) {
            fg.binding.spReferal.isRefreshing = false
        } else if (showDialog) {
            fg.hideProcessDialog()
        }
        noData()
    }

    private fun noData() {
        if (fg.adapter.data.isEmpty()) {
            fg.binding.noDataFoundReferal.visibility = View.VISIBLE
            fg.binding.rvReferrals.visibility = View.GONE
        } else {
            fg.binding.noDataFoundReferal.visibility = View.GONE
            fg.binding.rvReferrals.visibility = View.VISIBLE
        }
    }

    fun deleteReferral() {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(true)
            return
        }

        if (!fg.binding.spReferal.isRefreshing
        ) {
            fg.showProcessDialog()
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val request = DeleteReferralRequest()
        request.chatId = fg.adapter.data[fg.adapter.deletedPosition].chatId.toString()
        request.recordId = fg.adapter.data[fg.adapter.deletedPosition].recordId.toString()
        val observable = helper.deleteReferral(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BaseResponse>() {

            override fun onSuccess(any: BaseResponse?, message: String?) {
                if (any != null &&
                    any.responseStatus == 1
                ) {
                    fg.adapter.removeItem()
                }
                dismiss(true)
            }

            override fun onError(error: RetroError?) {
                dismiss(true)
            }

        }))

    }
}