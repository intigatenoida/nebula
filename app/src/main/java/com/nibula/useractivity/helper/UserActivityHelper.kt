package com.nibula.useractivity.helper

import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.notifiaction.NotificationResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.useractivity.ui.UserActivityFragment
import com.nibula.utils.PrefUtils

class UserActivityHelper(val fg: UserActivityFragment) : BaseHelperFragment() {

    fun getNotificationCount() {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            //fg.hideProcessDialog()
            PrefUtils.saveValueInPreference(
                fg.requireContext()
                , PrefUtils.notificationCount, 0
            )
            PrefUtils.saveValueInPreference(
                fg.requireContext()
                , PrefUtils.referralCount, 0
            )
            return
        }

        fg.showProcessDialog()


        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getNotificationCount()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<NotificationResponse>() {

            override fun onSuccess(any: NotificationResponse?, message: String?) {


                if (any?.responseCollection != null &&
                    any.responseStatus == 1
                ) {
                    PrefUtils.saveValueInPreference(
                        fg.requireContext()
                        , PrefUtils.notificationCount, any.totalRecords
                    )
                }
                getReferralCount()
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
                PrefUtils.saveValueInPreference(
                    fg.requireContext()
                    , PrefUtils.notificationCount, 0
                )
                getReferralCount()
            }

        }))

    }

    fun getReferralCount() {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideProcessDialog()
            PrefUtils.saveValueInPreference(
                fg.requireContext()
                , PrefUtils.referralCount, 0
            )
            fg.intiUI()
            return
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getReferralCount()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<NotificationResponse>() {

            override fun onSuccess(any: NotificationResponse?, message: String?) {

                fg.hideProcessDialog()
                if (any?.responseCollection != null &&
                    any.responseStatus == 1
                ) {
                    PrefUtils.saveValueInPreference(
                        fg.requireContext()
                        , PrefUtils.referralCount, any.totalRecords
                    )
                }
                fg.intiUI()
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
                PrefUtils.saveValueInPreference(
                    fg.requireContext()
                    , PrefUtils.referralCount, 0
                )
                fg.intiUI()
            }

        }))

    }
}