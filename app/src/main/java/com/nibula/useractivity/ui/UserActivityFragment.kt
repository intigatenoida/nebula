package com.nibula.useractivity.ui

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayout
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentUserActivityBinding
import com.nibula.useractivity.adapters.UserActivityViewPagerAdapter
import com.nibula.useractivity.helper.UserActivityHelper
import com.nibula.useractivity.vmodels.UserActivityFramentViewModel
import com.nibula.utils.PrefUtils


class UserActivityFragment : BaseFragment() {

    companion object {
        lateinit var instance: UserActivityFragment
        fun newInstance(): UserActivityFragment {
//            if (!::instance.isInitialized) {
                instance = UserActivityFragment()
//            }
            return instance
        }
    }

    private lateinit var viewModel: UserActivityFramentViewModel
    lateinit var adapter: UserActivityViewPagerAdapter
    lateinit var helper: UserActivityHelper
lateinit var binding:FragmentUserActivityBinding
    lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_user_activity, container, false)
            binding= FragmentUserActivityBinding.inflate(inflater,container,false)
            rootView=binding.root
            initHelper()
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
      //  ivNotification.setColorFilter(ContextCompat.getColor(requireContext(), R.color.colorAccent))
    }

    fun initHelper() {
        helper = UserActivityHelper(this)
        helper.getNotificationCount()
    }


    fun intiUI() {
        initTopHeader()
        prepareTab()
    }

    private fun prepareTab() {
        adapter = UserActivityViewPagerAdapter(requireActivity().supportFragmentManager)

        binding.viewPager.adapter = adapter
        binding.tbUserActivity.setupWithViewPager(binding.viewPager)
//      rootView.viewPager.offscreenPageLimit = 1
        binding.viewPager.setOnTouchListener { v, event -> true }
        binding.viewPager.beginFakeDrag()

     /*   addTab(
            rootView.tb_user_activity.getTabAt(0),
            getString(R.string.referrals),
            R.font.pt_sans_bold,
            Typeface.NORMAL,
            View.VISIBLE, PrefUtils.getIntValue(requireContext(), PrefUtils.referralCount)
        )*/

        addTab(
            binding.tbUserActivity.getTabAt(0),
            getString(R.string.notifications),
            R.font.pt_sans,
            Typeface.NORMAL,
            View.VISIBLE, PrefUtils.getIntValue(requireContext(), PrefUtils.notificationCount)
        )

        binding.tbUserActivity.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
                var count = 0
                if (binding.tbUserActivity.getTabAt(0)==p0) {
                    count = PrefUtils.getIntValue(requireContext(), PrefUtils.referralCount)
                } else{
                    count = PrefUtils.getIntValue(requireContext(), PrefUtils.notificationCount)
                }
                changeTabAppearance(p0, View.GONE, count)
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                var count = 0
                if (binding.tbUserActivity.getTabAt(1)==p0) {
                    count = PrefUtils.getIntValue(requireContext(), PrefUtils.notificationCount)
                } else {
                    count = PrefUtils.getIntValue(requireContext(), PrefUtils.referralCount)
                }
                changeTabAppearance(p0, View.VISIBLE, count)
            }

        })

        wrapTabIndicatorToTitle(binding.tbUserActivity, 0, 0)

    }

    private fun initTopHeader() {
        binding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()

        }

        binding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }
    }
}
