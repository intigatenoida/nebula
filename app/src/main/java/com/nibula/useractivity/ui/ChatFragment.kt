package com.nibula.useractivity.ui
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import com.microsoft.signalr.HubConnection
import com.microsoft.signalr.HubConnectionBuilder
import com.microsoft.signalr.HubConnectionState
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.FragmentUserRefrralsDetailsBinding
import com.nibula.retrofit.ApiClient
import com.nibula.useractivity.adapters.AlbumAdapter
import com.nibula.useractivity.adapters.ChatHistoryAdapter
import com.nibula.useractivity.helper.ChatHelper
import com.nibula.useractivity.vmodels.UserRefrralsDetailsViewModel
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import io.reactivex.Single

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ChatFragment : BaseFragment(), AlbumAdapter.onOpenChatHistoryListener,
    ChatHistoryAdapter.onOpenChatRecordListener {

    companion object {
        fun newInstance(bundle: Bundle): ChatFragment {
            val fg = ChatFragment()
            fg.arguments = bundle
            return fg
        }
    }
    var isLoading = false
    var isDataAvailable = false
    lateinit var layoutManager: LinearLayoutManager
    private var chatId: Int = -1
    private var partnerName = ""
    private var recordId: String = ""
    private var partnerId: String = ""
    private var selectedRecordId = ""
    private lateinit var hubConnection: HubConnection
    lateinit var binding:FragmentUserRefrralsDetailsBinding
    private lateinit var viewModel: UserRefrralsDetailsViewModel
    lateinit var rootView: View
    val albumAdapter: AlbumAdapter by lazy { AlbumAdapter(requireContext(), this) }
    val chatHistoryAdapter: ChatHistoryAdapter by lazy {
        ChatHistoryAdapter(
            requireContext(),
            this
        )
    }
    private var originalMode: Int? = null
    private lateinit var helper: ChatHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        originalMode = activity?.window?.getSoftInputMode()
        activity?.window?.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::binding.isInitialized) {
            //binding = inflater.inflate(R.layout.fragment_user_refrrals_details, container, false)
            binding= FragmentUserRefrralsDetailsBinding.inflate(inflater,container,false)
            rootView=binding.root
            intiUI()
            initHelper()
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
        )
     /*   GlobalScope.launch {
            createConnection()
        }*/
    }

    private fun initHelper() {
        helper = ChatHelper(this)
        helper.getChatRecords(chatId, recordId)
        helper.getChatHistory(chatId, recordId, true)
    }

    private fun intiUI() {
        chatId = arguments?.getInt(AppConstant.CHAT_ID) ?: -1
        recordId = arguments?.getString(AppConstant.RECORD_ID) ?: ""
        selectedRecordId = recordId
        albumAdapter.checkedId=selectedRecordId
        partnerId = arguments?.getString(AppConstant.partner_ID) ?: ""
        partnerName = arguments?.getString(AppConstant.RECORD_ARTIST_NAME) ?: ""
        binding.layoutTbMain.tvTitle.text = partnerName
        binding.layoutTbMain.imgHome.setOnClickListener {
            navigate.manualBack()
        }
        CommonUtils.loadImage(
            requireContext(),
            arguments?.getString(AppConstant.RECORD_IMAGE_URL) ?: "",
            binding.layoutTbMain.imgUser
        )
        setAlbumAdapter()
        setMessageAdapter()
        binding.tvSend.setOnClickListener {

            CustomToast.showToast(
                requireContext(),
                getString(R.string.message_something_wrong),
                CustomToast.ToastType.FAILED
            )
            //sendMessage()
        }
        binding.tvMessage.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    sendMessage()
                    return true
                }
                return false
            }

        })
    }

    private fun sendMessage() {
        if (::hubConnection.isInitialized && hubConnection.connectionState == HubConnectionState.CONNECTED
        ) {
            if (binding.tvMessage.text.toString().trim().isEmpty()) return
            hubConnection.send(
                "SendMessage",
                partnerId,
                chatId,
                recordId,
                binding.tvMessage.text.toString()
            )
            addSendMessage(binding.tvMessage.text.toString())
            binding.tvMessage.setText("")
        } else {
            GlobalScope.launch {
                createConnection()
            }
            CustomToast.showToast(
                requireContext(),
                getString(R.string.message_something_wrong),
                CustomToast.ToastType.FAILED
            )
        }
    }


    private fun addSendMessage(message: String) {
        addMessage(message, ChatHistoryAdapter.VIEW_TYPE_SENT)
    }

    private fun addReceiveMessage(message: String) {
        addMessage(message, ChatHistoryAdapter.VIEW_TYPE_RECEIVED)
    }

    private fun addMessage(message: String, viewType: Int) {
        var shouldAddMessage = false

        for (i in chatHistoryAdapter.chatData.indices.reversed()) {
            if (chatHistoryAdapter.chatData[i].viewType == ChatHistoryAdapter.VIEW_TYPE_DATE) {
                if (chatHistoryAdapter.chatData[i].message != getString(R.string.today)) {
                    shouldAddMessage = true
                }
                break
            }
        }
        if (shouldAddMessage) {
            val temp = com.nibula.response.chatHistoryResponse.ResponseCollection()
            temp.viewType = ChatHistoryAdapter.VIEW_TYPE_DATE
            temp.message = getString(R.string.today)
            helper.lastSelectedData
            chatHistoryAdapter.addSendChat(temp)
        }
        if (viewType == ChatHistoryAdapter.VIEW_TYPE_RECEIVED) {
            chatHistoryAdapter.registerAdapterDataObserver(object : AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    super.onItemRangeInserted(positionStart, itemCount)
                    val myMessageCount: Int = chatHistoryAdapter.itemCount
                    val lastVisiblePosition: Int =
                        layoutManager.findLastCompletelyVisibleItemPosition()
                    // If the recycler view is initially being loaded or the
                    // user is at the bottom of the list, scroll to the bottom
                    // of the list to show the newly added message.
                    if ((lastVisiblePosition + 2) == myMessageCount) {//add two here 1 for indexing and 1 for recently added item which is not shown
                        scrollTOBottom()
                    }
                }
            })
        }
        val temp = com.nibula.response.chatHistoryResponse.ResponseCollection()
        temp.viewType = viewType
        temp.message = message
        temp.time = CommonUtils.chatTimeFormatter()
        helper.lastSelectedData
        chatHistoryAdapter.addSendChat(temp)
//        if (viewType == ChatHistoryAdapter.VIEW_TYPE_SENT) {
//            scrollTOBottom()
//        }
        scrollTOBottom()
    }

    private fun scrollTOBottom() {
//        layoutManager.scrollToPositionWithOffset(chatHistoryAdapter.getFileSize(), 0)
        binding.rvMessages.smoothScrollToPosition(chatHistoryAdapter.getFileSize()-1)
    }

    private fun setAlbumAdapter() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvAlbums.layoutManager = layoutManager
        binding.rvAlbums.adapter = albumAdapter
    }

    private fun setMessageAdapter() {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false).apply {
            stackFromEnd = true
            isSmoothScrollbarEnabled = true
        }
        binding.rvMessages.layoutManager = layoutManager
        binding.rvMessages.adapter = chatHistoryAdapter
        binding.rvMessages.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val topVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                if (!isLoading &&
                    isDataAvailable &&
                    topVisibleItem == 0
                ) {
                    helper.getChatHistory(chatId, recordId, false)
                }
            }
        })
    }
    override fun onDestroy() {
        super.onDestroy()
        originalMode?.let { activity?.window?.setSoftInputMode(it) }
    }
    private fun createConnection() {
        if (::hubConnection.isInitialized) return

        val token = Single.just(
            PrefUtils.getValueFromPreference(
                requireContext(),
                PrefUtils.TOKEN
            )
        )

        hubConnection =
            HubConnectionBuilder.create(ApiClient.CHAT_BASE_URL).withAccessTokenProvider(token)
                .build()
        hubConnection.start()?.blockingAwait()

        if (hubConnection.connectionState == HubConnectionState.CONNECTED) {
            CommonUtils.showLog("************chat", "chat connected")
        } else {
            CommonUtils.showLog("************chat", "chat disconnected")
        }

        hubConnection.on(
            "ReceiveMessage",
            { name: String?, message: String? ->
                if (partnerName == name) {
                    addReceiveMessage(message ?: "")
                }
            },
            String::class.java,
            String::class.java
        )
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun openChatHistory(recordId: String, partnerId: String) {
        if (selectedRecordId == recordId) return
        helper.totalRecord = 0
        helper.nfNextPage = 1
        this.partnerId=partnerId
        helper.getChatHistory(chatId, recordId, true)
        selectedRecordId = recordId
        this.recordId=recordId
        albumAdapter.checkedId = selectedRecordId
        albumAdapter.dataList.filter { id==9 }
        albumAdapter.notifyDataSetChanged()
    }

    override fun openChatRecord(recordId: String) {
        navigate.gotorequestinvest(recordId)
    }


}
