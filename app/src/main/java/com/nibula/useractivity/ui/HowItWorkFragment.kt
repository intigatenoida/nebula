package com.nibula.useractivity.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentWebContainerBinding
import com.nibula.response.HowItWork.ResponseCollection
import com.nibula.useractivity.adapters.HowItWorkAdapter
import com.nibula.useractivity.helper.HowItWorkHelper
import com.nibula.utils.CommonUtils


class HowItWorkFragment : BaseFragment() {

    companion object {
        fun newInstance() = HowItWorkFragment()
    }

    private lateinit var rootView: View
    private lateinit var helper: HowItWorkHelper
    lateinit var binding:FragmentWebContainerBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_web_container, container, false)
            binding= FragmentWebContainerBinding.inflate(inflater,container,false)
            rootView=binding.root
            initTopHeader()
        }

        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        /*val header = requireActivity().findViewById<View>(R.id.header)
        header.visibility = GONE*/

        binding.layoutTbMain.imgHome.setOnClickListener {
            navigate.manualBack()
            //requireActivity().supportFragmentManager.popBackStack()
        }
    }

    private fun initTopHeader() {
        helper = HowItWorkHelper(this)
        helper.getContent()
        binding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }
    }

    override fun onResume() {
        super.onResume()
        binding.header.ivNotification.setColorFilter(
            ContextCompat.getColor(
                requireContext(),
                R.color.violet_shade_100per
            )
        )
        CommonUtils.setNotificationDot(requireContext(), binding.header.imgNotificationCount)
    }

    fun updateUI(responseCollection: List<ResponseCollection>?) {
        if (responseCollection != null && responseCollection.isNotEmpty()) {
            binding.noDataFound.visibility = View.GONE
            binding.rcyHowItWork.visibility = View.VISIBLE
            val adapter = HowItWorkAdapter(
                requireContext(),
                (responseCollection as ArrayList<ResponseCollection>?)!!
            )
            binding.rcyHowItWork.layoutManager = LinearLayoutManager(requireContext())
            binding.rcyHowItWork.adapter = adapter
        } else {
            binding.noDataFound.visibility = View.VISIBLE
            binding.rcyHowItWork.visibility = View.GONE

        }
    }


}