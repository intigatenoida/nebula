package com.nibula.useractivity.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.dashboard.ConstantsNavTabs
import com.nibula.databinding.FragmentNotificationBinding
import com.nibula.pending_request.PendingRequestFragment
import com.nibula.response.notifiaction.ResponseCollection
import com.nibula.useractivity.adapters.NotificationAdapter
import com.nibula.useractivity.helper.NotificationHelper
import com.nibula.useractivity.vmodels.NotificationViewModel
import com.nibula.utils.AppConstant


class NotificationFragment : BaseFragment(), NotificationAdapter.onNotificationListener {

    companion object {
        fun newInstance() = NotificationFragment()
    }

    private lateinit var viewModel: NotificationViewModel
    lateinit var binding:FragmentNotificationBinding

    lateinit var rootView: View
    lateinit var helper: NotificationHelper
    lateinit var adapter: NotificationAdapter
    lateinit var data: ResponseCollection
    var isDataAvailable = false
    var isLoading = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_notification, container, false)
            binding= FragmentNotificationBinding.inflate(inflater,container,false)
            rootView=binding.root
            intiUI()
        }
        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        helper.onDestroy()
    }

    private fun intiUI() {
        setAdapter()
        helper = NotificationHelper(this)
        helper.requestNotificationsList(true, true)
    }

    private fun setAdapter() {
        val verticalDecoration =
            DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        val verticalDivider =
            ContextCompat.getDrawable(requireContext(), R.drawable.vertical_divider_0_5dp)
        verticalDecoration.setDrawable(verticalDivider!!)
        binding.rvNotifications.addItemDecoration(verticalDecoration)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvNotifications.layoutManager = layoutManager
        adapter = NotificationAdapter(requireContext(), this)
/*
        adapter.setData(mutableListOf())
*/
        binding.rvNotifications.adapter = adapter
        binding.rvNotifications.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.requestNotificationsList(false, false)
                }
            }
        })

        binding.spNotif.setOnRefreshListener {
            helper.nfNextPage = 1
            helper.nfMaxPage = 0
            helper.requestNotificationsList(false, true)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onNotificationClick(data: ResponseCollection) {
        this.data = data
        updateNotificationStatus(data?.id)

        /* navigate.changeTopTab(ConstantsNavTabs.NOTIFICATION_REDIRECTION)
         when (data.notificationTypeId) {
             AppConstant.InvestmentRequest -> {
                 val fragment = PendingRequestFragment.newInstance().apply {
                     arguments = Bundle().apply {
                         putString(AppConstant.ID, data.dependentRowId)
                     }
                 }

                 navigate.pushFragments(fragment)
             }
             AppConstant.SharesSold -> {
                 navigate.gotorequestinvest(data.dependentRowId)
             }
             AppConstant.NewOffering, AppConstant.PurchasedRecord, AppConstant.ConvertedToReleased -> {
                 navigate.gotorequestinvest(data.dependentRowId)
             }
         }*/
    }

    fun updateNotificationStatus(notificationId: Int?) {
        helper.updateNotificationStatus(notificationId)

    }

    fun redirectedScreen() {
        navigate.changeTopTab(ConstantsNavTabs.NOTIFICATION_REDIRECTION)
        when (data.notificationTypeId) {
            AppConstant.InvestmentRequest -> {
                val fragment = PendingRequestFragment.newInstance().apply {
                    arguments = Bundle().apply {
                        putString(AppConstant.ID, data.dependentRowId)
                    }
                }
                navigate.pushFragments(fragment)
            }
            AppConstant.SharesSold -> {
                navigate.gotorequestinvest(data.dependentRowId)
            }

            AppConstant.RecordSubmitted -> {

            }
            // AppConstant.NewOfferingUpdatedStatus, AppConstant.PurchasedRecord, AppConstant.ConvertedToReleasedUpdateStatus -> {
            else -> {
                navigate.gotorequestinvest(data.dependentRowId)

            }

        }
    }

}
