package com.nibula.useractivity.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.bottomsheets.PNBSheetFragment

import com.nibula.dashboard.ConstantsNavTabs.TOP_USER_RECORD_INVEST
import com.nibula.databinding.FragmentReferralsBinding
import com.nibula.records.invest.RecordInvestReferralFragment
import com.nibula.useractivity.adapters.UserActivityReferralsAdapter
import com.nibula.useractivity.helper.ReferralsHelper
import com.nibula.useractivity.vmodels.ReferralsViewModel
import com.nibula.utils.callbacks.SwipeToDeleteCallback
import com.nibula.utils.interfaces.DialogFragmentClicks


class ReferralsFragment : BaseFragment(), UserActivityReferralsAdapter.Communicator,
    DialogFragmentClicks {

    companion object {
        fun newInstance() = ReferralsFragment()
    }

    private lateinit var viewModel: ReferralsViewModel
    var isDataAvailable = false
    var isLoading = true
    lateinit var rootView: View
    private lateinit var helper: ReferralsHelper
    lateinit var adapter: UserActivityReferralsAdapter
    lateinit var binding:FragmentReferralsBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
           // rootView = inflater.inflate(R.layout.fragment_referrals, container, false)
            binding= FragmentReferralsBinding.inflate(inflater,container,false)
            rootView=binding.root
            intiUI()
            intiHelper()
        }
        return rootView
    }

    private fun intiHelper() {
        helper = ReferralsHelper(this)
        helper.getReferalList(false)
    }

    private fun intiUI() {
        binding.tvHowToWork.setOnClickListener {
            navigate.pushFragments(HowItWorkFragment.newInstance())

//            navigate.changeTopTab(TOP_USER_HOW_TO_WORK)
//            setFragment(
//                HowItWorkFragment.newInstance(),
//                R.id.containerFragment,
//                false
//            )
        }


        binding.spReferal.setOnRefreshListener {
            helper.nfNextPage = 1
            helper.isEarlierHeaderAdded = false
            helper.isNewHeaderAdded = false
            helper.getReferalList(false)
        }

        setAdapter()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvReferrals.layoutManager = layoutManager

        adapter = UserActivityReferralsAdapter(requireContext(), this)
        binding.rvReferrals.adapter = adapter


        binding.rvReferrals.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.getReferalList(false)
                }
            }
        })

        val swipeHandler = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if (direction == ItemTouchHelper.LEFT) {
                    val bPrompt = PNBSheetFragment(
                        getString(R.string.message_delete),
                        getString(R.string.delete),
                        getString(R.string.cancel),
                        this@ReferralsFragment
                    )
                    bPrompt.isCancelable = false
                    bPrompt.setColor(
                        ContextCompat.getColor(requireContext(), R.color.colorAccent),
                        ContextCompat.getColor(requireContext(), R.color.white),
                        ContextCompat.getColor(requireContext(), R.color.gray_1_51per),
                        ContextCompat.getColor(requireContext(), R.color.gray_1_51per)
                    )
                    bPrompt.show(
                        this@ReferralsFragment.childFragmentManager,
                        "PositiveNegativeBottomSheetFragment"
                    )
                    adapter.deletedPosition = viewHolder.adapterPosition
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(binding.rvReferrals)
    }


    override fun openDetails(bundle: Bundle) {
        navigate.openChat(bundle)
//        this@ReferralsFragment.startActivity(
//            Intent(
//                requireContext(),
//                UserReferralDetailActivity::class.java
//            )
//        )
    }

    override fun openOther(type: Int) {
        if (type == 1) {
           /* navigate.changeTopTab(TOP_USER_RECORD_BUY)
            setFragment(
                RecordBuyReferralsFragment.newInstance(),
                R.id.containerFragment,
                false
            )*/
        } else {
            navigate.changeTopTab(TOP_USER_RECORD_INVEST)
            setFragment(
                RecordInvestReferralFragment.newInstance(),
                R.id.containerFragment,
                false
            )
        }
    }

    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        dialog.dismiss()
        helper.deleteReferral()
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        dialog.dismiss()
        adapter.refreshItem(adapter.deletedPosition)
    }
}


