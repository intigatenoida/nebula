package com.nibula.useractivity.adapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.nibula.R
import com.nibula.databinding.ItemUserActivityReferralsBinding
import com.nibula.databinding.ItemUserActivityReferralsHeaderBinding
import com.nibula.response.referalresponse.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils



class UserActivityReferralsAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class HeaderViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class UserReferralsViewHolder(val view: View) : RecyclerView.ViewHolder(view)


    private lateinit var layoutInflater: LayoutInflater
    private lateinit var context: Context
    private lateinit var listener: Communicator
    lateinit var binding:ItemUserActivityReferralsHeaderBinding
    lateinit var refBinding:ItemUserActivityReferralsBinding

    var deletedPosition: Int = -1

    var data = ArrayList<ResponseCollection>()

    companion object {

        const val VIEW_TYPE_USER_REFERRAL = 1

        const val VIEW_TYPE_HEADER = 2

    }

    constructor(context: Context, listener: Communicator) : this() {
        this.context = context
        this.listener = listener
        layoutInflater = LayoutInflater.from(context)
    }

    fun addData(data: ArrayList<ResponseCollection>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun clearAndAddData(data: ArrayList<ResponseCollection>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun removeItem() {
        data.removeAt(deletedPosition)
        notifyItemRemoved(deletedPosition)
    }

    fun refreshItem(position: Int) {
        notifyItemChanged(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_HEADER) {
          /*  val view =
                layoutInflater.inflate(R.layout.item_user_activity_referrals_header, parent, false)
            HeaderViewHolder(view)*/

            val inflater = LayoutInflater.from(parent.context)
            binding = ItemUserActivityReferralsHeaderBinding.inflate(inflater, parent, false)
            val holder = HeaderViewHolder(binding.root)
            return holder
        } else {
          /*  val view = layoutInflater.inflate(R.layout.item_user_activity_referrals, parent, false)
            UserReferralsViewHolder(view)*/

            val inflater = LayoutInflater.from(parent.context)
            refBinding = ItemUserActivityReferralsBinding.inflate(inflater, parent, false)
            val holder = UserReferralsViewHolder(binding.root)
            return holder
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position].headerType == 2) {
            VIEW_TYPE_HEADER
        } else {
            VIEW_TYPE_USER_REFERRAL
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            binding.tvHeader.text = data[position].message
        } else {
            //UserReferralsViewHolder
            val itemHolder = holder as UserReferralsViewHolder
            val temp = data[position]

            itemHolder.itemView.setOnClickListener {
                val bundle = Bundle().apply {
                    putInt(AppConstant.CHAT_ID, temp.chatId)
                    putString(AppConstant.partner_ID, temp.partnerId)
                    putString(AppConstant.RECORD_ID, temp.recordId)
                    putString(AppConstant.RECORD_ARTIST_NAME, temp.partnerName)
                    putString(AppConstant.RECORD_IMAGE_URL, temp.partnerImage)
                }
                listener.openDetails(bundle)
            }
            refBinding.image.clImage.setImageResource(R.drawable.nebula_placeholder)

            Glide
                .with(context)
                .load(data[position].recordImage)
                .placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.IMMEDIATE)
                .into(refBinding.image.clImage)


            refBinding.tvName.text = temp.partnerName
           refBinding.imgUser.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_record_user_place_holder
                )
            )

            if (temp.partnerImage.isNullOrEmpty()) {
                refBinding.imgUser.setImageDrawable(ContextCompat.getDrawable(context,   R.drawable.ic_record_user_place_holder))
            } else {
                refBinding.imgUser.setImageDrawable(ContextCompat.getDrawable(context,   R.drawable.ic_record_user_place_holder))
                val tempUrl = temp.partnerImage.trim()
                Log.e("*******************", tempUrl)
                val options: RequestOptions = RequestOptions()
                    .placeholder(  R.drawable.ic_record_user_place_holder)
                    .error(  R.drawable.ic_record_user_place_holder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)


                Glide.with(context).load(tempUrl).apply(options).into(refBinding.imgUser)
            }
//            CommonUtils.loadImage(context, temp.partnerImage, holder.itemView.img_user)

           refBinding.tvMessage.text =
                CommonUtils.setReferralSpan(context, temp.isRead, temp.message, temp.timePeriod)

            if (!temp.isRead) {
                refBinding.ctlUserReferrals.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.violet_shade_100per
                    )
                )
            } else {
                refBinding.ctlUserReferrals.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimaryDark
                    )
                )
            }
        }
    }

    interface Communicator {
        fun openDetails(bundle: Bundle)

        fun openOther(type: Int)
    }

}