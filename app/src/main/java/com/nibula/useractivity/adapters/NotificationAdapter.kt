package com.nibula.useractivity.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemNotificationDetailsBinding
import com.nibula.response.notifiaction.ResponseCollection
import okhttp3.internal.addHeaderLenient

class NotificationAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding:ItemNotificationDetailsBinding

    class NotificationViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class NotificationHeaderViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    private var context: Context? = null

    private lateinit var layoutInflater: LayoutInflater
    var dataList = ArrayList<ResponseCollection>()
    lateinit var listener: onNotificationListener

    companion object {

        const val NOTIFY_HEADER = 1

        const val NOTIFY_DETAIL = 2

    }

    constructor(context: Context, listener: onNotificationListener) : this() {
        this.context = context
        this.listener = listener
        layoutInflater = LayoutInflater.from(context)
    }

    override fun getItemViewType(position: Int): Int {
        return NOTIFY_DETAIL
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        /*return if (viewType == NOTIFY_HEADER) {
           *//* val view =
                layoutInflater.inflate(R.layout.item_notification_header, parent, false)
            NotificationHeaderViewHolder(view)*//*

            val view =
                layoutInflater.inflate(R.layout.item_notification_details, parent, false)
            NotificationViewHolder(view)
        } else {
            val view =
                layoutInflater.inflate(R.layout.item_notification_details, parent, false)
            NotificationViewHolder(view)
        }*/
        /*val view =
            layoutInflater.inflate(R.layout.item_notification_details, parent, false)
        return NotificationViewHolder(view)*/


        val inflater = LayoutInflater.from(parent.context)
        binding = ItemNotificationDetailsBinding.inflate(inflater, parent, false)
        val holder = NotificationViewHolder(binding.root)
        return holder
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]
        /*  if (holder is NotificationHeaderViewHolder) {
              holder.itemView.tv_header.text = data.headerDate

          } else*/
      binding.tvMessage.text = data.description
        binding.tvOldTime.text = data.timePeriod
        if (data.isRead.not()) {
            binding.ctlNotify.setBackgroundColor(
                ContextCompat.getColor(
                    context!!,
                    R.color.violet_shade_100per
                )
            )
            val tf = ResourcesCompat.getFont(context!!, R.font.pt_sans_bold)
            binding.tvMessage.setTypeface(tf, Typeface.NORMAL)
        } else {
            binding.ctlNotify.setBackgroundColor(
                ContextCompat.getColor(
                    context!!,
                    R.color.colorPrimary
                )
            )
            val tf = ResourcesCompat.getFont(context!!, R.font.pt_sans)
            binding.tvMessage.setTypeface(tf, Typeface.NORMAL)
        }
        binding.ctlNotify.setOnClickListener {
            if (dataList.get(position).isRead) return@setOnClickListener
            dataList.get(position).isRead = true
            notifyDataSetChanged()
            listener?.onNotificationClick(data)
        }

    }

/*    fun setData(data: List<ResponseCollection>) {
        dataList = data as ArrayList<ResponseCollection>
        notifyDataSetChanged()
    }*/

    fun addData(data: List<ResponseCollection>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        dataList.clear()
    }

    public interface onNotificationListener {
        fun onNotificationClick(data: ResponseCollection)
    }


}