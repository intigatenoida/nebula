package com.nibula.useractivity.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.customview.CustomToast
import com.nibula.response.chatHistoryResponse.ResponseCollection
import com.nibula.utils.CommonUtils

import kotlin.collections.ArrayList

class ChatHistoryAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    interface onOpenChatRecordListener {
        fun openChatRecord(recordId: String)
    }

    var chatData = ArrayList<ResponseCollection>()

    class DateViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ReplyViewHolderLeft(val view: View) : RecyclerView.ViewHolder(view)

    class ReplyViewHolderRight(val view: View) : RecyclerView.ViewHolder(view)

    class ReceivedMsgViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class SentMsgViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class StickyViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ChatMsgWithoutTimeViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ChatRecordWithoutTimeViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ReceivedMsgWithoutTimeViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ReceivedRecordWithoutTimeViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    private lateinit var layoutInflater: LayoutInflater
    private lateinit var context: Context
    private lateinit var listener: onOpenChatRecordListener


    companion object {

        const val VIEW_TYPE_DATE = 1

        const val VIEW_TYPE_RECORD_LEFT = 2

        const val VIEW_TYPE_RECEIVED = 3

        const val VIEW_TYPE_SENT = 4

        const val VIEW_TYPE_STICKY = 5

        const val VIEW_TYPE_RECORD_RIGHT = 6

        const val VIEW_CHAT_MESSAGE_WITHOUT_TIME = 7

        const val VIEW_RECORD_WITHOUT_TIME = 8

        const val RECEIVED_CHAT_MESSAGE_WITHOUT_TIME = 9

        const val RECEIVED_RECORD_WITHOUT_TIME = 10

    }

    constructor(context: Context, listener: onOpenChatRecordListener) : this() {
        this.context = context
        layoutInflater = LayoutInflater.from(context)
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return chatData[position].viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_DATE -> {
                val view =
                    layoutInflater.inflate(R.layout.item_date, parent, false)
                DateViewHolder(view)//15 , 5
            }
            VIEW_TYPE_RECORD_LEFT -> {
                val view =
                    layoutInflater.inflate(R.layout.item_reply_message_left, parent, false)
                ReplyViewHolderLeft(view)//5 , 5
            }
            VIEW_TYPE_RECEIVED -> {
                val view =
                    layoutInflater.inflate(R.layout.item_recieved_message, parent, false)
                ReceivedMsgViewHolder(view)//2 , 2
            }
            VIEW_TYPE_RECORD_RIGHT -> {
                val view =
                    layoutInflater.inflate(R.layout.item_reply_message_right, parent, false)
                ReplyViewHolderRight(view)//5 , 5
            }
            VIEW_TYPE_SENT -> {
                val view =
                    layoutInflater.inflate(R.layout.item_send_message, parent, false)
                SentMsgViewHolder(view)//10 , 10
            }
            VIEW_CHAT_MESSAGE_WITHOUT_TIME -> {
                val view =
                    layoutInflater.inflate(R.layout.item_msg_send_without_time, parent, false)
                ChatMsgWithoutTimeViewHolder(view)
            }

            VIEW_RECORD_WITHOUT_TIME -> {
                val view =
                    layoutInflater.inflate(R.layout.item_record_msg_chat, parent, false)
                ChatRecordWithoutTimeViewHolder(view)
            }
            RECEIVED_CHAT_MESSAGE_WITHOUT_TIME -> {
                val view =
                    layoutInflater.inflate(R.layout.item_received_without_time, parent, false)
                ReceivedMsgWithoutTimeViewHolder(view)
            }
            RECEIVED_RECORD_WITHOUT_TIME -> {
                val view =
                    layoutInflater.inflate(R.layout.item_received_record_without_time, parent, false)
                ReceivedRecordWithoutTimeViewHolder(view)
            }

            else -> {
                val view = layoutInflater.inflate(R.layout.item_sticky, parent, false)
                StickyViewHolder(view)//5 , 10
            }
        }
    }

    override fun getItemCount(): Int {
        return chatData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (holder) {
        is DateViewHolder -> {
            //(holder as DateViewHolder).itemView.txtDate.text = chatData[position].message
            (holder as DateViewHolder).itemView.findViewById<TextView>(R.id.txtDate).text = chatData[position].message
        }

        is ChatMsgWithoutTimeViewHolder -> {
            //(holder as ChatMsgWithoutTimeViewHolder).itemView.txtMessageWithoutDate.text = chatData[position].message
            (holder as ChatMsgWithoutTimeViewHolder).itemView.findViewById<TextView>(R.id.txtMessageWithoutDate).text = chatData[position].message
        }
        is ReplyViewHolderLeft -> {
            //(holder as ReplyViewHolderLeft).itemView.tvTimeReply.text=chatData[position].time
            (holder as ReplyViewHolderLeft).itemView.findViewById<TextView>(R.id.tvTimeReply).text=chatData[position].time
            //(holder as ReplyViewHolderLeft).itemView.tv_user_name.text =
            (holder as ReplyViewHolderLeft).itemView.findViewById<TextView>(R.id.tv_user_name).text =
                chatData[position].record.artist ?: ""
            CommonUtils.loadImage(
                context,
                chatData[position].record.artistImage,
                (holder as ReplyViewHolderLeft).itemView.findViewById(R.id.img_user)
            )
            CommonUtils.loadImage(
                context,
                chatData[position].record.recordImage,
                (holder as ReplyViewHolderLeft).itemView.findViewById(R.id.img_album)
            )
            (holder as ReplyViewHolderLeft).itemView.findViewById<ConstraintLayout>(R.id.recordContainerLeft).setOnClickListener {
                if (::listener.isInitialized && !chatData[position].recordId.isNullOrEmpty()){
                    listener.openChatRecord(chatData[position].recordId?: "")
                }else{
                    CustomToast.showToast(context,"Record Id Not Found")
                }
            }
        }

        is ReceivedRecordWithoutTimeViewHolder -> {
//            (holder as ReceivedRecordWithoutTimeViewHolder).itemView.tvTimeReply.text=chatData[position].time
            (holder as ReceivedRecordWithoutTimeViewHolder).itemView.findViewById<TextView>(R.id.tv_user_name).text =
                chatData[position].record.artist ?: ""
            CommonUtils.loadImage(
                context,
                chatData[position].record.artistImage,
                (holder as ReceivedRecordWithoutTimeViewHolder).itemView.findViewById(R.id.img_user)
            )
            CommonUtils.loadImage(
                context,
                chatData[position].record.recordImage,
                (holder as ReceivedRecordWithoutTimeViewHolder).itemView.findViewById(R.id.img_album)
            )
            (holder as ReceivedRecordWithoutTimeViewHolder).itemView.findViewById<ConstraintLayout>(R.id.recordContainerLeft).setOnClickListener {
                if (::listener.isInitialized && !chatData[position].recordId.isNullOrEmpty()){
                    listener.openChatRecord(chatData[position].recordId?: "")
                }else{
                    CustomToast.showToast(context,"Record Id Not Found")
                }
            }
        }



        is ReplyViewHolderRight -> {
            (holder as ReplyViewHolderRight).itemView.findViewById<TextView>(R.id.tvTimeRight).text=chatData[position].time
            (holder as ReplyViewHolderRight).itemView.findViewById<TextView>(R.id.tv_user_name).text =
                chatData[position].record.artist ?: ""
            CommonUtils.loadImage(
                context,
                chatData[position].record.artistImage,
                (holder as ReplyViewHolderRight).itemView.findViewById(R.id.img_user)
            )
            CommonUtils.loadImage(
                context,
                chatData[position].record.recordImage,
                (holder as ReplyViewHolderRight).itemView.findViewById(R.id.img_album)
            )
            (holder as ReplyViewHolderRight).itemView.findViewById<ConstraintLayout>(R.id.recordContainerLeft).setOnClickListener {
                if (::listener.isInitialized && !chatData[position].recordId.isNullOrEmpty()){
                    listener.openChatRecord(chatData[position].recordId?: "")
                }else{
                    CustomToast.showToast(context,"Record Id Not Found")
                }
            }
        }

        is ChatRecordWithoutTimeViewHolder -> {
//            (holder as ReplyViewHolderRight).itemView.tvTimeRight.text=chatData[position].time
            (holder as ChatRecordWithoutTimeViewHolder).itemView.findViewById<TextView>(R.id.tv_user_name).text =
                chatData[position].record.artist ?: ""
            CommonUtils.loadImage(
                context,
                chatData[position].record.artistImage,
                (holder as ChatRecordWithoutTimeViewHolder).itemView.findViewById(R.id.img_user)
            )
            CommonUtils.loadImage(
                context,
                chatData[position].record.recordImage,
                (holder as ChatRecordWithoutTimeViewHolder).itemView.findViewById(R.id.img_album)
            )
            (holder as ChatRecordWithoutTimeViewHolder).itemView.findViewById<ConstraintLayout>(R.id.recordContainerLeft).setOnClickListener {
                if (::listener.isInitialized && !chatData[position].recordId.isNullOrEmpty()){
                    listener.openChatRecord(chatData[position].recordId?: "")
                }else{
                    CustomToast.showToast(context,"Record Id Not Found")
                }
            }
        }

        is ReceivedMsgViewHolder -> {
            (holder as ReceivedMsgViewHolder).itemView.findViewById<TextView>(R.id.tvTimeReplyText).text=chatData[position].time
            (holder as ReceivedMsgViewHolder).itemView.findViewById<TextView>(R.id.txtReply).text =
                chatData[position].message
        }

        is ReceivedMsgWithoutTimeViewHolder -> {
            (holder as ReceivedMsgWithoutTimeViewHolder).itemView.findViewById<TextView>(R.id.txtReplyWithOutTime).text =
                chatData[position].message
        }
        is SentMsgViewHolder -> {
            (holder as SentMsgViewHolder).itemView.findViewById<TextView>(R.id.tvTimeSend).text=chatData[position].time
            (holder as SentMsgViewHolder).itemView.findViewById<TextView>(R.id.txtMessage).text = chatData[position].message
        }
        else -> {
            val itemHolder = holder as StickyViewHolder

        }
    }

    fun addChat(data: ArrayList<ResponseCollection>) {
        chatData.addAll(0, data)
        notifyDataSetChanged()
    }

    fun addSendChat(data: ResponseCollection) {
        chatData.add(data)
        notifyItemInserted(chatData.size)
    }

    fun clearAddChat(data: ArrayList<ResponseCollection>) {
        chatData.clear()
        chatData.addAll(data)
        notifyDataSetChanged()
    }

    fun getFileSize() = chatData.size
    fun getChatInfo() = chatData


}