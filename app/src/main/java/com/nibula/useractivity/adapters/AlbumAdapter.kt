package com.nibula.useractivity.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemAlbumsBinding
import com.nibula.response.chatRecordResponse.ResponseCollection
import com.nibula.utils.CommonUtils


class AlbumAdapter(val context: Context, val listener: onOpenChatHistoryListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var checkedId = "0"
    lateinit var binding:ItemAlbumsBinding

    interface onOpenChatHistoryListener {
        fun openChatHistory(recordId: String, partnerId: String)
    }

    class AlbumViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    val dataList = ArrayList<ResponseCollection>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       /* val view =
            LayoutInflater.from(context).inflate(R.layout.item_albums, parent, false)
        return AlbumViewHolder(view)*/

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemAlbumsBinding.inflate(inflater, parent, false)
        val holder = AlbumViewHolder(binding.root)
        return holder

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]
        if (checkedId.equals(data.recordId)) {
           binding.imgAbums.background =
                ContextCompat.getDrawable(context, R.drawable.pink_border)
           binding.imgAbums.setPadding(3, 3, 3, 3)
        } else {
           binding.imgAbums.background = null
        }
        CommonUtils.loadImage(context, data.recordImage, binding.imgAbums)
        binding.recordContainer.setOnClickListener {
            listener.openChatHistory(data.recordId, data.partnerId)
        }
    }

    fun addData(responseCollection: List<ResponseCollection>) {
        dataList.clear()
        dataList.addAll(responseCollection)
        notifyDataSetChanged()

    }
}