package com.nibula.useractivity.adapters
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nibula.useractivity.ui.NotificationFragment
import com.nibula.useractivity.ui.ReferralsFragment
import com.nibula.utils.SmartFragmentStatePagerAdapter

class UserActivityViewPagerAdapter(fn: FragmentManager) :
    SmartFragmentStatePagerAdapter(fn) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            NotificationFragment.newInstance()

            //ReferralsFragment.newInstance()
        } else {
            NotificationFragment.newInstance()
        }
    }

    override fun getCount(): Int {
        return 1
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}