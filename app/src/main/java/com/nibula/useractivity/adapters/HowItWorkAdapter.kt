package com.nibula.useractivity.adapters

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.nibula.databinding.ItemHowtoworkBinding
import com.nibula.response.HowItWork.ResponseCollection

class HowItWorkAdapter(val context: Context, val dataList: ArrayList<ResponseCollection>) :
    RecyclerView.Adapter<HowItWorkAdapter.ViewHolder>() {

    lateinit var binding: ItemHowtoworkBinding

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemHowtoworkBinding.inflate(inflater, parent, false)
        val holder = ViewHolder(binding.root)
        return holder
    }
    // LayoutInflater.from(context).inflate(R.layout.item_howtowork, parent, false)


    override fun getItemCount() = dataList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = dataList[position]
        if (data.title.isNullOrEmpty()) {
            binding.title.visibility = View.GONE
        } else {
            binding.title.visibility = View.VISIBLE
        }
        binding.title.text = data.title
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.txtHowItWork.text =
                Html.fromHtml(data.content, HtmlCompat.FROM_HTML_MODE_LEGACY)
        } else {
            binding.txtHowItWork.text = Html.fromHtml(data.content)
        }
    }
}