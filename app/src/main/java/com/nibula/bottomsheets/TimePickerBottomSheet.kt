package com.nibula.bottomsheets
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.nibula.databinding.LayoutBottomDatePickerBinding

class TimePickerBottomSheet : BottomSheetDialogFragment {

    private lateinit var rootView: View
    private lateinit var callbacks: DateBottomSheetCommunicator
    private lateinit var binding:LayoutBottomDatePickerBinding

    constructor(callback: DateBottomSheetCommunicator) : super() {
        this.callbacks = callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.layout_bottom_date_picker, container, false)
            binding=LayoutBottomDatePickerBinding.inflate(inflater,container,false)
            rootView=binding.root
        }
        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.tvFixedDone.setOnClickListener {
            
        }
    }
    interface DateBottomSheetCommunicator {

    }
}