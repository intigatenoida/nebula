package com.nibula.bottomsheets

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.databinding.LayoutBottomSheetDeleteBinding
import com.nibula.utils.interfaces.DialogFragmentClicks

class PNBSheetFragment() : BaseBottomSheetFragment() {

    constructor(
        info: String,
        pInfo: String,
        nInfo: String,
        dgFrgListener: DialogFragmentClicks,
        tCInfo: SpannableStringBuilder = SpannableStringBuilder(""),
        isTermVisible: Boolean = false,
        promptId: Int = 0
    ) : this() {
        this.info = info
        this.pInfo = pInfo
        this.nInfo = nInfo
        this.tCInfo = tCInfo
        this.promptId = promptId
        this.dgFrgListener = dgFrgListener
        this.isTermVisible = isTermVisible
    }

    fun setColor(
        infoColor: Int,
        pInfoColor: Int,
        nInfoColor: Int,
        tCInfoColor: Int
    ): PNBSheetFragment {
        this.infoColor = infoColor
        this.pInfoColor = pInfoColor
        this.nInfoColor = nInfoColor
        this.tCInfoColor = tCInfoColor
        return this
    }

    private lateinit var info: String
    private lateinit var pInfo: String
    private lateinit var nInfo: String
    private lateinit var tCInfo: SpannableStringBuilder
    private var isTermVisible: Boolean = false
    private var promptId: Int = 0

    lateinit var dgFrgListener: DialogFragmentClicks

    private lateinit var rootView: View

    private var infoColor: Int = 0
    private var pInfoColor: Int = 0
    private var nInfoColor: Int = 0
    private var tCInfoColor: Int = 0
    private lateinit var binding:LayoutBottomSheetDeleteBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            binding= LayoutBottomSheetDeleteBinding.inflate(inflater,container,false)
            rootView=binding.root
            /*rootView = inflater.inflate(R.layout.layout_bottom_sheet_delete, container, false)*/
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.tvFixedDeleteMessage.text = info
        binding.tvDelete.text = pInfo
        binding.tvCancle.text = nInfo
        binding.tvTermCond.text = tCInfo

        binding.tvFixedDeleteMessage.setTextColor(infoColor)
        binding.tvDelete.setTextColor(pInfoColor)
        binding.tvCancle.setTextColor(nInfoColor)
        binding.tvTermCond.setTextColor(tCInfoColor)

        if (isTermVisible) {
            binding.tvTermCond.visibility = VISIBLE
        } else {
            binding.tvTermCond.visibility = GONE
        }

        binding.tvDelete.setOnClickListener {
            val b = Bundle()
            b.putInt(PROMPT_ID, promptId)
            dgFrgListener.onPositiveButtonClick(this, b,dialog!!)
        }

        binding.tvCancle.setOnClickListener {
            val b = Bundle()
            b.putInt(PROMPT_ID, promptId)
            dgFrgListener.onNegativeButtonClick(this, b,dialog!!)
        }

    }

}