package com.nibula.bottomsheets

import android.os.Bundle
import android.os.CountDownTimer
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.databinding.LayoutPhoneVarificationBinding
import com.nibula.utils.AppConstant


class VerifyEmailBottomSheet(
    private val actionType: Int,
    private val registerValue: String,
    private val listener: OnVerifyOtpListener
) : BaseBottomSheetFragment() {

    interface OnVerifyOtpListener {
        fun onVerifyOtp(emailOtp: String, phoneOtp: String)
        fun resendOTP()
        fun onDismissCalled()
    }

    private lateinit var rootView: View
    private val totalTime: Long = 60 * 1000
    private val timeInterval: Long = 1000
    private var countDownTimer: CountDownTimer? = null
    private lateinit var binding:LayoutPhoneVarificationBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       // rootView = inflater.inflate(R.layout.layout_phone_varification, container, false)
        binding= LayoutPhoneVarificationBinding.inflate(inflater,container,false)
        rootView=binding.root
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.imgClose.setOnClickListener {
            dialog?.dismiss()
        }
        binding.btnVerify.setOnClickListener {
            if (binding.edtOTPEmail.text.toString().trim().length < 6) {
                binding.txtErrorEmail.visibility = View.VISIBLE
                return@setOnClickListener
            }

            binding.txtErrorEmail.visibility = View.INVISIBLE
            when (actionType) {
                AppConstant.ACTION_TYPE_EMAIL -> {//1 for Email
                    listener.onVerifyOtp(binding.edtOTPEmail.text.toString(), "0")
                }
                AppConstant.ACTION_TYPE_PHONE -> {//2 for PhoneNumber
                    listener.onVerifyOtp("0", binding.edtOTPEmail.text.toString())
                }
            }
        }
        binding.txtEmailPhone.text = registerValue
        startCountDownTimer()
        setSpannableResend()
    }

    fun startCountDownTimer() {
        countDownTimer?.cancel()
        countDownTimer = object : CountDownTimer(totalTime, timeInterval) {
            override fun onFinish() {
                binding.txtResendTimer.text = getString(R.string.did_not_get_the_otp_resend_otp)
            }

            override fun onTick(millisUntilFinished: Long) {
                val minute = (millisUntilFinished / 1000) / 60
                val seconds = (millisUntilFinished / 1000) % 60
                binding.txtResendTimer.text = "${getString(R.string.resend_code_in)} $minute:$seconds"
            }

        }.start()
    }

    private fun setSpannableResend() {
        val str = getString(R.string.resend)
        val spannableString =
            SpannableStringBuilder(str)

        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
//                listener.resendOTP()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                ds.color = ContextCompat.getColor(requireContext(), R.color.colorAccent)
            }
        }

        spannableString.setSpan(
            clickableSpan,
            0,
            spannableString.length,
            SpannableString.SPAN_INCLUSIVE_INCLUSIVE
        )
        binding.txtResend.text = spannableString
        binding.txtResend.movementMethod = LinkMovementMethod.getInstance()
    }


    override fun onDestroy() {
        countDownTimer?.cancel()
        listener?.onDismissCalled()
        super.onDestroy()
    }

    fun showError() {
        binding.txtErrorEmail.visibility = View.VISIBLE
    }
}