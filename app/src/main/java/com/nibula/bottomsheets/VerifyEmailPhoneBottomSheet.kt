package com.nibula.bottomsheets

import android.os.Bundle
import android.os.CountDownTimer
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.databinding.LayoutPhoneEmailVerificationBinding
import com.nibula.utils.AppConstant

class VerifyEmailPhoneBottomSheet(
    val email: String,
    val phoneNumber: String,
    private val listener: OnVerifyOtpListener
) : BaseBottomSheetFragment() {


    interface OnVerifyOtpListener {
        fun onVerifyOtp(emailOtp: String, phoneOtp: String)
        fun resendOTPEmail()
        fun resendOTPPhone()
        fun onDismissCalled()
    }


    private lateinit var rootView: View
    private val totalTime: Long = 60 * 1000
    private val timeInterval: Long = 1000
    private var countDownTimerEmail: CountDownTimer? = null
    private var countDownTimerPhone: CountDownTimer? = null
    private lateinit var binding:LayoutPhoneEmailVerificationBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= LayoutPhoneEmailVerificationBinding.inflate(inflater,container,false)
        rootView=binding.root
        //rootView = inflater.inflate(R.layout.layout_phone_email_verification, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.imgClose.setOnClickListener {
            dialog?.dismiss()
        }
        binding.btnVerify.setOnClickListener {
            binding.txtErrorEmail.visibility = View.INVISIBLE
            binding.txtErrorPhone.visibility = View.INVISIBLE
            listener.onVerifyOtp(binding.edtOTPEmail.text.toString(), binding.edtOTPphone.text.toString())

        }

        binding.txtEmailPhone.text = "$email & $phoneNumber"

        startCountDownTimerEmail()
        startCountDownTimerPhone()

    }


    private fun startCountDownTimerEmail() {
        countDownTimerEmail?.cancel()
        countDownTimerEmail = object : CountDownTimer(totalTime, timeInterval) {
            override fun onFinish() {
                setSpannableResendEmail()
//               txtResendTimer.text = getString(R.string.did_not_get_the_otp_resend_otp)
            }

            override fun onTick(millisUntilFinished: Long) {
                val minute = (millisUntilFinished / 1000) / 60
                val seconds = (millisUntilFinished / 1000) % 60
                binding.txtResendTimerEmail.text = "${getString(R.string.resend_code_in)} $minute:$seconds"

            }

        }.start()
    }

    private fun startCountDownTimerPhone() {
        countDownTimerPhone?.cancel()
        countDownTimerPhone = object : CountDownTimer(totalTime, timeInterval) {
            override fun onFinish() {
                setSpannableResendMobile()
//                txtResendTimer.text = getString(R.string.did_not_get_the_otp_resend_otp)
            }

            override fun onTick(millisUntilFinished: Long) {
                val minute = (millisUntilFinished / 1000) / 60
                val seconds = (millisUntilFinished / 1000) % 60

                binding.txtResendTimerMobile.text = "${getString(R.string.resend_code_in)} $minute:$seconds"
            }

        }.start()

    }

    private fun setSpannableResendEmail() {
        val str = getString(R.string.resend)
        val spannableString =
            SpannableStringBuilder(str)

        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                listener.resendOTPEmail()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                ds.color = ContextCompat.getColor(requireContext(), R.color.colorAccent)
            }
        }

        spannableString.setSpan(
            clickableSpan,
            0,
            spannableString.length,
            SpannableString.SPAN_INCLUSIVE_INCLUSIVE
        )
        binding.txtResendTimerEmail.text = spannableString
        binding.txtResendTimerEmail.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun setSpannableResendMobile() {
        val str = getString(R.string.resend)
        val spannableString =
            SpannableStringBuilder(str)

        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                listener.resendOTPPhone()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                ds.color = ContextCompat.getColor(requireContext(), R.color.colorAccent)
            }
        }

        spannableString.setSpan(
            clickableSpan,
            0,
            spannableString.length,
            SpannableString.SPAN_INCLUSIVE_INCLUSIVE
        )
        binding.txtResendTimerMobile.text = spannableString
        binding.txtResendTimerMobile.movementMethod = LinkMovementMethod.getInstance()
    }


    override fun onDestroy() {
        countDownTimerPhone?.cancel()
        countDownTimerEmail?.cancel()
        listener?.onDismissCalled()
        super.onDestroy()
    }

    fun showError(emailStatus: Int, phoneStatus: Int) {
        if (emailStatus == AppConstant.ALREDY_VERIFIED) {
            binding.txtErrorEmail.visibility = View.VISIBLE
            binding.txtErrorEmail.text = getString(R.string.verified_successfully)
            binding.txtErrorEmail.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.green_shade_1_100per
                )
            )
            binding.edtOTPEmail.isEnabled = false
            binding.edtOTPEmail.isFocusable = false
            binding.edtOTPEmail.alpha = 0.5f
            binding.txtResendTimerEmail.visibility = View.INVISIBLE
        } else {
            binding.txtErrorEmail.visibility = View.VISIBLE
        }
        if (phoneStatus == AppConstant.ALREDY_VERIFIED) {
            binding.txtErrorPhone.visibility = View.VISIBLE
            binding.txtErrorPhone.text = getString(R.string.verified_successfully)
            binding.txtErrorPhone.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.green_shade_1_100per
                )
            )
            binding.edtOTPphone.isEnabled = false
            binding.edtOTPphone.isFocusable = false
            binding.edtOTPphone.alpha = 0.5f
            binding.txtResendTimerMobile.visibility = View.INVISIBLE
        } else {
            binding.txtErrorPhone.visibility = View.VISIBLE
        }

    }

    fun restartTimer(emailStatus: Int, phoneStatus: Int) {
        if (emailStatus == AppConstant.CODE_SEND) {
            startCountDownTimerEmail()
        }
        if (phoneStatus == AppConstant.CODE_SEND) {
            startCountDownTimerPhone()
        }
    }
}