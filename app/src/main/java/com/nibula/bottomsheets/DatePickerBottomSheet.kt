package com.nibula.bottomsheets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.nibula.databinding.LayoutBottomDatePickerBinding
import java.util.*

class DatePickerBottomSheet(
    val startCalendar: Calendar,
    val callback: DateBottomSheetCommunicator) :
    BottomSheetDialogFragment() {

    private lateinit var rootView: View

    private var minCalendar: Calendar? = null
    private var maxCalendar: Calendar? = null
    private lateinit var binding:LayoutBottomDatePickerBinding

    fun setRange(min: Calendar?, max: Calendar?) {
        this.minCalendar = min
        this.maxCalendar = max
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            /*rootView = inflater.inflate(R.layout.layout_bottom_date_picker, container, false)*/
            binding=LayoutBottomDatePickerBinding.inflate(inflater,container,false)
            rootView=binding.root
        }
        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.tvFixedDone.setOnClickListener {
            dialog?.dismiss()
        }

        val datePicker =
            DatePicker.OnDateChangedListener { view, year, monthOfYear, dayOfMonth ->
                callback.onDateSelected(year, monthOfYear, dayOfMonth)
            }

        binding.dpPicker.init(
            startCalendar.get(Calendar.YEAR),
            startCalendar.get(Calendar.MONTH),
            startCalendar.get(Calendar.DAY_OF_WEEK),
            datePicker
        )

        if (minCalendar != null) {
            binding.dpPicker.minDate = minCalendar!!.timeInMillis
        }

        if (maxCalendar != null) {
            binding.dpPicker.minDate = maxCalendar!!.timeInMillis
        }
    }


    interface DateBottomSheetCommunicator {
        fun onDateSelected(year: Int, monthOfYear: Int, dayOfMonth: Int)
    }
}