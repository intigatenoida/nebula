package com.nibula.bottomsheets

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.FrameLayout
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.google.android.exoplayer2.util.Util
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.databinding.LayoutBottomSheetPlayerBinding
import com.nibula.utils.AudioPlayerService


class SongPlayBottomSheetFragment() : BaseBottomSheetFragment(), Player.EventListener,
    PlayerNotificationManager.NotificationListener {
    private lateinit var rootView: View
    private var player: ExoPlayer? = null
    private var mService: AudioPlayerService? = null
    private var mBound = false
    private var serviceIntent: Intent? = null
    private lateinit var binding:LayoutBottomSheetPlayerBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            binding= LayoutBottomSheetPlayerBinding.inflate(inflater,container,false)
            //rootView = inflater.inflate(R.layout.layout_bottom_sheet_player, container, false)
            rootView=binding.root
            binding.playerView.useController = true
            binding.playerView.showController()
            binding.playerView.controllerAutoShow = true
            binding.playerView.controllerHideOnTouch = false
            serviceIntent = Intent(requireContext(), AudioPlayerService::class.java)
            initializePlayer()
            prepareSong()
//          rootView.img_play_pause.setOnClickListener {
//
//            }
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)

                behavior.state = BottomSheetBehavior.STATE_EXPANDED
                behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                    override fun onSlide(bottomSheet: View, slideOffset: Float) {
                        // println("slideOffset: $slideOffset")
                    }
                    override fun onStateChanged(bottomSheet: View, newState: Int) {
                        println("newState: $newState")
                        /*if (newState == BottomSheetBehavior.STATE_COLLAPSED ||
                            newState == BottomSheetBehavior.STATE_HALF_EXPANDED ||
                            newState == BottomSheetBehavior.STATE_HIDDEN
                        ) {
                            this@SongPlayBottomSheetFragment.dialog?.dismiss()
                        }*/
                    }
                })
            }
        })
    }

    private fun prepareSong() {
        serviceIntent?.let { Util.startForegroundService(requireContext(), it) }
        initializePlayer()
        if (mBound) {
            unBindService()
        }
        requireContext().bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            val binder = iBinder as AudioPlayerService.LocalBinder
            mService = binder.service
//            mService!!.prepareSong("", artistName, songTitle, recordImagePath)
            mBound = true
            initializePlayer()
        }


        override fun onServiceDisconnected(componentName: ComponentName) {
            mBound = false
        }
    }

    private fun unBindService() {
        if (mBound) {
            requireContext().unbindService(mConnection)
            mBound = false
        }

    }
    private fun initializePlayer() {
        if (mBound) {
            player = mService!!.getplayerInstance()
            binding.playerView.player = player
            binding.playerView.controllerHideOnTouch = false
            player!!.addListener(this)
        }
    }
    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        assignPlayerParams()
    }
    private fun assignPlayerParams() {
        binding.tvSongTitle.text = "Mitski"
        binding.tvPlayerSong.text = "Be the cowboy"
        binding.tvPlayerAlbum.text = "Mitski"

//        rootView.tv_time_left.text = "0:5"
//        rootView.tv_time_right.text = "-0:25"
        binding.imgDrop.setOnClickListener {
            dialog?.dismiss()
        }

//        rootView.slider.onSeekChangeListener = object : OnSeekChangeListener {
//            override fun onSeeking(seekParams: SeekParams?) {
//                if (seekParams == null) {
//                    return
//                }
//
//            }
//
//            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar?) {
//
//            }
//
//            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar?) {
//
//            }
//
//        }

//        rootView.img_share.setOnClickListener {
//            dialog?.dismiss()
////            fg.navigate.pushFragments(ShareFragment.newInstance())
//            //fg.setFragment(ShareFragment.newInstance(), R.id.containerFragment, true)
//        }

    }
}
                      /*val upperState = 0.66
                        val lowerState = 0.33
                        if (behavior.state == BottomSheetBehavior.STATE_SETTLING) {
                            if (slideOffset >= upperState) {
                                behavior.state = BottomSheetBehavior.STATE_EXPANDED
                            }
                            if (slideOffset > lowerState && slideOffset < upperState) {
                                this@SongPlayBottomSheetFragment.dialog?.dismiss()
                                //behavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
                            }
                            if (slideOffset <= lowerState) {
                                this@SongPlayBottomSheetFragment.dialog?.dismiss()
                                //behavior.state = BottomSheetBehavior.STATE_COLLAPSED
                            }
                        }*/