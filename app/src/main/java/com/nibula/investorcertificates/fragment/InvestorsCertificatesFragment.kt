package com.nibula.investorcertificates.fragment

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.certificatepdf.InvestorCertificate
import com.nibula.certificatepdf.NewCertificateActivity
import com.nibula.databinding.CertificatesFragmentBinding
import com.nibula.investorcertificates.adapter.InvestorsCertificatesAdapter
import com.nibula.investorcertificates.modals.InvestorsCertificatesResponse
import com.nibula.request_to_invest.toBitmap
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils

import kotlinx.coroutines.*
import java.net.URL

class InvestorsCertificatesFragment : BaseFragment(), InvestorsCertificatesAdapter.Communicator {

    lateinit var rootView: View
    lateinit var helper: InvestorsCertificatesHelper
    var recordId: String? = null
    var recordTitle: String? = null
    var recordTypeId: Int? =0
    var artistName: String? = null
    var recordImageColorCode: Int = 0x000000
    lateinit var binding:CertificatesFragmentBinding

    val adapter by lazy {
        InvestorsCertificatesAdapter(requireContext(), this)
    }

    companion object {
        fun newInstance(bundle: Bundle?): InvestorsCertificatesFragment {
            val fg = InvestorsCertificatesFragment()
            fg.arguments = bundle
            return fg
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if (!::rootView.isInitialized) {
           // rootView = inflater.inflate(R.layout.certificates_fragment, container, false)
            binding= CertificatesFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
        }
        initUi()
        return rootView
    }

    private fun initUi() {
        recordId = arguments?.getString(AppConstant.ID) ?: ""
        helper = InvestorsCertificatesHelper(this@InvestorsCertificatesFragment)

        // recyclerview
        binding.coownerRecycler.adapter = adapter

        binding.baseHeader.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.baseHeader.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.baseHeader.ivUpload.setOnClickListener {
            loginUploadCheck()
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.ivBack.setOnClickListener {
            navigate.manualBack()
        }
        recordId?.let {
            helper.getDetails(it)
        }
    }

    private fun releaseDateString(date: String): SpannableString {
        val ss1 = SpannableString("${requireContext()?.getString(R.string.drop_date)} $date")
        ss1.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.colorAccent)),
            requireContext().getString(R.string.drop_date).length + 1,
            ss1.length,
            0
        )
        return ss1
    }

    fun updateScreen(data: InvestorsCertificatesResponse.Response?) {
        recordTitle = data?.recordTitle
        artistName = data?.artistName
        recordTypeId=data?.recordTypeId
        with(rootView) {
            binding.artistName.text = "${getString(R.string.artist)}: " + data?.artistName ?: ""
            binding.albumName.text = data?.recordType + ": " + data?.recordTitle ?: ""
            /*  (getString(R.string.amount_invested) + ": ${data?.currencyType}" +data?.totalInvestedAmount?.let {
                  CommonUtils.trimDecimalToTwoPlaces(
                      it
                  )
              }).also { tvEarned.text = it }*/


            (getString(R.string.amount_invested) + ": ${data?.currencyType}" + "0.0").also {
                binding.tvEarned.text = it
            }

            binding.tvReleasedDate.text =
                data?.releaseDate?.let { releaseDateString(it) }

            Glide
                .with(context)
                .load(data!!.recordImage)
                .placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.IMMEDIATE)
                .into(binding.layoutImage.clImage)

            //update investors list
            adapter.updateList(data?.investments)

            setRecordImage(data!!.recordImage!!)

        }
    }

    override fun openCertificate(dataInvestment: InvestorsCertificatesResponse.Response.Investment) {
        /*   startActivity(Intent(requireContext(),InvestorCertificate::class.java).apply {
               putExtra(AppConstant.RECORD_ID,recordId)
               putExtra(AppConstant.ID,data.id)
               putExtra(AppConstant.COLOR_HEXA,recordImageColorCode)
           })*/

        startActivity(Intent(requireContext(), NewCertificateActivity::class.java).apply {
            putExtra(AppConstant.RECORD_ID, recordId)
            putExtra(AppConstant.ID, dataInvestment.id)
            putExtra(AppConstant.RECORD_TITLE, recordTitle)
            putExtra(AppConstant.RECORD_ARTIST_NAME, artistName)
            putExtra(AppConstant.COLOR_HEXA, recordImageColorCode)
            putExtra(AppConstant.RECORD_TYPE_ID, recordTypeId)
        })
    }

    fun setRecordImage(urlImage: String) {
        if (urlImage.isEmpty()) return
        val resizeUrl = "$urlImage?maxwidth=300&maxheight=300&quality=50"
        val urlImage =
            URL(resizeUrl)
        val result: Deferred<Bitmap?> = GlobalScope.async {
            urlImage.toBitmap()
        }
        GlobalScope.launch(Dispatchers.Main) {
            createDarkPaletteAsync(result.await())
        }
    }

    fun createDarkPaletteAsync(bitmap: Bitmap?) {

        try {
            Palette.from(bitmap!!).generate { p ->
                val defaultValue = 0x000000
                //ivRendering?.setBackgroundColor(p!!.getDominantColor(defaultValue))
                Log.d("Color", p!!.getVibrantColor(defaultValue).toString())
                val hexaColor = Integer.toHexString(p.getDominantColor(defaultValue))

                // Default color in case color parse exception found
                var rgbColor: Int
                try {
                    rgbColor = Color.parseColor("#$hexaColor")
                } catch (e: IllegalArgumentException) {
                    rgbColor = Color.parseColor("#000000")
                }

                val halfTransparentColor: Int = adjustAlpha(rgbColor, 0.4f)
                recordImageColorCode = halfTransparentColor
                // cl_main_root.setBackgroundColor(halfTransparentColor)

            }
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }

    }

    @ColorInt
    fun adjustAlpha(@ColorInt color: Int, factor: Float): Int {
        val alpha = Math.round(Color.alpha(color) * factor)
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)
        return Color.argb(alpha, red, green, blue)
    }
}