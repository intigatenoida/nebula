package com.nibula.investorcertificates.fragment

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.request.MyViewCollectorsRequest
import com.nibula.retrofit.*
import com.nibula.response.mycollectorresponse.MyCollecrtorsResponse
import com.nibula.utils.AppConstant


class ViewCollectorsHelper(val fg: ViewCollectorsFragment) : BaseHelperFragment() {

    var investmentNextPage: Int = 1
    var investmentMaxPage: Int =  0
    fun getCollectorsListByRecordId(isProgressDialog: Boolean, recordId: String) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideProcessDialog()
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        fg.isLoading = true

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var viewCollectorsRequest = MyViewCollectorsRequest()
        viewCollectorsRequest.currentPage = investmentNextPage
        viewCollectorsRequest.recordsPerPage = 10
        viewCollectorsRequest.recordId = recordId

        val call = helper.getViewCollectorsList(viewCollectorsRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<MyCollecrtorsResponse>() {
            override fun onSuccess(any: MyCollecrtorsResponse?, message: String) {
                /*Load More*/
                investmentMaxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = investmentNextPage < investmentMaxPage ||
                        (any?.totalRecords ?: 0) > (investmentNextPage * 10)

                /* Data */
                if (any?.responseCollection != null &&
                    any?.responseStatus == 1
                ) {
                    if (!any.responseCollection.isNullOrEmpty()) {
                        fg.binding.rvViewCollectors.visibility = View.VISIBLE
                        if (investmentNextPage == 1) {
                            fg.adapter.updateList(any.responseCollection!!)
                        } else if (investmentNextPage > 1) {
                            fg.adapter.addData(any.responseCollection!!)
                        }
                        investmentNextPage++

                    } else {

                        if (investmentNextPage == 1) {
                            fg.adapter.clearData()
                        }

                        noData()
                    }
                } else {
                    if (investmentNextPage == 1) {
                        fg.adapter.clearData()
                    }
                }
                dismiss(true)
                /*  any?.myInvestmentsTotals?.let {
                      val totalAmt = "Total: ${any?.myInvestmentsTotals ?: "0"}"
                      fg.updateTotalAmount(totalAmt)
                  }*/

            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })

    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        fg.isLoading = false

    }


    private fun noData() {
        if (fg.adapter.list.isNullOrEmpty()) {
            fg.binding.layoutNoMusic.root.visibility = View.VISIBLE
            fg.binding.rvViewCollectors.visibility = View.GONE
        } else {
            fg.binding.layoutNoMusic.root.visibility = View.GONE
            fg.binding.rvViewCollectors.visibility = View.VISIBLE
        }
    }

}