package com.nibula.investorcertificates.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentViewCollectorsBinding
import com.nibula.investorcertificates.adapter.ViewCollectorsAdapter
import com.nibula.utils.AppConstant


class ViewCollectorsFragment : BaseFragment(), Communicator {

    lateinit var rootView: View
    lateinit var helper: ViewCollectorsHelper
    var recordId: String? = null
    var isDataAvailable = false
    var isLoading = true
    lateinit var binding: FragmentViewCollectorsBinding
    val adapter by lazy {
        ViewCollectorsAdapter(requireContext(), this)
    }

    companion object {
        fun newInstance(bundle: Bundle?): ViewCollectorsFragment {
            val fg = ViewCollectorsFragment()
            fg.arguments = bundle
            return fg
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if (!::rootView.isInitialized) {
            // rootView = inflater.inflate(R.layout.fragment_view_collectors, container, false)
            binding = FragmentViewCollectorsBinding.inflate(inflater, container, false)
            rootView = binding.root
        }
        initUi()
        return rootView
    }

    private fun initUi() {
        recordId = arguments?.getString(AppConstant.RECORD_ID) ?: ""
        helper = ViewCollectorsHelper(this)
        setAdapter()
        recordId?.let {
            helper.getCollectorsListByRecordId(true, it)
        }
    }

    private fun setAdapter() {
        val verticalDecoration =
            DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        val verticalDivider =
            ContextCompat.getDrawable(requireContext(), R.drawable.vertical_divider_0_5dp)
        verticalDecoration.setDrawable(verticalDivider!!)
        binding.rvViewCollectors.addItemDecoration(verticalDecoration)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvViewCollectors.layoutManager = layoutManager
        binding.rvViewCollectors.adapter = adapter

        binding.rvViewCollectors.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    recordId?.let {
                        helper.getCollectorsListByRecordId(true, it)
                    }
                }
            }
        })

        binding.swipeViewCollector.setOnRefreshListener {
            helper.investmentNextPage = 1
            helper.investmentMaxPage = 0
            recordId?.let {
                helper.getCollectorsListByRecordId(true, it)
            }
        }
    }


    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.ivBack.setOnClickListener {
            navigate.manualBack()
        }

    }

    override fun onProfileClick(artistId: String, userType: Int) {
        navigate.openTopArtist(artistId, userType)
    }


}

interface Communicator {
    fun onProfileClick(userId: String, userType: Int)
}
