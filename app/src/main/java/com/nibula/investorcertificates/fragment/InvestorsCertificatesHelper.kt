package com.nibula.investorcertificates.fragment

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.investorcertificates.modals.InvestorsCertificatesResponse
import com.nibula.investorcertificates.modals.RecordIdRequest
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError

class InvestorsCertificatesHelper(val fg: InvestorsCertificatesFragment) : BaseHelperFragment() {


    fun getDetails(id: String) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        fg.showProcessDialog()
        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val request = RecordIdRequest(id)
        val observable = helper.requestCertificatesDetails(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<InvestorsCertificatesResponse>() {
            override fun onSuccess(any: InvestorsCertificatesResponse?, message: String?) {
                val response = any as InvestorsCertificatesResponse
                if (response.responseStatus ?: 0 == 1) {
                    fg.binding.imagesection.visibility = View.VISIBLE
                    fg.hideProcessDialog()
                    fg.updateScreen(response.response)

                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }
        }))
    }

    fun getCertificate(investedBy: String?) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }

        fg.showProcessDialog()

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = investedBy?.let { helper.requestCertificate(it) }
        disposables.add(observable?.let { sendApiRequest(it) }!!.subscribeWith(object :
            CallbackWrapper<InvestorsCertificatesResponse>() {
            override fun onSuccess(any: InvestorsCertificatesResponse?, message: String?) {
                val response = any as InvestorsCertificatesResponse
                if (response.responseStatus ?: 0 == 1) {
                    fg.hideProcessDialog()
                    fg.updateScreen(response.response)

                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }
        }))
    }

}