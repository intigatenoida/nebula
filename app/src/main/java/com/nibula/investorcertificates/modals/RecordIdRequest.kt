package com.nibula.investorcertificates.modals


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class RecordIdRequest(
    @SerializedName("RecordId")
    var recordId: String? = ""
)