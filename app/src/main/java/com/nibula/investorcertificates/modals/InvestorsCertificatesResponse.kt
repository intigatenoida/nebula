package com.nibula.investorcertificates.modals
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class InvestorsCertificatesResponse(
    @SerializedName("Response")
    var response: Response? = Response()
):BaseResponse() {
    @Keep
    data class Response(
        @SerializedName("CurrencyType")
        var currencyType: String? = "",
        @SerializedName("CurrencyTypeAbbreviation")
        var currencyTypeAbbreviation: String? = "",
        @SerializedName("Id")
        var id: String? = "",
        @SerializedName("Investments")
        var investments: List<Investment> = listOf(),
        @SerializedName("RecordImage")
        var recordImage: String? = "",
        @SerializedName("RecordStatusTypeId")
        var recordStatusTypeId: Int? = 0,
        @SerializedName("RecordTitle")
        var recordTitle: String? = "",
        @SerializedName("RecordType")
        var recordType: String? = "",
        @SerializedName("RecordTypeId")
        var recordTypeId: Int? = 0,
        @SerializedName("ReleaseDate")
        var releaseDate: String? = "",
        @SerializedName("TotalInvestedAmount")
        var totalInvestedAmount: Double? = 0.0,
        @SerializedName("ArtistName")
        var artistName: String? = ""

    ) {
        @Keep
        data class Investment(
            @SerializedName("Id")
            var id: Int? = 0,
            @SerializedName("InvestedBy")
            var investedBy: String? = "",
            @SerializedName("InvestedOn")
            var investedOn: String? = "",
            @SerializedName("ShowToPublic")
            var showToPublic: Boolean = false
        )
    }
}