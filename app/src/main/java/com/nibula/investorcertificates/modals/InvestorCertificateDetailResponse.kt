package com.nibula.investorcertificates.modals

import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class InvestorCertificateDetailResponse(
    @SerializedName("Response")
    var response: Response? = Response()
) : BaseResponse() {
    @Keep
    data class Response(
        @SerializedName("InvestorName")
        var investorName: String? = "",

        @SerializedName("RecordImage")
        var recordImage: String? = "",

        @SerializedName("RecordTitle")
        var recordTitle: String? = "",

        @SerializedName("SharePercentage")
        var sharePercentage: String? = "",
        @SerializedName("DateOfInvestment")
        var dateOfInvestment: String? = "",
        @SerializedName("ArtistName")
        var artistName: String? = "",

        @SerializedName("InvestorNumber")
        var investorNumber: String? = "",
        @SerializedName("ReferenceCode")
        var referenceCode: String? = "",
        @SerializedName("CountryOfIssue")
        var countryOfIssue: String? = "",
        @SerializedName("QRCodeBase64Data")
        var qRCodeBase64Data: String? = "",

        @SerializedName("Rewards")
        var rewards: String? = "",
        @SerializedName("ISWCCode")
        var iSWCCode: String? = "",

        @SerializedName("RecordId")
        var recordId: String? = "",

        @SerializedName("ArtistId")
        var artistId: String? = "",

        @SerializedName("PdfUrl")
        var pdfUrl: String? = ""
    )
}