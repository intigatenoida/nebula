package com.nibula.investorcertificates.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.nibula.R
import com.nibula.databinding.ItemCollectorViewBinding
import com.nibula.investorcertificates.fragment.Communicator
import com.nibula.response.myinvestmentresponse.ResponseCollection
import com.nibula.utils.CommonUtils

class ViewCollectorsAdapter(val context: Context, val communicator: Communicator) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var list = mutableListOf<ResponseCollection>()
    lateinit var binding: ItemCollectorViewBinding

    class DataHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding= ItemCollectorViewBinding.inflate(inflater, parent, false)
        var holder = DataHolder(binding.root)
        return holder
    } /*= DataHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_collector_view, parent, false)
    )
*/

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]

       binding.investedBy.text = data.investorName
     binding.investedByUserName.text = data.investorUserName
        binding.tvInvested.text = "${data?.shares?.toInt()}/${data?.TotalShares?.toInt()}"

        if (!data.investorImage.isNullOrEmpty()) {
            CommonUtils.loadImage(
                context,
                data.investorImage ?: "",
               binding.ivInvestor,
                R.drawable.ic_record_user_place_holder
            )
        }

        binding.ivInvestor.setOnClickListener {
            communicator.onProfileClick(data.investorId.toString() ?: "", 4)
        }
    }



    override fun getItemCount() = list.size


    fun updateList(viewCollectorslist: MutableList<ResponseCollection>?) {
        viewCollectorslist?.let {
            list = it
        }
        notifyDataSetChanged()
    }


    fun addData(viewCollectorslist: MutableList<ResponseCollection>?) {
        this.list.addAll(viewCollectorslist!!)
        notifyDataSetChanged()
    }

    fun clearData() {
        list.clear()
    }

}