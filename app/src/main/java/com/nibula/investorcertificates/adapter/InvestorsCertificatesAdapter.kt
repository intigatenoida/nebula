package com.nibula.investorcertificates.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.nibula.databinding.ItemCoownershipFragmentBinding
import com.nibula.investorcertificates.modals.InvestorsCertificatesResponse

class InvestorsCertificatesAdapter(val context: Context, val communicator: Communicator) :
    RecyclerView.Adapter<ViewHolder>() {
    lateinit var binding: ItemCoownershipFragmentBinding

    var list: List<InvestorsCertificatesResponse.Response.Investment> = listOf()

    class DataHolder(itemView: View) : ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        /*  val view = LayoutInflater.from(context).inflate(
              R.layout.item_my_investment,
              parent,
              false
          )*/
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemCoownershipFragmentBinding.inflate(inflater, parent, false)
        val holder = DataHolder(binding.root)

        return holder

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]

        binding.shareCertificateTitle1.text = "Token #${position + 1} Certificate"

        binding.forwardImageEnable.visibility =
            if (data.showToPublic) View.VISIBLE else View.INVISIBLE
        binding.forwardImageDisable.visibility =
            if (data.showToPublic) View.INVISIBLE else View.VISIBLE

        holder.itemView.setOnClickListener {
            if (data.showToPublic) communicator.openCertificate(data)
        }
    }


    override fun getItemCount() = list.size


    fun updateList(investments: List<InvestorsCertificatesResponse.Response.Investment>?) {
        investments?.let {
            list = it
        }
        notifyDataSetChanged()
    }

    interface Communicator {

        fun openCertificate(data: InvestorsCertificatesResponse.Response.Investment)
    }
}