package com.nibula.pending_request

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemInvestorBinding
import com.nibula.response.details.investments.MyInvestment
import com.nibula.utils.CommonUtils

class PendingInvestorAdapter(val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

lateinit var binding:ItemInvestorBinding
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    var usersList: MutableList<MyInvestment> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemInvestorBinding.inflate(inflater, parent, false)

        val holder = ViewHolder(binding.root)

        return holder
      /*  return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_investor,
                parent,
                false
            )
        )*/
    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val data = usersList[position]

//      val color = ContextCompat.getColor(context, R.color.green_shade_1_100per)

        //--> image
        if (!data.investorImage.isNullOrEmpty()) {
            CommonUtils.loadImage(
                context,
                data.investorImage ?: "",
                binding.ivInvestor,
                R.drawable.ic_album_nebula
            )
        }
        //--> Investment str
        data.amount?.let { it ->
            val amount =
                "$${CommonUtils.trimDecimalToTwoPlaces(it)} ${context.getString(R.string.invested)}"
            binding.tvInvested.text =
                CommonUtils.setSpannableGreen(context,amount, 0, amount.split(" ")[0].length)
        }

        //--> Investor
        data.investorName?.let {
            val sharesPurchased = data.shares
            val txtRoyalty = if (sharesPurchased!! > 1) {
                context.getString(R.string.royalty_shares)
            } else {
                context.getString(R.string.royalty_share)
            }
            binding.investedBy.text =
                "$it ${context.getString(R.string.purchased)} $sharesPurchased $txtRoyalty"
        }
    }

    fun updateList(list: MutableList<MyInvestment>) {
        usersList.addAll(list)
        notifyDataSetChanged()
    }

}