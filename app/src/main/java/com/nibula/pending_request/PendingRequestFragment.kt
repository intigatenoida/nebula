package com.nibula.pending_request

import android.app.Dialog
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.FragmentPendingRequestBinding
import com.nibula.request.AcceptAllInvestor
import com.nibula.request.AcceptInvestor
import com.nibula.response.AcceptInvestorResponse
import com.nibula.response.BaseResponse
import com.nibula.response.investerResponse.ArtistInvestor
import com.nibula.response.investerResponse.InvesterResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.interfaces.DialogFragmentClicks



class PendingRequestFragment : BaseFragment(), PendingRequestAdapter.onClickItem,
    DialogFragmentClicks {

    companion object {
        fun newInstance() = PendingRequestFragment()
    }

    private val dataList = ArrayList<ArtistInvestor>()
    private val itemPerPage = 10
    var selectionType = -1
    private val acceptAll = 1
    private val accept = 2
    private val reject = 3
    var selectedId = -1
    var recordId = ""
    var currentPage = 1
    private lateinit var adapter: PendingRequestAdapter
    lateinit var root: View
    lateinit var binding:FragmentPendingRequestBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       // root = inflater.inflate(R.layout.fragment_pending_request, null)
        binding= FragmentPendingRequestBinding.inflate(inflater,container,false)
        root=binding.root
        recordId = arguments?.getString(AppConstant.ID, "") ?: ""
        binding.header.tvTitle.text = getString(R.string.p_requeust)
        adapter = PendingRequestAdapter(requireActivity(), this, dataList)
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.rvPendingRequest.adapter = adapter
        binding.rvPendingRequest.layoutManager = layoutManager
        binding.rvPendingRequest.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager.findLastCompletelyVisibleItemPosition() == ((itemPerPage * currentPage) - 1)) {
                    currentPage++
                    getAllRequest()
                }
            }
        })

        binding.investSwipe.setOnRefreshListener {
            currentPage = 1
            getAllRequest()
        }

        binding.header.ivBack.setOnClickListener {
            navigate.manualBack()
        }

        binding.baseHeader.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.baseHeader.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.baseHeader.ivUpload.setOnClickListener {
            loginUploadCheck()
        }

        manageAcceptAllButton()
        dataList.clear()
        getAllRequest()
        return root
    }

    private fun manageAcceptAllButton() {
        if (recordId.isEmpty()) {
            binding.tvAcceptAll.visibility = View.GONE
        } else {
            if (dataList.isEmpty()) {
                binding.tvAcceptAll.visibility = View.GONE
            } else {
                binding.tvAcceptAll.visibility = View.VISIBLE
                val color = ContextCompat.getColor(requireContext(), R.color.green_shade_1_100per)
                binding.tvAcceptAll.text =
                    CommonUtils.underlineSpannable(binding.tvAcceptAll.text.toString(), color)
                binding.tvAcceptAll.setOnClickListener {
                    selectionType = acceptAll
                    val cl = ContextCompat.getColor(requireContext(), R.color.gray)
                    val terms = SpannableStringBuilder("${getString(R.string.term_con)} ")
                    terms.append(CommonUtils.underlineSpannable("T&C", cl))
                        .append(CommonUtils.underlineSpannable(" and ", cl, false))
                        .append(CommonUtils.underlineSpannable("Privacy Policy", cl))

                    val bPrompt = PNBSheetFragment(
                        getString(R.string.message_accept_all),
                        getString(R.string.accept_all),
                        getString(R.string.cancel),
                        this@PendingRequestFragment,
                        terms,
                        true
                    )
                    bPrompt.isCancelable = false
                    bPrompt.setColor(
                        ContextCompat.getColor(requireContext(), R.color.white),
                        ContextCompat.getColor(requireContext(), R.color.green_shade_1_100per),
                        ContextCompat.getColor(requireContext(), R.color.gray_1_51per),
                        ContextCompat.getColor(requireContext(), R.color.gray)
                    )
                    bPrompt.show(
                        this@PendingRequestFragment.childFragmentManager,
                        "PositiveNegativeBottomSheetFragment"
                    )
                }
            }
        }
    }

    private fun getAllRequest() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        if (!binding.investSwipe.isRefreshing) {
            showProcessDialog()
        }
        val helper = ApiClient.getClientInvestor(requireContext()).create(ApiAuthHelper::class.java)
        val call = helper.getAllInvestorList(recordId, currentPage)
        call.enqueue(object : CallBackManager<InvesterResponse>() {
            override fun onFailure(message: String) {
                hideProcessDialog()
                hideRefreshLayout()
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
                hideRefreshLayout()
                if (error.kind == RetroError.Kind.API_CALL_CANCEL) {

                } else {

                }
            }

            override fun onSuccess(any: InvesterResponse?, message: String) {
                hideProcessDialog()
                val res = any as InvesterResponse
                if (binding.investSwipe.isRefreshing) {
                    dataList.clear()
                }
                res?.artistInvestors?.let { dataList.addAll(it) }
                checkNoDataFound()
                hideRefreshLayout()
            }
        })
    }

    private fun checkNoDataFound() {
        if (dataList.isEmpty()) {
            binding.noDataFoundTv.root.visibility = View.VISIBLE
            binding.noDataFoundTv.tvMessage.text = getString(R.string.no_pending_requests)
            binding.rvPendingRequest.visibility = View.GONE
        } else {
            binding.noDataFoundTv.root.visibility = View.GONE
            binding.rvPendingRequest.visibility = View.VISIBLE
            adapter.notifyDataSetChanged()
        }
        manageAcceptAllButton()
    }

    private fun checkNoDataFound(position: Int) {
        if (dataList.isEmpty()) {
            binding.noDataFoundTv.root.visibility = View.VISIBLE
            binding.noDataFoundTv.tvMessage.text = getString(R.string.no_pending_requests)
            binding.rvPendingRequest.visibility = View.GONE
        } else {
            binding.noDataFoundTv.root.visibility = View.GONE
            binding.rvPendingRequest.visibility = View.VISIBLE
            adapter.notifyItemRemoved(position)
        }
        manageAcceptAllButton()
    }


    private fun hideRefreshLayout() {
        if (binding.investSwipe.isRefreshing) {
            binding.investSwipe.isRefreshing = false
        }
    }

    override fun onClickAccept(id: Int) {
        selectedId = id
        selectionType = accept
        val cl = ContextCompat.getColor(requireContext(), R.color.gray)
        val terms = SpannableStringBuilder("${getString(R.string.term_con)} ")
        terms.append(CommonUtils.underlineSpannable("T&C", cl))
            .append(CommonUtils.underlineSpannable(" and ", cl, false))
            .append(CommonUtils.underlineSpannable("Privacy Policy", cl))

        val bPrompt = PNBSheetFragment(
            getString(R.string.are_u_sure),
            getString(R.string.accept),
            getString(R.string.cancel),
            this@PendingRequestFragment,
            terms,
            true
        )
        bPrompt.isCancelable = false
        bPrompt.setColor(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.green_shade_1_100per),
            ContextCompat.getColor(requireContext(), R.color.gray_1_51per),
            ContextCompat.getColor(requireContext(), R.color.gray)
        )
        bPrompt.show(
            this@PendingRequestFragment.childFragmentManager,
            "PositiveNegativeBottomSheetFragment"
        )
    }

    override fun onclickReject(id: Int) {
        selectedId = id
        selectionType = reject
        val bPrompt = PNBSheetFragment(
            getString(R.string.message_reject),
            getString(R.string.reject),
            getString(R.string.cancel),
            this@PendingRequestFragment,
            SpannableStringBuilder(""),
            false
        )
        bPrompt.isCancelable = false
        bPrompt.setColor(
            ContextCompat.getColor(requireContext(), R.color.colorAccent),
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.gray_1_51per),
            ContextCompat.getColor(requireContext(), R.color.gray)
        )
        bPrompt.show(
            this@PendingRequestFragment.childFragmentManager,
            "PositiveNegativeBottomSheetFragment"
        )
    }

    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        dialog.dismiss()
        when (selectionType) {
            acceptAll -> {
                acceptAllAPI()
            }
            accept -> {
                acceptApi(true)
            }
            reject -> {
                acceptApi(false)
            }
        }
    }

    private fun acceptApi(isAccept: Boolean) {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClientMusic(requireContext()).create(ApiAuthHelper::class.java)
        val request = AcceptInvestor(
            selectedId,
            if (isAccept) 2 else 3
        )// (InvestmentRequestStatusId-  2 for approve and 3 for reject)
        val call = helper.acceptInvestment(request)
        call.enqueue(object : CallBackManager<AcceptInvestorResponse>() {
            override fun onFailure(message: String) {
                hideProcessDialog()
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
                if (error.kind == RetroError.Kind.API_CALL_CANCEL) {

                } else {

                }
            }

            override fun onSuccess(any: AcceptInvestorResponse?, message: String) {
                hideProcessDialog()
                val res = any as AcceptInvestorResponse
                if ((res.responseStatus ?: 0) == 1) {
                    for (i in 0..dataList.size) {
                        if (dataList[i].id == selectedId) {
                            dataList.removeAt(i)
                            checkNoDataFound(i)
                            break
                        }
                    }
                    if (dataList.isEmpty()) {

                    }
                    selectedId = -1
                }
            }
        })
    }

    private fun acceptAllAPI() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClientMusic(requireContext()).create(ApiAuthHelper::class.java)
        val list = adapter.getList()
        var idStr = ""
        for (i in 0 until list.size) {
            if (idStr.isEmpty()) {
                idStr += list[i].id
            } else {
                idStr += "," + list[i].id
            }
        }
        val request = AcceptAllInvestor(idStr)
        val call = helper.acceptAllInvestment(request)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onFailure(message: String) {
                hideProcessDialog()
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
                if (error.kind == RetroError.Kind.API_CALL_CANCEL) {

                } else {

                }
            }

            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDialog()
                val res = any as BaseResponse
                currentPage = 1
                binding.investSwipe.isRefreshing = true
                getAllRequest()

            }
        })
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        dialog.dismiss()
    }

    override fun onResume() {
        super.onResume()
        if (binding.baseHeader.imgNotificationCount.visibility!=View.VISIBLE){
            CommonUtils.setNotificationDot(requireContext(), binding.baseHeader.imgNotificationCount)
        }
    }
}