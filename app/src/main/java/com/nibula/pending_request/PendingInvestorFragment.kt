package com.nibula.pending_request


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.FragmentInvestorBinding
import com.nibula.response.details.AlbumDetailResponse
import com.nibula.response.details.RecordDetail
import com.nibula.response.details.investments.InvestorList
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils


class PendingInvestorFragment : BaseFragment() {

    companion object {
        fun newInstance() = PendingInvestorFragment()
    }

    lateinit var albumResponse: AlbumDetailResponse
    lateinit var root: View
    lateinit var adapter: PendingInvestorAdapter

    var currentPageCount = 1
    var title: String = ""
    var recordId = ""
    lateinit var binding: FragmentInvestorBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //root = inflater.inflate(R.layout.fragment_investor, null)

        binding = FragmentInvestorBinding.inflate(inflater, container, false)
        root = binding.root

        //--> Album Response
        albumResponse =
            requireArguments().getParcelable<RecordDetail>(AppConstant.RECORD_DETAIL)!! as AlbumDetailResponse

        //--> ALbum Title
        albumResponse.response!!.recordTitle.let {
            if (it != null) {
                title = it.trim() ?: ""
                binding.header.tvTitle.text = "${getString(R.string.investor_in)} $title"
            }
        }

        //--> Record Id
        recordId = albumResponse.response!!.id!!

        //--> Init Adapter
        setInvestorAdapter()

        //--> API Calling
        apiCallInvestorList(true)


        //--> Title
        binding.header.tvTitle.text = CommonUtils.setSpannable(
            requireContext(),
            binding.header.tvTitle.text.toString(),
            0,
            binding.header.tvTitle.text.toString().split(":")[0].length
        )

        binding.header.ivBack.setOnClickListener {
            try {
                navigate.manualBack()
            } catch (e: Exception) {
            }
            //activity?.supportFragmentManager?.popBackStack()
        }
        return root
    }

    private fun setInvestorAdapter() {
        adapter = PendingInvestorAdapter(requireActivity())
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.rvPendingRequest.adapter = adapter
        binding.rvPendingRequest.layoutManager = layoutManager
        binding.rvPendingRequest.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager.findLastCompletelyVisibleItemPosition() == 10) {
                    currentPageCount++
                    apiCallInvestorList(false)
                }
            }
        })
    }

    private fun apiCallInvestorList(isProgressShow: Boolean) {
        if (!isOnline(requireActivity())) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        if (isProgressShow) {
            showProcessDialog()
        }
        val helper = ApiClient.getClientInvestor(requireContext()).create(ApiAuthHelper::class.java)
        val call = helper.getInvestorList(recordId, currentPageCount)
        call.enqueue(object : CallBackManager<InvestorList>() {
            override fun onSuccess(any: InvestorList?, message: String) {
                hideProcessDialog()
                val investorList = any as InvestorList
                investorList.responseStatus.let { status ->
                    when (status) {
                        1 -> {
                            if (investorList.myInvestments?.isNotEmpty()!!) {
                                investorList.myInvestments?.let { adapter.updateList(it) }
                            } else {
                                if (currentPageCount == 1) {
                                    binding.incNoData.root.visibility = View.VISIBLE
                                    binding.incNoData.tvMessage.text = getString(R.string.no_investors)

                                } else {
                                    binding.incNoData.root.visibility = View.GONE
                                }
                            }
                        }
                        0 -> {
                            if (currentPageCount == 1) {
                                showToast(
                                    activity!!,
                                    investorList.responseMessage ?: "",
                                    CustomToast.ToastType.FAILED
                                )
                            } else {

                            }
                        }
                        2 -> {
                            if (currentPageCount == 1) {
                                binding.incNoData.root.visibility = View.VISIBLE
                                binding.incNoData.tvMessage.text = getString(R.string.no_investors)
                            } else {
                                binding.incNoData.root.visibility = View.GONE
                            }
                        }
                        else -> {

                        }
                    }
                }
            }

            override fun onFailure(message: String) {
                hideProcessDialog()
                if (currentPageCount == 1)
                    showToast(activity!!, message ?: "", CustomToast.ToastType.FAILED)
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
                if (currentPageCount == 1)
                    showToast(activity!!, error.errorMessage ?: "", CustomToast.ToastType.FAILED)
            }
        })
    }


}