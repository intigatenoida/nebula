package com.nibula.pending_request


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemPendingRequestBinding
import com.nibula.response.investerResponse.ArtistInvestor
import com.nibula.utils.CommonUtils

class PendingRequestAdapter(
    val context: Context,
    val onClick: onClickItem,
    val dataList: ArrayList<ArtistInvestor>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding:ItemPendingRequestBinding
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
         binding = ItemPendingRequestBinding.inflate(inflater, parent, false)


        val holder = ViewHolder(binding.root)
        return holder
       /* return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_pending_request,
                parent,
                false
            )
        )*/
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]
        val color = ContextCompat.getColor(context, R.color.green_shade_1_100per)
        binding.tvAccept.text =
            CommonUtils.underlineSpannable(binding.tvAccept.text.toString(), color)
        val amountStr =  "${data.currencyType} ${CommonUtils.trimDecimalToTwoPlaces(data.totalRequestedAmount)}"
        val amount = "$amountStr ${context.getString(R.string.pending)}"
        binding.tvPendingamount.text = CommonUtils.setSpannableGreen(context,
            amount,
            0,
            amountStr.length
        )
        var detail = "${context.getString(R.string.wants_to_purchase_shares)}"
        var tempCount = if (data.requestedShares > 1) {
            context.getString(R.string.shares)
        } else {
            context.getString(R.string.share)
        }
        detail = detail.replace("#", data.investorName)
        detail = detail.replace(
            "$",
            "${data.requestedShares} $tempCount"
        )
        detail = detail.replace("*", "'${data.recordTitle}'")
        binding.detail.text = detail
        CommonUtils.loadImage(context, data.recordImage, binding.ivart)
        CommonUtils.loadImage(context, data.investorImage, binding.iv,R.drawable.ic_profile)
        binding.tvAccept.setOnClickListener {
            onClick.onClickAccept(data.id)
        }


        binding.tvReject.setOnClickListener {
            onClick.onclickReject(data.id)
        }

    }


    fun getList(): ArrayList<ArtistInvestor> {
        return dataList
    }

    interface onClickItem {
        fun onClickAccept(id: Int)
        fun onclickReject(id: Int)
    }
}