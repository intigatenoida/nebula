package com.nibula.search

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.globalsearchResponse.GlobalSearchResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.search.ui.SearchFragment


class SearchHelper(val fg: SearchFragment) : BaseHelperFragment() {

    fun getSearchData(
        requestType: Int,
        searchText: String,
        currentPage: Int,
        clearOldData: Boolean
    ) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss()
            return
        }

//        if (!fg.rootView.spSearch.isRefreshing
//        ) {
//            fg.showProcessDialog()
//        }

        fg.isLoading = true
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.globalSearch(requestType, searchText, currentPage)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<GlobalSearchResponse>() {

            override fun onSuccess(any: GlobalSearchResponse?, message: String?) {
                if (clearOldData) {
                    fg.adapter.apply {
                        if (any?.result?.isNullOrEmpty() == true) {
                            clearData()
                        } else {
                            any?.result?.let {
                                clearAndAddData(it)
                               // fg.hideKeyboard()
                                fg.updateRecentUpdate(searchText)
                                fg.binding.rvSearches?.scrollToPosition(0)
                            }
                        }
                    }
                    fg.isDataAvailable = (fg.adapter.searchData.size % 10 == 0)
                    fg.currentPage++
                } else {
                    any?.result?.let {
                        if (it.isNotEmpty()) {
                            fg.adapter.addData(it)
                            fg.isDataAvailable = (fg.adapter.searchData.size % 10 == 0)
                            fg.currentPage++
                        } else {
                            fg.isDataAvailable = false
                        }
                        fg.updateRecentUpdate(searchText)
                    }
                }
                fg.isLoading = false
                fg.updateTabs(any)
                dismiss()
            }

            override fun onError(error: RetroError?) {
                fg.isLoading = false
                dismiss()
            }

        }))

    }


    private fun dismiss() {
        if (fg.binding.spSearch.isRefreshing) {
            fg.binding.spSearch.isRefreshing = false
        }
        fg.hideProcessDialog()
        fg.hideKeyboard()
        noData()
    }

    private fun noData() {
        fg.binding.gdRecentSearches.visibility = View.GONE
        if (fg.adapter.searchData.isEmpty()) {
            fg.binding.gdSearches.visibility = View.GONE
            fg.binding.layoutNoDataSearch.tvMessage.text = "No results found"
            fg.binding.layoutNoDataSearch.noData.visibility = View.VISIBLE
            fg.binding.ctbSearchFilter.visibility = View.VISIBLE
        } else {
            fg.binding.gdSearches.visibility = View.VISIBLE
            fg.binding.ctbSearchFilter.visibility = View.VISIBLE
            fg.binding.layoutNoDataSearch.tvMessage.text = ""
            fg.binding.layoutNoDataSearch.noData.visibility = View.GONE
        }
    }
}