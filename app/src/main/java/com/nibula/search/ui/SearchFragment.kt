package com.nibula.search.ui

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.nibula.DBHandler.NebulaDBHelper
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentSearchBinding
import com.nibula.response.globalsearchResponse.GlobalSearchResponse
import com.nibula.search.SearchHelper
import com.nibula.search.adapters.RecentSearchAdapter
import com.nibula.search.adapters.SearchAdapter
import com.nibula.utils.CommonUtils

import java.util.*


class SearchFragment : BaseFragment(), RecentSearchAdapter.onRecentSearchClickListener {

    companion object {
        fun newInstance() = SearchFragment()
    }

    var isLoading = false
    var isDataAvailable = true
    var requestType = 1
    var currentPage = 1
    private val recentSearchList = ArrayList<String>()
    private lateinit var dbHelper: NebulaDBHelper
    lateinit var rootView: View
    lateinit var adapter: SearchAdapter
    private lateinit var recentAdapter: RecentSearchAdapter
    private lateinit var helper: SearchHelper
    lateinit var timer: Timer
    lateinit var binding:FragmentSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //binding = inflater.inflate(R.layout.fragment_search, container, false)
            
            binding= FragmentSearchBinding.inflate(inflater,container,false)
            rootView=binding.root
           // showKeyboard(requireContext())
            dbHelper = NebulaDBHelper(requireContext())
            recentSearchList.addAll(dbHelper.searchData)
            helper = SearchHelper(this)

            intiUI()

        }
        return rootView
    }


    private fun intiUI() {

        prepareFilterTab()
        binding.edSearchUser.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val s = binding.edSearchUser.text
                if (s != null &&
                    s.toString().trim().isNotEmpty()
                ) {
                    currentPage = 1
                    helper.getSearchData(requestType, s.toString().trim(), currentPage, true)
                } else {
                    binding.layoutNoDataSearch.noData.visibility = GONE
                    binding.gdRecentSearches.visibility = VISIBLE
                    binding.gdSearches.visibility = GONE
                    binding.ctbSearchFilter.visibility = GONE
                }
//                hideKeyboard()
            }
            true
        }
        binding.edSearchUser.addTextChangedListener(object : TextWatcher {
            private val DELAY: Long = 1500 // Milliseconds to delay api call
            override fun afterTextChanged(s: Editable?) {
                timer = Timer()
                timer.schedule(
                    object : TimerTask() {
                        override fun run() {
                            hitSearchAPi(s)
                        }
                    },

                    DELAY
                )
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (::timer.isInitialized) {
                    timer.cancel();
                }
            }

        })
        binding.imgClose.setOnClickListener {
            binding.edSearchUser.setText("")
        }

        binding.edSearchUser.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                true
            } else {
                false
            }
        })

        initTopHeader()
        setSearchAdapter()
        setRecentSearchAdapter()
        if (recentSearchList.isEmpty()) {
            binding.edSearchUser.requestFocus()
            binding.gdRecentSearches.visibility = GONE
        }

        binding.spSearch.setOnRefreshListener {
            if (binding.edSearchUser.text.toString().trim().isEmpty()) {
                binding.spSearch.isRefreshing = false
            } else {
                currentPage = 1
                helper.getSearchData(
                    requestType,
                    binding.edSearchUser.text.toString().trim(),
                    currentPage,
                    true
                )
            }
        }

    }

    private fun hitSearchAPi(s: Editable?) {
        activity?.runOnUiThread {
            if (s != null &&
                s.toString().isNotEmpty()
            ) {
                binding.imgClose.visibility = VISIBLE
                if (s.toString().trim().length >= 1) {
                    currentPage = 1
                    dbHelper.insertSearchData(s.toString().trim())
                    if (!recentSearchList.contains(s.toString().trim())) {
                        recentSearchList.add(s.toString().trim())
                        recentAdapter.addData(s.toString().trim())
                    }
                    CommonUtils.triggerSearchEvent(s?.toString())
                    helper.getSearchData(requestType, s.toString().trim(), currentPage, true)

                }
            } else {
                if (recentSearchList.isEmpty()) {
                    binding.gdRecentSearches.visibility = GONE
                } else {
                    binding.gdRecentSearches.visibility = VISIBLE
                }
                recentSearchList.clear()
                binding.layoutNoDataSearch.noData.visibility = GONE
                binding.gdSearches.visibility = GONE
                binding.ctbSearchFilter.visibility = GONE
                binding.imgClose.visibility = GONE
            }
        }
    }

    private fun initTopHeader() {
        binding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }
    }

    private fun setRecentSearchAdapter() {
        /*val verticalDecoration =
           DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
           val verticalDivider =
            ContextCompat.getDrawable(requireContext(), R.drawable.vertical_divider_o_5dp_15padding)
              verticalDecoration.setDrawable(verticalDivider!!)
              binding.rv_recent_searches.addItemDecoration(verticalDecoration)*/

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvRecentSearches.layoutManager = layoutManager
        recentAdapter = RecentSearchAdapter(requireContext(), this)
        binding.rvRecentSearches.adapter = recentAdapter
        recentAdapter.addData(recentSearchList)

    }

    private fun setSearchAdapter() {

        val verticalDecoration =
            DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        val verticalDivider =
            ContextCompat.getDrawable(requireContext(), R.drawable.vertical_divider_0_5dp)
        verticalDecoration.setDrawable(verticalDivider!!)
        binding.rvSearches.addItemDecoration(verticalDecoration)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvSearches.layoutManager = layoutManager

        adapter = SearchAdapter(requireContext(), navigate)
        binding.rvSearches.adapter = adapter

        binding.rvSearches.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastCompletelyVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    lastVisibleItem == (totalItem - 1)
                ) {
                    helper.getSearchData(
                        requestType,
                        binding.edSearchUser.text.toString().trim(),
                        currentPage,
                        false
                    )
                }
            }
        })

    }

    private fun prepareFilterTab() {
        binding.ctbSearchFilter.addTab(addTab("All", true))
        binding.ctbSearchFilter.addTab(addTab("Music", false))
        binding.ctbSearchFilter.addTab(addTab("Users", false))
        /* binding.ctb_search_filter.addTab(addTab("Friends", false))*/

        binding.ctbSearchFilter.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
                changeTab(p0, false)
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                changeTab(p0, true)
                when (p0?.position ?: 0) {
                    0 -> requestType = 1
                    1 -> requestType = 2
                    2 -> requestType = 3
                    /*  3 -> requestType = 3*/
                }
                currentPage = 1
                helper.getSearchData(
                    requestType,
                    binding.edSearchUser.text.toString().trim(),
                    currentPage,
                    true
                )
            }
        })

        wrapTabIndicatorToTitle(binding.ctbSearchFilter, 0, 0)
    }

    private fun addTab(info: String, isSelected: Boolean): TabLayout.Tab {
        val tab = binding.ctbSearchFilter.newTab()

        val customTabView = LayoutInflater.from(requireContext())
            .inflate(R.layout.layout_tab_search_filter, null)

        val tvName = customTabView.findViewById<AppCompatTextView>(R.id.tv_name)

        if (isSelected) {
            tvName.setBackgroundResource(R.drawable.button_rounded_boarder_red_10dp)
        } else {
            tvName.setBackgroundResource(R.drawable.search_background_10dp)
        }

        tvName.text = info
        tab.customView = customTabView
        return tab
    }

    fun changeTab(tab: TabLayout.Tab?, isSelected: Boolean) {
        val tvName = tab?.customView?.findViewById<AppCompatTextView>(R.id.tv_name) ?: return

        if (isSelected) {
            tvName.setBackgroundResource(R.drawable.button_rounded_boarder_red_10dp)
        } else {
            tvName.setBackgroundResource(R.drawable.search_background_10dp)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
    }

    override fun onRecentSearchClick(data: String) {
        binding.edSearchUser.setText(data)
        binding.edSearchUser.requestFocus()
        binding.edSearchUser.setSelection(data.length)
        currentPage = 1
        if (requestType == 1) {
            helper.getSearchData(
                requestType,
                data,
                currentPage,
                true
            )
        } else {

            binding.ctbSearchFilter.getTabAt(0)!!.select()
        }

    }

    /*
    Condition changed
     */
    fun updateTabs(any: GlobalSearchResponse?) {
        if (binding.ctbSearchFilter.tabCount > 1) {
            if (any?.musicIncluded == true) {
                (binding.ctbSearchFilter.getTabAt(1)?.view as LinearLayout).visibility = VISIBLE
            } else {
                (binding.ctbSearchFilter.getTabAt(1)?.view as LinearLayout).visibility = VISIBLE
            }
            if (any?.artistsIncluded == true) {
                (binding.ctbSearchFilter.getTabAt(1)?.view as LinearLayout).visibility = VISIBLE
            } else {
                (binding.ctbSearchFilter.getTabAt(1)?.view as LinearLayout).visibility = VISIBLE
            }
            if (any?.friendsIncluded == true) {
                (binding.ctbSearchFilter.getTabAt(2)?.view as LinearLayout).visibility = VISIBLE
            } else {
                (binding.ctbSearchFilter.getTabAt(2)?.view as LinearLayout).visibility = VISIBLE
            }
        }
    }

    fun updateRecentUpdate(s: String) {
        dbHelper.insertSearchData(s.trim())
        if (!recentSearchList.contains(s.trim())) {
            recentSearchList.add(s.trim())
            recentAdapter.addData(s.trim())
        }
    }

    fun showKeyboard(context: Context) {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY
        )
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
        helper.disposables.dispose()
    }
}
