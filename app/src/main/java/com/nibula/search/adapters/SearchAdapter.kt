package com.nibula.search.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.dashboard.ConstantsNavTabs
import com.nibula.databinding.ItemSearchAlbumBinding
import com.nibula.databinding.ItemSearchArtistBinding
import com.nibula.request_to_buy.navigate
import com.nibula.response.globalsearchResponse.GlobalSearchResponseItem
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils


class SearchAdapter(val context: Context, private val navigate: navigate) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class AlbumViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ArtistViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    val searchData = ArrayList<GlobalSearchResponseItem>()

    lateinit var bindingArtist:ItemSearchArtistBinding
    lateinit var bindingAlbum:ItemSearchAlbumBinding


    companion object {

        const val VIEW_TYPE_ALBUM = 1

        const val VIEW_TYPE_ARTIST = 2

    }

    override fun getItemViewType(position: Int): Int {
        return if (searchData[position].typeId == 2 || searchData[position].typeId == 1) {
            VIEW_TYPE_ALBUM
        } else {
            VIEW_TYPE_ARTIST
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_ARTIST) {
          /*  val view =
                LayoutInflater.from(context).inflate(R.layout.item_search_artist, parent, false)
            ArtistViewHolder(view)*/

            val inflater = LayoutInflater.from(parent.context)
            bindingArtist = ItemSearchArtistBinding.inflate(inflater, parent, false)

            val holder = ArtistViewHolder(bindingArtist.root)
            return holder

        } else {
          /*  val view =
                LayoutInflater.from(context).inflate(R.layout.item_search_album, parent, false)
            AlbumViewHolder(view)*/

            val inflater = LayoutInflater.from(parent.context)
            bindingAlbum= ItemSearchAlbumBinding.inflate(inflater, parent, false)

            val holder = AlbumViewHolder(bindingAlbum.root)
            return holder
        }
    }

    override fun getItemCount(): Int {
        return searchData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = searchData[position]
        if (holder is ArtistViewHolder) {
            bindingArtist.tvArtist.text = data.recordOrUserName
            bindingArtist.tvTagName.text = data.userName
            bindingArtist.tvInfo.text = data.userBio
            bindingArtist.tvInfo.visibility =
                if (data.userBio.isNullOrEmpty()) View.GONE else View.VISIBLE

            bindingArtist.tvTagName.visibility =
                if (data.userName.isNullOrEmpty()) View.GONE else View.VISIBLE

            bindingArtist.tvArtist.visibility =
                if (data.recordOrUserName.isNullOrEmpty()) View.GONE else View.VISIBLE

            CommonUtils.loadImage(
                context,
                data.recordOrUserImage,
                bindingArtist.imgArtist,
                R.drawable.ic_record_user_place_holder
            )
           bindingArtist.searchArtistContainer.setOnClickListener {
                if (data.id == PrefUtils.getValueFromPreference(context, PrefUtils.USER_ID)) {
                    navigate.changeTab(ConstantsNavTabs.NAV_TAB_PROFILE)
                } else {
                    navigate.openTopArtist(data.id, data.typeId)
                }
            }
        } else {
            val itemViewHolder = holder as AlbumViewHolder
           bindingAlbum.ctlUserReferrals.setOnClickListener {

                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL,
                        data.recordOrUserImage
                    )
                    putString(
                        AppConstant.ID,
                        data.id
                    )
                }
               navigate.goToNewOffering(bundle)

               // navigate.gotorequestinvest(data.id)
            }
            bindingAlbum.tvArtist.text =
                "${context.getString(R.string.artist)}: ${data.recordArtistName}"
            bindingAlbum.tvAlbum.text =
                "${context.getString(R.string.album)}: ${data.recordOrUserName}"
            bindingAlbum.tvInvestors.text =
                CommonUtils.redWhiteSpannable(
                    context, "${data.totalInvestors}", if (data.totalInvestors == 1) {
                        "${context.getString(R.string.investor)}"
                    } else {
                        "${context.getString(R.string.investors)}"
                    }
                )
            bindingAlbum.imgAlbum.clImage.setImageResource(R.drawable.nebula_placeholder)

            Glide
                .with(context)
                .load(data.recordOrUserImage)
                .placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.IMMEDIATE)
                .into(bindingAlbum.imgAlbum.clImage)



            bindingAlbum.tvAmount.text =
                "$${CommonUtils.trimDecimalToTwoPlaces(data.totalInvestedAmount)} ${
                    context.getString(
                        R.string.raised
                    )
                }"


            when (data.recordStatusTypeId) {
                AppConstant.released -> {
                    val tfLight = ResourcesCompat.getFont(context, R.font.pt_sans)
                    bindingAlbum.tvReleaseDate.visibility = View.GONE
                    bindingAlbum.tvStatus.typeface = tfLight
                    bindingAlbum.tvStatus.text = AppConstant.AVAILABLE
                    bindingAlbum.tvStatus.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.green_shade_1_100per
                        )
                    )

                }

            }
        }




    }

    fun clearAndAddData(data: ArrayList<GlobalSearchResponseItem>) {
        this.searchData.clear()
        this.searchData.addAll(data)
        notifyDataSetChanged()
    }

    fun addData(data: ArrayList<GlobalSearchResponseItem>) {
        this.searchData.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        this.searchData.clear()
        notifyDataSetChanged()
    }
}