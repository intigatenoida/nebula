package com.nibula.search.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemRecentSearchBinding


class RecentSearchAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class RecentSearchViewHolder(val view: View) : RecyclerView.ViewHolder(view)
    lateinit var binding:ItemRecentSearchBinding

    private val recentDataList = ArrayList<String>()

    private lateinit var layoutInflater: LayoutInflater
    private lateinit var context: Context
    private lateinit var listener: onRecentSearchClickListener


    constructor(context: Context, listener: onRecentSearchClickListener) : this() {
        this.context = context
        this.listener = listener
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       /* val view =
            layoutInflater.inflate(R.layout.item_recent_search, parent, false)*/

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemRecentSearchBinding.inflate(inflater, parent, false)
        val holder = RecentSearchViewHolder(binding.root)
        return holder
    }

    override fun getItemCount(): Int {
        return recentDataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RecentSearchViewHolder) {
            binding.tvRecentSearch.text = recentDataList[position]
            binding.recentSearchContainer.setOnClickListener {
                if (::listener.isInitialized) {
                    listener.onRecentSearchClick(recentDataList[position])
                }
            }
            if (position == recentDataList.size - 1) {
                binding.divider.visibility = View.GONE
            } else {
                binding.divider.visibility = View.VISIBLE
            }
        }
    }

    fun addData(dataList: ArrayList<String>) {
        recentDataList.addAll(dataList)
        notifyDataSetChanged()
    }


    fun addData(data: String) {
        recentDataList.add(data)
        notifyItemInserted(recentDataList.size - 1)
    }

    public interface onRecentSearchClickListener {
        fun onRecentSearchClick(data: String)
    }
}
