package com.nibula.withdraw

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentWithdrawBinding
import com.nibula.response.bankDetailListResponse.BankDetailListResponse
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.MinMaxFilter
import com.nibula.utils.PrefUtils


class WithdrawFragment : BaseFragment() {

    companion object {
        fun newInstance() = WithdrawFragment()
    }

    lateinit var bankList: BankDetailListResponse
    lateinit var helper: WithdrawHelper
    lateinit var root: View
    var isRemoveZero = false
    var currentBalance = 0.0
    var selectedBankId = -1
    lateinit var binding:FragmentWithdrawBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::root.isInitialized) {
           // root = inflater.inflate(R.layout.fragment_withdraw, null)
            binding=FragmentWithdrawBinding.inflate(inflater,container,false)
            root=binding.root
            currentBalance = arguments?.getDouble(AppConstant.AMOUNT) ?: 0.0
            binding.header.tvTitle.text = getString(R.string.withdraw_funds)
            binding.baseHeader.ivNotification.setOnClickListener {
                loginNotifyCheck()
            }
            binding.tvTransferUpto.text = "${getString(R.string.transfer_up_to)} $$currentBalance"

            binding.baseHeader.imgSearch.setOnClickListener {
                navigate.onClickSearch()
            }

            binding.baseHeader.ivUpload.setOnClickListener {
                loginUploadCheck()
            }


            binding.header.ivBack.setOnClickListener {
                navigate.manualBack()
                //activity?.supportFragmentManager?.popBackStack()
            }

            binding.ivclose.setOnClickListener {
                binding.edamount.setText("")

            }


            val color = ContextCompat.getColor(requireActivity(), R.color.colorAccent)

            binding.tvLearn.text = CommonUtils.underlineSpannable(binding.tvLearn.text.toString(), color)


            binding.edamount.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                }

                override fun beforeTextChanged(
                    s: CharSequence, start: Int, count: Int,
                    after: Int
                ) {


                }

                override fun afterTextChanged(s: Editable) {


                    if (s.toString().replace("$", "").isNotEmpty() && s.toString().replace("$", "")
                            .toDouble() > 0 && s.toString().replace("$", "")
                            .toDouble() <= currentBalance
                    ) {
                        binding.tvTransfer.background =
                            ContextCompat.getDrawable(activity!!, R.drawable.enable_7)
                        binding.tvTransfer.text = "Transfer $".plus(s.toString())
                    } else {
                        binding.tvTransfer.background =
                            ContextCompat.getDrawable(activity!!, R.drawable.diable_7)
                        binding.tvTransfer.text = "Transfer $0"

                    }
                }
            })
            binding.edamount.filters =
                arrayOf<InputFilter>(MinMaxFilter(1.0, currentBalance))
            binding.tvTransfer.setOnClickListener {
                if (binding.edamount.text.toString().trim()
                        .isNotEmpty() && binding.edamount.text.toString()
                        .trim().toDouble() > 0.0
                ) {
                    withdrawAmount()
                }
            }
            binding.rightArrow.setOnClickListener {
                openBankList()
            }

            binding.txtBankName.setOnClickListener {
                openBankList()
            }
            initHelper()

        }
        return root
    }

    private fun openBankList() {
        if (::bankList.isInitialized) {
            val bundle = Bundle().also {
                it.putInt(AppConstant.BANK_DETAIL, selectedBankId)
            }
            navigate.openBankList(bundle)
        }
    }

    private fun initHelper() {
        helper = WithdrawHelper(this)

    }

    private fun withdrawAmount() {
        helper.getWithDraw()
    }


    override fun onResume() {
        super.onResume()
        helper.getBankList()
        selectedBankId = -1

        if (binding.baseHeader.imgNotificationCount.visibility != View.VISIBLE) {
            CommonUtils.setNotificationDot(requireContext(), binding.baseHeader.imgNotificationCount)
        }
    }


    fun updateUI(bankList: BankDetailListResponse) {
        val previousBankId = PrefUtils.getIntValue(requireContext(), PrefUtils.SELECTED_BANK_ID)
        this.bankList = bankList
        for (bankDetail in bankList.responseCollection) {
            if (previousBankId > 0) {
                if (bankDetail.id == previousBankId) {
                    selectedBankId = bankDetail.id
                    binding.txtBankName.text =
                        "${bankDetail.bankName} ${bankDetail.bankAccountNumber}"
                    break
                }
            } else {
                if (bankDetail.isDefault) {
                    selectedBankId = bankDetail.id
                    binding.txtBankName.text = "${bankDetail.bankName} ${bankDetail.bankAccountNumber}"
                    CommonUtils.loadImage(
                        requireContext(),
                        bankDetail.bankIcon,
                        binding.imgBankLogo
                    )
                    break
                }
            }
        }
    }

}