package com.nibula.withdraw

import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.request.WithdrawRequest
import com.nibula.request.WithdrawSuccessRequest
import com.nibula.response.BaseResponse
import com.nibula.response.bankDetailListResponse.BankDetailListResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import java.util.*

class WithdrawHelper(val fg: WithdrawFragment) : BaseHelperFragment() {

    fun getWithDraw() {
        fg.hideKeyboard()
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }


        fg.showProcessDialog()
        val request = WithdrawRequest()
        request.availableAmount = fg.currentBalance.toString()
        request.currencyTypeId = 1
        request.UserBankAccountId = fg.selectedBankId
        request.requestedAmount = fg.binding.edamount.text.toString()
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.withdrawAmount(request)

        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BaseResponse>() {

            override fun onSuccess(any: BaseResponse?, message: String?) {
                val response = any as BaseResponse
                if (response.responseStatus == 1) {
                    fg.hideProcessDialog()

                    withdrawAmountComplete(response.withdrawalFundRequestsId)
//                    fg.navigate.manualBack()
                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }
        }))
    }

    fun withdrawAmountComplete(withdrawalFundRequestsId: Int) {
        fg.hideKeyboard()
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }


        fg.showProcessDialog()
        val rnd = Random()
        val request = WithdrawSuccessRequest()
        request.transactionNo = 100000000 + rnd.nextInt(900000000)
        request.currencyTypeId = 1
        request.referenceNo = 100000000 + rnd.nextInt(900000000)
        request.requestedAmount = fg.binding.edamount.text.toString()
        request.withdrawalFundRequestId = withdrawalFundRequestsId
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.withdrawAmountComplete(request)

        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BaseResponse>() {

            override fun onSuccess(any: BaseResponse?, message: String?) {
                val response = any as BaseResponse
                if (response.responseStatus == 1) {
                    fg.hideProcessDialog()
                    CustomToast.showToast(
                        fg.requireContext(),
                        response.responseMessage,
                        CustomToast.ToastType.SUCCESS
                    )
                    fg.navigate.manualBack()
                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }
        }))
    }


    fun getBankList() {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }


        fg.showProcessDialog()


        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getBankList(1)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BankDetailListResponse>() {
            override fun onSuccess(any: BankDetailListResponse?, message: String?) {
                fg.hideProcessDialog()
                val respone = any as BankDetailListResponse
                respone.let {
                    fg.updateUI(it)
                }
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }

        }))
    }
}