package com.nibula.withdraw
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.dashboard.DashboardActivity
import com.nibula.databinding.WithdrawalFundsWithPaypalBinding
import com.nibula.utils.PrefUtils
class WithdrawWithPaypal : BaseFragment() {

    companion object {
        private const val REQUEST_CODE = 123

        fun newInstance() = WithdrawWithPaypal()
    }

    lateinit var binding: WithdrawalFundsWithPaypalBinding
    lateinit var root: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        if (!::root.isInitialized) {
            // root = inflater.inflate(R.layout.fragment_withdraw, null)
            binding = WithdrawalFundsWithPaypalBinding.inflate(inflater, container, false)
            root = binding.root

        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.header.ivBack.setOnClickListener {
            navigate.manualBack()
            //activity?.supportFragmentManager?.popBackStack()
        }
        binding.ivclose.setOnClickListener {
            binding.edamount.setText("")

        }
        binding.tvTransfer.setOnClickListener {
            initiatepayOutProcess()
        /*    val intent = Intent(requireActivity(), PayPaypalPayoutsActivity::class.java)
            intent.putExtra("amount", binding.edamount.text.toString())
            startActivity(intent)*/
        }
    }

    fun initiatepayOutProcess() {
        val baseurl = "https://stage.nebu.la/payout"
        val token = PrefUtils.getValueFromPreference(requireContext(), PrefUtils.TOKEN)
            .replace("Bearer", "").trim()
        var paymentUrl = "$baseurl?amt=10&token=$token"
        val customTabsIntentBuilder = CustomTabsIntent.Builder()

        // Customize toolbar colors (optional)
        customTabsIntentBuilder.setToolbarColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorPrimary
            )
        )
        customTabsIntentBuilder.setSecondaryToolbarColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorPrimaryDark
            )
        )

        // Show the website title (optional)
        customTabsIntentBuilder.setShowTitle(false)

        // Create a PendingIntent for your custom action button
        val customActionButtonPendingIntent = createCustomActionButtonIntent()
        val bitmap: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.g_tick)

        customTabsIntentBuilder.setActionButton(
            bitmap,
            getString(R.string.done),
            customActionButtonPendingIntent,
            true
        )

        // Create a CustomTabsIntent and launch the URL
        val customTabsIntent = customTabsIntentBuilder.build()
        customTabsIntent.launchUrl(requireContext(), Uri.parse(paymentUrl))
    }


    private fun createCustomActionButtonIntent(): PendingIntent {
        val targetIntent = Intent(requireContext(), DashboardActivity::class.java)
        // Use PendingIntent.FLAG_IMMUTABLE to make the PendingIntent immutable
        targetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            requireContext(), 0 /* Request code */, targetIntent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        return pendingIntent
    }
}
