package com.nibula.fcm


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class UpdateDeviceInfoReq(
    @SerializedName("DeviceNotificationID")
    var deviceNotificationID: String="",
    @SerializedName("DeviceType_Fk_Id")
    var deviceTypeFkId: String="2",
    @SerializedName("LanguageTypeId")
    var languageTypeId: String="1"
)