package com.nibula.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.nibula.R
import com.nibula.dashboard.DashboardActivity
import com.nibula.utils.AppConstant
import com.nibula.utils.PrefUtils
import org.json.JSONObject
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {

    lateinit var intent: Intent

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.d(TAG, "From: ${remoteMessage.from}")

        if (remoteMessage.data.isNullOrEmpty()) {
            if (!TextUtils.isEmpty(remoteMessage.notification?.body)) {
                val data = remoteMessage.notification
                Log.d(TAG, "From: $data")
                val imageUrl = remoteMessage.toIntent().extras!!.get("gcm.notification.image").toString()
                sendDefaultNotification(
                    remoteMessage.notification!!.title!!,
                    remoteMessage.notification!!.body!!,
                    imageUrl
                )
            }
        } else {
            sendNotification(remoteMessage.data)
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
        }
    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
        PrefUtils.saveValueInPreference(applicationContext, PrefUtils.NOTIFICATION_TOKEN, token)
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     *
     */
    private fun sendNotification(messageBody: MutableMap<String, String>) {
        Log.d("MyFirebaseMsgService", "Message data payload 2: $messageBody")
        try {
            val json = JSONObject(messageBody["body"] ?: "{}")
            Log.d(
                "MyFirebaseMsgService",
                "Message data payload 3: ${json["NotificationMgs"]} \n ${json["Action"]} \n ${json["RecordId"]} \n  ${json["ChatId"]}"
            )
            val recordId: String = json["RecordId"] as String
            val action: String = json["Action"] as String
            val notification: String = json["NotificationMgs"] as String
            val chatId: String = json["ChatId"] as String
            val partnerId: String = json["PartnerId"] as String
            val artistName: String = json["ArtistName"] as String
            val recordImage: String = json["RecordImage"] as String
            intent = Intent(this, DashboardActivity::class.java)
            intent.putExtra(AppConstant.RECORD_ID, recordId)
            when (action) {
                "1" -> {
                    intent.action = AppConstant.DETAILS_VIEW
                }
                "2" -> {
                    intent.action = AppConstant.WALLET_VIEW
                }
                "3" -> {
                    intent.action = AppConstant.CHAT_VIEW
                    intent.putExtra(AppConstant.CHAT_ID, chatId)
                    intent.putExtra(AppConstant.partner_ID, partnerId)
                    intent.putExtra(AppConstant.RECORD_ARTIST_NAME, artistName)
                    intent.putExtra(AppConstant.RECORD_IMAGE_URL, recordImage)
                }
                "4" -> {
                    intent.action = AppConstant.REJECTED_VIEW
                }

                "5" -> {
                    intent.action = AppConstant.PENDING_REQUESTS
                }

                "7" -> {
                    intent.action = AppConstant.TOKEN_PURCHASE
                }
                else -> {
                    intent.action = AppConstant.NORMAL_VIEW
                }
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            /*   val pendingIntent = PendingIntent.getActivity(
                   this, 0 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT
            )*/

            val pendingIntent = PendingIntent.getActivity(
                this, 0 /* Request code */, intent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            )
            val bigTextStyle = NotificationCompat.BigTextStyle()
            bigTextStyle.setBigContentTitle(messageBody["title"])
            bigTextStyle.bigText(notification)

            val channelId = getString(R.string.default_notification_channel_id)
            val largeIcon = BitmapFactory.decodeResource(
                resources,
                R.mipmap.ic_launcher_round
            )
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(messageBody["title"])
                .setContentText(notification)
                .setStyle(bigTextStyle)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val random = Random()
            val notificationID = random.nextInt(3000)

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT
                )
                notificationManager.createNotificationChannel(channel)
            }

            notificationManager.notify(
                notificationID /* ID of notification */,
                notificationBuilder.build()
            )
        } catch (e: Exception) {
            println(e.stackTrace)
        }
    }

    companion object {

        private const val TAG = "MyFirebaseMsgService"
    }

    private fun sendDefaultNotification(title: String, messageBody: String, imageUrl: String) {
        val intent = Intent(this, DashboardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )


        val random = Random()
        val notificationID = random.nextInt(3000)
        val largeIcon = BitmapFactory.decodeResource(
            resources,
            R.mipmap.ic_launcher_round
        )

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(largeIcon)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        if (imageUrl != null && imageUrl != "null") {
            val bitmap: Bitmap = getBitmapfromUrl(imageUrl)!!
            notificationBuilder.setStyle(
                NotificationCompat.BigPictureStyle()
                    .bigPicture(bitmap)
                    .bigLargeIcon(null)
            ).build()
        }
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            // Since android Oreo notification channel is needed.
        setUpChannel(notificationManager, channelId)
        notificationManager.notify(notificationID, notificationBuilder.build())
    }
}

 private fun setUpChannel(notificationManager: NotificationManager, channelID: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val adminChannelName = "Nebula"
        val adminChannelDescription = "Nebula Notification"

        val channel = NotificationChannel(
            channelID,
            adminChannelName,
            NotificationManager.IMPORTANCE_HIGH
        )
        channel.description = adminChannelDescription
        channel.enableLights(true)
        channel.enableVibration(true)
        notificationManager.createNotificationChannel(channel)
    }


}

fun getBitmapfromUrl(imageUrl: String?): Bitmap? {
    return try {
        val url = URL(imageUrl)
        val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
        connection.setDoInput(true)
        connection.connect()
        val input: InputStream = connection.getInputStream()
        BitmapFactory.decodeStream(input)
    } catch (e: java.lang.Exception) {
        Log.e("awesome", "Error in getting notification image: " + e.localizedMessage)
        null
    }
}