package com.nibula.upload_music.ui.uploadrecord.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.AddArtistFragmentBinding
import com.nibula.response.globalsearchResponse.GlobalSearchResponseItem
import com.nibula.upload_music.ui.uploadrecord.activity.AddArtistsActivity
import com.nibula.upload_music.ui.uploadrecord.adapter.SelectedArtistsAdapter
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.AddArtistHelper
import com.nibula.upload_music.ui.uploadrecord.model.ArtistModel
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.user.profile.ProfileResponce
import com.nibula.utils.AppConstant
import com.nibula.utils.DebouncedOnClickListener
import com.nibula.utils.RequestCode


class AddArtistFragment : BaseFragment(), SelectedArtistsAdapter.onDelete {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): AddArtistFragment {
            val fg = AddArtistFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    private lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    private lateinit var selectedArtistsAdapter: SelectedArtistsAdapter

    var selectedArtistsList = arrayListOf<ArtistModel>()
    var globalSearchParacableList = arrayListOf<GlobalSearchResponseItem>()
    var selectedArtistsIdsString: String = ""

    var artistIdsArrayListNew = arrayListOf<String>()
    lateinit var addArtishHelper: AddArtistHelper
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    lateinit var binding:AddArtistFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.add_artist_fragment, container, false)
            binding=AddArtistFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root

            binding.dotsIndicator.setDotSelection(3)
            addArtishHelper = AddArtistHelper(this@AddArtistFragment, listenere)

            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            addArtishHelper.getProfile(true)
            binding.tvAddArtists.setOnClickListener {
                val intent = Intent(requireActivity(), AddArtistsActivity::class.java)
                startActivityForResult(intent, RequestCode.ARTIST_REQUEST_CODE)
            }

            binding.ivNext.setOnClickListener(object :
                DebouncedOnClickListener(AppConstant.TIME_TO_WAIT) {
                override fun onDebouncedClick(v: View?) {
                    listenere.nextStep()
                }
            })

            binding.ivPrevious.setOnClickListener(object :
                DebouncedOnClickListener(AppConstant.TIME_TO_WAIT) {
                override fun onDebouncedClick(v: View?) {
                    listenere.prevStep()
                }
            })

        }
        return rootView
    }

    fun setSelectedArtistsAdapter() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvSelectedArtists.layoutManager = layoutManager
        selectedArtistsAdapter = SelectedArtistsAdapter(requireContext(), this)
        binding.rvSelectedArtists.adapter = selectedArtistsAdapter
        selectedArtistsAdapter.addData(selectedArtistsList)

    }

    override fun onDelete(data: ArtistModel) {
        selectedArtistsList.remove(data)
        if (selectedArtistsList.size == 0) {
            displayErrorMessage()
        }

        selectedArtistsAdapter.addData(selectedArtistsList)
    }

    fun addFirstRow(profileResponse: ProfileResponce?) {
        var artistModel = ArtistModel()
        artistModel.artistName =
            profileResponse?.userProfile?.firstName + " " + profileResponse?.userProfile?.lastName
        artistModel.artistUserName =
            profileResponse?.userProfile?.userName?.replace("@", "")
        artistModel.artistUserImage =
            profileResponse?.userProfile?.userImage
        artistModel.isSelectedArtist = false
        artistModel.artistId = profileResponse?.userProfile?.id.toString()
        selectedArtistsList.add(artistModel)
        setSelectedArtistsAdapter()
    }


    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCode.ARTIST_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK
        ) {

            globalSearchParacableList =
                data?.getParcelableArrayListExtra<GlobalSearchResponseItem>("search_data")!!
            if (globalSearchParacableList.size > 0) hideErrorMessage()
            initiateUpdateListData()
        }
    }

    fun validation(): Boolean {
        return addArtishHelper.checkValidation()
    }


    fun initiateUpdateListData() {
        for (item in globalSearchParacableList) {
            if (item.isUserSelectArtist!!) {
                var artistModel = ArtistModel()
                artistModel.artistId = item.id
                artistModel.artistName = item.recordOrUserName
                artistModel.artistUserName = item.userName
                artistModel.artistUserImage = item.recordOrUserImage
                if (!selectedArtistsList.contains(artistModel))
                    selectedArtistsList.add(artistModel)

            }
        }

        selectedArtistsAdapter.addData(selectedArtistsList)
    }


    fun displayErrorMessage() {
        binding.tvNoArtistsText.visibility = View.VISIBLE
    }

    fun hideErrorMessage() {
        binding.tvNoArtistsText.visibility = View.GONE
    }
}