package com.nibula.upload_music.ui.uploadrecord.fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.GetStartedFragmentBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.GetStartedHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel


class GetStartFragment : BaseFragment() {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): GetStartFragment {
            val fg = GetStartFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    private lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    private lateinit var getStartedHelper: GetStartedHelper
     lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
     lateinit var binding:GetStartedFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.get_started_fragment, container, false)
            binding=GetStartedFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            binding.dotsIndicator.setDotSelection(1)
            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            getStartedHelper=GetStartedHelper(this@GetStartFragment,listenere)
            getStartedHelper.getRecordIdAndFileId()
            binding.tvGetStarted.setOnClickListener {
                listenere.nextStep()
            }
        }
        return rootView
    }
}