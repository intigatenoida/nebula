package com.nibula.upload_music.ui.uploadrecord.helper
import android.content.Intent
import android.os.Bundle
import com.nibula.base.BaseHelperFragment
import com.nibula.response.saveddraft.SaveAsDraftNewDataResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.upload_music.ui.savedrafts.newuploadsavasdraft.SaveAsDraftNewActivity
import com.nibula.user.profile.music.ProfileMusicFragment
import retrofit2.Call

class RecordDataHelper(
    val fg: ProfileMusicFragment
) : BaseHelperFragment() {
    lateinit var call: Call<SaveAsDraftNewDataResponse>

    fun getRecordIdDetail(recordId: String?) {
        fg.showProcessDialog()
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.getRecordIdDetail(
            recordId!!
        )
        call.enqueue(object : CallBackManager<SaveAsDraftNewDataResponse>() {
            override fun onSuccess(any: SaveAsDraftNewDataResponse?, message: String) {

                if (any?.responseStatus != null) {
                    if (any.responseStatus == 1) {
                        fg.hideProcessDialog()
                        val intent = Intent(fg.requireActivity(), SaveAsDraftNewActivity::class.java)
                        val bundle= Bundle()
                        bundle.putParcelable("draft_data",any)
                        intent.putExtras(bundle)
                        fg.requireActivity().startActivity(intent)

                        /*fg.uploadRecordDataViewModel.setSaveAsDraftResponseData(any)
                        fg.uploadRecordDataViewModel.setIsRecordFromSaveAsDraft(true)*/

                    } else {

                    }
                } else {

                }
            }

            override fun onFailure(message: String) {

            }

            override fun onError(error: RetroError) {

            }

        })

    }


}