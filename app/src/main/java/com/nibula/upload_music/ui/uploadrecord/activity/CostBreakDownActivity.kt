package com.nibula.upload_music.ui.uploadrecord.activity

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import androidx.core.content.res.ResourcesCompat
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.databinding.ActivityNewCostBreakDownBinding
import com.nibula.utils.AppConstant


class CostBreakDownActivity : BaseActivity() {
    lateinit var costBreakdownResponse: CostBreakdownResponse.Response
    lateinit var binding:ActivityNewCostBreakDownBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_new_cost_break_down)
        binding= ActivityNewCostBreakDownBinding.inflate(layoutInflater)
        setContentView(binding.root)
        costBreakdownResponse =
            intent.extras?.getParcelable<CostBreakdownResponse.Response>(AppConstant.COST_BREAKDOWN)!! as CostBreakdownResponse.Response
        initView()
        binding.ivBack.setOnClickListener {
            finish()
        }
    }

    override fun onBackPressed() {
        finish()
    }

    private fun initView() {


        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.priceMusicExclusiveOfTax?.let {
            com.nibula.utils.CommonUtils.trimDecimalToTwoPlaces(
                it
            )
        }).also { binding.tvPriceUMusic.text = it }


        binding.tvMusicPriceNote.text =
            costBreakdownResponse.priceMusicExclusiveOfTaxLabel.toString()

        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.taxCharged?.let {
            com.nibula.utils.CommonUtils.trimDecimalToTwoPlaces(
                it
            )
        }).also { binding.tvPriceWorkVat.text = it }


        binding.tvPriceVatNote.text = costBreakdownResponse.taxChargedLabel.toString()

        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.priceMusicInclusiveOfTax?.let {
            com.nibula.utils.CommonUtils.trimDecimalToTwoPlaces(
                it
            )
        }).also { binding.tvPriceMusicInclusive.text = it }

        binding.tvPriceMusicInclusiveNote.text =
            costBreakdownResponse.priceMusicInclusiveOfTaxLabel.toString()

        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.pricePerRoyaltyShareRecording?.let {
            com.nibula.utils.CommonUtils.trimDecimalToTwoPlaces(
                it
            )
        }).also { binding.tvPricePerRoyaltyRecording.text = it }

        binding.tvPricePerRoyaltyRecordingNote.text =
            costBreakdownResponse.pricePerRoyaltyShareRecordingLabel.toString()

        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.pricePerRoyaltyShareWork?.let {
            com.nibula.utils.CommonUtils.trimDecimalToTwoPlaces(
                it
            )
        }).also { binding.tvPricePerRoyaltyWork.text = it }

        binding.tvPricePerRoyaltyWorkNote.text =
            costBreakdownResponse.pricePerRoyaltyShareWorkLabel.toString()

        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.priceInitialMusicOffering?.let {
            com.nibula.utils.CommonUtils.trimDecimalToTwoPlaces(
                it
            )
        }).also { binding.tvPriceInitialOffering.text = it }


        binding.tvPriceInitialOfferingNote.text =
            costBreakdownResponse.priceInitialMusicOfferingLabel.toString()


        if (costBreakdownResponse.recordType != AppConstant.released) {
            binding.tvPriceEtherumNote.visibility = android.view.View.VISIBLE

            binding.tvPriceEtherum.visibility = android.view.View.VISIBLE

            binding.viewBottom.visibility = android.view.View.GONE
            binding.tvPriceEtherumNote.text =
                costBreakdownResponse.ethereumTransactionCostLabel.toString()
            (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.ethereumTransactionCost?.let {
                com.nibula.utils.CommonUtils.trimDecimalToThreePlaces(
                    it
                )
            }).also { binding.tvPriceEtherum.text = it }
        } else {
            binding.releaseGroup.visibility = android.view.View.GONE

            binding.viewBottom.visibility = android.view.View.VISIBLE
            binding.tvPriceEtherumNote.visibility = android.view.View.VISIBLE
            binding.tvPriceEtherum.visibility = android.view.View.VISIBLE
            binding.tvPriceMusicInclusiveNote.text =
                setBoldPrice(costBreakdownResponse.priceMusicInclusiveOfTaxLabel.toString())
        }



        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.ethereumTransactionCost?.let {
            com.nibula.utils.CommonUtils.trimDecimalToThreePlaces(
                it
            )
        }).also { binding.tvPriceEtherum.text = it }
        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.paypalTransactionFees?.let {
            com.nibula.utils.CommonUtils.trimDecimalToTwoPlaces(
                it
            )
        }).also { binding.tvPricePaypal.text = it }

        binding.tvPricePaypalNote.text = costBreakdownResponse.payPalFixedFeesLabel.toString()
        (costBreakdownResponse.currencyTypeSymbol + " " + costBreakdownResponse.stripeTransactionFees?.let {
            com.nibula.utils.CommonUtils.trimDecimalToTwoPlaces(
                it
            )
        }).also { binding.tvPriceStripe.text = it }


        binding.tvPriceStripeNote.text = costBreakdownResponse.stripeFixedFeesLabel.toString()
    }

    private fun setBoldPrice(str: String): Spannable {
        val spannable = SpannableString(str)
        val typeface = ResourcesCompat.getFont(this@CostBreakDownActivity, R.font.pt_sans_bold)
        if (typeface != null) {
            spannable.setSpan(
                StyleSpan(typeface.style),
                0,
                str.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
        }
        return spannable
    }


}