package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import androidx.lifecycle.Observer
import com.nibula.base.BaseHelperFragment
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.FinalSubmitFragment
import com.nibula.upload_music.ui.uploadrecord.fragment.UploadRecordFragmentNew
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call

class DiscardUploadingHelper(
    val fg: UploadRecordFragmentNew
) : BaseHelperFragment() {
    lateinit var call: Call<UploadRecordFileResponse>

    fun discardUploading() {
        fg.showProcessDialog()
        var recordId: String = ""

        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }


        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.discardUploading(
            recordId.toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {

                        Toast.makeText(
                            fg.requireContext(),
                            "Record discard successfully",
                            Toast.LENGTH_SHORT
                        ).show()
                        fg.requireActivity().finish()
                    } else {
                        Toast.makeText(fg.requireContext(), "Error Occured", Toast.LENGTH_SHORT)
                            .show()

                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }


}