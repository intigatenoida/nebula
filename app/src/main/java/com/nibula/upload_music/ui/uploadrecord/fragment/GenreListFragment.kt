package com.nibula.upload_music.ui.uploadrecord.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import androidx.lifecycle.ViewModelProvider
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentGenereListBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.GenreHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.utils.SpinnerAdapter


class GenreListFragment : BaseFragment() {
    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): GenreListFragment {
            val fg = GenreListFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var genereAdapter: SpinnerAdapter
    lateinit var genereHelper: GenreHelper
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    var genereTypeId: String = ""
    lateinit var binding:FragmentGenereListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
           //rootView = inflater.inflate(R.layout.fragment_genere_list, container, false)
            binding= FragmentGenereListBinding.inflate(inflater,container,false)
            rootView=binding.root
            binding.dotsIndicator.setDotSelection(4)

            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            genereHelper = GenreHelper(this@GenreListFragment, listenere)

            setGenerSpinnerAdapter(binding.spnGenre, getString(R.string.genre_song).toUpperCase())
            genereHelper.requestGenere(true)

            binding.ivPrevious.setOnClickListener {
                listenere.prevStep()
            }

            binding.ivNext.setOnClickListener {

                listenere.nextStep()
            }
        }
        return rootView
    }


    private fun setGenerSpinnerAdapter(sp: Spinner, topItem: String) {
        genereAdapter = SpinnerAdapter(requireActivity(), getAdapterData(topItem, mutableListOf()))
        sp.adapter = genereAdapter
        sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,

                id: Long
            ) {

                if (position == 0) return
                genereTypeId = genereHelper.generDataList[position - 1].value.toString()
                uploadRecordDataViewModel.selectedGenere(genereHelper.generDataList[position - 1].text.toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }

    private fun getAdapterData(
        topItem: String,
        dataList: MutableList<String>
    ): MutableList<String> {
        val temp = mutableListOf<String>()
        temp.add(topItem)
        temp.addAll(dataList)
        return temp
    }

    fun validation(): Boolean {
        return genereHelper.checkValidation()
    }
}