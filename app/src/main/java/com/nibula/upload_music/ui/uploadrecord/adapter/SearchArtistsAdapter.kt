package com.nibula.upload_music.ui.uploadrecord.adapter

import android.content.Context
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemSearchAlbumBinding
import com.nibula.databinding.ItemSearchArtistNewBinding

import com.nibula.response.globalsearchResponse.GlobalSearchResponseItem
import com.nibula.utils.CommonUtils



class SearchArtistsAdapter(
    val context: Context,
    private val listener: onRecentSearchClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class AlbumViewHolder(val view: View) : RecyclerView.ViewHolder(view)
lateinit var binding:ItemSearchArtistNewBinding
lateinit var bindingAlbum:ItemSearchAlbumBinding
    class ArtistViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    val searchData = ArrayList<GlobalSearchResponseItem>()


    companion object {

        const val VIEW_TYPE_ALBUM = 1

        const val VIEW_TYPE_ARTIST = 2

    }

    override fun getItemViewType(position: Int): Int {
        return if (searchData[position].typeId == 2 || searchData[position].typeId == 1) {
            VIEW_TYPE_ALBUM
        } else {
            VIEW_TYPE_ARTIST
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_ARTIST) {
          /*  val view =
                LayoutInflater.from(context).inflate(R.layout.item_search_artist_new, parent, false)
            ArtistViewHolder(view)*/

            val inflater = LayoutInflater.from(parent.context)
            binding = ItemSearchArtistNewBinding.inflate(inflater, parent, false)
            val holder = ArtistViewHolder(binding.root)
            return holder
        } else {
          /*  val view =
                LayoutInflater.from(context).inflate(R.layout.item_search_album, parent, false)
            AlbumViewHolder(view)*/

            val inflater = LayoutInflater.from(parent.context)
            bindingAlbum = ItemSearchAlbumBinding.inflate(inflater, parent, false)
            val holder = AlbumViewHolder(binding.root)
            return holder
        }
    }

    override fun getItemCount(): Int {
        return searchData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = searchData[position]
        if (holder is ArtistViewHolder) {
          binding.tvArtist.text = data.recordOrUserName
            binding.tvTagName.text = data.userName


           binding.tvTagName.visibility =
                if (data.userName.isNullOrEmpty()) View.GONE else View.VISIBLE

            binding.tvArtist.visibility =
                if (data.recordOrUserName.isNullOrEmpty()) View.GONE else View.VISIBLE

            CommonUtils.loadImage(
                context,
                data.recordOrUserImage,
                binding.imgArtist,
                R.drawable.ic_record_user_place_holder
            )

            if (searchData.get(position).isUserSelectArtist!!) {
                binding.imgNoResult.visibility = View.GONE
                binding.rightTick.visibility = View.VISIBLE

            } else {
                binding.imgNoResult.visibility = View.VISIBLE
                binding.rightTick.visibility = View.GONE

            }
            binding.imgNoResult.setOnClickListener {

                searchData.get(position).isUserSelectArtist = true
                notifyDataSetChanged()
                listener.onAdd(searchData.get(position).id)
            }

            binding.rightTick.setOnClickListener {

                searchData.get(position).isUserSelectArtist = false
                notifyDataSetChanged()
                listener.onRemove(searchData.get(position).id)
            }
        }

    }

    private fun releaseDateString(date: String): SpannableString {
        val ss1 = SpannableString("${context.getString(R.string.release_date)} $date")
        ss1.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
            context.getString(R.string.release_date).length + 1,
            ss1.length,
            0
        )
        return ss1
    }

    fun clearAndAddData(data: ArrayList<GlobalSearchResponseItem>) {
        this.searchData.clear()
        this.searchData.addAll(data)
        notifyDataSetChanged()
    }

    fun addData(data: ArrayList<GlobalSearchResponseItem>) {
        this.searchData.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        this.searchData.clear()
        notifyDataSetChanged()
    }

     interface onRecentSearchClickListener {
        fun onAdd(data: String)
        fun onRemove(data: String)
    }
}