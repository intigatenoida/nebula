package com.nibula.upload_music.ui.uploadrecord.model

import androidx.annotation.Keep


@Keep
data class CountryModel(
    var countryName: String? = "",
    var countryImage: String? = ""

)