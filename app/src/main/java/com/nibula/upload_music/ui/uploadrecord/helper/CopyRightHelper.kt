package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import androidx.lifecycle.Observer
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.*
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.collecting_society_res.CollectingSocietyResponse
import com.nibula.upload_music.ui.uploadrecord.fragment.CopyRightCodeFragment
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call

class CopyRightHelper(
    val fg: CopyRightCodeFragment,
    var listenere: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {

    lateinit var call: Call<UploadRecordFileResponse>

    private fun requestToAddCopyRight() {

        fg.showProcessDialog()
        var copyRightSocietyId: String = ""
        var recordId: String = ""
        var recordFiledId: String = ""
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        if (fg.copyRightSocietyworkId.trim().isEmpty()) {
            copyRightSocietyId = "1"
        } else {
            copyRightSocietyId = fg.copyRightSocietyworkId.trim()
        }
        // val durations = recordData.recordDurationInSeconds ?: 0
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.requestToAddCopyRightCode(
            9.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.binding.etIsrcCode.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.binding.etIswcCode.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),

            copyRightSocietyId
                .toRequestBody("text/plain".toMediaTypeOrNull()),

            fg.binding.etDistributer.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),

            fg.countryName.trim()
                .toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {

                        listenere.onSuccess()
                    } else {
                        listenere.failed(any?.responseMessage!!)

                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })
    }

    fun checkValidation(): Boolean {
        if (fg.binding.etIsrcCode.text.toString().isEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Please enter ISRC Code",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else if (fg.countryName.equals("United States") && fg.copyRightSocietyworkId.isEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Please select society work ID",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else {
            requestToAddCopyRight()
        }
        return true
    }


    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }

    fun getCollectingSociety(isProgressDialog: Boolean, currentTypeId: Int) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val authHelper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = authHelper.requestCopyrightSociety(currentTypeId)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<CollectingSocietyResponse>() {

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }

            override fun onSuccess(any: CollectingSocietyResponse?, message: String?) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.response?.societyWork.isNullOrEmpty()) {
                        any.response?.societyWork?.let {
                            fg.cswlist.addAll(it)
                            val temp = mutableListOf<String>()
                            temp.add(
                                fg.getString(R.string.copyright_collecting_society_work)
                                    .toUpperCase()
                            )
                            for (obj in it) {
                                temp.add(obj.societyName ?: "")
                            }
                            fg.copyrightSocietyWork.setData(temp)
                        }

                        /*any.response?.societyWork?.let {
                            for (society in it) {

                                fg.rootView.spn_csc_work.setSelection(
                                    it.indexOf(
                                        society
                                    ) + 1
                                )


                            }
                        }*/

                    }
/*
                    if (!any.response?.societyRecording.isNullOrEmpty()) {
                        any.response?.societyRecording?.let {
                            fg.csrlist.addAll(it)
                            val temp = mutableListOf<String>()
                            temp.add(
                                fg.getString(R.string.copyright_collecting_society_recording)
                                    .toUpperCase()
                            )
                            for (obj in it) {
                                temp.add(obj.societyName ?: "")
                            }
                            fg.copyrightSocietyRecording.setData(temp)
                        }
                        any.response?.societyRecording?.let {
                            if (fg.data != null && fg.data!!.copyrightSocietyRecording != null && fg.data!!.copyrightSocietyRecording!! > 0) {
                                for (society in it) {
                                    if (society.id == fg.data!!.copyrightSocietyRecording) {
                                        fg.rootView.spn_csc_recording.setSelection(
                                            it.indexOf(
                                                society
                                            ) + 1
                                        )

                                        break
                                    }
                                }
                            }
                        }
                    }
*/
                }

                dismiss(isProgressDialog)
            }


        }))
    }

}