package com.nibula.upload_music.ui.uploadrecord.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter

class ReviewAndSubmitFragment : BaseFragment() {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): ReviewAndSubmitFragment {
            val fg = ReviewAndSubmitFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    private lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.review_submit_fragment, container, false)

        }
        return rootView
    }
}