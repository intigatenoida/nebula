package com.nibula.upload_music.ui.savedrafts

import android.view.View
import com.nibula.R
import com.nibula.customview.CustomToast
import com.nibula.response.BaseResponse
import com.nibula.response.saveddraft.SavedDraftRecordResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError


class SaveDraftHelper(val fg: SaveDraftsFragment) {

    var nextPage: Int = 1
    var maxPage: Int = 0
    var isDataAvailable = true
    var isLoading = true

    fun requestSavedDraftsList(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.swpMain.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        isLoading = true

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.call = helper.requestSavedDrafts(nextPage)
        fg.call.enqueue(object : CallBackManager<SavedDraftRecordResponse>() {

            override fun onSuccess(any: SavedDraftRecordResponse?, message: String) {
                /*Data*/
                if (any?.responseStatus != null) {
                    if (nextPage == 1) {
                        fg.adapter.dataList.clear()
                    }
                    if (any.responseStatus == 1) {
                        if (!any.dataList.isNullOrEmpty()) {
                            fg.binding.rvSavedDrafts.visibility = View.VISIBLE
                            if (fg.adapter.dataList.isNotEmpty()) {
                                fg.adapter.addData(any.dataList)
                            } else {
                                fg.adapter.setData(any.dataList)
                            }
                            nextPage++
                        }
                    }
                    fg.adapter.notifyDataSetChanged()
                }
                /*Load More*/
                maxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                isDataAvailable = nextPage < maxPage ||
                        ((any?.totalRecords ?: 0) > (nextPage * 10))

                if (fg.adapter.dataList.isEmpty()) {
                    fg.binding.rvSavedDrafts.visibility = View.GONE
                }
                isLoading = false
                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                isLoading = false
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {
                isLoading = false
                dismiss(isProgressDialog)
                if (error.kind == RetroError.Kind.API_CALL_CANCEL) {

                } else {

                }
            }

        })
    }

    fun requestDelete(position: Int, id: String, isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.swpMain.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.callDelete = helper.requestDeleteSavedDrafts(id)
        fg.callDelete.enqueue(object : CallBackManager<BaseResponse>() {

            override fun onSuccess(any: BaseResponse?, message: String) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    fg.adapter.dataList.removeAt(position)
                    fg.adapter.notifyItemRemoved(position)
                    fg.adapter.notifyItemRangeChanged(position, fg.adapter.dataList.size)
                }

                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {
                dismiss(isProgressDialog)
                if (error.kind == RetroError.Kind.API_CALL_CANCEL) {

                } else {

                }
            }

        })
    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (fg.binding.swpMain.isRefreshing) {
            fg.binding.swpMain.isRefreshing = false
        } else if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        noData()
    }

    private fun noData() {
        if (fg.adapter.dataList.isEmpty()) {
            fg.binding.incNoData.root.visibility = View.VISIBLE
            fg.binding.incNoData.tvMessage.text = fg.getString(R.string.no_drafts_available)
        } else {
            fg.binding.rvSavedDrafts.visibility = View.VISIBLE
            fg.binding.incNoData.root.visibility = View.GONE
        }
    }

}