package com.nibula.upload_music.ui.uploadrecord.collecting_society_res


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class CollectingSocietyResponse(
    @SerializedName("Response")
    var response: Response? = Response()
):BaseResponse() {
    @Keep
    data class Response(
        @SerializedName("SocietyRecording")
        var societyRecording: ArrayList<SocietyRecording>? = arrayListOf(),
        @SerializedName("SocietyWork")
        var societyWork: ArrayList<SocietyWork>? = arrayListOf()
    ) {
        @Keep
        data class SocietyRecording(
            @SerializedName("CurrencyType")
            var currencyType: String? = "",
            @SerializedName("Id")
            var id: Int? = 0,
            @SerializedName("SocietyName")
            var societyName: String? = ""
        )

        @Keep
        data class SocietyWork(
            @SerializedName("CurrencyType")
            var currencyType: String? = "",
            @SerializedName("Id")
            var id: Int? = 0,
            @SerializedName("SocietyName")
            var societyName: String? = ""
        )
    }
}