package com.nibula.upload_music.ui.uploadrecord.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.TokenSellFragmenNewBinding
import com.nibula.databinding.TokenSellFragmentBinding
import com.nibula.databinding.TokenSellFragmetnNewBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.KeyboardaAdapterNew
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.TokenSellHelper
import com.nibula.upload_music.ui.uploadrecord.model.KeyboardModel
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.utils.CommonUtils


class TokenSellFragment : BaseFragment(), KeyboardaAdapterNew.onItemClick {

    companion object {
        /*     var MINIMUM_RASING_MONEY = 10
               var MINIMUM_TOKEN_FOR_SALE = 1
               var MINIMUN_OWNERSHIP_PERCENTAGE = 0.01 */
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): TokenSellFragment {
            val fg = TokenSellFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var tokenSellHelper: TokenSellHelper
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    lateinit var keyboardAdapter: KeyboardaAdapterNew
    var modelTypeArrayList = arrayListOf<KeyboardModel>()
    var perTokenValue: Double = 0.0
    var ownerShipPercentage: Double = 0.0
    var percentageToSell: Double = 0.0
    var royalityPercentage: Float = 0.0f
    val listener: KeyboardaAdapterNew.onItemClick = this@TokenSellFragment
    lateinit var binding:TokenSellFragmetnNewBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //inflater.inflate(R.layout.token_sell_fragmetn_new, container, false).also { rootView = it }

            binding=TokenSellFragmetnNewBinding.inflate(inflater,container,false)
            rootView=binding.root


            /*   modelTypeArrayList.add(KeyboardModel("1", false))
               modelTypeArrayList.add(KeyboardModel("2", false))
               modelTypeArrayList.add(KeyboardModel("3", false))
               modelTypeArrayList.add(KeyboardModel("4", false))
               modelTypeArrayList.add(KeyboardModel("5", false))
               modelTypeArrayList.add(KeyboardModel("6", false))
               modelTypeArrayList.add(KeyboardModel("7", false))
               modelTypeArrayList.add(KeyboardModel("8", false))
               modelTypeArrayList.add(KeyboardModel("9", false))
               modelTypeArrayList.add(KeyboardModel("0", false))
               modelTypeArrayList.add(KeyboardModel(".", false))
               modelTypeArrayList.add(KeyboardModel("Clear", false))

               rootView.findViewById<RecyclerView>(R.id.recyclerView).layoutManager =
                   GridLayoutManager(requireContext(), 3)
               keyboardAdapter = KeyboardaAdapterNew(requireContext(), modelTypeArrayList, this)
               rootView.findViewById<RecyclerView>(R.id.recyclerView).adapter = keyboardAdapter*/
            tokenSellHelper = TokenSellHelper(this@TokenSellFragment, listenere)

            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")

            if (uploadRecordDataViewModel?.isRecordFromSaveAsDraft?.value!!) {

                val completedStepNumber =
                    uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.stepNumber
                if (completedStepNumber!! >= 8) {
                    setNineStepData()

                }

            }
            keypadListener(rootView)

        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        binding.edtAmount.text?.clear()
        binding.etPricePerToken.setText("0")
        binding.etRoyaltyShare.setText("0")
    }

    fun validation(): Boolean {
        return tokenSellHelper.checkValidation()
    }

    fun onKeyboardItemClick(text: String) {

        if (text.equals("") || text.equals(".")) return
        var display: String = binding.edtAmount?.getText().toString()
        if (text.equals("Clear", ignoreCase = true)) {
            if (!TextUtils.isEmpty(display)) {
                display = display.substring(0, display.length - 1)
                binding.edtAmount?.setText(display)
            }
        } else {
            if (text == "." && display.contains(".")) {
            } else {
                display += text
            }
            binding.edtAmount?.setText(display)
        }
        if (display.isEmpty()) {
            binding.etPricePerToken.setText("0")
            binding.etRoyaltyShare.setText("0")
            return
        }
        if (display.length > 4) {
            return
        }
        uploadRecordDataViewModel.raisingMoney.observe(viewLifecycleOwner, Observer {
            val result = (it.toFloat() / display.toInt())
            perTokenValue = result.toDouble()
            var floatValue = CommonUtils.trimDecimalToThreePlacesNew(result.toDouble())
            binding.etPricePerToken.text = floatValue
        })
        uploadRecordDataViewModel.percentageToSell.observe(viewLifecycleOwner, Observer {
            percentageToSell = it.toDouble()
        })
        uploadRecordDataViewModel.ownerShipPercentage.observe(viewLifecycleOwner, Observer {
            royalityPercentage = it.toFloat()
        })
        var royalityOwnership = (royalityPercentage * percentageToSell) / display.toInt()
        var floatValue = CommonUtils.trimDecimalToFourPlaces(royalityOwnership)
        ownerShipPercentage = royalityPercentage.toDouble()
        binding.etRoyaltyShare.setText(floatValue)
    }

    override fun itemClick(textValue: String) {
        onKeyboardItemClick(textValue)
    }

    /*
     Set Up Minumum value calculation for Price per token and royality Ownership
       min: $10 (variable)
       max: $10,000,000 (variable)
     */
    private fun keypadListener(rootView: View) {
        with(rootView) {
            binding.tvOne.setOnClickListener {
                listener.itemClick("1")
            }
            binding.tvTwo.setOnClickListener {
                listener.itemClick("2")
            }
            binding.tvThree.setOnClickListener {
                listener.itemClick("3")
            }
            binding.tvFour.setOnClickListener {
                listener.itemClick("4")
            }
            binding.tvFive.setOnClickListener {
                listener.itemClick("5")
            }
            binding.tvSix.setOnClickListener {
                listener.itemClick("6")
            }
            binding.tvSeven.setOnClickListener {
                listener.itemClick("7")
            }
            binding.tvEight.setOnClickListener {
                listener.itemClick("8")
            }
            binding.tvNine.setOnClickListener {
                listener.itemClick("9")
            }
            binding.tvDot.setOnClickListener {
                listener.itemClick(".")
            }
            binding.tvZero.setOnClickListener {
                listener.itemClick("0")
            }
            binding.tvClear.setOnClickListener {
                listener.itemClick("Clear")
            }
        }

    }

    fun setNineStepData() {
        val noOfTokenToSell =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.totalShares
        val ownerShipPercentage =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.royalityOwnershipPercentagePerToken
        val pricePerToken =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.valuePerShareInDollars

        binding.edtAmount.setText(noOfTokenToSell.toString())
        binding.etPricePerToken.setText(ownerShipPercentage.toString())
        binding.etRoyaltyShare.setText(pricePerToken.toString())


    }

}