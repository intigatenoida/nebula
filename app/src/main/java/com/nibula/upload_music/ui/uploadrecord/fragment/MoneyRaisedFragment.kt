package com.nibula.upload_music.ui.uploadrecord.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.MoneyRaisedFragmentBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.KeyboardaAdapterNew
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.currency_res.Currecy
import com.nibula.upload_music.ui.uploadrecord.helper.MoneyRaisedHelper
import com.nibula.upload_music.ui.uploadrecord.model.KeyboardModel
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.utils.SpinnerAdapter

import java.util.*

class MoneyRaisedFragment : BaseFragment(), KeyboardaAdapterNew.onItemClick {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): MoneyRaisedFragment {
            val fg = MoneyRaisedFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    lateinit var currencyAdapter: SpinnerAdapter
    lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var helper: MoneyRaisedHelper
    var currancyList = ArrayList<Currecy>()
    lateinit var keyboardAdapter: KeyboardaAdapterNew
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    var currencyTypeId: String = "1"
    var modelTypeArrayList = arrayListOf<KeyboardModel>()
    lateinit var binding:MoneyRaisedFragmentBinding
    val listener: KeyboardaAdapterNew.onItemClick = this@MoneyRaisedFragment

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
           // rootView = inflater.inflate(R.layout.money_raised_fragment, container, false)
            binding= MoneyRaisedFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root

            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            helper = MoneyRaisedHelper(this, listenere)
            helper.getCurrency(true)
            rootView.findViewById<RecyclerView>(R.id.recyclerView).layoutManager =
                GridLayoutManager(requireContext(), 3)
            keyboardAdapter = KeyboardaAdapterNew(requireContext(), modelTypeArrayList, this)
            rootView.findViewById<RecyclerView>(R.id.recyclerView).adapter = keyboardAdapter

            setCurrencyAdapter(
                binding.spnCurrency,
                "USD"
            )
            if (uploadRecordDataViewModel?.isRecordFromSaveAsDraft?.value!!) {

                val completedStepNumber =
                    uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.stepNumber
                if (completedStepNumber!! >= 6) {
                    setSevenStepData()
                }
            }
        }
        keypadListener(rootView)

        return rootView
    }

    private fun setCurrencyAdapter(sp: Spinner, topItem: String) {

        currencyAdapter =
            SpinnerAdapter(requireActivity(), getAdapterData(topItem, mutableListOf()))
        sp.adapter = currencyAdapter
        sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (currancyList.isNotEmpty() && position > 0) {
                    if (binding.edtAmountRaised.text.toString().isNotEmpty()) {
                        //rootView.edtAmountRaised.setText("")
                        when (currancyList[position - 1].toString()) {

                            "USD" -> {
                                binding.tvCurrencyType.text = "$"
                                currencyTypeId = "1"
                                uploadRecordDataViewModel.selectedCurrency("USD")

                            }
                            else -> {
                                binding.tvCurrencyType.text = "CHF"
                                currencyTypeId = "2"
                                uploadRecordDataViewModel.selectedCurrency("CHF")

                            }
                        }
                    } else {

                        when (currancyList[position - 1].toString()) {

                            "USD" -> {
                                binding.tvCurrencyType.text = "$"
                                currencyTypeId = "1"
                                uploadRecordDataViewModel.selectedCurrency("USD")
                            }
                            else -> {
                                binding.tvCurrencyType.text = "CHF"
                                currencyTypeId = "2"
                                uploadRecordDataViewModel.selectedCurrency("CHF")
                            }
                        }
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }

    private fun getAdapterData(
        topItem: String,
        dataList: MutableList<String>
    ): MutableList<String> {
        val temp = mutableListOf<String>()
        temp.add(topItem)
        temp.addAll(dataList)
        return temp
    }

    fun onKeyboardItemClick(text: String) {
        if (text.isEmpty()) return
        var display: String = binding.edtAmountRaised?.getText().toString()
        if (text.equals("Clear", ignoreCase = true)) {
            if (!TextUtils.isEmpty(display)) {
                display = display.substring(0, display.length - 1)
                binding.edtAmountRaised?.setText(display)
            }
        } else {
            if (text == "." && display.contains(".")) {

            } else {
                display = display + text
            }
            if (display.length == 1) {
                binding.edtAmountRaised?.setText(display)

            } else {
                binding.edtAmountRaised?.setText(display)

            }
        }
    }

    override fun itemClick(textValue: String) {
        onKeyboardItemClick(textValue)
    }

    fun validation(): Boolean {
        return helper.checkValidation()
    }

    private fun keypadListener(rootView: View) {
        with(rootView) {
            binding.tvOne.setOnClickListener {
                listener.itemClick("1")
            }
            binding.tvTwo.setOnClickListener {
                listener.itemClick("2")
            }
            binding.tvThree.setOnClickListener {
                listener.itemClick("3")
            }
            binding.tvFour.setOnClickListener {
                listener.itemClick("4")
            }
            binding.tvFive.setOnClickListener {
                listener.itemClick("5")
            }
            binding.tvSix.setOnClickListener {
                listener.itemClick("6")
            }
            binding.tvSeven.setOnClickListener {
                listener.itemClick("7")
            }
            binding.tvEight.setOnClickListener {
                listener.itemClick("8")
            }
            binding.tvNine.setOnClickListener {
                listener.itemClick("9")
            }
            binding.tvDot.setOnClickListener {
                listener.itemClick(".")
            }
            binding.tvZero.setOnClickListener {
                listener.itemClick("0")
            }
            binding.tvClear.setOnClickListener {
                listener.itemClick("Clear")
            }
        }

    }

    fun setSevenStepData() {
        val currentType =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.currencyType
        val amount =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.investmentAmountNeeded

        if (currentType.equals("CHF")) {
            setCurrencyAdapter(
                binding.spnCurrency,
                "CHF"
            )
        } else {
            setCurrencyAdapter(
                binding.spnCurrency,
                "USD"
            )
        }
        binding.edtAmountRaised.setText(amount?.toString())
    }

}