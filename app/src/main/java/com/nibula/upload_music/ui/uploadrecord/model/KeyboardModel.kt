package com.nibula.upload_music.ui.uploadrecord.model

import androidx.annotation.Keep

@Keep
data class KeyboardModel(
    var number: String? = "",
    var isCircleSelected: Boolean? = false
)