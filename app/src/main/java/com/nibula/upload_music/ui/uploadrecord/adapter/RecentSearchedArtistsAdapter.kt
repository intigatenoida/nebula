package com.nibula.upload_music.ui.uploadrecord.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemSearchArtistNewBinding
import com.nibula.response.globalsearchResponse.GlobalSearchResponseItem
import com.nibula.utils.CommonUtils


class RecentSearchedArtistsAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class RecentSearchViewHolder(val view: View) : RecyclerView.ViewHolder(view)
    lateinit var binding:ItemSearchArtistNewBinding

     val recentDataList = ArrayList<GlobalSearchResponseItem>()

    private lateinit var layoutInflater: LayoutInflater
    private lateinit var context: Context
    private lateinit var listener: onRecentSearchClickListener


    constructor(context: Context, listener: onRecentSearchClickListener) : this() {
        this.context = context
        this.listener = listener
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
      /*  val view =
            layoutInflater.inflate(R.layout.item_search_artist_new, parent, false)
        return RecentSearchViewHolder(view)*/

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemSearchArtistNewBinding.inflate(inflater, parent, false)
        val holder = RecentSearchViewHolder(binding.root)
        return holder
    }

    override fun getItemCount(): Int {
        return recentDataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RecentSearchViewHolder) {
            binding.tvArtist.text = recentDataList[position]?.recordOrUserName
            binding.tvTagName.text = recentDataList[position]?.userName
            recentDataList[position]?.recordOrUserImage?.let {

                CommonUtils.loadImage(
                    context,
                    recentDataList[position]?.recordOrUserImage,
                    binding.imgArtist,
                    R.drawable.ic_record_user_place_holder
                )

            }

            if (recentDataList.get(position).isUserSelectArtist!!) {
                binding.imgNoResult.visibility = View.GONE
                binding.rightTick.visibility = View.VISIBLE

            } else {
                binding.imgNoResult.visibility = View.VISIBLE
                binding.rightTick.visibility = View.GONE

            }

            binding.imgNoResult.setOnClickListener {
                recentDataList.get(position).isUserSelectArtist = true
                notifyDataSetChanged()
                listener.onAdd(recentDataList.get(position).id!!)
            }

            binding.rightTick.setOnClickListener {

                recentDataList.get(position).isUserSelectArtist = false
                notifyDataSetChanged()
                listener.onRemove(recentDataList.get(position).id!!)
            }

        }
    }

    fun addData(dataList: ArrayList<GlobalSearchResponseItem>) {
        recentDataList.addAll(dataList)
        notifyDataSetChanged()
    }


    fun addData(data: GlobalSearchResponseItem) {
        recentDataList.add(data)
        notifyItemInserted(recentDataList.size - 1)
    }

    public interface onRecentSearchClickListener {
        fun onAdd(data: String)
        fun onRemove(data: String)
    }
}
