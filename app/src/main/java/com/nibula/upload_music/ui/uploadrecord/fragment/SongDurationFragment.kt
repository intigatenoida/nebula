package com.nibula.upload_music.ui.uploadrecord.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.SongDurationFragmentBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.SongDurationHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel



class SongDurationFragment : BaseFragment() {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): SongDurationFragment {
            val fg = SongDurationFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    lateinit var songDurationHelper: SongDurationHelper
    var selectedDuration: String = ""
    var fullSongLength: String = ""
    var isPreviouslyReleased: Boolean = false
    lateinit var binding:SongDurationFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.song_duration_fragment, container, false)
            binding= SongDurationFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            binding.dotsIndicator.setDotSelection(4)

            songDurationHelper = SongDurationHelper(this@SongDurationFragment, listenere)
            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            uploadRecordDataViewModel.durationOfSongInSeconds.observe(viewLifecycleOwner, Observer {
                fullSongLength = it.toString()
            })
            if (uploadRecordDataViewModel?.isRecordFromSaveAsDraft?.value!!) {

                val completedStepNumber =
                    uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.stepNumber
                if (completedStepNumber !!>=5) {
                    setSixStepData()

                }
            }

            binding.etMessageToCollector.setOnTouchListener(OnTouchListener { view, motionEvent ->
                view.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> view.parent.requestDisallowInterceptTouchEvent(false)
                }
                false
            })
            binding.sbHide.setOnCheckedChangeListener { view, isChecked ->
                if (isChecked) {
                    selectedDuration = fullSongLength
                    binding.tvMessageHideCoOwners.setText(R.string.yes)
                    isPreviouslyReleased = true
                    binding.tv20.setBackgroundResource(R.drawable.bg_grey_circle)
                    binding.tv10.setBackgroundResource(R.drawable.bg_grey_circle)
                    binding.tvFull.setBackgroundResource(R.drawable.bg_selected_duration_oval)

                    binding.tv10.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.greay_circle
                        )
                    )
                    binding.tv20.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.greay_circle
                        )
                    )

                    //Disable click listener
                    binding.tv10.isClickable = false
                    binding.tv20.isClickable = false
                } else {
                    selectedDuration = ""
                    isPreviouslyReleased = false
                    binding.tvMessageHideCoOwners.setText(R.string.no)
                    binding.tv20.setBackgroundResource(R.drawable.bg_oval)
                    binding.tv10.setBackgroundResource(R.drawable.bg_oval)
                    binding.tvFull.setBackgroundResource(R.drawable.bg_oval)


                    binding.tv10.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                    binding.tv20.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )

                    //Enable click listener
                    binding.tv10.isClickable = true
                    binding.tv20.isClickable = true
                }
            }

            binding.tv10.setOnClickListener {
                selectedDuration = "10"
                binding.tv10.setBackgroundResource(R.drawable.bg_selected_duration_oval)
                binding.tv20.setBackgroundResource(R.drawable.bg_oval)
                binding.tvFull.setBackgroundResource(R.drawable.bg_oval)
            }

            binding.tv20.setOnClickListener {
                selectedDuration = "20"
                binding.tv20.setBackgroundResource(R.drawable.bg_selected_duration_oval)
                binding.tv10.setBackgroundResource(R.drawable.bg_oval)
                binding.tvFull.setBackgroundResource(R.drawable.bg_oval)

            }

            binding.tvFull.setOnClickListener {
                selectedDuration = fullSongLength
                binding.tv20.setBackgroundResource(R.drawable.bg_oval)
                binding.tv10.setBackgroundResource(R.drawable.bg_oval)
                binding.tvFull.setBackgroundResource(R.drawable.bg_selected_duration_oval)

            }
            binding.ivNext.setOnClickListener {

                listenere.nextStep()
            }

            binding.ivPrevious.setOnClickListener {
                listenere.prevStep()
            }

        }
        return rootView
    }

    fun validation(): Boolean {
        return songDurationHelper.checkValidation()
    }

    fun setSixStepData() {

        val coownerMessage =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.messageToCoOwners

        val isPrereleasedSong =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.isSongPreviouslyReleased
        if (isPrereleasedSong!!) {
            binding.sbHide.isChecked = true
            selectedDuration =
                uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.files?.get(0)?.musicDurationInSeconds.toString()

            binding.tvMessageHideCoOwners.setText(R.string.yes)
            isPreviouslyReleased = true
            binding.tv20.setBackgroundResource(R.drawable.bg_grey_circle)
            binding.tv10.setBackgroundResource(R.drawable.bg_grey_circle)
            binding.tvFull.setBackgroundResource(R.drawable.bg_selected_duration_oval)
            binding.tv10.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.white
                )
            )
            binding.tv20.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.white
                )
            )

            //Disable click listener
            binding.tv10.isClickable = false
            binding.tv20.isClickable = false
        } else {
            binding.sbHide.isChecked = false
            isPreviouslyReleased = false

            val songDuration =
                uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.files?.get(0)?.musicDurationInSeconds.toString()
            if (songDuration!!.equals("10")) {
                selectedDuration = "10"
                binding.tv10.setBackgroundResource(R.drawable.bg_selected_duration_oval)
                binding.tv20.setBackgroundResource(R.drawable.bg_oval)
                binding.tvFull.setBackgroundResource(R.drawable.bg_oval)
            } else if (songDuration.equals("20")) {
                selectedDuration = "20"
                binding.tv20.setBackgroundResource(R.drawable.bg_selected_duration_oval)
                binding.tv10.setBackgroundResource(R.drawable.bg_oval)
                binding.tvFull.setBackgroundResource(R.drawable.bg_oval)
            }
            binding.tvMessageHideCoOwners.setText(R.string.no)

            binding.tv10.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.white
                )
            )
            binding.tv20.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.white
                )
            )

            //Enable click listener
            binding.tv10.isClickable = true
            binding.tv20.isClickable = true
        }
        binding.etMessageToCollector.setText(coownerMessage)
    }
}