package com.nibula.upload_music.ui.uploadrecord.fragment

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import androidx.lifecycle.ViewModelProvider
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.CopyRightCodeFragmentBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.SpineerAdapterForCountry
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.collecting_society_res.CollectingSocietyResponse
import com.nibula.upload_music.ui.uploadrecord.helper.CopyRightHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.utils.SpinnerAdapter

class CopyRightCodeFragment : BaseFragment() {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): CopyRightCodeFragment {
            val fg = CopyRightCodeFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    lateinit var copyRightHelper: CopyRightHelper
    lateinit var copyrightSocietyWork: SpinnerAdapter
    lateinit var countryAdapter: SpineerAdapterForCountry
    var copyRightSocietyworkId: String = ""
    var countryName: String = "United States"
    var cswlist: ArrayList<CollectingSocietyResponse.Response.SocietyWork> = arrayListOf()
    var countryList: ArrayList<String> = arrayListOf()
    lateinit var binding:CopyRightCodeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
           //rootView = inflater.inflate(R.layout.copy_right_code_fragment, container, false)
            binding= CopyRightCodeFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            val ss1 = SpannableString(getString(R.string.where_do_i_find))
            ss1.setSpan(UnderlineSpan(), 0, ss1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            binding.tvFixedFind.text = ss1
            copyRightHelper = CopyRightHelper(this, listenere)
            copyRightHelper.getCollectingSociety(true, 1)
            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")

            setSocietyWorkAdapter(
                binding.spnCscWork,
                getString(R.string.copyright_collecting_society_work).toUpperCase()
            )

            setCountryAdapter(
                binding.spnCurrency,
                getString(R.string.name).toUpperCase()
            )
            countryList.add("Select Country")
            countryList.add("United States")
            countryList.add("Switzerland")
            countryAdapter.setData(countryList)
            for (name in countryList) {
                binding.spnCurrency.setSelection(
                    1
                )
            }

            if (uploadRecordDataViewModel?.isRecordFromSaveAsDraft?.value!!) {

                val completedStepNumber =
                    uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.stepNumber
                if (completedStepNumber!! >= 9) {
                    setCopyRightData()

                }
            }


        }
        return rootView
    }

    fun validation(): Boolean {
        return copyRightHelper.checkValidation()
    }

    private fun setSocietyWorkAdapter(sp: Spinner, topItem: String) {
        copyrightSocietyWork =
            SpinnerAdapter(requireActivity(), getAdapterData(topItem, mutableListOf()))
        sp.adapter = copyrightSocietyWork
        sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 0) return
                copyRightSocietyworkId = cswlist[position - 1].id.toString()

//                if (currancyList.isNotEmpty() && position > 0) {
////                    rootView.tv_amount_currency_2.text = currancyList[position - 1].text
////                    rootView.tv_amount_currency.text = currancyList[position - 1].text
//                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }

    private fun getAdapterData(
        topItem: String,
        dataList: MutableList<String>
    ): MutableList<String> {
        val temp = mutableListOf<String>()
        temp.add(topItem)
        temp.addAll(dataList)
        return temp
    }

    private fun setCountryAdapter(sp: Spinner, topItem: String) {
        countryAdapter =
            SpineerAdapterForCountry(
                requireActivity(),
                getAdapterForCountry(topItem, mutableListOf())
            )
        sp.adapter = countryAdapter
        sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                if (position == 2) {
                    binding.ctlCopyRight.visibility = View.GONE
                    binding.etDistributer.visibility = View.GONE
                    countryName = "Switzerland"

                } else {
                    countryName = "United States"
                    binding.ctlCopyRight.visibility = View.VISIBLE
                    binding.etDistributer.visibility = View.VISIBLE
                }
//                if (currancyList.isNotEmpty() && position > 0) {
////                    rootView.tv_amount_currency_2.text = currancyList[position - 1].text
////                    rootView.tv_amount_currency.text = currancyList[position - 1].text
//                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }

    private fun getAdapterForCountry(
        topItem: String,
        dataList: MutableList<String>
    ): MutableList<String> {
        val temp = mutableListOf<String>()
        temp.add(topItem)
        temp.addAll(dataList)
        return temp
    }

    fun setCopyRightData() {
        val currencyType =
            uploadRecordDataViewModel?.saveAsDraftResponse?.value?.response?.countryName
        val ISRCCODE = uploadRecordDataViewModel?.saveAsDraftResponse?.value?.response?.iSCRCode
        val ISWCCODE = uploadRecordDataViewModel?.saveAsDraftResponse?.value?.response?.iSWCCode
        if (currencyType.equals("United States")) {
            binding.etIsrcCode.setText(ISRCCODE)
            binding.etIswcCode.setText(ISWCCODE)
        } else {
            binding.etIsrcCode.setText(ISRCCODE)
            binding.etIswcCode.setText(ISWCCODE)

            binding.spnCurrency.setSelection(
                2
            )
        }
    }
}