package com.nibula.upload_music.ui.uploadrecord.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.widget.AppCompatTextView
import com.nibula.R
import com.nibula.upload_music.ui.uploadrecord.model.KeyboardModel

class KeyboardAdapter(
    var keyboardArrayList: ArrayList<KeyboardModel>,
    var context: Context?,
    val listener: onItemClick
) : BaseAdapter() {

    class KeyboardView(view: View) {
        var tvTextView: AppCompatTextView = view.findViewById(R.id.k_textView)
        var imageView: View = view.findViewById(R.id.k_imageView)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val vHolder: KeyboardView?
        val view: View? =
            convertView ?: LayoutInflater.from(context!!)!!
                .inflate(R.layout.keyboard_item, parent, false)

        vHolder = KeyboardView(view!!)

        if (keyboardArrayList[position].number.equals("Clear")) {
            vHolder.tvTextView.setVisibility(View.INVISIBLE)
            vHolder.imageView.setVisibility(View.VISIBLE)
        } else {
            vHolder.tvTextView.setVisibility(View.VISIBLE)
            vHolder.imageView.setVisibility(View.GONE)
        }
        if (keyboardArrayList.get(position).isCircleSelected!!) {
            vHolder.tvTextView.text = keyboardArrayList[position].number
            vHolder.tvTextView.setBackgroundResource(R.drawable.bg_white_circle)

        } else {
            vHolder.tvTextView.text = keyboardArrayList[position].number
            vHolder.tvTextView.setBackgroundResource(0)

        }
        vHolder.tvTextView.setOnClickListener {

            if(keyboardArrayList[position].isCircleSelected!!)
            {
                keyboardArrayList[position].isCircleSelected = false
                listener.itemClick(keyboardArrayList[position].number.toString())
                notifyDataSetChanged()
            }
            else
            {
                keyboardArrayList[position].isCircleSelected = true
                listener.itemClick(keyboardArrayList[position].number.toString())
                notifyDataSetChanged()
            }

        }
        vHolder.imageView.setOnClickListener {

            listener.itemClick(keyboardArrayList[position].number.toString())
        }

        return view
    }

    override fun getItem(position: Int): Any {
        return keyboardArrayList!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return keyboardArrayList!!.size
    }

    interface onItemClick {
        fun itemClick(textValue: String)
    }
}