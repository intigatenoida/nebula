package com.nibula.upload_music.ui.uploadrecord.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.NewUplaodLayoutBinding
import com.nibula.permissions.PermissionHelper
import com.nibula.request.upload.UploadProgressRequestBody
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.NewUploadFragmentHelper
import com.nibula.upload_music.ui.uploadrecord.model.UploadRecordDataModel
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.uploadimage.UploadImageHelper
import com.nibula.utils.AppConstant
import java.io.File

class UploadSongFragment : BaseFragment() {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): UploadSongFragment {
            val fg = UploadSongFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    lateinit var newUploadFragmentHelper: NewUploadFragmentHelper
    var isAlbum: Boolean = true
    lateinit var rootView: View
    lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    lateinit var songLocaleFileUri: String
    var songTitle: String = ""
    var isSaveAsDraft: Boolean = false
    lateinit var songFilePath: String
    var songFilePathUrl: String = ""

    var currentUploadStatus: String = ""
    var currentProgressValue: Int = 0
    
    lateinit var binding:NewUplaodLayoutBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //binding = inflater.inflate(R.layout.new_uplaod_layout, container, false)
            binding= NewUplaodLayoutBinding.inflate(inflater,container,false)
            rootView=binding.root
            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            newUploadFragmentHelper = NewUploadFragmentHelper(this, listenere)
            if (uploadRecordDataViewModel?.isRecordFromSaveAsDraft?.value!!) {
                isSaveAsDraft = true
                songFilePath =
                    uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.files?.get(0)?.id.toString()
                setFirstStepData()
            }

            binding.ivPlay.setOnClickListener {

                if (isSaveAsDraft) {
                    if (songFilePathUrl.isBlank()) {
                        newUploadFragmentHelper.getSongUrl(songFilePath.toInt(), listenere)
                        binding.ivPlay.visibility = View.INVISIBLE
                        binding.ivPause.visibility = View.VISIBLE
                    } else {
                        binding.ivPlay.visibility = View.INVISIBLE
                        binding.ivPause.visibility = View.VISIBLE
                        listenere.onSongClick(
                            songFilePathUrl,
                            true
                        )
                    }

                } else {
                    binding.ivPlay.visibility = View.INVISIBLE
                    binding.ivPause.visibility = View.VISIBLE
                    newUploadFragmentHelper?.selectedRecordedFile?.toString()?.let { it1 ->
                        listenere.onSongClick(
                            it1,
                            true
                        )
                    }
                }


            }
            binding.ivPause.setOnClickListener {

                binding.ivPlay.visibility = View.VISIBLE
                binding.ivPause.visibility = View.INVISIBLE
                listenere.onSongClick(
                    newUploadFragmentHelper?.selectedRecordedFile.toString(),
                    false
                )

            }
            binding.ivDeleteSong.setOnClickListener {

                if (newUploadFragmentHelper.currrentStatus.equals(UploadProgressRequestBody.RUNNING) && newUploadFragmentHelper.currentProgressValue >= 0) {

                    Toast.makeText(
                        requireContext(),
                        "Please wait till uploading...",
                        Toast.LENGTH_SHORT
                    ).show()

                } else {
                    newUploadFragmentHelper?.songFileName = ""
                    binding.tvFixedMsg1.visibility = View.VISIBLE
                    binding.imgUpload.visibility = View.VISIBLE
                    binding.ivPlay.visibility = View.GONE
                    binding.tvSongTitleName.visibility = View.GONE
                    binding.ivPause.visibility = View.GONE
                    binding.ivDeleteSong.visibility = View.GONE
                    listenere.releaseSong()
                }

            }

            binding.ivCloseCover.setOnClickListener {

                if (newUploadFragmentHelper.currrentStatus.equals(UploadProgressRequestBody.RUNNING) && newUploadFragmentHelper.currentProgressValue >= 0) {
                    Toast.makeText(
                        requireContext(),
                        "Please wait till uploading...",
                        Toast.LENGTH_SHORT
                    ).show()

                } else {
                    newUploadFragmentHelper?.sendFile = null
                    binding.ivCloseCover.visibility = View.GONE
                    binding.imgCover.setImageResource(R.drawable.rectangle)
                    binding.group2.visibility = View.VISIBLE
                    binding.imgCover.visibility = View.GONE


                }

            }
            binding.imgUpload.setOnClickListener {

                if (PermissionHelper.isPermissionsAllowed2(
                        this,
                        PermissionHelper.permissions,
                        true,
                        AppConstant.PERMISSION_WRITE_EXTERNAL_STORAGE
                    )
                ) {

                    if (!currentUploadStatus.equals(UploadProgressRequestBody.RUNNING)) {
                        newUploadFragmentHelper?.selectAudioSongFile()

                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Please wait till uploading...",
                            Toast.LENGTH_SHORT
                        ).show()

                    }
                }
            }
            binding.imgCoverDemo.setOnClickListener {

                if (PermissionHelper.isPermissionsAllowed2(
                        this,
                        PermissionHelper.permissions,
                        true,
                        AppConstant.PERMISSION_WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    chooseImage()
                }
            }
/*
            binding.img_id_proof.setOnClickListener {
                if (PermissionHelper.isPermissionsAllowed2(
                        this,
                        PermissionHelper.permissions,
                        true,
                        AppConstant.PERMISSION_WRITE_EXTERNAL_STORAGE
                    )
                ) {

                    if (!currentUploadStatus.equals(UploadProgressRequestBody.RUNNING)) {
                        newUploadFragmentHelper?.selectAudioSongFile()

                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Please wait till uploading...",
                            Toast.LENGTH_SHORT
                        ).show()

                    }
                }

            }
*/

        }
        return rootView
    }


    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        newUploadFragmentHelper?.onActivityResult(requestCode, resultCode, data)
    }

    private fun chooseImage() {

       startActivityForResult(
            newUploadFragmentHelper?.setImageUri()?.let {
                UploadImageHelper.getPickImageIntent(
                    requireActivity(),
                    it
                )
            },
            AppConstant.REQUEST_UPLOAD_IMAGE
        )
    }

    fun cropData(uri: Uri?) {
        newUploadFragmentHelper?.cropDataSection(uri)
    }


    fun validation(): Boolean {

        return newUploadFragmentHelper?.checkValidation()!!
    }

    fun setFirstStepData() {

        binding.imgCover.visibility = View.VISIBLE
        binding.gdSongCover.visibility = View.GONE
        binding.ivCloseCover.visibility = View.VISIBLE
        binding.group1.visibility = View.GONE
        binding.tvSongTitleName.visibility = View.VISIBLE
        binding.ivPlay.visibility = View.VISIBLE
        binding.ivDeleteSong.visibility = View.VISIBLE
        binding.tvSongTitleName.text =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.recordTitle

        newUploadFragmentHelper.songFileName =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.recordTitle
        newUploadFragmentHelper.sendFile = File("")
        Glide
            .with(requireActivity())
            .load(uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.imgCover)
        val recordModel = UploadRecordDataModel()
        recordModel.recordId = uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.id
        recordModel.recordFiledId =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.files?.get(0)?.id.toString()
        // Set path of the song
        recordModel.songCoverImage = ""
        recordModel.isCoverImageChange = true
        recordModel.isCoverImageChange = true
        uploadRecordDataViewModel.setUplaodSongFirstStepData(recordModel)

        uploadRecordDataViewModel.setRecordFileChanges(true)
        uploadRecordDataViewModel.setRecordCoverImage(true)
        uploadRecordDataViewModel.setSongDurationFile(
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.files?.get(
                0
            )?.musicDurationInSeconds.toString()
        )
        uploadRecordDataViewModel.setSongCoverImage(
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.recordImage!!,
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.recordTitle!!,
            ""
        )
        uploadRecordDataViewModel.songTitle(uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.recordTitle!!)

    }
}