package com.nibula.upload_music.ui.uploadrecord.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.upload_music.ui.uploadrecord.model.KeyboardModel

class KeyboardaAdapterNew(
    var context: Context,
    var dataList: ArrayList<KeyboardModel>,
    var listener: onItemClick
) : RecyclerView.Adapter<KeyboardaAdapterNew.ViewHolder>() {

    // Provide a direct reference to each of the views with data items
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var title: TextView

        init {
            image = itemView.findViewById(R.id.k_imageView)
            title = itemView.findViewById(R.id.k_textView)
        }

    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): KeyboardaAdapterNew.ViewHolder {

        // Inflate the custom layout
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.keyboard_item, parent, false)
        return ViewHolder(view)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: KeyboardaAdapterNew.ViewHolder, position: Int) {

        // Get the data model based on position
        var data = dataList[position]

        holder.title.text = data.number

        if (dataList[position].number.equals("Clear")) {
            holder.title.setVisibility(View.INVISIBLE)
            holder.image.setVisibility(View.VISIBLE)
        } else {
            holder.title.setVisibility(View.VISIBLE)
            holder.image.setVisibility(View.GONE)
        }
        if (dataList.get(position).isCircleSelected!!) {
            holder.title.text = dataList[position].number
            holder.title.setBackgroundResource(R.drawable.bg_white_circle)

        } else {
            holder.title.text = dataList[position].number

            holder.title.setBackgroundResource(R.drawable.bg_black_circle)

        }
        holder.title.setOnClickListener {

            if (dataList[position].isCircleSelected!!) {
                listener.itemClick(dataList[position].number.toString())
            } else {
                for (i in 0..dataList.size - 1) {
                    dataList[i].isCircleSelected = position == i
                }
                listener.itemClick(dataList[position].number.toString())
                notifyDataSetChanged()
            }

        }
        holder.image.setOnClickListener {

            listener.itemClick(dataList[position].number.toString())
        }
    }

    //  total count of items in the list
    override fun getItemCount() = dataList.size


    interface onItemClick {
        fun itemClick(textValue: String)
    }
}