package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import com.nibula.base.BaseHelperFragment
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.SongDurationFragment
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call

class SongDurationHelper(
    val fg: SongDurationFragment,
    var viewPagerListener: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {
    lateinit var call: Call<UploadRecordFileResponse>
    private fun songDurationRequest() {

        fg.showProcessDialog()
        var recordId: String = ""
        var recordFiledId: String = ""
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.addSongDurationRequest(
            5.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.isPreviouslyReleased.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.selectedDuration.trim().toRequestBody("text/plain".toMediaTypeOrNull()),

            fg.binding.etMessageToCollector.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {

                        viewPagerListener.onSuccess()
                    } else {
                        viewPagerListener.failed(any?.responseMessage!!)

                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

    fun checkValidation(): Boolean {
        if (fg.binding.etMessageToCollector.text.toString().isEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Message to collector is required",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else if (fg.selectedDuration.isEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Please select the duration",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else {
            songDurationRequest()
        }
        return true
    }

}