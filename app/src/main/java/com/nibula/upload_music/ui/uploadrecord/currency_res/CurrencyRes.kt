package com.nibula.upload_music.ui.uploadrecord.currency_res


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nibula.response.BaseResponse

@Keep
data class CurrencyRes(
    @SerializedName("ResponseCollection")
    var currecyList: MutableList<Currecy>
) : BaseResponse()