package com.nibula.upload_music.ui.uploadrecord.helper

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import com.nibula.BuildConfig
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.customview.CustomToast.showToast
import com.nibula.request.mp3request.mp3UrlRequest
import com.nibula.request.upload.UploadProgressRequestBody
import com.nibula.response.mp3Response.mp3Response
import com.nibula.response.uploadrecord.RecordGetDataUpdate
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.UploadSongFragment
import com.nibula.upload_music.ui.uploadrecord.model.UploadRecordDataModel
import com.nibula.uploadimage.UploadImageHelper
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.RequestCode
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import java.io.File

class NewUploadFragmentHelper(
    val fg: UploadSongFragment,
    var viewPagerListener: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment(),
    UploadProgressRequestBody.UploadCallbacks {
    var sendFile: File? = null
    var originalFilePath: String? = null
    var songFileName: String? = ""
    var originalUri: Uri? = null
    var isCameraSelected = false
    var recordFileUri: Uri? = null
    var selectedRecordedFile: Uri? = null
    lateinit var call: Call<UploadRecordFileResponse>
    var recordId: String = ""
    lateinit var recordModel: UploadRecordDataModel
    var songDuration: Int = 0
    var currrentStatus: String = ""
    var currentProgressValue: Int = 0
    lateinit var coverImageBody: MultipartBody.Part
    var isSongFileChanged: Boolean = false
    var isImageCoverFileChanged: Boolean = false
    companion object {
        var isSongRecordFileChanged: Boolean = false
    }
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            AppConstant.REQUEST_UPLOAD_IMAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    handleImageResult(data)
                }
            }
            RequestCode.REQUEST_SONG_FILES_SELECTION -> {
                val uriPaths: ArrayList<Uri> = ArrayList()

                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        if (data.clipData != null) {
                            for (i in 0 until (data.clipData?.itemCount ?: 0)) {
                                val uri: Uri? = data.clipData?.getItemAt(i)?.uri
                                if (uri != null) {
                                    uriPaths.add(uri)
                                }
                            }
                        } else {
                            val uri = data.data
                            if (uri != null) {
                                uriPaths.add(uri)
                            }
                            val fileName = getFileName(fg.requireContext(), uri)
                            fg.binding.tvSongTitleName.visibility = View.VISIBLE
                            fg.binding.ivPlay.visibility = View.VISIBLE
                            /* fg.rootView.img_upload.visibility = View.GONE
                             fg.rootView.tv_fixed_msg_1.visibility = View.GONE*/
                            fg.binding.group1.visibility = View.GONE
                            fg.binding.tvSongTitleName.text = fileName
                            fg.songTitle = fileName
                            fg.uploadRecordDataViewModel.setRecordFileChanges(false)
                            fg.binding.ivDeleteSong.visibility = View.VISIBLE
                            songDuration =
                                getRecordFileDurations(fg.requireContext(), selectedRecordedFile)
                            songDuration?.toString()
                                ?.let { fg.uploadRecordDataViewModel.setSongDurationFile(it) }

                        }
                    }
                }

            }
        }

    }
    fun setImageUri(): Uri {
        val file = UploadImageHelper.tempFileReturn(
            fg.requireContext(),
            UploadImageHelper.TEMPORARY_PROFILE_IMAGE
        )
        originalUri = FileProvider.getUriForFile(
            fg.requireContext(),
            "${BuildConfig.APPLICATION_ID}.${fg.getString(R.string.file_provider_name)}",
            file
        )

        originalFilePath = file.absolutePath
        return originalUri!!
    }

    private fun handleImageResult(data: Intent?) {
        fg.showProcessDialog()
        isCameraSelected = false
        val disposableObserver = object : DisposableObserver<String>() {
            override fun onComplete() {
                fg.hideProcessDialog()
            }

            override fun onNext(t: String) {
                fg.hideProcessDialog()
                if (originalUri == null) {
                    fg.showToast(
                        fg.requireContext(),
                        fg.getString(R.string.message_upload_image),
                        CustomToast.ToastType.FAILED
                    )
                    return
                }
                //start cropping
                startCropImageActivity(originalUri!!)
            }

            override fun onError(e: Throwable) {
                fg.hideProcessDialog()
            }

        }
        disposables.add(getObserver(data, disposableObserver))
    }

    private fun getObserver(
        data: Intent?,
        disposableObserver: DisposableObserver<String>
    ): DisposableObserver<String> {
        return getObservable(data)
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(disposableObserver)
    }

    private fun getObservable(data: Intent?): Observable<String> {
        return Observable.create { emitter ->
            if (data?.data != null) {
                //Photo from gallery
                val f = UploadImageHelper.copyFileFromStream(
                    fg.requireContext(),
                    data.data!!,
                    UploadImageHelper.TEMPORARY_PROFILE_IMAGE
                )
                originalUri = Uri.fromFile(f)
                originalFilePath = f.absolutePath
            } else {
                //Photo from Camera
                isCameraSelected = true
            }

            if (!originalFilePath.isNullOrEmpty()) {
                emitter.onNext(originalFilePath ?: "")
                emitter.onComplete()
            } else {
                emitter.onError(Throwable("queryImageUrl is empty"))
            }
        }
    }

    fun startCropImageActivity(imageUri: Uri) {
        CropImage.activity(imageUri)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setAspectRatio(1, 1)
            .start(fg.requireActivity())
    }

    fun cropDataSection(uri: Uri?) {
        if (uri == null) {
            fg.showToast(
                fg.requireContext(),
                "Record image cropping failed",
                CustomToast.ToastType.FAILED
            )
            return
        }
        if (sendFile != null) {
            sendFile!!.delete()
        }

        fg.binding.imgCover.visibility = View.VISIBLE
        fg.binding.imgCover.setBackgroundResource(R.drawable.recent_blank)
        fg.binding.ivCloseCover.visibility = View.VISIBLE

        fg.binding.group2.visibility = View.GONE

        fg.uploadRecordDataViewModel.setRecordCoverImage(false)
        val path = uri.path ?: ""
        sendFile = if (path.isNotEmpty()) {
            CommonUtils.loadImageWithOutCache(
                fg.requireContext(),
                path,
                fg.binding.imgCover,
                R.drawable.black_gradient
            )


            File(path)
        } else {
            null
        }
    }

    fun selectAudioSongFile() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "audio/*"
        if (fg.isAlbum) {
            fg.startActivityForResult(
                Intent.createChooser(
                    intent,
                    fg.getString(R.string.select_songs)
                ),
                RequestCode.REQUEST_SONG_FILES_SELECTION
            )
        }
    }

    fun getFileName(context: Context, localSongsUri: Uri?): String {
        recordFileUri = localSongsUri
        Log.d("Uri is", recordFileUri.toString())
        selectedRecordedFile = getFilePathFromUri()?.toUri()
        var fileName = "Song"

        if (localSongsUri == null) {
            return fileName
        }
        val projection: Array<String> = arrayOf(OpenableColumns.DISPLAY_NAME)
        val cr =
            context.contentResolver.query(localSongsUri, projection, null, null, null)
        try {
            if (cr != null) {
                if (cr.moveToFirst()) {
                    val nameIndex: Int = cr.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                    fileName = cr.getString(nameIndex)
                }

                songFileName = fileName
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return fileName
    }


    fun getRecordFileDurations(context: Context, localSongsUri: Uri?): Int {
        var duration = 0
        if (localSongsUri == null) {
            return duration
        }
        val mmr = MediaMetadataRetriever()
        mmr.setDataSource(context, localSongsUri)
        val durationStr =
            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
        try {
            val millSecond = durationStr?.toInt() ?: 0
            duration = (millSecond / 1000.0).toInt()
        } catch (e: Exception) {

        }
        return duration
    }

    fun checkValidation(): Boolean {

        if (!(songFileName?.toString()!!.contains(".mp3") || songFileName?.toString()!!
                .contains(".wav"))
        ) {
            Toast.makeText(
                fg.requireContext(),
                "You can upload either .mp3 file or .wav file.",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        }
        if (currrentStatus.equals(UploadProgressRequestBody.RUNNING)) {
            viewPagerListener.onSuccess()
            return true
        } else if (songFileName.isNullOrEmpty()) {
            Toast.makeText(fg.requireContext(), "Please select song file", Toast.LENGTH_SHORT)
                .show()
            return false
        } else if (sendFile == null) {
            Toast.makeText(fg.requireContext(), "Please select cover image", Toast.LENGTH_SHORT)
                .show()
            return false
        } else {
            if (!fg.uploadRecordDataViewModel.isRecordSongFileChanged) {

                currrentStatus = UploadProgressRequestBody.RUNNING
                fg.listenere.uploadStatus(currrentStatus)

                fg.binding.tvUploadingPercentage.visibility = View.VISIBLE
                fg.binding.tvSongTitleName.visibility = View.GONE
                fg.binding.ivPlay.visibility = View.GONE
                fg.binding.ivPause.visibility = View.GONE

                saveRecordFile(
                    recordFileUri, RecordGetDataUpdate(
                        recordId = "",
                        id = 0,
                        songFilePath = recordFileUri.toString(),
                        recordDurationInSeconds = 150
                    ), this
                )
            } else if (!fg.uploadRecordDataViewModel.isCoverImageChanged) {
                currrentStatus = UploadProgressRequestBody.RUNNING
                fg.listenere.uploadStatus(currrentStatus)

                /*    fg.rootView.tvUploadingPercentage.visibility = View.VISIBLE
                    fg.rootView.tvSongTitleName.visibility = View.GONE
                    fg.rootView.ivPlay.visibility = View.GONE
                    fg.rootView.ivPause.visibility = View.GONE*/
                saveRecordFile(
                    recordFileUri, RecordGetDataUpdate(
                        recordId = "",
                        id = 0,
                        songFilePath = recordFileUri.toString(),
                        recordDurationInSeconds = 150
                    ), this
                )

            } else {
                viewPagerListener.onSuccess()

            }

        }
        return true
    }


    fun saveRecordFile(
        uri: Uri?,
        recordData: RecordGetDataUpdate,
        listener: UploadProgressRequestBody.UploadCallbacks
    ) {

        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }

        recordData.progress = 0
        recordData.uploadStatus = UploadProgressRequestBody.RUNNING

        if (recordFileUri != null) {
            handleRecordFile(recordData, listener)
        } else {
            handleCoverImage(recordData, listener)
        }
    }

    private fun handleRecordFile(
        recordData: RecordGetDataUpdate,
        listener: UploadProgressRequestBody.UploadCallbacks
    ) {
        val disposableObserver = object : DisposableObserver<File>() {
            override fun onComplete() {

            }

            override fun onError(e: Throwable) {

                fg.binding.tvUploadingPercentage.visibility = View.GONE
                fg.binding.ivPlay.visibility = View.VISIBLE
                fg.binding.tvSongTitleName.visibility = View.VISIBLE
                listener.onError(
                    UploadProgressRequestBody.RETRY,
                    RetroError(RetroError.Kind.UNEXPECTED, e.message, -999)
                )
            }

            override fun onNext(t: File) {
                requestSaveSongRecordWithFile(recordData, t, listener)
            }
        }
        disposables.add(getObserver(recordFileUri, disposableObserver))
    }


    private fun handleCoverImage(

        recordData: RecordGetDataUpdate,
        listener: UploadProgressRequestBody.UploadCallbacks
    ) {
        fg.showProcessDialog()
        requestSaveSongRecordWithFile(recordData, sendFile!!, listener)

    }

    private fun requestSaveSongRecordWithFile(
        recordData: RecordGetDataUpdate,
        recordFile: File,
        listener: UploadProgressRequestBody.UploadCallbacks
    ) {
        val audioFileRequestBody = if (CommonUtils.isDebug()) {
            if (fg.uploadRecordDataViewModel.isRecordSongFileChanged) {

                val mediaType = "audio/mpeg".toMediaTypeOrNull()!!
                val requestBody = "".toRequestBody(mediaType)
                MultipartBody.Part.createFormData("SongFile", "", requestBody)
            } else {
                val uploadRequestBody =
                    UploadProgressRequestBody(recordFile, "audio/mpeg", listener)
                MultipartBody.Part.createFormData("SongFile", recordFile.name, uploadRequestBody)
            }

        } else {
            if (fg.uploadRecordDataViewModel.isRecordSongFileChanged) {
                val mediaType = "audio/mpeg".toMediaTypeOrNull()!!
                val requestBody = "".toRequestBody(mediaType)
                MultipartBody.Part.createFormData("SongFile", "", requestBody)
            } else {
                val uploadRequestBody =
                    UploadProgressRequestBody(recordFile, "audio/mpeg", listener)
                MultipartBody.Part.createFormData("SongFile", recordFile.name, uploadRequestBody)
            }
        }
        if (fg.uploadRecordDataViewModel.isCoverImageChanged) {
            val mediaType = "image/*".toMediaTypeOrNull()!!
            val requestBody = "".toRequestBody(mediaType)
            coverImageBody =
                MultipartBody.Part.createFormData("SongCover", "" ?: "", requestBody)
        } else {
            val mediaType = "image/*".toMediaTypeOrNull()!!
            val requestBody = sendFile?.asRequestBody(mediaType) ?: "".toRequestBody(mediaType)
            coverImageBody =
                MultipartBody.Part.createFormData("SongCover", sendFile?.name ?: "", requestBody)
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel?.recordId?.let {
            recordId = it
        }

        isSongFileChanged = !fg.uploadRecordDataViewModel.isRecordSongFileChanged
        isImageCoverFileChanged = !fg.uploadRecordDataViewModel.isCoverImageChanged
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.requestSavedRecordFileNew(
            1.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toRequestBody("text/plain".toMediaTypeOrNull()),
            songDuration.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            isImageCoverFileChanged.toString()
                .toRequestBody("text/plain".toMediaTypeOrNull()),
            isSongFileChanged.toString()
                .toRequestBody("text/plain".toMediaTypeOrNull()),
            audioFileRequestBody,
            coverImageBody
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {
                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {
                        currrentStatus = UploadProgressRequestBody.COMPLETED
                        /*
                        Save data in model
                         */
                        recordModel = UploadRecordDataModel()
                        recordModel.recordId = any.id
                        recordModel.recordFiledId = any.recordFileId.toString()
                        recordModel.songCoverImage = any.songCover
                        recordModel.isCoverImageChange = true
                        recordModel.isCoverImageChange = true
                        fg.uploadRecordDataViewModel.setUplaodSongFirstStepData(recordModel)
                        fg.uploadRecordDataViewModel.setSongCoverImage(
                            any?.songCover!!,
                            any?.recordsTitle!!, selectedRecordedFile.toString()
                        )
                        fg.uploadRecordDataViewModel.setRecordCoverImage(true)
                        fg.uploadRecordDataViewModel.setRecordFileChanges(true)
                        /*
                        View Visivility
                         */
                        fg.binding.tvUploadingPercentage.visibility = View.GONE
                        fg.binding.ivPlay.visibility = View.VISIBLE
                        fg.binding.tvSongTitleName.visibility = View.VISIBLE
                        listener.onFinish(UploadProgressRequestBody.API_SUCCESS, any)

                        fg.uploadRecordDataViewModel.setIsRecordSuccessfullyUploaded(true)
                        viewPagerListener.onSuccess()
                    } else {

                        viewPagerListener.failed(any?.responseMessage!!)
                    }
                } else {
                    fg.hideProcessDialog()
                    /* listener.onError(
                         UploadProgressRequestBody.RETRY,
                         RetroError(
                             RetroError.Kind.UNEXPECTED,
                             "server response error Response Status: ${any?.responseStatus ?: 0}",
                             -999
                         )
                     )*/
                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()
                listener.onError(
                    UploadProgressRequestBody.RETRY,
                    RetroError(RetroError.Kind.UNEXPECTED, message, -999)
                )
            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()

                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    listener.onError(UploadProgressRequestBody.RETRY, error)
                }
            }

        })


    }

    private fun getObserver(
        recordFileUri: Uri?,
        disposableObserver: DisposableObserver<File>
    ): DisposableObserver<File> {
        return getObservable(recordFileUri)
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(disposableObserver)
    }

    private fun getObservable(recordFileUri: Uri?): Observable<File> {
        return Observable.create { emitter ->
            val file = UploadImageHelper.copyFileFromStreamWithNull(
                fg.requireContext(),
                recordFileUri!!,
               getFileNameNew(fg.requireContext(), recordFileUri)
            )

            if (file != null) {
                emitter.onNext(file)
                emitter.onComplete()
            } else {
                emitter.onError(Throwable("File not found"))
            }
        }
    }

    override fun onProgressUpdate(status: String, progress: Int) {
        currrentStatus = UploadProgressRequestBody.RUNNING
        currentProgressValue = progress
        fg.listenere.uploadStatus(status)

        fg.binding.tvUploadingPercentage.text =
            with("Uploading...0") {
                replace("0", "${progress}%")
            }
    }

    override fun onFinish(status: String, response: UploadRecordFileResponse) {

        Toast.makeText(fg.requireContext(), "Record file successfully uploaded", Toast.LENGTH_SHORT)
            .show()

    }

    override fun onError(status: String, error: RetroError) {
        Log.d("Error", error.toString())
        currrentStatus = ""


    }

    fun getFilePathFromUri(): File {
        val file = UploadImageHelper.copyFileFromStreamWithNull(
            fg.requireContext(),
            recordFileUri!!,
            getFileNameNew(fg.requireContext(), recordFileUri)
        )
        return file!!
    }


    fun getSongUrl(songId: Int, listener: UploadSongViewPageAdapter.Communicator) {
        if (!fg.isOnline()) {
            showToast(
                fg.requireActivity(),
                fg.requireActivity().getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        val helper: ApiAuthHelper?

        helper = ApiClient.getClientMusic().create(ApiAuthHelper::class.java)

        val request = mp3UrlRequest()
        request.fileId = songId
        request.requestHeader.deviceIP = "106.201.10.241"  //TODO remove hard code IP
        val call = helper.getSongUrl(request)
        call.enqueue(object : CallBackManager<mp3Response>() {
            override fun onSuccess(any: mp3Response?, message: String) {
                val response = any as mp3Response
                if (response.responseStatus != 1 || response.response?.songFilePath.isNullOrEmpty()) {
                    response.responseMessage?.let {
                        showToast(
                            fg.requireActivity(),
                            it,
                            CustomToast.ToastType.FAILED
                        )
                    }
                    return
                }

                response?.response?.let {
                    val songUrl = it.songFilePath
                    fg.songFilePathUrl = songUrl
                    listener.onSongClick(songUrl, true)

                }
            }

            override fun onFailure(message: String) {
                showToast(
                    fg.requireActivity(),
                    message,
                    CustomToast.ToastType.NETWORK
                )
            }

            override fun onError(error: RetroError) {
                fg.requireActivity()
            }

        })

    }
    fun getFileNameNew(context: Context, localSongsUri: Uri?): String {
        var fileName = "Song"
        if (localSongsUri == null) {
            return fileName
        }
        val projection: Array<String> = arrayOf(OpenableColumns.DISPLAY_NAME)
        val cr =
            context.contentResolver.query(localSongsUri, projection, null, null, null)
        try {
            if (cr != null) {
                if (cr.moveToFirst()) {
                    val nameIndex: Int = cr.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                    fileName = cr.getString(nameIndex)
                }
            }
        } catch (e: Exception) {

        }
        return fileName
    }
}