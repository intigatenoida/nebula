package com.nibula.upload_music.ui.uploadrecord.helper

import com.nibula.base.BaseHelperFragment
import com.nibula.response.uploadrecord.GetRecordIdResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.GetStartFragment
import com.nibula.upload_music.ui.uploadrecord.model.UploadRecordDataModel
import com.nibula.utils.AppConstant

import retrofit2.Call
class GetStartedHelper(
    val fg: GetStartFragment,
    var viewPagerListener: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {
    lateinit var call: Call<GetRecordIdResponse>
     fun getRecordIdAndFileId() {
        fg.showProcessDialog()
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.getRecordIdAndFileId(AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<GetRecordIdResponse>() {
            override fun onSuccess(any: GetRecordIdResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {
                        val recordDataModel = UploadRecordDataModel()
                        recordDataModel.recordFiledId = any.recordFileID
                        recordDataModel.recordId = any.recordId
                        fg.uploadRecordDataViewModel.setUplaodSongFirstStepData(recordDataModel)
                    } else {

                        viewPagerListener.failed(any?.responseMessage!!)
                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

}