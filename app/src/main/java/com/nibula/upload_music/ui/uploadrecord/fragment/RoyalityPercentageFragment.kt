package com.nibula.upload_music.ui.uploadrecord.fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.RoyalityPercentageFragmentBinding
import com.nibula.databinding.RoyalityPercentageNewBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.KeyboardaAdapterNew
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.RoyaltyPercentageHelper
import com.nibula.upload_music.ui.uploadrecord.model.KeyboardModel
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.utils.CommonUtils


class RoyalityPercentageFragment : BaseFragment(), KeyboardaAdapterNew.onItemClick {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): RoyalityPercentageFragment {
            val fg = RoyalityPercentageFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var keyboardAdapter: KeyboardaAdapterNew
    lateinit var royaltyPercentageHelper: RoyaltyPercentageHelper
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    var modelTypeArrayList = arrayListOf<KeyboardModel>()
    val listener: KeyboardaAdapterNew.onItemClick = this@RoyalityPercentageFragment
    private var isPercentageToSell: Boolean = false
    private var isPercentageToOwn: Boolean = true
    lateinit var binding:RoyalityPercentageNewBinding

    @SuppressLint("CutPasteId")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.royality_percentage_new, container, false)
            binding= RoyalityPercentageNewBinding.inflate(inflater,container,false)
            rootView=binding.root

            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            royaltyPercentageHelper = RoyaltyPercentageHelper(this, listenere)
            rootView.findViewById<RecyclerView>(R.id.recyclerView).layoutManager =
                GridLayoutManager(requireContext(), 3)
            keyboardAdapter = KeyboardaAdapterNew(requireContext(), modelTypeArrayList, this)
            rootView.findViewById<RecyclerView>(R.id.recyclerView).adapter = keyboardAdapter
            keypadListener(rootView)

            binding.edtAmount.setTextColor(Color.parseColor("#4CD964"))
            binding.tvPercentage.setTextColor(Color.parseColor("#4CD964"))
            binding.edtAmount.setHintTextColor(Color.parseColor("#4CD964"))

            if (uploadRecordDataViewModel?.isRecordFromSaveAsDraft?.value!!) {
                val completedStepNumber =
                    uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.stepNumber
                if (completedStepNumber!! >= 7) {
                    setEightStepData()
                }
            }

            binding.edtpercentage.setOnClickListener {
                isPercentageToOwn = false
                isPercentageToSell = true
                binding.edtpercentage.setTextColor(Color.parseColor("#4CD964"))
                binding.edtAmount.setTextColor(Color.parseColor("#ffffff"))
                binding.tvPercentage.setTextColor(Color.parseColor("#ffffff"))
                binding.tvPercentageToSell.setTextColor(Color.parseColor("#4CD964"))

                binding.edtpercentage.setHintTextColor(Color.parseColor("#4CD964"))
                binding.edtAmount.setHintTextColor(Color.parseColor("#ffffff"))

            }
            binding.edtAmount.setOnClickListener {
                isPercentageToOwn = true
                isPercentageToSell = false
                binding.edtpercentage.setTextColor(Color.parseColor("#ffffff"))
                binding.edtAmount.setTextColor(Color.parseColor("#4CD964"))
                binding.tvPercentage.setTextColor(Color.parseColor("#4CD964"))
                binding.tvPercentageToSell.setTextColor(Color.parseColor("#ffffff"))
                binding.edtAmount.setHintTextColor(Color.parseColor("#4CD964"))
                binding.edtpercentage.setHintTextColor(Color.parseColor("#ffffff"))

            }
        }
        return rootView
    }


    fun onKeyboardItemClick(text: String) {
        if (isPercentageToOwn) {

            var display: String = binding.edtAmount?.getText().toString()
            if (text.equals("Clear", ignoreCase = true)) {
                if (!TextUtils.isEmpty(display)) {
                    display = display.substring(0, display.length - 1)
                    binding.edtAmount?.setText(display)
                }
            } else {
                if (text == "." && display.contains(".")) {

                } else {
                    display += text
                }
                binding.edtAmount?.setText(display)
            }
        } else {
            var display: String = binding.edtpercentage?.getText().toString()
            if (text.equals("Clear", ignoreCase = true)) {
                if (!TextUtils.isEmpty(display)) {
                    display = display.substring(0, display.length - 1)
                    binding.edtpercentage?.setText(display)
                }
            } else {
                if (text == "." && display.contains(".")) {

                } else {
                    display += text
                }
                binding.edtpercentage?.setText(display)
            }
        }

    }

    override fun itemClick(textValue: String) {
        onKeyboardItemClick(textValue)
    }

    fun validation(): Boolean {
        return royaltyPercentageHelper.checkValidation()
    }

    private fun keypadListener(rootView: View) {
        with(rootView) {
            binding.tvOne.setOnClickListener {
                listener.itemClick("1")
            }
            binding.tvTwo.setOnClickListener {
                listener.itemClick("2")
            }
            binding.tvThree.setOnClickListener {
                listener.itemClick("3")
            }
            binding.tvFour.setOnClickListener {
                listener.itemClick("4")
            }
            binding.tvFive.setOnClickListener {
                listener.itemClick("5")
            }
            binding.tvSix.setOnClickListener {
                listener.itemClick("6")
            }
            binding.tvSeven.setOnClickListener {
                listener.itemClick("7")
            }
            binding.tvEight.setOnClickListener {
                listener.itemClick("8")
            }
            binding.tvNine.setOnClickListener {
                listener.itemClick("9")
            }
            binding.tvDot.setOnClickListener {
                listener.itemClick(".")
            }
            binding.tvZero.setOnClickListener {
                listener.itemClick("0")
            }
            binding.tvClear.setOnClickListener {
                listener.itemClick("Clear")
            }
        }

    }

    fun setEightStepData() {
        val percentageToOwn =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.artistOwnershipPercentage
        val percentageToSell =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.percentageToSell
        binding.edtAmount.setText(percentageToOwn.toString())
        binding.edtpercentage.setText(percentageToSell.toString())

        val percentageSell =
            percentageToSell?.div(100.0)
        val twoDecimalPlaces =
            percentageSell?.let { CommonUtils.setTwoDecimalPlaces(it) }

        twoDecimalPlaces?.let { uploadRecordDataViewModel.setPercentageToSell(it) }

        uploadRecordDataViewModel.setOwnerShipPercentage(binding.edtAmount.text.toString())

    }
}