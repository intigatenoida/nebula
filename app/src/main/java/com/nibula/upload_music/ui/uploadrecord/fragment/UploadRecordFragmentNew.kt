package com.nibula.upload_music.ui.uploadrecord.fragment

import android.app.Dialog
import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.bottomsheets.UploadCancelBottomSheet
import com.nibula.databinding.FragmentUploadRecordNewBinding
import com.nibula.request.upload.UploadProgressRequestBody
import com.nibula.response.saveddraft.SaveAsDraftNewDataResponse
import com.nibula.upload_music.ui.savedrafts.newuploadsavasdraft.SaveAsDraftNewActivity
import com.nibula.upload_music.ui.uploadrecord.activity.UploadRecordActivityNew
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.DiscardUploadingHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.utils.AppConstant
import com.nibula.utils.DebouncedOnClickListener
import com.nibula.utils.interfaces.CancelUplaodClicks
import com.nibula.utils.interfaces.DialogFragmentClicks


/*
As per the new design all helper is new for new uplaod flow
 */
class UploadRecordFragmentNew : BaseFragment(), UploadSongViewPageAdapter.Communicator,
    DialogFragmentClicks, CancelUplaodClicks {

    companion object {
        const val FIRST_STEP = 0
        const val SECOND_STEP = 1
        const val THIRD_STEP = 2
        const val FOURTH_STEP = 3
        const val FIFTH_STEP = 4
        const val SIX_STEP = 5
        const val SEVEN_STEP = 6
        const val EIGHT_STEP = 7
        const val NINE_STEP = 8
        const val TEN_STEP = 9
        const val ELEVEN_STEP = 10
        fun newInstance(bundle: Bundle): UploadRecordFragmentNew {
            val fg = UploadRecordFragmentNew()
            fg.arguments = bundle
            return fg
        }

        fun newInstance() = UploadRecordFragmentNew()

    }

    lateinit var rootView: View
    lateinit var closeImageView: ImageView
    lateinit var backImageView: ImageView
    lateinit var tvTitle: TextView
    private var isFinalStepReached = false
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var discardUploadingHelper: DiscardUploadingHelper
    private var currentStatus: String = ""
    lateinit var recordId: String
    private lateinit var saveAsDraftData: SaveAsDraftNewDataResponse
    lateinit var binding: FragmentUploadRecordNewBinding
    lateinit var ivPlay: ImageView
    lateinit var ivPause: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
            // binding = inflater.inflate(R.layout.fragment_upload_record_new, container, false)
            binding = FragmentUploadRecordNewBinding.inflate(inflater, container, false)
            rootView = binding.root
            initUI()
            if (arguments != null) {
                saveAsDraftData = arguments?.getParcelable("draft_data")!!
                if (saveAsDraftData.response?.id.toString()
                        .isNotEmpty() && saveAsDraftData.response?.id.toString().isNotEmpty()
                ) {
                    uploadRecordDataViewModel.setIsRecordFromSaveAsDraft(true)
                    uploadRecordDataViewModel.setSaveAsDraftResponseData(saveAsDraftData)

                } else {
                    uploadRecordDataViewModel.setIsRecordFromSaveAsDraft(false)
                }
            } else {
                uploadRecordDataViewModel.setIsRecordFromSaveAsDraft(false)

            }

            binding.ivNextNew.visibility = View.GONE
            binding.ivPreviousNew.visibility = View.GONE

            binding.tvPercetageNew.text = "0% Completed"
            closeImageView.visibility = View.GONE
            binding.dotsIndicatorNew.setDotSelection(1)

            uploadRecordDataViewModel.currentUploadingStatus.observe(viewLifecycleOwner, {
                currentStatus = it

            })

            binding.layoutTb.ivclose.setOnClickListener {

                if (currentStatus == UploadProgressRequestBody.RUNNING) {
                    /* Toast.makeText(
                         requireContext(),
                         "Please wait till the uploading finished...",
                         Toast.LENGTH_SHORT
                     ).show()
                     return@setOnClickListener*/

                    val bPrompt = UploadCancelBottomSheet(
                        getString(R.string.cancel_upload),
                        getString(R.string.text_continue),
                        getString(R.string.discard_changes),
                        this@UploadRecordFragmentNew,
                        SpannableStringBuilder(""),
                        false,
                        3
                    )
                    bPrompt.isCancelable = true
                    bPrompt.setColor(
                        ContextCompat.getColor(requireContext(), R.color.colorAccent),
                        ContextCompat.getColor(requireContext(), R.color.white),
                        ContextCompat.getColor(requireContext(), R.color.gray_1_51per),
                        ContextCompat.getColor(requireContext(), R.color.gray_1_51per)
                    )
                    bPrompt.show(
                        this@UploadRecordFragmentNew.childFragmentManager,
                        "PositiveNegativeForCancelUplaod"
                    )
                } else if (currentStatus == "") {

                    requireActivity().finish()

                } else {
                    val bPrompt = PNBSheetFragment(
                        getString(R.string.message_saved_draft),
                        getString(R.string.save_as_draft),
                        getString(R.string.discard_changes),
                        this@UploadRecordFragmentNew,
                        SpannableStringBuilder(""),
                        false,
                        3
                    )
                    bPrompt.isCancelable = true
                    bPrompt.setColor(
                        ContextCompat.getColor(requireContext(), R.color.colorAccent),
                        ContextCompat.getColor(requireContext(), R.color.white),
                        ContextCompat.getColor(requireContext(), R.color.gray_1_51per),
                        ContextCompat.getColor(requireContext(), R.color.gray_1_51per)
                    )
                    bPrompt.show(
                        this@UploadRecordFragmentNew.childFragmentManager,
                        "PositiveNegativeBottomSheetFragment"
                    )
                }

            }

            binding.ivNextNew.setOnClickListener(object :
                DebouncedOnClickListener(AppConstant.TIME_TO_WAIT_NEXT) {
                override fun onDebouncedClick(v: View?) {
                    nextStep()

                }
            })
            binding.ivPreviousNew.setOnClickListener(object :
                DebouncedOnClickListener(AppConstant.TIME_TO_WAIT_NEXT) {
                override fun onDebouncedClick(v: View?) {
                    prevStep()

                }
            })


        }
        requireActivity().window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
        )

        return rootView
    }

    private fun initUI() {
        closeImageView = rootView.findViewById(R.id.ivclose)
        backImageView = rootView.findViewById(R.id.img_home)
        tvTitle = rootView.findViewById(R.id.tv_title)
        ivPlay = rootView.findViewById(R.id.ivPlay)
        ivPause = rootView.findViewById(R.id.ivPause)
        uploadRecordDataViewModel = activity?.run {
            ViewModelProvider(this)[UploadRecordDataViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        binding.vpgMain.setSwipeAble(true)
        discardUploadingHelper = DiscardUploadingHelper(this@UploadRecordFragmentNew)
        binding.layoutTb.imgHome.setOnClickListener {
            if (binding.vpgMain.currentItem == 0) {
                requireActivity().finish()
            } else {
                prevStep()
            }

        }
        setAdapter()
    }

    private fun setAdapter() {
        val adapter = UploadSongViewPageAdapter(requireActivity().supportFragmentManager, this)
        binding.vpgMain.adapter = adapter
        binding.vpgMain.offscreenPageLimit = 13
        binding.vpgMain.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                when (position) {

                    FIRST_STEP -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.GONE
                        binding.ivPreviousNew.visibility = View.GONE
                        binding.tvPercetageNew.text = "0% Completed"
                        binding.dotsIndicatorNew.setDotSelection(1)
                        closeImageView.visibility = View.GONE
                        backImageView.visibility = View.VISIBLE


                    }
                    SECOND_STEP -> {
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "25% Completed"
                        binding.dotsIndicatorNew.setDotSelection(2)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }

                    THIRD_STEP -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "50% Completed"
                        binding.dotsIndicatorNew.setDotSelection(3)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }


                    FOURTH_STEP -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "50% Completed"
                        binding.dotsIndicatorNew.setDotSelection(3)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }


                    FIFTH_STEP -> {
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "75% Completed"
                        binding.dotsIndicatorNew.setDotSelection(4)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }


                    SIX_STEP -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "75% Completed"
                        binding.dotsIndicatorNew.setDotSelection(4)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }


                    5 -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "75% Completed"
                        binding.dotsIndicatorNew.setDotSelection(4)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }

                    6 -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "75% Completed"
                        binding.dotsIndicatorNew.setDotSelection(4)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }

                    7 -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "75% Completed"
                        binding.dotsIndicatorNew.setDotSelection(4)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }

                    8 -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "75% Completed"
                        binding.dotsIndicatorNew.setDotSelection(4)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }


                    9 -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "80% Completed"
                        binding.dotsIndicatorNew.setDotSelection(4)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }

                    10 -> {
                        binding.tvPercetageNew.visibility = View.VISIBLE
                        binding.dotsIndicatorNew.visibility = View.VISIBLE
                        binding.ivNextNew.visibility = View.VISIBLE
                        binding.ivPreviousNew.visibility = View.VISIBLE
                        binding.tvPercetageNew.text = "Last Step"
                        binding.dotsIndicatorNew.setDotSelection(5)
                        closeImageView.visibility = View.VISIBLE
                        backImageView.visibility = View.INVISIBLE
                    }

                    11 -> {
                        binding.ivNextNew.visibility = View.GONE
                        binding.ivPreviousNew.visibility = View.GONE
                        binding.tvPercetageNew.visibility = View.GONE
                        binding.dotsIndicatorNew.visibility = View.GONE
                        closeImageView.visibility = View.INVISIBLE
                        backImageView.visibility = View.INVISIBLE
                        tvTitle.text = "Review & Submit!"
                        tvTitle.setTextColor(Color.parseColor("#FFFFFF"))

                    }
                    12 -> {
                        isFinalStepReached = true
                        tvTitle.text = "Success"
                        tvTitle.setTextColor(Color.parseColor("#4CD964"))

                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })
    }

    override fun nextStep() {

        when (binding.vpgMain.currentItem) {
            FIRST_STEP -> {

                if (binding.vpgMain.currentItem < 12) {
                    binding.vpgMain.currentItem = binding.vpgMain.currentItem + 1
                } else {
                    // setFragment(GetStartFragment.newInstance(), R.id.maincontainer, false)
                }
                if (!this::mediaPlayer.isInitialized) return
                if (mediaPlayer.isPlaying) {
                    mediaPlayer.pause()
                    ivPlay.visibility = View.VISIBLE
                    ivPause.visibility = View.INVISIBLE
                }

            }
            SECOND_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[2]
                (fragment as UploadSongFragment).validation()
                if (!this::mediaPlayer.isInitialized) return
                if (mediaPlayer.isPlaying) {
                    mediaPlayer.pause()
                    ivPlay.visibility = View.INVISIBLE
                    ivPause.visibility = View.INVISIBLE
                }
            }
            THIRD_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[3]
                (fragment as SongTitleFragment).validation()
            }
            FOURTH_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[4]
                (fragment as AddArtistFragment).validation()
            }

            FIFTH_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[5]
                (fragment as GenreListFragment).validation()
            }

            SIX_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[6]
                (fragment as SongDurationFragment).validation()
            }


            SEVEN_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[7]
                (fragment as MoneyRaisedFragment).validation()
            }

            EIGHT_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[8]
                (fragment as RoyalityPercentageFragment).validation()
            }

            NINE_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[9]
                (fragment as TokenSellFragment).validation()
            }


            TEN_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[10]
                (fragment as CopyRightCodeFragment).validation()
            }

            ELEVEN_STEP -> {
                val fragment = requireActivity().supportFragmentManager.fragments[11]
                (fragment as LaunchDateFragment).validation()
            }

            11 -> {
                val fragment = requireActivity().supportFragmentManager.fragments[12]

                binding.layoutTb.root.visibility = View.GONE
            }

            12 -> {
                val isSaveAsDraft = uploadRecordDataViewModel.isRecordFromSaveAsDraft.value
                if (isSaveAsDraft!!) {
                    (requireContext() as SaveAsDraftNewActivity).onBackPressed()

                } else {
                    (requireContext() as UploadRecordActivityNew).onBackPressed()

                }
            }
        }

    }

    override fun prevStep() {
        if (binding.vpgMain.currentItem > 0) {
            binding.vpgMain.currentItem -= 1
            if (!this::mediaPlayer.isInitialized) return
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
               ivPlay.visibility = View.VISIBLE
               ivPause.visibility = View.INVISIBLE
            }
        }
    }

    override fun onSuccess() {
        if (binding.vpgMain.currentItem < 13) {
            binding.vpgMain.currentItem = binding.vpgMain.currentItem + 1
        } else {
            (requireContext() as UploadRecordActivityNew).onBackPressed()
        }
    }

    override fun failed(responseMessage: String) {

        //Toast.makeText(requireContext(), responseMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onSongClick(songUri: String, isPlay: Boolean) {
        if (isPlay) {
            mediaPlayer = MediaPlayer()
            mediaPlayer.setDataSource(songUri)
            mediaPlayer.prepare()
            mediaPlayer.start()
        } else {
            mediaPlayer.pause()
        }

    }

    override fun onPause() {
        super.onPause()
        if (!this::mediaPlayer.isInitialized) return
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
            ivPlay.visibility = View.VISIBLE
            ivPause.visibility = View.INVISIBLE
        }
    }

    override fun releaseSong() {
        if (!this::mediaPlayer.isInitialized) return
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
        }
    }

    override fun uploadStatus(status: String) {
        currentStatus = status
    }

    fun onBack() {
        if (isFinalStepReached) {
            requireActivity().finish()
        } else {
            prevStep()
        }
    }

    fun cropData(uri: Uri?) {
        val fragment = requireActivity().supportFragmentManager.fragments[2]
        (fragment as UploadSongFragment).cropData(uri)
    }

    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {

        Toast.makeText(requireContext(), "Save as draft successfully", Toast.LENGTH_SHORT).show()
        requireActivity().finish()
        dialog.dismiss()
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        discardUploadingHelper.discardUploading()
        dialog.dismiss()

    }

    override fun onCancelUpload(obj: Any, bundle: Bundle?, dialog: Dialog) {
        val fragment = requireActivity().supportFragmentManager.fragments[2]
        (fragment as UploadSongFragment).newUploadFragmentHelper.call.cancel()
        requireActivity().finish()
        dialog.dismiss()
    }

    override fun onContinue(obj: Any, bundle: Bundle?, dialog: Dialog) {
        dialog.dismiss()
    }

}