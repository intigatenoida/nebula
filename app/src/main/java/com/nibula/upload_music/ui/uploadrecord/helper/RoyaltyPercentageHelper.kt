package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import com.nibula.base.BaseHelperFragment
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.*
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.RoyalityPercentageFragment
import com.nibula.utils.CommonUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call

class RoyaltyPercentageHelper(
    val fg: RoyalityPercentageFragment,
    var listenere: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {

    lateinit var call: Call<UploadRecordFileResponse>

    private fun royaltyPercentageRequest() {
        var percentageValue: String
        fg.showProcessDialog()
        percentageValue = if (!fg.binding.edtAmount.text.toString().contains(".")) {

            fg.binding.edtAmount.text.toString() + ".00"
        } else {
            fg.binding.edtAmount.text.toString()
        }
        fg.showProcessDialog()
        var recordId: String = ""
        var recordFiledId: String = ""

        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.royaltyPercentageREquest(
            7.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            percentageValue.trim().toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.binding.edtpercentage.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {
                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {
                        listenere.onSuccess()

                        val percentageSell =
                            fg.binding.edtpercentage.text.toString().toFloat() / 100.0
                        val twoDecimalPlaces =
                            percentageSell?.toDouble()?.let { CommonUtils.setTwoDecimalPlaces(it) }
                        fg.uploadRecordDataViewModel.setOwnerShipPercentage(fg.binding.edtAmount.text.toString())

                        twoDecimalPlaces?.let { fg.uploadRecordDataViewModel.setPercentageToSell(it) }

                    } else {
                        listenere.failed(any?.responseMessage!!)
                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

    fun checkValidation(): Boolean {
        if (fg.binding.edtAmount.text.toString().isEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Please enter percentage",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else if (fg.binding.edtAmount.text.toString() == ".") {
            Toast.makeText(
                fg.requireContext(),
                "Please enter a valid percentage",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else if (fg.binding.edtAmount.text.toString().toFloat() < 0.01f) {
            Toast.makeText(
                fg.requireContext(),
                "Percentage ownership should be greater than 0.01%",
                Toast.LENGTH_SHORT
            )
                .show()
        }


        else if (fg.binding.edtAmount.text.toString().toFloat() > 100f) {
            Toast.makeText(
                fg.requireContext(),
                "Percentage ownership should be less than 100%",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (fg.binding.edtAmount.text.toString().endsWith(".")) {
            Toast.makeText(
                fg.requireContext(),
                "Incorrect percentage",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (fg.binding.edtpercentage.text.toString().isEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Please enter percentage to sell",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (fg.binding.edtpercentage.text.toString()?.toFloat()!! <= 0.0) {
            Toast.makeText(
                fg.requireContext(),
                "Please enter percentage to sell",
                Toast.LENGTH_SHORT
            )
                .show()
        }
        else if (fg.binding.edtpercentage.text.toString().toFloat() < 0.01f) {
            Toast.makeText(
                fg.requireContext(),
                "Percentage to sell should be greater than 0.01%",
                Toast.LENGTH_SHORT
            )
                .show()
        }
        else if (fg.binding.edtpercentage.text.toString()?.toFloat()!! > 100) {
            Toast.makeText(
                fg.requireContext(),
                "Percentage to sell should be less than 100%",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (fg.binding.edtpercentage.text.toString()
                .toFloat() > fg.binding.edtAmount.text.toString()?.toFloat()!!
        ) {
            Toast.makeText(
                fg.requireContext(),
                "Percentage to sell should be less than equal to percentage to own",
                Toast.LENGTH_SHORT
            )
                .show()
        } else {
            royaltyPercentageRequest()
        }
        return true
    }


    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }
}