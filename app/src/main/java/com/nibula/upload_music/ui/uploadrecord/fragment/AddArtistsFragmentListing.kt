package com.nibula.upload_music.ui.uploadrecord.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.DBHandler.NebulaDBHelper
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentArtistsListingBinding
import com.nibula.response.globalsearchResponse.GlobalSearchResponseItem
import com.nibula.search.vmodel.SearchViewModel
import com.nibula.upload_music.ui.uploadrecord.adapter.RecentSearchedArtistsAdapter
import com.nibula.upload_music.ui.uploadrecord.adapter.SearchArtistsAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.ArtistsHelper

import java.util.*
import kotlin.collections.ArrayList

class AddArtistsFragmentListing : BaseFragment(),
    RecentSearchedArtistsAdapter.onRecentSearchClickListener,
    SearchArtistsAdapter.onRecentSearchClickListener {

    companion object {
        fun newInstance() = AddArtistsFragmentListing()
    }

    var isLoading = false
    var isDataAvailable = true
    var requestType = 1
    var currentPage = 1
    private val recentSearchList = ArrayList<String>()
    val recentSearchListNew = ArrayList<GlobalSearchResponseItem>()

    private lateinit var viewModel: SearchViewModel
    private lateinit var dbHelper: NebulaDBHelper

    lateinit var rootView: View
    lateinit var adapter: SearchArtistsAdapter
    private lateinit var recentAdapter: RecentSearchedArtistsAdapter
    private lateinit var helper: ArtistsHelper
    lateinit var timer: Timer
    var artistsIdsArrayList = arrayListOf<String>()

lateinit var binding:FragmentArtistsListingBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_artists_listing, container, false)
            binding=FragmentArtistsListingBinding.inflate(inflater,container,false)
            rootView=binding.root
            dbHelper = NebulaDBHelper(requireContext())
            recentSearchListNew.addAll(dbHelper.searchDataNew)
            helper = ArtistsHelper(this)
            intiUI()
        }
        return rootView
    }

    private fun intiUI() {

        //https://stagegateway1.nebu.la/api/searchnshare/Search?requestType=3&search_text=Devans&currentPage=1
        binding.edSearchUser.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val s = binding.edSearchUser.text
                if (s != null &&
                    s.toString().trim().isNotEmpty()
                ) {
                    currentPage = 1
                    helper.getSearchData(3, s.toString().trim(), currentPage, true)
                } else {
                    binding.layoutNoDataSearch.root.visibility = GONE
                    binding.gdRecentSearches.visibility = VISIBLE
                    binding.gdSearches.visibility = GONE
                    binding.ctbSearchFilter.visibility = GONE
                }
//                hideKeyboard()
            }
            true
        }
        binding.edSearchUser.addTextChangedListener(object : TextWatcher {
            private val DELAY: Long = 1500 // Milliseconds to delay api call
            override fun afterTextChanged(s: Editable?) {
                timer = Timer()
                timer.schedule(
                    object : TimerTask() {
                        override fun run() {
                            hitSearchAPi(s)
                        }
                    },
                    DELAY
                )
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (::timer.isInitialized) {
                    timer.cancel();
                }
            }

        })


        binding.imgClose.setOnClickListener {
            binding.edSearchUser.setText("")

        }
        binding.tvDone.setOnClickListener {

            goBack()
        }


        setSearchAdapter()
        setRecentSearchAdapter()
        if (recentSearchListNew.isEmpty()) {
            binding.edSearchUser.requestFocus()
            binding.gdRecentSearches.visibility = GONE
        }

        binding.spSearch.setOnRefreshListener {
            if (binding.edSearchUser.text.toString().trim().isEmpty()) {
                binding.spSearch.isRefreshing = false
            } else {
                currentPage = 1
                helper.getSearchData(
                    3,
                    binding.edSearchUser.text.toString().trim(),
                    currentPage,
                    true
                )
            }
        }


    }

    private fun hitSearchAPi(s: Editable?) {
        activity?.runOnUiThread {
            if (s != null &&
                s.toString().isNotEmpty()
            ) {
                binding.imgClose.visibility = VISIBLE
                if (s.toString().trim().length > 1) {
                    currentPage = 1
                    /* dbHelper.insertSearchData(s.toString().trim())
                     if (!recentSearchList.contains(s.toString().trim())) {
                         recentSearchList.add(s.toString().trim())
                         recentAdapter.addData(s.toString().trim())
                     }*/
                    helper.getSearchData(3, s.toString().trim(), currentPage, true)
                }
            } else {
                if (recentSearchListNew.isEmpty()) {
                    binding.gdRecentSearches.visibility = GONE
                } else {
                    binding.gdRecentSearches.visibility = VISIBLE
                }
                recentSearchListNew.clear()
                binding.layoutNoDataSearch.root.visibility = GONE
                binding.gdSearches.visibility = GONE
                binding.ctbSearchFilter.visibility = GONE
                binding.imgClose.visibility = GONE
            }
        }
    }


    private fun setRecentSearchAdapter() {

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvRecentSearches.layoutManager = layoutManager
        recentAdapter = RecentSearchedArtistsAdapter(requireContext(), this)
        binding.rvRecentSearches.adapter = recentAdapter
        recentAdapter.addData(recentSearchListNew)

    }

    private fun setSearchAdapter() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvSearches.layoutManager = layoutManager

        adapter = SearchArtistsAdapter(requireContext(), this)
        binding.rvSearches.adapter = adapter

        binding.rvSearches.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastCompletelyVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    lastVisibleItem == (totalItem - 1)
                ) {
                    helper.getSearchData(
                        3,
                        binding.edSearchUser.text.toString().trim(),
                        currentPage,
                        false
                    )
                }
            }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
    }


    fun updateRecentUpdateNew(s: GlobalSearchResponseItem) {
        val artistModel = GlobalSearchResponseItem()
        artistModel.recordArtistName = s.recordOrUserName
        artistModel.recordOrUserImage = s.recordOrUserImage
        artistModel.recordArtistName = s.recordArtistName
        artistModel.id = s.id
        dbHelper.insertSearchDataNew(s.id, s.recordOrUserName, s.userName, s.recordOrUserImage)
        if (!recentSearchListNew.contains(artistModel)) {
            recentSearchListNew.add(artistModel)
            recentAdapter.addData(recentSearchListNew)
        }
    }

    override fun onAdd(data: String) {


    }

    override fun onRemove(data: String) {

    }


    fun goBack() {
        val sendIntent = Intent()
        sendIntent.putExtra(
            "artists_ids", artistsIdsArrayList
        )


        if (adapter.searchData.size > 0) {
            sendIntent.putExtra(
                "search_data", adapter.searchData
            )

        } else {
            sendIntent.putExtra(
                "search_data", recentAdapter.recentDataList!!
            )
        }
        requireActivity().setResult(Activity.RESULT_OK, sendIntent)
        requireActivity().finish()
    }
}