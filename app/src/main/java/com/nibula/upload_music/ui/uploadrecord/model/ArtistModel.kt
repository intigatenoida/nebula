package com.nibula.upload_music.ui.uploadrecord.model

import androidx.annotation.Keep
import java.util.*


@Keep
data class ArtistModel(
    var artistId: String? = "",
    var artistName: String? = "",
    var artistUserName: String? = "",
    var artistUserImage: String? = "",
    var isSelectedArtist: Boolean = false
) : Object() {
    override fun equals(other: Any?): Boolean =
        other is ArtistModel && other.artistName == artistName && other.artistUserImage == artistUserImage && other.artistUserName == artistUserName

}
