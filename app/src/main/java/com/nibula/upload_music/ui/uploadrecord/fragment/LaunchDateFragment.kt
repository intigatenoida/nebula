package com.nibula.upload_music.ui.uploadrecord.fragment

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.Observer
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.vmodel.BaseActivityViewModel
import com.nibula.databinding.LaunchDateFragmentBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.LanuchDateHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.utils.CommonUtils
import com.nibula.utils.TimestampTimeZoneConverter
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog

import java.text.SimpleDateFormat

import java.util.*

class LaunchDateFragment : BaseFragment() {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): LaunchDateFragment {
            val fg = LaunchDateFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    private var dpd: com.wdullaer.materialdatetimepicker.date.DatePickerDialog? = null
    private var tpd: TimePickerDialog? = null
    var fromCalendar: Calendar? = null
    var toCalendar: Calendar? = null

    var localFromCalendar: Calendar? = null
    var localToCalendar: Calendar? = null
    var releseCalendar: Calendar? = null
lateinit var binding:LaunchDateFragmentBinding
    lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    lateinit var launDateHelper: LanuchDateHelper
    var isApprovedInvestor: Boolean = true
    lateinit var baseActivityViewModel: BaseActivityViewModel

    var timeZone: String = ""
    var timeZoneString: String = ""
    var isRecordSuccessfullyUploaded: Boolean = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {

            //rootView = inflater.inflate(R.layout.launch_date_fragment, container, false)
            binding= LaunchDateFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            binding.dotsIndicator.setDotSelection(4)
            val calender = Calendar.getInstance()
            timeZone = calender.timeZone.id
            timeZoneString = calender.timeZone.getDisplayName()

            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")

            uploadRecordDataViewModel.isRecordSuccessfullyUploaded.observe(viewLifecycleOwner,
                Observer {
                    isRecordSuccessfullyUploaded = true
                })
            binding.tvReviewSong.setOnClickListener {

                launDateHelper.checkValidation()
            }
            uploadRecordDataViewModel.songCoverImage.observe(viewLifecycleOwner, Observer {
                CommonUtils.loadAsBitmap(
                    requireContext(),
                    binding.clImage,
                    null,
                    null,
                    it,
                    R.drawable.recent_blank
                )
            })

            binding.sbHide.setOnCheckedChangeListener { view, isChecked ->

                when (isChecked) {
                    true -> {
                        isApprovedInvestor = true
                        binding.tvMessageHideCoOwners.setText(R.string.yes)
                    }
                    else -> {
                        binding.tvMessageHideCoOwners.setText(R.string.no)
                        isApprovedInvestor = false

                    }

                }
            }
            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")

            /* CommonUtils.loadImageWithOutCache(
                 requireContext(),
                 uploadRecordDataViewModel.uploadRecordDataModel.value!!.songCoverImage,
                 rootView.clImage,
                 R.drawable.demo_1
             )*/
            launDateHelper = LanuchDateHelper(this, listenere)
            binding.ivNext.setOnClickListener {

                listenere.nextStep()
            }
/*
            rootView.etLaunchDate.setOnClickListener {
                val dpd = DatePickerDialog(
                    requireContext(), R.style.datePicker,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        luchchDate = "$year-${monthOfYear + 1}-$dayOfMonth 06:07:00.000"
                        rootView.etLaunchDate.text =
                            "$year-${(monthOfYear + 1)}-$dayOfMonth"
                    },
                    year,
                    month,
                    day
                )

                dpd.datePicker.minDate = System.currentTimeMillis()
                dpd.show()


            }
*/
            binding.etLaunchDate.setOnClickListener {
                timeZoneChangedCheck()
                val dateSetListener =
                    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        val temp = Calendar.getInstance()
                        temp.set(year, monthOfYear, dayOfMonth)
                        temp.add(Calendar.HOUR_OF_DAY, 1)
                        localFromCalendar = temp.clone() as Calendar
                        releseCalendar = temp.clone() as Calendar
                        updateDateLabel(binding.etLaunchDate, localFromCalendar!!)

                        binding.etLaunchTime.setText("HH:MM")
                    }

                val now = Calendar.getInstance()

//              now.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
                dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                    dateSetListener,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
                )
                dpd!!.accentColor =
                    ContextCompat.getColor(requireContext(), R.color.violet_shade_100per)
                val date = Calendar.getInstance()
                var diff: Int = 3
                Log.d("Datedsfs", "$diff")

                Log.d("Datedsfs11", "$diff")
                date.add(Calendar.DAY_OF_WEEK, diff)

                dpd!!.minDate = date

                dpd!!.setOnCancelListener { dialog: DialogInterface? ->
                    dpd = null
                }
                dpd!!.show(requireActivity().supportFragmentManager, "DatePickerDialog")
            }

            binding.etLaunchTime.setOnClickListener {
                if (binding.etLaunchDate.text?.toString().isNullOrEmpty() ||
                    localFromCalendar == null
                ) {
                    showToast(
                        context,
                        getString(R.string.please_select_the_date),
                        CustomToast.ToastType.FAILED
                    )
                    return@setOnClickListener
                }

                val timeSetListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute, second ->
                        if (localFromCalendar == null) {
                            showToast(
                                requireContext(),
                                getString(R.string.please_select_the_date),
                                CustomToast.ToastType.FAILED
                            )
                            return@OnTimeSetListener
                        }
                        val temp = Calendar.getInstance()
                        temp.add(Calendar.DATE, 1)
                        localFromCalendar!!.set(
                            localFromCalendar?.get(Calendar.YEAR) ?: temp.get(Calendar.YEAR),
                            localFromCalendar?.get(Calendar.MONTH) ?: temp.get(Calendar.MONTH),
                            localFromCalendar?.get(Calendar.DATE) ?: temp.get(Calendar.DATE),
                            hourOfDay,
                            minute
                        )
                        if (localFromCalendar!!.before(temp)) {
                            showToast(
                                requireContext(),
                                "Minimum 24 hour gap is required from the uploading time",
                                CustomToast.ToastType.FAILED
                            )
                            return@OnTimeSetListener
                        }
                        temp.set(
                            localFromCalendar?.get(Calendar.YEAR) ?: temp.get(Calendar.YEAR),
                            localFromCalendar?.get(Calendar.MONTH) ?: temp.get(Calendar.MONTH),
                            localFromCalendar?.get(Calendar.DATE) ?: temp.get(Calendar.DATE),
                            hourOfDay,
                            minute
                        )
                        fromCalendar = temp.clone() as Calendar

                        updateTimeLabel(binding.etLaunchTime, fromCalendar!!)

                        if (!binding.etLaunchDate.text?.toString().isNullOrEmpty()) {
                            fromCalendar?.set(
                                localFromCalendar?.get(Calendar.YEAR) ?: temp.get(Calendar.YEAR),
                                localFromCalendar?.get(Calendar.MONTH) ?: temp.get(Calendar.MONTH),
                                localFromCalendar?.get(Calendar.DATE) ?: temp.get(Calendar.DATE),
                                fromCalendar?.get(Calendar.HOUR_OF_DAY)
                                    ?: temp.get(Calendar.HOUR_OF_DAY),
                                fromCalendar?.get(Calendar.MINUTE) ?: temp.get(Calendar.MINUTE)
                            )
                        }

                    }

                tpd = TimePickerDialog.newInstance(
                    timeSetListener,
                    localFromCalendar!!.get(Calendar.HOUR_OF_DAY),
                    localFromCalendar!!.get(Calendar.MINUTE), true
                )
                tpd!!.accentColor =
                    ContextCompat.getColor(requireContext(), R.color.violet_shade_100per)
                tpd!!.setOnCancelListener {
                    tpd = null
                }
                tpd?.show(requireActivity().supportFragmentManager, "TimePickerDialog")
            }
            binding.ivPrevious.setOnClickListener {

                listenere.prevStep()
            }

        }
        return rootView
    }

    fun validation(): Boolean {
        return launDateHelper.checkValidation()
    }


    private fun updateDateLabel(tvDate: AppCompatTextView, inputCalender: Calendar) {
        val myFormat = "YYYY-MM-dd" //In which you need put here
        val sdf = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            SimpleDateFormat(myFormat, Locale.getDefault())
        } else {
            SimpleDateFormat("YYYY-MM-dd", Locale.getDefault())
        }
        tvDate.text = sdf.format(inputCalender.time)

        /* val getCurrentMilliseconds = System.currentTimeMillis()
         setTimer(inputCalender.timeInMillis.minus(getCurrentMilliseconds))*/
    }

    private fun timeZoneChangedCheck() {
        if ((fromCalendar != null &&
                    (fromCalendar!!.timeZone != Calendar.getInstance().timeZone)) ||
            (localFromCalendar != null &&
                    (localFromCalendar!!.timeZone != Calendar.getInstance().timeZone)) ||
            (toCalendar != null &&
                    (toCalendar!!.timeZone != Calendar.getInstance().timeZone)) ||
            (localToCalendar != null &&
                    (localToCalendar!!.timeZone != Calendar.getInstance().timeZone))
        ) {
            showToast(requireContext(), "Time zone changed", CustomToast.ToastType.FAILED)
        }
    }

    private fun updateTimeLabel(tvTime: AppCompatTextView, inputCalender: Calendar) {
        val myFormat = "HH:mm" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        binding.etLaunchTime.text = sdf.format(inputCalender.time).plus("($timeZoneString)")

        val getCurrentMilliseconds = System.currentTimeMillis()
        setTimer(fromCalendar?.timeInMillis!!.minus(getCurrentMilliseconds))
    }


    fun setTimer(milliseconds: Long) {
        val time =
            TimestampTimeZoneConverter.getDaysAndHourDifferenceNew(
                requireContext(),
                milliseconds
            )

        binding.relesedTime.setText(time)
    }

}