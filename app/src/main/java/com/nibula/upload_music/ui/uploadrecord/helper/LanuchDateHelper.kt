package com.nibula.upload_music.ui.uploadrecord.helper

import com.nibula.upload_music.ui.uploadrecord.fragment.LaunchDateFragment
import android.widget.Toast
import com.nibula.base.BaseHelperFragment
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.*
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.utils.TimestampTimeZoneConverter
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call

class LanuchDateHelper(
    val fg: LaunchDateFragment,
    var listenere: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {


    lateinit var call: Call<UploadRecordFileResponse>

    var recordId: String = ""
    var recordFiledId: String = ""
    var utcLaunchDate: String = ""
    var localeLaunchDate: String = ""


    private fun requestToLaunchDate() {

        fg.showProcessDialog()
        fg.showProcessDialog()

        if (fg.fromCalendar != null) {
            utcLaunchDate = TimestampTimeZoneConverter.convertToUTCDate(
                fg.fromCalendar!!.time.time
            )
        }

        if (fg.fromCalendar != null) {
            localeLaunchDate = TimestampTimeZoneConverter.convertToLocaleTimeZone(
                fg.fromCalendar!!.time.time
            )
        }

        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        // val durations = recordData.recordDurationInSeconds ?: 0
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.requestToLaunchDate(
            10.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toRequestBody("text/plain".toMediaTypeOrNull()),
            localeLaunchDate.trim().toRequestBody("text/plain".toMediaTypeOrNull()),
            utcLaunchDate.trim().toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.timeZone.trim().toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.isApprovedInvestor.toString().toRequestBody("text/plain".toMediaTypeOrNull())

        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {

                        listenere.onSuccess()
                    } else {
                        Toast.makeText(
                            fg.requireContext(),
                            any?.responseMessage!!,
                            Toast.LENGTH_SHORT
                        )
                       /* listenere.failed(any?.responseMessage!!)*/

                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

    fun checkValidation(): Boolean {
        if (!fg.isRecordSuccessfullyUploaded) {
            Toast.makeText(
                fg.requireContext(),
                "Uploading is in progress.Please wait...",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (fg.binding.etLaunchDate.text.toString().equals("YYYY-MM-DD")) {
            Toast.makeText(
                fg.requireContext(),
                "Please select drop date",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else if (fg.binding.etLaunchTime.text.toString().equals("HH:MM")) {
            Toast.makeText(
                fg.requireContext(),
                "Please select time",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else {
            requestToLaunchDate()
        }
        return true
    }


    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }
}