package com.nibula.upload_music.ui.uploadrecord.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nibula.upload_music.ui.uploadrecord.fragment.*
import com.nibula.utils.AppConstant

class UploadSongViewPageAdapter(val activity: FragmentManager, val listenere: Communicator) :
    SmartFragmentStatePagerAdapter(activity) {


    override fun getItem(position: Int): Fragment {
        val b = Bundle()
        b.putInt(AppConstant.POSITION, position)
        return when (position) {
            1 -> {
                UploadSongFragment.newInstance(b,listenere)
            }
            2 -> {
                SongTitleFragment.newInstance(b,listenere)
            }
            3 -> {
                AddArtistFragment.newInstance(b,listenere)
            }
            4 -> {
                GenreListFragment.newInstance(b,listenere)
            }

            5 -> {
                SongDurationFragment.newInstance(b,listenere)
            }

            6 -> {
                MoneyRaisedFragment.newInstance(b,listenere)
            }

            7 -> {
                RoyalityPercentageFragment.newInstance(b,listenere)
            }

            8 -> {
                TokenSellFragment.newInstance(b,listenere)
            }

            9 -> {
                CopyRightCodeFragment.newInstance(b,listenere)
            }

            10 -> {
                LaunchDateFragment.newInstance(b,listenere)
            }

            11 -> {
                FinalSubmitFragment.newInstance(b,listenere)
            }

            12 -> {
                SuccessFragment.newInstance(b,listenere)
            }
            else -> {
                GetStartFragment.newInstance(b,listenere)
            }
        }
    }

    override fun getCount(): Int {
        return 13
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }

    interface Communicator {
        fun nextStep()

        fun prevStep()

        fun onSuccess()

        fun failed(responseMessage:String)

        fun onSongClick(songFile:String,isPlay:Boolean)

        fun releaseSong()

        fun uploadStatus(status:String)

    }

}