package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import androidx.lifecycle.Observer
import com.nibula.base.BaseHelperFragment
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.SongTitleFragment
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call

class SongTitleHelper(
    val fg: SongTitleFragment,
    var viewPagerListener: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {
    lateinit var call: Call<UploadRecordFileResponse>
    private fun requestAddSongTitleWithRecordId() {
        fg.showProcessDialog()
        var recordId: String = ""
        var recordFiledId: String = ""
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.requestSavedRecordSongTitle(
            2.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.binding.etSongTitle.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {
                        fg.uploadRecordDataViewModel.songTitle(
                            fg.binding.etSongTitle.text.toString().trim()
                        )
                        viewPagerListener.onSuccess()
                    } else {

                        viewPagerListener.failed(any?.responseMessage!!)
                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

    fun checkValidation(): Boolean {
        if (fg.binding.etSongTitle.text.isNullOrEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Please enter title of the song",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else {
            requestAddSongTitleWithRecordId()
        }
        return true
    }

}