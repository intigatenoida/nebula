package com.nibula.upload_music.ui.uploadrecord.currency_res


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Currecy(
    @SerializedName("Text")
    var text: String,
    @SerializedName("Value")
    var value: Int
) {
    override fun toString(): String {
        return text
    }
}