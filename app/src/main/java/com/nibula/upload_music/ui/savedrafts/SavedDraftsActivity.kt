package com.nibula.upload_music.ui.savedrafts

import android.os.Bundle
import com.nibula.R
import com.nibula.base.BaseActivity

class SavedDraftsActivity : BaseActivity() {
	
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_saved_draft)
		if (savedInstanceState == null) {
			supportFragmentManager.beginTransaction()
				.replace(
					R.id.container,
					SaveDraftsFragment.newInstance()
				)
				.commitNow()
		}
	}
	
	@Deprecated("Deprecated in Java")
	override fun onBackPressed() {
		finish()
	}
}