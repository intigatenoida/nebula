package com.nibula.upload_music.ui.uploadrecord.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FregmnetSuccessBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.FinalSubmitHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel

class SuccessFragment : BaseFragment() {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): SuccessFragment {
            val fg = SuccessFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    private lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var finalSubmitHelper: FinalSubmitHelper
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    lateinit var binding:FregmnetSuccessBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fregmnet_success, container, false)
            binding=FregmnetSuccessBinding.inflate(inflater,container,false)
            rootView=binding.root

            binding.tvChooseFiles.setOnClickListener {
                listenere.nextStep()
            }


            rootView.setOnClickListener {


            }
        }
        return rootView
    }
}