package com.nibula.upload_music.ui.uploadrecord.fragment

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.nibula.R
import com.nibula.TermsConditionActivity
import com.nibula.base.BaseFragment
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.databinding.FinalRecordSubmitBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.FinalSubmitHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel
import com.nibula.utils.CommonUtils
import java.text.DecimalFormat

class FinalSubmitFragment : BaseFragment(), FinalSubmitHelper.OnSongClick {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): FinalSubmitFragment {
            val fg = FinalSubmitFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }
    private lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    private lateinit var finalSubmitHelper: FinalSubmitHelper
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    private lateinit var mediaPlayer: MediaPlayer
    private var songLocaleUri: String = ""
    private var selectedCurrency: String = ""
    private var isSelected: Boolean = false
    var songFilePathUrl: String = ""
    lateinit var costBreakdownResponse: CostBreakdownResponse.Response
    lateinit var binding:FinalRecordSubmitBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.final_record_submit, container, false)
            binding= FinalRecordSubmitBinding.inflate(inflater,container,false)
            rootView=binding.root
            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this)[UploadRecordDataViewModel::class.java]
            } ?: throw Exception("Invalid Activity")
            val ss1 = SpannableString(getString(R.string.cost_break_down))
            ss1.setSpan(
                UnderlineSpan(), 0,
                ss1.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            checkedSpanable()
            binding.tvCostBreakdown.text = ss1

            binding.tvCostBreakdown.setOnClickListener {
                finalSubmitHelper.getCostBreakDown(uploadRecordDataViewModel.uploadRecordDataModel.recordId)
            }
            binding.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {

                    binding.tvSubmit.isClickable = true
                    binding.tvSubmit.setBackgroundResource(
                        R.drawable.blue_back_7
                    )
                    isSelected = true

                } else {

                    binding.tvSubmit.isClickable = false
                    isSelected = false
                    binding.tvSubmit.setBackgroundResource(
                        R.drawable.bg_review_new
                    )
                }
            }

            uploadRecordDataViewModel.songCoverImage.observe(viewLifecycleOwner, {
                CommonUtils.loadAsBitmap(
                    requireContext(),
                    binding.productImg,
                    null,
                    null,
                    it,
                    R.drawable.recent_blank
                )
            })


            uploadRecordDataViewModel.songTitleForSecondStep.observe(viewLifecycleOwner, {

                binding.tvTrackName.text = it?.plus("")
            })


            uploadRecordDataViewModel.selectedgenere.observe(viewLifecycleOwner, {

                binding.tvProductCategory.text = it
            })


            uploadRecordDataViewModel.songLocaleUri.observe(viewLifecycleOwner, {
                songLocaleUri = it
            })
            uploadRecordDataViewModel.durationOfSongInSeconds.observe(viewLifecycleOwner, {

                val seconds: Int = it.toInt() % 60
                val minutes = (it.toInt() % 3600) / 60
                val str = String.format("%d:%02d", minutes, seconds)
                binding.tvTimeDuration.text = str
            })

            binding.imgPauseIcon.setOnClickListener {
                binding.imgPauseIcon.visibility = View.INVISIBLE
                binding.imgPlayIcon.visibility = View.VISIBLE
                if (!this::mediaPlayer.isInitialized) return@setOnClickListener
                if (mediaPlayer.isPlaying) {
                    mediaPlayer.pause()
                }
            }
            binding.imgPauseIcon.setOnClickListener {

                if (uploadRecordDataViewModel.isRecordFromSaveAsDraft?.value!!) {
                    if (songFilePathUrl.isBlank()) {
                        val songId =
                            uploadRecordDataViewModel.saveAsDraftResponse?.value?.response?.files?.get(
                                0
                            )?.id
                        finalSubmitHelper.getSongUrlFinalStep(songId!!, this)
                    } else {
                        playSong(songFilePathUrl)

                    }

                } else {
                    playSong(songLocaleUri)
                }


            }
            uploadRecordDataViewModel.selectedCurrency.observe(viewLifecycleOwner, {

                selectedCurrency = it.toString()

            })


            uploadRecordDataViewModel.raisingMoney.observe(viewLifecycleOwner, {

                val formatter = DecimalFormat("#,###,###")
                val yourFormattedString: String = formatter.format(it?.toDouble())
                if (selectedCurrency == "USD") {
                    binding.tvRaisingAmount.text = "$$yourFormattedString"

                } else {
                    binding.tvRaisingAmount.text = "CHF$yourFormattedString"

                }

            })
            uploadRecordDataViewModel.totalTokenForSell.observe(viewLifecycleOwner, {

                binding.totalToken.text = it.toString()

            })

            uploadRecordDataViewModel.pricePertoken.observe(viewLifecycleOwner, {

                if (selectedCurrency == "USD") {

                    binding.tvPricePerToken.text = "$" + it?.toString()


                } else {

                    binding.tvPricePerToken.text = "CHF" + it?.toString()

                }

            })

            uploadRecordDataViewModel.ownerShipPercentageFinal.observe(
                viewLifecycleOwner,
                {

                    binding.tvOwnershipPercentage.text = it?.toString() + "%"

                })


            finalSubmitHelper = FinalSubmitHelper(this@FinalSubmitFragment, listenere)
            binding.tvSubmit.setOnClickListener {

                if (isSelected)
                    finalSubmitHelper.finalSubmit()
            }

        }
        return rootView
    }


    override fun onPause() {
        super.onPause()
        if (!this::mediaPlayer.isInitialized) return
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
            binding.imgPlayIcon.visibility = View.VISIBLE
            binding.imgPauseIcon.visibility = View.INVISIBLE
        }
    }

    private fun playSong(songLocaleUri: String) {
        binding.imgPauseIcon.visibility = View.VISIBLE
        binding.imgPlayIcon.visibility = View.INVISIBLE
        if (songLocaleUri.isEmpty()) return
        mediaPlayer = MediaPlayer()
        mediaPlayer.setDataSource(songLocaleUri)
        mediaPlayer.prepare()
        mediaPlayer.start()
    }

    override fun onSongClick(path: String) {

        playSong(path)
    }
    private fun checkedSpanable() {
        val span1: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                startActivity(
                    Intent(
                        requireActivity(),
                        TermsConditionActivity::class.java
                    ).putExtra("VALUE", 2)
                )
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                ds.color = ContextCompat.getColor(requireActivity(), R.color.gray)
            }

        }

        val span2: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                startActivity(
                    Intent(
                        requireActivity(),
                        TermsConditionActivity::class.java
                    ).putExtra("VALUE", 1)
                )

            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                ds.color = ContextCompat.getColor(requireActivity(), R.color.gray)
            }
        }

        val str =
            SpannableString(getString(R.string.submit_final_message))
        str.setSpan(span1, 101, str.length, 0)
        str.setSpan(span2, 93, 96, 0)
        binding.tvDescription.text = str
        binding.tvDescription.movementMethod = LinkMovementMethod.getInstance()

    }

}