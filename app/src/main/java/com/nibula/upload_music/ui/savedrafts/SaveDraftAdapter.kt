package com.nibula.upload_music.ui.savedrafts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemSaveDraftBinding
import com.nibula.response.saveddraft.SavedDraftRecordData
import com.nibula.utils.CommonUtils


class SaveDraftAdapter(val context: Context, val listener: Communicator) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {

        const val ACTION_CONTINUE = 1

        const val ACTION_DELETE = 2

    }

    lateinit var dataList: MutableList<SavedDraftRecordData>
    lateinit var binding: ItemSaveDraftBinding

    class SaveDraftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setData(data: MutableList<SavedDraftRecordData>) {
        dataList = data
    }

    fun addData(data: MutableList<SavedDraftRecordData>) {
        dataList.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemSaveDraftBinding.inflate(inflater, parent, false)
        val holder = SaveDraftViewHolder(binding.root)
        return holder
        /*return SaveDraftViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_save_draft,
                parent,
                false
            )
        )*/
    }

    override fun getItemCount(): Int {
        return if (::dataList.isInitialized) dataList.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]

        binding.tvContinue.setOnClickListener {
            listener.proceedAction(position, ACTION_CONTINUE, data)
        }

        binding.tvDelete.setOnClickListener {
            listener.proceedAction(position, ACTION_DELETE, data)
        }

        binding.tvContinue.text = CommonUtils.underlineSpannable(
            context.getString(R.string.text_continue),
            ContextCompat.getColor(context, R.color.green_shade_1_100per),
            true
        )

        binding.tvTitle.text = data.recordTitle ?: ""

        //--> image
        if (!data.recordImage.isNullOrEmpty()) {
            CommonUtils.loadImage(
                context,
                data.recordImage ?: "",
                binding.imgRecord,
                R.drawable.nebula_placeholder
            )
        }

        /*CommonUtils.roundRectangleBoarder(
            context,
            holder.itemView.img_record,
            data.recordImage ?: "",
            R.drawable.appicon,
            27,
            0,
            "#FFFFFF",
            1
        )*/

    }

    interface Communicator {
        fun proceedAction(position: Int, action: Int, data: SavedDraftRecordData)
    }

}
