package com.nibula.upload_music.ui.uploadrecord.helper

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.globalsearchResponse.GlobalSearchResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.upload_music.ui.uploadrecord.fragment.AddArtistsFragmentListing


class ArtistsHelper(val fg: AddArtistsFragmentListing) : BaseHelperFragment() {

    fun getSearchData(
        requestType: Int,
        searchText: String,
        currentPage: Int,
        clearOldData: Boolean
    ) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss()
            return
        }

//        if (!fg.rootView.spSearch.isRefreshing
//        ) {
//            fg.showProcessDialog()
//        }

        fg.isLoading = true
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.globalSearch(requestType, searchText, currentPage)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<GlobalSearchResponse>() {

            override fun onSuccess(any: GlobalSearchResponse?, message: String?) {
                fg.hideKeyboard()
                if (clearOldData) {
                    fg.adapter.apply {
                        if (any?.result?.isNullOrEmpty() == true) {
                            clearData()
                        } else {
                            any?.result?.let {
                                clearAndAddData(it)
                                //fg.updateRecentUpdate(searchText)
                                fg.updateRecentUpdateNew(it.get(0))
                            }
                        }
                    }
                    fg.isDataAvailable = (fg.adapter.searchData.size % 10 == 0)
                    fg.currentPage++
                } else {
                    any?.result?.let {
                        if (it.isNotEmpty()) {
                            fg.adapter.addData(it)
                            fg.isDataAvailable = (fg.adapter.searchData.size % 10 == 0)
                            fg.currentPage++
                        } else {
                            fg.isDataAvailable = false
                        }
                        fg.updateRecentUpdateNew(it.get(0))

                        // fg.updateRecentUpdate(searchText)
                    }!!
                }
                fg.isLoading = false
                //  fg.updateTabs(any)
                dismiss()
            }

            override fun onError(error: RetroError?) {
                fg.isLoading = false
                dismiss()
            }

        }))

    }


    private fun dismiss() {
        if (fg.binding.spSearch.isRefreshing) {
            fg.binding.spSearch.isRefreshing = false
        }
        fg.hideProcessDialog()

        noData()
    }

    private fun noData() {
        fg.binding.gdRecentSearches.visibility = View.GONE
        if (fg.adapter.searchData.isEmpty()) {
            fg.binding.gdSearches.visibility = View.GONE
            fg.binding.tvFixedRecentSearch.visibility=View.VISIBLE

            fg.binding.layoutNoDataSearch.tvMessage.text = "No results found"
            fg.binding.layoutNoDataSearch.root.visibility = View.VISIBLE
            fg.binding.ctbSearchFilter.visibility = View.VISIBLE
        } else {
            fg.binding.gdSearches.visibility = View.VISIBLE
            fg.binding.tvFixedRecentSearch.visibility=View.GONE
            fg.binding.ctbSearchFilter.visibility = View.VISIBLE
            fg.binding.layoutNoDataSearch.tvMessage.text = ""
            fg.binding.layoutNoDataSearch.root.visibility = View.GONE
        }
    }
}