package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import androidx.lifecycle.Observer
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.*
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.currency_res.CurrencyRes
import com.nibula.upload_music.ui.uploadrecord.fragment.MoneyRaisedFragment

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call


class MoneyRaisedHelper(
    val fg: MoneyRaisedFragment,
    var viewPagerListener: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {

    lateinit var call: Call<UploadRecordFileResponse>

    fun getCurrency(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val authHelper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = authHelper.getCurrencyType()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<CurrencyRes>() {

            override fun onSuccess(any: CurrencyRes?, message: String?) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.currecyList.isNullOrEmpty()) {
                        fg.currancyList.addAll(any.currecyList)
                        val temp = mutableListOf<String>()
                        temp.add(fg.getString(R.string.currency).toUpperCase())
                        for (obj in any.currecyList) {
                            temp.add(obj.text ?: "")
                        }
                        fg.currencyAdapter.setData(temp)

                        for (currency in any.currecyList) {
                            fg.binding.spnCurrency.setSelection(
                                fg.currancyList.indexOf(
                                    currency
                                ) + 0
                            )
                        }
                    }
                }

                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }

        }))
    }


    private fun raisedMoneyRequest() {
        var recordId: String = ""
        var recordFiledId: String = ""
        fg.showProcessDialog()

        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }

        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.raisedMoneyRequest(
            6.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.binding.edtAmountRaised.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.currencyTypeId.trim().toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {
                        fg.uploadRecordDataViewModel.setRaisingMoney(fg.binding.edtAmountRaised.text.toString())

                        viewPagerListener.onSuccess()

                    } else {
                        viewPagerListener.failed(any?.responseMessage!!)
                    }
                } else {
                    fg.hideProcessDialog()

                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

    fun checkValidation(): Boolean {
        if (fg.binding.edtAmountRaised.text.toString().isEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Please enter amount",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else if (fg.binding.edtAmountRaised.text.toString().equals(".")) {
            Toast.makeText(
                fg.requireContext(),
                "Please enter a valid amount",
                Toast.LENGTH_SHORT
            ).show()
            return false
        } else if (fg.binding.edtAmountRaised.text.toString().endsWith(".")) {
            Toast.makeText(
                fg.requireContext(),
                "Please enter a valid amount",
                Toast.LENGTH_SHORT
            ).show()
            return false
        } else if (fg.binding.edtAmountRaised.text.toString().toFloat() < 10f) {
            Toast.makeText(
                fg.requireContext(),
                "You can raise min 10$",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (fg.binding.edtAmountRaised.text.toString().toFloat() > 10000000f) {
            Toast.makeText(
                fg.requireContext(),
                "You can raise max 10000000$",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (fg.currencyTypeId.isEmpty()) {
            Toast.makeText(
                fg.requireContext(),
                "Please select currency type",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else {
            raisedMoneyRequest()
        }
        return true
    }


    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }
}