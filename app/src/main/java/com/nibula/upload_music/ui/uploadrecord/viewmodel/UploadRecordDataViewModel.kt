package com.nibula.upload_music.ui.uploadrecord.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nibula.response.saveddraft.SaveAsDraftNewDataResponse
import com.nibula.upload_music.ui.uploadrecord.model.UploadRecordDataModel

open class UploadRecordDataViewModel : ViewModel() {
    var uploadRecordDataModel = UploadRecordDataModel()
    var raisingMoney = MutableLiveData<String>()
    var ownerShipPercentage = MutableLiveData<String>()
    var percentageToSell = MutableLiveData<String>()
    var ownerShipPercentagePerToken = MutableLiveData<String>()
    var songCoverImage = MutableLiveData<String>()
    var songLocaleUri = MutableLiveData<String>()
    var selectedgenere = MutableLiveData<String>()
    var currentStatus = MutableLiveData<String>()
    var selectedCurrency = MutableLiveData<String>()
    var totalTokenForSell = MutableLiveData<String>()
    var pricePertoken = MutableLiveData<String>()
    var ownerShipPercentageFinal = MutableLiveData<String>()
    var songTitleForSecondStep = MutableLiveData<String>()
    var isRecordSongFileChanged: Boolean = false
    var isCoverImageChanged: Boolean = false
    var isRecordFromSaveAsDraft = MutableLiveData<Boolean>()

    var recordTitle = MutableLiveData<String>()
    var durationOfSongInSeconds = MutableLiveData<String>()
    var currentUploadingStatus = MutableLiveData<String>()
    var currentProgressValue = MutableLiveData<Int>()
    var saveAsDraftResponse = MutableLiveData<SaveAsDraftNewDataResponse>()

    var isRecordSuccessfullyUploaded = MutableLiveData<Boolean>(false)
    fun setUplaodSongFirstStepData(data: UploadRecordDataModel) {
        uploadRecordDataModel = data
    }

    fun setIsRecordSuccessfullyUploaded(uploadSuccess: Boolean) {
        isRecordSuccessfullyUploaded.value = uploadSuccess
    }

    fun setRaisingMoney(raiaingMoneyAmount: String) {
        raisingMoney.value = raiaingMoneyAmount
    }

    fun setOwnerShipPercentage(percentage: String) {
        ownerShipPercentage.value = percentage
    }

    fun setPercentageToSell(percentage: String) {
        percentageToSell.value = percentage
    }


    fun setPricePerToken(pricePerToken: String) {
        pricePertoken.value = pricePerToken
    }

    fun setTokenForSell(sellingToken: String) {
        totalTokenForSell.value = sellingToken
    }

    fun setSaveAsDraftResponseData(response: SaveAsDraftNewDataResponse) {
        saveAsDraftResponse.value = response
    }

    fun setFinalOwnerShipPercentage(finalOwnerShipPercentage: String) {
        ownerShipPercentageFinal.value = finalOwnerShipPercentage
    }

    fun setOwnershipPercentagePerToken(ownerShipPercentage: String) {
        ownerShipPercentagePerToken.value = ownerShipPercentage
    }


    fun setRecordFileChanges(isSongFileChanged: Boolean) {
        isRecordSongFileChanged = isSongFileChanged
    }


    fun setRecordCoverImage(isCoverImage: Boolean) {
        isCoverImageChanged = isCoverImage
    }

    fun setIsRecordFromSaveAsDraft(isSaveAsDraft: Boolean?) {
        isRecordFromSaveAsDraft.value = isSaveAsDraft!!
    }

    fun setSongCoverImage(
        songCoverImageUrl: String,
        recordTitleString: String,
        songLocaleUriString: String
    ) {
        songCoverImage.value = songCoverImageUrl
        recordTitle.value = recordTitleString
        songLocaleUri.value = songLocaleUriString
    }

    // Song title for second step
    fun songTitle(songTitle: String) {
        songTitleForSecondStep.value = songTitle
    }


    fun selectedGenere(genere: String) {
        selectedgenere.value = genere
    }


    fun selectedCurrency(currency: String) {
        selectedCurrency.value = currency
    }

    fun setSongDurationFile(songDuration: String) {
        durationOfSongInSeconds.value = songDuration
    }

    fun setCurrentStatus(currentStatusValue: String) {
        currentStatus.value = currentStatusValue
    }
}