package com.nibula.upload_music.ui.uploadrecord.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.databinding.UploadSingleMusicActivityBinding
import com.nibula.upload_music.ui.uploadrecord.fragment.UploadRecordFragmentNew
import com.theartofdev.edmodo.cropper.CropImage

class UploadRecordActivityNew : BaseActivity() {

    lateinit var fg: UploadRecordFragmentNew
    lateinit var binding:UploadSingleMusicActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.upload_single_music_activity)
        binding= UploadSingleMusicActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (intent?.hasExtra("draft_data")!!) {
            fg = UploadRecordFragmentNew.newInstance(intent?.extras!!)

        } else {
            fg = UploadRecordFragmentNew.newInstance()
        }
        fg.arguments = intent?.extras
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fg)
                .commitNow()

        }

    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                fg.cropData(result.uri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.error, Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (fg.binding.vpgMain.currentItem == 0) {
            finish()
        } else {
            fg.onBack()
        }
    }

  /*  fun hideAction() {
        if (::fg.isInitialized) {
            fg.binding.layoutTb.visibility = View.GONE
        }
    }*/
}