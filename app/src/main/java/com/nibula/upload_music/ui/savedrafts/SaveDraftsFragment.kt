package com.nibula.upload_music.ui.savedrafts

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.databinding.LayoutSavedDraftsBinding
import com.nibula.response.BaseResponse
import com.nibula.response.saveddraft.SavedDraftRecordData
import com.nibula.response.saveddraft.SavedDraftRecordResponse
import com.nibula.upload_music.ui.savedrafts.SaveDraftAdapter.Companion.ACTION_CONTINUE
import com.nibula.upload_music.ui.savedrafts.SaveDraftAdapter.Companion.ACTION_DELETE
import com.nibula.utils.AppConstant
import com.nibula.utils.AppConstant.Companion.REQUEST_SAVE_DRAFTS_REFRESH
import com.nibula.utils.RequestCode.REQUEST_EDIT_SONG_SCREEN
import com.nibula.utils.interfaces.DialogFragmentClicks
import retrofit2.Call


class SaveDraftsFragment : BaseFragment(), SaveDraftAdapter.Communicator,
						   DialogFragmentClicks {
	
	private lateinit var rootView: View
	private lateinit var helper: SaveDraftHelper
	
	lateinit var call: Call<SavedDraftRecordResponse>
	lateinit var callDelete: Call<BaseResponse>
	
	lateinit var adapter: SaveDraftAdapter
	
	lateinit var data: SavedDraftRecordData
	var position: Int = -1
	lateinit var binding:LayoutSavedDraftsBinding
	
	companion object {
		fun newInstance() =
			SaveDraftsFragment()
	}
	
	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		if (!::rootView.isInitialized) {
			//rootView = inflater.inflate(R.layout.layout_saved_drafts, container, false)
			binding=LayoutSavedDraftsBinding.inflate(inflater,container,false)
			rootView=binding.root
			intiUI()
		}
		return rootView
	}
	
	override fun onResume() {
		super.onResume()
		if (!::call.isInitialized ||
			(call.isCanceled && !call.isExecuted)
		) {
			helper.requestSavedDraftsList(true)
		}
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		hideKeyboard()
	}
	
	override fun onStop() {
		super.onStop()
		if (!::call.isInitialized) {
			call.cancel()
		}
	}
	
	private fun intiUI() {
		helper = SaveDraftHelper(this)

		binding.layoutTbMain.imgHome.setOnClickListener {
			requireActivity().finish()
		}
		
		setSavedDraftsAdapter()
		binding.swpMain.setOnRefreshListener {
			helper.nextPage = 1
			helper.maxPage = 0
			helper.requestSavedDraftsList(false)
		}
	}
	
	private fun setSavedDraftsAdapter() {
		adapter =
			SaveDraftAdapter(
				requireActivity(),
				this
			)
		adapter.setData(mutableListOf())
		binding.rvSavedDrafts.adapter = adapter
		val layoutManager =
			LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		binding.rvSavedDrafts.layoutManager = layoutManager

		binding.rvSavedDrafts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
			override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
				super.onScrolled(recyclerView, dx, dy)
				val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
				val totalItem = layoutManager.itemCount
				val threshold = 5
				if (!helper.isLoading &&
					helper.isDataAvailable &&
					totalItem < (lastVisibleItem + threshold)
				) {
					helper.requestSavedDraftsList(true)
				}
			}
		})
	}
	
	
	override fun proceedAction(position: Int, action: Int, data: SavedDraftRecordData) {
		
		this@SaveDraftsFragment.data = data
		this@SaveDraftsFragment.position = position
		
		when (action) {
			
			ACTION_CONTINUE -> {
				if (!data.id.isNullOrEmpty()) {
					if (data.screenType == 0) {
						/*this@SaveDraftsFragment.startActivityForResult(
							Intent(
								requireActivity(),
								UploadRecordActivity::class.java
							)
								.putExtra(AppConstant.RECORD_DATA, data)
							, REQUEST_SAVE_DRAFTS_REFRESH
						)*/
					} else {
					/*	val sendIntent = Intent(requireActivity(), EditSongActivity::class.java)
						sendIntent.putExtra(AppConstant.RECORD_DATA, data)
						this@SaveDraftsFragment.startActivityForResult(
							sendIntent,
							RequestCode.REQUEST_EDIT_SONG_SCREEN
						)*/
					}
				}
			}
			
			ACTION_DELETE -> {
				val bPrompt = PNBSheetFragment(
					getString(R.string.message_delete_2),
					getString(R.string.delete),
					getString(R.string.cancel),
					this@SaveDraftsFragment
				)
				bPrompt.isCancelable = false
				bPrompt.setColor(
					ContextCompat.getColor(requireContext(), R.color.colorAccent),
					ContextCompat.getColor(requireContext(), R.color.white),
					ContextCompat.getColor(requireContext(), R.color.gray_1_51per),
					ContextCompat.getColor(requireContext(), R.color.gray_1_51per)
				)
				bPrompt.show(
					this@SaveDraftsFragment.childFragmentManager,
					"PositiveNegativeBottomSheetFragment"
				)
			}
			
		}
	}
	
	@Deprecated("Deprecated in Java")
	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		when (requestCode) {
			
			REQUEST_EDIT_SONG_SCREEN -> {
				if (resultCode == Activity.RESULT_OK) {
					val sendToDetail = data?.getBooleanExtra("openDetail", false) ?: false
					if (sendToDetail) {
						/*this@SaveDraftsFragment.startActivityForResult(
							Intent(
								requireActivity(),
								UploadRecordActivity::class.java
							)
								.putExtra(AppConstant.RECORD_DATA, this@SaveDraftsFragment.data)
							, REQUEST_SAVE_DRAFTS_REFRESH
						)*/
					}
					val isRefresh = data?.getBooleanExtra("refresh", false) ?: false
					if (isRefresh) {
						helper.nextPage = 1
						helper.maxPage = 0
						helper.requestSavedDraftsList(true)
					}
				}
			}
			
			REQUEST_SAVE_DRAFTS_REFRESH -> {
				if (resultCode == Activity.RESULT_OK) {
					helper.nextPage = 1
					helper.maxPage = 0
					helper.requestSavedDraftsList(true)
				}
			}
		}
		
	}
	
	override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
		if (bundle == null) {
			dialog.dismiss()
			return
		}
		when (obj) {
			is PNBSheetFragment -> {
				if (::data.isInitialized &&
					!data.id.isNullOrEmpty()
				) {
					helper.requestDelete(position, data.id!!, true)
				}
			}
		}
		dialog.dismiss()
	}
	
	override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
		dialog.dismiss()
	}
	
	
}
