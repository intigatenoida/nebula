package com.nibula.upload_music.ui.uploadrecord.adapter
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemSelectedArtistsBinding
import com.nibula.upload_music.ui.uploadrecord.model.ArtistModel
import com.nibula.utils.CommonUtils

class SelectedArtistsAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class RecentSearchViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    private val recentDataList = ArrayList<ArtistModel>()

    private lateinit var layoutInflater: LayoutInflater
    private lateinit var context: Context
    private lateinit var listener: onDelete
    lateinit var binding:ItemSelectedArtistsBinding


    constructor(context: Context, listener: onDelete) : this() {
        this.context = context
        this.listener = listener
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
      /*  val view =
            layoutInflater.inflate(R.layout.item_selected_artists, parent, false)
        return RecentSearchViewHolder(view)*/
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemSelectedArtistsBinding.inflate(inflater, parent, false)
        val holder = RecentSearchViewHolder(binding.root)
        return holder
    }

    override fun getItemCount(): Int {
        return recentDataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RecentSearchViewHolder) {
           binding.tvArtist.text = recentDataList[position]?.artistName
           binding.tvTagName.text = recentDataList[position]?.artistUserName
            recentDataList[position]?.artistUserImage?.let {

                CommonUtils.loadImage(
                    context,
                    recentDataList[position]?.artistUserImage,
                   binding.imgArtist,
                    R.drawable.ic_record_user_place_holder
                )

            }

           binding.imgNoResult.setOnClickListener {

                listener.onDelete(recentDataList.get(position))
            }
        }
    }

    fun addData(dataList: ArrayList<ArtistModel>) {
        recentDataList.clear()
        recentDataList.addAll(dataList)
        notifyDataSetChanged()
    }

    fun notifyData()
    {
        notifyDataSetChanged()
    }

    fun addData(data: ArtistModel) {
        recentDataList.add(data)
        notifyItemInserted(recentDataList.size - 1)
    }

    public interface onDelete {
        fun onDelete(data: ArtistModel)
    }

}

