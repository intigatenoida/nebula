package com.nibula.upload_music.ui.uploadrecord.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.SongTitleFragmentBinding
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.helper.SongTitleHelper
import com.nibula.upload_music.ui.uploadrecord.viewmodel.UploadRecordDataViewModel


class SongTitleFragment : BaseFragment() {

    companion object {
        fun newInstance(
            bundle: Bundle,
            listenere: UploadSongViewPageAdapter.Communicator
        ): SongTitleFragment {
            val fg = SongTitleFragment()
            fg.arguments = bundle
            fg.listenere = listenere
            return fg
        }
    }

    lateinit var rootView: View
    private lateinit var listenere: UploadSongViewPageAdapter.Communicator
    lateinit var songHelper: SongTitleHelper
    lateinit var uploadRecordDataViewModel: UploadRecordDataViewModel
    lateinit var binding:SongTitleFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.song_title_fragment, container, false)
            binding= SongTitleFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            binding.dotsIndicator.setDotSelection(3)
            uploadRecordDataViewModel = activity?.run {
                ViewModelProvider(this).get(UploadRecordDataViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            songHelper = SongTitleHelper(this@SongTitleFragment, listenere)

            if (uploadRecordDataViewModel?.isRecordFromSaveAsDraft?.value!!) {
                setSecondStepData()
            }
            binding.ivPrevious.setOnClickListener {
                listenere.prevStep()
            }

            binding.ivNext.setOnClickListener {

                listenere.nextStep()
            }
        }
        return rootView
    }


    fun validation(): Boolean {
        return songHelper.checkValidation()
    }

    fun setSecondStepData() {
        val completedStepNumber =
            uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.stepNumber
        if (completedStepNumber!! >= 2) {
            val songTitle =
                uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.recordTitle
            binding.etSongTitle.setText(songTitle)
        }

    }
}