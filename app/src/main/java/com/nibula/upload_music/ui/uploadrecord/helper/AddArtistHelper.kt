package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import androidx.lifecycle.Observer
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.*
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.AddArtistFragment
import com.nibula.user.profile.ProfileResponce
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call


class AddArtistHelper(
    val fg: AddArtistFragment, var viewPagerListener: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {

    fun getProfile(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg!!.showToast(
                fg.requireContext()!!,
                fg!!.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideProcessDialog()
            return
        }

        if (isProgressDialog) {
            fg!!.showProcessDialog()
        }

        val helper =
            ApiClient.getClientAuth(fg.requireContext()!!).create(ApiAuthHelper::class.java)
        val observable = helper.getProfile("")
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<ProfileResponce>() {
            override fun onSuccess(any: ProfileResponce?, message: String?) {
                if (any != null &&
                    any.responseStatus == 1 &&
                    any.userProfile != null
                ) {

                    fg.addFirstRow(any)

                }
                fg.hideProcessDialog()
            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
            }

        }))

    }

    lateinit var call: Call<UploadRecordFileResponse>


    private fun addArtist() {

        fg.showProcessDialog()
        var recordId: String = ""
        var recordFiledId: String = ""
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.addArtists(
            3.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.selectedArtistsIdsString.trim().toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {

                        viewPagerListener.onSuccess()
                    } else {
                        viewPagerListener.failed(any?.responseMessage!!)
                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

    fun checkValidation(): Boolean {
        if (fg.selectedArtistsList.size == 0) {
            Toast.makeText(
                fg.requireContext(),
                "Please select at least one artist",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else {
            for (item in fg.selectedArtistsList) {
                if (!item.artistId.isNullOrEmpty())
                    fg.artistIdsArrayListNew.add(item.artistId!!)
            }
            fg.selectedArtistsIdsString = fg.artistIdsArrayListNew.joinToString(",").toString()
            addArtist()
        }
        return true
    }

}

