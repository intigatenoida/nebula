package com.nibula.upload_music.ui.uploadrecord.model

import androidx.annotation.Keep

@Keep
data class UploadRecordDataModel(
    var songCoverImage: String? = "",
    var songFile: String? = "",
    var recordId: String? = "",
    var recordFiledId: String? = "",
    var isCoverImageChange: Boolean = false,
    var isSongFileChanged: Boolean = false,
    var raisingMoney:String="",
    var royalitypercentageOwn:String=""

)