package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import androidx.lifecycle.Observer
import com.nibula.base.BaseHelperFragment
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.*
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.TokenSellFragment
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call


class TokenSellHelper(
    val fg: TokenSellFragment,
    var listenere: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {


    lateinit var call: Call<UploadRecordFileResponse>


    private fun tokenSellRequest() {

        fg.showProcessDialog()
        fg.showProcessDialog()
        var recordId: String = ""
        var recordFiledId: String = ""
        var ownerShipPercentage: String = ""
        var raisingAmount: String = ""

        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        fg.uploadRecordDataViewModel.ownerShipPercentage.observe(fg.viewLifecycleOwner, Observer {
            ownerShipPercentage = it.toString()
        })

        fg.uploadRecordDataViewModel.raisingMoney.observe(fg.viewLifecycleOwner, Observer {
            raisingAmount = it.toString()
        })

        // val durations = recordData.recordDurationInSeconds ?: 0
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.tokenSellRequest(
            8.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.binding.edtAmount.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.binding.etPricePerToken.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.binding.etRoyaltyShare.text.toString().trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),

            ownerShipPercentage.trim()
                .toRequestBody("text/plain".toMediaTypeOrNull()),
            raisingAmount.trim()
                .toRequestBody("text/plain".toMediaTypeOrNull())

        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {
                        fg.uploadRecordDataViewModel.setPricePerToken(fg.binding.etPricePerToken.text.toString())
                        fg.uploadRecordDataViewModel.setFinalOwnerShipPercentage(fg.binding.etRoyaltyShare.text.toString())
                        fg.uploadRecordDataViewModel.setTokenForSell(fg.binding.edtAmount.text.toString())
                        listenere.onSuccess()
                    } else {
                        listenere.failed(any?.responseMessage!!)

                    }
                } else {
                    fg.hideProcessDialog()

                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

    fun checkValidation(): Boolean {
        when {
            fg.binding.edtAmount.text.toString().isEmpty() -> {
                Toast.makeText(
                    fg.requireContext(),
                    "Please enter amount",
                    Toast.LENGTH_SHORT
                )
                    .show()
                return false
            }

            fg.binding.edtAmount.text.toString() == "." -> {
                Toast.makeText(
                    fg.requireContext(),
                    "Please enter valid token quantity",
                    Toast.LENGTH_SHORT
                )
                    .show()
                return false
            }


            fg.binding.edtAmount.text.toString().endsWith(".") -> {
                Toast.makeText(
                    fg.requireContext(),
                    "Please enter valid token quantity",
                    Toast.LENGTH_SHORT
                )
                    .show()
                return false
            }
            fg.binding.edtAmount.text.toString().toInt() < 1 -> {
                Toast.makeText(
                    fg.requireContext(),
                    "At least 1 token is required",
                    Toast.LENGTH_SHORT
                )
                    .show()
                return false
            }


            fg.binding.edtAmount.text.toString().toInt() > 2000 -> {
                Toast.makeText(
                    fg.requireContext(),
                    "You can sell max 2000 token",
                    Toast.LENGTH_SHORT
                )
                    .show()
                return false
            }



            else -> {
                tokenSellRequest()
            }
        }
        return true
    }


    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }
}