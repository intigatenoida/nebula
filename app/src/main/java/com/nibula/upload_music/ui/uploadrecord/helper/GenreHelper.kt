package com.nibula.upload_music.ui.uploadrecord.helper

import android.widget.Toast
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.uploadrecord.GenereData
import com.nibula.response.uploadrecord.GenereResponse
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.*
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.GenreListFragment

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call

class GenreHelper(
    val fg: GenreListFragment,
    var viewPagerListener: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {

    lateinit var generDataList: MutableList<GenereData>
    lateinit var call: Call<UploadRecordFileResponse>

    fun requestGenere(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val authHelper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = authHelper.requestGenere()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<GenereResponse>() {

            override fun onSuccess(any: GenereResponse?, message: String?) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.dataList.isNullOrEmpty()) {
                        generDataList = any.dataList
                        val temp = mutableListOf<String>()
                        temp.add(fg.getString(R.string.genre).toUpperCase())
                        for (obj in any.dataList) {
                            temp.add(obj.text ?: "")
                        }
                        fg.genereAdapter.setData(temp)
                    }
                }


                val genreIds: IntArray? = fetchGenreArray()
                if (genreIds != null && genreIds.isNotEmpty()) {
                    fg.binding.spnGenre.setSelection(
                        getSelectedGenerPosition(
                            genreIds[0]
                        )
                    )
                }

                if (fg.uploadRecordDataViewModel?.isRecordFromSaveAsDraft?.value!!) {
                    val completedStepNumber =
                        fg.uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.stepNumber
                    if (completedStepNumber!! >= 3) {

                        val genereTypeId =
                            fg.uploadRecordDataViewModel.saveAsDraftResponse.value?.response?.genreTypeId

                        fg.genereTypeId = genereTypeId.toString()
                        if (genereTypeId != null && genereTypeId.isNotEmpty()) {
                            fg.binding.spnGenre.setSelection(
                                getSelectedGenerPosition(
                                    genereTypeId.toInt()
                                )
                            )
                        }
                    }

                }

                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }
        }))
    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }


    fun fetchGenreArray(): IntArray? {
        // val temp: String = fg.data?.genreTypeIds ?: ""
        val temp: String = ""
        if (temp.isEmpty()) {
            return null
        }
        val fArray: IntArray?
        try {
            val gArray: Array<String> = temp.split(",").toTypedArray()
            fArray = IntArray(gArray.size)
            for (i in gArray.indices) {
                if (gArray[i].isNotEmpty()) {
                    fArray[i] = gArray[i].trim().toInt()
                }
            }
            return fArray
        } catch (e: Exception) {

        }
        return null
    }

    fun getSelectedGenerPosition(genereId: Int): Int {
        var index = 0
        for (i in 0 until generDataList.size) {
            if (genereId == (generDataList[i].value ?: 0)) {
                index = i + 1
                break
            }
        }
        return index
    }


    private fun addRequestForGenre() {

        fg.showProcessDialog()
        var recordId: String = ""
        var recordFiledId: String = ""
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        // val durations = recordData.recordDurationInSeconds ?: 0
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.addGenreRequest(
            4.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            recordFiledId.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            fg.genereTypeId
                .toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {

                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {

                        viewPagerListener.onSuccess()
                    } else {
                    }
                } else {
                    fg.hideProcessDialog()


                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }

    fun checkValidation(): Boolean {
        if (fg.genereTypeId?.isEmpty()!! || fg.genereTypeId?.equals("0")!!) {
            Toast.makeText(
                fg.requireContext(),
                "Please select genere type",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        } else {
            addRequestForGenre()
        }
        return true
    }

}