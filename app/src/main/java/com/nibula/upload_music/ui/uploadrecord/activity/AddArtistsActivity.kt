@file:Suppress("OverrideDeprecatedMigration", "OverrideDeprecatedMigration")

package com.nibula.upload_music.ui.uploadrecord.activity

import android.os.Bundle
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.upload_music.ui.uploadrecord.fragment.AddArtistsFragmentListing

@Suppress("OverrideDeprecatedMigration", "OverrideDeprecatedMigration")
class AddArtistsActivity : BaseActivity() {

    lateinit var fg: AddArtistsFragmentListing
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.upload_single_music_activity)
        fg = AddArtistsFragmentListing.newInstance()
        fg.arguments = intent?.extras
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fg)
                .commitNow()

        }
    }


    override fun onBackPressed() {
        if (::fg.isInitialized) {
            finish()
        }
    }
}