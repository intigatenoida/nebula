package com.nibula.certificate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.nibula.databinding.FragmentCertificateBinding


class CertificateFragment : Fragment() {
    private lateinit var rootView: View
private lateinit var binding:FragmentCertificateBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //rootView = inflater.inflate(R.layout.fragment_certificate, container, false)
        binding= FragmentCertificateBinding.inflate(inflater,container,false)
        rootView=binding.root
        binding.rvCertificate.layoutManager = LinearLayoutManager(requireContext())
        binding.rvCertificate.adapter = CertificateFileAdapter(requireContext())
        return rootView
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CertificateFragment()
    }
}