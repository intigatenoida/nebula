package com.nibula.counter

import android.os.CountDownTimer

class TimerCounter {

    var mainTimer: CountDownTimer? = null
    lateinit var callbacks: TimerCommunicator

    companion object {
        fun newInstance(callbacks: TimerCommunicator): TimerCounter {
            val t = TimerCounter()
            t.callbacks = callbacks
            return t
        }
        const val MAX_DAY_COUNTER_LIMIT: Long = 1000 * 60 * 60 * 36
        const val MAX_COUNT_MILLIS: Long = 1000 * 60 * 60 * 12
        const val INTERVAL_COUNT_MILLIS: Long = 1000
    }

    fun reInitializeCounter() {
        clear()
        initCounter()
    }

    fun initCounter() {
        if (mainTimer == null) {
            mainTimer =
                object : CountDownTimer(MAX_COUNT_MILLIS, INTERVAL_COUNT_MILLIS) {
                    override fun onTick(millisUntilFinished: Long) {
                        callbacks.onTick(millisUntilFinished)
                    }

                    override fun onFinish() {
                        callbacks.onFinish()
                        reInitializeCounter()
                    }
                }.start()
        }
    }

    fun clear() {
        if (mainTimer != null) {
            mainTimer!!.cancel()
        }
        mainTimer = null
    }

    interface TimerCommunicator {
        fun onTick(millisUntilFinished: Long)

        fun onFinish()
    }

    interface  TimerItemRemove{
        fun onTimerFinish(recordId: String, position: Int)
    }
}