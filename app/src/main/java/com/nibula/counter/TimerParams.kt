package com.nibula.counter

open class TimerParams {
    var rId: String = ""
    var tltr: Long = 0L
    var isFinish: Boolean = false
    var topLabel: String? = ""
    var remainingMillis: Long = 0L
}

