package com.nibula.guide

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentGuideBinding
import com.nibula.response.walkthrought.ResponseCollection
import com.nibula.utils.PrefUtils

class GuideFragment : BaseFragment() {
    lateinit var tagList: List<ResponseCollection>
    lateinit var rootView: View
    lateinit var binding:FragmentGuideBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //rootView = inflater.inflate(R.layout.fragment_guide, container, false)
        binding=FragmentGuideBinding.inflate(inflater,container,false)
        rootView=binding.root

        val walkThoughResponse =
            PrefUtils.getValueFromPreference(requireContext(), PrefUtils.WALK_THROUGH_REPONSE)
        if (!TextUtils.isEmpty(walkThoughResponse)) {
            val type = object : TypeToken<List<ResponseCollection?>?>() {}.type
            tagList = (Gson().fromJson<List<ResponseCollection?>?>(
                walkThoughResponse,
                type
            ) as List<ResponseCollection>?)!!
        }

        when (val value = requireArguments().getInt("POSITION")) {
            0 -> {
                binding.gdFirst.firstLayout.visibility = View.VISIBLE
                binding.gdSecond.secondLayout.visibility = View.GONE
                binding.gdThird.thirdLayout.visibility = View.GONE

                /* if (::tagList.isInitialized && tagList.isNotEmpty()) {
                     val title = tagList[value].title!!.replace("\\n", "<br>")
                     val description = tagList[value].description!!.replace("\\n", "<br>")
                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                         rootView.tvHeading.text = Html.fromHtml(title, Html.FROM_HTML_MODE_LEGACY)
                         rootView.tvDescription.text = Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY)
                     } else {
                         rootView.tvHeading.text = Html.fromHtml(title)
                         rootView.tvDescription.text = Html.fromHtml(description)
                     }
                 }*/
//                rootView.tvHeading.text = getString(R.string.pre_releases_nfor_music_investors)
//                rootView.tvDescription.text =
//                    getString(R.string.discover_hit_songs_before_they_take_off_invest_by_buying_music_and_receive_royalties)
            }

            1 -> {
                binding.gdFirst.firstLayout.visibility = View.GONE
                binding.gdSecond.secondLayout.visibility = View.VISIBLE
                binding.gdThird.thirdLayout.visibility = View.GONE
            }

            2 -> {
                binding.gdFirst.firstLayout.visibility = View.GONE
                binding.gdSecond.secondLayout.visibility = View.GONE
                binding.gdThird.thirdLayout.visibility = View.VISIBLE
            }
//            1 -> {
//                rootView.tvHeading.text = getString(R.string.cash_back_for_songs)
//                rootView.tvDescription.text =
//                   getString(R.string.when_music_is_created)
//            }
//            2 -> {
//                rootView.tvHeading.text = getString(R.string.create_the)
//                rootView.tvDescription.text =
//                    getString(R.string.pick_your_rhythm)
//            }
        }
        return rootView
    }

    companion object {
        @JvmStatic
        fun newInstance() = GuideFragment()

    }


}
