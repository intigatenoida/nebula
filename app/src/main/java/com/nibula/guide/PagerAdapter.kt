package com.nibula.guide
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        val fragment = GuideFragment.newInstance()
        val bundle = Bundle()
        bundle.putInt("POSITION", position)
        fragment.arguments = bundle
        return fragment
    }
}