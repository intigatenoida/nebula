package com.nibula.guide

import android.content.Intent
import android.os.Bundle
import com.google.android.material.tabs.TabLayoutMediator
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.dashboard.DashboardActivity
import com.nibula.databinding.ActivityGuideBinding
import com.nibula.user.login.LoginActivity
import com.nibula.user.sign_up.CreateAccountActivity
import com.nibula.utils.AppConstant
import com.nibula.utils.PrefUtils
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class WalkThroughActivity : BaseActivity(){
    lateinit var binding:ActivityGuideBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
/*
        setContentView(R.layout.activity_guide)
*/
        binding= ActivityGuideBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val adapter = PagerAdapter(this)
        binding.viewPager.adapter = adapter
        binding.skipTxt.setOnClickListener {
            PrefUtils.saveValueInPreference(this, PrefUtils.IS_GUIDE_PAGE_ENABLE, "true")
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
           finish()
        }


        binding.btnSignUp.setOnClickListener {
            PrefUtils.saveValueInPreference(this, PrefUtils.IS_GUIDE_PAGE_ENABLE, "true")
            val intent = Intent(this, CreateAccountActivity::class.java)
            intent.putExtra(AppConstant.IS_START_FROM_GUIDE, true)
            startActivity(intent)
          finish()
        }

        binding.tvAlready.setOnClickListener {
            PrefUtils.saveValueInPreference(this, PrefUtils.IS_GUIDE_PAGE_ENABLE, "true")
            val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra(AppConstant.IS_START_FROM_GUIDE, true)
            intent.putExtra(AppConstant.ISFIRSTTIME, true)
            startActivity(intent)
            finish()
        }

        TabLayoutMediator(binding.tabLayout, binding.viewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->

                tab.text = ""
            }).attach()
        var page = binding.viewPager.currentItem

        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate({
            runOnUiThread {
                println("WalkThorugh--->  ${binding.viewPager.currentItem}")
                if (binding.viewPager.currentItem == 1) {
                    binding.viewPager.currentItem = 0
                } else {
                    binding.viewPager.currentItem = binding.viewPager.currentItem + 1
                }
                binding.viewPager.setCurrentItem(binding.viewPager.currentItem, true)
            }

        }, 10, 10, TimeUnit.SECONDS)
    }
}
