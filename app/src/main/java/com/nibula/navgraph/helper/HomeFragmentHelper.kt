package com.nibula.navgraph.helper



import android.util.Log
import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.navgraph.fragment.HomeFragment
import com.nibula.request.TrendingRequest
import com.nibula.response.home.recentuploaded.HomeScreenCommonModel
import com.nibula.response.home.recentuploaded.RecentUploadedResponse
import com.nibula.response.notifiaction.NotificationResponse
import com.nibula.response.topoffertopartist.*
import com.nibula.retrofit.*
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils


class HomeFragmentHelper(val fg: HomeFragment) : BaseHelperFragment() {

    var recentUploadCurrentPageCount = 1

    /*//Old APi
    fun requestTrendsOfferingTopArtists(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.swp_main.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        val helper =
            ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.topArtistObservable = helper.trendingOfferingTopArtist("application/json")
        disposables.add(sendApiRequest(fg.topArtistObservable!!)!!.subscribeWith(object :
            CallbackWrapper<TopOfferTopArtistReponse>() {

            override fun onSuccess(any: TopOfferTopArtistReponse?, message: String?) {
                if (any?.trendingOffering?.responseStatus != null &&
                    any.trendingOffering!!.responseStatus == 1
                ) {
                    if (!any.trendingOffering?.responseCollection.isNullOrEmpty()) {
                        fg.binding.gp_trending.visibility = View.VISIBLE
                        fg.trendingOfferAdapter.setData(getTrendingFilterData(any.trendingOffering!!.responseCollection))
                    } else {
                        fg.binding.gp_trending.visibility = View.GONE
                    }
                }

                if (!any?.topArtists.isNullOrEmpty()) {
                    fg.binding.gp_top_artist.visibility = View.VISIBLE
                    //any!!.topArtists.add(any.topArtists[any.topArtists.size - 1])
                    fg.artistAdapter.setData(any!!.topArtists)
                }

                if (fg.trendingOfferAdapter.dataList.isEmpty()) {
                    fg.binding.gp_trending.visibility = View.GONE
                }

                if (fg.artistAdapter.dataList.isEmpty()) {
                    fg.binding.gp_top_artist.visibility = View.GONE
                }

                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }

        }))

    }*/
    fun requestFreaturedArtistList(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var trendingRequest = TrendingRequest()
        trendingRequest.currentPage = 1
        trendingRequest.recordsPerPage = 10
        trendingRequest.search = ""

        val call = helper.freaturedArtistList(trendingRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<FreaturedArtistlistResponse>() {
            override fun onSuccess(any: FreaturedArtistlistResponse?, message: String) {
                fg.hideProcessDialog()
                val response = any as FreaturedArtistlistResponse
                if (response?.responseStatus == 1) {
                    if (fg.freaturedArtistAdapter.dataList.size != 0) {
                        fg.freaturedArtistAdapter.dataList.clear()
                    }
                    fg.freaturedArtistAdapter.setData(response.responseCollection!!)
                }

            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()


            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })
    }

    fun trendingOfferingList(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var trendingRequest = TrendingRequest()
        trendingRequest.currentPage = 1
        trendingRequest.recordsPerPage = 10
        trendingRequest.search = ""

        val call = helper.getTrendingOfferingListNew(trendingRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<NewOfferTopArtistResponse>() {
            override fun onSuccess(any: NewOfferTopArtistResponse?, message: String) {
                fg.hideProcessDialog()
                val response = any as NewOfferTopArtistResponse
                if (!any.responseCollection.isNullOrEmpty()) {


                    fg.binding.rvTrendinOffers?.visibility = View.VISIBLE
                    fg.binding.tvTrendingOffers?.visibility = View.VISIBLE
                    /*  fg.rvRecently?.visibility = View.VISIBLE
                        fg.tvTrendingOffers?.visibility = View.VISIBLE */
                    //  fg.trendingOfferAdapter.setData(getTrendingFilterData(any.responseCollection))
                    fg.trendingOfferAdapter?.setData(any.responseCollection)
                } else {
                    /* fg.rvRecently?.visibility = View.GONE
                     fg.tvTrendingOffers?.visibility = View.GONE*/
                    /*    fg.binding.gp_trending?.visibility = View.GONE*/
                    fg.binding.rvTrendinOffers?.visibility = View.GONE
                    fg.binding.tvTrendingOffers?.visibility = View.GONE
                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })
    }

    /*
      This is the new api in which we will get all the data like top artist,freatured list and trending list
     */
    //Old Api
    fun topArtistsList(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var trendingRequest = TrendingRequest()
        trendingRequest.currentPage = 1
        trendingRequest.recordsPerPage = 10
        trendingRequest.search = ""

        val call = helper.getTopArtistsNew(trendingRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<NewTopArtistsResponse>() {
            override fun onSuccess(any: NewTopArtistsResponse?, message: String) {
                fg.hideProcessDialog()
                val response = any as NewTopArtistsResponse
                if (!any?.responseCollection.isNullOrEmpty()) {

                    fg.binding.rvArtist.visibility = View.VISIBLE
                    fg.binding.tvartist.visibility = View.VISIBLE
                    //any!!.topArtists.add(any.topArtists[any.topArtists.size - 1])
                    fg.artistAdapter.setData(any!!.responseCollection)
                }

            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()
            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }
        })
    }

    /*
       This is the new api in which we will get all the data like top artist,freatured list and trending list
      */
    fun getHomeScreenData(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        if (!fg.binding.swpMain.isRefreshing &&
            isProgressDialog
        ) {
            // fg.showProcessDialog()
        }
        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.homeDataObservable = helper.getHomeScreenData("application/json")
        disposables.add(sendApiRequest(fg.homeDataObservable!!)!!.subscribeWith(object :
            CallbackWrapper<HomeScreenCommonModel>() {
            override fun onSuccess(any: HomeScreenCommonModel?, message: String?) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (any.response!!.topArtists!!.isEmpty()) {
                        fg.binding.rvArtist.visibility = View.GONE
                        fg.binding.tvartist.visibility = View.GONE
                    } else {
                        fg.binding.rvArtist.visibility = View.VISIBLE
                        fg.binding.tvartist.visibility = View.VISIBLE
                    }
                    if (!any.response!!.trendingList!!.isEmpty()) {

                        fg.binding.rvTrendinOffers.visibility = View.VISIBLE
                        fg.binding.rvTrendinOffers.visibility = View.VISIBLE
                        //any!!.topArtists.add(any.topArtists[any.topArtists.size - 1])
                        fg.artistAdapter.setData(any!!.response!!.topArtists!!)

                        fg.freaturedArtistAdapter.setData(any!!.response!!.featuredList!!)
                        fg.trendingOfferAdapter?.setData(any!!.response!!.trendingList!!)

                    } else {
                        fg.binding.rvTrendinOffers.visibility = View.GONE
                        fg.binding.rvTrendinOffers.visibility = View.GONE
                    }
                }

                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }

        }))

    }

    private fun getTrendingFilterData(responseCollection: MutableList<TrendingOfferingData>): MutableList<TrendingOfferingData> {
        //--> Code to filter Negative timer
        val temp = ArrayList<TrendingOfferingData>()
        for (data in responseCollection) {
            if (data.timeLeftToRelease ?: 0 > 0) {
                temp.add(data)
            }
        }
        return temp
    }

    fun apiCallRecentUploaded() {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        fg.isLoading = true
        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.recentUpload(recentUploadCurrentPageCount, 10, "")
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<RecentUploadedResponse>() {
            override fun onSuccess(any: RecentUploadedResponse?, message: String?) {
                val response = any as RecentUploadedResponse
                response.responseStatus.let { status ->
                    when (status) {
                        1 -> {
                            fg.binding.tvrecently.visibility = View.VISIBLE
                            if (recentUploadCurrentPageCount == 1)
                                fg.adapter.clearList()
                            fg.adapter.updateList(response.responseCollection)
                            fg.isDataAvailable = response.responseCollection.size >= 10
                        }
                        0 -> {
                            fg.isDataAvailable = false
                            if (recentUploadCurrentPageCount == 1) {
                                fg.showToast(
                                    fg.requireActivity(),
                                    response.responseMessage ?: "",
                                    CustomToast.ToastType.FAILED
                                )
                            }
                        }
                    }
                }
                recentUploadCurrentPageCount += 1
                if (fg.adapter.listData.isEmpty()) {
                    fg.binding.tvrecently.visibility = View.GONE
                } else {
                    fg.binding.tvrecently.visibility = View.VISIBLE
                }
                fg.isLoading = false
                // noData()
                if (fg.binding.progressBar.visibility == View.VISIBLE) {
                    fg.isLoading = false
                    fg.binding.progressBar.visibility = View.INVISIBLE
                    //  fg.binding.rvRecently.invalidate()
                }
            }

            override fun onError(error: RetroError?) {
                // noData()
                if (fg.binding.progressBar.visibility == View.VISIBLE) {
                    fg.binding.progressBar.visibility = View.INVISIBLE
                }
                if (recentUploadCurrentPageCount == 1)
                    fg.showToast(
                        fg.requireActivity(),
                        error?.errorMessage ?: "",
                        CustomToast.ToastType.FAILED
                    )
            }
        }))

    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (fg.binding.swpMain.isRefreshing) {
            fg.binding.swpMain.isRefreshing = false
        } else if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        //  noData()
    }


    fun getNotificationCount() {
        if (!CommonUtils.isLogin(fg.requireContext())) {
            return
        }
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getNotificationCount()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<NotificationResponse>() {

            override fun onSuccess(any: NotificationResponse?, message: String?) {
                if (any?.responseCollection != null &&
                    any.responseStatus == 1
                ) {
                    PrefUtils.saveValueInPreference(
                        fg.requireContext(), PrefUtils.notificationCount, any.totalRecords
                    )
                }
                getReferralCount()
            }

            override fun onError(error: RetroError?) {
                getReferralCount()
            }

        }))
    }

    fun getReferralCount() {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideProcessDialog()
            PrefUtils.saveValueInPreference(
                fg.requireContext(), PrefUtils.referralCount, 0
            )

            return
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getReferralCount()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<NotificationResponse>() {

            override fun onSuccess(any: NotificationResponse?, message: String?) {

                if (any?.responseCollection != null &&
                    any.responseStatus == 1
                ) {
                    PrefUtils.saveValueInPreference(
                        fg.requireContext(), PrefUtils.referralCount, any.totalRecords
                    )
                }
                fg.updateNotificationIcon()
            }

            override fun onError(error: RetroError?) {
                PrefUtils.saveValueInPreference(
                    fg.requireContext(), PrefUtils.referralCount, 0
                )
                fg.updateNotificationIcon()
            }

        }))

    }

    override fun onDestroy() {
        Log.d("Destory", "")
        fg.hideProcessDialog()
        if (fg.binding.swpMain.isRefreshing) {
            fg.binding.swpMain.isRefreshing = false
        }
        super.onDestroy()
    }

    //Send Wallet Address

}