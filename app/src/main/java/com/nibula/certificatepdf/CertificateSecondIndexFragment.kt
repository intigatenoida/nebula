package com.nibula.certificatepdf

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.FragmetnCertificateSecondIndexBinding
import com.nibula.investorcertificates.modals.InvestorCertificateDetailResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant


class CertificateSecondIndexFragment : BaseFragment() {
    lateinit var rootView: View
    var recordId: String = ""
    var investmentId: Int = 0
    var colorCode: Int = 0x000000
    lateinit var pdfFileUrl: String
    var recordTypeId: Int? = 0
    private lateinit var binding: FragmetnCertificateSecondIndexBinding

    companion object {

        fun newInstance(bundle: Bundle?): CertificateSecondIndexFragment {
            val fg = CertificateSecondIndexFragment()
            fg.arguments = bundle
            return fg
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //rootView = inflater.inflate(R.layout.fragmetn_certificate_second_index, container, false)
        binding = FragmetnCertificateSecondIndexBinding.inflate(inflater, container, false)
        rootView = binding.root

        binding.ivBack.setOnClickListener {
            requireActivity().finish()
        }
        binding.icMore.setOnClickListener {

            val popupMenu = PopupMenu(requireActivity(), binding.icMore)
            popupMenu.menuInflater.inflate(R.menu.menu_investor_certificate, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { menuItem -> // Toast message on menu item clicked

                when (menuItem.itemId) {
                    R.id.item1 -> {

                        generatePdfFile()

                        true
                    }

                }
                true
            }

            popupMenu.show()
        }

        recordId = requireArguments().getString(AppConstant.RECORD_ID, "").toString()
        investmentId = requireArguments().getInt(AppConstant.ID, 0)!!
        colorCode = requireArguments().getInt(AppConstant.COLOR_HEXA, 0)!!
        recordTypeId = requireArguments().getInt(AppConstant.RECORD_TYPE_ID, 0)!!
        getInvestmentDetail()
        return rootView
    }

    fun getInvestmentDetail() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            hideProcessDialog()
            return
        }
        showProcessDialog()

        val helper =
            ApiClient.getClientMusic(requireActivity()).create(ApiAuthHelper::class.java)
        val call = helper.getCertificateDetail(investmentId)
        call.enqueue(object : CallBackManager<InvestorCertificateDetailResponse>() {
            override fun onSuccess(any: InvestorCertificateDetailResponse?, message: String) {
                hideProcessDialog()
                binding.clMainRoot.visibility = View.VISIBLE
                binding.clMainRoot.setBackgroundColor(colorCode)
                any!!.response?.artistName.let {
                    binding.tvArtistName.text = it
                }
                any!!.response?.recordTitle.let {
                    binding.tvRecordTitle.text = it
                }


                when (recordTypeId) {
                    1 -> {
                        binding.tvfixedISCR.text = "UPC/EAN"
                        binding.tvvauleBlockChain.text = "1234567890"

                    }
                    else -> {
                        binding.tvvauleBlockChain.text = any?.response!!.iSWCCode

                    }
                }
            }

            override fun onFailure(message: String) {
                hideProcessDialog()
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
            }
        })
    }


    fun generatePdfFile() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            hideProcessDialog()
            return
        }
        showProcessDialog()

        val helper =
            ApiClient.getClientMusic(requireActivity()).create(ApiAuthHelper::class.java)
        val call = helper.generatePdfFile(investmentId)
        call.enqueue(object : CallBackManager<InvestorCertificateDetailResponse>() {
            override fun onSuccess(any: InvestorCertificateDetailResponse?, message: String) {
                hideProcessDialog()
                pdfFileUrl = any?.response?.pdfUrl.toString()
                val browserIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(pdfFileUrl)
                )
                startActivity(browserIntent)
            }

            override fun onFailure(message: String) {
                hideProcessDialog()
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
            }
        })
    }
}