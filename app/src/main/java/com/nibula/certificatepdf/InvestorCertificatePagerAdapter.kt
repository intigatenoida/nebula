package com.nibula.certificatepdf


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nibula.utils.SmartFragmentStatePagerAdapter

class InvestorCertificatePagerAdapter(fn: FragmentManager,var bundle: Bundle) : SmartFragmentStatePagerAdapter(fn) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                CertificateFirstIndexFragment.newInstance(bundle)
            }
            else -> {
                CertificateSecondIndexFragment.newInstance(bundle)
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}