package com.nibula.certificatepdf

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import androidx.appcompat.widget.PopupMenu
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.customview.CustomToast
import com.nibula.databinding.ActivityInvestorNewCertificateBinding
import com.nibula.investorcertificates.modals.InvestorCertificateDetailResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant

class InvestorCertificate : BaseActivity() {
    private var myWebView: WebView? = null
    var recordId: String = ""
    var investmentId: Int = 0
    var colorCode: Int = 0x000000
    lateinit var pdfFileUrl: String
    private lateinit var binding:ActivityInvestorNewCertificateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_investor_new_certificate)
        binding=ActivityInvestorNewCertificateBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.ivBack.setOnClickListener {
            finish()
        }
        binding.icMore.setOnClickListener {
            val popupMenu = PopupMenu(this@InvestorCertificate, binding.icMore)
            popupMenu.menuInflater.inflate(R.menu.menu_investor_certificate, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { menuItem -> // Toast message on menu item clicked

                when (menuItem.itemId) {
                    R.id.item1 -> {

                        generatePdfFile()

                        true
                    }
                }
                true
            }
            popupMenu.show()
        }
        recordId = intent.extras?.getString(AppConstant.RECORD_ID, "").toString()
        investmentId = intent.extras?.getInt(AppConstant.ID, 0)!!
        colorCode = intent.extras?.getInt(AppConstant.COLOR_HEXA, 0)!!
        getInvestmentDetail()
        /*
        val webView = findViewById<WebView>(R.id.webview) as WebView
        webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(
                view: WebView,
                url: String
            ): Boolean {
                return false
            }

            override fun onPageFinished(view: WebView, url: String) {
                myWebView = null
            }

            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler?,
                error: SslError?
            ) {
                var message = "SSL Certificate error."
                when (error?.primaryError ?: -1) {
                    SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                    SslError.SSL_EXPIRED -> message = "The certificate has expired."
                    SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                    SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
                }
                message += " Do you want to continue anyway?"
                val builder: AlertDialog.Builder = AlertDialog.Builder(this@InvestorCertificate)
                builder.setMessage(message)
                builder.setPositiveButton("continue",
                    { _, _ -> handler!!.proceed() })
                builder.setNegativeButton("cancel",
                    { _, _ -> handler!!.cancel() })
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }
        webView.loadUrl(ApiClient.BASE_URL_MUSIC + "api/invest/GetCertificate?InvestmentId=${investmentId}")
        webView.settings.allowContentAccess = true
        webView.settings.allowFileAccess = true
        webView.pageUp(true)
        myWebView = webView

        ivDownload.setOnClickListener {
            var path = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                Environment.getStorageDirectory().absolutePath + "/" + "Nebula Investors"
            } else {

                Environment.getExternalStorageDirectory().absolutePath + "/" + "Nebula Investors"
            }
            val fileName = "InvestorCertificate.pdf"
            val dir = File(path);
            if (!dir.exists())
                dir.mkdirs()

            val file = File(dir, fileName)
            val progressDialog = ProgressDialog(this@InvestorCertificate)
            progressDialog.setMessage("Please wait")
            progressDialog.show()
            PdfView.createWebPdfJob(
                this@InvestorCertificate,
                webView,
                file,
                fileName,
                object : PdfView.Callback {

                    override fun success(path: String) {
                        progressDialog.dismiss()
                        val builder = AlertDialog.Builder(this@InvestorCertificate)
                        with(builder) {
                            setTitle("Certificate")
                            setMessage("Do you want to open a file?")
                            setPositiveButton("Open") { dialog, whichButton ->
                                openPdfFile(this@InvestorCertificate, path)
                                //sendMail(path)
                            }
                            setNegativeButton("Cancel") { dialog, whichButton ->
                                //showMessage("Close the game or anything!")
                                dialog.dismiss()
                            }

                            // Dialog
                            val dialog = builder.create()

                            dialog.show()
                        }
                    }

                    override fun failure() {
                        progressDialog.dismiss()

                    }
                })
        }*/
    }

    fun getInvestmentDetail() {
        if (!isOnline()) {
            showToast(
                this@InvestorCertificate,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK)
            hideProcessDailog()
            return
        }
        showProcessDialog()
        val helper =
            ApiClient.getClientMusic(this@InvestorCertificate).create(ApiAuthHelper::class.java)
        val call = helper.getCertificateDetail(investmentId)
        call.enqueue(object : CallBackManager<InvestorCertificateDetailResponse>() {
            override fun onSuccess(any: InvestorCertificateDetailResponse?, message: String) {
                hideProcessDailog()
                binding.clMainRoot.visibility = View.VISIBLE
                binding.clMainRoot.setBackgroundColor(colorCode)
                any!!.response?.artistName.let {
                    binding.tvArtistName.text = it
                }
                any!!.response?.recordTitle.let {
                    binding.tvRecordTitle.text = it
                }

                any!!.response?.recordTitle.let {
                    binding.tvAlbumName.text = it
                }
                any!!.response?.artistName.let {
                    binding.tvAlbumOwnerName.text = it
                }
                any!!.response?.artistName.let {
                    binding.tvArtistName01.text = it
                }
                any!!.response?.sharePercentage.let {
                    binding.tvOwnerShipPercentage.text = "$it%"
                }

                any!!.response?.sharePercentage.let {
                    binding.tvOwnerShipPercentage.text = it
                }

                any!!.response?.countryOfIssue.let {
                    binding.tvCountryIssueValue.text = it
                }

                any!!.response?.investorNumber.let {
                    binding.tvInvesterNumberValue.text = it
                }

                any!!.response?.dateOfInvestment.let {
                    binding.tvInvestmentValue.text = it
                }

                any!!.response?.referenceCode.let {
                    binding.tvReferenceNumberCode.text = it
                }
                any!!.response?.recordImage.let {
                    Glide.with(this@InvestorCertificate).load(it)
                        .placeholder(R.drawable.nebula_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
                        .into(binding.clImage2)
                }

            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }
        })
    }

    fun generatePdfFile() {
        if (!isOnline()) {
            showToast(
                this@InvestorCertificate,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            hideProcessDailog()
            return
        }
        showProcessDialog()

        val helper =
            ApiClient.getClientMusic(this@InvestorCertificate).create(ApiAuthHelper::class.java)
        val call = helper.generatePdfFile(investmentId)
        call.enqueue(object : CallBackManager<InvestorCertificateDetailResponse>() {
            override fun onSuccess(any: InvestorCertificateDetailResponse?, message: String) {
                hideProcessDailog()
                pdfFileUrl = any?.response?.pdfUrl.toString()
                val browserIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(pdfFileUrl)
                )
                startActivity(browserIntent)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }
        })
    }

}