package com.nibula.certificatepdf

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.ActivityInvestorNewCertificateBinding
import com.nibula.investorcertificates.modals.InvestorCertificateDetailResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant


class CertificateFirstIndexFragment : BaseFragment() {
    lateinit var rootView: View
    var recordId: String = ""
    var investmentId: Int = 0
    var recordTypeID: Int = 0
    var colorCode: Int = 0x000000
    lateinit var pdfFileUrl: String
    private lateinit var binding: ActivityInvestorNewCertificateBinding

    companion object {
        fun newInstance(bundle: Bundle?): CertificateFirstIndexFragment {
            val fg = CertificateFirstIndexFragment()
            fg.arguments = bundle
            return fg
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // rootView = inflater.inflate(R.layout.activity_investor_new_certificate, container, false)
        binding = ActivityInvestorNewCertificateBinding.inflate(inflater, container, false)
        rootView = binding.root
        binding.ivBack.setOnClickListener {
            requireActivity().finish()
        }
        binding.icMore.setOnClickListener {
            val popupMenu = PopupMenu(requireActivity(), binding.icMore)
            popupMenu.menuInflater.inflate(R.menu.menu_investor_certificate, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { menuItem -> // Toast message on menu item clicked

                when (menuItem.itemId) {
                    R.id.item1 -> {

                        generatePdfFile()

                        true
                    }

                }
                true
            }

            popupMenu.show()
        }
        recordId = requireArguments().getString(AppConstant.RECORD_ID, "").toString()
        investmentId = requireArguments().getInt(AppConstant.ID, 0)!!
        colorCode = requireArguments().getInt(AppConstant.COLOR_HEXA, 0)!!
        recordTypeID = requireArguments().getInt(AppConstant.RECORD_TYPE_ID, 0)!!


        getInvestmentDetail()
        return rootView
    }

    fun getInvestmentDetail() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            hideProcessDialog()
            return
        }
        showProcessDialog()

        val helper = ApiClient.getClientMusic(requireActivity()).create(ApiAuthHelper::class.java)
        val call = helper.getCertificateDetail(investmentId)
        call.enqueue(object : CallBackManager<InvestorCertificateDetailResponse>() {
            override fun onSuccess(any: InvestorCertificateDetailResponse?, message: String) {
                hideProcessDialog()
                binding.clMainRoot.visibility = View.VISIBLE
                binding.clMainRoot.setBackgroundColor(colorCode)
                any!!.response?.artistName.let {
                    binding.tvArtistName.text = it
                }
                any!!.response?.recordTitle.let {
                    binding.tvRecordTitle.text = it
                }

                any!!.response?.recordTitle.let {
                    binding.tvAlbumName.text = it
                }
                any!!.response?.artistName.let {
                    binding.tvAlbumOwnerName.text = it
                }
                any!!.response?.artistName.let {
                    binding.tvArtistName01.text = it
                }
                any!!.response?.sharePercentage.let {
                    binding.tvOwnerShipPercentage.text = "$it%"
                }

                any!!.response?.countryOfIssue.let {
                    binding.tvCountryIssueValue.text = it
                }

                any!!.response?.investorNumber.let {
                    binding.tvInvesterNumberValue.text = it
                }

                any!!.response?.dateOfInvestment.let {
                    binding.tvInvestmentValue.text = it
                }
                any!!.response?.referenceCode.let {
                    binding.tvReferenceNumberCode.text = it
                }
                any!!.response?.recordImage.let {
                    Glide.with(requireActivity()).load(it)
                        .placeholder(R.drawable.nebula_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
                        .into(binding.clImage2)

                }

                when (recordTypeID) {
                    1 -> {
                        binding.clAlbumInfo.visibility = View.VISIBLE
                    }
                    else -> {
                        binding.clAlbumInfo.visibility = View.GONE
                    }
                }

            }

            override fun onFailure(message: String) {
                hideProcessDialog()
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
            }
        })
    }

    fun generatePdfFile() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            hideProcessDialog()
            return
        }
        showProcessDialog()

        val helper = ApiClient.getClientMusic(requireActivity()).create(ApiAuthHelper::class.java)
        val call = helper.generatePdfFile(investmentId)
        call.enqueue(object : CallBackManager<InvestorCertificateDetailResponse>() {
            override fun onSuccess(any: InvestorCertificateDetailResponse?, message: String) {
                hideProcessDialog()
                pdfFileUrl = any?.response?.pdfUrl.toString()
                val browserIntent = Intent(
                    Intent.ACTION_VIEW, Uri.parse(pdfFileUrl)
                )
                startActivity(browserIntent)
            }

            override fun onFailure(message: String) {
                hideProcessDialog()
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
            }
        })
    }
}