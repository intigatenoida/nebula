package com.nibula.certificatepdf

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.widget.PopupMenu
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.customview.CustomToast
import com.nibula.databinding.ActivityNewCertificateBinding
import com.nibula.investorcertificates.modals.InvestorCertificateDetailResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant



class NewCertificateActivity : BaseActivity() {
    var recordImageColorCode: Int = 0x000000
     var investmentId: Int=0
    private lateinit var binding:ActivityNewCertificateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_new_certificate)
        binding= ActivityNewCertificateBinding.inflate(layoutInflater)
        setContentView(binding.root)
        var arguments = intent?.extras
        investmentId = arguments!!.getInt(AppConstant.ID)
        binding.tvRecordTitle.text = arguments!!.getString(AppConstant.RECORD_TITLE)
        binding.tvArtistName.text = arguments!!.getString(AppConstant.RECORD_ARTIST_NAME)
        recordImageColorCode = arguments!!.getInt(AppConstant.COLOR_HEXA)
        binding.cl1.setBackgroundColor(recordImageColorCode)
        binding.ivBack.setOnClickListener {
            finish()
        }
        binding.icMore.setOnClickListener {


            val popupMenu = PopupMenu(this@NewCertificateActivity, binding.icMore)
            popupMenu.menuInflater.inflate(R.menu.menu_investor_certificate, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { menuItem -> // Toast message on menu item clicked

                when (menuItem.itemId) {
                    R.id.item1 -> {

                        generatePdfFile()

                        true
                    }

                }
                true
            }

            popupMenu.show()

        }
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, InvestorCertificateFragment.newInstance(arguments!!)).commit()
    }


    fun generatePdfFile() {
        if (!isOnline()) {
            showToast(
                this@NewCertificateActivity,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            hideProcessDailog()
            return
        }
        showProcessDialog()

        val helper =
            ApiClient.getClientMusic(this@NewCertificateActivity).create(ApiAuthHelper::class.java)
        val call = helper.generatePdfFile(investmentId)
        call.enqueue(object : CallBackManager<InvestorCertificateDetailResponse>() {
            override fun onSuccess(any: InvestorCertificateDetailResponse?, message: String) {
                hideProcessDailog()
              var  pdfFileUrl = any?.response?.pdfUrl.toString()
                val browserIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(pdfFileUrl)
                )
                startActivity(browserIntent)
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }
        })
    }

}