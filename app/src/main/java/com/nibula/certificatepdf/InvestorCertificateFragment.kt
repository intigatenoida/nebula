package com.nibula.certificatepdf

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.nibula.R

class InvestorCertificateFragment : Fragment() {
    private lateinit var investorCertificatePagerAdapter: InvestorCertificatePagerAdapter
    private lateinit var viewPager: ViewPager
    companion object {
        fun newInstance(bundle: Bundle?): InvestorCertificateFragment {
            val fg = InvestorCertificateFragment()
            fg.arguments = bundle
            return fg
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_investor_certificate, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewPager = view.findViewById(R.id.pager)
        investorCertificatePagerAdapter = InvestorCertificatePagerAdapter(requireActivity().supportFragmentManager,requireArguments())
        viewPager.adapter = investorCertificatePagerAdapter
        viewPager.offscreenPageLimit = 2
    }
}

