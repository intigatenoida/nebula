package com.nibula.request_to_invest

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.FragmentFinalCheckoutBinding
import com.nibula.databinding.LayoutBottomSheetGetNotifiedBinding
import com.nibula.utils.CommonUtils
import com.nibula.utils.interfaces.DialogFragmentClicks



class GetNotifyBottomSheetFragment() : BaseBottomSheetFragment() {

    constructor(
        recordId: String, dgFrgListener: DialogFragmentClicks
    ) : this() {
        this.dgFrgListener = dgFrgListener
        this.recordId = recordId
    }

    lateinit var dgFrgListener: DialogFragmentClicks

    private lateinit var recordId: String

    private lateinit var rootView: View
    lateinit var binding: LayoutBottomSheetGetNotifiedBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.layout_bottom_sheet_get_notified, container, false)

            binding=LayoutBottomSheetGetNotifiedBinding.inflate(inflater,container,false)
            rootView=binding.root
        }

        binding.edtEmail?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
                binding.txtError.visibility = View.GONE
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

            }
        })
        binding.btnSubmit.setOnClickListener {
            if (!CommonUtils.isValidEmail(binding.edtEmail.text.toString().trim())) {
                binding.txtError.visibility = View.VISIBLE
                binding.txtError.text = getString(R.string.valid_email_address)
            } else {
                clickEvent()
            }
        }

        binding.ivClose.setOnClickListener {
            val b = Bundle()
            dgFrgListener.onNegativeButtonClick(this, b, dialog!!)
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }

    fun clickEvent() {
        val b = Bundle()
        b.putString("email", binding.edtEmail?.text.toString())
        dgFrgListener.onPositiveButtonClick(this, b, dialog!!)
    }

}