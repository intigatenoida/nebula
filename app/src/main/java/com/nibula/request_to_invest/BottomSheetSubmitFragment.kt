package com.nibula.request_to_invest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.LayoutSubmitFragmentBinding
import com.nibula.request_to_invest.viewmodal.PaymentInfoViewModel
import com.nibula.utils.CommonUtils
import com.nibula.utils.interfaces.DialogFragmentClicks


class BottomSheetSubmitFragment() : BaseBottomSheetFragment() {

    constructor(
        recordId: String, dgFrgListener: DialogFragmentClicks, submitButton: OnSubmitButton
    ) : this() {
        this.dgFrgListener = dgFrgListener
        this.recordId = recordId
        this.submitButton = submitButton
    }

    lateinit var dgFrgListener: DialogFragmentClicks
    var stripePrice: Double = 0.0
    var paypalPrice: Double = 0.0
    var walletPrice: Double = 0.0
    var valuePerShareInDolor: Double = 0.0
    var numberOfToken: Int = 0
    private lateinit var recordId: String

    private lateinit var rootView: View
    lateinit var submitButton: OnSubmitButton
    lateinit var paymentInfoViewModel: PaymentInfoViewModel
    var stripeFixedPrice: Double = 0.0
    var paypalFixedPrice: Double = 0.0
    var ethereumFixedPrice: Double = 0.0
    var selectedPaymentMethod: Int = 0
    lateinit var binding:LayoutSubmitFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            /*rootView =
                inflater.inflate(R.layout.layout_submit_fragment, container, false)*/

            binding= LayoutSubmitFragmentBinding.inflate(inflater,container,false)

            rootView=binding.root

            paymentInfoViewModel = activity?.run {
                ViewModelProvider(this).get(PaymentInfoViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
        }

        paymentInfoViewModel.paymentMethod?.value.let {

            selectedPaymentMethod = it!!
        }
        paymentInfoViewModel.numberOfToken.value?.let {
            numberOfToken = it!!
            binding.tvTotalQuantity.text = "Total Quantity: $it"
        }

        paymentInfoViewModel.recordDetailResponse.value?.valuePerShareInDollars.let {
            valuePerShareInDolor = it!! * numberOfToken
        }

        paymentInfoViewModel.costBreakDownResponse.value?.response.let {

            binding.tvValueBlockChain.text =
                "${it!!.currencyTypeSymbol}" + CommonUtils.trimDecimalToThreePlaces(
                    it.ethereumTransactionCost ?: 0.0
                )

            binding.tvValueStripe.text =
                "${it!!.currencyTypeSymbol}" + CommonUtils.trimDecimalToThreePlaces(
                    it.stripeTransactionFees ?: 0.0
                )

            binding.tvValuePaypal.text =
                "${it!!.currencyTypeSymbol}" + CommonUtils.trimDecimalToThreePlaces(
                    it.paypalTransactionFees ?: 0.0
                )
            val ethereumFixedPrice = it.ethereumTransactionCost ?: 0.0
            val royaltySharePrice = it.priceMusicRightCoOwnership ?: 0.0
            stripeFixedPrice = it?.stripeTransactionFees ?: 0.0
            paypalFixedPrice = it?.paypalTransactionFees ?: 0.0

            binding.tvValueSubTotal.text =
                " ${it?.currencyTypeSymbol ?: "USD"}${
                    CommonUtils.trimDecimalToTwoPlaces(
                        royaltySharePrice.plus(ethereumFixedPrice)
                    )
                }"

            when (selectedPaymentMethod) {
                1 -> {

                    ("${it.currencyTypeSymbol!!}" + (ethereumFixedPrice.plus(
                        valuePerShareInDolor.plus(
                            stripeFixedPrice
                        )
                    )).let {
                        stripePrice = it
                        CommonUtils.trimDecimalToTwoPlaces(it)
                        paymentInfoViewModel.setTotalAmount(it)
                    })
                    binding.tvValueTotal.text =
                        updateTotalPrice(it.currencyTypeSymbol!!, stripePrice)
                    binding.tvTotalAmount.text =
                        updateTotalPrice(it.currencyTypeSymbol!!, stripePrice)

                }
                2 -> {
                    ("${it.currencyTypeSymbol!!}" + (ethereumFixedPrice.plus(
                        valuePerShareInDolor.plus(
                            paypalFixedPrice
                        )
                    )).let {
                        paypalPrice = it
                        CommonUtils.trimDecimalToTwoPlaces(it)
                        paymentInfoViewModel.setTotalAmount(it)
                    })
                    binding.tvTotalAmount.text =
                        updateTotalPrice(it.currencyTypeSymbol!!, paypalPrice)
                    binding.tvValueTotal.text =
                        updateTotalPrice(it.currencyTypeSymbol!!, paypalPrice)

                }
                else -> {
                    binding.tvValueTotal.text =
                        updateTotalPrice(it.currencyTypeSymbol!!, walletPrice)

                }
            }
        }

        paymentInfoViewModel.numberOfToken.value?.let {
            binding.tvTotalQuantity.text = "Total Quantity: $it"
        }
        binding.ivClose.setOnClickListener {
            val b = Bundle()
            dgFrgListener.onNegativeButtonClick(this, b, dialog!!)
        }

        binding.btnSubmit.setOnClickListener {
            submitButton.onClickOfSubmitButton()
        }

        paymentInfoViewModel.rImage?.value.let {
            Glide
                .with(requireActivity())
                .load(it)
                .placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.IMMEDIATE)
                .into(binding.clImage2)
        }


        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }

    fun updateTotalPrice(currencyType: String, price: Double): String {
        return "Total: ${currencyType ?: "USD"}${CommonUtils.trimDecimalToTwoPlaces(price)}"
    }

}