package com.nibula.request_to_invest

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.CreateDefaultWalletAddressBinding
import com.nibula.utils.interfaces.DialogFragmentClicks

import org.bouncycastle.asn1.x9.X9ECParameters
import org.bouncycastle.crypto.AsymmetricCipherKeyPair
import org.bouncycastle.crypto.ec.CustomNamedCurves
import org.bouncycastle.crypto.generators.ECKeyPairGenerator
import org.bouncycastle.crypto.params.ECDomainParameters
import org.bouncycastle.crypto.params.ECKeyGenerationParameters
import org.bouncycastle.crypto.params.ECPrivateKeyParameters
import org.bouncycastle.crypto.params.ECPublicKeyParameters
import org.bouncycastle.math.ec.ECPoint
import org.bouncycastle.util.encoders.Hex
import java.math.BigInteger
import java.security.SecureRandom

class BottomSheetCreateWalletAddress() : BaseBottomSheetFragment() {

    constructor(
        dgFrgListener: DialogFragmentClicks
    ) : this() {
        this.dgFrgListener = dgFrgListener
    }

    lateinit var dgFrgListener: DialogFragmentClicks

    private lateinit var rootView: View
    private var clipboardManager: ClipboardManager? = null
    private lateinit var privatekeyHex: String
    lateinit var binding:CreateDefaultWalletAddressBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
            /*rootView =
                inflater.inflate(R.layout.create_default_wallet_address, container, false)
            */
            binding= CreateDefaultWalletAddressBinding.inflate(inflater,container,false)

            rootView=binding.root
            val walletAddress = generateWalletAddress()

            binding.ivCopyWallet.setOnClickListener {
                if (walletAddress.isNotEmpty()) {
                    clipboardManager =
                        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                    val clipData = ClipData.newPlainText("key", walletAddress)
                    clipboardManager?.setPrimaryClip(clipData)
                    Toast.makeText(
                        requireContext(),
                        "Wallet Address Copied",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "No text to be copied",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            binding.clCreateWalletAddress.setOnClickListener {

                openBackUpWalletPopUp()
            }

            binding.ivCopyPrivatekey.setOnClickListener {
                if (privatekeyHex.isNotEmpty()) {
                    clipboardManager =
                        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                    val clipData = ClipData.newPlainText("key", privatekeyHex)
                    clipboardManager?.setPrimaryClip(clipData)
                    Toast.makeText(
                        requireContext(),
                        "Private Key Copied",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "No text to be copied",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            binding.ivClose.setOnClickListener {

                val b = Bundle()
                dgFrgListener.onNegativeButtonClick(this, b, dialog!!)
            }

        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }

    @SuppressLint("SetTextI18n")
    fun generateWalletAddress(): String {

        val secureRandom = SecureRandom()
        val keyPairGenerator = ECKeyPairGenerator()
        val curveName = "secp256k1"
        val curveParams: X9ECParameters = CustomNamedCurves.getByName(curveName)
        // Create an instance of ECDomainParameters from the curve parameters
        val domainParams =
            ECDomainParameters(curveParams.curve, curveParams.g, curveParams.n, curveParams.h)
        val keyGenParams = ECKeyGenerationParameters(domainParams, secureRandom)
        keyPairGenerator.init(keyGenParams)
        val keyPair: AsymmetricCipherKeyPair = keyPairGenerator.generateKeyPair()
        val privateKeyParams: ECPrivateKeyParameters = keyPair.private as ECPrivateKeyParameters
        val publicKeyParams: ECPublicKeyParameters = keyPair.public as ECPublicKeyParameters
        val publicKey: ECPoint = publicKeyParams.q
        val privateKey: BigInteger = privateKeyParams.d
        val publicKeyBytes = publicKey.getEncoded(false)
        val privateKeyBytes = privateKey.toByteArray()
        val walletAddressBytes = publicKeyBytes.copyOfRange(1, publicKeyBytes.size)
        val walletAddress = Hex.toHexString(walletAddressBytes)
        privatekeyHex = Hex.toHexString(privateKeyBytes)

        binding.tvWalletAddressValue.text = walletAddress
        binding.tvPrivateKeyValue.text = privatekeyHex
        walletAddress?.let {
            val subString = walletAddress.substring(0, 25)
            binding.tvWalletAddressValue.text = "oX$subString..."
        }

        privatekeyHex.let {
            val subString = privatekeyHex.substring(0, 25)
            binding.tvPrivateKeyValue.text = "$subString..."
        }
        return walletAddress
    }


    fun openBackUpWalletPopUp() {

        val dialog = BottomSheetBackUpWallet(object : DialogFragmentClicks {
            override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                dialog.dismiss()

            }

            override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                dialog.dismiss()
            }
        })
        dialog.show(requireActivity().supportFragmentManager, "BottomSheetCreateWalletAccount")
    }

}