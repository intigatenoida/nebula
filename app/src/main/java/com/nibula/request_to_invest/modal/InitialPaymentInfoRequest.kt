package com.nibula.request_to_invest.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class InitialPaymentInfoRequest(
    @SerializedName("RecordId")
    var recordId: String? = null,
    @SerializedName("RequestedShares")
    var requestedShares: Int? = null,
    @SerializedName("TotalRequestedAmount")
    var totalRequestedAmount: Double? = null,
    @SerializedName("PaymentMethodId")
    var paymentMethodId: Int? = null,
    @SerializedName("ReferralCode")
    var referralCode: String? = null,
    @SerializedName("NetAmount")
    var netAmount: Double? = null,
    @SerializedName("ShowToPublic")
    var showToPublic: Int? = null,
    @SerializedName("ApprovedPaymentInitiationToken")
    var approvedPaymentInitiationToken: String? = null


)