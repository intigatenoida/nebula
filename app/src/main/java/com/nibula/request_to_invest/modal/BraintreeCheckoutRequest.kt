package com.nibula.request_to_invest.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class BraintreeCheckoutRequest(
    @SerializedName("PaymentMethodNonce")
    var paymentMethodNonce: String? = "",
    @SerializedName("PaymentType")
    var paymentType: String? = "",
    @SerializedName("Price")
    var price: Double? = 0.0,
    @SerializedName("CurrencyIsoCode")
    var currencyIsoCode: String? =null

)