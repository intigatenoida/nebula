package com.nibula.request_to_invest.modal

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class StripPaymentData(

    @SerializedName("Brand")
    var brand:String? = "",
    @SerializedName("Country")
    var country:String? = "",
    @SerializedName("ExpiryMonth")
    var expiryMonth:Int? = 0,
    @SerializedName("ExpiryYear")
    var expiryYear:Int? = 0,
    @SerializedName("Funding")
    var funding:String? = "",
    @SerializedName("last4")
    var last4:String? = "",
    @SerializedName("PaymentMethodTypes")
    var paymentMethodTypes:String? = "",
    @SerializedName("Currency")
    var currency:String? = "",
    @SerializedName("Created")
    var created:Long? = 0,
    @SerializedName("postalCode")
    var postalCode:String? = "",

    /*Error Picture*/
    @SerializedName("ErrorCode")//
    var errorCode:String? = "",
    @SerializedName("ErrorDocUrl")
    var errorDocUrl:String? = "",
    @SerializedName("StripErrorMessage")
    var stripeErrorMessage:String? = ""

)