package com.nibula.request_to_invest.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class InvestorPaymentRequest(
    @SerializedName("ReferenceId")
    var referenceId: String? = "",
    @SerializedName("RequestToken")
    var requestToken: String? = "",
    @SerializedName("TransactionId")
    var transactionId: String? = "",
    @SerializedName("CardTypeId")
    var cardtypeid: Int? = 0,
    @SerializedName("PaymentResponse")
    var paymentResponse: String? = "",

    @SerializedName("PaymentMethodId")
    var paymentMethodId:Int? = 0,

    /*@SerializedName("StripeData")
    var stripObj:StripPaymentData? = StripPaymentData(),
    @SerializedName("PayPalData")
    var paypalObj:PayPalPaymentData?=PayPalPaymentData(),*/

    /*OurError*/
    @SerializedName("ErrorId")
    var errorId:Int?= 0,// 1 - for user cancel, 2 - for other failed
    @SerializedName("ErrorMessage")
    var errorMessage:String?= ""

)