package com.nibula.request_to_invest.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("ApprovedPaymentInitiationToken")
    var approvedPaymentInitiationToken: String? = null,
    @SerializedName("Id")
    var id: Int? = null,
    @SerializedName("RecordOwnerId")
    var recordOwnerId: String? = null,
    @SerializedName("RequestedShares")
    var requestedShares: Double? = null,
    @SerializedName("TotalRequestedAmount")
    var totalRequestedAmount: Double? = null,
    @SerializedName("ConflictMessage")
    var conflictMessage: String? = null,
    @SerializedName("SharesConflict")
    var sharesConflict: Boolean? = null

)