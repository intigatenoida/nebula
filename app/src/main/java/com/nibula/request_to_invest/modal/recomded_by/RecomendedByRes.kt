package com.nibula.request_to_invest.modal.recomded_by


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class RecomendedByRes(
    @SerializedName("Response")
    var response: Response? = Response(),
    @SerializedName("ResponseMessage")
    var responseMessage: String = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int = 0
)