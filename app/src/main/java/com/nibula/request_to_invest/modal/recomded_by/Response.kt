package com.nibula.request_to_invest.modal.recomded_by


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Response(
    @SerializedName("IsInvestedOrPurchased")
    var isInvestedOrPurchased: Boolean = false,
    @SerializedName("recommendedBy")
    var recommendedBy: ArrayList<RecommendedBy> = ArrayList(),
    @SerializedName("RecordId")
    var recordId: String = "",
    @SerializedName("RecordStatusId")
    var recordStatusId: Int = 0,
    @SerializedName("Price")
    var price: String = "0",
    @SerializedName("ApprovedRefferalCode")
    var approvedRefferalCode: String = ""

)