package com.nibula.request_to_invest.modal.recomded_by


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class RecommendedBy(
    @SerializedName("RecommendedByUserId")
    var recommendedByUserId: String = "",
    @SerializedName("RecommendedByUserImage")
    var recommendedByUserImage: String = "",
    @SerializedName("RecommendedByUserName")
    var recommendedByUserName: String = "",
    @SerializedName("ReferralCode")
    var referralCode: String = "",
    var isUserApproved : Boolean = false
)