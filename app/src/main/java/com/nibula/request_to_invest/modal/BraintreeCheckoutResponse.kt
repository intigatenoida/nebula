package com.nibula.request_to_invest.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class BraintreeCheckoutResponse(
    @SerializedName("ReferenceId")
    var referenceId: Any? = Any(),
    @SerializedName("ResponseMessage")
    var responseMessage: String? = "",
    @SerializedName("ResponseStatus")
    var responseStatus: Int? = 0,
    @SerializedName("TransactionId")
    var transactionId: String? = ""
)