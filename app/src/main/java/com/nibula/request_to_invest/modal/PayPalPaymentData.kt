package com.nibula.request_to_invest.modal

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class PayPalPaymentData(
    @SerializedName("Created")
    var created: String? = "",
    @SerializedName("Id")
    var id: String? = "",
    @SerializedName("Intent")
    var mIntent: String? = "",
    @SerializedName("State")
    var state: String? = ""
)
