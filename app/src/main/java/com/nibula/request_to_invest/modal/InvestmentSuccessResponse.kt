package com.nibula.request_to_invest.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class InvestmentSuccessResponse(
    @SerializedName("PId")
    var pId: Int? = 0
):BaseResponse()