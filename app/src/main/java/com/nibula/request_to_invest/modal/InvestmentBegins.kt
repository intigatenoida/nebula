package com.nibula.request_to_invest.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class InvestmentBegins(
    @SerializedName("RequestToken")
    var requestToken: String?,
    @SerializedName("ClientSecret")
    var clientSecret: String?
):BaseResponse()