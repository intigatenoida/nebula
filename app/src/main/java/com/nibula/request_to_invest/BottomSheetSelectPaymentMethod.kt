package com.nibula.request_to_invest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.LayoutBottomSheetSelectPaymentMethodBinding
import com.nibula.utils.interfaces.DialogFragmentClicks


class BottomSheetSelectPaymentMethod() : BaseBottomSheetFragment() {

    constructor(
        recordId: String,
        dgFrgListener: DialogFragmentClicks,
        onPaymentMethodClick: OnPaymentMethodClick
    ) : this() {
        this.dgFrgListener = dgFrgListener
        this.recordId = recordId
        this.onPaymentMethodClick = onPaymentMethodClick
    }

    lateinit var dgFrgListener: DialogFragmentClicks
    lateinit var onPaymentMethodClick: OnPaymentMethodClick

    private lateinit var recordId: String

    private lateinit var rootView: View
    lateinit var binding:LayoutBottomSheetSelectPaymentMethodBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {

            binding=LayoutBottomSheetSelectPaymentMethodBinding.inflate(inflater,container,false)
            rootView=binding.root
           /* rootView = inflater.inflate(
                R.layout.layout_bottom_sheet_select_payment_method,
                container,
                false
            )
*/


        }

        binding.clCreditCard.setOnClickListener {
            onPaymentMethodClick.onPaymentMethodClick(1)
        }

        binding.clPaypal.setOnClickListener {
            onPaymentMethodClick.onPaymentMethodClick(2)

        }
        binding.ivClose.setOnClickListener {
            val b = Bundle()
            dgFrgListener.onNegativeButtonClick(this, b, dialog!!)
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }

}