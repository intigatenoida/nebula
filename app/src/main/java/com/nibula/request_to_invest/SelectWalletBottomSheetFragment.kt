package com.nibula.request_to_invest


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.NoWalletBottomsheetFragmentBinding
import com.nibula.utils.interfaces.DialogFragmentClicks

class SelectWalletBottomSheetFragment() : BaseBottomSheetFragment() {

    constructor(
        dgFrgListener: DialogFragmentClicks
    ) : this() {
        this.dgFrgListener = dgFrgListener
    }

    lateinit var dgFrgListener: DialogFragmentClicks
    lateinit var binding:NoWalletBottomsheetFragmentBinding

    private lateinit var rootView: View
/*
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme1)

    }
*/

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
           /* rootView =
                inflater.inflate(R.layout.no_wallet_bottomsheet_fragment, container, false)*/


            binding= NoWalletBottomsheetFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root

            binding.ivMetamask.setOnClickListener {

                var intent = Intent(Intent.ACTION_VIEW)
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=io.metamask"))
                startActivity(intent)
            }

            binding.ivRainbow.setOnClickListener {
                var intent = Intent(Intent.ACTION_VIEW)
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=me.rainbow"))
                startActivity(intent)
            }

            binding.ivCoinbase.setOnClickListener {

                var intent = Intent(Intent.ACTION_VIEW)
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.wallet.crypto.trustapp"))
                startActivity(intent)
            }


            /*        rootView.tvWalletConnect.setOnClickListener {

                val b = Bundle()
                dgFrgListener.onPositiveButtonClick(this, b,dialog!!)
            }


            rootView.ivClose.setOnClickListener {

                val b = Bundle()
                dgFrgListener.onNegativeButtonClick(this, b,dialog!!)
            }*/

            binding.ivClose.setOnClickListener {

                val b = Bundle()
                dgFrgListener.onNegativeButtonClick(this, b, dialog!!)
            }

        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }


}