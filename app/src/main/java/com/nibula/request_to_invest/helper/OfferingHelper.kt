package com.nibula.request_to_invest.helper

import android.app.Activity
import android.content.Intent
import com.nibula.request_to_invest.OfferingFragment
import android.util.DisplayMetrics
import android.view.View
import android.view.View.*
import android.widget.Toast
import com.google.gson.Gson
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.customview.CustomToast
import com.nibula.request.MyViewCollectorsRequest
import com.nibula.request.NotifyRequest
import com.nibula.request.NotifyRequestUpdate
import com.nibula.request.RecordFavoriteRequest
import com.nibula.request_to_invest.modal.*
import com.nibula.response.BaseResponse
import com.nibula.response.details.AlbumDetailResponse
import com.nibula.response.details.Response
import com.nibula.response.mycollectorresponse.MyCollecrtorsResponse
import com.nibula.response.recorddetail.GetInvestorStatusResponse
import com.nibula.retrofit.*
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import com.nibula.utils.RequestCode
import com.stripe.android.model.PaymentIntent

class OfferingHelper(val fg: OfferingFragment) : BaseHelperFragment() {

    lateinit var detailsResponse: Response
    var isRecomened = false
    var isInvestmentStatusUpdated = false
    var investmentStatusType: Int? = 0
    var shareCount: Int = 1
    var totalAmount: Double? = 0.0
    var approvedInitiationToken: String? = null
    val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
    var requestToken: String? = null

    fun notifyMeViaMail(isProgressDialog: Boolean, recordName: String, emailId: String) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var notifyRequest = NotifyRequest()
        notifyRequest.recordName = recordName
        notifyRequest.email = emailId
        notifyRequest.firstName = ""
        notifyRequest.lastName = ""
        notifyRequest.fullName = ""

        val call = helper.notifyRecordNew(notifyRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                any?.responseStatus?.let {
                    when (it) {
                        1 -> {
                            fg.binding.ivNotify.setBackgroundResource(0)
                            fg.binding.ivNotify.setBackgroundResource(R.drawable.noti_icon)
                            notifyMe(true)
                        }
                        else -> {
                            CustomToast.showToast(
                                fg.requireContext(),
                                any.responseMessage
                                    ?: fg.getString(R.string.message_something_wrong),
                                CustomToast.ToastType.FAILED
                            )
                        }
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }
        })
    }

    fun notifyMe(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var notifyRequest = NotifyRequestUpdate()
        notifyRequest.recordId = fg.recordId!!

        val call = helper.notifyRecord(notifyRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {

                any?.responseStatus?.let {
                    when (it) {
                        1 -> {

                        }
                        else -> {
                            CustomToast.showToast(
                                fg.requireContext(),
                                any.responseMessage
                                    ?: fg.getString(R.string.message_something_wrong),
                                CustomToast.ToastType.FAILED
                            )
                        }
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })

    }


    fun recordFavorite(isProgressDialog: Boolean, recordId: String, isFav: Boolean, fileId: Int) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var notifyRequest = RecordFavoriteRequest()
        notifyRequest.recordId = recordId
        notifyRequest.fileID = fileId
        notifyRequest.isSelected = isFav

        val call = helper.recordFavorite(notifyRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {

                any?.responseStatus?.let {
                    when (it) {
                        1 -> {
                            if (isFav) {
                                fg.checkRecordFavorite = false
                                fg.binding.ivLike.setBackgroundResource(R.drawable.ic_baseline_favorite_24)


                            } else {
                                fg.checkRecordFavorite = true
                                fg.binding.ivLike.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24)

                            }

                        }
                        else -> {
                            CustomToast.showToast(
                                fg.requireContext(),
                                any.responseMessage
                                    ?: fg.getString(R.string.message_something_wrong),
                                CustomToast.ToastType.FAILED
                            )
                        }
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })

    }


    fun likeSong(
        isProgressDialog: Boolean,
        recordId: String,
        isFav: Boolean,
        fileId: Int,
        position: Int
    ) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var notifyRequest = RecordFavoriteRequest()
        notifyRequest.recordId = recordId
        notifyRequest.fileID = fileId
        notifyRequest.isSelected = isFav

        val call = helper.recordFavorite(notifyRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {

                any?.responseStatus?.let {
                    when (it) {
                        1 -> {
                            fg.albumAdapter!!.updateList(position,isFav)
                        }
                        else -> {
                            CustomToast.showToast(
                                fg.requireContext(),
                                any.responseMessage
                                    ?: fg.getString(R.string.message_something_wrong),
                                CustomToast.ToastType.FAILED
                            )
                        }
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })

    }

    fun getRecordDetailsNew(albumId: String?) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(true)
            return
        }
        fg.showProcessDialog()
        val helper: ApiAuthHelper?
        if (CommonUtils.isLogin(fg.requireContext())) {
            helper =
                ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        } else {
            helper = ApiClient.getClientInvestor().create(ApiAuthHelper::class.java)
        }
        val observable = helper.getRecordDetails(albumId ?: "", fg.isRecordTimerFinish)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<AlbumDetailResponse>() {
            override fun onSuccess(any: AlbumDetailResponse?, message: String?) {
                if (any?.response != null) {
                    if (any.responseStatus == 1) {
                        val res = any.response
                        if (res != null) {
                            detailsResponse = res
                            fg.updateUI(res)
                            fg.hideProcessDialog()
                            fg.paymentInfoViewModel.setRecordDetailResponse(res)
                            val recordSatusTypeID = res.recordStatusTypeId ?: 0
                            if (recordSatusTypeID == AppConstant.initialoffering || recordSatusTypeID == AppConstant.released) {

                                if (CommonUtils.isLogin(fg.requireActivity())) {

                                    requestGetInvestorStatus(
                                        detailsResponse.id,
                                        detailsResponse.approveEachInvestor == 1,
                                        true
                                    )
                                    isRecomened = true
                                } else {
                                    fg.binding.btnRecordStatus.visibility = VISIBLE
                                    fg.binding.btnRecordStatus.text =
                                        detailsResponse.recordStatusType
                                    fg.binding.btnRecordStatus.setPadding(
                                        dpToPx(50),
                                        dpToPx(7),
                                        dpToPx(50),
                                        dpToPx(7)
                                    )
                                }

                            } else {
                            }


                        } else {
                            CustomToast.showToast(
                                fg.requireContext(),
                                any.responseMessage
                                    ?: fg.getString(R.string.message_something_wrong),
                                CustomToast.ToastType.FAILED
                            )
                        }
                    } else {
                        CustomToast.showToast(
                            fg.requireContext(),
                            any.responseMessage
                                ?: fg.getString(R.string.message_something_wrong),
                            CustomToast.ToastType.FAILED
                        )
                    }
                } else {

                    CustomToast.showToast(
                        fg.requireContext(),
                        fg.getString(R.string.message_something_wrong),
                        CustomToast.ToastType.FAILED
                    )
                }
            }

            override fun onError(error: RetroError?) {
                CustomToast.showToast(
                    fg.requireContext(),
                    error?.errorMessage ?: "",
                    CustomToast.ToastType.FAILED
                )
            }
        }))
    }

    fun requestGetInvestorStatus(
        recordId: String?,
        isApproveInvestor: Boolean,
        isProgressDialog: Boolean
    ) {
        isInvestmentStatusUpdated = false

        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        val helper =
            ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.requestGetInvestorStatus(recordId ?: "0", isApproveInvestor)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<GetInvestorStatusResponse>() {
            override fun onSuccess(any: GetInvestorStatusResponse?, message: String?) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    investmentStatusType = any.response?.mystatustypeonrecord
                    fg.shareAllowedToPurchase = any.response?.SharesAllowedToMe!!
                    val recommendationMessage = any.response?.recommendationMessage ?: ""
                    if (investmentStatusType == AppConstant.SelfOwned && detailsResponse.recordStatusTypeId == AppConstant.approved_by_admin) {
                        //--> Hide (02-Sep-2021)
                        // if Record is self owned and record is approved by admin
                        fg.binding.btnRecordStatus.visibility = VISIBLE

                    }
                    if (detailsResponse.recordStatusTypeId == AppConstant.approved_by_admin) {
                        return
                    }
                    fg.updateButton(
                        any.response?.mystatustypeonrecord ?: -1,
                        any.response?.mystatusonrecord ?: "",
                        recordId,
                        any.response?.totalPendingInvestmentRequests, detailsResponse
                    )

                } else {
                    fg.showToast(
                        fg.context,
                        fg.getString(R.string.message_something_wrong),
                        CustomToast.ToastType.FAILED
                    )

                }
                //  dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {

                if (error!!.errorMessage.equals("HTTP 401 Unauthorized")) {
                    CommonUtils.logOut(fg.requireActivity())
                } else {
                    dismiss(true)

                }
            }
        }))
    }

    fun getCollectorsListByRecordId(isProgressDialog: Boolean, recordId: String) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideProcessDialog()
            return
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var viewCollectorsRequest = MyViewCollectorsRequest()
        viewCollectorsRequest.currentPage = 1
        viewCollectorsRequest.recordsPerPage = 5
        viewCollectorsRequest.recordId = recordId

        val call = helper.getViewCollectorsList(viewCollectorsRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<MyCollecrtorsResponse>() {
            override fun onSuccess(any: MyCollecrtorsResponse?, message: String) {

                if (any?.responseCollection != null &&
                    any?.responseStatus == 1
                ) {

                    if (!any.responseCollection.isNullOrEmpty()) {
                        fg.binding.clCollectedBy.visibility = View.VISIBLE
                        fg.adapter.updateList(any.responseCollection!!)
                    } else {
                        fg.binding.clCollectedBy.visibility = View.GONE
                        // No Data Found
                    }
                } else {

                }
                // dismiss(true)

            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })

    }

    fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        /* if (fg.binding.swp_main.isRefreshing) {
             fg.binding.swp_main.isRefreshing = false
         }*/
    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = fg.requireContext().resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }


    fun getCostBreakDown(albumId: String?, shares: Int, isProgress: Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            // dismiss(true)
            return
        }

        if (isProgress) {
            fg.showProcessDialog()

        }


        val helper: ApiAuthHelper?
        if (CommonUtils.isLogin(fg.requireContext())) {
            helper =
                ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        } else {
            helper = ApiClient.getClientMusic().create(ApiAuthHelper::class.java)
        }

        val observable = helper.getCostBreakDown(albumId ?: "", shares)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<CostBreakdownResponse>() {
            override fun onSuccess(any: CostBreakdownResponse?, message: String?) {
                fg.hideKeyboard()
                fg.hideProcessDialog()
                if (any?.response != null) {
                    if (any.responseStatus == 1) {
                        val res = any.response
                        if (res != null) {
                            fg.costBreakDownResponse = any
                            fg.numberOfToken = shares
                            fg.paymentInfoViewModel.setCostBreakDownResponse(any)
                            fg.paymentInfoViewModel.setNumberOfTokenToInvest(
                                shares,
                                fg.detailResponse.recordImage!!
                            )

                        }
                    } else {
                        CustomToast.showToast(
                            fg.requireContext(),
                            any.responseMessage
                                ?: fg.getString(R.string.message_something_wrong),
                            CustomToast.ToastType.FAILED
                        )
                    }
                } else {
                    CustomToast.showToast(
                        fg.requireContext(),
                        fg.getString(R.string.message_something_wrong),
                        CustomToast.ToastType.FAILED
                    )
                }

            }

            override fun onError(error: RetroError?) {
                fg.hideProcessDialog()
                CustomToast.showToast(
                    fg.requireContext(),
                    error?.errorMessage ?: "",
                    CustomToast.ToastType.FAILED
                )
            }
        }))
    }


/*
    fun investNowPaymentBegin(
        mrecordId: String,
        mpaymentID: Int,
        isProgressDialog: Boolean,
        referenceId: String, tokenToPurchase: Int
    ) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }

        val temp =
//        if (fg.data?.approveEachInvestor ?: 0 == 1) {
//            sharesRequested
//        } else {
            tokenToPurchase
//        }

        val totalAmount = fg.paymentInfoViewModel.totalAmount?.value
        if ((temp ?: 0 <= 0) ||
            (totalAmount ?: 0.0 <= 0.0)
        ) {
            fg.showToast(
                fg.requireContext(),
                "Amount and shares are not available",
                CustomToast.ToastType.FAILED
            )
            return
        }

        val request = InitialPaymentInfoRequest().apply {
            recordId = mrecordId
            requestedShares = tokenToPurchase
            totalRequestedAmount = totalAmount
            paymentMethodId = mpaymentID
            referralCode = referenceId
            showToPublic = 0
            approvedPaymentInitiationToken = approvedInitiationToken
        }

        val observable = helper.getInitialPaymentInfo(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<InvestmentBegins>() {
            override fun onSuccess(any: InvestmentBegins?, message: String?) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    requestToken = any.requestToken
                    if (mpaymentID == 1) {
                        fg.clientSecret = any.clientSecret
                        fg.initatePayment()
                    } else if (mpaymentID == 2) {
                        fg.processPayment(
                            totalAmount ?: 0.0,
                            "Record Investment",
                            any.clientSecret ?: ""
                        )
                    } else if (mpaymentID == 3) {
                        // payFromWallet(isProgressDialog)
                    }

                } else {
                    any?.responseMessage?.let {
                        fg.showToast(fg.context, it, CustomToast.ToastType.FAILED)
                    }
                }
            }

            override fun onError(error: RetroError?) {
                fg.showToast(
                    fg.context,
                    fg.getString(R.string.message_something_wrong),
                    CustomToast.ToastType.FAILED
                )
            }

        }))
    }
*/


    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RequestCode.STRIP_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        val successJson: String? = data.getStringExtra("success")
                        val successPaymentIntent: PaymentIntent? =
                            Gson().fromJson(successJson, PaymentIntent::class.java)
                        stripSuccessPayment(successJson ?: "", successPaymentIntent)
                        fg.showToast(
                            fg.requireContext(),
                            fg.getString(R.string.meessage_payment_success),
                            CustomToast.ToastType.SUCCESS,
                            Toast.LENGTH_LONG
                        )
                    } else {
                        stripErrorPayment(isUserCanceled = false)
                        fg.showToast(
                            fg.requireContext(),
                            fg.getString(R.string.message_payment_failed),
                            CustomToast.ToastType.FAILED,
                            Toast.LENGTH_LONG
                        )
                    }
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    if (data != null) {

                        val isUserCanceled = data.getBooleanExtra("isUserCanceled", false)
                        val errorJson: String? = data.getStringExtra("error")
                        val errorPaymentIntent: PaymentIntent? =
                            Gson().fromJson(errorJson, PaymentIntent::class.java)
                        val errorMessage: String? = if (isUserCanceled) {
                            fg.getString(R.string.message_user_canceled)
                        } else {
                            data.getStringExtra("errorMessage")
                        }
                        fg.showToast(
                            fg.requireContext(),
                            errorMessage ?: fg.getString(R.string.message_payment_failed),
                            CustomToast.ToastType.FAILED,
                            Toast.LENGTH_LONG
                        )
                        stripErrorPayment(
                            errorJson ?: "",
                            errorPaymentIntent,
                            isUserCanceled,
                            errorMessage
                        )

                    } else {
                        stripErrorPayment(isUserCanceled = false)
                        fg.showToast(
                            fg.requireContext(),
                            fg.getString(R.string.message_payment_failed),
                            CustomToast.ToastType.FAILED,
                            Toast.LENGTH_LONG
                        )
                    }
                } else {
                    stripErrorPayment(isUserCanceled = false)
                    fg.showToast(
                        fg.requireContext(),
                        fg.getString(R.string.message_payment_failed),
                        CustomToast.ToastType.FAILED,
                        Toast.LENGTH_LONG
                    )
                }
            }
            /*      //Pay Pal response
                  RequestCode.PAYPAL_REQUEST_CODE -> {
                      if (resultCode == Activity.RESULT_OK) {
      //                    val confirmation: PaymentConfirmation? =
      //                        data?.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)
      //                    if (confirmation != null) {
      //
      //                        val paymentDetails =
      //                            confirmation.toJSONObject().toString()
      //                        println("PAYPAL_REQUEST_CODE: $paymentDetails")
      //
      //                        payPalSuccessPayment(paymentDetails, confirmation)
      //                        fg.showToast(
      //                            fg.requireContext(),
      //                            fg.getString(R.string.meessage_payment_success),
      //                            CustomToast.ToastType.SUCCESS,
      //                            Toast.LENGTH_LONG
      //                        )
      //                    } else {
      //                        payPalErrorPayment(isUserCanceled = false)
      //                        fg.showToast(
      //                            fg.requireContext(),
      //                            fg.getString(R.string.message_payment_failed),
      //                            CustomToast.ToastType.FAILED,
      //                            Toast.LENGTH_LONG
      //                        )
      //                    }
                      } else if (resultCode == Activity.RESULT_CANCELED) {
                          payPalErrorPayment(
                              isUserCanceled = true,
                              errorMessage = fg.getString(R.string.message_user_canceled)
                          )
                          fg.showToast(
                              fg.requireContext(),
                              fg.getString(R.string.message_user_canceled),
                              CustomToast.ToastType.FAILED,
                              Toast.LENGTH_LONG
                          )
                      }
                  }*/

        }
    }

    private fun stripErrorPayment(
        errorJson: String = "",
        errorIntent: PaymentIntent? = null,
        isUserCanceled: Boolean = false,
        errorMessage: String? = null
    ) {
        val request = InvestorPaymentRequest()
        /*Old Params*/
        request.requestToken = requestToken ?: ""
        request.transactionId = ""
        request.cardtypeid = 1
        request.paymentResponse = errorJson

        /*New Our Params*/
        request.paymentMethodId = 1
        if (isUserCanceled) {
            request.errorId = 1
        } else {
            request.errorId = 2
        }
        request.errorMessage = errorMessage ?: fg.getString(R.string.message_payment_failed)

        if (errorIntent != null) {
            request.transactionId = errorIntent.id ?: ""
        }

        investNowPaymentFailed(request, true)

    }

    private fun stripSuccessPayment(successJson: String = "", successIntent: PaymentIntent?) {
        if (successIntent != null) {
            val request = InvestorPaymentRequest()
            /*Old Params*/
            request.requestToken = requestToken ?: ""
            request.transactionId = successIntent.id ?: ""
            request.cardtypeid = 1

            /*New Our Params*/
            request.paymentMethodId = 1
            request.errorId = 0
            request.errorMessage = ""
            request.paymentResponse = successJson

            investNowPaymentSuccess(request, true)
        } else {
            stripErrorPayment(isUserCanceled = false)
            fg.showToast(
                fg.requireContext(),
                fg.getString(R.string.message_payment_failed),
                CustomToast.ToastType.FAILED,
                Toast.LENGTH_LONG
            )
        }
    }


    fun payPalErrorPayment(isUserCanceled: Boolean = false, errorMessage: String? = null) {
        val request = InvestorPaymentRequest()
        /*Old Params*/
        request.requestToken = requestToken ?: ""
        request.transactionId = ""
        request.cardtypeid = 1
        request.paymentResponse = ""

        /*New Our Params*/
        request.paymentMethodId = 2
        if (isUserCanceled) {
            request.errorId = 1
        } else {
            request.errorId = 2
        }
        request.errorMessage = errorMessage ?: fg.getString(R.string.message_payment_failed)

        investNowPaymentFailed(request, true)

    }

    //--> Investor Success Payment
    fun investNowPaymentSuccess(
        request: InvestorPaymentRequest,
        isProgressDialog: Boolean
    ) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }

        val observer = helper.sendPaymentSuccessInfo(request)
        disposables.add(sendApiRequest(observer)!!.subscribeWith(object :
            CallbackWrapper<InvestmentSuccessResponse>() {
            override fun onSuccess(any: InvestmentSuccessResponse?, message: String?) {
                PrefUtils.saveValueInPreference(
                    fg.requireContext(),
                    PrefUtils.PLAYLIST_REFRESH,
                    true
                )
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    fg.navigate.manualBack(1)

                } else {
                    any?.responseMessage?.let {
                        fg.showToast(
                            fg.context,
                            it,
                            CustomToast.ToastType.FAILED
                        )
                    }

                }
            }

            override fun onError(error: RetroError?) {
                fg.showToast(
                    fg.context,
                    fg.getString(R.string.message_something_wrong),
                    CustomToast.ToastType.FAILED
                )
            }

        }))
    }

    //--> Investor Failed Payment
    fun investNowPaymentFailed(
        request: InvestorPaymentRequest,
        isProgressDialog: Boolean
    ) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val observer = helper.sendPaymentFailedInfo(request)
        disposables.add(sendApiRequest(observer)!!.subscribeWith(object :
            CallbackWrapper<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String?) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    val temp =
//                        if (fg.data?.approveEachInvestor ?: 0 == 1) {
//                        sharesRequested
//                    } else {
                        shareCount
//                    }
                    //  fg.navToPaymentSuccess(false, totalAmount ?: 0.0, temp ?: 0)
                    Toast.makeText(fg.requireActivity(), "Payment Cancelled", Toast.LENGTH_SHORT)
                        .show()

                    fg.navigate.manualBack(1)

                } else {
                    any?.responseMessage?.let {
                        fg.showToast(
                            fg.context,
                            it,
                            CustomToast.ToastType.FAILED
                        )
                    }

                }
                // dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
                fg.showToast(
                    fg.context,
                    fg.getString(R.string.message_something_wrong),
                    CustomToast.ToastType.FAILED
                )
            }
        }))
    }

    // Paypal success payment without Braintree

    fun payPalSuccessPaymentWithOrderId(orderId: String) {
        val request = InvestorPaymentRequest()
        /*Old Params*/
        request.requestToken = requestToken ?: ""
        request.cardtypeid = 1
        request.paymentResponse = ""

        /*New Our Params*/
        request.paymentMethodId = 2
        request.referenceId = orderId
        request.errorId = 0
        request.errorMessage = ""

        investNowPaymentSuccess(request, true)
    }
}

