package com.nibula.request_to_invest
import android.app.Activity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.FrameLayout
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.FragmentFinalCheckoutBinding
import com.nibula.request_to_invest.viewmodal.PaymentInfoViewModel
import com.nibula.utils.CommonUtils
import com.nibula.utils.interfaces.DialogFragmentClicks

class CheckOutBottomSheetFragment() : BaseBottomSheetFragment() {

    constructor(
        recordId: String, dgFrgListener: DialogFragmentClicks, onClickOfCheckOut: OnClickOfCheckOut
    ) : this() {
        this.dgFrgListener = dgFrgListener
        this.recordId = recordId
        this.onClickOfCheckOut = onClickOfCheckOut
    }

    lateinit var dgFrgListener: DialogFragmentClicks
    lateinit var paymentInfoViewModel: PaymentInfoViewModel
    private lateinit var recordId: String

    private lateinit var rootView: View
    var paymentMethod: Int = 0
    lateinit var onClickOfCheckOut: OnClickOfCheckOut
    lateinit var binding:FragmentFinalCheckoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme1)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            /*rootView =
                inflater.inflate(R.layout.fragment_final_checkout, container, false)*/
            binding= FragmentFinalCheckoutBinding.inflate(inflater,container,false)
            rootView=binding.root
            paymentInfoViewModel = activity?.run {
                ViewModelProvider(this).get(PaymentInfoViewModel::class.java)
            } ?: throw Exception("Invalid Activity")

            paymentInfoViewModel.paymentMethod?.value.let {
                paymentMethod = it!!
            }
            when (paymentMethod) {
                1 -> {
                    binding.clCreditCard.visibility = View.VISIBLE
                }
                else -> {
                    binding.clPaypal.visibility = View.VISIBLE

                }
            }

            paymentInfoViewModel.recordDetailResponse?.value.let {

                it?.recordTitle.let {
                    binding.tvRecordTitle.text = it
                }
                it?.artistName.let {
                    binding.tvArtistName.text = it
                }
                it?.recordImage.let {
                    Glide
                        .with(requireActivity())
                        .load(it)
                        .placeholder(R.drawable.nebula_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.IMMEDIATE)
                        .into(binding.clImage2)
                }

                it?.artistImage.let {
                    Glide
                        .with(requireActivity())
                        .load(it)
                        .placeholder(R.drawable.nebula_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.IMMEDIATE)
                        .into(binding.ivInvestor)
                }

                it?.recordTitle.let {
                    binding.investedByUserName.text = it
                }

                it?.artistName.let {
                    binding.investedBy.text = it
                }

                binding.tvValueTotal.text =
                    "${it?.sharesAvailable?.toInt()}/${it?.totalNoOfShares?.toInt()}"
                binding.tvValueTotalTokens.text =
                    "${it?.sharesAvailable?.toInt()}/${it?.totalNoOfShares?.toInt()}"
                binding.tvValuePricePerToken.text =
                    "${it?.royalityOwnershipPercentagePerToken.toString() + "%"}"


                binding.tvValue.text = "${it?.currencyType ?: "USD"}${
                    CommonUtils.trimDecimalToTwoPlaces(
                        it?.valuePerShareInDollars ?: 0.00
                    )
                }"
            }

            binding.btnCheckOut.setOnClickListener {
                if (paymentMethod == 1) {
                    binding.progressBarLarge.visibility = View.VISIBLE

                }

                onClickOfCheckOut.onClickOfFinalCheckOut()
            }
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById< View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                val layoutParams = bottomSheet.layoutParams
                val windowHeight = getWindowHeight()
                if (layoutParams != null) {
                    layoutParams.height = windowHeight
                }
                bottomSheet.layoutParams = layoutParams
                behavior.state = BottomSheetBehavior.STATE_EXPANDED

              /*  dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
                    if (i == KeyEvent.KEYCODE_BACK) {
                        Toast.makeText(
                            requireActivity(),
                            "Please wait...",
                            Toast.LENGTH_SHORT
                        ).show()
                        return@setOnKeyListener true
                    } else {
                        return@setOnKeyListener false
                    }

                }*/
            }
        })
    }

    private fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        (context as Activity?)!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

}