package com.nibula.request_to_invest

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.nibula.R
import com.nibula.databinding.RecommenededBuyLayoutBinding
import com.nibula.request_to_invest.modal.recomded_by.RecommendedBy
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils

class RecommendedBuyAdapter(
    val context: Context,
    val list: ArrayList<RecommendedBy>,
    val listener: CallbackRecommendations
) :
    RecyclerView.Adapter<RecommendedBuyAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    var isInvestedOrPurchased = true
    var investmentStatusId: Int = -1
    var isAutoApproveEachInvestor: Boolean = false
    var recordStatusId = 2
    var amount = "0"
    var sharesAvailable: Double = 0.0
    lateinit var binding:RecommenededBuyLayoutBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.recommeneded_buy_layout, parent, false)

        val inflater = LayoutInflater.from(parent.context)
         binding = RecommenededBuyLayoutBinding.inflate(inflater, parent, false)

        val holder = ViewHolder(binding.root)
        return holder
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]
        binding.buyerNameTV.text = list[position].recommendedByUserName
        if (!list[position].recommendedByUserImage.isNullOrEmpty()) {
            val options: RequestOptions = RequestOptions()
                .placeholder(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_record_user_place_holder
                    )
                )
                .error(ContextCompat.getDrawable(context, R.drawable.ic_record_user_place_holder))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
            list[position].recommendedByUserImage?.let {
                Glide.with(context).load(list[position].recommendedByUserImage)
                    .thumbnail(CommonUtils.THUMBNAIL).apply(options).into(binding.buyerImg)

            }

        }

        binding.btnBuyNow.setOnClickListener {
            if (sharesAvailable <= 0.0) {
                return@setOnClickListener
            }

            if (recordStatusId == 6) {
                if (isAutoApproveEachInvestor) {
                    listener.perRequestToInvest(data.referralCode)
                } else {
                    listener.performInvestNow(data.referralCode)
                }
            } else if (recordStatusId == 3) {
                listener.performBuyNow(data.referralCode)
            }
        }
        if (recordStatusId == 3) {
            binding.btnBuyNow.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )
            binding.btnBuyNow.background =
                ContextCompat.getDrawable(context, R.drawable.green_storck)
            if (isInvestedOrPurchased) {
                binding.btnBuyNow.text = context.getString(R.string.purchased)
            } else {
                if (sharesAvailable != null && sharesAvailable!! <= 0.0) {
                    binding.btnBuyNow.text =
                        "${context.getString(R.string.soldOut)} $ $amount"
                } else {
                    binding.btnBuyNow.text =
                        "${context.getString(R.string.buy)} $ $amount"
                }
            }
        } else {
            if (!isInvestedOrPurchased) {
                if (!isAutoApproveEachInvestor) {
                    if (sharesAvailable != null && sharesAvailable!! > 0.0) {
                        binding.btnBuyNow.background =
                            ContextCompat.getDrawable(context, R.drawable.yellow_border)
                        binding.btnBuyNow.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.white
                            )
                        )
                        binding.btnBuyNow.text = context.getString(R.string.invest_now_new)
                        binding.btnBuyNow.isEnabled = true
                    } else {
                        binding.btnBuyNow.background =
                            ContextCompat.getDrawable(context, R.drawable.yellow_border)
                        binding.btnBuyNow.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.neptune_blue
                            )
                        )
                        binding.btnBuyNow.text = context.getString(R.string.soldOut)
                        binding.btnBuyNow.isEnabled = false
                    }
                } else {
                    if (sharesAvailable != null && sharesAvailable!! > 0.0) {
                        if (investmentStatusId == AppConstant.RequestApproved) {
                            if (data.isUserApproved) {
                                binding.btnBuyNow.background =
                                    ContextCompat.getDrawable(context, R.drawable.yellow_border)
                                binding.btnBuyNow.setTextColor(
                                    ContextCompat.getColor(
                                        context,
                                        R.color.draft_yellow
                                    )
                                )
                                binding.btnBuyNow.text =
                                    context.getString(R.string.invest_now)
                                binding.btnBuyNow.isEnabled = true
                            } else {
                                binding.btnBuyNow.background =
                                    ContextCompat.getDrawable(context, R.drawable.gray_borders)
                                binding.btnBuyNow.setTextColor(
                                    ContextCompat.getColor(
                                        context,
                                        R.color.gray
                                    )
                                )
                                binding.btnBuyNow.text =
                                    context.getString(R.string.request_to_invest)
                                binding.btnBuyNow.isEnabled = false
                            }
                        } else if (investmentStatusId == AppConstant.RequestPending) {
                            binding.btnBuyNow.background =
                                ContextCompat.getDrawable(
                                    context,
                                    R.drawable.gray_borders
                                )
                            binding.btnBuyNow.setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.gray
                                )
                            )
                            binding.btnBuyNow.text =
                                context.getString(R.string.pending_review)
                            binding.btnBuyNow.isEnabled = true
                        } else {
                            binding.btnBuyNow.background =
                                ContextCompat.getDrawable(
                                    context,
                                    R.drawable.request_to_invest_border
                                )
                            binding.btnBuyNow.setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.colorAccent
                                )
                            )
                            binding.btnBuyNow.text =
                                context.getString(R.string.request_to_invest)
                            binding.btnBuyNow.isEnabled = true
                        }
                    } else {
                        binding.btnBuyNow.background =
                            ContextCompat.getDrawable(context, R.drawable.yellow_border)
                        binding.btnBuyNow.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.draft_yellow
                            )
                        )
                        binding.btnBuyNow.text =
                            context.getString(R.string.soldOut)
                        binding.btnBuyNow.isEnabled = false
                    }
                }
            } else {
                binding.btnBuyNow.text = context.getString(R.string.invested)
                binding.btnBuyNow.background = null
                binding.btnBuyNow.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.gray
                    )
                )
            }
        }
    }

    interface CallbackRecommendations {
        fun performBuyNow(referralCode: String)
        fun perRequestToInvest(referralCode: String)
        fun performInvestNow(referralCode: String)
    }
}