package com.nibula.request_to_invest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.request_to_invest.interfaces.AlbumOperation
import com.nibula.response.details.File
import com.nibula.response.details.Response

class AlbumAdapter(
    val context: Context,
    var response: Response?,
    var files: MutableList<File>?,
    var albumOperation: AlbumOperation
) : RecyclerView.Adapter<AlbumAdapter.ViewHolder>() {
    var previousPosition: Int = -1

    var artistNameString: String = ""


    /*class ViewHolder(itemView: ItemAlbumBinding) : RecyclerView.ViewHolder(itemView.root)*/

    class ViewHolder(itemVew: View) : RecyclerView.ViewHolder(itemVew) {
        val songName: TextView = itemVew.findViewById(R.id.tvsongName)
        val artistName: TextView = itemVew.findViewById(R.id.tvArtistName)
        val songCount: TextView = itemVew.findViewById(R.id.tvCount)
        val ivPlay: ImageView = itemVew.findViewById(R.id.ivPlay)
        val ivLike: ImageView = itemVew.findViewById(R.id.ivLike)

    }

    /*override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        binding = ItemAlbumBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }
*/
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_album, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return files!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(
        holder: ViewHolder, position: Int
    ) {
        artistNameString = ""
        var filePositionData = files?.get(holder.absoluteAdapterPosition)
        filePositionData?.songTitle.let {

            holder.songName.text = it
        }
        for (i in filePositionData?.artistsList!!) {
            artistNameString += i.artistName + ","
        }
        filePositionData?.let {
            holder.artistName.text = artistNameString
        }
        holder.songCount.text = position.plus(1).toString()
        holder.ivPlay.setOnClickListener {

             if (previousPosition == -1) {
                 previousPosition = position
                 files!![position]?.isPlaying = true
                 albumOperation.onSongPlay(position, response?.id.toString(), files)
             } else if (previousPosition == position && files!![position].isPlaying) {
                 files!![position]?.isPlaying = false
                 previousPosition = -1
                 albumOperation.onPauseSong()
             } else {
                 files!![position]?.isPlaying = true
                 files!![previousPosition]?.isPlaying = false
                 previousPosition = position
                 albumOperation.onSongPlay(position, response?.id.toString(), files)
             }
            notifyDataSetChanged()
        }

        filePositionData.isPlaying.let {
            when (it) {
                true -> {
                    holder.ivPlay.setBackgroundResource(R.drawable.pause)
                }
                else -> {
                    holder.ivPlay.setBackgroundResource(R.drawable.play)
                }
            }
        }
        filePositionData.IsFavourite.let {
            when (it) {
                true -> {
                    holder.ivLike.setBackgroundResource(R.drawable.ic_baseline_favorite_24)
                }
                else -> {
                    holder.ivLike.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24)
                }
            }
        }
        holder.ivLike.setOnClickListener {
            albumOperation.onSongLike(
                holder.absoluteAdapterPosition, filePositionData.id!!, filePositionData.IsFavourite
            )
        }
    }

    fun updateList(index: Int, isLike: Boolean) {
        files!![index]?.IsFavourite = isLike
       notifyDataSetChanged()
    }


    fun refreshAdapter() {
        if (previousPosition == -1) return
        files!![previousPosition].isPlaying = false
        notifyDataSetChanged()
    }
    fun playPause(isPlaying: Boolean) {
        if (previousPosition == -1) return
        if (isPlaying) {
            files!![previousPosition]?.isPlaying   = true

        } else {
            files!![previousPosition]?.isPlaying   = true

            files!![previousPosition]?.isPlaying   = false


        }
        notifyDataSetChanged()
    }
}