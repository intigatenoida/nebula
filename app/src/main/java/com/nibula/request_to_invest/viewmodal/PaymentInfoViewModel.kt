package com.nibula.request_to_invest.viewmodal

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.response.details.Response
import com.stripe.android.model.PaymentMethodCreateParams

class PaymentInfoViewModel : ViewModel() {
    var costBreakDownResponse = MutableLiveData<CostBreakdownResponse>()
    var recordDetailResponse = MutableLiveData<Response>()
    var numberOfToken = MutableLiveData<Int>()
    var rImage = MutableLiveData<String>()
    var paymentMethod = MutableLiveData<Int>()
    var totalAmount = MutableLiveData<Double>()

    fun setCostBreakDownResponse(response: CostBreakdownResponse) {
        costBreakDownResponse.value = response
    }

    fun setNumberOfTokenToInvest(numberOfTokenToInvest: Int, recordImage: String) {
        rImage.value = recordImage
        numberOfToken.value = numberOfTokenToInvest
    }

    fun setPaymentMethod(paymentMethodResult: Int) {
        paymentMethod.value = paymentMethodResult
    }

    fun setTotalAmount(amount:Double) {
        totalAmount.value=amount
    }

    fun setRecordDetailResponse(response: Response) {
        recordDetailResponse.value = response
    }
}