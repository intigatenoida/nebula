package com.nibula.request_to_invest.paypal

import android.annotation.TargetApi
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.webkit.*
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.request_to_buy.navigate
import com.nibula.utils.AppConstant
import com.nibula.utils.PrefUtils
import com.nibula.utils.RequestCode

class PaypalPaymentActivity :BaseActivity() {
     var url = AppConstant.TERMS_CONDITION
     lateinit var webView:WebView
     lateinit var recordId:String
     lateinit var currencyType:String
     var amountInvestetment:Double=0.0
     var shareCount:Int=0
     var paymentUrl="https://dev.nebu.la/payment"

     var isPageFinished:Boolean=false
     lateinit var navigate: navigate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.paypal_payment_activity)
        navigate = this@PaypalPaymentActivity as navigate
        webView=findViewById(R.id.webview)
        recordId=intent?.getStringExtra("recordId")!!
        currencyType=intent?.getStringExtra("currency_type")!!
        amountInvestetment=intent?.getDoubleExtra("amount",0.0)!!
        shareCount=intent?.getIntExtra("investment_token",0)!!
        paymentUrl=paymentUrl
        val token = PrefUtils.getValueFromPreference(this@PaypalPaymentActivity, PrefUtils.TOKEN)
            .replace("Bearer", "").trim()
        // This is only for one record
        paymentUrl = "https://nebu.la/payment?Amt=$amountInvestetment&tc=$shareCount&currency=${"USD"}&recordId=$recordId&token=$token"
        Log.d("","Url is"+paymentUrl)
        setWebView()

        findViewById<TextView>(R.id.tvOk).setOnClickListener {
         /* val sendBroadCast = Intent()
            val localBroadcastManager = LocalBroadcastManager.getInstance(this@PaypalPaymentActivity)
            sendBroadCast.action = AppConstant.PAYMENT_SUCCCESS_BROADCAST_ACTION
            localBroadcastManager.sendBroadcast(sendBroadCast)*/
            setResult(RequestCode.PAYPAL_WEB_VIEW_RESULT_CODE)
            finish()
        }
    }

    private fun setWebView() {
        showProcessDialog()
        webView.webViewClient = ServiceWebViewClient()
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(paymentUrl)
    }

    private inner class ServiceWebViewClient : WebViewClient() {
        override fun onLoadResource(view: WebView, url: String) {
            super.onLoadResource(view, url)
            //--> Investor payment Success
              //--> Investor payment Failed
          //  @POST("api/invest/InvestmentPaymentFailed")

            if (view.progress > 30) {
                hideProcessDailog()
            }
           /* if(url.contains("InvestmentPaymentSuccessful")&&!isPageFinished)
            {
                isPageFinished=true
                Toast.makeText(this@PaypalPaymentActivity,"Payment Successful",Toast.LENGTH_SHORT).show()
                val sendBroadCast = Intent()
                val localBroadcastManager = LocalBroadcastManager.getInstance(this@PaypalPaymentActivity)
                sendBroadCast.action = AppConstant.PAYMENT_SUCCCESS_BROADCAST_ACTION
                localBroadcastManager.sendBroadcast(sendBroadCast)
                finish()
            }
            else if(url.contains("InvestmentPaymentFailed"))
            {
                Toast.makeText(this@PaypalPaymentActivity,"Payment Failed",Toast.LENGTH_SHORT).show()
                val sendBroadCast = Intent()
                val localBroadcastManager = LocalBroadcastManager.getInstance(this@PaypalPaymentActivity)
                sendBroadCast.action = AppConstant.PAYMENT_SUCCCESS_BROADCAST_ACTION
                localBroadcastManager.sendBroadcast(sendBroadCast)
                finish()
            }
            else
            {

            }*/
        }

        @Deprecated("Deprecated in Java")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            if (url!!.startsWith("http:") || url.startsWith("https:")) {
                return false
            }

            // Otherwise allow the OS to handle it
            // Otherwise allow the OS to handle it
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            view!!.context.startActivity(intent)
            return true
        }


        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            url?.let { Log.d("Loded Url is", it) }
        }
        @TargetApi(Build.VERSION_CODES.M)
        override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
            super.onReceivedError(view, request, error)
            Log.e("*********", "******************************${error.description.toString()}")
            hideProcessDailog()
        }

        override fun onReceivedHttpError(
            view: WebView,
            request: WebResourceRequest,
            errorResponse: WebResourceResponse) {
            super.onReceivedHttpError(view, request, errorResponse)
            Log.i("*********", errorResponse.toString())
            hideProcessDailog()
        }

        override fun onReceivedSslError(
            view: WebView?,
            handler: SslErrorHandler?,
            error: SslError?
        ) {
            var message = "SSL Certificate error."
            when (error?.primaryError ?: -1) {
                SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                SslError.SSL_EXPIRED -> message = "The certificate has expired."
                SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
            }
            message += " Do you want to continue anyway?"
            val builder: AlertDialog.Builder = AlertDialog.Builder(this@PaypalPaymentActivity)
            builder.setMessage(message)
            builder.setPositiveButton("continue",
                DialogInterface.OnClickListener { _, _ -> handler!!.proceed() })
            builder.setNegativeButton("cancel",
                DialogInterface.OnClickListener { _, _ -> handler!!.cancel() })
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }
}