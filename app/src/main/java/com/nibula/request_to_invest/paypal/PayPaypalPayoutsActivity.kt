package com.nibula.request_to_invest.paypal
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.webkit.*
import android.widget.TextView
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.dashboard.DashboardActivity
import com.nibula.request_to_buy.navigate
import com.nibula.utils.PrefUtils
import com.nibula.utils.RequestCode

class PayPaypalPayoutsActivity :BaseActivity() {
    lateinit var webView:WebView
    lateinit var amount:String
    var paymentUrl="https://stage.nebu.la/payout"
    lateinit var navigate: navigate
    companion object {
        private const val REQUEST_CODE = 123
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.paypal_payment_activity)
        navigate = this@PayPaypalPayoutsActivity as navigate
        webView=findViewById(R.id.webview)
        amount=intent?.getStringExtra("amount")!!
        paymentUrl=paymentUrl
        val token = PrefUtils.getValueFromPreference(this@PayPaypalPayoutsActivity, PrefUtils.TOKEN)
            .replace("Bearer", "").trim()
        // This is only for one record
        paymentUrl = "$paymentUrl?amt=$amount&token=$token"
        Log.d("","Url is"+paymentUrl)

       // setWebView()
        val customTabsIntentBuilder = CustomTabsIntent.Builder()

        // Customize toolbar colors (optional)
        customTabsIntentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
        customTabsIntentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))

        // Show the website title (optional)
        customTabsIntentBuilder.setShowTitle(false)

        // Create a PendingIntent for your custom action button
        val customActionButtonPendingIntent = createCustomActionButtonIntent()
        val bitmap: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.g_tick)

        customTabsIntentBuilder.setActionButton(bitmap,getString(R.string.done),customActionButtonPendingIntent,true)

        // Create a CustomTabsIntent and launch the URL
        val customTabsIntent = customTabsIntentBuilder.build()
        customTabsIntent.launchUrl(this, Uri.parse(paymentUrl))

        findViewById<TextView>(R.id.tvOk).setOnClickListener {
            /* val sendBroadCast = Intent()
               val localBroadcastManager = LocalBroadcastManager.getInstance(this@PaypalPaymentActivity)
               sendBroadCast.action = AppConstant.PAYMENT_SUCCCESS_BROADCAST_ACTION
               localBroadcastManager.sendBroadcast(sendBroadCast)*/
            setResult(RequestCode.PAYPAL_WEB_VIEW_RESULT_CODE)
            finish()
        }
    }

  /*  private fun setWebView() {
        showProcessDialog()
        webView.webViewClient = ServiceWebViewClient()
        webView.settings.javaScriptEnabled = true
        webView.settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW)
        webView.loadUrl(paymentUrl)

    }

    private inner class ServiceWebViewClient : WebViewClient() {
        override fun onLoadResource(view: WebView, url: String) {
            super.onLoadResource(view, url)
            //--> Investor payment Success
            //--> Investor payment Failed
            //  @POST("api/invest/InvestmentPaymentFailed")

            if (view.progress > 30) {
                hideProcessDailog()
            }
            *//* if(url.contains("InvestmentPaymentSuccessful")&&!isPageFinished)
             {
                 isPageFinished=true
                 Toast.makeText(this@PaypalPaymentActivity,"Payment Successful",Toast.LENGTH_SHORT).show()
                 val sendBroadCast = Intent()
                 val localBroadcastManager = LocalBroadcastManager.getInstance(this@PaypalPaymentActivity)
                 sendBroadCast.action = AppConstant.PAYMENT_SUCCCESS_BROADCAST_ACTION
                 localBroadcastManager.sendBroadcast(sendBroadCast)
                 finish()
             }
             else if(url.contains("InvestmentPaymentFailed"))
             {
                 Toast.makeText(this@PaypalPaymentActivity,"Payment Failed",Toast.LENGTH_SHORT).show()
                 val sendBroadCast = Intent()
                 val localBroadcastManager = LocalBroadcastManager.getInstance(this@PaypalPaymentActivity)
                 sendBroadCast.action = AppConstant.PAYMENT_SUCCCESS_BROADCAST_ACTION
                 localBroadcastManager.sendBroadcast(sendBroadCast)
                 finish()
             }
             else
             {

             }*//*
        }

        @Deprecated("Deprecated in Java")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            if (url!!.startsWith("http:") || url.startsWith("https:")) {
                return false
            }

            // Otherwise allow the OS to handle it
            // Otherwise allow the OS to handle it
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            view!!.context.startActivity(intent)
            return true
        }


        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            url?.let { Log.d("Loded Url is", it) }
        }
        @TargetApi(Build.VERSION_CODES.M)
        override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
            super.onReceivedError(view, request, error)
            Log.e("*********", "******************************${error.description.toString()}")
            hideProcessDailog()
        }

        override fun onReceivedHttpError(
            view: WebView,
            request: WebResourceRequest,
            errorResponse: WebResourceResponse) {
            super.onReceivedHttpError(view, request, errorResponse)
            Log.i("*********", errorResponse.toString())
            hideProcessDailog()
        }

        override fun onReceivedSslError(
            view: WebView?,
            handler: SslErrorHandler?,
            error: SslError?
        ) {
            var message = "SSL Certificate error."
            when (error?.primaryError ?: -1) {
                SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                SslError.SSL_EXPIRED -> message = "The certificate has expired."
                SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
            }
            message += " Do you want to continue anyway?"
            val builder: AlertDialog.Builder = AlertDialog.Builder(this@PayPaypalPayoutsActivity)
            builder.setMessage(message)
            builder.setPositiveButton("continue",
                DialogInterface.OnClickListener { _, _ -> handler!!.proceed() })
            builder.setNegativeButton("cancel",
                DialogInterface.OnClickListener { _, _ -> handler!!.cancel() })
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }*/

    private fun createCustomActionButtonIntent(): PendingIntent {
        // Replace this method with your custom action button logic
        // You can create a PendingIntent to perform specific actions when the button is clicked
        // For example, you can use PendingIntent.getActivity() to return to your app when clicked
        // or launch a specific action within your app.
        // Note: Custom button logic is outside the scope of this example.
        // You need to implement your specific custom action here.
        val targetIntent = Intent(this, DashboardActivity::class.java)
        // Use PendingIntent.FLAG_IMMUTABLE to make the PendingIntent immutable
        return PendingIntent.getActivity(
            this,
            REQUEST_CODE,
            targetIntent,
            PendingIntent.FLAG_IMMUTABLE
        )

    }

    override fun onDestroy() {
        super.onDestroy()
        // Call finish() to clear the Chrome Custom Tab and return to your app
        finish()
    }
}