package com.nibula.request_to_invest.paypal.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.nibula.R

class PaypalPaymentFragment : Fragment() {

    companion object {
        fun newInstance(extras: Bundle?) = PaypalPaymentFragment().apply {
            arguments = extras
        }
    }

    private lateinit var viewModel: paypalViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.paypal_fragment, container, false)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(paypalViewModel::class.java)

    }



}