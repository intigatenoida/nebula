package com.nibula.request_to_invest

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebSettings.PluginState
import android.webkit.WebView
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.devs.readmoreoption.ReadMoreOption
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.GsonBuilder
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.customview.CustomToast
import com.nibula.dashboard.DashboardActivity
import com.nibula.databinding.FragmentNewOfferingBinding
import com.nibula.investorcertificates.adapter.NewInvestorCertificateAdapter
import com.nibula.request_to_invest.adapter.AlbumAdapter
import com.nibula.request_to_invest.helper.OfferingHelper
import com.nibula.request_to_invest.interfaces.AlbumOperation
import com.nibula.request_to_invest.paypal.PaypalPaymentActivity
import com.nibula.request_to_invest.viewmodal.PaymentInfoViewModel
import com.nibula.response.details.File
import com.nibula.response.details.Response
import com.nibula.user.login.LoginActivity
import com.nibula.utils.*
import com.nibula.utils.interfaces.DialogFragmentClicks
import com.nibula.wallect_connect.SignUtils

import com.stripe.android.ApiResultCallback
import com.stripe.android.PaymentIntentResult
import com.stripe.android.Stripe
import com.stripe.android.model.ConfirmPaymentIntentParams
import com.stripe.android.model.PaymentMethodCreateParams
import com.stripe.android.model.StripeIntent
import com.walletconnect.sign.client.Sign
import com.walletconnect.sign.client.SignClient
import kotlinx.coroutines.*
import java.io.IOException
import java.net.URL
import java.util.concurrent.TimeUnit

class OfferingFragment : BaseFragment(), CommunicatorOffering, timerListiner, DialogFragmentClicks,
    OnCreditCardProceed, OnSubmitButton, OnPayPalProceed, OnPaymentMethodClick, OnClickOfCheckOut,
    AlbumOperation {
    lateinit var rootView: View
    private var imageUrl: String? = null
    private var albumId: String? = null
    private var sessionTopic: String? = null
    private var account: String? = null
    private var walletUri: String? = null
    lateinit var helper: OfferingHelper
    var isInvestmentStatusUpdated = false
    private var countTimer: CountDownTimer? = null
    lateinit var timerListiner: timerListiner
    var recordId: String? = null
    var fileId: Int? = 0
    var recordName: String? = null
    var checkRecordNotified: Boolean = false
    var checkRecordFavorite: Boolean = true
    private var selectedPosition: Int = -1
    lateinit var costBreakDownResponse: CostBreakdownResponse
    lateinit var detailResponse: Response
    lateinit var paymentInfoViewModel: PaymentInfoViewModel
    lateinit var params: PaymentMethodCreateParams
    private lateinit var background: FrameLayout
    var recordTitleStr = ""
    var isReleased = 0
    var releasedDate = ""
    var numberOfToken: Int = 0
    private lateinit var stripe: Stripe
    var clientSecret: String? = null
    lateinit var window: Window
    private var EXPAND_AVATAR_SIZE: Float = 0F
    private var COLLAPSE_IMAGE_SIZE: Float = 0F
    private var horizontalToolbarMargin: Float = 0F
    private lateinit var appBarLayout: AppBarLayout
    private lateinit var toolbar: Toolbar
    private lateinit var ivUserAvatar: ImageView
    private var animationStartPointY: Float = 0F
    private var collapseAnimationChangeWeight: Float = 0F
    private var isCalculated = false
    private var verticalToolbarAvatarMargin = 0F
    var isRecordTimerFinish = false
    var shareAllowedToPurchase: Int = 0
    lateinit var binding: FragmentNewOfferingBinding
    var albumAdapter: AlbumAdapter? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        timerListiner = this@OfferingFragment
    }

    val adapter by lazy {
        NewInvestorCertificateAdapter(requireContext(), this)
    }

    companion object {
        fun newInstance(bundle: Bundle?): OfferingFragment {
            val fg = OfferingFragment()
            fg.arguments = bundle
            return fg
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_new_offering, container, false)
            binding = FragmentNewOfferingBinding.inflate(inflater, container, false)
            rootView = binding.root
            helper = OfferingHelper(this@OfferingFragment)
            window = requireActivity().window!!

            paymentInfoViewModel = activity?.run {
                ViewModelProvider(this).get(PaymentInfoViewModel::class.java)
            } ?: throw Exception("Invalid Activity")
            binding.ivBack.setOnClickListener {
                hideKeyboard()
                navigate.manualBack()
            }

            binding.tvMenu.setOnClickListener {
                navigate.onClickShare(recordId!!, recordTitleStr, isReleased, releasedDate)
            }
            binding.tvViewAll.setOnClickListener {

                navigate.goToViewCollectorsScreen(recordId)
            }

            binding.edtTokenNumber.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {

                }

                override fun beforeTextChanged(
                    s: CharSequence, start: Int, count: Int, after: Int
                ) {

                }

                @SuppressLint("SetTextI18n")
                override fun onTextChanged(
                    s: CharSequence, start: Int, before: Int, count: Int
                ) {
                    if (CommonUtils.isLogin(requireActivity())) {
                        if (s.length == 0 || s.toString().toInt() == 0) {
                            binding.tvAmount.text = ""
                            return
                        }
                        if (s.toString().toInt() > shareAllowedToPurchase) {
                            Toast.makeText(
                                requireActivity(),
                                "You can buy max ${shareAllowedToPurchase} Token(s)",
                                Toast.LENGTH_SHORT
                            ).show()
                            return
                        }
                        if (s.toString().toInt() > detailResponse.sharesAvailable?.toInt()!!) {
                            Toast.makeText(
                                requireActivity(),
                                "Available tokens are ${detailResponse.sharesAvailable!!.toInt()}",
                                Toast.LENGTH_SHORT
                            ).show()
                            return
                        }
                        val pricePerTokenInDolor = CommonUtils.trimDecimalToTwoPlaces(
                            detailResponse.valuePerShareInDollars ?: 0.00
                        )
                        val resultantPrice =
                            (s.toString().toInt()) * pricePerTokenInDolor.toDouble()
                        binding.tvAmount.setText(
                            "${detailResponse.currencyType ?: "USD"}${
                                CommonUtils.trimDecimalToTwoPlaces(resultantPrice)
                            }"
                        )
                        helper.getCostBreakDown(detailResponse.id, s.toString().toInt(), true)
                    } else {
                        val intent = Intent(requireActivity(), LoginActivity::class.java)
                        intent.putExtra(AppConstant.ISFIRSTTIME, false)
                        startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
                    }
                }
            })

/*            rootView.tvOpenInMetaMask.setOnClickListener {
                try {
                    val i = Intent(Intent.ACTION_VIEW)

                    i.data = Uri.parse("https://metamask.app.link/dapp/nebu.la")
                    startActivity(i)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        requireActivity(),
                        "There is no wallet found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }*/

/*
            binding.tvOpenInMetaMask.setOnClickListener {

                */
/*   val smartContractAddressMain = "0x5b9515893F5791B0101f5319b107ba2e316A946f"
                     val smartContractAddress = "0xcD0FC5078Befa4701C3692F4268641A468DEB8d5"
                     // keccak256(transactionMethod).take(4 bytes)
                     val smartContractTransactionMethod = "a9059cbb"
                     // must be 32 bytes*//*

                */
/* val to = "621261D26847B423Df639848Fb53530025a008e8".padStart(64, '0')
                 // 3 decimal; must be 32 bytes; 1_000L.toHex()
                 val amount = BigInteger("1000").toString(16).padStart(64, '0')*//*


                val function = org.web3j.abi.datatypes.Function(
                    "buyTokens", listOf(
                        Address("0x5b9515893F5791B0101f5319b107ba2e316A946f"),
                        Uint256(BigInteger.valueOf(1)),
                        Uint256(BigInteger.valueOf(1))
                    ), emptyList<TypeReference<*>>()
                )
                val gasPrice = "0x02540be400"
                val gasLimit = "0x9c40"
                val data = FunctionEncoder.encode(function)
                var walletAddress: String = PrefUtils.getValueFromPreference(
                    requireContext(),
                    PrefUtils.CONNECTED_WALLET_ADDRESS
                )
                val chainId = "eip155:1"
                */
/* EthTransaction(
                        from = walletAddress,
                        to = "0x5b9515893F5791B0101f5319b107ba2e316A946f",
                        data = data!!,
                        chainId = chainId,
                        gas = gasLimit, // can't auto calculate for smart contract
                        gasPrice = gasPrice, // can auto calculate
                        gasLimit = null,
                        maxFeePerGas = null,
                        maxPriorityFeePerGas = null,
                        value = "", // this is in 'data' field now
                        nonce = null // ignored by metamask
                    )*//*


                val web3j =
                    Web3j.build(HttpService("https://mumbai.polygonscan.com/address/0x5b9515893F5791B0101f5319b107ba2e316A946f"))
                val nonce = BigInteger.valueOf(100)
                val gasprice = BigInteger.valueOf(100)
                val gaslimit = BigInteger.valueOf(100)
                val transaction: Transaction = Transaction
                    .createFunctionCallTransaction(
                        walletAddress,
                        nonce,
                        gasprice,
                        gaslimit,
                        "0x5b9515893F5791B0101f5319b107ba2e316A946f",
                        BigInteger.ONE,
                        data
                    )
                val transactionHash = web3j.ethSendTransaction(transaction).sendAsync().get()
                Log.d("TransactionHash", transactionHash.toString())
                */
/*  ExampleApplication.session.performMethodCall(
                      Session.MethodCall.SendTransaction(
                          id = txRequest,
                          from = from,
                          to = "0x5b9515893F5791B0101f5319b107ba2e316A946f",
                          nonce = "",
                          gasPrice = gasPrice,
                          gasLimit = gasLimit,
                          value = "0x00",
                          data = data
                      ),
                      ::handleResponse
                  )
                  this@MainActivity.txRequest = txRequest
                  val intent = Intent(Intent.ACTION_VIEW)
                  intent.data = Uri.parse("wc:")
                  intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                  startActivity(intent)
              } catch (e: Exception) {
                  Log.i(TAG, "onStart: screenMainTxButton:${e.message}")
              }*//*

            }
*/

            /*binding.btnRecordStatus.setOnClickListener {
                signInAuthenticationOfRecord()
            }
*/

            binding.btnRecordStatus.setOnClickListener {
                if (CommonUtils.isLogin(requireActivity())) {
                    if (detailResponse.recordStatusTypeId!! == AppConstant.released) {
                        if (binding.edtTokenNumber.text.toString().isEmpty()) {
                            Toast.makeText(
                                requireActivity(),
                                "Please enter number of tokens",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (binding.edtTokenNumber.text.toString().toInt() == 0) {
                            Toast.makeText(
                                requireActivity(),
                                "Number of token should be greater than 0.",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (binding.edtTokenNumber.text.toString()
                                .toInt() > detailResponse.sharesAvailable!!.toInt()
                        ) {
                            Toast.makeText(
                                requireActivity(),
                                "Available tokens are ${detailResponse.sharesAvailable!!.toInt()}",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (binding.edtTokenNumber.text.toString()
                                .toInt() > shareAllowedToPurchase
                        ) {
                            Toast.makeText(
                                requireActivity(),
                                "You can purchase max ${shareAllowedToPurchase} Token(s)",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            val bPrompt = BottomSheetSelectPaymentMethod(
                                recordId!!, this@OfferingFragment, this@OfferingFragment
                            )
                            bPrompt.isCancelable = true
                            bPrompt.show(
                                this@OfferingFragment.childFragmentManager,
                                "PositiveNegativeBottomSheetFragment"
                            )
                        }
                    }

                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    intent.putExtra(AppConstant.ISFIRSTTIME, false)
                    startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
                }
            }

            binding.ivPlay.setOnClickListener {
                onSongClick(true)
            }
            binding.ivLike.setOnClickListener {
                if (CommonUtils.isLogin(requireActivity())) {
                    helper.recordFavorite(true, recordId!!, checkRecordFavorite, fileId!!)

                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    intent.putExtra(AppConstant.ISFIRSTTIME, false)
                    startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
                }

            }
            binding.ivNotify.setOnClickListener {
                if (CommonUtils.isLogin(requireActivity())) {
                    if (detailResponse.recordStatusTypeId == AppConstant.approved_by_admin) {
                        val bPrompt = GetNotifyBottomSheetFragment(
                            recordId!!, this@OfferingFragment
                        )
                        bPrompt.isCancelable = false
                        bPrompt.show(
                            this@OfferingFragment.childFragmentManager,
                            "PositiveNegativeBottomSheetFragment"
                        )
                    } else {
                        Toast.makeText(
                            requireActivity(),
                            "You can notify the record only before the release",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    intent.putExtra(AppConstant.ISFIRSTTIME, false)
                    startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
                }
            }
            albumId = arguments?.getString(AppConstant.ID) ?: ""
            imageUrl = arguments?.getString(AppConstant.RECORD_IMAGE_URL) ?: ""
            walletUri = arguments?.getString(AppConstant.WALLET_URI) ?: ""
            account = arguments?.getString(AppConstant.CONNECTED_WALLET_ACCOUNT) ?: ""
            sessionTopic = arguments?.getString(AppConstant.SELECTED_SESSION_TOPIC) ?: ""
            val linearLayoutManager =
                LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            binding.rvCollectedBy.layoutManager = linearLayoutManager
            binding.rvCollectedBy.adapter = adapter
            setRecordImage(imageUrl.toString())
            if (albumId.isNullOrEmpty().not()) {
                helper.getRecordDetailsNew(albumId)
                helper.getCollectorsListByRecordId(true, albumId!!)
            } else {
                navigate.manualBack()
            }

        }

        return rootView
    }

    override fun onResume() {
        super.onResume()

        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
            playerReceiver, IntentFilter(AppConstant.PLAYER_INTENT_FILTER)
        )

        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
            songCloseReceiver, IntentFilter(AppConstant.IMAGE_CLOSE)
        )
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
            playPauseReceiver, IntentFilter(AppConstant.PLAY_PAUSE)
        )
    }

    private val playerReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val lastRecordId = intent?.getStringExtra(AppConstant.RECORD_ID) ?: ""
            if (recordId == lastRecordId) {
                // val id = intent?.getIntExtra(AppConstant.ID, -1) ?: -1
                // val isPlaying = intent?.getBooleanExtra(AppConstant.STATUS, false) ?: false
                //val fromTopButton = intent?.getBooleanExtra(AppConstant.FromTopButton, false) ?: false
                selectedPosition = intent?.getIntExtra(AppConstant.SECRET_KEY, -1) ?: -1

                // managePlayPauseIcon(isPlaying)
                //updatePlayAllButton(fromTopButton, isPlaying)

                // (requireContext() as DashboardActivity).managePlayPauseIcon(isPlaying)
                //
                //  adapter.updateSongStatus(id, isPlaying)
            }
        }
    }



    private val playPauseReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val isPlaying = intent?.getBooleanExtra("is_playing", false) ?: false
            albumAdapter?.playPause(isPlaying)
            /*if (recordId == lastRecordId) {
                val id = intent?.getIntExtra(AppConstant.ID, -1) ?: -1
                val isPlaying = intent?.getBooleanExtra(AppConstant.STATUS, false) ?: false
                val fromTopButton =intent?.getBooleanExtra(AppConstant.FromTopButton, false) ?: false
                selectedPosition = intent?.getIntExtra(AppConstant.SECRET_KEY, -1) ?: -1
                //updatePlayAllButton(fromTopButton, isPlaying)
                adapter.updateSongStatus(id, isPlaying)
            }*/
        }
    }


    private val songCloseReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            albumAdapter?.refreshAdapter()
        }
    }

/*
    private fun managePlayPauseIcon(isPlaying: Boolean) {

        if (isPlaying) {

            img_play.setImageDrawable(
                ContextCompat.getDrawable(
                    requireActivity(),
                    R.drawable.ic_pause
                )
            )
        } else {
            img_play.setImageDrawable(
                ContextCompat.getDrawable(
                    requireActivity(),
                    R.drawable.ic_play
                )
            )
        }
    }
*/


    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (this::stripe.isInitialized) {
            stripe.onPaymentResult(requestCode,
                data,
                object : ApiResultCallback<PaymentIntentResult> {
                    override fun onError(e: Exception) {
                        val sendIntent = Intent()
                        sendIntent.putExtra("errorMessage", e.message)
                        sendIntent.putExtra("isUserCanceled", false)
                        println("onError: ${e.message}")
                        helper.onActivityResult(
                            RequestCode.STRIP_REQUEST_CODE, Activity.RESULT_CANCELED, sendIntent
                        )
                        /*  hideProcessDialog()
                          requireActivity().setResult(Activity.RESULT_CANCELED, sendIntent)*/
                    }

                    override fun onSuccess(result: PaymentIntentResult) {
                        val paymentIntent = result.intent
                        val status = paymentIntent.status
                        val gson = GsonBuilder().create()
                        val sendIntent = Intent()
                        sendIntent.putExtra("isUserCanceled", false)
                        if (status == StripeIntent.Status.Succeeded) {
                            // Payment completed successfully
                            sendIntent.putExtra("success", gson.toJson(paymentIntent))
                            helper.onActivityResult(
                                RequestCode.STRIP_REQUEST_CODE, Activity.RESULT_OK, sendIntent
                            )
                        } else if (status == StripeIntent.Status.RequiresPaymentMethod) {
                            //sendIntent.putExtra("error", paymentIntent.lastPaymentError?.message)
                            sendIntent.putExtra("error", gson.toJson(paymentIntent))
                            requireActivity().setResult(Activity.RESULT_CANCELED, sendIntent)
                        }
                        hideProcessDialog()
                    }

                })
        }

    }

    override fun onStop() {
        super.onStop()
        window.statusBarColor = ContextCompat.getColor(requireActivity(), R.color.colorPrimary)

    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopCounter()
        helper.onDestroy()
    }

    fun setRecordImage(urlImage: String) {
        if (urlImage.isEmpty()) return
        val resizeUrl = "$urlImage?maxwidth=300&maxheight=300&quality=50"
        val urlImage = URL(resizeUrl)
        val result: Deferred<Bitmap?> = GlobalScope.async {
            urlImage.toBitmap()
        }
        GlobalScope.launch(Dispatchers.Main) {
            binding.ivRoundedImage?.setImageBitmap(result.await())
            createDarkPaletteAsync(result.await())
        }
    }

    private fun startCounter(timeLeftToRelease: Long) {
        if (countTimer != null) {
            countTimer?.cancel()
            countTimer = null
        }
        countTimer = object : CountDownTimer(timeLeftToRelease * 1000, 1000) {
            override fun onFinish() {

            }

            override fun onTick(millisUntilFinished: Long) {
                TimestampTimeZoneConverter.setTimerTextViewNewOffering(
                    (millisUntilFinished / 1000),
                    binding.tvDays,
                    binding.tvHour,
                    binding.tvMin,
                    binding.tvSec,
                    timerListiner
                )
                Log.e("***********timer", "" + millisUntilFinished)
            }

        }
        countTimer?.start()
    }

    fun stopCounter() {
        countTimer?.let {
            it.cancel()
            countTimer = null
        }
    }

    @SuppressLint("SetTextI18n")
    fun updateUI(response: Response?) {

        detailResponse = response!!
        binding.mainRoot.visibility = View.VISIBLE
        if (response.RaffleLandingPage?.isNotEmpty()!!) {
            var videoId = response.RaffleLandingPage
            initiateYouTubeWebView(videoId.toString(), response)
        }
        response.isNotified.let {
            if (it!!) {
                checkRecordNotified = true
                binding.ivNotify.setBackgroundResource(R.drawable.noti_icon)
            } else {
                checkRecordNotified = false
                binding.ivNotify.setBackgroundResource(R.drawable.notification_2)

            }
        }
        response.isFavourite?.let {
            if (it) {
                checkRecordFavorite = false
                binding.ivLike.setBackgroundResource(R.drawable.ic_baseline_favorite_24)
            } else {
                checkRecordFavorite = true
                binding.ivLike.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24)
            }
        }

        response.recordTitle?.let {
            recordName = it
        }

        recordId = response.id
        fileId = response.files?.get(0)?.id
        recordTitleStr = response?.recordTitle!!

        binding.tvTotalToken.text =
            "${response.sharesAvailable?.toInt()}/${response?.totalNoOfShares?.toInt()}"

        binding.tvOwnershipPercentage.text =
            "${response.royalityOwnershipPercentagePerToken.toString() + "%"}"

        binding.tvPricePerToken.text = "${response?.currencyType ?: "USD"}${
            CommonUtils.trimDecimalToTwoPlaces(
                response?.valuePerShareInDollars ?: 0.00
            )
        }"
        response.recordTitle?.let {
            binding.tvRecordTitle.text = it
        }

        response.artistName?.let {
            binding.tvArtistName.text = it

        }

        if (response?.recordStatusTypeId == 2) {
            isReleased = 0
        } else {
            isReleased = 1
        }
        response.artistImage?.let {
            Glide.with(requireActivity()).load(it).placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
                .into(binding.ivArtist)
        }

        response.recordTitle?.let {
            binding.tvRecordTitle2.text = it
        }

        response.artistName?.let {
            binding.tvDescription.text = it
        }

        if (imageUrl!!.isEmpty()) {
            setRecordImage(response.recordImage!!)
        }

        response.rewards?.let {
            if (it.isEmpty()) {
                binding.clRewardds.visibility = View.GONE
                // rootView.tvRewardsVaule.text = "No Reward found"
            } else {
                binding.clRewardds.visibility = View.VISIBLE

                val readMoreOption = ReadMoreOption.Builder(requireActivity())
                    .textLength(5, ReadMoreOption.TYPE_LINE) // OR
                    //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
                    .moreLabel("Read More").lessLabel("Read Less").moreLabelColor(Color.GRAY)
                    .lessLabelColor(Color.GRAY).labelUnderLine(true).expandAnimation(true).build()

                readMoreOption.addReadMoreTo(binding.tvRewardsVaule, it)

            }
        }

        response.messageToCoOwners?.let {

            if (it.isEmpty()) {
                binding.clBackStory.visibility = View.GONE
                // rootView.tvRewardsVaule.text = "No Reward found"
                return
            }
            // rootView.tvBackstoryValue.text = it
            binding.clBackStory.visibility = View.VISIBLE

            val readMoreOption = ReadMoreOption.Builder(requireActivity())
                .textLength(5, ReadMoreOption.TYPE_LINE) // OR
                //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
                .moreLabel("Read More").lessLabel("Read Less").moreLabelColor(Color.GRAY)
                .lessLabelColor(Color.GRAY).labelUnderLine(true).expandAnimation(true).build()

            readMoreOption.addReadMoreTo(binding.tvBackstoryValue, it)

        }
        response.recordTypeId.let {

            when (it) {
                1 -> {
                    binding.clAlbum.visibility=View.VISIBLE
                    binding.tvFixedAvailableToken.text = "Available Tokens Bundles"
                    binding.tvFixedPricePerToken.text = "Full Album Price"
                    binding.tvAlbumType.text = "Album"
                    binding.ivAlbum.visibility = View.VISIBLE
                    binding.ivPlay.visibility = View.GONE
                    val recordTitle = response.recordTitle
                    val fullAlbum = "$recordTitle\n(Full Album)"
                    binding.ivLike.visibility = View.GONE
                    binding.tvAlbum.text = fullAlbum
                    binding.ivAlbumArtist.visibility = View.VISIBLE
                    val layoutManager =
                        LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                    binding.rvAlbum.layoutManager = layoutManager
                    albumAdapter = AlbumAdapter(
                        requireContext(),
                        response,
                        response.files!!,
                        this@OfferingFragment
                    )
                    binding.rvAlbum.adapter = albumAdapter

                    Glide.with(requireActivity()).load(response.artistImage)
                        .placeholder(R.drawable.nebula_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
                        .into(binding.ivAlbumArtist)
                }
                else -> {
                    //Single Song
                    binding.clAlbum.visibility=View.VISIBLE

                    binding.ivPlay.visibility = View.VISIBLE
                    binding.ivAlbum.visibility = View.GONE
                    binding.ivLike.visibility = View.VISIBLE
                }
            }
        }

        response?.recordStatusTypeId?.let {

            when (response.recordStatusTypeId) {

                AppConstant.request_submitted -> {
                    binding.btnRecordStatus.text = response.recordStatusType
                    binding.btnRecordStatus.setBackgroundResource(R.drawable.panding_background_7dp)
                    binding.btnRecordStatus.isEnabled = false

                }
                AppConstant.approved_by_admin -> {
                    binding.mainRow.visibility = View.VISIBLE
                    startCounter(response.timeLeftToRelease!!.toLong())
                    binding.btnRecordStatus.text = response.recordStatusType
                    binding.btnRecordStatus.setBackgroundResource(R.drawable.panding_background_7dp)
                    binding.btnRecordStatus.isEnabled = false


                }
                AppConstant.released -> {
                    binding.mainRow.visibility = View.GONE
                    binding.tvRelease.visibility = View.GONE
                    binding.btnRecordStatus.text = response.recordStatusType
                    binding.btnRecordStatus.setBackgroundResource(R.drawable.button_blue_selector)
                    binding.btnRecordStatus.isEnabled = true
                    binding.edtTokenNumber.isEnabled = true
                    binding.edtTokenNumber!!.isFocusableInTouchMode = true
                    binding.releaseGroup.visibility = View.VISIBLE

                    binding.tvOpenInMetaMask.text = "Purchase NFT"
                    binding.tvOpenInMetaMask.setBackgroundResource(R.drawable.button_blue_selector)
                    binding.tvOpenInMetaMask.isEnabled = true
                    binding.tvOpenInMetaMask.isEnabled = true


                }
                AppConstant.sold_out -> {
                    binding.mainRow.visibility = View.GONE
                    binding.ivRoundedImage.alpha = 1f
                    binding.btnRecordStatus.text = response.recordStatusType
                    binding.btnRecordStatus.setBackgroundResource(R.drawable.panding_background_7dp)
                    binding.btnRecordStatus.isEnabled = false
                }
                else -> {
                    // Nothing
                }
            }

        }

    }

    private fun collapseViewInit() {
        EXPAND_AVATAR_SIZE = resources.getDimension(R.dimen._150dp)
        COLLAPSE_IMAGE_SIZE = resources.getDimension(R.dimen._55dp)
        horizontalToolbarMargin = resources.getDimension(R.dimen._34dp)
        appBarLayout = rootView.findViewById(R.id.app_bar_layout)
        toolbar = rootView.findViewById(R.id.anim_toolbar)
        ivUserAvatar = rootView.findViewById(R.id.iv1)
        background = rootView.findViewById(R.id.bg)

        (toolbar.height - COLLAPSE_IMAGE_SIZE) * 2

        appBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, i ->
            if (isCalculated.not()) {
                animationStartPointY =
                    Math.abs((appBarLayout.height - (EXPAND_AVATAR_SIZE + horizontalToolbarMargin)) / appBarLayout.totalScrollRange)
                collapseAnimationChangeWeight = 1 / (1 - animationStartPointY)
                verticalToolbarAvatarMargin = (toolbar.height - COLLAPSE_IMAGE_SIZE) * 2
                isCalculated = true
            }
            //updateViews(Math.abs(i / appBarLayout.totalScrollRange.toFloat()))
        })
    }

    fun updateButton(
        investmentStatusType: Int,
        investmentStatus: String?,
        recordId: String?,
        pendinginvestments: Int?,
        detailsResponse: Response
    ) {
        when (investmentStatusType) {
            AppConstant.InvestNow, AppConstant.RequestApproved -> {
                binding.btnRecordStatus.text = investmentStatus
                binding.btnRecordStatus.setBackgroundResource(R.drawable.button_blue_selector)
                binding.btnRecordStatus.isEnabled = true
                binding.edtTokenNumber.isEnabled = true
            }
            AppConstant.RequestToInvest -> {
                binding.btnRecordStatus.setBackgroundResource(R.drawable.button_blue_selector)
                binding.btnRecordStatus.isEnabled = true
                binding.btnRecordStatus.text = investmentStatus
            }
            AppConstant.SelfOwned -> {
                run {
                    when (detailsResponse.recordStatusTypeId) {
                        AppConstant.initialoffering -> {
                            binding.btnRecordStatus.visibility = View.GONE
                        }
                        AppConstant.released -> {
                            binding.btnRecordStatus.visibility = View.GONE
                        }
                    }
                }
            }

            AppConstant.RequestRejected -> {
                binding.btnRecordStatus.visibility = View.GONE
            }

            AppConstant.AlreadyInvested -> {
                run {
                    binding.btnRecordStatus.visibility = View.VISIBLE
                    binding.btnRecordStatus.setBackgroundResource(R.drawable.panding_background_7dp)
                    binding.btnRecordStatus.text = investmentStatus
                    binding.btnRecordStatus.isEnabled = false
                }

            }
            else -> {
                binding.btnRecordStatus.visibility = View.GONE
            }
        }
        isInvestmentStatusUpdated = true
    }

    fun createDarkPaletteAsync(bitmap: Bitmap?) {

        try {
            Palette.from(bitmap!!).generate { p ->
                val defaultValue = 0x000000
                binding.ivRendering?.setBackgroundColor(p!!.getDominantColor(defaultValue))
                Log.d("Color", p!!.getVibrantColor(defaultValue).toString())
                val hexaColor = Integer.toHexString(p.getDominantColor(defaultValue))

                // Default color in case color parse exception found
                var rgbColor: Int
                try {
                    rgbColor = Color.parseColor("#$hexaColor")
                } catch (e: IllegalArgumentException) {
                    rgbColor = Color.parseColor("#000000")
                }

                val halfTransparentColor: Int = adjustAlpha(rgbColor, 0.4f)
                window?.statusBarColor = halfTransparentColor

            }
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }

    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = requireActivity().resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    @ColorInt
    fun adjustAlpha(@ColorInt color: Int, factor: Float): Int {
        val alpha = Math.round(Color.alpha(color) * factor)
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)
        return Color.argb(alpha, red, green, blue)
    }


    fun onSongClick(fromTop: Boolean) {
        val list = detailResponse.files!!
        if (list.isNotEmpty()) {
            (requireActivity() as DashboardActivity).playMusic(
                list, 0, fromTop, detailResponse.recordTitle!!, detailResponse.id!!, 1, ""
            )
        }

    }

    fun onPauseMusic() {
        (requireActivity() as DashboardActivity).pauseMusic()
    }

    override fun onProfileClick(userId: String, userType: Int) {
        navigate.openTopArtist(userId, userType)
    }

    override fun onTimerFinish(position: Int) {
        if (::rootView.isInitialized) {
            isRecordTimerFinish = true
            helper.getRecordDetailsNew(albumId)
        }
    }

    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {

        dialog.dismiss()

        helper.notifyMeViaMail(true, recordName!!, bundle?.getString("email").toString())
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        dialog.dismiss()
    }

    override fun onPaymentMethodClick(paymentmethod: Int) {
        /*       if (paymentmethod == 1) {
                   val bPrompt = StripeBottomSheetFragment(
                       recordId!!, this@OfferingFragment, this@OfferingFragment, this@OfferingFragment
                   )
                   bPrompt.isCancelable = true
                   bPrompt.show(
                       this@OfferingFragment.childFragmentManager,
                       "PositiveNegativeBottomSheetFragment"
                   )
               } else {

               }*/
        val bPrompt = BottomSheetSubmitFragment(
            recordId!!, this@OfferingFragment, this@OfferingFragment
        )
        bPrompt.isCancelable = true
        bPrompt.show(
            this@OfferingFragment.childFragmentManager, "PositiveNegativeBottomSheetFragment"
        )
        paymentInfoViewModel.setPaymentMethod(paymentmethod)

    }

    override fun onCreditCardProceed(createParams: PaymentMethodCreateParams) {

        val bPrompt = BottomSheetSubmitFragment(
            recordId!!, this@OfferingFragment, this@OfferingFragment
        )
        bPrompt.isCancelable = true
        bPrompt.show(
            this@OfferingFragment.childFragmentManager, "PositiveNegativeBottomSheetFragment"
        )
        params = createParams

    }

    override fun onClickOfSubmitButton() {
        val bPrompt = CheckOutBottomSheetFragment(
            recordId!!, this@OfferingFragment, this@OfferingFragment
        )
        bPrompt.isCancelable = true
        bPrompt.show(
            this@OfferingFragment.childFragmentManager, "PositiveNegativeBottomSheetFragment"
        )
    }

    fun initatePayment() {

        stripeConfirmPayment(params)
    }

    fun onConfirmOfStripePayment() {
        val bPrompt = BottomSheetSubmitFragment(
            recordId!!, this@OfferingFragment, this@OfferingFragment
        )
        bPrompt.isCancelable = true
        bPrompt.show(
            this@OfferingFragment.childFragmentManager, "PositiveNegativeBottomSheetFragment"
        )
    }


    fun stripeConfirmPayment(stripeParams: PaymentMethodCreateParams) {
        if (clientSecret.isNullOrEmpty()) {
            showToast(
                requireContext(),
                getString(R.string.message_stripe_error),
                CustomToast.ToastType.FAILED
            )
        }

        if (stripeParams == null) {
            showToast(
                requireContext(),
                getString(R.string.message_credit_card),
                CustomToast.ToastType.FAILED
            )
        }
        val confirmParams = ConfirmPaymentIntentParams.createWithPaymentMethodCreateParams(
            stripeParams, clientSecret!!
        )
        stripe = Stripe(
            requireContext(), AppConstant.LIVE_PUBLISH_KEY_STRIP
        )
        stripe.confirmPayment(this, confirmParams)
    }


/*
    fun processPayment(amount: Double, shortDescription: String, token: String) {
        PayPalCheckout.start(CreateOrder { createOrderActions ->
            val order = Order(
                intent = OrderIntent.CAPTURE, appContext = AppContext(
                    userAction = UserAction.PAY_NOW
                ), purchaseUnitList = listOf(
                    PurchaseUnit(
                        amount = Amount(
                            currencyCode = CurrencyCode.USD, //---> default current ot accept payent in
                            value = amount.toString() //--> amount
                        )
                    )
                )
            )
            createOrderActions.create(order)
        }, OnApprove { approval ->
            approval.orderActions.capture { captureOrderResult ->
                Log.i("CaptureOrder", "Order successfully captured: $captureOrderResult")
                approval.data.orderId?.let { helper.payPalSuccessPaymentWithOrderId(it) }
            }
        }*/
/*,
            OnCancel {
                Log.i("CaptureOrder", "Order errorInfo:on canclled")
                // Optional callback for when a buyer cancels the paysheet
                helper.payPalErrorPayment(false, "Payment Cancelled")

            },
            OnError { errorInfo ->
                //Optional error callback
                helper.payPalErrorPayment(false, errorInfo.toString())
                Log.i("CaptureOrder", "Order errorInfo: $errorInfo")
            }*//*

        )
        //---> run
    }
*/


    override fun onClickOfFinalCheckOut() {
        /*   val intent= Intent(requireActivity(), PaypalPaymentActivity::class.java)
            val bundle=Bundle()
            bundle.putParcelable("response",detailResponse)
            intent.putExtras(bundle)
            startActivity(intent)*/

        /*
        Kotlin formation
         */
        Intent(requireActivity(), PaypalPaymentActivity::class.java).apply {
            putExtra("recordId", detailResponse.id)
            putExtra("currency_type", detailResponse.currencyType)
            putExtra("amount", paymentInfoViewModel.totalAmount.value!!)
            putExtra("investment_token", numberOfToken)
            startActivityForResult(this, 4050)


        }
        /*  var paymentMethod = paymentInfoViewModel.paymentMethod?.value!!
          helper.investNowPaymentBegin(
              detailResponse.id!!,
              paymentMethod,
              true,
              "",
              edtTokenNumber.text.toString().toInt()
          )
  */

    }

    fun initiateYouTubeWebView(videoID: String, response: Response?) {
        binding.clYouTubePlayer.visibility = View.VISIBLE
        binding.tvFixedVideoTitle.text = binding.tvFixedVideoTitle.text.toString()
            .replace("#", """"[${response!!.recordTitle.toString()}]"""")
        val web_view: WebView = binding.playerWebView
        web_view.getSettings().setJavaScriptEnabled(true)
        web_view.getSettings().setPluginState(PluginState.ON)
        web_view.loadUrl("https://www.youtube.com/embed/" + videoID + "?autoplay=1&vq=small")

    }

    fun signInAuthenticationOfRecord() {

        val sessionTopic = sessionTopic
        val method = "personal_sign"
        val params = SignUtils.getPersonalSignBody(account!!)
        val chainId = "eip155:1"
        val expiry =
            (System.currentTimeMillis() / 1000) + TimeUnit.SECONDS.convert(7, TimeUnit.DAYS)
        val requestParams = Sign.Params.Request(sessionTopic!!, method, params, chainId, expiry);
        val activeConnection = checkNotNull(SignClient.getActiveSessionByTopic(sessionTopic!!))
        Log.d("Connection is active", activeConnection.toString())
        SignClient.request(requestParams, onSuccess = { req: Sign.Model.SentRequest ->
            /* callback for success while sending request over session */
            Log.d("Sign_Client_Request_Send_Success", req.toString())
        }, onError = { error: Sign.Model.Error ->
            /* callback for error while sending request over session */
            Log.e("Sign_Client_Request_Send_Error", error.throwable.stackTraceToString())
        })

        startActivity(Intent(Intent.ACTION_VIEW, walletUri!!.toUri()))
    }

    override fun onPaypalProceed() {

        val bPrompt = BottomSheetSubmitFragment(
            recordId!!, this@OfferingFragment, this@OfferingFragment
        )
        bPrompt.isCancelable = true
        bPrompt.show(
            this@OfferingFragment.childFragmentManager, "PositiveNegativeBottomSheetFragment"
        )
    }

    override fun onSongLike(position: Int, fileId: Int, isFavourite: Boolean) {
        if (CommonUtils.isLogin(requireActivity())) {
            helper.likeSong(true, recordId!!, !isFavourite, fileId!!, position)

        } else {
            val intent = Intent(requireActivity(), LoginActivity::class.java)
            intent.putExtra(AppConstant.ISFIRSTTIME, false)
            startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
        }
    }

    override fun onSongPlay(position: Int, recordId: String, files: MutableList<File>?) {
        (requireActivity() as DashboardActivity).playMusic(
            files!!, position, true, detailResponse.recordTitle!!, detailResponse.id!!, 1, ""
        )
    }

    override fun onPauseSong() {
        onPauseMusic()
    }


}

fun URL.toBitmap(): Bitmap? {
    return try {
        BitmapFactory.decodeStream(openStream())
    } catch (e: IOException) {
        null
    }

}

interface CommunicatorOffering {
    fun onProfileClick(userId: String, userType: Int)
}

interface OnPaymentMethodClick {
    fun onPaymentMethodClick(paymentmethod: Int)
}

interface OnCreditCardProceed {
    fun onCreditCardProceed(params: PaymentMethodCreateParams)
}

interface OnPayPalProceed {
    fun onPaypalProceed()
}

interface OnSubmitButton {
    fun onClickOfSubmitButton()
}

interface OnClickOfCheckOut {
    fun onClickOfFinalCheckOut()
}