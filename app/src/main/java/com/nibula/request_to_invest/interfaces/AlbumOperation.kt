package com.nibula.request_to_invest.interfaces

import com.nibula.response.details.File

interface AlbumOperation {
    fun onSongLike(position: Int, songId: Int, isFavourite: Boolean)
    fun onSongPlay(position: Int, recordId: String, files: MutableList<File>?)
    fun onPauseSong()
}