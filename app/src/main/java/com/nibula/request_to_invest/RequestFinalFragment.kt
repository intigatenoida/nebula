package com.nibula.request_to_invest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentRequestBuyFinalBinding
import com.nibula.databinding.FragmentRequestFinalBinding
import com.nibula.response.details.Response
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.TimestampTimeZoneConverter


class RequestFinalFragment : BaseFragment() {

    companion object {
        fun newInstance() = RequestFinalFragment()
    }

    lateinit var rootView: View
    lateinit var binding:FragmentRequestFinalBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::binding.isInitialized) {
            //binding = inflater.inflate(R.layout.fragment_request_final, null)
            binding=FragmentRequestFinalBinding.inflate(inflater,container,false)
            rootView=binding.root
            inti()
        }
        return rootView
    }

    private fun inti() {
        val detailsResponse = arguments?.getParcelable<Response?>(AppConstant.RECORD_DETAIL)

        //--> Setting values Need for UI

        //:: Shares
        val sharesCount = arguments?.getInt(AppConstant.SHARES_PURCHASED, 0) ?: 0
        binding.tvSharesCount.text = "$sharesCount"

        //:: Album image
        /* detailsResponse?.recordImage?.let {
             CommonUtils.loadImage(requireContext(), it, binding.ivAlbumImg, R.drawable.appicon)
         }
 */

        Glide
            .with(requireContext())
            .load( detailsResponse?.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.layoutImage.clImage)


        //:: Timer Remaining
        detailsResponse?.timeLeftToRelease?.let {
            if (it > 0) {
                TimestampTimeZoneConverter.setTimerTextView(it, binding.tvTimeRemain, null)
            }
        }

        //:: Artist Name
        detailsResponse?.artistName?.let {
            binding.tvArtistName.text = it

            val artist = CommonUtils.setSpannable(requireContext(),it, 0, it.length)

            // :: Bottom Text
            val value =
                requireActivity().getString(R.string.of_will_review_your_request).replace("#", it)
            binding.tvnote3.text = CommonUtils.setRedSpannableWithTextSizeSixteen(
                requireContext(),
                value,
                0,
                it.length
            )
        }

        //:: Album Title
        detailsResponse?.recordType?.let { type ->
            detailsResponse.recordTitle?.let { title ->
                binding.tvAlbum.text = "$title"
               /* binding.tvAlbum.text = CommonUtils.setSpannable(requireContext(),
                    binding.tvAlbum.text as String,
                    0,
                    binding.tvAlbum.text.split(":")[0].length + 1
                )*/
            }
        }

        //:: Release Date ( calculated from offering End Date according to User TimeZone)
        detailsResponse?.offeringEndDate?.let { endDate ->
            (getString(R.string.released_with_colon) + " " + TimestampTimeZoneConverter.convertToLocalDate(
                endDate,
                TimestampTimeZoneConverter.UTC_TIME
            )).also { binding.tvReleaseDate.text = it }
        }

        //:: Genere Type
        detailsResponse?.genreType?.let { genereType ->
            binding.tvTags.text = genereType
        }

        //:: Share to Earn Cashback
        binding.tvShare.setOnClickListener {
            detailsResponse?.id?.let {
                navigate.onClickShare(
                    it,
                    detailsResponse.recordTitle!!,
                    detailsResponse.recordStatusTypeId!!,
                    detailsResponse.offeringEndDate!!
                )
            }
        }

        //:: Shares requested
        binding.tvnote1.text =
            requireActivity().getString(R.string.note_rti).replace("#", "$sharesCount")
        binding.tvnote1.text = CommonUtils.setRedSpannableWithTextSizeTwenty(
            requireContext(),
            binding.tvnote1.text.toString(),
            binding.tvnote1.text.toString().lastIndexOf(" "),
            binding.tvnote1.text.toString().length
        )

        //:: Shares of single / album
        detailsResponse?.recordType?.let { type ->
            detailsResponse.recordTitle?.let { albumtitle ->
                binding.tvnote2.text = with(requireActivity().getString(R.string.shares_of_the)) {
                    replace("#", "$type")
                }.run { replace("@", "$albumtitle") }
//                requireActivity().getString(R.string.shares_of_the).apply {
//                        replace("#", "$type")
//                        replace("@", "$albumtitle")
//                    }
                binding.tvnote2.text = CommonUtils.setRedSpannableWithTextSizeSixteen(
                    requireContext(),
                    binding.tvnote2.text.toString(),
                    binding.tvnote2.text.toString().indexOf(":") + 1,
                    binding.tvnote2.text.toString().length
                )
            }
        }

        //:: Done
        binding.tvdone.setOnClickListener {
            navigate.manualBack(3)
        }
    }

    override fun onBackPressed(): Boolean {
        return true
    }

}