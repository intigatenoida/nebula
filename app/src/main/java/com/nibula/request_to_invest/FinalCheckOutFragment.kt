package com.nibula.request_to_invest

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentFinalCheckoutBinding
import com.nibula.request_to_invest.viewmodal.PaymentInfoViewModel
import com.nibula.utils.CommonUtils


class FinalCheckOutFragment : BaseFragment() {
    lateinit var rootView: View
    lateinit var paymentInfoViewModel: PaymentInfoViewModel
    lateinit var binding:FragmentFinalCheckoutBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_final_checkout, container, false)
            binding= FragmentFinalCheckoutBinding.inflate(inflater,container,false)
            rootView=binding.root
            paymentInfoViewModel = activity?.run {
                ViewModelProvider(this).get(PaymentInfoViewModel::class.java)
            } ?: throw Exception("Invalid Activity")

            paymentInfoViewModel.recordDetailResponse?.value.let {

                it?.recordTitle.let {
                    binding.tvRecordTitle.text=it
                }
                it?.artistName.let {
                    binding.tvArtistName.text=it
                }
                it?.recordImage.let {
                    Glide
                        .with(requireActivity())
                        .load(it)
                        .placeholder(R.drawable.nebula_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.IMMEDIATE)
                        .into(binding.clImage2)
                }

                it?.artistName.let {
                    Glide
                        .with(requireActivity())
                        .load(it)
                        .placeholder(R.drawable.nebula_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.IMMEDIATE)
                        .into(binding.ivInvestor)
                }

                it?.recordTitle.let {
                    binding.investedByUserName.text=it
                }

                it?.artistName.let {
                    binding.investedBy.text=it
                }

                binding.tvValueTotal.text = "${it?.sharesAvailable?.toInt()}/${it?.totalNoOfShares?.toInt()}"
                binding.tvValueTotalTokens.text = "${it?.sharesAvailable?.toInt()}/${it?.totalNoOfShares?.toInt()}"
                binding.tvValuePricePerToken.text =
                    "${it?.royalityOwnershipPercentagePerToken.toString() + "%"}"


                binding.tvValue.text = "${it?.currencyType ?: "USD"}${
                    CommonUtils.trimDecimalToTwoPlaces(
                        it?.valuePerShareInDollars ?: 0.00
                    )
                }"
            }
        }
        return rootView
    }

}