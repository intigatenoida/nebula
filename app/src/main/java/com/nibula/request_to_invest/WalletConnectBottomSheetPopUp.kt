package com.nibula.request_to_invest
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.WalletConnectBottomSheetBinding
import com.nibula.utils.interfaces.DialogFragmentClicks

import org.bouncycastle.asn1.x9.X9ECParameters
import org.bouncycastle.crypto.AsymmetricCipherKeyPair
import org.bouncycastle.crypto.ec.CustomNamedCurves
import org.bouncycastle.crypto.generators.ECKeyPairGenerator
import org.bouncycastle.crypto.params.ECDomainParameters
import org.bouncycastle.crypto.params.ECKeyGenerationParameters
import org.bouncycastle.crypto.params.ECPrivateKeyParameters
import org.bouncycastle.crypto.params.ECPublicKeyParameters
import org.bouncycastle.math.ec.ECPoint
import org.bouncycastle.util.encoders.Hex
import java.math.BigInteger
import java.security.SecureRandom

class WalletConnectBottomSheetPopUp() : BaseBottomSheetFragment() {

    constructor(
        dgFrgListener: DialogFragmentClicks
    ) : this() {
        this.dgFrgListener = dgFrgListener
    }

    lateinit var dgFrgListener: DialogFragmentClicks
    lateinit var binding:WalletConnectBottomSheetBinding

    private lateinit var rootView: View
/*
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme1)

    }
*/

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
           // rootView = inflater.inflate(R.layout.wallet_connect_bottom_sheet, container, false)
            binding= WalletConnectBottomSheetBinding.inflate(inflater,container,false)
            rootView=binding.root

            binding.tvWalletConnect.setOnClickListener {

                val b = Bundle()
                b.putInt("type",1)
                dgFrgListener.onPositiveButtonClick(this, b,dialog!!)
            }

            binding.ivClose.setOnClickListener {

                val b = Bundle()
                dgFrgListener.onNegativeButtonClick(this, b,dialog!!)
            }

            binding.clCreateWalletAddress.setOnClickListener {
                val b = Bundle()
                b.putInt("type",2)
                dgFrgListener.onPositiveButtonClick(this, b,dialog!!)
                //val walletAddress = generateWalletAddress()
            }

        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }
    fun generateWalletAddress(): String {

        binding.keyGroup.visibility=View.VISIBLE
        val secureRandom = SecureRandom()
        val keyPairGenerator = ECKeyPairGenerator()
        val curveName = "secp256k1"
        val curveParams: X9ECParameters = CustomNamedCurves.getByName(curveName)
        // Create an instance of ECDomainParameters from the curve parameters
        val domainParams = ECDomainParameters(curveParams.curve, curveParams.g, curveParams.n, curveParams.h)
        val keyGenParams = ECKeyGenerationParameters(domainParams, secureRandom)
        keyPairGenerator.init(keyGenParams)
        val keyPair: AsymmetricCipherKeyPair = keyPairGenerator.generateKeyPair()
        val privateKeyParams: ECPrivateKeyParameters = keyPair.private as ECPrivateKeyParameters
        val publicKeyParams: ECPublicKeyParameters = keyPair.public as ECPublicKeyParameters
        val publicKey: ECPoint = publicKeyParams.q
        val privateKey: BigInteger = privateKeyParams.d
        val publicKeyBytes = publicKey.getEncoded(false)
        val privateKeyBytes = privateKey.toByteArray()
        val walletAddressBytes = publicKeyBytes.copyOfRange(1, publicKeyBytes.size)
        val walletAddress = Hex.toHexString(walletAddressBytes)
        val privatekeyHex = Hex.toHexString(privateKeyBytes)
        binding.tvWalletAddressValue.text=walletAddress
        binding.tvPrivateKeyValue.text=privatekeyHex.toString()
        return walletAddress
    }

}