package com.nibula.request_to_invest
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.utils.interfaces.DialogFragmentClicks

class BottomSheetBackUpWallet() : BaseBottomSheetFragment() {

    constructor(
        dgFrgListener: DialogFragmentClicks
    ) : this() {
        this.dgFrgListener = dgFrgListener
    }

    lateinit var dgFrgListener: DialogFragmentClicks

    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.bottom_sheet_wallet_backup, container, false)

        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }
}