package com.nibula.request_to_invest.strippayment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.GsonBuilder
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.FragmentStripPaymentBinding
import com.nibula.utils.AppConstant
import com.stripe.android.ApiResultCallback
import com.stripe.android.PaymentIntentResult
import com.stripe.android.Stripe
import com.stripe.android.model.ConfirmPaymentIntentParams
import com.stripe.android.model.StripeIntent


class StripPaymentFragment : BaseFragment() {

    companion object {
        fun newInstance() = StripPaymentFragment()
    }

    private lateinit var rootView: View
    private lateinit var helper: StripPaymentHelper

    private var paymentClientSecret: String? = ""
    private lateinit var stripe: Stripe
    lateinit var binding:FragmentStripPaymentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_strip_payment, container, false)
            binding= FragmentStripPaymentBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
        }
        return rootView
    }

    private fun initUI() {
        helper = StripPaymentHelper(this)

        paymentClientSecret = arguments?.getString(AppConstant.CLIENT_SECRETE) ?: ""

        binding.layoutTbMain.tvTitle.text = getString(R.string.card_payment)
        binding.layoutTbMain.imgHome.setOnClickListener {
            hideProcessDialog()
            val sendIntent = Intent()
            sendIntent.putExtra("isUserCanceled",true)
            requireActivity().setResult(Activity.RESULT_CANCELED,sendIntent)
            requireActivity().finish()
        }

        binding.tvPay.setOnClickListener {
            if (paymentClientSecret.isNullOrEmpty()) {
                showToast(
                    requireContext(),
                    getString(R.string.message_stripe_error),
                    CustomToast.ToastType.FAILED
                )
                hideProcessDialog()
                return@setOnClickListener
            }

            val params = binding.cardInputWidget.paymentMethodCreateParams
            if (params == null) {
                showToast(
                    requireContext(),
                    getString(R.string.message_credit_card),
                    CustomToast.ToastType.FAILED
                )
                hideProcessDialog()
                return@setOnClickListener
            }
            showProcessDialog()
            val confirmParams =
                ConfirmPaymentIntentParams.createWithPaymentMethodCreateParams(
                    params,
                    paymentClientSecret!!
                )
            stripe = Stripe(
                requireContext(),
                AppConstant.LIVE_PUBLISH_KEY_STRIP
            )
            stripe.confirmPayment(this, confirmParams)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        stripe.onPaymentResult(requestCode, data,
            object : ApiResultCallback<PaymentIntentResult> {
                override fun onError(e: Exception) {
                    val sendIntent = Intent()
                    sendIntent.putExtra("errorMessage", e.message)
                    sendIntent.putExtra("isUserCanceled",false)
                    println("onError: ${e.message}")
                    hideProcessDialog()
                    requireActivity().setResult(Activity.RESULT_CANCELED, sendIntent)
                    requireActivity().finish()
                }

                override fun onSuccess(result: PaymentIntentResult) {
                    val paymentIntent = result.intent
                    val status = paymentIntent.status
                    val gson =
                        GsonBuilder().create()
                    //println("gson ${gson.toJson(paymentIntent)}")
                    val sendIntent = Intent()
                    sendIntent.putExtra("isUserCanceled",false)
                    if (status == StripeIntent.Status.Succeeded) {
                        // Payment completed successfully
                        sendIntent.putExtra("success", gson.toJson(paymentIntent))
                        requireActivity().setResult(Activity.RESULT_OK, sendIntent)
                    } else if (status == StripeIntent.Status.RequiresPaymentMethod) {
                        //sendIntent.putExtra("error", paymentIntent.lastPaymentError?.message)
                        sendIntent.putExtra("error", gson.toJson(paymentIntent))
                        requireActivity().setResult(Activity.RESULT_CANCELED, sendIntent)
                    }
                    hideProcessDialog()
                    requireActivity().finish()
                }

            })
    }
}