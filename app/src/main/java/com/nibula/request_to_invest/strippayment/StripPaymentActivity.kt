package com.nibula.request_to_invest.strippayment

import android.os.Bundle
import com.nibula.R
import com.nibula.base.BaseActivity

class StripPaymentActivity : BaseActivity() {

    lateinit var fg:StripPaymentFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_strip_payment)

        fg = StripPaymentFragment.newInstance()
        fg.arguments = intent?.extras
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fg)
                .commitNow()
        }
    }
}