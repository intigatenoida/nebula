package com.nibula.request_to_invest

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemSampleAlbumBinding
import com.nibula.response.details.File
import com.nibula.utils.CommonUtils

class SampleAlbumAdapter(val context: Context, private val listener: OnSongClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var binding:ItemSampleAlbumBinding
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

     interface OnSongClickListener {
        fun onSongClick(id: Int, fromTop: Boolean)
    }
    var list: MutableList<File> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
         binding = ItemSampleAlbumBinding.inflate(inflater, parent, false)


        val holder = ViewHolder(binding.root)
        return holder
        /*return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_sample_album,
                parent,
                false
            )
        )*/
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = list[position]
        binding.tvPosition.text = (position + 1).toString()
        binding.tvTitle.text = data.songTitle
        binding.songDuration.text = "${
            data.musicDurationInSeconds?.let {
                CommonUtils.convertToMinute(
                    it
                )
            }
        }"
        binding.container.setOnClickListener {
            listener.onSongClick(position, false)
        }

        if (data.isPlaying) {
            binding.ivPlay.setBackgroundResource(R.drawable.pause)

            /*  holder.itemView.tvTitle.setCompoundDrawablesWithIntrinsicBounds(
                  R.drawable.ic_pause_new,
                  0,
                  0,
                  0
              )*/
        } else {
            /* holder.itemView.tvTitle.setCompoundDrawablesWithIntrinsicBounds(
                 R.drawable.ic_play_new,
                 0,
                 0,
                 0
             )*/

            binding.ivPlay.setBackgroundResource(R.drawable.play)

        }


    }

    fun updateList(files: MutableList<File>) {
        list = files
        notifyDataSetChanged()
    }

    fun getSongList(): MutableList<File> {
        return list
    }

    fun updateSongStatus(id: Int, playing: Boolean) {
        if (id == -1) return
        for (i in 0 until list.size) {
            if (list[i].id == id) {
                list[i].isPlaying = playing

            } else {
                list[i].isPlaying = false
            }
        }
        notifyDataSetChanged()
    }
}