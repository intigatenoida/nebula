package com.nibula.request_to_invest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nibula.R
import com.nibula.base.BaseFragment


class OfferingFragmentNew : BaseFragment() {
    private lateinit var rootView: View

    companion object {
        fun newInstance(bundle: Bundle?): OfferingFragmentNew {
            val fg = OfferingFragmentNew()
            fg.arguments = bundle
            return fg
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            rootView = inflater.inflate(R.layout.fragment_new_offering, container, false)
            showProcessDialog()
        }
        return rootView
    }

}