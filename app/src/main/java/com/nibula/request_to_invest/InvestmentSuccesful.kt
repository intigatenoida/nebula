package com.nibula.request_to_invest

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseApplication
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentRequestBuyFinalBinding
import com.nibula.response.details.Response
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.TimestampTimeZoneConverter

class InvestmentSuccesful : BaseFragment() {

    companion object {
        fun newInstance(bundle: Bundle): InvestmentSuccesful {
            val fg = InvestmentSuccesful()
            fg.arguments = bundle
            return fg
        }
    }

    private var isPaymentSuccess: Boolean? = false
    private var record: Response? = null
    private var totalAmount: Double? = 0.0
    private var sharesPurchased: Int? = 0
    private var recordId: String = ""
    var currentPageCount = 1
    var investmentId: Int = 0

    lateinit var binding:FragmentRequestBuyFinalBinding
    lateinit var rootView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_request_buy_final, container, false)
            binding= FragmentRequestBuyFinalBinding.inflate(inflater,container,false)
            rootView=binding.root
            init()
        }
        return rootView
    }

    private fun init() {
        getArgumentData()
        val data = record
        val misPaymentSuccess = isPaymentSuccess
        if (data == null) {
            return
        }
        recordId = data.id ?: ""
        //:: success / Failed Status
        if (misPaymentSuccess == true) {
            trigerTokenPurchasedSuccess()
            binding.tvRequestMade.text = getString(R.string.success_excel)
            binding.tvRequestMade.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(
                    requireActivity(),
                    R.drawable.combined_shape
                ), null, null, null
            )
            if (data.recordStatusTypeId == AppConstant.released) {
                //:: purchased album
                data.recordType?.let { type ->
                    binding.ll.text = getString(R.string.album_purchased_new).replace("#", type)
                }
                binding.ll.textSize = 16f
                binding.playButton.visibility = View.VISIBLE
                binding.dessscriptionTv.visibility = View.GONE
                binding.tvPaidamount.visibility = View.VISIBLE
                binding.tvNumberOfToken.visibility = View.VISIBLE
                binding.tvnote.visibility = View.VISIBLE

                //:: paid amount
                val value = "You paid: $${CommonUtils.setTwoDecimalPlaces(totalAmount ?: 0.0)}"
                // number of token
                val numberOfToken = "Tokens Purchased: $sharesPurchased"

                binding.tvPaidamount.text =
                    CommonUtils.setSpannable(
                        requireContext(),
                        value,
                        value.indexOf(":") + 1,
                        value.length
                    )

                binding.tvNumberOfToken.text =
                    CommonUtils.setSpannable(
                        requireContext(),
                        numberOfToken,
                        numberOfToken.indexOf(":") + 1,
                        numberOfToken.length
                    )

            } else {
                //:: purchased shares
                binding.llShares.visibility = View.VISIBLE
                binding.llShares.text = "$sharesPurchased"
                binding.ll.text = getString(R.string.shares_purchased)
                binding.ll.textSize = 21f
                binding.playButton.visibility = View.GONE

                binding.dessscriptionTv.visibility = View.VISIBLE
                binding.tvPaidamount.visibility = View.GONE
                binding.tvnote.visibility = View.GONE

                val textValue = getString(R.string.shares_purchased_note).run {
                    replace("@", data.recordType ?: "")
                }.run {
                    replace("#", data.recordTitle ?: "")
                }.run { replace("%", "$sharesPurchased") }


                binding.dessscriptionTv.text = CommonUtils.setSpannableRed(
                    context,
                    data.recordTitle,
                    data.recordType,
                    sharesPurchased ?: 0,
                    textValue
                )
            }

        } else {
            trigerTokenPurchasedFailed()
            navigate.manualBack(2)

            /*    rootView.dessscriptionTv.visibility = View.GONE
                rootView.tvRequestMade.text = getString(R.string.failed)
                rootView.tvRequestMade.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireActivity(),
                        R.drawable.ic_cross_red
                    ), null, null, null
                )
                if (data.recordStatusTypeId == AppConstant.released) {
                    //:: purchased album
                    data.recordType?.let { type ->
                        rootView.ll.text =
                            getString(R.string.album_purchased_failed_new).replace("#", "Song")
                    }

                } else {
                    //:: purchased shares
                    rootView.ll.text = getString(R.string.shares_purchased_failed)
                }*/
        }


        //:: Album image
        /* data.recordImage?.let {
             CommonUtils.loadImage(requireContext(), it, rootView.ivAlbumImg, R.drawable.appicon)
         }
 */


        Glide
            .with(requireContext())
            .load(data.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.layoutImage.clImage)


        //:: Timer Remaining
        data.timeLeftToRelease?.let {
            binding.tvTimeRemain.visibility = View.GONE
            if (data.recordStatusTypeId == AppConstant.released) {
                binding.tvTimeRemain.text = getString(R.string.released)
                binding.tvTimeRemain.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.colorAccent
                    )
                )
                binding.tvTimeRemain.setBackgroundResource(R.drawable.button_rounded_white_2dp)
            } else if (it > 0) {
                TimestampTimeZoneConverter.setTimerTextView(it, binding.tvTimeRemain, null)
                binding.tvTimeRemain.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
                binding.tvTimeRemain.setBackgroundResource(R.drawable.red_back_2_dp)
            } else {
                binding.tvTimeRemain.visibility = View.GONE
            }
        }

        //:: Artist Name
        data.artistName?.let {
            binding.tvArtistName.text = it

        }

        //:: Album Title
        data.recordType?.let { type ->
            data.recordTitle?.let { title ->
                binding.recordTitle.text = CommonUtils.redWhiteSpannable(
                    requireContext(), "",
                    title
                )
            }
        }

        //:: Release Date ( calculated from offering End Date according to User TimeZone)
        data.offeringEndDate?.let { endDate ->
            binding.tvReleaseDate.text =
                "${getString(R.string.release)} ${
                    TimestampTimeZoneConverter.convertToLocalDate(
                        endDate,
                        TimestampTimeZoneConverter.UTC_TIME
                    )
                }"
        }

        //:: Genere Type
        data.genreType.let { genereType ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.tvTags.text = Html.fromHtml(genereType, Html.FROM_HTML_MODE_LEGACY)
            } else {
                binding.tvTags.text = Html.fromHtml(genereType)
            }
        }

        //:: Share to Earn Cashback
        binding.tvShare.setOnClickListener {
            data.id?.let {
                navigate.onClickShare(
                    it,
                    data.recordTitle!!,
                    data.recordStatusTypeId!!,
                    data.offeringEndDate!!
                )
            }
        }


        //:: Done
        binding.tvdone.setOnClickListener {
//            if (data.showInvestorsToPublic == 1 && data.recordStatusTypeId == AppConstant.initialoffering && isPaymentSuccess == true) {
//                val privacyDialog = PrivacyChangBottomSheet(
//                    data.id,
//                    data.showInvestorsToPublic,
//                    investmentId,
//                    object : DialogFragmentClicks {
//                        override fun onPositiveButtonClick(
//                            obj: Any,
//                            bundle: Bundle?,
//                            dialog: Dialog
//                        ) {
//                            dialog.dismiss()
//                            navigate.manualBack(3)
//                        }
//
//                        override fun onNegativeButtonClick(1
//                            obj: Any,
//                            bundle: Bundle?,
//                            dialog: Dialog
//                        ) {
//
//                        }
//
//                    })
//                privacyDialog.show(
//                    requireActivity().supportFragmentManager,
//                    "PrivacyChangeDialog"
//                )
//            } else {
            if (isPaymentSuccess == true)
                navigate.manualBack(3)
            else
                navigate.manualBack(2)
//            }
        }
    }

    private fun getArgumentData() {
        isPaymentSuccess = arguments?.getBoolean("paymentSuccess", false)
        record = arguments?.getParcelable("record")
        totalAmount = arguments?.getDouble("totalAmount")
        sharesPurchased = arguments?.getInt("shares")
        investmentId = arguments?.getInt("investmentId") ?: 0
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    fun trigerTokenPurchasedFailed() {
        val firebaseAnalytics = BaseApplication.firebaseAnalytics
        val bundle = Bundle()
        bundle.putString("purchase_amount", totalAmount.toString())
        bundle.putString("drop_name", record!!.recordTitle)
        bundle.putString("payment_method", "STRIPE")
        bundle.putString("currency_type", record?.currencyType)
        firebaseAnalytics.logEvent("token_purchase_failed", bundle)
    }

    fun trigerTokenPurchasedSuccess() {
        val firebaseAnalytics = BaseApplication.firebaseAnalytics
        val bundle = Bundle()
        bundle.putString("purchase_amount", totalAmount.toString())
        bundle.putString("drop_name", record?.recordTitle)
        bundle.putString("currency_type", record?.currencyType)
        bundle.putString("payment_method","STRIPE")
        firebaseAnalytics.logEvent("token_purchase_success", bundle)
    }
}