package com.nibula.request_to_invest

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.response.details.AlbumDetailResponse


class NewOfferingFragment : BaseFragment() {
    private lateinit var rootView: View
    var artistId = ""
    private lateinit var ivUserAvatar: ImageView
    private var EXPAND_AVATAR_SIZE: Float = 0F
    private var COLLAPSE_IMAGE_SIZE: Float = 0F
    private var horizontalToolbarMargin: Float = 0F
    private lateinit var toolbar: Toolbar
    private lateinit var appBarLayout: AppBarLayout
    private var cashCollapseState: Pair<Int, Int>? = null
    private lateinit var background: FrameLayout
    private var animationStartPointY: Float = 0F
    private var collapseAnimationChangeWeight: Float = 0F
    private var isCalculated = false
    private var verticalToolbarAvatarMargin = 0F
    lateinit var albumdetailresponse: AlbumDetailResponse
    lateinit var costBreakdownResponse: CostBreakdownResponse.Response
    var recordTitleStr = ""
    var recordId = ""
    lateinit var invisibleTextViewWorkAround: TextView
    var sharesAvailable = 0

    companion object {
        fun newInstance(bundle: Bundle?): NewOfferingFragment {
            val fg = NewOfferingFragment()
            fg.arguments = bundle
            return fg
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_offering_new, container, false)
        // collapseViewInit()
        showProcessDialog()
        Handler().postDelayed(
            Runnable {
                //rootView.main_root.visibility=View.VISIBLE
                hideProcessDialog()
            },
            10000
        )
        return rootView
    }

/*    private fun collapseViewInit() {
        EXPAND_AVATAR_SIZE = resources.getDimension(R.dimen._150dp)
        COLLAPSE_IMAGE_SIZE = resources.getDimension(R.dimen._55dp)
        horizontalToolbarMargin = resources.getDimension(R.dimen._34dp)
        appBarLayout = rootView.findViewById(R.id.app_bar_layout)
        toolbar = rootView.findViewById(R.id.anim_toolbar)
        ivUserAvatar = rootView.findViewById(R.id.iv1)
        background = rootView.findViewById(R.id.bg)
        invisibleTextViewWorkAround = rootView.findViewById(R.id.tv_workaround)
        (toolbar.height - COLLAPSE_IMAGE_SIZE) * 2
        appBarLayout.addOnOffsetChangedListener(
            AppBarLayout.OnOffsetChangedListener { appBarLayout, i ->
                if (isCalculated.not()) {
                    animationStartPointY =
                        Math.abs((appBarLayout.height - (EXPAND_AVATAR_SIZE + horizontalToolbarMargin)) / appBarLayout.totalScrollRange)
                    collapseAnimationChangeWeight = 1 / (1 - animationStartPointY)
                    verticalToolbarAvatarMargin = (toolbar.height - COLLAPSE_IMAGE_SIZE) * 2
                    isCalculated = true
                }
                updateViews(Math.abs(i / appBarLayout.totalScrollRange.toFloat()))
            })
    }

    private fun updateViews(offset: Float) {
        when (offset) {
            in 0F..0.15F -> {
                ivUserAvatar.alpha = 1f
            }
        }
        */
    /** collapse - expand switch*//*
        when {
            offset < RecordDetailFragmentNew.SWITCH_BOUND -> Pair(
                RecordDetailFragmentNew.TO_EXPANDED,
                cashCollapseState?.second ?: RecordDetailFragmentNew.WAIT_FOR_SWITCH
            )
            else -> Pair(RecordDetailFragmentNew.TO_COLLAPSED, cashCollapseState?.second ?: RecordDetailFragmentNew.WAIT_FOR_SWITCH)
        }.apply {
            when {
                cashCollapseState != null && cashCollapseState != this -> {
                    when (first) {
                        RecordDetailFragmentNew.TO_EXPANDED -> {
                            *//*set avatar on start position (center of parent frame layout)*//*
                            ivUserAvatar.translationX = 0F
                            toolbar.visibility = View.VISIBLE
                            toolbar.setBackgroundResource(0)

                        }
                        RecordDetailFragmentNew.TO_COLLAPSED -> background.apply {
                            alpha = 0F
                            toolbar.visibility = View.VISIBLE
                            rootView.tvReleasedDate.visibility = View.GONE
                            animate().setDuration(250).alpha(1.0F)
                            toolbar.setBackgroundResource(R.drawable.back_background)
                        }
                    }
                    cashCollapseState = Pair(first, RecordDetailFragmentNew.SWITCHED)
                }
                else -> {
                    cashCollapseState = Pair(first, RecordDetailFragmentNew.WAIT_FOR_SWITCH)
                }
            }
            ivUserAvatar.apply {
                when {
                    offset > animationStartPointY -> {
                        val avatarCollapseAnimateOffset =
                            (offset - animationStartPointY) * collapseAnimationChangeWeight
                        val avatarSize =
                            EXPAND_AVATAR_SIZE - (EXPAND_AVATAR_SIZE - COLLAPSE_IMAGE_SIZE) * avatarCollapseAnimateOffset
                        this.layoutParams.also {
                            it.height = Math.round(avatarSize)
                            it.width = Math.round(avatarSize)
                        }
                        invisibleTextViewWorkAround.setTextSize(
                            TypedValue.COMPLEX_UNIT_PX,
                            offset
                        )
                        this.translationX =
                            ((appBarLayout.width - horizontalToolbarMargin - avatarSize) / 2) * avatarCollapseAnimateOffset
                        this.translationY =
                            ((toolbar.height - verticalToolbarAvatarMargin - avatarSize) / 2) * avatarCollapseAnimateOffset
                    }
                    else -> this.layoutParams.also {
                        if (it.height != EXPAND_AVATAR_SIZE.toInt()) {
                            it.height = EXPAND_AVATAR_SIZE.toInt()
                            it.width = EXPAND_AVATAR_SIZE.toInt()
                            this.layoutParams = it
                        }
                        translationX = 0f
                    }
                }
            }
        }
    }*/
}