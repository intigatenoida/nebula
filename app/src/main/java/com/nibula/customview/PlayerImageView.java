package com.nibula.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.nibula.dashboard.DashboardActivity;

import org.jetbrains.annotations.NotNull;

public class PlayerImageView extends AppCompatImageView {



    public interface onVisibilityChangeListener {
        void onImageVisibilityChange(int visibility);
    }

    private onVisibilityChangeListener listener;

    public void setVisibilityChangeListener(onVisibilityChangeListener listener) {
        this.listener = listener;
    }


    public PlayerImageView(@NonNull Context context) {
        super(context);
    }

    public PlayerImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PlayerImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (listener != null) {
            listener.onImageVisibilityChange(visibility);
        }
    }
}
