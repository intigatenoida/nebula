package com.nibula.customview

data class ChatData(
    var message: String = "",
    var url: String = "",
    var recordId: String = "",
    var type: Int = 1

)