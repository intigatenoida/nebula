package com.nibula.customview

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import com.nibula.R

object CustomToast {

    fun showToast(
        context: Context?,
        message: String?,
        type: ToastType = ToastType.FAILED,
        duration: Int = Toast.LENGTH_SHORT
    ) {
        val toast: Toast = Toast.makeText(
            context, message, if (type == ToastType.FAILED) {
                Toast.LENGTH_LONG
            } else duration
        )
        toast.setGravity(Gravity.BOTTOM, toast.xOffset / 3, toast.yOffset / 3)
        val textView = TextView(context)
        val typeface = ResourcesCompat.getFont(context!!, R.font.pt_sans)
        textView.setTextColor(Color.WHITE)
        textView.typeface = typeface
        textView.textSize = 16f
        textView.setPadding(25, 5, 25, 5)
        textView.text = message
        textView.gravity = Gravity.CENTER
        toast.setGravity(Gravity.CENTER, 0, 175)
        toast.view = textView

        when (type) {
            ToastType.FAILED -> {
                textView.setBackgroundResource(R.drawable.toast_back)
            }
            ToastType.SUCCESS -> {
                textView.setBackgroundResource(R.drawable.toast_green)
            }
            ToastType.NETWORK -> {
                textView.setBackgroundResource(R.drawable.toast_back)
            }
        }

        toast.show()
    }

    enum class ToastType {
        SUCCESS, FAILED, NETWORK
    }
}