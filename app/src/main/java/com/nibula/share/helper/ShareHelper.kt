package com.nibula.share.helper

import android.text.TextUtils
import android.view.View.GONE
import android.view.View.VISIBLE
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.request.SharingRequest
import com.nibula.request.VerifyEmailPhoneRequest
import com.nibula.request.adminapproval.AdminApprovalRequest
import com.nibula.request_to_buy.navigate
import com.nibula.response.BaseResponse
import com.nibula.response.carificationResponse.PhoneEmailVerificationResponse
import com.nibula.response.defaultListResponse.DefaultUserResponse
import com.nibula.response.externalShareResponse.ExternalShareResponse
import com.nibula.response.searchShareResponse.SearchShareResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.share.ui.ShareFragment
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils

class ShareHelper(val fg: ShareFragment, val navigate: navigate) : BaseHelperFragment() {

    var nfNextPage: Int = 1
    var nfMaxPage: Int = 0


    fun searchShare(s: String, isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog, false)
            return
        }

        if (!fg.binding.spShare.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        val helper =
            ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.searchSharing(nfNextPage, s)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<SearchShareResponse>() {

            override fun onSuccess(any: SearchShareResponse?, message: String?) {
                /*Load More*/
                nfMaxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = nfNextPage < nfMaxPage ||
                        (any?.totalRecords ?: 0) > (nfNextPage * 10)

                if (any?.users != null &&
                    any.responseStatus == 1
                ) {
                    fg.updateAdapterSearch(any.users, isProgressDialog)
                }
                dismiss(isProgressDialog, true)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog, true)
            }

        }))

    }

    fun getDefaultSearchList(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog, false)
            return
        }

        if (!fg.binding.spShare.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        val helper =
            ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getDefaultSearch()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<DefaultUserResponse>() {

            override fun onSuccess(any: DefaultUserResponse?, message: String?) {

                if (any?.defaultSharing?.responseStatus ?: 0 == 1) {
                    fg.updateDefaultData(
                        users = any!!.defaultSharing.responseCollection,
                        shouldRefresh = isProgressDialog
                    )
                }
                dismiss(isProgressDialog, false)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog, false)
            }

        }))

    }


    private fun dismiss(isProgressDialog: Boolean, fromSearchApi: Boolean) {
        if (fg.binding.spShare.isRefreshing) {
            fg.binding.spShare.isRefreshing = false
        } else if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        if (fromSearchApi) {
            fg.isLoading = false
            noData()
        }
    }

    fun noData() {
        if (fg.adapter.userList.isEmpty()) {
            fg.binding.gdNoData.visibility = VISIBLE
            fg.binding.gdSearchUser.visibility = GONE
        } else {
            fg.binding.gdNoData.visibility = GONE
            fg.binding.gdSearchUser.visibility = VISIBLE
        }
    }

    fun saveSharing(
        recordId: String,
        shareTo: String,
        title: String,
        isRelease: Int,
        date: String
    ) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }

        fg.showProcessDialog()

        val request = SharingRequest(recordId, shareTo,title,isRelease,date)
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.saveSharing(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BaseResponse>() {

            override fun onSuccess(any: BaseResponse?, message: String?) {
                dismiss(true, false)
                if (any?.responseStatus == 1) {
                    CustomToast.showToast(
                        fg.requireContext(), any.responseMessage ?: "",
                        CustomToast.ToastType.SUCCESS
                    )
                    navigate.manualBack()
                    getDefaultSearchList(true)
                }
            }

            override fun onError(error: RetroError?) {
                dismiss(true, false)
            }

        }))

    }

    fun getExternalShareUrl(recordId: String) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }

        fg.showProcessDialog()

        val request = AdminApprovalRequest(recordId)
        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getExternalShareUrl(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<ExternalShareResponse>() {

            override fun onSuccess(any: ExternalShareResponse?, message: String?) {
                dismiss(true, false)
                if (any?.responseStatus == 1 && any.response.url.isNullOrEmpty().not()) {
                    CommonUtils.sharePlainText(fg.requireActivity(), any.response.url)
                }
            }

            override fun onError(error: RetroError?) {
                dismiss(true, false)
            }

        }))

    }

    fun checkEmailPhoneVerificartion(actionType: Int) {
//
//        if (!fg.isOnline()) {
//            fg.showToast(
//                fg.requireActivity(),
//                fg.getString(R.string.internet_connectivity),
//                CustomToast.ToastType.NETWORK
//            )
//            dismiss(true, false)
//            return
//        }
//        fg.showProcessDialog()
//
//        val helper = ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
//        var observable = helper.getVerificationStatus(actionType)
//        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
//            CallbackWrapper<PhoneEmailVerificationResponse>() {
//            override fun onSuccess(any: PhoneEmailVerificationResponse?, message: String?) {
////                    fg.openVerificationDialog(fromResend)
//                if (any?.responseStatus != null &&
//                    any.responseStatus == 1 &&
//                    any.response?.email?.status ?: 0 == AppConstant.ALREDY_VERIFIED
////                    && any.response?.phone?.status ?: 0 == AppConstant.ALREDY_VERIFIED
//                ) {// success
//                    fg.doShare()
//                } else {// failed
//                    fg.openVerificationDialog(
//                        any?.response?.email?.status ?: 0,
//                        any?.response?.phone?.status ?: 0,
//                        any?.response?.email?.registeredValue ?: "",
//                        any?.response?.phone?.registeredValue ?: ""
//                    )
//                }
//                dismiss(true, false)
//            }
//
//            override fun onError(error: RetroError?) {
//                dismiss(true, false)
//            }
//
//        }))
    }

    fun resendOtp(actionType: Int) {
//
//        if (!fg.isOnline()) {
//            fg.showToast(
//                fg.requireActivity(),
//                fg.getString(R.string.internet_connectivity),
//                CustomToast.ToastType.NETWORK
//            )
//            dismiss(true, false)
//            return
//        }
//        fg.showProcessDialog()
//
//        val helper = ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
//        var observable = helper.getVerificationStatus(actionType)
//        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
//            CallbackWrapper<PhoneEmailVerificationResponse>() {
//            override fun onSuccess(any: PhoneEmailVerificationResponse?, message: String?) {
////                    fg.openVerificationDialog(fromResend)
//                if (any?.responseStatus != null &&
//                    any.responseStatus == 1
//                ) {// success
//
//                    fg.updateResendStatus(
//                        any?.response?.email?.status ?: 0,
//                        any?.response?.phone?.status ?: 0
//                    )
//                }
//                dismiss(true, false)
//            }
//
//            override fun onError(error: RetroError?) {
//                dismiss(true, false)
//            }
//
//        }))
    }

    fun verifyPhoneEmail(actionType: Int, emailOtp: String, phoneOtp: String) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(true, false)
            return
        }
        fg.showProcessDialog()
        val request = VerifyEmailPhoneRequest()
        request.action = actionType
        request.emailConfirmationCode = if (!TextUtils.isEmpty(emailOtp)) {
            emailOtp.toInt()
        } else {
            "0".toInt()
        }
        request.phoneConfirmationCode = if (!TextUtils.isEmpty(phoneOtp)) {
            phoneOtp.toInt()
        } else {
            "0".toInt()
        }


        val helper = ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.verifyEmailPhone(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<PhoneEmailVerificationResponse>() {
            override fun onSuccess(any: PhoneEmailVerificationResponse?, message: String?) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (actionType == AppConstant.ACTION_TYPE_PHONE && any.response.phone.status == AppConstant.ALREDY_VERIFIED) {
                        fg.doShare()
                    } else if (actionType == AppConstant.ACTION_TYPE_EMAIL &&
                        any.response.email.status == AppConstant.ALREDY_VERIFIED
                    ) {
                        fg.doShare()
                    } else if (actionType == AppConstant.ACTION_TYPE_EMAIL_AND_PHONE &&
                        any.response.email.status == AppConstant.ALREDY_VERIFIED &&
                        any.response.phone.status == AppConstant.ALREDY_VERIFIED
                    ) {
                        fg.doShare()
                    } else {// failed
                        fg.showVerificationErrorMsgBoth(
                            any.response.email.status,
                            any.response.phone.status
                        )

                    }
                } else {
                    fg.showVerificationErrorMsgBoth(
                        any?.response?.email?.status ?: 0,
                        any?.response?.phone?.status ?: 0
                    )
//                    CustomToast.showToast(
//                        fg.requireContext(),
//                        any?.responseMessage,
//                        CustomToast.ToastType.FAILED
//                    )
                }
                dismiss(true, false)
            }

            override fun onError(error: RetroError?) {
                dismiss(true, false)
            }

        }))
    }
}