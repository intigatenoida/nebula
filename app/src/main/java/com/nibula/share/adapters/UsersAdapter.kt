package com.nibula.share.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.nibula.R
import com.nibula.databinding.ItemShareUserBinding
import com.nibula.response.defaultListResponse.ResponseCollection



class UsersAdapter() : RecyclerView.Adapter<UsersAdapter.UserViewHolder>(), Filterable {

    class UserViewHolder(val view: View) : RecyclerView.ViewHolder(view)

     val userList = ArrayList<ResponseCollection>()

    private lateinit var context: Context
    private var layoutInflater: LayoutInflater? = null

    private lateinit var listener: UserCommunicator
    lateinit var binding:ItemShareUserBinding


    constructor(context: Context, listener: UserCommunicator) : this() {
        this.context = context
        this.listener = listener
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        //val view = layoutInflater!!.inflate(R.layout.item_share_user, parent, false)
        val inflater = LayoutInflater.from(parent.context)
         binding = ItemShareUserBinding.inflate(inflater, parent, false)


        val holder = UserViewHolder(binding.root)
        return holder
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

        val data = userList[position]
        binding.tvUserName.text = data.name
        binding.tvSubName.text = data.userName
//        CommonUtils.loadImage(context, data.userImage, binding.img_user)
        val url:String?= data.userImage
        if (url.isNullOrEmpty()) {
            binding.imgUser.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_record_user_place_holder))
        } else {
            val tempUrl = url.trim()
            Log.e("*******************", tempUrl)
            val options: RequestOptions = RequestOptions()
                .placeholder(R.drawable.ic_record_user_place_holder)
                .error(R.drawable.ic_record_user_place_holder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)


            Glide.with(context).load(tempUrl).apply(options).into( binding.imgUser)
        }
        binding.chUser.setOnCheckedChangeListener(null)
        binding.chUser.isChecked = data.isChecked
        binding.chUser.setOnCheckedChangeListener { compoundButton, b ->
            data.isChecked = b
            listener.changeButton(user = data)
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val searchText = constraint.toString()
                if (searchText.isEmpty()) {

                } else {

                }
                val filterResults = FilterResults()
                //filterResults.values =
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {

            }
        }
    }

    fun clearAndAddData(users: List<ResponseCollection>) {
        userList.clear()
        userList.addAll(users)
        notifyDataSetChanged()
    }

    fun addData(users: List<ResponseCollection>) {
        userList.addAll(users)
        notifyDataSetChanged()
    }

    interface UserCommunicator {
        fun changeButton(user: ResponseCollection)
    }


}