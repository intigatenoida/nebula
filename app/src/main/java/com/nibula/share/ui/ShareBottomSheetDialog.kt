package com.nibula.share.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.LayoutShareBottmSheatBinding
import com.nibula.utils.interfaces.DialogFragmentClicks

class ShareBottomSheetDialog(private val dgFrgListener: DialogFragmentClicks): BaseBottomSheetFragment() {

    private lateinit var rootView: View
    lateinit var binding:LayoutShareBottmSheatBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            binding= LayoutShareBottmSheatBinding.inflate(inflater,container,false)
            rootView=binding.root
            //rootView = inflater.inflate(R.layout.layout_share_bottm_sheat, container, false)
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }


    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.tvMobile.setOnClickListener {
            val b = Bundle()
            dgFrgListener.onPositiveButtonClick(this, b, dialog!!)
        }

        binding.tvDesktop.setOnClickListener {
            val b = Bundle()
            dgFrgListener.onNegativeButtonClick(this, b, dialog!!)
        }
    }

}