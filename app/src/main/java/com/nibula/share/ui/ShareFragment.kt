package com.nibula.share.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.bottomsheets.VerifyEmailBottomSheet
import com.nibula.bottomsheets.VerifyEmailPhoneBottomSheet
import com.nibula.customview.CustomToast
import com.nibula.databinding.FragmentShareBinding
import com.nibula.response.defaultListResponse.ResponseCollection
import com.nibula.response.searchShareResponse.User
import com.nibula.share.adapters.UsersAdapter
import com.nibula.share.helper.ShareHelper
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils


class ShareFragment : BaseFragment(), UsersAdapter.UserCommunicator {

    lateinit var rootView: View
    var isDataAvailable = false
    lateinit var adapter: UsersAdapter
    private lateinit var helper: ShareHelper
    private val selectedUserList = ArrayList<ResponseCollection>()
    private val defaultUserList = ArrayList<ResponseCollection>()
    var isLoading = true
    var verifyEmailBottomSheet: VerifyEmailBottomSheet? = null
    var verifyEmailPhoneBottomSheet: VerifyEmailPhoneBottomSheet? = null
    var isExternalUrlClick = false
    var shareActionType = 3
    lateinit var binding:FragmentShareBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //binding = inflater.inflate(R.layout.fragment_share, container, false)
            binding= FragmentShareBinding.inflate(inflater,container,false)
            rootView=binding.root
            intiUI()
            initHelper()
        }
        return rootView
    }

    private fun initHelper() {
        helper = ShareHelper(this, navigate)
        helper.getDefaultSearchList(true)
//        helper.searchShare("", true)
    }

    private fun intiUI() {
        binding.tvSend.text = getString(R.string.send)
        binding.tvFixedCancel.setOnClickListener {

            adapter.clearAndAddData(defaultUserList)
            helper.noData()
            binding.edSearchUser.setText("")
        }
        binding.edSearchUser.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val s = binding.edSearchUser.text
                if (s != null &&
                    s.toString().trim().isNotEmpty()
                ) {
                    helper.nfMaxPage = 1
                    hideKeyboard()
                    helper.searchShare(s.toString(), true)
//                    doLocalSearch(s.toString())
/*                    if (s.toString().equals("nd", true)) {
                        binding.gd_no_data.visibility = VISIBLE
                        binding.gd_search_user.visibility = GONE
                    } else {
                        binding.gd_no_data.visibility = GONE
                        binding.gd_search_user.visibility = VISIBLE
                    }
                } else {
                    binding.gd_no_data.visibility = GONE
                    binding.gd_search_user.visibility = VISIBLE*/
                }

            }
            true
        }
        /* binding.ed_search_user.addTextChangedListener(object : TextWatcher {
             override fun afterTextChanged(s: Editable?) {
                 if (s != null &&
                     s.toString().isNotEmpty()
                 ) {
                     binding.img_close.visibility = VISIBLE
                 } else {
                     binding.img_close.visibility = GONE
                 }
             }

             override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

             }

             override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

             }

         })*/
        binding.ivBack.setOnClickListener {
            navigate.manualBack()
            //requireActivity().supportFragmentManager.popBackStack()
        }

        binding.imgClose.setOnClickListener {
            binding.edSearchUser.setText("")
            binding.gdNoData.visibility = GONE
            binding.gdSearchUser.visibility = VISIBLE
        }

        initTopHeader()

        binding.spShare.setOnRefreshListener {
            if (binding.edSearchUser.text.toString().trim().isNotEmpty()) {
                helper.nfNextPage = 1
                helper.searchShare(binding.edSearchUser.text.toString().trim(), false)
            } else {
                helper.getDefaultSearchList(false)
            }
        }
        binding.tvSend.setOnClickListener {
            if (selectedUserList.isNotEmpty()) {
                isExternalUrlClick = false
//                helper.checkEmailPhoneVerificartion(AppConstant.ACTION_TYPE_EMAIL)
                doShare()
            } else {
                CustomToast.showToast(
                    requireContext(),
                    "Please select the User to Share",
                    CustomToast.ToastType.FAILED
                )
            }
        }

        binding.tvMoreOptions.setOnClickListener {
            isExternalUrlClick = true
//            helper.checkEmailPhoneVerificartion(AppConstant.ACTION_TYPE_EMAIL)
            doShare()
        }
        binding.tvNoDataMoreOptions.setOnClickListener {
            isExternalUrlClick = true
//            helper.checkEmailPhoneVerificartion(AppConstant.ACTION_TYPE_EMAIL)
            doShare()
        }
        setAdapter()
    }

    fun getExternalUrl() {
        helper.getExternalShareUrl(arguments?.getString(AppConstant.ID, "") ?: "")
    }

    fun hitShareApi() {

        if (selectedUserList.isNotEmpty()) {
            var shareTo = ""
            for (user in selectedUserList) {
                shareTo += if (shareTo.isEmpty()) {
                    user.userId
                } else {
                    "," + user.userId
                }
            }
            helper.saveSharing(
                arguments?.getString(AppConstant.ID, "") ?: "",
                shareTo,
                arguments?.getString(AppConstant.RECORD_TITLE, "") ?: "",
                arguments?.getInt(AppConstant.RECORD_STATUS, 0) ?: 1,
                arguments?.getString(AppConstant.RELEASED_DATE, "") ?: ""
            )
        }
    }

    private fun doLocalSearch(str: String) {
        if (defaultUserList.isEmpty()) {
            helper.searchShare(str, true)
        } else {
            val tempUser = ArrayList<ResponseCollection>()
            for (user in defaultUserList) {
                if (user.userName.contains(str) || user.name.contains(str)) {
                    tempUser.add(user)
                }
            }
            if (tempUser.isEmpty()) {
                helper.searchShare(str, true)
            } else {
                adapter.clearAndAddData(tempUser)
            }

        }

    }

    private fun initTopHeader() {
        binding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }
    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvUsers.layoutManager = layoutManager

        adapter = UsersAdapter(requireContext(), this)
        binding.rvUsers.adapter = adapter

        binding.rvUsers.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (binding.edSearchUser.toString().trim().isNotEmpty()) {
                    val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                    val totalItem = layoutManager.itemCount
                    val threshold = 5
                    if (!isLoading &&
                        isDataAvailable &&
                        totalItem < (lastVisibleItem + threshold)
                    ) {
                        helper.searchShare(binding.edSearchUser.toString().trim(), false)
                    }
                }
            }
        })

    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun changeButton(user: ResponseCollection) {
        if (selectedUserList.contains(user)) {
            selectedUserList.remove(user)
        } else {
            selectedUserList.add(user)
            if (!defaultUserList.contains(user)) {
                defaultUserList.add(user)
            }
        }
        if (selectedUserList.size > 1) {
            binding.tvSend.text = getString(R.string.send_separatly)
        } else {
            binding.tvSend.text = getString(R.string.send)
        }
    }

    fun updateAdapterSearch(
        users: List<User>,
        clearOldData: Boolean
    ) {
        val temp = ArrayList<ResponseCollection>()
        for (user in users) {
            temp.add(
                ResponseCollection(
                    "",
                    user.name,
                    user.userId,
                    user.userImage,
                    user.UserName,
                    false
                )
            )
        }
        if (binding.spShare.isRefreshing || clearOldData) {
            adapter.clearAndAddData(temp)
        } else {
            adapter.addData(temp)
        }
    }

    fun updateDefaultData(
        users: List<ResponseCollection>,
        shouldRefresh: Boolean
    ) {
        defaultUserList.clear()
        selectedUserList.clear()
        defaultUserList.addAll(users)
        if (binding.spShare.isRefreshing || shouldRefresh) {
            adapter.clearAndAddData(defaultUserList)
        } else {
            adapter.addData(defaultUserList)
        }
    }

    override fun onResume() {
        super.onResume()
//        if (binding.imgNotificationCount.visibility!=View.VISIBLE){
        CommonUtils.setNotificationDot(requireContext(), binding.header.imgNotificationCount)
//        }
    }

    fun openVerificationDialog(
        emailCode: Int,
        phoneCode: Int,
        email: String,
        phoneNumber: String
    ) {
//        if (emailCode == AppConstant.CODE_SEND && phoneCode == AppConstant.CODE_SEND) {
//            shareActionType = AppConstant.ACTION_TYPE_EMAIL_AND_PHONE
//            openEmailPhoneVerificationDialog(email,phoneNumber)
//        } else if (emailCode == AppConstant.ALREDY_VERIFIED && phoneCode == AppConstant.CODE_SEND) {
//            shareActionType = AppConstant.ACTION_TYPE_PHONE
//            openVerifyEmailDialog(shareActionType,phoneNumber)
//        } else if (emailCode == AppConstant.CODE_SEND && phoneCode == AppConstant.ALREDY_VERIFIED) {
        shareActionType = AppConstant.ACTION_TYPE_EMAIL
        openVerifyEmailDialog(shareActionType, email)
//        }

    }

    private fun openEmailPhoneVerificationDialog(email: String, phoneNumber: String) {
        if (verifyEmailPhoneBottomSheet == null) {
            verifyEmailPhoneBottomSheet = VerifyEmailPhoneBottomSheet(email, phoneNumber, object :
                VerifyEmailPhoneBottomSheet.OnVerifyOtpListener {
                override fun onVerifyOtp(emailOtp: String, phoneOtp: String) {
                    helper.verifyPhoneEmail(
                        shareActionType,
                        emailOtp,
                        phoneOtp
                    )
                }

                override fun resendOTPEmail() {
                    helper.resendOtp(AppConstant.ACTION_TYPE_EMAIL)
                }

                override fun resendOTPPhone() {
                    helper.resendOtp(AppConstant.ACTION_TYPE_PHONE)
                }

                override fun onDismissCalled() {
                    verifyEmailPhoneBottomSheet = null
                }

            })
            verifyEmailPhoneBottomSheet?.show(childFragmentManager, "verifyEmailPhoneBottomSheet")
        } else {

        }
    }

    private fun openVerifyEmailDialog(actionType: Int, registerValue: String) {
        if (verifyEmailBottomSheet == null) {
            verifyEmailBottomSheet =
                VerifyEmailBottomSheet(
                    actionType, registerValue,
                    object : VerifyEmailBottomSheet.OnVerifyOtpListener {
                        override fun onVerifyOtp(emailOtp: String, phoneOtp: String) {
                            helper.verifyPhoneEmail(
                                shareActionType,
                                emailOtp,
                                phoneOtp
                            )
                        }

                        override fun resendOTP() {
                            helper.resendOtp(shareActionType)
                        }

                        override fun onDismissCalled() {
                            verifyEmailBottomSheet = null
                        }
                    })
            verifyEmailBottomSheet?.show(childFragmentManager, "verifyEmailBottomSheet")
        } else {

        }
    }


    fun showVerificationErrorMsgBoth(emailstatus: Int, phonestatus: Int) {
        if (emailstatus == AppConstant.ALREDY_VERIFIED && (phonestatus == AppConstant.CODE_SEND || phonestatus == AppConstant.INVALID_OTP)) {
            shareActionType = AppConstant.ACTION_TYPE_PHONE
        } else if ((emailstatus == AppConstant.CODE_SEND || emailstatus == AppConstant.INVALID_OTP) && phonestatus == AppConstant.ALREDY_VERIFIED) {
            shareActionType = AppConstant.ACTION_TYPE_EMAIL
        }
        if (verifyEmailPhoneBottomSheet != null) {
            verifyEmailPhoneBottomSheet?.showError(emailstatus, phonestatus)
        } else {
            verifyEmailBottomSheet?.showError()
        }
    }

    fun updateResendStatus(emailstatus: Int, phonestatus: Int) {
        if (verifyEmailPhoneBottomSheet != null) {
            verifyEmailPhoneBottomSheet?.restartTimer(emailstatus, phonestatus)
        } else {
            verifyEmailBottomSheet?.startCountDownTimer()
        }
    }

    fun doShare() {
        verifyEmailPhoneBottomSheet?.dismiss()
        verifyEmailBottomSheet?.dismiss()
        if (isExternalUrlClick) {
            getExternalUrl()
        } else {
            hitShareApi()
        }
    }
}