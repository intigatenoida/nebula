package com.nibula.dashboard

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.*
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.exoplayer2.PlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.DefaultTimeBar
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.google.android.exoplayer2.ui.TimeBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.base.room.NebulaDatabase
import com.nibula.base.room.PlaySongEntity
import com.nibula.customview.CustomToast
import com.nibula.customview.PlayerImageView
import com.nibula.dashboard.ConstantsNavTabs.NAV_TAB_ASSETS
import com.nibula.dashboard.ConstantsNavTabs.NAV_TAB_HOME
import com.nibula.dashboard.ConstantsNavTabs.NAV_TAB_MUSIC
import com.nibula.dashboard.ConstantsNavTabs.NAV_TAB_PROFILE
import com.nibula.dashboard.ConstantsNavTabs.TOP_SEARCH_TAB
import com.nibula.dashboard.ConstantsNavTabs.TOP_USER_ACTIVITY_TAB
import com.nibula.dashboard.ConstantsNavTabs.TOP_USER_HOW_TO_WORK
import com.nibula.dashboard.ConstantsNavTabs.TOP_USER_RECORD_BUY
import com.nibula.dashboard.ConstantsNavTabs.TOP_USER_RECORD_INVEST
import com.nibula.dashboard.ui.assets.WalletFragment
import com.nibula.dashboard.ui.home.HomeFragment
import com.nibula.dashboard.ui.music.MusicFragment
import com.nibula.databinding.ActivityDashboardBinding
import com.nibula.fcm.UpdateDeviceInfoReq
import com.nibula.request.SaveCryptoWalletAddress
import com.nibula.request.mp3request.mp3UrlRequest
import com.nibula.request_to_invest.BottomSheetCreateWalletAddress
import com.nibula.request_to_invest.OfferingFragment
import com.nibula.request_to_invest.SelectWalletBottomSheetFragment
import com.nibula.request_to_invest.WalletConnectBottomSheetPopUp
import com.nibula.response.BaseResponse
import com.nibula.response.cryptowalletresponse.SaveCryptoWalletResponse
import com.nibula.response.details.File
import com.nibula.response.mp3Response.mp3Response
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.upload_music.ui.uploadrecord.activity.UploadRecordActivityNew
import com.nibula.user.login.LoginActivity
import com.nibula.user.profile.ProfileFragment
import com.nibula.utils.*
import com.nibula.utils.RequestCode.LOGIN_REQUEST_CODE
import com.nibula.utils.RequestCode.LOGIN_REQUEST_CODE_ASSESTS
import com.nibula.utils.RequestCode.LOGIN_REQUEST_CODE_MUSIC
import com.nibula.utils.RequestCode.LOGIN_REQUEST_CODE_SHARE
import com.nibula.utils.RequestCode.PAYPAL_SUCCESS
import com.nibula.utils.interfaces.DialogFragmentClicks
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.theartofdev.edmodo.cropper.CropImage
import com.walletconnect.android.CoreClient
import com.walletconnect.sign.client.Sign
import com.walletconnect.sign.client.SignClient
import org.web3j.crypto.Credentials
import org.web3j.crypto.Hash
import org.web3j.protocol.Web3j
import org.web3j.protocol.http.HttpService
import org.web3j.utils.Numeric
import timber.log.Timber
import java.nio.charset.StandardCharsets
import java.util.*
import kotlin.math.abs

@Suppress("DEPRECATION", "DEPRECATED_IDENTITY_EQUALS")
class DashboardActivity : BaseActivity(), Player.EventListener,
    PlayerNotificationManager.NotificationListener, PlayerImageView.onVisibilityChangeListener {
    private var fromTopButton: Boolean = false
    private var isCurrentSongEnded: Boolean = false
    private var fragmentHome: HomeFragment? = null
    private var fragmentMusic: MusicFragment? = null
    private var fragmentWallet: WalletFragment? = null
    private var fragmentProfile: ProfileFragment? = null
    private lateinit var songIdsList: MutableList<File>
    var songIndex = 0
    lateinit var myMusic: MusicPlayer
    private var recordTitleStr = ""
    var recordTitle = ""
    var recordId = ""
    private var isReleased = 0
    var date = ""
    var playerCurrentTime = ""
    private var isRecordChanged = false
    private lateinit var sliderListener: SlidingUpPanelLayout.PanelSlideListener
    private var countDownTimer: CountDownTimer? = null
    private lateinit var localBroadcast: LocalBroadcastManager
    //Wallet Connect v2

    var apprSession: Sign.Model.ApprovedSession? = null
    var deeplinkPairingUri: String? = null
    var selectedSessionTopic: String? = null
    var account: String? = null
    lateinit var exo_progress: DefaultTimeBar
    lateinit var exo_position: AppCompatTextView
    lateinit var exo_duration: AppCompatTextView
    lateinit var exo_play: PlayerImageView
    lateinit var exo_pause: AppCompatImageView
    lateinit var img_next: AppCompatImageView
    lateinit var img_prev: AppCompatImageView
    lateinit var progressExpandPlayer: ProgressBar
    lateinit var cl_play_pause: ConstraintLayout

    companion object {
        lateinit var activityDashboardBinding: ActivityDashboardBinding

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_dashboard)
        activityDashboardBinding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(activityDashboardBinding.root)
        exo_progress = findViewById(R.id.exo_progress)
        exo_position = findViewById(R.id.exo_position)
        exo_duration = findViewById(R.id.exo_duration)
        exo_pause = findViewById(R.id.exo_pause)
        exo_play = findViewById(R.id.exo_play)
        img_next = findViewById(R.id.img_next)
        img_prev = findViewById(R.id.img_prev)
        cl_play_pause = findViewById(R.id.cl_play_pause)
        progressExpandPlayer = findViewById(R.id.progressExpandPlayer)
        setContentView(activityDashboardBinding.root)
        myMusic = MusicPlayer()
        mStacks = viewModel.getBackStackMap()
        mStacks[NAV_TAB_HOME] = Stack()
        mStacks[NAV_TAB_MUSIC] = Stack()
        mStacks[NAV_TAB_ASSETS] = Stack()
        mStacks[NAV_TAB_PROFILE] = Stack()
        //lyricsView = findViewById(R.id.lyricViewX)

        if (savedInstanceState == null) {
            selectedTab(NAV_TAB_HOME)
        } else {
            selectedTab(NAV_TAB_HOME, false)
        }
        activityDashboardBinding.navView.setOnNavigationItemSelectedListener(bottomNav)
        activityDashboardBinding.slidingLayout.panelState = SlidingUpPanelLayout.PanelState.HIDDEN
        initPlayerUi()
        addListenerToBottomSlider()
        localBroadcast = LocalBroadcastManager.getInstance(this@DashboardActivity)
        activityDashboardBinding.songDetailsBottomView.setOnClickListener {
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.EXPANDED
        }
        activityDashboardBinding.bottomLayoutPlay.cdBottom.setOnClickListener {
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.EXPANDED
        }


        activityDashboardBinding.imgDrop.imgDrop.setOnClickListener {
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        pushStrimingData()
        sendNotificationToken()
        handleIntent(intent)

        //Call the smart contract method
    }

    private val tripBroadcast = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                AppConstant.PAYMENT_SUCCCESS_BROADCAST_ACTION -> {
                    onBackPressed()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        localBroadcast.registerReceiver(
            tripBroadcast, IntentFilter(AppConstant.PAYMENT_SUCCCESS_BROADCAST_ACTION)
        )
    }

    private fun sendNotificationToken() {
        val mToken =
            PrefUtils.getValueFromPreference(this@DashboardActivity, PrefUtils.NOTIFICATION_TOKEN)
        if (mToken.isNotEmpty()) {
            sendRegistrationToServer(mToken)
        }
    }

    private fun sendRegistrationToServer(token: String?) {
        val updateDeviceInfoReq = UpdateDeviceInfoReq()
        updateDeviceInfoReq.deviceNotificationID = token!!
        val helper = ApiClient.getClientMusic(this).create(ApiAuthHelper::class.java)
        helper.saveDeviceInfo(updateDeviceInfoReq)
            .enqueue(object : CallBackManager<BaseResponse>() {
                override fun onSuccess(any: BaseResponse?, message: String) {

                }

                override fun onFailure(message: String) {

                }

                override fun onError(error: RetroError) {

                }
            })
    }

    private fun addListenerToBottomSlider() {

        sliderListener = object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View?, slideOffset: Float) {

//              if (slideOffset > 0.50) {
                //songDetailsBottomView.alpha = 1 - slideOffset
                activityDashboardBinding.songDetailsBottomView.alpha = 1 - slideOffset
//                }
                if (slideOffset > 0.90) {
                    if (activityDashboardBinding.songDetailsBottomView.visibility == View.VISIBLE) activityDashboardBinding.songDetailsBottomView.visibility =
                        View.GONE
                } else {
                    if (activityDashboardBinding.songDetailsBottomView.visibility == View.GONE) activityDashboardBinding.songDetailsBottomView.visibility =
                        View.VISIBLE
                }
            }

            override fun onPanelStateChanged(
                panel: View?,
                previousState: SlidingUpPanelLayout.PanelState?,
                newState: SlidingUpPanelLayout.PanelState?
            ) {
                if (newState == SlidingUpPanelLayout.PanelState.ANCHORED) {
                    activityDashboardBinding.slidingLayout.panelState =
                        SlidingUpPanelLayout.PanelState.EXPANDED
                }

            }

        }
        //sliding_layout.addPanelSlideListener(sliderListener)
        activityDashboardBinding.slidingLayout.addPanelSlideListener(sliderListener)

    }

    private fun openRecordDetail(intentString: String, isRecordID: Boolean) {
        val recordId = if (isRecordID) intentString else intentString.substring(
            intentString.lastIndexOf("=") + 1, intentString.length
        )
        val bundle = Bundle().apply {
            putString(AppConstant.RECORD_IMAGE_URL, "")
            putString(AppConstant.ID, recordId)
            putString(AppConstant.CONNECTED_WALLET_ACCOUNT, account)
            putString(AppConstant.SELECTED_SESSION_TOPIC, selectedSessionTopic)
            putString(AppConstant.WALLET_URI, deeplinkPairingUri)
        }

        //goToNewOffering(bundle)
        //gotorequestinvest(recordId)
    }

    private fun initPlayerUi() {

        exo_progress.addListener(object : TimeBar.OnScrubListener {
            /* override fun onScrubMove(timeBar: TimeBar?, position: Long) {
                 val duration = CommonUtils.convertPlayerDuration(
                     exo_position.text.toString(),
                     myMusic.getPlayerDuration()
                 )
                 exo_duration.text = if (duration == "00:00") {
                     duration
                 } else {
                     "-$duration"
                 }
                 CommonUtils.showLog(
                     "***********Timer",
                     "onScrubMove  ${myMusic.getPlayerDuration()}" + exo_duration.text.toString()
                 )
             }

             override fun onScrubStart(timeBar: TimeBar?, position: Long) {
              
             }

             override fun onScrubStop(timeBar: TimeBar?, position: Long, canceled: Boolean) {
                 val duration = CommonUtils.convertPlayerDuration(
                     exo_position.text.toString(),
                     myMusic.getPlayerDuration()
                 )
                 exo_duration.text = if (duration == "00:00") {
                     duration
                 } else {
                     "-$duration"
                 }
                 CommonUtils.showLog(
                     "***********Timer",
                     "onScrubStop " + exo_duration.text.toString()
                 )
             }*/

            override fun onScrubStart(timeBar: TimeBar, position: Long) {
            }

            override fun onScrubMove(timeBar: TimeBar, position: Long) {
                val duration = CommonUtils.convertPlayerDuration(
                    exo_position.text.toString(), myMusic.getPlayerDuration()
                )
                exo_duration.text = if (duration == "00:00") {
                    duration
                } else {
                    "-$duration"
                }
                CommonUtils.showLog(
                    "***********Timer",
                    "onScrubMove  ${myMusic.getPlayerDuration()}" + exo_duration.text.toString()
                )
            }

            override fun onScrubStop(timeBar: TimeBar, position: Long, canceled: Boolean) {
                val duration = CommonUtils.convertPlayerDuration(
                    exo_position.text.toString(), myMusic.getPlayerDuration()
                )
                exo_duration.text = if (duration == "00:00") {
                    duration
                } else {
                    "-$duration"
                }
                CommonUtils.showLog(
                    "***********Timer", "onScrubStop " + exo_duration.text.toString()
                )
            }
        })

        exo_position.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (playerCurrentTime != s.toString()) {
                    playerCurrentTime = s.toString()
                    val duration = CommonUtils.convertPlayerDuration(
                        s.toString(), myMusic.getPlayerDuration()
                    )

                    val currentPlayTime = abs(n = myMusic.getPlayerCurrentIndex() / 1000)

                    if (currentPlayTime > 1) {
                        PrefUtils.saveValueInPreference(
                            this@DashboardActivity,
                            PrefUtils.PLAYING_DURATION,
                            currentPlayTime.toInt() + 1
                        )
                    }
                    exo_duration.text = if (duration == "00:00") {
                        duration
                    } else {
                        "-$duration"
                    }
//                    CommonUtils.showLog(
//                        "***********Timer",
//                        "onTextChanged " + s.toString()
//                    )
                    CommonUtils.showLog(
                        "***********Timer", "onTextChanged " + exo_duration.text.toString()
                    )
                }

            }
        })

        exo_play.setVisibilityChangeListener(this)

        activityDashboardBinding.bottomLayoutPlay.imgPlay.setOnClickListener {

            if (myMusic.isPlaying()) {
                launchPlayPauseListener(false)
                myMusic.playPausePlayer(false)
                launchBroadcastListener(false)
                managePlayPauseIcon(false)
            } else {
                launchPlayPauseListener(true)

                if (isCurrentSongEnded) {
                    playCurrentSong()
                } else {
                    managePlayPauseIcon(true)
                    myMusic.playPausePlayer(true)
                    launchBroadcastListener(true)
                    managePlayPauseIcon(true)
                }
            }

        }

        exo_pause.setOnClickListener {
            myMusic.playPausePlayer(false)
            launchBroadcastListener(false)
            launchPlayPauseListener(false)
            managePlayPauseIcon(false)
        }

        exo_play.setOnClickListener {
            if (isCurrentSongEnded) {
                playCurrentSong()
            } else {
                myMusic.playPausePlayer(true)
                launchBroadcastListener(true)
                launchPlayPauseListener(true)

                managePlayPauseIcon(true)
            }
        }

        img_next.setOnClickListener {
            if (img_next.alpha == 1f) {
                playNextSong()
            }
        }

        img_prev.setOnClickListener {
            if (img_prev.alpha == 1f) {
                playPreviousSong()
            }

        }

        activityDashboardBinding.bottomLayoutPlay.imgClose.setOnClickListener {
            launchImageCrossListener()
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.HIDDEN
            if (myMusic.isPlaying()) {
//                myMusic.stopPLayer()
                myMusic.playPausePlayer(false)

            }

            activityDashboardBinding.songDetailsBottomView.visibility = View.GONE


            launchBroadcastListener(false)
            trackSongPlayTime()
            PrefUtils.saveValueInPreference(
                this@DashboardActivity, PrefUtils.SONG_ID, 0
            )
        }

        activityDashboardBinding.imgDrop.shareIcon.setOnClickListener {
            checkLoginAndShare()
        }
    }

    private fun checkLoginAndShare() = if (CommonUtils.isLogin(this@DashboardActivity)) {
        gotoShare()
    } else {
        val intent = Intent(this@DashboardActivity, LoginActivity::class.java)
        startActivityForResult(intent, LOGIN_REQUEST_CODE_SHARE)
    }

    private fun gotoShare() {
        if (activityDashboardBinding.slidingLayout.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        onClickShare(recordId, recordTitleStr, isReleased, date)
    }

    private fun playCurrentSong() {
        playMusic(songIdsList[songIndex].id ?: -1)
    }

    private fun updatePrevNextIcon() {

        if (songIdsList.size == 1) {
            img_next.alpha = 0.5f
            img_prev.alpha = 0.5f
        } else {
            if (songIndex == 0) {  // list first song
                img_prev.alpha = 0.5f
                img_next.alpha = 1f
            } else if (songIndex == songIdsList.size - 1) { // list last song
                img_prev.alpha = 1f
                img_next.alpha = 0.5f
            } else if (songIndex > 0 && songIndex < songIdsList.size) {
                img_prev.alpha = 1f
                img_next.alpha = 1f
            }
        }
    }


    private fun launchImageCrossListener() {
        val intent = Intent(AppConstant.IMAGE_CLOSE)
        intent.putExtra(AppConstant.ID, songIdsList[songIndex].id)
        LocalBroadcastManager.getInstance(this@DashboardActivity).sendBroadcast(intent)
    }

    private fun launchPlayPauseListener(isPlaying: Boolean) {
        val intent = Intent(AppConstant.PLAY_PAUSE)
        intent.putExtra("is_playing", isPlaying)
        LocalBroadcastManager.getInstance(this@DashboardActivity).sendBroadcast(intent)
    }
    private fun launchBroadcastListener(isPlaying: Boolean) {
        if (::songIdsList.isInitialized) {
            val intent = Intent(AppConstant.PLAYER_INTENT_FILTER)
            intent.putExtra(AppConstant.ID, songIdsList[songIndex].id)
            intent.putExtra(AppConstant.RECORD_ID, recordId)
            intent.putExtra(AppConstant.STATUS, isPlaying)
            intent.putExtra(AppConstant.FromTopButton, fromTopButton)
            intent.putExtra(AppConstant.SECRET_KEY, songIndex)
            LocalBroadcastManager.getInstance(this@DashboardActivity).sendBroadcast(intent)
        }
    }

    private val bottomNav = object : BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.navigation_home -> {
                    selectedTab(NAV_TAB_HOME)
                    return true
                }
                R.id.navigation_music -> {
                    if (PrefUtils.getValueFromPreference(this@DashboardActivity, PrefUtils.USER_ID)
                            .isEmpty()
                    ) {
                        val intent = Intent(this@DashboardActivity, LoginActivity::class.java)
                        intent.putExtra(AppConstant.ISFIRSTTIME, false)
                        startActivityForResult(intent, LOGIN_REQUEST_CODE_MUSIC)
                        return false
                    } else {
                        selectedTab(NAV_TAB_MUSIC)
                    }
                    return true
                }
                R.id.navigation_assets -> {
                    if (PrefUtils.getValueFromPreference(this@DashboardActivity, PrefUtils.USER_ID)
                            .isEmpty()
                    ) {
                        val intent = Intent(this@DashboardActivity, LoginActivity::class.java)
                        intent.putExtra(AppConstant.ISFIRSTTIME, false)
                        startActivityForResult(intent, LOGIN_REQUEST_CODE_ASSESTS)
                        return false
                    } else {
                        selectedTab(NAV_TAB_ASSETS)
                    }
                    return true
                }
                R.id.navigation_profile -> {
                    if (PrefUtils.getValueFromPreference(this@DashboardActivity, PrefUtils.USER_ID)
                            .isEmpty()
                    ) {
                        val intent = Intent(this@DashboardActivity, LoginActivity::class.java)
                        intent.putExtra(AppConstant.ISFIRSTTIME, false)
                        startActivityForResult(intent, LOGIN_REQUEST_CODE)
                        return false
                    } else {
                        selectedTab(NAV_TAB_PROFILE)
                    }
                    return true
                }
            }
            return false
        }

    }

    private fun selectedTab(tabId: String, isSameClickConsider: Boolean = true) {

        if (activityDashboardBinding.slidingLayout.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.COLLAPSED
        }

        var temp = isSameClickConsider

        if (viewModel.mCurrentTopTab.isNotEmpty()) {
            viewModel.mCurrentTopTab = ""
            temp = false
        }

        if (tabId == viewModel.mCurrentTab && temp) {
            if (mStacks.containsKey(tabId) && mStacks[tabId]!!.size > 1) {
                val firstFragment = mStacks[tabId]!!.firstElement()
                mStacks[tabId]!!.clear()
                pushFragments(tabId, firstFragment, true)
            }

            return
        }
        viewModel.mCurrentTab = tabId
        if (!(!mStacks.containsKey(tabId) || mStacks[tabId]!!.size !== 0)) {
            when (tabId) {
                NAV_TAB_HOME -> {
                    if (fragmentHome == null) {
                        fragmentHome = HomeFragment.newInstance()
                    }
                    pushFragments(tabId, fragmentHome!!, true)
                }
                NAV_TAB_MUSIC -> {
                    if (fragmentMusic == null) {
                        fragmentMusic = MusicFragment.newInstance()
                    }
                    pushFragments(tabId, fragmentMusic!!, true)
                }
                NAV_TAB_ASSETS -> {
                    if (fragmentWallet == null) {
                        fragmentWallet = WalletFragment.newInstance()
                    }
                    pushFragments(tabId, fragmentWallet!!, true)

                }
                NAV_TAB_PROFILE -> {
                    if (fragmentProfile == null) {
                        fragmentProfile = ProfileFragment.newInstance()
                    }
                    pushFragments(tabId, fragmentProfile!!, true)
                }
            }
        } else {

            pushFragments(tabId, mStacks[tabId]!!.lastElement(), false)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (activityDashboardBinding.slidingLayout.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.COLLAPSED
            return
        }

        if (viewModel.mCurrentTopTab == ConstantsNavTabs.NOTIFICATION_REDIRECTION) {
            viewModel.mCurrentTopTab = TOP_USER_ACTIVITY_TAB
            popFragments()
            return
        }

        if (viewModel.mCurrentTopTab == TOP_USER_HOW_TO_WORK || viewModel.mCurrentTopTab == TOP_USER_RECORD_BUY || viewModel.mCurrentTopTab == TOP_USER_RECORD_INVEST) {
            onClickNotification()
            return
        }

        if (viewModel.mCurrentTopTab == TOP_SEARCH_TAB || viewModel.mCurrentTopTab == TOP_USER_ACTIVITY_TAB) {
            selectedTab(viewModel.mCurrentTab, false)
            return
        }
/*

        if (viewModel.mCurrentTab == NAV_TAB_HOME) {
            super.onBackPressed()

        } else {

            viewModel.mCurrentTab = NAV_TAB_HOME
            selectedTab(viewModel.mCurrentTab, false)
        }
*/

        super.onBackPressed()

    }

    override fun onClickNotification() {
        if (isUserActivityOnTop()) return
//      viewModel.mCurrentTopTab = TOP_USER_ACTIVITY_TAB
        super.onClickNotification()
    }

    override fun onClickSearch() {
        if (isSearchOnTop()) return
//        viewModel.mCurrentTopTab = TOP_SEARCH_TAB
        super.onClickSearch()
    }

    override fun openTopArtist(id: String, userTypeId: Int) {
        if (id == PrefUtils.getValueFromPreference(this@DashboardActivity, PrefUtils.USER_ID)) {
            selectedTab(NAV_TAB_PROFILE)
            activityDashboardBinding.navView.selectedItemId = R.id.navigation_profile
            return
        }
        super.openTopArtist(id, userTypeId)
    }

    override fun uploadRecord() {

        val dialog = WalletConnectBottomSheetPopUp(object : DialogFragmentClicks {
            override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                dialog.dismiss()
                try {
                    val type = bundle?.getInt("type")
                    if (type == 1) {
                        initiateWalletConnectConnectionProcess()
                    } else {
                        openCreateWalletBottomSheetPopUp()
                    }

                } catch (e: ActivityNotFoundException) {
                    openSelectWalletPopUp()
                    e.printStackTrace()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                dialog.dismiss()
            }
        })
        dialog.show(supportFragmentManager, "UploadMobileDesktopDialog")
/*
        showProcessDialog()
*/
        /*  Handler().postDelayed(
              Runnable {
                  startActivity(Intent(this@DashboardActivity, UploadRecordActivityNew::class.java))
                  hideProcessDailog()
              },
              2000
          )*/
/*        var walletAddress = PrefUtils.getValueFromPreference(
            this@DashboardActivity,
            PrefUtils.CONNECTED_WALLET_ADDRESS
        )
        walletAddress.let {
            if (it.isEmpty()) {
                val dialog = WalletConnectBottomSheetPopUp(object : DialogFragmentClicks {
                    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                        dialog.dismiss()
                        try {
                            initiateWalletConnectConnectionProcess()
                        } catch (e: ActivityNotFoundException) {
                            openSelectWalletPopUp()
                            Toast.makeText(
                                this@DashboardActivity,
                                "No wallet found",
                                Toast.LENGTH_SHORT
                            ).show()
                            e.printStackTrace()
                        }

                    }

                    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                        dialog.dismiss()
                    }
                })
                dialog.show(supportFragmentManager, "UploadMobileDesktopDialog")
            } else {
                startActivity(Intent(this@DashboardActivity, UploadRecordActivityNew::class.java))
            }
        }*/
    }

    fun openSelectWalletPopUp() {
        val dialog = SelectWalletBottomSheetFragment(object : DialogFragmentClicks {
            override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                dialog.dismiss()

            }

            override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                dialog.dismiss()
            }
        })
        dialog.show(supportFragmentManager, "SelectWalletBottomSheet")
    }

/*
    private fun checkChat() {

        val token = Single.just("${PrefUtils.getValueFromPreference(this, PrefUtils.TOKEN) ?: ""}")

        var hubConnection =
            HubConnectionBuilder.create(ApiClient.CHAT_BASE_URL).withAccessTokenProvider(token)
                .build()
        hubConnection.start().blockingAwait()

        if (hubConnection.connectionState == HubConnectionState.CONNECTED) {
            CommonUtils.showLog("************chat", "chat connected")
        } else {
            CommonUtils.showLog("************chat", "chat disconnected")
        }
        hubConnection?.on(
            "ReceiveMessage",
            { chatData: ChatData? ->
                Log.e("Name--->", chatData?.message ?: "")
            },
            ChatData::class.java
        )
    }
*/

/*
    private fun checkAccessCode() {
        if (!isOnline()) {
            showToast(
                this@DashboardActivity,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }

        showProcessDialog()
        val helper =
            ApiClient.getClientAuth(this@DashboardActivity).create(ApiAuthHelper::class.java)
        val call = helper.getAccessCode()
        call.enqueue(object : CallBackManager<UserAccessCodeResponse>() {
            override fun onSuccess(any: UserAccessCodeResponse?, message: String) {
                hideProcessDailog()
                startActivity(Intent(this@DashboardActivity, UserAccessActivity::class.java).apply {
                    putExtra(AppConstant.ID, any?.response?.accessCode?.toString() ?: "")
                })
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                showToast(
                    this@DashboardActivity,
                    message,
                    CustomToast.ToastType.FAILED
                )
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }
        })
    }
*/

    override fun changeTopTab(changeTab: String) {
        viewModel.mCurrentTopTab = changeTab
        super.changeTopTab(changeTab)
    }

    fun initiateWalletConnectConnectionProcess() {
        // for proposer to create inactive pairing
        val pairing = CoreClient.Pairing.create()
        deeplinkPairingUri = pairing!!.uri

        val dappDelegate = object : SignClient.DappDelegate {
            override fun onSessionApproved(approvedSession: Sign.Model.ApprovedSession) {

                apprSession = approvedSession
                selectedSessionTopic = approvedSession.topic
                account = approvedSession.accounts[0]



                generatingSignature(selectedSessionTopic.toString())
                PrefUtils.saveValueInPreference(
                    this@DashboardActivity,
                    PrefUtils.CONNECTED_WALLET_ADDRESS,
                    selectedSessionTopic!!
                )
                saveCryptoWalletAddress(approvedSession.topic)
                // Triggered when dapp receives the session approval from wallet
                Timber.d("Session was approved by the wallet")
                Timber.tag("Session_Topic")
                    .d("Approved session's topic is: %s", approvedSession.topic)
            }

            override fun onSessionRejected(rejectedSession: Sign.Model.RejectedSession) {
                // Triggered when Dapp receives the session rejection from wallet
                Timber.tag("Session_Rejected").d("The wallet rejected the session")
            }

            override fun onSessionUpdate(updatedSession: Sign.Model.UpdatedSession) {
                // Triggered when Dapp receives the session update from wallet
                Timber.tag("Session_Updated").d(updatedSession.toString())

            }

            override fun onSessionExtend(session: Sign.Model.Session) {
                // Triggered when Dapp receives the session extend from wallet
                Timber.tag("Session_Extended").d(session.toString())

            }

            override fun onSessionEvent(sessionEvent: Sign.Model.SessionEvent) {
                // Triggered when the peer emits events that match the list of events agreed upon session settlement
                Timber.tag("Received_Session_Event").d(sessionEvent.toString())


            }

            override fun onSessionDelete(deletedSession: Sign.Model.DeletedSession) {
                // Triggered when Dapp receives the session delete from wallet
                Timber.tag("Session_Deleted").d(deletedSession.toString())

            }

            override fun onSessionRequestResponse(response: Sign.Model.SessionRequestResponse) {
                // Triggered when Dapp receives the session request response from wallet
                Timber.tag("Received_Session_Request_Response").d(response.toString())
            }

            override fun onConnectionStateChange(state: Sign.Model.ConnectionState) {
                //Triggered whenever the connection state is changed
                Timber.tag("Connection_State_Changed").d(state.toString())

            }

            override fun onError(error: Sign.Model.Error) {
                // Triggered whenever there is an issue inside the SDK
                Timber.tag("Error_In_SignClient_SDK").e(error.toString())

            }
        }

        SignClient.setDappDelegate(dappDelegate)

        val chains = listOf("eip155:1", "eip155:137")
        val methods = listOf(
            "eth_sendTransaction",
            "eth_signTransaction",
            "eth_sign",
            "personal_sign",
            "eth_signTypedData",
            "get_balance"
        )
        val events = listOf("accountsChanged", "chainChanged", "connect", "disconnect")
        val namespaces = mapOf("eip155" to Sign.Model.Namespace.Proposal(chains, methods, events))

        // for proposer to create a session
        val connectParams = Sign.Params.Connect(namespaces, null, null, pairing)

        SignClient.connect(connectParams, onSuccess = {

            /*Session proposal was being sent successfully over pairing topic*/
            Timber.tag("SignClient_Connect_Proposal_Send_Success")
                .d("The sign client connection proposal was successfully sent to the wallet")
        }, onError = { error -> /*callback for error while sending session proposal*/
            Timber.tag("CONNECTION_ERROR").e(error.throwable.stackTraceToString())
        })

        startActivity(Intent(Intent.ACTION_VIEW, deeplinkPairingUri!!.toUri()))

    }

    fun openCreateWalletBottomSheetPopUp() {

        val dialog = BottomSheetCreateWalletAddress(object : DialogFragmentClicks {
            override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                dialog.dismiss()

            }

            override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                dialog.dismiss()
            }
        })
        dialog.show(supportFragmentManager, "BottomSheetCreateWalletAccount")
    }

    @Deprecated("Deprecated in Java")
    override fun onTracksChanged(
        trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray
    ) {
        super.onTracksChanged(trackGroups, trackSelections)
        if (isRecordChanged) {
            val currentIndex = myMusic.getPlayerCurrentWindowIndex()
            CommonUtils.showLog("***********", "currentIndex $currentIndex")
            if (songIndex != currentIndex && currentIndex < songIdsList.size) {
                exo_pause.performClick()
                songIndex = currentIndex
                playMusic(songIdsList[songIndex].id ?: -1)
            }
            isRecordChanged = false
            isRecordChanged = false
        }
    }


    @Deprecated("Deprecated in Java")
    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        super.onPlayerStateChanged(playWhenReady, playbackState)

        when (playbackState) {
            Player.STATE_ENDED -> {

                isCurrentSongEnded = true
               /* launchBroadcastListener(false)
                managePlayPauseIcon(false)*/
            }
            Player.STATE_READY -> {
                isCurrentSongEnded = false
              /*  launchBroadcastListener(true)
                managePlayPauseIcon(true)*/
            }
            Player.STATE_BUFFERING -> {

            }

            Player.STATE_IDLE -> {

            }
        }

    }

    fun managePlayPauseIcon(isPlaying: Boolean) {

        if (isPlaying) {
            exo_pause.visibility = View.VISIBLE
            exo_play.visibility = View.GONE
            activityDashboardBinding.bottomLayoutPlay.imgPlay.setImageDrawable(
                ContextCompat.getDrawable(
                    this, R.drawable.ic_pause
                )
            )
        } else {
            exo_pause.visibility = View.GONE
            exo_play.visibility = View.VISIBLE
            activityDashboardBinding.bottomLayoutPlay.imgPlay.setImageDrawable(
                ContextCompat.getDrawable(
                    this, R.drawable.ic_play
                )
            )
        }
    }

    private fun playNextSong() {
        try {
            if (::songIdsList.isInitialized && songIdsList.size > songIndex + 1) {
                songIndex++
                playMusic(songIdsList[songIndex].id ?: -1)
            }
        } catch (e: ArrayIndexOutOfBoundsException) {
            songIndex--
        }
    }

    private fun playPreviousSong() {
        try {
            if (::songIdsList.isInitialized && songIndex > 0) {
                songIndex--
                playMusic(songIdsList[songIndex].id ?: -1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            selectedTab(NAV_TAB_PROFILE)
            activityDashboardBinding.navView.selectedItemId = R.id.navigation_profile
            viewModel.refresh(true)
        }

        if (resultCode == RequestCode.PAYPAL_WEB_VIEW_RESULT_CODE) {
            onBackPressed()
        }

        if (requestCode == PAYPAL_SUCCESS && resultCode == Activity.RESULT_OK) {
            selectedTab(NAV_TAB_HOME)
            activityDashboardBinding.navView.selectedItemId = R.id.navigation_home
            viewModel.refresh(true)
        }

        if (requestCode == LOGIN_REQUEST_CODE_ASSESTS && resultCode == Activity.RESULT_OK) {
            selectedTab(NAV_TAB_ASSETS)
            activityDashboardBinding.navView.selectedItemId = R.id.navigation_assets
            viewModel.refresh(true)
        }

        if (requestCode == LOGIN_REQUEST_CODE_MUSIC && resultCode == Activity.RESULT_OK) {
            selectedTab(NAV_TAB_MUSIC)
            activityDashboardBinding.navView.selectedItemId = R.id.navigation_music
            viewModel.refresh(true)
        }
        if (requestCode == LOGIN_REQUEST_CODE_SHARE && resultCode == Activity.RESULT_OK) {
            gotoShare()
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK && viewModel.mCurrentTab == NAV_TAB_PROFILE) {
            val fragment = getProfileTopFragment()
            fragment?.let {
                if (it is ProfileFragment) {
                    it.onActivityResult(requestCode, resultCode, data)
                }
            }
        }

    }


    override fun openDashboard() {
        selectedTab(NAV_TAB_HOME)
        activityDashboardBinding.navView.selectedItemId = R.id.navigation_home
    }

    override fun openWalletScreen() {
        selectedTab(NAV_TAB_ASSETS)
        activityDashboardBinding.navView.selectedItemId = R.id.navigation_assets
    }

    override fun openMusicScreen() {
        selectedTab(NAV_TAB_MUSIC)
        activityDashboardBinding.navView.selectedItemId = R.id.navigation_music
    }

    override fun pauseMusic() {

        pauseMusicService()
    }

    override fun openUploadSong() {

        showProcessDialog()
        Handler().postDelayed(
            {
                val intent = Intent(this@DashboardActivity, UploadRecordActivityNew::class.java)
                startActivity(intent)
                hideProcessDailog()
            }, 2000
        )
    }

    override fun playSong(
        songIdList: MutableList<File>,
        selectedIndex: Int,
        fromTop: Boolean,
        recordTitleStr: String,
        recordId: String,
        isReleased: Int,
        date: String,
        isRecordChanged: Boolean
    ) {
        playMusic(
            songIdList,
            selectedIndex,
            fromTop,
            recordTitleStr,
            recordId,
            isReleased,
            date,
            isRecordChanged
        )
    }

    override fun goToProfileScreen() {

    }

    override fun changeTab(tab: String) {
        when (tab) {
            NAV_TAB_PROFILE -> {
                activityDashboardBinding.navView.selectedItemId = R.id.navigation_profile
            }
            NAV_TAB_HOME -> {
                activityDashboardBinding.navView.selectedItemId = R.id.navigation_home
            }
        }
    }

    override fun clearLogOut() {
        super.clearLogOut()
        fragmentHome = null
        fragmentMusic = null
        fragmentWallet = null
        fragmentProfile = null
        stopPlayerFromLogout()
    }

    fun playMusic(
        songIdList: MutableList<File>,
        selectedIndex: Int,
        fromTop: Boolean,
        recordTitleStr: String,
        recordId: String,
        isReleased: Int,
        date: String,
        isRecordChanged: Boolean = false
    ) {
        this.recordTitleStr = recordTitleStr
        this.recordId = recordId
        fromTopButton = fromTop
        this.isReleased = isReleased
        this.date = date
        this.isRecordChanged = isRecordChanged

        if (::songIdsList.isInitialized) {
            if (fromTopButton) {

                if (songIdsList[songIndex].id == songIdList[selectedIndex].id) {
                    if (myMusic.isPlaying()) {
                        exo_pause.performClick()
                    } else {
//                        myMusic.initializePlayer()
//                        startFromTop(songIdList, selectedIndex)
                        activityDashboardBinding.songDetailsBottomView.visibility = View.VISIBLE
                        activityDashboardBinding.slidingLayout.panelState =
                            SlidingUpPanelLayout.PanelState.COLLAPSED
                        exo_play.performClick()
                    }

                } else {
                    startFromTop(songIdList, selectedIndex)
                }

            } else if (songIdsList[songIndex] == songIdList[selectedIndex]) {
                if (myMusic.isPlaying()) {
                    exo_pause.performClick()
                } else {
                    activityDashboardBinding.songDetailsBottomView.visibility = View.VISIBLE
                    activityDashboardBinding.slidingLayout.panelState =
                        SlidingUpPanelLayout.PanelState.COLLAPSED
                    exo_play.performClick()
                }
            } else {
                startFromTop(songIdList, selectedIndex)
            }
        } else {
            startFromTop(songIdList, selectedIndex)
        }

    }

    private fun startFromTop(songIdList: MutableList<File>, selectedIndex: Int) {
        if (::songIdsList.isInitialized && myMusic.isPlaying()) {
            launchBroadcastListener(false)
        }
        this.songIdsList = songIdList
        songIndex = selectedIndex
        playMusic(songIdsList[songIndex].id ?: -1)
    }

    private fun playMusic(songId: Int) {
        if (!isOnline()) {
            showToast(
                this, getString(R.string.internet_connectivity), CustomToast.ToastType.NETWORK
            )
            return
        }
        if (activityDashboardBinding.slidingLayout.panelState == SlidingUpPanelLayout.PanelState.HIDDEN) {
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        progressExpandPlayer.visibility = View.VISIBLE
        activityDashboardBinding.bottomLayoutPlay.progressBottomPlayer.visibility = View.VISIBLE

        cl_play_pause.visibility = View.INVISIBLE
        activityDashboardBinding.bottomLayoutPlay.imgPlay.visibility = View.INVISIBLE

//		showProcessDialog()
        val helper: ApiAuthHelper? = if (CommonUtils.isLogin(this)) {
            ApiClient.getClientMusic(this).create(ApiAuthHelper::class.java)
        } else {
            ApiClient.getClientMusic().create(ApiAuthHelper::class.java)
        }
        val request = mp3UrlRequest()
        request.fileId = songId
        request.requestHeader.deviceIP = "106.201.10.241"  //TODO remove hard code IP
        val call = helper?.getSongUrl(request)
        call?.enqueue(object : CallBackManager<mp3Response>() {
            override fun onSuccess(any: mp3Response?, message: String) {
                hideProcessDailog()
                val response = any as mp3Response
                if (response.responseStatus != 1 || response.response.songFilePath.isEmpty()) {
                    activityDashboardBinding.slidingLayout.panelState =
                        SlidingUpPanelLayout.PanelState.HIDDEN
                    showToast(
                        this@DashboardActivity,
                        response.responseMessage,
                        CustomToast.ToastType.FAILED
                    )
                    return
                }

                progressExpandPlayer.visibility = View.GONE
                activityDashboardBinding.bottomLayoutPlay.progressBottomPlayer.visibility =
                    View.GONE
                cl_play_pause.visibility = View.VISIBLE
                activityDashboardBinding.bottomLayoutPlay.imgPlay.visibility = View.VISIBLE

                response.response.let {
                    /*  val artistName = if (recordTitleStr.isNullOrEmpty()) {
                          it.artistName
                      } else {
                          recordTitleStr
                      }*/
                  /*  val artistName = it.artistName
                    val recordTitle = it.songTitle*/
                  /*  if (songIdsList[songIndex].artistName.isEmpty()) {
                        for (i in 0 until songIdsList.size) {
                            songIdsList[i].artistName = artistName
                            songIdsList[i].songTitle = recordTitle
                        }
                    }*/
                    updatePrevNextIcon()
                    updatePlayerTextUI(response)
                    managePlayPauseIcon(true)
                    if (it.RecordImagePath == null) {
                        it.RecordImagePath = ""
                    }
                    launchBroadcastListener(true)
                    trackSongPlayTime()

                    PrefUtils.saveValueInPreference(
                        this@DashboardActivity, PrefUtils.SONG_ID, songId
                    )
                    myMusic.onCreate(
                        this@DashboardActivity,
                        activityDashboardBinding.imgDrop.playerView,
                        it.songFilePath,
                        it.RecordImagePath!!,
                        songIdsList,
                        recordId,
                        songIndex
                    )

                    // setLyricsView()
                    //                    PrefUtils.saveValueInPreference(
                    //                        this@DashboardActivity,
                    //                        PrefUtils.PLAYING_DURATION,
                    //                        0
                    //                    )

                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                showToast(
                    this@DashboardActivity, message, CustomToast.ToastType.NETWORK
                )
                activityDashboardBinding.slidingLayout.panelState =
                    SlidingUpPanelLayout.PanelState.HIDDEN
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
                activityDashboardBinding.slidingLayout.panelState =
                    SlidingUpPanelLayout.PanelState.HIDDEN
            }

        })

    }

    override fun onPlayerError(error: PlaybackException) {
        super.onPlayerError(error)
        managePlayPauseIcon(false)
        activityDashboardBinding.slidingLayout.panelState = SlidingUpPanelLayout.PanelState.HIDDEN
        launchBroadcastListener(false)
    }


    private fun updatePlayerTextUI(response: mp3Response) {


        activityDashboardBinding.bottomLayoutPlay.tvSong.text = response.response.songTitle
        activityDashboardBinding.imgDrop.tvPlayerSong.text =
            response.response.songTitle.replace(".mp3", "")

        activityDashboardBinding.bottomLayoutPlay.tvArtist.text = response.response.artistName

        activityDashboardBinding.imgDrop.tvSongTitle.text = response.response.artistName

        activityDashboardBinding.imgDrop.tvPlayerAlbum.text = response.response.artistName

        activityDashboardBinding.imgDrop.tvPlayerSong.isSelected = true

        activityDashboardBinding.bottomLayoutPlay.tvSong.isSelected = true


        CommonUtils.loadImage(
            this@DashboardActivity,
            response.response.RecordImagePath ?: "",
            activityDashboardBinding.imgDrop.imgSong
        )
    }

    fun getCurrentSongId(): Int {

        if (::myMusic.isInitialized && myMusic.isPlaying() && ::songIdsList.isInitialized && songIndex > -1) {
            return songIdsList[songIndex].id ?: -1
        }
        return -1
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun openWallet() {
        selectedTab(NAV_TAB_ASSETS)
        activityDashboardBinding.navView.selectedItemId = R.id.navigation_assets
        viewModel.refresh(true)
    }

    private fun openProfile() {
        selectedTab(NAV_TAB_PROFILE)
        activityDashboardBinding.navView.selectedItemId = R.id.navigation_profile
        viewModel.refresh(true)
    }

    override fun openChat(bundle: Bundle) {
        activityDashboardBinding.navView.visibility = View.GONE
        super.openChat(bundle)
    }

    override fun onImageVisibilityChange(visibility: Int) {

        if (::songIdsList.isInitialized) {
            if (visibility == View.VISIBLE) {
                launchBroadcastListener(myMusic.isPlaying())
                managePlayPauseIcon(myMusic.isPlaying())
            } else {
                launchBroadcastListener(myMusic.isPlaying())
                managePlayPauseIcon(myMusic.isPlaying())

            }
        }
    }

    private fun stopTimer() {
        countDownTimer?.let {
            it.cancel()
            countDownTimer = null
        }
    }

    override fun onDestroy() {
        stopTimer()
        super.onDestroy()
        if (selectedSessionTopic != null) {
            val disconnectParams =
                Sign.Params.Disconnect(sessionTopic = requireNotNull(selectedSessionTopic))

            SignClient.disconnect(disconnectParams) { error ->
                Timber.tag("Error").e(error.throwable.stackTraceToString())
            }

            selectedSessionTopic = null
        }

    }

    private fun stopPlayerFromLogout() {
        if (::sliderListener.isInitialized) {
            activityDashboardBinding.slidingLayout.removePanelSlideListener(sliderListener)
        }
        if (activityDashboardBinding.slidingLayout.panelState == SlidingUpPanelLayout.PanelState.EXPANDED || activityDashboardBinding.slidingLayout.panelState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
            activityDashboardBinding.slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.HIDDEN
        }
        if (myMusic.isPlaying()) {
            myMusic.stopPLayer()
        }
        addListenerToBottomSlider()
        hideProcessDailog()
    }

    @SuppressLint("HardwareIds")
    private fun trackSongPlayTime() {
        val storedSongID = PrefUtils.getIntValue(
            this@DashboardActivity, PrefUtils.SONG_ID
        )
        if (storedSongID > 0) {
            val data =
                NebulaDatabase.getDatabase(this@DashboardActivity).songDao().getRow(storedSongID)
            val deviceId = Settings.Secure.getString(
                contentResolver, Settings.Secure.ANDROID_ID
            )
            if (data != null) {
                val duration = PrefUtils.getIntValue(
                    this@DashboardActivity, PrefUtils.PLAYING_DURATION
                )
                data.timeInSecond = data.timeInSecond + duration
                NebulaDatabase.getDatabase(this@DashboardActivity).songDao().insert(data)
            } else {
                val duration = PrefUtils.getIntValue(
                    this@DashboardActivity, PrefUtils.PLAYING_DURATION
                )

                val songEntity = PlaySongEntity(storedSongID, duration, deviceId)
                NebulaDatabase.getDatabase(this@DashboardActivity).songDao().insert(songEntity)
            }
            PrefUtils.saveValueInPreference(
                this@DashboardActivity, PrefUtils.PLAYING_DURATION, 0
            )
        }
    }

    private fun handleIntent(intent: Intent?) {

        /*    val appLinkAction = intent!!.action
            val appLinkData: Uri? = intent.data
            if (Intent.ACTION_VIEW == appLinkAction && appLinkData != null) {
                val recipeId: String = appLinkData?.getLastPathSegment().toString()

            }
    */
        if (intent!!.hasExtra(AppConstant.CONTENT_TYPE) && intent.getIntExtra(
                AppConstant.CONTENT_TYPE, 0
            ) == AppConstant.FROM_NOTIFICATION
        ) {
            val recordId = intent.getStringExtra(AppConstant.RECORD_ID) ?: "0"
            val fragment = mStacks[viewModel.mCurrentTab]?.lastElement()
            if (recordId != "0" && fragment != null && fragment !is OfferingFragment) {
                if (activityDashboardBinding.slidingLayout.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    activityDashboardBinding.slidingLayout.panelState =
                        SlidingUpPanelLayout.PanelState.COLLAPSED
                }
                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL, ""
                    )
                    putString(
                        AppConstant.ID, recordId
                    )
                    putString(
                        AppConstant.CONNECTED_WALLET_ACCOUNT, account
                    )

                    putString(
                        AppConstant.SELECTED_SESSION_TOPIC, selectedSessionTopic
                    )

                    putString(
                        AppConstant.WALLET_URI, deeplinkPairingUri
                    )
                }
                goToNewOffering(bundle)
                //gotorequestinvest(recordId)
            }
        } else {
            when (intent.action) {
                AppConstant.DETAILS_VIEW -> {
                    val recordID = intent.getStringExtra(AppConstant.RECORD_ID)
                    if (!recordID.isNullOrEmpty()) {
                        openRecordDetail(recordID, true)
                    }
                }
                AppConstant.WALLET_VIEW -> {
                    openWallet()
                }
                AppConstant.CHAT_VIEW -> {
                    val chatId = intent.getStringExtra(AppConstant.CHAT_ID) ?: "0"
                    val partnerId = intent.getStringExtra(AppConstant.partner_ID) ?: ""
                    val artistName = intent.getStringExtra(AppConstant.RECORD_ARTIST_NAME) ?: ""
                    val recordImage = intent.getStringExtra(AppConstant.RECORD_IMAGE_URL) ?: ""
                    val bundle = Bundle().apply {
                        putInt(AppConstant.CHAT_ID, chatId.toInt())
                        putString(AppConstant.partner_ID, partnerId)
                        putString(AppConstant.RECORD_ID, recordId)
                        putString(AppConstant.RECORD_ARTIST_NAME, artistName)
                        putString(AppConstant.RECORD_IMAGE_URL, recordImage)
                    }
                    openChat(bundle)
                }
                AppConstant.NORMAL_VIEW -> {
                    openRecordDetail(intent.data?.toString() ?: "", false)
                }
                AppConstant.REJECTED_VIEW -> {
                    openProfile()
                }
                AppConstant.PENDING_REQUESTS -> {
                    openPendingFragment(recordId)
                }
                AppConstant.TOKEN_PURCHASE -> {
                    val recordID = intent.getStringExtra(AppConstant.RECORD_ID)
                    onTokenPurchased(recordID!!)
                }
                Intent.ACTION_VIEW -> {

                    openRecordDetail(intent.data?.getQueryParameters("ri")?.get(0) ?: "", false)
                }
            }
        }
    }

    private fun pauseMusicService() {
        myMusic.playPausePlayer(false)
        launchBroadcastListener(false)
        managePlayPauseIcon(false)
    }

    fun saveCryptoWalletAddress(walletAddress: String) {
        if (!isOnline()) {
            showToast(
                this@DashboardActivity,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            hideProcessDailog()
            return
        }
        val helper =
            ApiClient.getClientAuth(this@DashboardActivity).create(ApiAuthHelper::class.java)
        val saveCryptoWalletAddress = SaveCryptoWalletAddress()
        saveCryptoWalletAddress.cryptoWalletAddress = walletAddress
        saveCryptoWalletAddress.walletTypeId = 1
        val call =
            helper.saveUserCryptoWalletAddress(saveCryptoWalletAddress, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<SaveCryptoWalletResponse>() {
            override fun onSuccess(any: SaveCryptoWalletResponse?, message: String) {
                val response = any as SaveCryptoWalletResponse
                if (response.responseStatus == 1) {

                    PrefUtils.saveValueInPreference(
                        this@DashboardActivity, PrefUtils.CONNECTED_WALLET_ADDRESS, walletAddress
                    )
                    Toast.makeText(
                        this@DashboardActivity,
                        "Wallet address added successfully.",
                        Toast.LENGTH_SHORT
                    ).show()

                } else {
                    Toast.makeText(
                        this@DashboardActivity, response.responseMessage, Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(message: String) {
                Toast.makeText(
                    this@DashboardActivity, message, Toast.LENGTH_SHORT
                ).show()
            }

            override fun onError(error: RetroError) {
                Toast.makeText(
                    this@DashboardActivity, error.toString(), Toast.LENGTH_SHORT
                ).show()
            }
        })
    }
/*    fun executeFunction(
        function: org.web3j.abi.datatypes.Function,
        contractAddress: String,
        web3j: Web3j,
        credentials: Credentials
    ): Any {
        val encodedFunction = FunctionEncoder.encode(function)

        val transaction = Transaction.createEthCallTransaction(
            credentials.address,
            contractAddress,
            encodedFunction
        )

        val response = web3j.ethCall(transaction, DefaultBlockParameterName.LATEST).send()
        val output = FunctionReturnDecoder.decode(response.value, function.outputParameters)

        return output[0].value
    }*/

    /* // Define the smart contract address, ABI, and web3j instance
     val contractAddress = "0x1234567890abcdef1234567890abcdef12345678"
     val contractABI = // ABI JSON string
     val web3j = Web3j.build(HttpService("http://localhost:8545"))

     // Create the Contract instance using the contract address and ABI
     val contract = Contract(contractAddress, contractABI, web3j, DefaultGasProvider())

     // Define the function name and arguments
     val functionName = "myFunction"
     val arg1 = "argument1"
     val arg2 = 12345

     // Create the Function instance using the function name and arguments
     val function = Function(functionName,
         listOf(Type.encode(arg1), Type.encode(arg2)),
         emptyList())

     // Define the gas price and limit for the transaction
     val gasPrice = DefaultGasProvider.GAS_PRICE
     val gasLimit = DefaultGasProvider.GAS_LIMIT

     // Define the sender's account and private key
     val account = "0x0123456789abcdef0123456789abcdef0123456"
     val privateKey = "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"

     // Create the transaction data using the Function instance
     val encodedFunction = FunctionEncoder.encode(function)
     val txData = Transaction.createFunctionCallTransaction(account, nonce, gasPrice, gasLimit, contractAddress, BigInteger.ZERO, encodedFunction)

     // Sign the transaction using the sender's private key
     val signedTx = RawTransaction.createTransaction(nonce, gasPrice, gasLimit, contractAddress, BigInteger.ZERO, encodedFunction)
     val credentials = Credentials.create(privateKey)
     val signedData = TransactionEncoder.signMessage(signedTx, credentials)

     // Send the signed transaction using the web3j instance
     val txHash = web3j.ethSendRawTransaction(signedData).send().transactionHash

     println("Transaction sent: $txHash")*/

    /*    private fun createTokenTransaction(gas: String,
                                           gasPrice: String)
                : EthTransaction {
            // bnb=56, bnb_test=97
            val chainId = "0x" + approvedChainId!!.toHex()
            // on bnb_test
            val smartContractAddressMain = "0xb465f3cb6Aba6eE375E12918387DE1eaC2301B05"
            val smartContractAddress = "0xcD0FC5078Befa4701C3692F4268641A468DEB8d5"
            // keccak256(transactionMethod).take(4 bytes)
            val smartContractTransactionMethod = "a9059cbb"
            // must be 32 bytes
            val to = "621261D26847B423Df639848Fb53530025a008e8".padStart(64, '0')
            // 3 decimal; must be 32 bytes; 1_000L.toHex()
            val amount = BigInteger("1000").toString(16).padStart(64, '0')


            return EthTransaction(
                from = approvedAddress!!,
                to = if (approvedChainId!! == 56) smartContractAddressMain else smartContractAddress,
                data = "0x$smartContractTransactionMethod$to$amount",
                chainId = chainId,
                gas = gas, // can't auto calculate for smart contract
                gasPrice = gasPrice, // can auto calculate
                gasLimit = null,
                maxFeePerGas = null,
                maxPriorityFeePerGas = null,
                value = "", // this is in 'data' field now
                nonce = null // ignored by metamask
            )
        }*/


    /*fun generatingSignature(walletAddress: String) {
        val web3j = Web3j.build(HttpService("https://matic-mumbai.chainstacklabs.com/"))
        val _msgToSign =
            "Welcome to Nebula! Click to sign in and accept the Nebula Terms of Service: https://nebu.la/tos This request will not trigger a blockchain transaction or cost any gas fees. Your authentication status will reset after 24 hours.\n\nWallet address: ${walletAddress}\n\nNonce: _nonce"

        // Create a hash of the message
        val hash = Hash.sha3(_msgToSign.toByteArray())

        // Convert the hash to a hexadecimal string
        val hashHex = hash.toHexString()

        // Execute the signing operation in a coroutine
        runBlocking {
            val signature = async(Dispatchers.IO) {
                web3j.ethSign(hashHex, "0X$walletAddress").send().signature
            }

            // Access the signature result once it is available
            val signatureResult = signature.await()

            // Process the signature
            processSignature(signatureResult)
        }
    }*/
/*
    fun generatingSignature(walletAddress: String) {
        val web3j = Web3j.build(HttpService("https://matic-mumbai.chainstacklabs.com/"))
        val _msgToSign =
            "Welcome to Nebula! Click to sign in and accept the Nebula Terms of Service: https://nebu.la/tos This request will not trigger a blockchain transaction or cost any gas fees. Your authentication status will reset after 24 hours.\n\nWallet address: $walletAddress\n\nNonce: _nonce"

        // Create a hash of the message
        val hash = Hash.sha3(_msgToSign.toByteArray())

        // Convert the hash to a hexadecimal string
        val hashHex = hash.toHexString()

        // Execute the signing operation in a coroutine
        runBlocking {
            try {
                val signature = async(Dispatchers.IO) {
                    web3j.ethSign(hashHex, walletAddress).send().signature
                }

                // Access the signature result once it is available
                val signatureResult = signature.await()

                if (signatureResult != null) {
                    // Process the signature
                    processSignature(signatureResult)
                } else {
                    println("Signature result is null.")
                }
            } catch (e: Exception) {
                println("Error occurred during signing: ${e.message}")
            }
        }
    }
*/

    fun generatingSignature(walletAddress: String) {
        val web3j = Web3j.build(HttpService("https://matic-mumbai.chainstacklabs.com"))
        // Ethereum account address
        val address = "0x$walletAddress" // Replace with the Ethereum account address
        val _nounce = null
        val message =
            "Welcome to Nebula! Click to sign in and accept the Nebula Terms of Service: https://nebu.la/tos This request will not trigger a blockchain transaction or cost any gas fees. Your authentication status will reset after 24 hours.\n\nWallet address: $address\n\nNonce: $_nounce"
        val messageHash = Hash.sha3(message.toByteArray())
        val messageHashHex = Numeric.toHexString(messageHash)
        val signature =
            org.web3j.crypto.Sign.signMessage(messageHash, Credentials.create(address).ecKeyPair)
        val r = Numeric.toHexStringNoPrefix(signature.r)
        val s = Numeric.toHexStringNoPrefix(signature.s)
        val v = signature.v.toString()
        // Print the signature components
        println("R: $r")
        println("S: $s")
        println("V: $v")
        println("adddress:$address")
        println("messageHash:$messageHashHex")
        println("message:$message")
    }
    /*   fun processSignature(signature: String) {
           // Handle the signature as needed
           println("Signature: $signature")
       }*/


    private fun sign(message: String, credentials: Credentials): String? {
        val hash: ByteArray = message.toByteArray(StandardCharsets.UTF_8)
        val signature: org.web3j.crypto.Sign.SignatureData =
            org.web3j.crypto.Sign.signPrefixedMessage(hash, credentials.ecKeyPair)
        val r = Numeric.toHexString(signature.getR())
        val s = Numeric.toHexString(signature.getS()).substring(2)
        val v = Numeric.toHexString(signature.getV()).substring(2)
        println("$r    $s    $v")
        return StringBuilder(r).append(s).append(v).toString()
    }
}
