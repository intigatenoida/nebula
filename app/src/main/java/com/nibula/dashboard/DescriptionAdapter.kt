package com.nibula.dashboard

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.nibula.R
import com.nibula.response.details.File
import com.nibula.utils.AppConstant


class DescriptionAdapter(
    var context: Context,
    val songTitle: MutableList<File>,
    val recordImagePath: String,
    val recordId: String
) :
    PlayerNotificationManager.MediaDescriptionAdapter {

    override fun createCurrentContentIntent(player: Player): PendingIntent? {
        val intent = Intent(context, DashboardActivity::class.java)
        intent.putExtra(AppConstant.CONTENT_TYPE, AppConstant.FROM_NOTIFICATION)
        intent.putExtra(AppConstant.RECORD_ID, recordId)
        return PendingIntent.getActivity(
            context, 0,
            intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
    }

    override fun getCurrentContentText(player: Player): CharSequence? {
        return songTitle[player!!.currentWindowIndex].songTitle
    }

    override fun getCurrentContentTitle(player: Player): CharSequence {
        return songTitle[player!!.currentWindowIndex].artistName
    }

    override fun getCurrentLargeIcon(
        player: Player,
        callback: PlayerNotificationManager.BitmapCallback
    ): Bitmap? {
        val icon = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.nebula_placeholder
        )

        Glide.with(context)
            .asBitmap()
            .load(recordImagePath)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(
                    resource: Bitmap,
                    transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                ) {
                    callback!!.onBitmap(resource)
                }


                override fun onLoadCleared(placeholder: Drawable?) {

                }
            })




        return icon

    }
}
