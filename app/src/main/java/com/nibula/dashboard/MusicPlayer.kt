package com.nibula.dashboard

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerView
import com.nibula.response.details.File
import com.nibula.utils.AudioPlayerService

class MusicPlayer {

    lateinit var context: Context
    lateinit var playerView: PlayerView
    lateinit var playerEventListener: Player.EventListener
    private var player: ExoPlayer? = null
    private var mService: AudioPlayerService? = null
    private var mBound = false
    private var serviceIntent: Intent? = null
    private var songUrl = ""
    private lateinit var recordImagePath: String
    private lateinit var recordId: String
    private lateinit var songList: MutableList<File>
    var songIndex: Int = -1


    fun onCreate(
        context: Context,
        playerView: PlayerView,
        songUrl: String,
        recordImagePath: String,
        songTitle: MutableList<File>,
        recordId: String,
        songIndex: Int
    ) {
        playerView.useController = true
        playerView.showController()
        playerView.controllerAutoShow = true
        playerView.controllerHideOnTouch = false
        this.songUrl = songUrl
        this.context = context
        this.playerView = playerView
        this.songList = songTitle
        this.recordId = recordId
        this.songIndex = songIndex
        if (recordImagePath!=null){
            this.recordImagePath = recordImagePath
        }
        playerEventListener = context as Player.EventListener
        serviceIntent = Intent(context, AudioPlayerService::class.java)
        initializePlayer()
        prepareSong()
    }

    fun initializePlayer() {
        //Initialize the music player
        if (mBound) {
            player = mService!!.getplayerInstance()
            playerView.player = player
            playerView.controllerHideOnTouch = false
            player!!.addListener(playerEventListener)
        }
    }

    private fun prepareSong() {
       /* serviceIntent?.let { Util.startForegroundService(context, it) }*/
        initializePlayer()
        if (mBound) {
            unBindService()
        }
        context.bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)
//        if (!mBound || mService == null) {
//            context.bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)
//        } else if (mBound && mService != null) {
//            mService!!.prepareSong(songUrl, songList, recordImagePath, recordId, songIndex)
//        }
    }

    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            val binder = iBinder as AudioPlayerService.LocalBinder
            mService = binder.service
            mService!!.prepareSong(songUrl, songList, recordImagePath, recordId, songIndex)
            mBound = true
            initializePlayer()
        }


        override fun onServiceDisconnected(componentName: ComponentName) {
            mBound = false
        }
    }

    private fun unBindService() {
        if (mBound) {
            context.unbindService(mConnection)
            mBound = false
        }

    }
    fun stopPLayer(){
        unBindService()
        playPausePlayer(false)
    }

    fun playPausePlayer(state: Boolean) {
        player?.playWhenReady = state
//		player!!.getPlayWhenReady()
    }


    fun isPlaying(): Boolean {
        return player != null
                && player!!.playbackState != Player.STATE_ENDED
                && player!!.playbackState != Player.STATE_IDLE
                && player!!.playWhenReady
    }

    fun getPlayerCurrentIndex(): Long {
        return player?.currentPosition ?: 0
    }

    fun getPlayerDuration(): Long {
        return player?.duration ?: 0
    }

    fun getPlayerCurrentWindowIndex(): Int {
        return player?.currentWindowIndex ?: 0
    }


}