package com.nibula.dashboard

object  ConstantsNavTabs {
    const val NAV_TAB_HOME = "nav_tab_home"
    const val NAV_TAB_MUSIC = "nav_tab_music"
    const val NAV_TAB_ASSETS = "nav_tab_assets"
    const val NAV_TAB_PROFILE = "nav_tab_profile"
    const val TOP_SEARCH_TAB = "top_search_tab"
    const val TOP_USER_ACTIVITY_TAB = "top_user_activity_tab"
    const val TOP_USER_HOW_TO_WORK= "top_how_to_work"
    const val TOP_USER_RECORD_BUY= "top_record_buy"
    const val TOP_USER_RECORD_INVEST= "top_record_invest"
    const val NOTIFICATION_REDIRECTION= "notification_redirection"
    const val TOP_RECORD_DETAIL= "record_detail"
}