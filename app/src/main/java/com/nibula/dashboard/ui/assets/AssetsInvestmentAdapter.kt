package com.nibula.dashboard.ui.assets

import android.content.Context
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.UnderlineSpan
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.ItemMyInvestmentBinding
import com.nibula.response.myinvestmentresponse.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils

import kotlin.math.roundToInt

class AssetsInvestmentAdapter(val fg: BaseFragment) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val dataList = mutableListOf<ResponseCollection>()

    var context: Context = fg.requireContext()

    private lateinit var binding: ItemMyInvestmentBinding


    class AssetInvestmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setData(data: MutableList<ResponseCollection>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun addData(data: MutableList<ResponseCollection>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        dataList.clear()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        /*  val view = LayoutInflater.from(context).inflate(
              R.layout.item_my_investment,
              parent,
              false
          )*/
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemMyInvestmentBinding.inflate(inflater, parent, false)


        val holder = AssetInvestmentViewHolder(binding.root)
        holder.itemView.setOnClickListener {
            if (holder.adapterPosition in 0..dataList.size) {


                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL, dataList[holder.adapterPosition].recordImage
                    )
                    putString(
                        AppConstant.ID, dataList[holder.adapterPosition].recordId
                    )
                }

                fg.navigate.goToNewOffering(bundle)

                //  fg.navigate.gotorequestinvest(dataList[holder.adapterPosition].recordId)
            }
        }

        binding.tvViewCollectors.setOnClickListener {

            if (holder.adapterPosition in 0..dataList.size) {
                fg.navigate.goToViewCollectorsScreen(dataList[holder.adapterPosition].recordId)

            }
        }


        binding.viewCertificate.setOnClickListener {
            if (holder.adapterPosition in 0..dataList.size) {
                dataList[holder.adapterPosition]?.totalPaidAmount?.let {
                    if (it > 0.0) {
                        fg.navigate.goToCertificatesScreen(dataList[holder.adapterPosition].recordId)

                    }
                }
            }
        }


        return holder
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]

        binding.layoutImage.clImage.setImageResource(R.drawable.nebula_placeholder)
        /*   holder.itemView.clImage1.setImageResource(R.drawable.nebula_placeholder)
           holder.itemView.clImage2.setImageResource(R.drawable.nebula_placeholder)*/

        if ((data.totalInvestedAmount ?: 0.0) > 0.0) {
            binding.certificateImage1.setImageResource(R.drawable.certificate_disabled)
            binding.shareCertificateTitle1.setTextColor(
                ContextCompat.getColor(
                    context, R.color.white
                )
            )
        } else {
            binding.certificateImage1.setImageResource(R.drawable.certificate)
            binding.shareCertificateTitle1.setTextColor(
                ContextCompat.getColor(
                    context, R.color.gray
                )
            )
        }

        Glide.with(context).load(data.recordImage).placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
            .into(binding.layoutImage.clImage)


        /*     if (data.recordTypeId == 1) {
                 holder.itemView.bottomShadow.visibility = View.VISIBLE
                 holder.itemView.clImage.borderWidth = 1f
                 CommonUtils.loadAsBitmap(
                     context,
                     holder.itemView.clImage,
                     holder.itemView.clImage1,
                     holder.itemView.clImage2,
                     data.recordImage,
                     R.drawable.nebula_placeholder
                 )
             } else {
                 holder.itemView.clImage.borderWidth = 0f
                 CommonUtils.loadAsBitmap(
                     context,
                     holder.itemView.clImage,
                     null,
                     null,
                     data.recordImage,
                     R.drawable.nebula_placeholder
                 )
                 holder.itemView.bottomShadow.visibility = View.INVISIBLE
             }
     */
        binding.artist.text = "${context.getString(R.string.artist)}: ${data.uploadingPerson}"

        when (data.recordTypeId) {
            1 -> {
                binding.album.text = "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
            2 -> {
                binding.album.text = "${context.getString(R.string.single)}: ${data.recordTitle}"
            }
            else -> {
                binding.album.text = "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
        }
        val spn1 = SpannableStringBuilder()
        spn1.append("${context.getString(R.string.token_owned)}: ")

        spn1.append(
            CommonUtils.setSpannableWhite(
                data.totalOwnedTokens.toString(), 0, data.totalOwnedTokens.toString().length
            )
        )

        binding.tvInvested.text = spn1

        val ss1 = SpannableString(context.getString(R.string.view_collectors))
        ss1.setSpan(
            UnderlineSpan(), 0, ss1.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        binding.tvViewCollectors.text = ss1

        when (data.recordStatusTypeId) {
            AppConstant.released -> {
                binding.tvInvestmentStatus.visibility = View.GONE

                val spn2 = SpannableStringBuilder()
                spn2.append("${context.getString(R.string.amount_earned)}: ")


                /*val investedAmount =
                    CommonUtils.trimDecimalToTwoPlaces(data?.totalInvestedAmount ?: 0.0)*/
                val investedAmount = "0.00"

                spn2.append(
                    CommonUtils.setSpannableWhite(
                        "${data.currencySymbol} 0.00",
                        0,
                        investedAmount?.length!! + data.currencySymbol!!.length + 1
                    )
                )
                binding.tvEarned.text = spn2
                //holder.itemView.released.visibility = View.VISIBLE
                binding.tvTimeRemain.visibility = View.GONE
                binding.tvReleased.visibility = View.GONE

            }
            else -> {
                binding.tvInvestmentStatus.visibility = View.GONE

                val spn2 = SpannableStringBuilder()
                spn2.append("${context.getString(R.string.amount_earned)}: ")


                /*val investedAmount =
                    CommonUtils.trimDecimalToTwoPlaces(data?.totalInvestedAmount ?: 0.0)*/
                val investedAmount = "0.00"

                spn2.append(
                    CommonUtils.setSpannableWhite(
                        "${data.currencySymbol} 0.00",
                        0,
                        investedAmount?.length!! + data.currencySymbol!!.length + 1
                    )
                )
                binding.tvEarned.text = spn2
                //holder.itemView.released.visibility = View.VISIBLE
                binding.tvTimeRemain.visibility = View.GONE
                binding.tvReleased.visibility = View.GONE

            }
            /*     val spn3 = SpannableStringBuilder()
                 spn3.append("${context.getString(R.string.amount_earned)}: ")
                 val nr = context.getString(R.string.not_released)
                 spn3.append(
                     CommonUtils.setSpannable(
                         context,
                         nr,
                         0,
                         nr.length
                     )
                 )
                 holder.itemView.tvEarned.text = spn3

                 holder.itemView.released.visibility = View.GONE
                 holder.itemView.tvTimeRemain.visibility = View.GONE
                 holder.itemView.tvReleased.visibility = View.GONE
                 holder.itemView.tvTimeRemain.setPadding(dpToPx(5), 0, dpToPx(5), 0)
                 *//*Counter*//*
                val temp = TimestampTimeZoneConverter.convertToMilliseconds(
                    data.timeLeftToRelease ?: 0
                )
                if (temp > TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                    val time =
                        TimestampTimeZoneConverter.getDaysAndHourDifference(
                            context,
                            temp
                        )
                    holder.itemView.tvTimeRemain.visibility = View.GONE
                    holder.itemView.tvTimeRemain.text = time
                } else {
                    holder.itemView.tvTimeRemain.visibility = View.INVISIBLE
                    if (temp > 0 &&
                        !data.isFinish
                    ) {
                        val workingTime =
                            TimestampTimeZoneConverter.getDaysAndHourDifference(
                                context,
                                temp
                            )
                        holder.itemView.tvTimeRemain.text = workingTime
                        holder.itemView.tvTimeRemain.visibility = View.VISIBLE
                    }
                }

                holder.itemView.tvReleased.text = data.offeringEndDate?.let {
                    TimestampTimeZoneConverter.convertToLocalDate(
                        it, TimestampTimeZoneConverter.UTC_TIME, "dd/MM/yyyy"
                    )
                }?.let { releaseDateString(it) }
                when (data.investmentStatusId) {

                    AppConstant.PENDING -> {
                        //  holder.itemView.tv_investment_status.visibility = View.VISIBLE
                        holder.itemView.tv_investment_status.text = "${data.investmentStatus}"
                        holder.itemView.tv_investment_status.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.gray_shade_1_100per
                            )
                        )


//                        holder.itemView.tvTimeRemain.visibility = View.GONE
//                        holder.itemView.tvTimeRemain.text = AppConstant.pending
//                        holder.itemView.tvTimeRemain.setTextColor(
//                            ContextCompat.getColor(
//                                context,
//                                R.color.white
//                            )
//                        )
//                        holder.itemView.tvTimeRemain.background =
//                            ContextCompat.getDrawable(context, R.drawable.panding_background)
//                        holder.itemView.tvTimeRemain.setPadding(dpToPx(17), 0, dpToPx(17), 0)
                    }

                    AppConstant.APPROVED -> {
                        // holder.itemView.tv_investment_status.visibility = View.VISIBLE
                        holder.itemView.tv_investment_status.text = "${data.investmentStatus}"
                        holder.itemView.tv_investment_status.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.green_shade_1_100per
                            )
                        )
                    }

                    AppConstant.REJECTED -> {
                        //holder.itemView.tv_investment_status.visibility = View.VISIBLE
                        holder.itemView.tv_investment_status.text = "${data.investmentStatus}"
                        holder.itemView.tv_investment_status.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.red_shade_1_100per
                            )
                        )
                    }

                    AppConstant.INVESTED -> {
                        // holder.itemView.tv_investment_status.visibility = View.VISIBLE
                        holder.itemView.tv_investment_status.text = "${data.investmentStatus}"
                        holder.itemView.tv_investment_status.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.green_shade_1_100per
                            )
                        )
                    }

                    else -> {
                        holder.itemView.tv_investment_status.visibility = View.GONE
                    }
                }
            }*/
        }

    }


    fun dpToPx(dp: Int): Int {
        val displayMetrics: DisplayMetrics = context.resources.displayMetrics
        return (dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }
}