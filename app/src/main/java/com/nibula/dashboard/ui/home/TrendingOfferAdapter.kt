package com.nibula.dashboard.ui.home

import android.content.Context
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.ItemHomeTrendingBlankBinding
import com.nibula.databinding.ItemHomeTrendingOfferNewBinding
import com.nibula.response.topoffertopartist.TrendingOfferingData
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils


class TrendingOfferAdapter(
    val fg: BaseFragment,
    val itemWidth: Int
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() /*TimerCounter.TimerItemRemove*/ {

    var dataList: MutableList<TrendingOfferingData> = mutableListOf()

    var context: Context = fg.requireContext()

    var thisMillisStart: Long = fg.millisStart
    lateinit var binding: ItemHomeTrendingOfferNewBinding
    lateinit var bindingBlank: ItemHomeTrendingBlankBinding

    class SpaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class BlankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class TrendingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setData(data: MutableList<TrendingOfferingData>) {
        val temp = TrendingOfferingData()
        temp.viewType = 2
        data.add(0, temp)
        dataList = data
        //setDummyData(dataList)
        setRemainingTime(dataList)
        thisMillisStart = fg.millisStart
        notifyDataSetChanged()
    }

    private fun setRemainingTime(dataList: MutableList<TrendingOfferingData>) {
        for (obj in dataList) {
            obj.remainingMillis = obj.timeLeftToRelease ?: 0
            obj.tltr = obj.timeLeftToRelease ?: 0
            obj.rId = obj.id ?: ""
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataList.isNotEmpty()) dataList[position].viewType ?: 1 else 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 1) {
            /*  val holder = TrendingViewHolder(
                  LayoutInflater.from(context).inflate(
                      R.layout.item_home_trending_offer_new,
                      parent,
                      false
                  )*/

            val inflater = LayoutInflater.from(parent.context)
            binding = ItemHomeTrendingOfferNewBinding.inflate(inflater, parent, false)

            val holder = TrendingViewHolder(binding.root)

            holder.itemView.setOnClickListener {
                if (holder.adapterPosition in 0..dataList.size) {
                    //fg.navigate.gotorequestinvest(dataList[holder.adapterPosition].id)
                    val bundle = Bundle().apply {
                        putString(
                            AppConstant.RECORD_IMAGE_URL,
                            dataList[holder.adapterPosition].recordImage
                        )

                        putString(
                            AppConstant.ID,
                            dataList[holder.adapterPosition].id
                        )
                    }
                    fg.navigate.goToNewOffering(bundle)
                }
            }
            holder
        } else if (viewType == 3) {


            val inflater = LayoutInflater.from(parent.context)
            bindingBlank = ItemHomeTrendingBlankBinding.inflate(inflater, parent, false)

            val holder = BlankViewHolder(bindingBlank.root)

            return holder
            /* BlankViewHolder(
                 LayoutInflater.from(context).inflate(
                     R.layout.item_home_trending_blank,
                     parent,
                     false
                 )
             )*/
        } else {

            SpaceViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_space_home_trending,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return if (dataList.isNotEmpty()) dataList.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]
        Log.d("OnBind", "TrendingList")

        if (holder is TrendingViewHolder) {
            val layoutParams = binding.cvImage.clImage.layoutParams
            layoutParams.width = itemWidth
            binding.tvAlbumTitle.text = data.recordTitle ?: ""
            binding.tvArtistName.text = data.recordArtist
            if (data.availableTokens != null) {
                val shareCount = data.availableTokens ?: 0.0
                val totalToken = data.totalTokens
                binding.tvTokenAvailable.text = "${shareCount}/$totalToken"

            }
            binding.tvTokenAvailableText.text = "Token Available"
            binding.tvReleaseData.text = "Value per Token"


            binding.tvAmountPerShare.text =
                "${data.currencyType ?: "$"}${CommonUtils.trimDecimalToTwoPlaces(data?.pricePerToken ?: 0.0)}"

            Glide
                .with(context)
                .load(data.recordImage + "?maxwidth=200&maxheight=200&quality=80")
                .placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.IMMEDIATE)
                .into(binding.cvImage.clImage)

            when (data.recordTypeId) {
                1 -> {
                    binding.cvImage.clAlbumInfo.visibility=View.VISIBLE
                }
                else->
                { binding.cvImage.clAlbumInfo.visibility=View.GONE

                }
            }

        } else if (holder is BlankViewHolder) {

            val ss1 = SpannableString(context.getString(R.string.artist))
            ss1.setSpan(UnderlineSpan(), 0, ss1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            bindingBlank.tvAvaShare.text = ss1
            val layoutParams = bindingBlank.cvImageBlank.layoutParams
            layoutParams.width = itemWidth
            bindingBlank.tvRemainBlank.text = "Sold Out"
        }

    }

    fun setDummyData() {
        var temp = TrendingOfferingData()
        temp.viewType = 3
        dataList.add(temp)

        temp = TrendingOfferingData()
        temp.viewType = 3
        dataList.add(temp)

        temp = TrendingOfferingData()
        temp.viewType = 3
        dataList.add(temp)

        temp = TrendingOfferingData()
        temp.viewType = 3
        dataList.add(temp)
        notifyDataSetChanged()
    }
}