package com.nibula.dashboard.ui.assets
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.dashboard.ui.assets.helper.AssetsMusicHelper
import com.nibula.databinding.AssestMusicFragmentBinding
import com.nibula.response.musicprofile.ProfileMusicReponse
import com.nibula.user.profile.music.MusicViewModel
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import retrofit2.Call

class AssetMusicFragment : BaseFragment() {

    companion object {
        fun newInstance() = AssetMusicFragment()
    }

    private lateinit var viewModel: MusicViewModel

    lateinit var rootView: View
    lateinit var adapter: MyMusicAdapter

    private lateinit var helper: AssetsMusicHelper

    var call: Call<ProfileMusicReponse>? = null

    var isDataAvailable = false
    var isLoading = true
     lateinit var binding:AssestMusicFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {

            binding=AssestMusicFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
           // rootView = inflater.inflate(R.layout.assest_music_fragment, container, false)
            initUI()
        }
        return rootView
    }
    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(this).get(MusicViewModel::class.java)
        binding.musicRv.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            helper.getMusicListing("", true)
        }

        updateTotalAmount("USD")
    }

    override fun onStop() {
        super.onStop()
        if (call != null && !call!!.isExecuted) {
            call!!.cancel()
        }
    }

    private fun initUI() {
        helper = AssetsMusicHelper(this)
        val layoutManager = LinearLayoutManager(requireContext())
        binding.musicRv.layoutManager = layoutManager
        adapter = MyMusicAdapter(this)
        binding.musicRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.getMusicListing("", false)
                }
            }
        })

        binding.swipeMusic.setOnRefreshListener {
            helper.musicNextPage = 1
            helper.musicMaxPage = 0
            helper.getMusicListing("", false)
        }

      /*  binding.layoutNoMusic.nosetOnClickListener {
            startActivity(Intent(requireContext(), UploadRecordActivity::class.java))
        }

        */
    }

    fun updateTotalAmount(currency:String?) {
        if (binding.totaltxt != null) {
            val temp = PrefUtils.getFloatValue(requireContext(), PrefUtils.MY_MONEY).toDouble()
            val totalAmt = if (temp == 0.0) {
                "${getString(R.string.total_raised)}: $currency${CommonUtils.removeOneDecimalPlaces(temp)}"
            } else {
                "${getString(R.string.total_raised)}: $currency${CommonUtils.trimDecimalToTwoPlaces(temp)}"
            }

            binding.totaltxt.text = totalAmt
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.musicRv.adapter = null
    }

}
