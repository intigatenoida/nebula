package com.nibula.dashboard.ui.music.helper

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ui.music.vfragments.OfferingsFragment
import com.nibula.response.music.offerings.OfferingResponses
import com.nibula.response.music.offerings.RecordDetailsData
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
class OfferingFragmentHelper(val fg: OfferingsFragment) : BaseHelperFragment() {

    var offeringNextPage: Int = 1
    var thisMonthNextPage: Int = 1
    var nextMonthNextPage: Int = 1

    var offeringMaxPage: Int = 0
    var thisMonthMaxPage: Int = 0
    var nextMonthMaxPage: Int = 0

    var isThisMonthMore: Boolean = false
    var isNextMonthMore: Boolean = false


    fun requestOfferings(search: String, isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (fg.call != null) {
            fg.call!!.cancel()
        }

        fg.isSearchProgress = true

        if (search.isNotEmpty()) {
            fg.binding.tvupcoming.visibility = View.GONE
            fg.binding.tvMoreThisMonth.visibility = View.GONE
            fg.binding.tvMoreNextMonth.visibility = View.GONE
            fg.binding.gdThisMonth.visibility = View.GONE
            fg.binding.gdNextMonth.visibility = View.GONE
        }

        if (!fg.binding.swpMain.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        val helper =
            ApiClient.getClientInvestor().create(ApiAuthHelper::class.java)
        fg.call = helper.requestOfferings(OfferingsFragment.OFFERINGS, offeringNextPage, search)
        fg.call!!.enqueue(object : CallBackManager<OfferingResponses>() {
            override fun onSuccess(any: OfferingResponses?, message: String) {
                /*More*/
                offeringMaxPage = ((any?.response?.totalRecords ?: 0) / 10.0).toInt()
                if (offeringNextPage < offeringMaxPage ||
                    (any?.response?.totalRecords ?: 0) > (offeringNextPage * 10)
                ) {
                    fg.binding.tvMoreOfferings.visibility = View.VISIBLE
                } else {
                    fg.binding.tvMoreOfferings.visibility = View.GONE
                }

                /*Data*/
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.response?.recordsList.isNullOrEmpty()) {
                        fg.binding.rvOffering.visibility = View.VISIBLE
                        if (offeringNextPage == 1) {
                            fg.offeringAdapter.setData(getFilerData(any.response!!.recordsList))
                        } else if (offeringNextPage > 1) {
                            fg.offeringAdapter.addData(getFilerData(any.response!!.recordsList))
                        }
                        offeringNextPage++
                    } else {
                        if (offeringNextPage == 1) {
                            fg.offeringAdapter.clearData()
                        }
                    }
                } else {
                    if (offeringNextPage == 1) {
                        fg.offeringAdapter.clearData()
                    }
                }

                if (fg.offeringAdapter.dataList.isEmpty()) {
                    fg.binding.rvOffering.visibility = View.GONE
                }

                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {
                dismiss(isProgressDialog)
            }

        })
    }

    private fun getFilerData(recordsList: MutableList<RecordDetailsData>): MutableList<RecordDetailsData> {
        val tempList = ArrayList<RecordDetailsData>()
//        for (record in recordsList) {
//            if (record.timeLeftToRelease ?: 0 > 0) {
//                tempList.add(record)
//            }
//        }
        return recordsList
    }

    fun requestThisMonth(search: String?, isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.swpMain.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        if (fg.call1 != null) {
            fg.call1!!.cancel()
        }

        val helper =
            ApiClient.getClientInvestor().create(ApiAuthHelper::class.java)
        fg.call1 =
            helper.requestOfferings(
                OfferingsFragment.THIS_MONTH_OFFERINGS,
                thisMonthNextPage,
                search ?: ""
            )
        fg.call1!!.enqueue(object : CallBackManager<OfferingResponses>() {

            override fun onSuccess(any: OfferingResponses?, message: String) {
                /*More*/
                thisMonthMaxPage = ((any?.response?.totalRecords ?: 0) / 10.0).toInt()
                isThisMonthMore = (thisMonthNextPage < thisMonthMaxPage ||
                        (any?.response?.totalRecords ?: 0) > (thisMonthNextPage * 10))
                if (isThisMonthMore) {
                    fg.binding.tvMoreThisMonth.visibility = View.VISIBLE
                } else {
                    fg.binding.tvMoreThisMonth.visibility = View.GONE
                }

                /*Data*/
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.response?.recordsList.isNullOrEmpty()) {
                        fg.binding.gdThisMonth.visibility = View.VISIBLE
                        if (thisMonthNextPage == 1) {
                            fg.thisMonthAdapter.setData(getFilerData(any.response!!.recordsList))
                        } else if (thisMonthNextPage > 1) {
                            fg.thisMonthAdapter.addData(getFilerData(any.response!!.recordsList))
                        }
                        thisMonthNextPage++
                    } else {
                        if (thisMonthNextPage == 1) {
                            fg.thisMonthAdapter.clearData()
                        }
                    }
                } else {
                    if (thisMonthNextPage == 1) {
                        fg.thisMonthAdapter.clearData()
                    }
                }

                if (fg.thisMonthAdapter.dataList.isEmpty()) {
                    fg.binding.gdThisMonth.visibility = View.GONE
                }

                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {
                dismiss(isProgressDialog)
            }

        })
    }

    fun requestNextMonth(search: String?, isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.swpMain.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        if (fg.call2 != null) {
            fg.call2!!.cancel()
        }

        val helper =
            ApiClient.getClientInvestor().create(ApiAuthHelper::class.java)
        fg.call2 =
            helper.requestOfferings(
                OfferingsFragment.NEXT_MONTH_OFFERINGS,
                nextMonthNextPage,
                search ?: ""
            )
        fg.call2!!.enqueue(object : CallBackManager<OfferingResponses>() {

            override fun onSuccess(any: OfferingResponses?, message: String) {
                /*More*/
                nextMonthMaxPage = ((any?.response?.totalRecords ?: 0) / 10.0).toInt()
                isNextMonthMore = (nextMonthNextPage < nextMonthMaxPage ||
                        (any?.response?.totalRecords ?: 0) > (nextMonthNextPage * 10))
                if (isNextMonthMore) {
                    fg.binding.tvMoreNextMonth.visibility = View.VISIBLE
                } else {
                    fg.binding.tvMoreNextMonth.visibility = View.GONE
                }

                /*Data*/
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.response?.recordsList.isNullOrEmpty()) {
                        fg.binding.gdNextMonth.visibility = View.VISIBLE
                        if (nextMonthNextPage == 1) {
                            fg.nextMonthAdapter.setData(getFilerData(any.response!!.recordsList))
                        } else if (nextMonthNextPage > 1) {
                            fg.nextMonthAdapter.addData(getFilerData(any.response!!.recordsList))
                        }
                        nextMonthNextPage++
                    } else {
                        if (nextMonthNextPage == 1) {
                            fg.nextMonthAdapter.clearData()
                        }
                    }
                } else {
                    if (nextMonthNextPage == 1) {
                        fg.nextMonthAdapter.clearData()
                    }
                }

                if (fg.nextMonthAdapter.dataList.isEmpty()) {
                    fg.binding.gdNextMonth.visibility = View.GONE
                }

                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {
                dismiss(isProgressDialog)
            }

        })
    }

    private fun dismiss(isProgressDialog: Boolean) {
        fg.isSearchProgress = false
        if (fg.binding.swpMain.isRefreshing) {
            fg.binding.swpMain.isRefreshing = false
        } else if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        noData()
    }

    private fun noData() {
        if (fg.searchKey.isNullOrEmpty()) {
            if (fg.nextMonthAdapter.dataList.isEmpty() &&
                fg.thisMonthAdapter.dataList.isEmpty()
            ) {
                fg.binding.tvupcoming.visibility = View.GONE
            } else {
                fg.binding.tvupcoming.visibility = View.VISIBLE
            }

            if (fg.offeringAdapter.dataList.isEmpty() &&
                fg.nextMonthAdapter.dataList.isEmpty() &&
                fg.thisMonthAdapter.dataList.isEmpty()
            ) {
                fg.binding.tvHeader.visibility = View.GONE
                fg.binding.rvOffering.visibility = View.GONE
                fg.binding.gdThisMonth.visibility = View.GONE
                fg.binding.gdNextMonth.visibility = View.GONE

                fg.binding.incNoData.root.visibility = View.VISIBLE
                fg.binding.incNoData.tvMessage.text =
                    fg.getString(R.string.no_offerings_available)
            } else {
                fg.binding.tvHeader.visibility = View.VISIBLE
                fg.binding.incNoData.root.visibility = View.GONE

                if (fg.thisMonthAdapter.dataList.isNotEmpty()) {
                    if (isThisMonthMore) {
                        fg.binding.tvMoreThisMonth.visibility = View.VISIBLE
                    }
                    if (isNextMonthMore) {
                        fg.binding.tvMoreNextMonth.visibility = View.VISIBLE
                    }
                    fg.binding.gdThisMonth.visibility = View.VISIBLE
                }
                if (fg.nextMonthAdapter.dataList.isNotEmpty()) {
                    fg.binding.gdThisMonth.visibility = View.VISIBLE
                }
            }
        } else {
            if (fg.offeringAdapter.dataList.isEmpty()) {
                fg.binding.tvHeader.visibility = View.GONE
                fg.binding.incNoData.root.visibility = View.VISIBLE
                fg.binding.incNoData.tvMessage.text =
                    fg.getString(R.string.no_offerings_available)
            } else {
                fg.binding.tvHeader.visibility = View.VISIBLE
                fg.binding.incNoData.root.visibility = View.GONE
            }
        }
    }
}

/*
if (offeringNextPage == 1) {
    //fresh search
    if (any?.responseStatus != null &&
        any.responseStatus == 1
    ) {
        if (!any.response?.recordsList.isNullOrEmpty()) {
            fg.offeringAdapter.setData(any.response!!.recordsList)
        } else {
            fg.offeringAdapter.clearData()
        }
    } else {
        fg.offeringAdapter.clearData()
    }
} else if (offeringNextPage > 1) {
    //More clicked for search
    if (any?.responseStatus != null &&
        any.responseStatus == 1
    ) {
        if (!any.response?.recordsList.isNullOrEmpty()) {

        }
    }
}*/
