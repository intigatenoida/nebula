package com.nibula.dashboard.ui.music
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nibula.dashboard.ui.music.vfragments.MyLikedFragment
import com.nibula.dashboard.ui.music.vfragments.MyPlayListFragment
import com.nibula.utils.SmartFragmentStatePagerAdapter
class MusicPagerAdapter(fn: FragmentManager) : SmartFragmentStatePagerAdapter(fn) {
    // Released and search music removed as the new XD design (21 December 2021 received new XD)
   /* override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                OfferingsFragment.newInstance()
            }
            1 -> {
                ReleasedFragment.newInstance()
            }
            else -> {
                MyPlayListFragment.newInstance()
            }
            }
    }*/
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                MyLikedFragment.newInstance()
            }
            1 -> {
                MyPlayListFragment.newInstance()
            }
            else -> {
                MyPlayListFragment.newInstance()
            }
        }
    }
    override fun getCount(): Int {
        return 2
    }
    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}