package com.nibula.dashboard.ui.home

import android.content.Context
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.dashboard.DashboardActivity
import com.nibula.databinding.ItemHomeRecentlyBlankBinding
import com.nibula.databinding.ItemSingleRecentUpdateNewBinding
import com.nibula.request_to_buy.navigate
import com.nibula.response.home.recentuploaded.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils


class RecentlyUpdatedAdapter(
    val fg: BaseFragment,
    val context: Context,
    val navigate: navigate,
    val itemWidth: Int
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>()/*, TimerCounter.TimerItemRemove */ {
    var listData: MutableList<ResponseCollection> = mutableListOf()
    lateinit var bindingBlankCell: ItemHomeRecentlyBlankBinding
    lateinit var binding: ItemSingleRecentUpdateNewBinding

    class BlankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class SingleImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    var thisMillisStart: Long = fg.millisStart

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 5) {
            /*  return BlankViewHolder(
                  LayoutInflater.from(context).inflate(
                      R.layout.item_home_recently_blank,
                      parent,
                      false
                  )
              )*/
            val inflater = LayoutInflater.from(parent.context)
            bindingBlankCell = ItemHomeRecentlyBlankBinding.inflate(inflater, parent, false)


            val holder = BlankViewHolder(bindingBlankCell.root)
            return holder

        } else if (viewType == 1) {

           /* return SingleImgViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_single_recent_update_new,
                    parent,
                    false
                )
            )*/


            val inflater = LayoutInflater.from(parent.context)
            binding = ItemSingleRecentUpdateNewBinding.inflate(inflater, parent, false)

            val holder = SingleImgViewHolder(binding.root)
            return holder
        } else {
            return BlankViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_home_recently_blank,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (listData.isEmpty()) {
            return 5
        }
        return 1
    }

    override fun getItemCount(): Int {
        if (listData.isEmpty()) {
            return 8
        }
        return listData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SingleImgViewHolder) {
            Log.d("OnBind", "TrendingList")
            val layoutParams = binding.cvImage.clImage.layoutParams
            layoutParams.width = itemWidth
            val data = listData[position]
            data.recordTitle?.let {
                binding.tvAlbumTitle.text = it.trim()
            }
         binding.tvArtistName.text = data.recordArtist.toString()
         binding.tvTokenAvailableText.text = "Token Available"
         binding.tvReleaseData.text = "Value Per Token"
            if (data.availableTokens != null) {
                val shareCount = data.availableTokens ?: 0.0
                val totalToken = data.totalTokens
                binding.tvTokenAvailable.text =
                    "${shareCount}/$totalToken "

            } else {

            }

            Glide
                .with(context)
                .load(data.recordImage)
                .placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.IMMEDIATE)
                .into(binding.cvImage.clImage)

            holder.itemView.setOnClickListener {
                var account = (context as DashboardActivity).account
                var selectedSessionTopic = (context as DashboardActivity).selectedSessionTopic
                var walletUri = (context as DashboardActivity).deeplinkPairingUri
                //navigate.gotorequestinvest(data.id)
                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL,
                        data.recordImage
                    )
                    putString(
                        AppConstant.ID,
                        data.id
                    )
                    putString(
                        AppConstant.CONNECTED_WALLET_ACCOUNT,
                        account
                    )

                    putString(
                        AppConstant.SELECTED_SESSION_TOPIC,
                        selectedSessionTopic
                    )

                    putString(
                        AppConstant.WALLET_URI,
                        walletUri
                    )
                }
                fg.navigate.goToNewOffering(bundle)
            }
            binding.tvAmountPerShare.text =
                "${data.currencyType ?: "$"}${CommonUtils.trimDecimalToTwoPlaces(data?.pricePerToken ?: 0.0)}"

            when (data.recordTypeId) {
                1 -> {
                    binding.cvImage.clAlbumInfo.visibility=View.VISIBLE
                }
                else->
                {
                    binding.cvImage.clAlbumInfo.visibility=View.GONE

                }
            }

        } else {
            //Static data ,we can add this data in model blank SKELTON model
          bindingBlankCell.tvBlankAvailable.setText("Available ")
            bindingBlankCell.tvAlbumTitleSingleBlank.text = "Drop title"
            bindingBlankCell.tvAvaShareBlank.text = "Artist"
            bindingBlankCell.tvTokenAvailableBlank.text = "00/300"
            bindingBlankCell.tvTokenAvailableBlankText.text = "Available Token"
            bindingBlankCell.tvAmountPerShareBlank.text = "$0"
            bindingBlankCell.tvReleaseDataBlank.text = "Value Per Token"
            val ss1 = SpannableString(context.getString(R.string.artist))
            ss1.setSpan(UnderlineSpan(), 0, ss1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            bindingBlankCell.tvAvaShareBlank.text = ss1

        }
    }

    fun updateList(data: ArrayList<ResponseCollection>) {
        if (listData.isEmpty()) {
            listData.addAll(data)
            //setRemainingTime(listData)
            //thisMillisStart = fg.millisStart
            notifyDataSetChanged()
        } else {
            val dataSize = listData.size
            listData.addAll(data)
            notifyItemRangeInserted(dataSize + 1, listData.size)
        }
    }

    fun setDummyData() {
        var temp = ResponseCollection()
        temp.viewType = 5
        listData.add(temp)

        temp = ResponseCollection()
        temp.viewType = 5
        listData.add(temp)

        temp = ResponseCollection()
        temp.viewType = 5
        listData.add(temp)

        temp = ResponseCollection()
        temp.viewType = 5
        listData.add(temp)
        notifyDataSetChanged()
    }

    fun clearList() {
        listData.clear()
    }

}