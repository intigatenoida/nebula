package com.nibula.dashboard.ui.music.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.counter.TimerCounter
import com.nibula.dashboard.ui.music.vfragments.OfferingsFragment
import com.nibula.databinding.ItemTrendingOfferBinding
import com.nibula.response.music.offerings.RecordDetailsData
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.LifecycleViewHolder
import com.nibula.utils.TimestampTimeZoneConverter



class OfferingsAdapter() : RecyclerView.Adapter<LifecycleViewHolder>(),
    TimerCounter.TimerItemRemove {

    private lateinit var context: Context
    private lateinit var fg: BaseFragment

    var dataList: MutableList<RecordDetailsData> = mutableListOf()
    var thisMillisStart: Long = 0L
    lateinit var binding:ItemTrendingOfferBinding

    /*var firstItemVisible: Int = -1
    var lastItemVisible: Int = -1*/

    constructor(fg: OfferingsFragment) : this() {
        this.fg = fg
        this.context = fg.requireContext()
        thisMillisStart = fg.millisStart
    }

    /*override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        val lm = recyclerView.layoutManager as GridLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    firstItemVisible = lm.findFirstVisibleItemPosition()
                    lastItemVisible = lm.findLastVisibleItemPosition()
                }
            }
        })
    }*/

    override fun onViewAttachedToWindow(holder: LifecycleViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.onAttached()
        if (holder is OfferingViewHolder) {
            holder.registerTimerLiveData()
        }
        /*if (firstItemVisible > -1 && lastItemVisible > -1) {
            if (holder.adapterPosition in firstItemVisible..lastItemVisible) {

            }
        }*/
    }

    override fun onViewDetachedFromWindow(holder: LifecycleViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.removeObserver(fg.baseViewModel.getTimeLv())
        holder.onDetached()
    }

    override fun onViewRecycled(holder: LifecycleViewHolder) {
        super.onViewRecycled(holder)
        holder.onRecycled()
    }

    override fun onTimerFinish(recordId: String, position: Int) {
       // println("onTimerFinish: recordId $recordId position $position")
        if (position >= 0) {
            fg.handler.postDelayed(Runnable {
                if (position < dataList.size) {
                    val record = dataList[position]
                    if (record.id.equals(recordId)) {
                       // println("onTimerFinish: MATCH recordId $recordId position $position")
                        dataList.removeAt(position)
                        notifyItemRemoved(position)
                        //notifyDataSetChanged()
                    } else {
                       // println("onTimerFinish: UN-MATCH record.id ${record.id} recordId $recordId position $position")
                    }
                }
            }, 1000)
        }
    }

    inner class OfferingViewHolder(vw: View) : LifecycleViewHolder(vw) {
        var tvTimer: AppCompatTextView? = vw.findViewById(R.id.tvTimeRemain)

        fun registerTimerLiveData() {
            //println("OfferingsAdapter: registerTimerLiveData $adapterPosition")
            registerTimerLiveData(
                fg.requireContext(),
                fg.baseViewModel.getTimeLv(),
                thisMillisStart,
                tvTimer,
                if (adapterPosition > -1 && adapterPosition < dataList.size) {
                    dataList[adapterPosition]
                } else {
                    null
                },
                this@OfferingsAdapter
            )
        }
    }

    fun setData(data: MutableList<RecordDetailsData>) {
        dataList = data
        //setDummyData(dataList)
        setRemainingTime(dataList)
        thisMillisStart = fg.millisStart
        notifyDataSetChanged()
    }

    private fun setRemainingTime(dataList: MutableList<RecordDetailsData>) {
        for (obj in dataList) {
            obj.remainingMillis = obj.timeLeftToRelease ?: 0
            obj.tltr = obj.timeLeftToRelease ?: 0
            obj.rId = obj.id ?: ""
        }
    }

    fun addData(data: MutableList<RecordDetailsData>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        dataList.clear()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LifecycleViewHolder {
        /*val holder = OfferingViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_trending_offer,
                parent,
                false
            )
        )*/

        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemTrendingOfferBinding.inflate(inflater, parent, false)
        val holder = OfferingViewHolder(binding.root)
        holder.itemView.setOnClickListener {
            if (holder.adapterPosition in 0..dataList.size) {
                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL,
                        dataList[holder.adapterPosition].recordImage
                    )
                    putString(
                        AppConstant.ID,
                        dataList[holder.adapterPosition].id
                    )
                }
                //fg.navigate.gotorequestinvest(dataList[holder.adapterPosition].id)
                fg.navigate.goToNewOffering(bundle)
            }
        }
        return holder
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: LifecycleViewHolder, position: Int) {
        if (holder is OfferingViewHolder) {
            val data = dataList[position]

            // Album Tittle
      binding.tvAlbumTitle.text = data.recordTitle ?: ""

            //Shares Count
            if (data.sharesAvailable != null) {
                val shareCount = data.sharesAvailable ?: 0.0
                val shareString =
                    if (shareCount.toInt() <= 1) context.getString(R.string.share_available) else context.getString(
                        R.string.shares_available
                    )
                val temp = CommonUtils.setFormattedNumber(shareCount.toInt())
                binding.tvAvaShare.text =
                    CommonUtils.setSpannable(context,"$temp $shareString", 0, temp.length + 1)
            } else {
               binding.tvAvaShare.text =
                    CommonUtils.setSpannable(context,
                        "0 ${context.getString(R.string.share_available)}",
                        0,
                        1
                    )
            }

           binding.tvAmountPerShare.text =
                "${data.currencyType?:"USD"} ${CommonUtils.trimDecimalToTwoPlaces(data.valuePerShareInDollars ?: 0.0)} ${context.getString(
                    R.string.per_share
                )}"

            if (!data.offeringEndDate.isNullOrEmpty()) {
                binding.tvReleaseData.visibility = VISIBLE
                val value2 =
                    "${context.getString(R.string.release_date)} ${TimestampTimeZoneConverter.convertToLocalDate(
                        data.offeringEndDate!!, TimestampTimeZoneConverter.UTC_TIME, "dd/MM/yyyy"
                    )}"
               binding.tvReleaseData.text =
                    CommonUtils.setSpannable(context,value2, value2.indexOf(":") + 1, value2.length)
            } else {
               binding.tvReleaseData.visibility = GONE
            }

            Glide
                .with(context)
                .load(data.recordImage)
                .placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.IMMEDIATE)
                .into(binding.cvImage.clImage)

            val temp = TimestampTimeZoneConverter.convertToMilliseconds(
                data.timeLeftToRelease ?: 0
            )
            if (temp > TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                val time =
                    TimestampTimeZoneConverter.getDaysAndHourDifference(
                        context,
                        temp
                    )
                binding.tvTimeRemain.visibility = VISIBLE
            binding.tvTimeRemain.text = time
            } else {
               binding.tvTimeRemain.visibility = View.INVISIBLE
                if (temp > 0 &&
                    !data.isFinish
                ) {
                    val workingTime =
                        TimestampTimeZoneConverter.getDaysAndHourDifference(
                            context,
                            data.remainingMillis
                        )
                  binding.tvTimeRemain.text = workingTime
                   binding.tvTimeRemain.visibility = VISIBLE
                }
            }
        }
    }

}
