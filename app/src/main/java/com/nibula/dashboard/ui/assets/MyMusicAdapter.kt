package com.nibula.dashboard.ui.assets

import android.content.Context
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.counter.TimerCounter
import com.nibula.databinding.ItemMyMusicBinding
import com.nibula.response.musicprofile.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.LifecycleViewHolder
import com.nibula.utils.TimestampTimeZoneConverter


class MyMusicAdapter(val fg: BaseFragment) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), TimerCounter.TimerItemRemove {

    var listData = mutableListOf<ResponseCollection>()

    var context: Context = fg.requireContext()

    var thisMillisStart: Long = fg.millisStart
    private lateinit var binding:ItemMyMusicBinding

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder is MyMusicOfferingViewHolder) {
            println("MyMusicAdapter: onViewAttachedToWindow ${holder.adapterPosition}")
            holder.onAttached()
            holder.registerTimerLiveData()
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is MyMusicOfferingViewHolder) {
            println("MyMusicAdapter: onViewDetachedFromWindow ${holder.adapterPosition}")
            holder.removeObserver(fg.baseViewModel.getTimeLv())
            holder.onDetached()
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
      //  clearGlide(holder)
        if (holder is MyMusicOfferingViewHolder) {
            holder.onRecycled()
        }
    }

    override fun onTimerFinish(recordId: String, position: Int) {
        //println("onTimerFinish: recordId $recordId position $position")
        if (position >= 0) {
            fg.handler.postDelayed(Runnable {
                if (position < listData.size) {
                    val record = listData[position]
                    if (record.id.equals(recordId)) {
                        //println("onTimerFinish: MATCH recordId $recordId position $position")
                        listData.removeAt(position)
                        notifyItemRemoved(position)
                        //notifyDataSetChanged()
                    } else {
                        //println("onTimerFinish: UN-MATCH record.id ${record.id} recordId $recordId position $position")
                    }
                }
            }, 1000)
        }
    }

    inner class MyMusicOfferingViewHolder(itemView: View) : LifecycleViewHolder(itemView) {
        var tvTimeRemain: AppCompatTextView? = itemView.findViewById(R.id.tvTimeRemain)

        fun registerTimerLiveData() {
            val data = if (adapterPosition > -1 && adapterPosition < listData.size) {
                listData[adapterPosition]
            } else {
                null
            }
            //println("MyMusicAdapter: registerTimerLiveData $adapterPosition")
            registerTimerLiveData(
                fg.requireContext(),
                fg.baseViewModel.getTimeLv(),
                thisMillisStart,
                tvTimeRemain,
                data,
                this@MyMusicAdapter
            )
        }
    }

    inner class MyMusicViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setData(data: MutableList<ResponseCollection>) {
        listData = data
        //setDummyData(listData)
        setRemainingTime(listData)
        thisMillisStart = fg.millisStart
        notifyDataSetChanged()
    }

    private fun setRemainingTime(dataList: MutableList<ResponseCollection>) {
        for (obj in dataList) {
            if (obj.recordStatusTypeId == AppConstant.initialoffering) {
                obj.remainingMillis = obj.timeLeftToRelease ?: 0
                obj.tltr = obj.timeLeftToRelease ?: 0
                obj.rId = obj.id ?: ""
            }
        }
    }

    fun addData(data: MutableList<ResponseCollection>) {
        //setDummyData(data)
        setRemainingTime(data)
        listData.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        listData.clear()
    }

    override fun getItemViewType(position: Int): Int {
        return if (listData.isNotEmpty() &&
            listData[position].recordStatusTypeId == AppConstant.initialoffering
        ) {
            1
        } else {
            2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
      /*  val view = LayoutInflater.from(context).inflate(
            R.layout.item_my_music,
            parent,
            false
        )*/
        val inflater = LayoutInflater.from(parent.context)
         binding = ItemMyMusicBinding.inflate(inflater, parent, false)


        val holder = if (viewType == 1) {
            MyMusicOfferingViewHolder(binding.root)
        } else {
            MyMusicViewHolder(binding.root)
        }
        holder.itemView.setOnClickListener {
            if (holder.adapterPosition in 0..listData.size) {
                //fg.navigate.gotorequestinvest(listData[holder.adapterPosition].id)
                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL,
                        listData[holder.adapterPosition].recordImage
                    )
                    putString(
                        AppConstant.ID,
                        listData[holder.adapterPosition].id
                    )
                }
                fg.navigate.goToNewOffering(bundle)
            }
        }
        return holder
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = listData[position]

       binding.layoutImage.clImage.setImageResource(R.drawable.nebula_placeholder)

        Glide
            .with(context)
            .load(data.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.layoutImage.clImage)

        when (data.recordTypeId) {
            1 -> {
                binding.album.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
            2 -> {
            binding.album.text =
                    "${context.getString(R.string.single)}: ${data.recordTitle}"
            }
            else -> {
                binding.album.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
        }

       binding.artist.text = "${context.getString(R.string.artist)}: ${data.artistsName}"

        val inv = data.totalNoOfInvesters ?: 0
        val temp = "$inv "
        val spn1 = SpannableStringBuilder()
        spn1.append(
            CommonUtils.setSpannable(context,
                "$inv ",
                0,
                temp.length
            )
        )
        if (inv <= 1) {
            spn1.append("${context.getString(R.string.investor_)}")

        } else {
            spn1.append("${context.getString(R.string.investors)}")
        }
       binding.tvInvested.text = spn1

        val earn = CommonUtils.trimDecimalToTwoPlaces(data.totalInvestedAmount ?: 0.0)
       binding.tvEarned.text = "${data.currencyType} $earn ${context.getString(R.string.raised)}"

        when (data.recordStatusTypeId) {
            AppConstant.released -> {
                binding.released.visibility = View.VISIBLE
                binding.tvTimeRemain.visibility = View.GONE
                binding.tvReleased.visibility = View.GONE
            }
            else -> {

                binding.released.visibility = View.GONE
                binding.tvTimeRemain.visibility = View.VISIBLE
                binding.tvReleased.visibility = View.VISIBLE

                /*Counter*/
                val temp = TimestampTimeZoneConverter.convertToMilliseconds(
                    data.timeLeftToRelease ?: 0
                )
                if (temp > TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                    val time =
                        TimestampTimeZoneConverter.getDaysAndHourDifference(
                            context,
                            temp
                        )
                    binding.tvTimeRemain.visibility = View.VISIBLE
                    binding.tvTimeRemain.text = time
                } else {
                    binding.tvTimeRemain.visibility = View.INVISIBLE
                    if (temp > 0 &&
                        !data.isFinish
                    ) {
                        val workingTime =
                            TimestampTimeZoneConverter.getDaysAndHourDifference(
                                context,
                                data.remainingMillis
                            )
                        binding.tvTimeRemain.text = workingTime
                        binding.tvTimeRemain.visibility = View.VISIBLE
                    }
                }

                binding.tvReleased.text = data.offeringEndDate?.let {
                    TimestampTimeZoneConverter.convertToLocalDate(
                        it, TimestampTimeZoneConverter.UTC_TIME, "dd/MM/yyyy"
                    )
                }?.let { releaseDateString(it) }
            }
        }


    }

    private fun releaseDateString(date: String): SpannableString {
        val ss1 = SpannableString("${context.getString(R.string.release_date)} $date")
        ss1.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
            context.getString(R.string.release_date).length + 1,
            ss1.length,
            0
        )
        return ss1
    }


}