package com.nibula.dashboard.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.dashboard.ConstantsNavTabs
import com.nibula.databinding.ItemArtistBinding
import com.nibula.request_to_buy.navigate
import com.nibula.response.topoffertopartist.ArtistInfoData
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils

class ArtistAdapter(val context: Context, val navigate: navigate, val itemWidth: Int) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var dataList: MutableList<ArtistInfoData>
    lateinit var binding: ItemArtistBinding

    class ArtistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class SpaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class BlankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 1) {

            val inflater = LayoutInflater.from(parent.context)
             binding = ItemArtistBinding.inflate(inflater, parent, false)


            val holder = ArtistViewHolder(binding.root)

            // ArtistViewHolder(LayoutInflater.from(context).inflate(R.layout.item_artist, parent, false))
            holder
        } else if (viewType == 3) {
            BlankViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_home_artist_blank,
                    parent,
                    false
                )
            )
        } else {
            SpaceViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_space_artist,
                    parent,
                    false
                )
            )
        }

    }

    fun setData(data: MutableList<ArtistInfoData>) {
        val temp = ArtistInfoData()
        temp.viewType = 2
        data.add(0, temp)
        dataList = data
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (::dataList.isInitialized) dataList[position].viewType ?: 1 else 3
    }

    override fun getItemCount(): Int {
        return if (::dataList.isInitialized) dataList.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ArtistViewHolder) {
            val layoutParams = binding.ivArtist.layoutParams
            layoutParams.width = itemWidth

            val data = dataList[position]
            binding.txtName.text = data.userName
            CommonUtils.loadImageNoThumbnail(
                context,
                data.userPic ?: "",
                binding.ivArtist,
                R.drawable.ic_record_user_place_holder
            )
           binding.parentLayout.setOnClickListener {
                if (data.isSelfUser == true) {
                    if (PrefUtils.getValueFromPreference(context, PrefUtils.USER_ID).isNotEmpty()) {
                        navigate.changeTab(ConstantsNavTabs.NAV_TAB_PROFILE)
                    }
                } else {
                    navigate.openTopArtist(data.id ?: "", 4)//user Type Id 4 for artist
                }
            }
        }
    }


    fun setBlankData() {
        if (::dataList.isInitialized.not()) {
            dataList = ArrayList()
        }
        val temp = ArtistInfoData()
        temp.viewType = 2
        dataList.add(temp)
        for (i in 0 until 7) {
            val temp = ArtistInfoData()
            temp.viewType = 3
            dataList.add(temp)
        }
        notifyDataSetChanged()
    }
}