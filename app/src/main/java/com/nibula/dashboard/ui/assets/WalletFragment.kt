package com.nibula.dashboard.ui.assets

import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.tabs.TabLayout
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.dashboard.DashboardActivity
import com.nibula.dashboard.ui.assets.helper.AssetsFragmentHelper
import com.nibula.databinding.FragmentAssetsBinding
import com.nibula.pending_request.PendingRequestFragment
import com.nibula.request_to_invest.paypal.PayPaypalPayoutsActivity
import com.nibula.transcation.TranscationHistoryFragment
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import com.nibula.withdraw.WithdrawWithPaypal

class WalletFragment : BaseFragment(), AssesInvestmentFragment.onAssetAmountUpdateListener {

    companion object {
        fun newInstance() = WalletFragment()
        private const val REQUEST_CODE = 123

    }

    lateinit var adapter: AssetsPagerAdapter
    var isDefaultAddressAvailable = false
    var currentBalance = 0.0
    lateinit var rootView: View
    lateinit var helper: AssetsFragmentHelper
    var walletAddress: String = ""
    lateinit var binding: FragmentAssetsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_assets, container, false)
            binding = FragmentAssetsBinding.inflate(inflater, container, false)
            rootView = binding.root
            intiUI()
        }
        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        helper.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        helper.getBalance(true)
        if (binding.header.imgNotificationCount.visibility != View.VISIBLE) {
            CommonUtils.setNotificationDot(requireContext(), binding.header.imgNotificationCount)
        }

        /*    walletAddress =
                PrefUtils.getValueFromPreference(requireContext(), PrefUtils.CONNECTED_WALLET_ADDRESS)
            if (walletAddress.isEmpty()) {
                rootView.clWalletConnect.visibility=View.VISIBLE
                rootView.tvConnectText.visibility=View.VISIBLE
            }
            else{
                rootView.clWalletConnect.visibility=View.GONE
                rootView.tvConnectText.visibility=View.GONE
            }*/

    }

    private fun intiUI() {
        binding.tvCurrentBalance.text = "$0"
        binding.totalEarning.text = "$0"
        binding.royaltyAmt.text = "$0"
        binding.cashbackAmt.text = "$0"
        binding.raisedAmt.text = "$0"

        helper = AssetsFragmentHelper(this)

        setAdapter()
        binding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }


        binding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }

        val color = ContextCompat.getColor(requireActivity(), R.color.colorAccent)
        binding.tvPending.text =
            CommonUtils.underlineSpannable(getString(R.string.pending), color)
        binding.tvTranHistory.text =
            CommonUtils.underlineSpannable(getString(R.string.transaction_history), color)
        binding.tvWithdrawFunds.text =
            CommonUtils.underlineSpannable(getString(R.string.withdraw_funds), color)

        binding.tvTranHistory.setOnClickListener {
            navigate.pushFragments(TranscationHistoryFragment.newInstance())
        }

        binding.clWalletConnect.setOnClickListener {
            navigate.uploadRecord()
        }
        binding.tvWithdrawFunds.setOnClickListener {

               navigate.pushFragments(WithdrawWithPaypal.newInstance().apply {
                    arguments = Bundle().apply {
                        putDouble(AppConstant.AMOUNT, this@WalletFragment.currentBalance)
                    }
                })
            /*    if (isDefaultAddressAvailable) {
                    navigate.pushFragments(WithdrawFragment.newInstance().apply {
                        arguments = Bundle().apply {
                            putDouble(AppConstant.AMOUNT, this@WalletFragment.currentBalance)
                        }
                    })
                } else {
                    val fragment = AddBankFragment.newInstance().apply {
                        arguments = Bundle().apply {
                            putBoolean(AppConstant.ADD_BANK, true)
                        }
                    }
                    navigate.pushFragments(fragment)
                }*/

        }

        binding.tvPending.setOnClickListener {
            navigate.pushFragments(PendingRequestFragment.newInstance())
        }

    }

    private fun setAdapter() {
        adapter = AssetsPagerAdapter(requireActivity().supportFragmentManager, navigate, this)
        binding.vpMain.adapter = adapter
//      rootView.vp_main.offscreenPageLimit = 2
        binding.vpMain.offscreenPageLimit = 1
        binding.tabLayout.setupWithViewPager(binding.vpMain)
        prepareTab()
    }

    private fun prepareTab() {
        val investmentTab = binding.tabLayout.getTabAt(0)
        addTabHere(
            investmentTab,
            R.layout.layout_tab_my_investments,
            getString(R.string.my_investments_new),
            R.font.pt_sans_bold, Typeface.NORMAL, VISIBLE
        )
//        val musicTab = rootView.tabLayout.getTabAt(1)
//        addTabHere(
//            musicTab,
//            R.layout.layout_tab_my_music,
//            getString(R.string.my_music),
//            R.font.pt_sans, Typeface.NORMAL,
//            INVISIBLE
//        )
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                changeTabAppearance2(tab, View.INVISIBLE)
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                changeTabAppearance2(tab, View.VISIBLE)

            }

        })
        wrapTabIndicatorToTitle(binding.tabLayout, 0, 0)
    }

    private fun addTabHere(
        tab: TabLayout.Tab?,
        layoutId: Int,
        title: String,
        font: Int,
        textStyle: Int,
        indicatorVisibility: Int
    ) {
        if (tab == null) {
            return
        }
        val customTabView: View =
            LayoutInflater.from(requireContext()).inflate(layoutId, null)

        customTabView.findViewById<View>(R.id.v_bottom_border).visibility = indicatorVisibility
        val tabTitle = customTabView.findViewById<TextView>(R.id.tv_tab_title)
        tabTitle.text = title

        val tf = ResourcesCompat.getFont(requireContext(), font)
        tabTitle.setTypeface(tf, textStyle)

        tab.customView = customTabView
        tab.tag = title

        if (indicatorVisibility == VISIBLE) {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        } else {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
        }
    }

    override fun onAssetAmountUpdate(amount: String) {
        binding.totalAmount.text = amount
    }

    fun manageWithdrawalUI() {
        if (currentBalance == 0.0) {
            binding.tvWithdrawFunds.alpha = 0.5f
            binding.tvWithdrawFunds.isEnabled = false
        } else {

            binding.tvWithdrawFunds.alpha = 1f
            binding.tvWithdrawFunds.isEnabled = true
        }
    }


}