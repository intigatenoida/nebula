package com.nibula.dashboard.ui.assets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.dashboard.ui.assets.helper.AssetsInvestmentHelper
import com.nibula.databinding.AssestInvestmentFragmentBinding
import com.nibula.response.myinvestmentresponse.MyInvestmentResponse
import com.nibula.user.profile.investments.InvestmentViewModel
import retrofit2.Call


class AssesInvestmentFragment : BaseFragment() {

    interface onAssetAmountUpdateListener {
        fun onAssetAmountUpdate(amount: String)
    }

    lateinit var listener: onAssetAmountUpdateListener


    companion object {
        fun newInstance() = AssesInvestmentFragment()
    }

    private lateinit var viewModel: InvestmentViewModel

    lateinit var rootView: View
    lateinit var adapter: AssetsInvestmentAdapter
     lateinit var helper: AssetsInvestmentHelper

    var call: Call<MyInvestmentResponse>? = null

    var isDataAvailable = false
    var isLoading = true

    var totalAmount = 0.0

     lateinit var binding: AssestInvestmentFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.assest_investment_fragment, container, false)
            binding = AssestInvestmentFragmentBinding.inflate(inflater, container, false)
            rootView=binding.root
            intiUI()
        }
        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(this).get(InvestmentViewModel::class.java)
        binding.rvInvestors.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            helper.myInvestmentTokenList(true)
        }
    }

    override fun onStop() {
        super.onStop()
        if (call != null &&
            !call!!.isExecuted
        ) {
            call!!.cancel()
        }
    }

    private fun intiUI() {
        helper = AssetsInvestmentHelper(this)

        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvInvestors.layoutManager = layoutManager
        adapter = AssetsInvestmentAdapter(this)
        binding.rvInvestors.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    //  helper.getInvestmentListing("", false)
                    helper.myInvestmentTokenList(false)
                }
            }
        })

        binding.swipeInvest.setOnRefreshListener {
            helper.investmentNextPage = 1
            helper.investmentMaxPage = 0
            //helper.getInvestmentListing("", false)
            helper.myInvestmentTokenList(false)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.rvInvestors.adapter = null
    }

    fun updateTotalAmount(totalAmt: String) {
        if (::listener.isInitialized) {
            listener.onAssetAmountUpdate(amount = totalAmt)
        }
    }

    fun updateListener(listener: onAssetAmountUpdateListener) {
        this.listener = listener
    }
}

