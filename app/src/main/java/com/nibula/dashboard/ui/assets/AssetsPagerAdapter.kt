package com.nibula.dashboard.ui.assets
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nibula.request_to_buy.navigate
import com.nibula.utils.SmartFragmentStatePagerAdapter

class AssetsPagerAdapter(
	val activity: FragmentManager,
	val navigate: navigate,
	val listener: AssesInvestmentFragment.onAssetAmountUpdateListener
) : SmartFragmentStatePagerAdapter(activity) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                val fragment = AssesInvestmentFragment.newInstance()
				fragment.updateListener(listener)
                return fragment
            }
            else -> {
                AssetMusicFragment.newInstance()
            }
        }
    }

    override fun getCount(): Int {
        return 1
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }

}