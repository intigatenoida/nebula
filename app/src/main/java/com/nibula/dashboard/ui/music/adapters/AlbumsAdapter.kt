package com.nibula.dashboard.ui.music.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.databinding.ItemUpcomingOfferBinding
import com.nibula.request_to_buy.navigate
import com.nibula.response.music.offerings.RecordDetailsData


class AlbumsAdapter(val context: Context, val navigate: navigate) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: ItemUpcomingOfferBinding

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    lateinit var dataList: MutableList<RecordDetailsData>

    fun setData(data: MutableList<RecordDetailsData>) {
        dataList = data
        notifyDataSetChanged()
    }

    fun addData(data: MutableList<RecordDetailsData>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        dataList.clear()
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemUpcomingOfferBinding.inflate(inflater, parent, false)


        val holder = ViewHolder(binding.root)
        /* return ViewHolder(
             LayoutInflater.from(context).inflate(
                 R.layout.item_upcoming_offer,
                 parent,
                 false
             )
         )*/
        return holder
    }

    override fun getItemCount(): Int {
        return if (::dataList.isInitialized) dataList.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val temp = dataList[position]

        if (!temp.recordType.isNullOrEmpty()) {
            binding.tvAlbumTitle.text = temp.recordTitle
        } else {
            binding.tvAlbumTitle.text = ""
        }

        if (!temp.artistName.isNullOrEmpty()) {
            binding.tvAvaShare.text = "by ${temp.artistName}"
        } else {
            binding.tvAvaShare.text = ""
        }

        Glide.with(context).load(temp.recordImage).placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
            .into(binding.cvImage.clImage)

    }

}