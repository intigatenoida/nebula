package com.nibula.dashboard.ui.music

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.dashboard.ui.music.vfragments.MyLikedFragment
import com.nibula.dashboard.ui.music.vfragments.MyPlayListFragment

import com.nibula.databinding.FragmentMusicBinding
import com.nibula.user.login.LoginActivity
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.RequestCode
import com.nibula.utils.RequestCode.LOGIN_REQUEST_CODE_2


class MusicFragment : BaseFragment(){

    companion object {
        fun newInstance() = MusicFragment()
    }

    private lateinit var root: View
    private lateinit var adapter: MusicPagerAdapter
    var verticalOffset = -1

    lateinit var binding:FragmentMusicBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::root.isInitialized) {
           // root = inflater.inflate(R.layout.fragment_music, container, false)
            binding= FragmentMusicBinding.inflate(inflater,container,false)
            root=binding.root
            initUI()
        }
        return root
    }

    private fun initUI() {
        setAdapter()
        binding.musicAppBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            this.verticalOffset = verticalOffset
        })
        binding.edSearchMusic.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                when (binding.tabLayoutMusic.selectedTabPosition) {
                    /*  0 -> {
                          //Offering
                          val fg =
                              adapter.getRegisteredFragment(root.tabLayoutMusic.selectedTabPosition) as OfferingsFragment?
                          if (fg != null) {
                              if (!s.isNullOrEmpty()) {
                                  root.img_close.visibility = VISIBLE
                                  if (s.toString().length > 1) {
                                      fg.clearOfferingPageCount()
                                      fg.isSearchProgress = true
                                      fg.searchKey = s.toString()
                                      fg.requestSearch()
                                  }
                              } else {
                                  if (!fg.searchKey.isNullOrEmpty()) {
                                      root.img_close.visibility = GONE
                                      fg.clearOfferingPageCount()
                                      fg.isSearchProgress = true
                                      fg.searchKey = ""
                                      fg.requestSearch()
                                  }
                              }
                          }
                      }*/

/*
                    1 -> {
                        val fg =
                            adapter.getRegisteredFragment(root.tabLayoutMusic.selectedTabPosition) as MyPlayListFragment?
                        if (fg != null) {
                            if (!s.isNullOrEmpty()) {
                                root.img_close.visibility = VISIBLE
                                if (s.toString().length > 1) {
                                    fg.clearReleasedPageCount()
                                    fg.isSearchProgress = true
                                    fg.searchKey = s.toString()
                                    fg.requestSearch()
                                }
                            } else {
                                if (!fg.searchKey.isNullOrEmpty()) {
                                    root.img_close.visibility = GONE
                                    fg.clearReleasedPageCount()
                                    fg.isSearchProgress = true
                                    fg.searchKey = ""
                                    fg.requestSearch()
                                }
                            }
                        }
                    }
*/
                    0 -> {
                        val fg =
                            adapter.getRegisteredFragment(binding.tabLayoutMusic.selectedTabPosition) as MyLikedFragment?
                        if (fg != null) {
                            if (!s.isNullOrEmpty()) {
                                binding.imgClose.visibility = VISIBLE
                                if (s.toString().length > 1) {
                                    fg.clearReleasedPageCount()
                                    fg.isSearchProgress = true
                                    fg.searchKey = s.toString()
                                }
                            } else {
                                if (!fg.searchKey.isNullOrEmpty()) {
                                    binding.imgClose.visibility = GONE
                                    fg.clearReleasedPageCount()
                                    fg.isSearchProgress = true
                                    fg.searchKey = ""
                                }
                            }
                        }
                    }
                }

            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        binding.imgClose.setOnClickListener {
            binding.edSearchMusic.setText("")
            searchClear()
        }

        binding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }

        binding.header.imgLogo.setOnClickListener {
            CommonUtils.toggelDebug(requireContext())
        }
    }

    private fun setAdapter() {
        adapter = MusicPagerAdapter(requireActivity().supportFragmentManager)
        binding.viewPagerMusic.adapter = adapter
        binding.viewPagerMusic.offscreenPageLimit = 2
        binding.tabLayoutMusic.setupWithViewPager(binding.viewPagerMusic)
        prepareTab()
    }

    private fun prepareTab() {
        val offeringsTab = binding.tabLayoutMusic.getTabAt(0)
        addTab3(
            offeringsTab, getString(R.string.liked_song),
            R.font.pt_sans_bold, Typeface.NORMAL, VISIBLE
        )
        val releasedTab = binding.tabLayoutMusic.getTabAt(1)
        addTab3(
            releasedTab, getString(R.string.owned_song),
            R.font.pt_sans, Typeface.NORMAL, INVISIBLE
        )
       /* val myPlaylistTab = binding.tabLayoutMusic.getTabAt(2)
        addTab3(
            myPlaylistTab, getString(R.string.my_playlist),
            R.font.pt_sans, Typeface.NORMAL, INVISIBLE
        )*/

        binding.tabLayoutMusic.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                expandAppBarLayout()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                changeTabAppearanceNew(tab, INVISIBLE)
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab == null) {
                    return
                }
                expandAppBarLayout()
                changeTabAppearanceNew(tab, VISIBLE)
                val searchKey = binding.edSearchMusic.text.toString()
                if (searchKey.isEmpty()) {
                    searchClear()
                }

                if (tab.position == 2 &&
                    !CommonUtils.isLogin(requireActivity())
                ) {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    intent.putExtra(AppConstant.ISFIRSTTIME, false)
                    startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE_2)
                }
            }
        })
        wrapTabIndicatorToTitle1(binding.tabLayoutMusic)
    }

    private fun expandAppBarLayout() {
        if (verticalOffset != 0) {
            binding.musicAppBarLayout.setExpanded(true, true)
        }
    }

    fun addTab3(
        tab: TabLayout.Tab?,
        title: String,
        font: Int,
        textStyle: Int,
        indicatorVisibility: Int
    ) {
        if (tab == null) {
            return
        }
        val customTabView: View =
            LayoutInflater.from(requireContext()).inflate(R.layout.layout_tab_2, null)
        val tabTitle = customTabView.findViewById<TextView>(R.id.tv_tab_title)
        tabTitle.text = title

        val tf = ResourcesCompat.getFont(requireContext(), font)
        tabTitle.setTypeface(tf, textStyle)

        tab.customView = customTabView
        tab.tag = title

        if (indicatorVisibility == VISIBLE) {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            tabTitle.setBackgroundResource(R.drawable.bg_blue_shape)
        } else {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
            tabTitle.setBackgroundResource(0)


        }
    }

    fun wrapTabIndicatorToTitle1(tabLayout: TabLayout) {
        val tabStrip = tabLayout.getChildAt(0)
        if (tabStrip is ViewGroup) {
            val childCount = tabStrip.childCount
            for (i in 0 until childCount) {
                val tabView = tabStrip.getChildAt(i)
                //set minimum width to 0 for instead for small texts, indicator is not wrapped as expected
                tabView.minimumWidth = 0
                // set padding to 0 for wrapping indicator as title
                tabView.setPadding(0, tabView.paddingTop, 0, tabView.paddingBottom)
                // setting custom margin between tabs
                if (tabView.layoutParams is ViewGroup.MarginLayoutParams) {
                    val layoutParams =
                        tabView.layoutParams as ViewGroup.MarginLayoutParams
                    if (i == 0) { // left
                        settingMargin(layoutParams, CommonUtils.dpToPx(requireContext(), 21), 0)
                    } else if (i == childCount - 1) { // right
                        settingMargin(layoutParams, 0, CommonUtils.dpToPx(requireContext(), 21))
                    } else { // internal
                        settingMargin(
                            layoutParams,
                            CommonUtils.dpToPx(requireContext(), 40),
                            CommonUtils.dpToPx(requireContext(), 40)
                        )
                    }
                }
            }
            tabLayout.requestLayout()
        }
    }

    fun searchClear() {
        when (binding.tabLayoutMusic.selectedTabPosition) {
            0 -> {
                val fg =
                    adapter.getRegisteredFragment(binding.tabLayoutMusic.selectedTabPosition) as MyLikedFragment?
                if (fg != null && !fg.searchKey.isNullOrEmpty()) {
                    fg.clearReleasedPageCount()
                    fg.isSearchProgress = true
                    fg.searchKey = ""
                }
            }
            // Released Tab has been removed as per the new XD design
            /*    1 -> {
                    val fg =
                        adapter.getRegisteredFragment(root.tabLayoutMusic.selectedTabPosition) as ReleasedFragment?
                    if (fg != null && !fg.searchKey.isNullOrEmpty()) {
                        fg.clearReleasedPageCount()
                        fg.isSearchProgress = true
                        fg.searchKey = ""
                        fg.requestSearch()
                    }
                }*/
            1 -> {
                val fg =
                    adapter.getRegisteredFragment(binding.tabLayoutMusic.selectedTabPosition) as MyPlayListFragment?
                if (fg != null && !fg.searchKey.isNullOrEmpty()) {
                    fg.clearReleasedPageCount()
                    fg.isSearchProgress = true
                    fg.searchKey = ""
                    fg.requestSearch()
                }
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            LOGIN_REQUEST_CODE_2 -> {
                if (resultCode == Activity.RESULT_OK) {
                    val fg =
                        adapter.getRegisteredFragment(binding.tabLayoutMusic.selectedTabPosition) as MyLikedFragment?
                    fg?.onResume2()
                    baseViewModel.refresh(true)
                } else {
                    binding.viewPagerMusic.currentItem = 0
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (binding.header.imgNotificationCount.visibility != VISIBLE) {
            CommonUtils.setNotificationDot(requireContext(), binding.header.imgNotificationCount)
        }
    }




}
