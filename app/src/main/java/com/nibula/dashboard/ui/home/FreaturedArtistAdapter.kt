package com.nibula.dashboard.ui.home

import android.animation.ObjectAnimator
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.transition.ViewPropertyTransition
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.counter.TimerCounter
import com.nibula.databinding.FreaturedItemBlankListBinding
import com.nibula.databinding.LayoutFreaturedListBinding
import com.nibula.request_to_buy.navigate
import com.nibula.response.artists.FreaturedArtistResponse
import com.nibula.utils.AppConstant
import com.nibula.utils.LifecycleViewHolder
import kotlin.math.roundToInt

class FreaturedArtistAdapter(
    val fg: BaseFragment, val navigate: navigate, val itemHeight: Int
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), TimerCounter.TimerItemRemove {
    var context: Context = fg.requireContext()
    lateinit var dataList: MutableList<FreaturedArtistResponse>
    var thisMillisStart: Long = fg.millisStart
    lateinit var binding: LayoutFreaturedListBinding
    lateinit var blankBinding: FreaturedItemBlankListBinding

    class SpaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class BlankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder is FreaturedViewHolder) {
            holder.onAttached()
            holder.registerTimerLiveData()

        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is FreaturedViewHolder) {
            holder.removeObserver(fg.baseViewModel.getTimeLv())
            holder.onDetached()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 1) {
            /*val holder = FreaturedViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.layout_freatured_list, parent, false
                )
            )*/
            val inflater = LayoutInflater.from(parent.context)
            binding = LayoutFreaturedListBinding.inflate(inflater, parent, false)


            val holder = FreaturedViewHolder(binding.root)
            holder
        } else if (viewType == 3) {
            /*  BlankViewHolder(
                  LayoutInflater.from(context).inflate(
                      R.layout.freatured_item_blank_list, parent, false
                  )
              )*/
            val inflater = LayoutInflater.from(parent.context)
            blankBinding = FreaturedItemBlankListBinding.inflate(inflater, parent, false)
            val holder = BlankViewHolder(blankBinding.root)
            holder
        } else {
            SpaceViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_space_home_trending, parent, false
                )
            )
        }
    }

    fun setData(data: MutableList<FreaturedArtistResponse>) {
        dataList = data
        setRemainingTime(dataList)
        thisMillisStart = fg.millisStart
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (::dataList.isInitialized) dataList[position].viewType ?: 1 else 3
    }

    override fun getItemCount(): Int {
        return dataList.size
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FreaturedViewHolder) {
            Log.d("OnBind", "FreaturedList")
            val layoutParams = binding.imgLogo.layoutParams
            // val itemViewHeight = (itemHeight * 0.62f)
            //layoutParams.height = itemViewHeight.roundToInt()
            val data = dataList[position]
            binding.tvArtistName.text = data.RecordArtist

            when (data.RecordTypeId) {
                1 -> {
                    binding.clAlbumInfo.visibility=View.VISIBLE
                }
                else->
                { binding.clAlbumInfo.visibility=View.GONE

                }
            }

            Glide.with(context).load(data.RecordImage).diskCacheStrategy(DiskCacheStrategy.ALL)
                .transition(GenericTransitionOptions.with(animationObject))
                .priority(Priority.IMMEDIATE).into(binding.imgLogo)


            if (dataList.get(position).RecordStatusTypeId.equals("6")) {
                binding.tvMinutes.text = "00"
                binding.tvDays.text = "00"
                binding.tvHour.text = "00"
            }


            /* CommonUtils.loadImageNoThumbnail(
                context,
                data.RecordImage ?: "",
                holder.itemView.img_logo,
                R.drawable.bg_spot
            )*/

            /*   if (data.RecordStatus.equals("Launched") && (data.TimeLeftToRelease!!.toInt() == 0)) {
                   holder.itemView.tvAvailabe.visibility = View.VISIBLE
               } else {
                   holder.itemView.tvAvailabe.visibility = View.GONE
               }*/
/*
            holder.itemView.setOnClickListener {
                if (holder.adapterPosition in 0..dataList.size) {
                    val bundle = Bundle().apply {
                        putString(
                            AppConstant.RECORD_IMAGE_URL,
                            dataList[holder.adapterPosition].RecordImage
                        )

                        putString(
                            AppConstant.ID,
                            dataList[holder.adapterPosition].Id
                        )
                    }
                    fg.navigate.goToNewOffering(bundle)
                }
            }
*/

            holder.itemView.setOnClickListener {

                /*    var account = (context as DashboardActivity).account
                    var selectedSessionTopic = (context as DashboardActivity).selectedSessionTopic
                    var walletUri = (context as DashboardActivity).deeplinkPairingUri*/

                if (holder.adapterPosition in 0..dataList.size) {
                    val bundle = bundleOf(
                        AppConstant.RECORD_IMAGE_URL to dataList[holder.adapterPosition].RecordImage,
                        AppConstant.ID to dataList[holder.adapterPosition].Id,
                        AppConstant.CONNECTED_WALLET_ACCOUNT to "",
                        AppConstant.SELECTED_SESSION_TOPIC to "",
                        AppConstant.WALLET_URI to ""
                    )
                    fg.navigate.goToNewOffering(bundle)
                }
            }
        } else {
            val layoutParams = blankBinding.view.layoutParams
            val itemViewHeight = (itemHeight * 0.62f)
            layoutParams.height = itemViewHeight.roundToInt()
        }
    }

    inner class FreaturedViewHolder(itemView: View) : LifecycleViewHolder(itemView) {
        var tvDay: AppCompatTextView? = itemView.findViewById(R.id.tvDays)
        var tvhour: AppCompatTextView? = itemView.findViewById(R.id.tvHour)
        var tvMinutes: AppCompatTextView? = itemView.findViewById(R.id.tvMinutes)

        fun registerTimerLiveData() {
            Log.d(
                "AdapterPosition",
                dataList.get(adapterPosition).tltr.toString() + adapterPosition.toString()
            )

            registerTimerLiveDataForFreaturedItem(
                context,
                fg.baseViewModel.getTimeLv(),
                thisMillisStart,
                tvDay,
                tvhour,
                tvMinutes,
                if (adapterPosition > -1 && adapterPosition < dataList.size) {
                    dataList[adapterPosition]
                } else {
                    null
                },
                this@FreaturedArtistAdapter
            )
        }
    }

    override fun onTimerFinish(recordId: String, position: Int) {

    }

    private fun setRemainingTime(dataList: MutableList<FreaturedArtistResponse>) {
        for (obj in dataList) {
            obj.remainingMillis = obj.TimeLeftToRelease ?: 0
            obj.tltr = obj.TimeLeftToRelease ?: 0
            obj.rId = obj.Id ?: ""
        }
    }

    fun setBlankData() {
        if (::dataList.isInitialized.not()) {
            dataList = ArrayList()
        }
        val temp = FreaturedArtistResponse()
        temp.viewType = 3
        dataList.add(temp)
        for (i in 0 until 7) {
            val temp = FreaturedArtistResponse()
            temp.viewType = 3
            dataList.add(temp)
        }
        notifyDataSetChanged()
    }

    var animationObject = ViewPropertyTransition.Animator { view ->
        view.alpha = 0f
        val fadeAnim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f)
        fadeAnim.duration = 1000
        fadeAnim.start()
    }
}