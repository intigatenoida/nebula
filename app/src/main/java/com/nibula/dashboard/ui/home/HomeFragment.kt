package com.nibula.dashboard.ui.home

import android.content.ActivityNotFoundException
import android.content.ComponentCallbacks2
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.*
import com.bumptech.glide.Glide
import com.nibula.R
import com.nibula.base.BaseApplication
import com.nibula.base.BaseFragment
import com.nibula.base.Event
import com.nibula.base.ForceUpdateDialog
import com.nibula.dashboard.DashboardActivity
import com.nibula.BuildConfig
import com.nibula.databinding.FragmentHomeBinding

import com.nibula.request_to_buy.navigate
import com.nibula.response.home.recentuploaded.HomeScreenCommonModel
import com.nibula.response.topoffertopartist.TopOfferTopArtistReponse
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import io.reactivex.Observable

import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator

class HomeFragment : BaseFragment() {
    companion object {
        fun newInstance() = HomeFragment()
    }

    lateinit var artistAdapter: ArtistAdapter
    lateinit var freaturedArtistAdapter: FreaturedArtistAdapter
    lateinit var trendingOfferAdapter: TrendingOfferAdapter
    lateinit var adapter: RecentlyUpdatedAdapter
    lateinit var rootView: View
    var isLoading = true
    var isDataAvailable = true
    var topArtistObservable: Observable<TopOfferTopArtistReponse>? = null
    var homeDataObservable: Observable<HomeScreenCommonModel>? = null
    lateinit var helper: HomeFragmentHelper
    lateinit var binding: FragmentHomeBinding
    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigate = context as navigate
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_home, container, false)
            binding = FragmentHomeBinding.inflate(inflater, container, false)
            rootView=binding.root
            initUI()

            triggerHomeScreenViewEvent()
        }
        return rootView
    }

    private fun initUI() {
        helper = HomeFragmentHelper(this)
        binding.header.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.header.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.header.ivUpload.setOnClickListener {
            loginUploadCheck()
        }

        binding.header.imgLogo.setOnClickListener {

            /* val intent = Intent(requireActivity(), UploadWebActivity::class.java)
             startActivity(intent)*/
        }
        binding.swpMain.setOnRefreshListener {
            //rootView.swp_main.isRefreshing = false
            helper.recentUploadCurrentPageCount = 1
            initialHit()
        }
        setFreaturedArtist()
        setTrendingOffers()
        setTopArtist()
        setRecentUploadedAdapter()
        initialHit()
    }

    private fun initialHit() {
        //goneAllViews()
        //helper.requestFreaturedArtistList(true)
        //helper.requestTrendsOfferingTopArtists(true)
        //helper.trendingOfferingList(true)0.
        //helper.topArtistsList(true)
        //helper.getTopArtists(true)
        helper.getHomeScreenData(true)
        helper.apiCallRecentUploaded()
        helper.getNotificationCount()
    }

    override fun onResume() {
        super.onResume()
        // Banner
        if (binding.header.imgNotificationCount.visibility != View.VISIBLE) {
            CommonUtils.setNotificationDot(requireContext(), binding.header.imgNotificationCount)
        }
        checkVersion()
        if (helper.disposables.size() == 0) {
            // helper.recentUploadCurrentPageCount = 1
            initialHit()
        }
        //check if user connected to any wallet address
        /*val walletAddress =
            PrefUtils.getValueFromPreference(requireContext(), PrefUtils.CONNECTED_WALLET_ADDRESS)
        if (walletAddress.isEmpty()) {
            navigate.uploadRecord()
        }
*/
    }

    override fun refreshHomeScreen() {
        initialHit()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /*      Because of timer Updating */
        /*      rootView.rvTrendinOffers.adapter = trendingOfferAdapter
                rootView.rvRecently.adapter = adapter*/

        binding.rvFreaturedlist.adapter = freaturedArtistAdapter
        baseViewModel.refreshAfterLogin.observe(viewLifecycleOwner,
            Observer<Event<Boolean?>> {
                if (it != null &&
                    it.getContentIfNotHandled() == true
                ) {
                    helper.recentUploadCurrentPageCount = 1
                    initialHit()
                }
            })
    }

/*
    private fun goneAllViews() {
        rootView.gp_trending.visibility = GONE
        rootView.gp_top_artist.visibility = GONE
        rootView.inc_no_data.visibility = GONE
    }
*/

    private fun setTrendingOffers() {

        binding.rvTrendinOffers.visibility = View.VISIBLE
        binding.tvTrendingOffers.visibility = View.VISIBLE
        val layoutManager =
            LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
        binding.rvTrendinOffers.layoutManager = layoutManager
        binding.rvTrendinOffers.isNestedScrollingEnabled = false
        trendingOfferAdapter =
            TrendingOfferAdapter(
                this,
                getTrendingItemWidth()
            )
        val decoration = DividerItemDecoration(
            activity,
            DividerItemDecoration.HORIZONTAL
        )
        val divider =
            ContextCompat.getDrawable(requireActivity(), R.drawable.divider_20)
        decoration.setDrawable(divider!!)
        binding.rvTrendinOffers.addItemDecoration(decoration)
        trendingOfferAdapter.setDummyData()
        binding.rvTrendinOffers.adapter = trendingOfferAdapter
    }

    private fun getTrendingItemWidth(): Int {
        val temp = ((getScreenWidth() - CommonUtils.dpToPx(requireContext(), 10)) * 45.86) / 100
        println("getTrendingItemWidth: $temp")
        return temp.toInt()
    }

    private fun setTopArtist() {
        binding.rvArtist.visibility = View.VISIBLE
        binding.tvartist.visibility = View.VISIBLE
        val layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
        binding.rvArtist.layoutManager = layoutManager
        binding.rvArtist.isNestedScrollingEnabled = false
        artistAdapter = ArtistAdapter(requireActivity(), navigate, getArtistItemWidth())
        artistAdapter.setBlankData()
        binding.rvArtist.adapter = artistAdapter
        val decoration = DividerItemDecoration(
            activity,
            DividerItemDecoration.HORIZONTAL
        )
        val divider = ContextCompat.getDrawable(requireActivity(), R.drawable.divider_20)
        decoration.setDrawable(divider!!)
        binding.rvArtist.addItemDecoration(decoration)
    }

    private fun setFreaturedArtist() {
        binding.rvFreaturedlist.visibility = View.VISIBLE
        val layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
        binding.rvFreaturedlist.layoutManager = layoutManager
        freaturedArtistAdapter = FreaturedArtistAdapter(this, navigate, getWindowHeight())
/*
        rootView.rvFreaturedlist.addItemDecoration(CirclePagerIndicatorDecoration())
*/
        freaturedArtistAdapter.setBlankData()
        binding.rvFreaturedlist.isNestedScrollingEnabled = false
        binding.rvFreaturedlist.adapter = freaturedArtistAdapter
        binding.rvFreaturedlist.setHasFixedSize(false);

        val pagerSnapHelper = PagerSnapHelper()
        pagerSnapHelper.attachToRecyclerView(binding.rvFreaturedlist)
        val recyclerIndicator: ScrollingPagerIndicator = rootView.findViewById(R.id.indicator)
        recyclerIndicator.attachToRecyclerView(binding.rvFreaturedlist)
        /*val indicator: CircleIndicator2 = rootView.findViewById(com.nibula.R.id.indicator)
            indicator.attachToRecyclerView(rootView.rvFreaturedlist, pagerSnapHelper)*/
        /*val snapHelper: PagerSnapHelper = PagerSnapHelper()
             val recyclerIndicator: ScrollingPagerIndicator = rootView.findViewById(R.id.indicator)
             recyclerIndicator.attachToRecyclerView(rootView.rvFreaturedlist)
             snapHelper.attachToRecyclerView(rootView.rvFreaturedlist)*/
    }

    private fun getArtistItemWidth(): Int {
        val temp = ((getScreenWidth() - CommonUtils.dpToPx(requireContext(), 30))) / 4.3
        println("getArtistItemWidth: $temp")
        return temp.toInt()
    }

    fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    fun getScreenWidth(): Int {
        val wm = requireContext().getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width: Int = size.x
        val height: Int = size.y
        return width
    }

    private fun setRecentUploadedAdapter() {
        binding.tvrecently.visibility = View.VISIBLE
        adapter = RecentlyUpdatedAdapter(this, requireActivity(), navigate, getTrendingItemWidth())
        val layoutManager = GridLayoutManager(requireActivity(), 2)
        binding.rvRecently.layoutManager = layoutManager
        binding.rvRecently.isNestedScrollingEnabled = false
        binding.rvRecently.setHasFixedSize(false)
        binding.rvRecently.adapter = adapter
        binding.nsMain.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (v.getChildAt(v.childCount - 1) != null) {
                if (scrollY >= v.getChildAt(v.childCount - 1)
                        .measuredHeight - (v.measuredHeight) &&
                    scrollY > oldScrollY && !isLoading && isDataAvailable
                ) {
                    binding.progressBar.visibility = View.VISIBLE
                    helper.apiCallRecentUploaded()
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()

        binding.rvFreaturedlist.adapter = null
        // Now we have removed the timer in the latest design
/*      rootView.rvTrendinOffers.adapter = null
        rootView.rvRecently.adapter = null*/
/*
        rootView.rvFreaturedlist.adapter=null
*/
    }

    override fun onDestroy() {
        super.onDestroy()
        helper.onDestroy()
    }

    fun updateNotificationIcon() {
        CommonUtils.setNotificationDot(requireContext(), binding.header.imgNotificationCount)
    }

    private fun checkVersion() {
        var versionServerStr =
            PrefUtils.getValueFromPreference(requireContext(), PrefUtils.APP_VERSION)
        val versionServer = (if (versionServerStr.isEmpty()) {
            "0.0"
        } else {
            versionServerStr
        }).toDouble()
        val appVerion = BuildConfig.VERSION_NAME.toDouble()
        if (versionServer > appVerion) {
            val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            val prev = requireActivity().supportFragmentManager.findFragmentByTag("UpdateDialog")
            if (prev != null) {
                val df = prev as ForceUpdateDialog
                df.dismissAllowingStateLoss()
            }
            fragmentTransaction.addToBackStack(null)
            val dialogFragment =
                ForceUpdateDialog.getInstance(object : ForceUpdateDialog.ForceUpdateListener {
                    override fun onUpdateClick() {
                        val appPackageName = "ch.nebula"
                        try {
                            startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                                )
                            )
                        } catch (anfe: ActivityNotFoundException) {
                            startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                                )
                            )
                        }
                    }

                    override fun closeApp() {
                        (requireActivity() as DashboardActivity).finish()
                    }
                })
            fragmentTransaction.add(dialogFragment, "UpdateDialog").commitAllowingStateLoss()
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        Glide.with(this).onLowMemory()
        Glide.with(this).onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL)
    }

    fun triggerHomeScreenViewEvent() {
        val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
        mFirebaseAnalytics.logEvent("home_screen_view", null)
    }

    override fun onDetach() {
        super.onDetach()
        Log.d("Fragment Detach", "HomeFragment")
    }


/*
    fun openRecordDetail(bundle:Bundle)
    {
        findNavController().navigate(R.id.action_navigation_home_to_offeringFragment,bundle)
    }
*/

}

