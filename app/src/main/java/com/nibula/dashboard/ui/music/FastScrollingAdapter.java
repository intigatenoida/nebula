package com.nibula.dashboard.ui.music;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nibula.R;
import com.nibula.request_to_buy.navigate;
import com.nibula.response.artists.ArtistData;
import com.nibula.response.artists.PlayMusic;
import com.nibula.response.details.File;
import com.nibula.utils.AppConstant;
import com.nibula.utils.FastScrollRecyclerViewInterface;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

public class FastScrollingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FastScrollRecyclerViewInterface {
    public ArrayList<ArtistData> mDataset;
    public ArrayList<PlayMusic> playingMusicList = new ArrayList<PlayMusic>();
    private HashMap<String, Integer> mMapIndex;
    private Context context;
    private navigate mNaviagte;
    private int previousPosition = -1;

    static class RecordHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView tvSongName;
        public AppCompatTextView tvTitle;
        public RoundedImageView imgRecord;
        public ConstraintLayout playListContainer;
        public ImageView playIcon;
        public ImageView pauseIcon;
        public ImageView favIcon;


        RecordHolder(View v) {
            super(v);
            tvSongName = (AppCompatTextView) v.findViewById(R.id.tvsongName);
            imgRecord = (RoundedImageView) v.findViewById(R.id.clImage);
            tvTitle = (AppCompatTextView) v.findViewById(R.id.tvTitle);
            playListContainer = (ConstraintLayout) v.findViewById(R.id.playListContainer);

            favIcon = v.findViewById(R.id.ivFav);
            playIcon = v.findViewById(R.id.ivPlay);
            pauseIcon = v.findViewById(R.id.ivPause);
        }
    }

    static class Header extends RecyclerView.ViewHolder {
        public TextView mTextView;

        Header(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.tvHeader);
        }
    }

    public FastScrollingAdapter(Context context, navigate mNaviagte) {
        this.mNaviagte = mNaviagte;
        this.context = context;
    }

    public void setData(ArrayList<ArtistData> mDataset, HashMap<String, Integer> mMapIndex) {
        this.mDataset = mDataset;
        this.mMapIndex = mMapIndex;
        for (int i = 0; i < mDataset.size(); i++) {
            PlayMusic playMusic = new PlayMusic();
            playMusic.isPlaying = false;
            playingMusicList.add(playMusic);

        }
        notifyDataSetChanged();
    }

    public void addData(ArrayList<ArtistData> data, HashMap<String, Integer> mMapIndex) {
        mDataset.addAll(data);

        for (String key : mMapIndex.keySet()) {
            this.mMapIndex.put(key, mMapIndex.get(key));
        }
        notifyDataSetChanged();
    }

    public void clearData() {
        mDataset.clear();
        playingMusicList.clear();
        mMapIndex.clear();
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_my_playlist, parent, false);
            return new RecordHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_playlist_header, parent, false);
            return new Header(v);
        }
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof RecordHolder) {
            clearGlide((RecordHolder) holder);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (holder instanceof Header) {
            // ((Header) holder).mTextView.setText(mDataset.get(position).getKey().toUpperCase());
        } else if (holder instanceof RecordHolder) {
            ((RecordHolder) holder).tvSongName.setText(mDataset.get(position).getRecordTitle());
            ((RecordHolder) holder).tvTitle.setText(mDataset.get(position).getArtistName());

            ((RecordHolder) holder).imgRecord.setImageResource(R.drawable.nebula_placeholder);

            if (mDataset.get(position).getRecordTypeId() == 1) {
                ((RecordHolder) holder).imgRecord.setBorderWidth(1f);
            } else {
                ((RecordHolder) holder).imgRecord.setBorderWidth(0f);
            }

            if (mDataset.get(position).getIsFavourite()) {

                ((RecordHolder) holder).favIcon.setBackgroundResource(R.drawable.ic_baseline_favorite_24);

            } else {
                ((RecordHolder) holder).favIcon.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24);

            }

        /*    mDataset.get(position).getIsFavourite().let {
                when (it) {
                    true -> {
                        holder.ivLike.setBackgroundResource(R.drawable.ic_baseline_favorite_24)
                    }
                else -> {
                        holder.ivLike.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24)
                    }
                }
            }*/

        /*    if (playingMusicList.get(position).getPlaying()) {


                ((RecordHolder) holder).playIcon.setVisibility(View.GONE);
                ((RecordHolder) holder).pauseIcon.setVisibility(View.VISIBLE);
            } else {

                ((RecordHolder) holder).playIcon.setVisibility(View.VISIBLE);
                ((RecordHolder) holder).pauseIcon.setVisibility(View.GONE);

            }*/

       /*     CommonUtils.loadAsBitmap(
                    context,
                    ((RecordHolder) holder).imgRecord,
                    null,
                    null,
                    mDataset.get(position).getRecordImage(),
                    R.drawable.nebula_placeholder
            );*/

            Glide
                    .with(context)
                    .load(mDataset.get(position).getRecordImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.IMMEDIATE)
                    .into(((RecordHolder) holder).imgRecord);
            ((RecordHolder) holder).imgRecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mNaviagte != null) {
                    /*    if (previousPosition == position) {
                            previousPosition = -1;
                        }
                        if (previousPosition == -1) {
                            previousPosition = position;
                            playingMusicList.get(position).isPlaying = true;

                        } else {
                            playingMusicList.get(position).isPlaying = true;
                            playingMusicList.get(previousPosition).isPlaying = false;
                            previousPosition = position;
                        }*/

                        Log.d("Data from data base", mDataset.toString());
                        ArrayList<File> fileArrayList = new ArrayList<>();

                  /*      @SerializedName("Id") var id: Int? = null,
                                @SerializedName("MusicDurationInSeconds") var musicDurationInSeconds: Int? = null,
                                @SerializedName("SongTitle") var songTitle: String? = null,
                                @SerializedName("isPlaying") var isPlaying: Boolean = false,

                                @SerializedName("IsFavourite") var IsFavourite: Boolean = false,
                                @SerializedName("artistName") var artistName: String = "",

                                @SerializedName("Artists") var artistsList: MutableList<AlbumData>? = null,*/
                        fileArrayList.add(new File(Integer.parseInt(mDataset.get(position).getFileId()), 120, "", false, false, "", null));
                        mNaviagte.playSong(fileArrayList,
                                0,
                                false,
                                mDataset.get(position).getRecordTitle(),
                                mDataset.get(position).getRecordId(),
                                1,
                                "",
                                false);

                        notifyDataSetChanged();
                    }
                }
            });


            ((RecordHolder) holder).pauseIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (previousPosition == -1) return;
                    playingMusicList.get(previousPosition).isPlaying = false;
                    mNaviagte.pauseMusic();
                    notifyDataSetChanged();
                }
            });


            ((RecordHolder) holder).playListContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mNaviagte != null) {


                        Bundle bundle = new Bundle();
                        bundle.putString(
                                AppConstant.RECORD_IMAGE_URL,
                                mDataset.get(position).getRecordImage()
                        );
                        bundle.putString(
                                AppConstant.ID,
                                mDataset.get(position).getRecordId()
                        );
                        mNaviagte.goToNewOffering(bundle);
                        // mNaviagte.gotorequestinvest(mDataset.get(position).getRecordId());
                    }
                }
            });


        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public HashMap<String, Integer> getMapIndex() {
        return this.mMapIndex;
    }

    private void clearGlide(RecordHolder holder) {
        Glide.with(context).clear(holder.imgRecord);
        holder.imgRecord.setImageDrawable(null);
    }

    public void refreshAdapter() {
        if (previousPosition == -1) return;
        playingMusicList.get(previousPosition).isPlaying = false;
        notifyDataSetChanged();
    }

    public void playPause(Boolean isPlaying) {
        if (previousPosition == -1) return;
        if (isPlaying) {
            playingMusicList.get(previousPosition).isPlaying = true;

        } else {
            playingMusicList.get(previousPosition).isPlaying = false;

        }
        notifyDataSetChanged();
    }
}
