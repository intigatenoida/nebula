package com.nibula.dashboard.ui.music.helper

import android.view.View
import com.nibula.DBHandler.NebulaDBHelper
import com.nibula.DBHandler.PlayListData
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ui.music.vfragments.MyPlayListFragment
import com.nibula.request.myplaylist.MyPlayListRequest
import com.nibula.response.artists.ArtistData
import com.nibula.response.myplaysit.MyPlaylistResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.CommonUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.util.*
import kotlin.collections.ArrayList

class MyPlayListFragmentHelper(val fg: MyPlayListFragment) : BaseHelperFragment() {

    var playListNextPage: Int = 1
    private val activityScope = CoroutineScope(Dispatchers.Main)

    fun requestGetMyPlaylist(search: String, isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.swpMain.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        if (fg.call != null) {
            fg.call!!.cancel()
        }

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.call = helper.requestGetMyPlaylist(MyPlayListRequest(playListNextPage, search))
        fg.call!!.enqueue(object : CallBackManager<MyPlaylistResponse>() {

            override fun onSuccess(any: MyPlaylistResponse?, message: String) {
                if (any?.responseStatus != null && any.responseStatus == 1) {
                    if (!any.playListData.isNullOrEmpty()) {
                        fg.binding.incNoData.noData.visibility=View.GONE
                        fg.binding.incNoData.tvMessage.visibility=View.GONE
                        fg.binding.rvFast.visibility=View.VISIBLE
                        for (i in 0 until any.playListData!!.size) {
                            if (!any.playListData!![i]?.startWith.isNullOrEmpty()) {
                                val artistList = any.playListData!![i]?.artistData
                                if (!artistList.isNullOrEmpty()) {
                                    addDataToDatabase(
                                        any.playListData!![i]!!.startWith!!,
                                        artistList
                                    )


                                }
                                CommonUtils.triggerMusicFragment(any.playListData?.size.toString())
                            }
                        }
                        if (any.playListData!!.size % 20 == 0) {
                            playListNextPage++
                            requestGetMyPlaylist(search, true)
                        } else {
                            dismiss(isProgressDialog)
                            updateAdapterFromDb(search)
                        }

                    } else {

                        fg.binding.incNoData.noData.visibility=View.VISIBLE
                        fg.binding.incNoData.tvMessage.visibility=View.VISIBLE
                        fg.binding.incNoData.tvMessage.text=fg.requireActivity().getString(R.string.no_song_record)
                        fg.binding.rvFast.visibility=View.GONE

                        dismiss(isProgressDialog)
                        updateAdapterFromDb(search)
                    }
                } else {
                    dismiss(isProgressDialog)
                    updateAdapterFromDb(search)
                }
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
                updateAdapterFromDb(search)
            }

            override fun onError(error: RetroError) {

                if (error.errorMessage.equals("HTTP 401 Unauthorized")) {
                    CommonUtils.logOut(fg.requireActivity())
                    dismiss(isProgressDialog)
                } else {
                    updateAdapterFromDb(search)
                    dismiss(isProgressDialog)
                }
            }

        })
    }

    private fun addDataToDatabase(startWith: String, artistList: MutableList<ArtistData>) {
        for (artist in artistList) {
            if (!artist.recordId.isNullOrEmpty()
                && !artist.artistName.isNullOrEmpty()
                && !artist.artistId.isNullOrEmpty() &&
                !artist.recordTitle.isNullOrEmpty() &&
                !artist.recordImage.isNullOrEmpty()
            ) {
                val playListData =
                    PlayListData(
                    )
                playListData.artistId = artist.artistId
                playListData.recordID = artist.recordId
                playListData.artistName = artist.artistName
                playListData.recordTitle = artist.recordTitle
                playListData.recordImage = artist.recordImage
                playListData.startWith = startWith
                playListData.recordType = artist.RecordType
                playListData.recordTypeId = artist.RecordTypeId
                playListData.fileId = artist.files?.get(0)?.id.toString()
                NebulaDBHelper(fg.requireContext()).InsertPlayListData(playListData)
            }

        }
    }

    private fun dismiss(isProgressDialog: Boolean) {
        fg.isSearchProgress = false
        if (fg.binding.swpMain.isRefreshing) {
            fg.binding.swpMain.isRefreshing = false
        } else if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        noData()
    }

    private fun noData() {
        if (fg.adapter.mDataset.isEmpty()) {
            fg.binding.incNoData.root.visibility = View.VISIBLE
            fg.binding.incNoData.tvMessage.text = fg.getString(R.string.no_records_available)
            fg.binding.rvFast.visibility = View.GONE
            fg.binding.tvHeader.visibility = View.GONE
        } else {
            fg.binding.incNoData.root.visibility = View.GONE
            fg.binding.rvFast.visibility = View.VISIBLE
            fg.binding.tvHeader.visibility = View.GONE
        }
    }

    private fun updateAdapterFromDb(search: String) {
        val map = HashMap<String, Int>()
        val data = ArrayList<ArtistData>()
        var allData = ArrayList<PlayListData>()
        allData = NebulaDBHelper(fg.requireContext()).playListAllData
        fg.databaseList = allData
        for (i in 0 until allData.size) {
            val playListData = allData[i]

            if (search.isNotEmpty()) {
                if (playListData.artistName.toLowerCase()
                        .contains(search) || playListData.recordTitle.toLowerCase()
                        .contains(search)
                ) {
                    val temp = playListData.startWith
                    if (!map.containsKey(temp)) {
                        if (map.isEmpty()) {
                            map[temp] = 0
                        } else {
                            map[temp] = data.size
                        }
                        data.add(
                            ArtistData(
                                "", "", "", false, "",
                                "", "", false, temp, 2,
                                "",
                                0
                            )
                        )
                    }
                    data.add(
                        ArtistData(
                            playListData.artistId,
                            playListData.recordImage,
                            playListData.artistName,
                            false,
                            playListData.recordID,
                            playListData.recordImage,
                            playListData.recordTitle,
                            false,
                            temp,
                            1,
                            playListData.recordType,
                            playListData.recordTypeId, playListData.fileId
                        )
                    )
                }
            } else {
                val temp = playListData.startWith
                if (!map.containsKey(temp)) {
                    if (map.isEmpty()) {
                        map[temp] = 0
                    } else {
                        map[temp] = data.size
                    }
                    data.add(
                        ArtistData(
                            "", "", "", false, "",
                            "", "", false, temp, 2, "",
                            0, ""
                        )
                    )
                }
                data.add(
                    ArtistData(
                        playListData.artistId,
                        playListData.recordImage,
                        playListData.artistName,
                        false,
                        playListData.recordID,
                        playListData.recordImage,
                        playListData.recordTitle,
                        false,
                        temp,
                        1,
                        playListData.recordType,
                        playListData.recordTypeId, playListData.fileId
                    )
                )

            }

        }
        if (search.isNotEmpty()) {
            fg.adapter.mDataset.clear()
        }
        if (data.isNotEmpty() && map.isNotEmpty()) {
            fg.binding.rvFast.visibility = View.VISIBLE
            fg.adapter.setData(data, map)
        }
        noData()
    }

    fun updateAdapterFromDb(allData: ArrayList<PlayListData>) {
        val map = HashMap<String, Int>()
        val data = ArrayList<ArtistData>()

        for (i in 0 until allData.size) {
            val playListData = allData[i]
            val temp = playListData.startWith
            if (!map.containsKey(temp)) {
                if (map.isEmpty()) {
                    map[temp] = 0
                } else {
                    map[temp] = data.size
                }
                data.add(
                    ArtistData(
                        "", "", "", false, "",
                        "", "", false, temp, 2, "",
                        0, ""
                    )
                )
            }
            data.add(
                ArtistData(
                    playListData.artistId,
                    playListData.recordImage,
                    playListData.artistName,
                    false,
                    playListData.recordID,
                    playListData.recordImage,
                    playListData.recordTitle,
                    false,
                    temp,
                    1,
                    playListData.recordType,
                    playListData.recordTypeId, playListData.fileId
                )
            )
        }

        if (data.isNotEmpty() && map.isNotEmpty()) {
            fg.binding.rvFast.visibility = View.VISIBLE
            fg.adapter.setData(data, map)
        }
        noData()
    }

}