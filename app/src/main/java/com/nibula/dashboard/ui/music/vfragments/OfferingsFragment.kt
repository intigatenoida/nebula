package com.nibula.dashboard.ui.music.vfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.nibula.base.BaseFragment
import com.nibula.dashboard.ui.music.adapters.AlbumsAdapter
import com.nibula.dashboard.ui.music.adapters.OfferingsAdapter
import com.nibula.dashboard.ui.music.helper.OfferingFragmentHelper
import com.nibula.dashboard.ui.music.vmodels.OfferingViewModel
import com.nibula.databinding.ViewOfferListBinding
import com.nibula.response.music.offerings.OfferingResponses
import retrofit2.Call

class OfferingsFragment : BaseFragment() {

    lateinit var rootView: View

    var call: Call<OfferingResponses>? = null
    var call1: Call<OfferingResponses>? = null
    var call2: Call<OfferingResponses>? = null

    lateinit var offeringAdapter: OfferingsAdapter
    lateinit var thisMonthAdapter: AlbumsAdapter
    lateinit var nextMonthAdapter: AlbumsAdapter

    private lateinit var helper: OfferingFragmentHelper

    var isSearchProgress: Boolean = false
    var searchKey: String? = ""

    lateinit var model: OfferingViewModel
    lateinit var binding:ViewOfferListBinding

    companion object {
        fun newInstance() = OfferingsFragment()

        const val OFFERINGS = "current-offerings"
        const val THIS_MONTH_OFFERINGS = "this_month"
        const val NEXT_MONTH_OFFERINGS = "next_month"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.view_offer_list, container, false)
            binding=ViewOfferListBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
        }
        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /*Because of timer Updating*/
        binding.rvOffering.adapter = offeringAdapter
    }

    private fun initUI() {
        model = ViewModelProvider(this).get(OfferingViewModel::class.java)
        helper = OfferingFragmentHelper(this)
        goneAllViews()
        setOfferingsAdapter()
        setThisMonthAdapter()
        setNextMonthAdapter()

        binding.swpMain.setOnRefreshListener {
            if (searchKey.isNullOrEmpty()) {
                helper.nextMonthNextPage = 1
                helper.thisMonthNextPage = 1
                helper.nextMonthMaxPage = 0
                helper.thisMonthMaxPage = 0
            }

            helper.offeringNextPage = 1
            helper.offeringMaxPage = 0

            helper.requestOfferings(searchKey ?: "", false)
            helper.requestThisMonth("", false)
            helper.requestNextMonth("", false)
        }

        binding.tvMoreOfferings.setOnClickListener {
            helper.requestOfferings(searchKey ?: "", true)
        }

        binding.tvMoreThisMonth.setOnClickListener {
            helper.requestThisMonth("", true)
        }

        binding.tvMoreNextMonth.setOnClickListener {
            helper.requestNextMonth("", true)
        }
    }

    private fun goneAllViews() {
        binding.tvHeader.visibility = GONE
        binding.rvOffering.visibility = GONE
        binding.tvupcoming.visibility = GONE
        binding.gdThisMonth.visibility = GONE
        binding.gdNextMonth.visibility = GONE
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            clearAllPages()
            helper.requestOfferings(searchKey ?: "", true)
            helper.requestThisMonth("", false)
            helper.requestNextMonth("", false)
        } else {
            if (call1 == null ||
                (call1!!.isCanceled && !call1!!.isExecuted)
            ) {
                helper.requestThisMonth("", true)
                helper.requestNextMonth("", false)
            } else if (call2 == null ||
                (call2!!.isCanceled && !call2!!.isExecuted)
            ) {
                helper.requestThisMonth("", false)
                helper.requestNextMonth("", true)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (call != null &&
            !call!!.isCanceled
        ) {
            call!!.cancel()
        }

        if (call1 != null &&
            !call1!!.isCanceled
        ) {
            call1!!.cancel()
        }

        if (call2 != null &&
            !call2!!.isCanceled
        ) {
            call2!!.cancel()
        }
    }

    private fun setOfferingsAdapter() {
        offeringAdapter =
            OfferingsAdapter(this)
        val layoutManager = GridLayoutManager(requireActivity(), 2)
        binding.rvOffering.layoutManager = layoutManager
    }

    private fun setThisMonthAdapter() {
        thisMonthAdapter =
            AlbumsAdapter(
                requireActivity(),
                navigate
            )
        thisMonthAdapter.setData(mutableListOf())
        val layoutManager = GridLayoutManager(requireActivity(), 2)
        binding.rvThisMonth.layoutManager = layoutManager
        binding.rvThisMonth.adapter = thisMonthAdapter
    }

    private fun setNextMonthAdapter() {
        nextMonthAdapter =
            AlbumsAdapter(
                requireActivity(),
                navigate
            )
        nextMonthAdapter.setData(mutableListOf())
        val layoutManager = GridLayoutManager(requireActivity(), 2)
        binding.rvNextMonth.layoutManager = layoutManager
        binding.rvNextMonth.adapter = nextMonthAdapter
    }

    fun requestSearch() {
        helper.requestOfferings(
            searchKey ?: "", searchKey.isNullOrEmpty()
        )
    }

    private fun clearAllPages() {
        helper.offeringNextPage = 1
        helper.offeringMaxPage = 0

        helper.nextMonthNextPage = 1
        helper.nextMonthMaxPage = 0

        helper.thisMonthNextPage = 1
        helper.thisMonthMaxPage = 0
    }

    fun clearOfferingPageCount() {
        helper.offeringNextPage = 1
        helper.offeringMaxPage = 0
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.rvOffering.adapter = offeringAdapter
    }
}