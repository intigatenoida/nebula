package com.nibula.dashboard.ui.assets.helper

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ui.assets.AssesInvestmentFragment
import com.nibula.request.MyTokenListRequest
import com.nibula.response.bankDetailListResponse.BankDetailListResponse
import com.nibula.response.bankDetailListResponse.ResponseCollection
import com.nibula.response.myinvestmentresponse.MyInvestmentResponse
import com.nibula.response.myinvestmentresponse.Myinvestments
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant

class AssetsInvestmentHelper(val fg: AssesInvestmentFragment) : BaseHelperFragment() {

    var investmentNextPage: Int = 1
    var investmentMaxPage: Int = 0

    fun getInvestmentListing(artistId: String, isProgressDialog: Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        fg.isLoading = true

        val helper =
            ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.call = helper.getInvestmentListing(artistId, investmentNextPage)
        fg.call?.enqueue(object : CallBackManager<MyInvestmentResponse>() {

            override fun onSuccess(any: MyInvestmentResponse?, message: String) {
                /*Load More*/
                investmentMaxPage = ((any?.myinvestments?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = investmentNextPage < investmentMaxPage ||
                        (any?.myinvestments?.totalRecords ?: 0) > (investmentNextPage * 10)

                /*Data*/
                if (any?.myinvestments != null &&
                    any.myinvestments?.responseStatus == 1
                ) {
                    if (!any.myinvestments?.responseCollection.isNullOrEmpty()) {
                        fg.binding.rvInvestors.visibility = View.VISIBLE
                        if (investmentNextPage == 1) {
                            fg.adapter.setData(any.myinvestments?.responseCollection!!)
                        } else if (investmentNextPage > 1) {
                            fg.adapter.addData(any.myinvestments?.responseCollection!!)
                        }
                        investmentNextPage++

                    } else {
                        if (investmentNextPage == 1) {
                            fg.adapter.clearData()
                        }
                    }
                } else {
                    if (investmentNextPage == 1) {
                        fg.adapter.clearData()
                    }
                }

                any?.myInvestmentsTotals?.let {
                    val totalAmt = "Total: ${any?.myInvestmentsTotals ?: "0"}"
                    fg.updateTotalAmount(totalAmt)
                }
                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {
                dismiss(isProgressDialog)
            }

        })
    }

    fun myInvestmentTokenList(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        fg.isLoading = true

        val helper =
            ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var trendingRequest = MyTokenListRequest()
        trendingRequest.currentPage = investmentNextPage
        trendingRequest.recordsPerPage = 10

        val call = helper.getMyTokenlist(trendingRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<Myinvestments>() {
            override fun onSuccess(any: Myinvestments?, message: String) {
                investmentMaxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = investmentNextPage < investmentMaxPage ||
                        (any?.totalRecords ?: 0) > (investmentNextPage * 10)
                any?.responseCollection?.let {
                    if (investmentNextPage == 1) {
                        fg.adapter.clearData()
                    }
                    fg.adapter.addData(it)
                    investmentNextPage++
                }
                /* *//* Load More *//*
                investmentMaxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = investmentNextPage < investmentMaxPage ||
                        (any?.totalRecords ?: 0) > (investmentNextPage * 10)

                *//*Data*//*
                if (any?.responseCollection != null &&
                    any?.responseStatus == 1
                ) {
                    if (!any.responseCollection.isNullOrEmpty()) {
                        fg.rootView.rvInvestors.visibility = View.VISIBLE
                        if (investmentNextPage == 1) {
                            fg.adapter.clearData()
                            fg.adapter.setData(any.responseCollection!!)
                        } else if (investmentNextPage > 1) {
                            fg.adapter.addData(any.responseCollection!!)
                        }
                        investmentNextPage++

                    } else {
                        if (investmentNextPage == 1) {
                            fg.adapter.clearData()
                        }
                    }
                } else {
                    if (investmentNextPage == 1) {
                        fg.adapter.clearData()
                    }
                }*/

                /*  any?.myInvestmentsTotals?.let {
                      val totalAmt = "Total: ${any?.myInvestmentsTotals ?: "0"}"
                      fg.updateTotalAmount(totalAmt)
                  }*/
                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })

    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        if (fg.binding.swipeInvest.isRefreshing) {
            fg.binding.swipeInvest.isRefreshing = false
        }
        fg.isLoading = false
        noData()
    }

    private fun noData() {
        if (fg.adapter.dataList.isNullOrEmpty()) {
            fg.binding.layoutNoInvestment.root.visibility = View.VISIBLE
            fg.binding.rvInvestors.visibility = View.GONE
        } else {
            fg.binding.layoutNoInvestment.root.visibility = View.GONE
            fg.binding.rvInvestors.visibility = View.VISIBLE
        }
    }

}