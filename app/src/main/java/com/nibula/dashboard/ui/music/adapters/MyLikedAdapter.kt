package com.nibula.dashboard.ui.music.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.request_to_buy.navigate
import com.nibula.response.artists.ArtistData
import com.nibula.response.details.File
import com.nibula.utils.interfaces.OnLikedSong

class MyLikedAdapter(val context: Context, val navigate: navigate, var onLikedSong: OnLikedSong) :
    RecyclerView.Adapter<MyLikedAdapter.ViewHolder>() {
    private var previousPosition = -1

/*
    lateinit var binding: ItemMyLikedBinding
*/

/*
    class ViewHolder(itemView: ItemMyLikedBinding) : RecyclerView.ViewHolder(itemView.root)
*/


    class ViewHolder(itemVew: View) : RecyclerView.ViewHolder(itemVew) {
        val songName: TextView = itemVew.findViewById(R.id.tvsongName)
        val tvTitle: TextView = itemVew.findViewById(R.id.tvTitle)
        val ivPlay: ImageView = itemVew.findViewById(R.id.ivPlay)
        val ivFav: ImageView = itemVew.findViewById(R.id.ivFav)
        val roundedImageView: ImageView = itemVew.findViewById(R.id.clImage)
    }

    var dataList = mutableListOf<ArtistData>()

    fun setData(data: MutableList<ArtistData>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun updateData(position: Int) {
        dataList.removeAt(position)
        notifyDataSetChanged()

    }

    /*
           fun addData(data: MutableList<MyPlayListData>) {
               dataList.addAll(data)
               notifyDataSetChanged()
           }
    */
    fun clearData() {
        dataList.clear()
    }

/*
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        binding = ItemMyLikedBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }
*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyLikedAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_my_liked, parent, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {

        return dataList.size
    }

    override fun onBindViewHolder(
        holder: ViewHolder, position: Int
    ) {
        val playListData = dataList[position]


        when (playListData.RecordTypeId) {
            1 -> {
                holder.tvTitle.text = playListData.artistName

                holder.songName.text = playListData.files?.get(0)?.songTitle
                Glide.with(context).load(playListData.recordImage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
                    .into(holder.roundedImageView)
            }
            else -> {
                holder.tvTitle.text = playListData.artistName

                holder.songName.text = playListData.recordTitle
                Glide.with(context).load(playListData.recordImage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
                    .into(holder.roundedImageView)
            }
        }


        holder.itemView.setOnClickListener {
            val fileArrayList = ArrayList<File>()
            fileArrayList.add(
                File(
                    playListData.FileId?.toInt(), 120, "", false, false, "", null
                )
            )
            navigate.playSong(
                fileArrayList,
                0,
                false,
                playListData.recordTitle!!,
                playListData.recordId!!,
                1,
                "",
                false
            )

        }

/*
        holder.ivPlay.setOnClickListener {

            if (navigate != null) {
                if (previousPosition == position) {
                    previousPosition = -1
                }
                if (previousPosition == -1) {
                    previousPosition = position
                    playListData.isPlaying = true
                } else {
                    playListData.isPlaying = false
                    previousPosition = position
                }

                val fileArrayList = ArrayList<File>()
                fileArrayList.add(
                    File(
                        playListData.FileId?.toInt(), 120, "", false, false,"",null
                    )
                )
                navigate.playSong(
                    fileArrayList,
                    0,
                    false,
                    playListData.recordTitle!!,
                    playListData.recordId!!,
                    1,
                    "",
                    false
                )

                notifyDataSetChanged()
            }
        }
*/


        holder.ivFav.setOnClickListener {

            Log.d("Play List Data is", playListData.toString())
            onLikedSong.onLikedSong(
                playListData.recordId.toString(),
                holder.absoluteAdapterPosition,
                playListData?.files?.get(0)?.id
            )

        }
    }

    fun refreshAdapter() {
        if (previousPosition == -1) return
        dataList[previousPosition].isPlaying = false
        notifyDataSetChanged()
    }

    fun playPause(isPlaying: Boolean) {
        if (previousPosition == -1) return
        if (isPlaying) {
            dataList[previousPosition].isPlaying = true

        } else {
            dataList[previousPosition].isPlaying = false

        }
        notifyDataSetChanged()
    }
}