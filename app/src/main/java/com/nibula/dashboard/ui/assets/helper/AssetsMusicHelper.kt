package com.nibula.dashboard.ui.assets.helper

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ui.assets.AssetMusicFragment
import com.nibula.response.musicprofile.ProfileMusicReponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.CommonUtils

class AssetsMusicHelper(val fg: AssetMusicFragment) : BaseHelperFragment() {

    var musicNextPage: Int = 1
    var musicMaxPage: Int = 0

    fun getMusicListing(artistId: String, isProgressDialog: Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        fg.isLoading = true

        val helper =
            ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.call = helper.getMusicListing(artistId, musicNextPage)
        fg.call!!.enqueue(object : CallBackManager<ProfileMusicReponse>() {
            override fun onSuccess(any: ProfileMusicReponse?, message: String) {
                /*Load More*/
                musicMaxPage = ((any?.myMusic?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = musicNextPage < musicMaxPage ||
                        (any?.myMusic?.totalRecords ?: 0) > (musicNextPage * 10)

                /*Data*/
                if (any?.myMusic != null &&
                    any.myMusic?.responseStatus == 1
                ) {
                    if (!any.myMusic?.responseCollection.isNullOrEmpty()) {
                        fg.binding.musicRv.visibility = View.VISIBLE
                        if (musicNextPage == 1) {
                            fg.adapter.setData(any.myMusic?.responseCollection!!)
                        } else if (musicNextPage > 1) {
                            fg.adapter.addData(any.myMusic?.responseCollection!!)
                        }
                        musicNextPage++

                    } else {
                        if (musicNextPage == 1) {
                            fg.adapter.clearData()
                        }
                    }
                } else {
                    if (musicNextPage == 1) {
                        fg.adapter.clearData()
                    }
                }

                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {

                if (error.errorMessage.equals("HTTP 401 Unauthorized")) {
                    CommonUtils.logOut(fg.requireActivity())
                    dismiss(isProgressDialog)
                } else {
                    dismiss(isProgressDialog)
                }

            }

        })

    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        if (fg.binding.swipeMusic.isRefreshing) {
            fg.binding.swipeMusic.isRefreshing = false
        }
        fg.isLoading = false
        noData()
    }

    private fun noData() {
        if (fg.adapter.listData.isNullOrEmpty()) {
            fg.binding.layoutNoMusic.root.visibility = View.VISIBLE
            fg.binding.musicRv.visibility = View.GONE
        } else {
            fg.binding.layoutNoMusic.root.visibility = View.GONE
            fg.binding.musicRv.visibility = View.VISIBLE
        }
    }

}