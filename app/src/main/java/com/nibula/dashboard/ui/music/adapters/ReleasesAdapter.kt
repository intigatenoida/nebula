package com.nibula.dashboard.ui.music.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.databinding.ItemReleasedBinding
import com.nibula.request_to_buy.navigate
import com.nibula.response.music.releases.ReleasesData
import com.nibula.utils.AppConstant


class ReleasesAdapter(val context: Context, val navigate: navigate) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: ItemReleasedBinding

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    lateinit var dataList: MutableList<ReleasesData>

    fun setData(data: MutableList<ReleasesData>) {
        dataList = data
        notifyDataSetChanged()
    }

    fun addData(data: MutableList<ReleasesData>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        dataList.clear()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemReleasedBinding.inflate(inflater, parent, false)


        val holder = ViewHolder(binding.root)
        /* return ViewHolder(
             LayoutInflater.from(context).inflate(
                 R.layout.item_released,
                 parent,
                 false
             )
         )*/
        return holder
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
    }

    override fun getItemCount(): Int {
        return if (::dataList.isInitialized) dataList.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val temp = dataList[position]

        binding.tvAlbumTitle.visibility = GONE
        binding.tvAvaShare.visibility = GONE

        /*Image Section*/

        Glide.with(context).load(temp.recordImage).placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.IMMEDIATE)
            .into(binding.cvImage.clImage)

        holder.itemView.setOnClickListener {
            temp.id?.let {
                // navigate.gotorequestinvest(temp.id)


                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL, temp.recordImage
                    )
                    putString(
                        AppConstant.ID, temp.id
                    )
                }

                navigate.goToNewOffering(bundle)
            }
        }

    }

}