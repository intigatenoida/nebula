package com.nibula.dashboard.ui.music.helper

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ui.music.vfragments.ReleasedFragment
import com.nibula.response.music.releases.ReleasesResponses
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError



class ReleasedFragmentHelper(val fg: ReleasedFragment) : BaseHelperFragment() {

    var releaseNextPage: Int = 1
    var releaseMaxPage: Int = 0


    fun requestReleaseOfferings(search: String, isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.swpMain.isRefreshing &&
            isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        if (fg.call != null) {
            fg.call!!.cancel()
        }

        fg.isLoading = true

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.call = helper.requestReleases(releaseNextPage, search)
        fg.call!!.enqueue(object : CallBackManager<ReleasesResponses>() {
            override fun onSuccess(any: ReleasesResponses?, message: String) {
                /*Load More*/
                releaseMaxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = releaseNextPage < releaseMaxPage ||
                        (any?.totalRecords ?: 0) > (releaseNextPage * 10)

                /*Data*/
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.responseDataList.isNullOrEmpty()) {
                        fg.binding.rvOffering.visibility = View.VISIBLE
                        if (releaseNextPage == 1) {
                            fg.releasesAdapter.setData(any.responseDataList)
                        } else if (releaseNextPage > 1) {
                            fg.releasesAdapter.addData(any.responseDataList)
                        }
                        releaseNextPage++
                    } else {
                        if (releaseNextPage == 1) {
                            fg.releasesAdapter.clearData()
                        }
                    }
                } else {
                    if (releaseNextPage == 1) {
                        fg.releasesAdapter.clearData()
                    }
                }

                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {
                dismiss(isProgressDialog)
            }

        })
    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (fg.binding.swpMain.isRefreshing) {
            fg.binding.swpMain.isRefreshing = false
        } else if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        fg.isLoading = false
        noData()
    }

    private fun noData() {
        if (fg.releasesAdapter.dataList.isEmpty()) {
            fg.binding.tvHeader.visibility = View.GONE
            fg.binding.rvOffering.visibility = View.GONE
            fg.binding.incNoData.root.visibility = View.VISIBLE
            fg.binding.incNoData.tvMessage.text = fg.getString(R.string.no_album_released)
        } else {
            fg.binding.tvHeader.visibility = View.VISIBLE
            fg.binding.rvOffering.visibility = View.VISIBLE
            fg.binding.incNoData.root.visibility = View.GONE
        }
    }

}