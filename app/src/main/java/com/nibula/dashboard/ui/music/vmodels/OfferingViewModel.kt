package com.nibula.dashboard.ui.music.vmodels

import androidx.lifecycle.MutableLiveData
import com.nibula.base.BaseViewModel

class OfferingViewModel : BaseViewModel() {

    private lateinit var timeUpdate: MutableLiveData<Long?>

    fun getTimeLv(): MutableLiveData<Long?> {
        if (!::timeUpdate.isInitialized) {
            timeUpdate = MutableLiveData()
        }
        return timeUpdate
    }

    fun updateTimer(valueMillis: Long?) {
        if (::timeUpdate.isInitialized) {
            timeUpdate.value = valueMillis
        }
    }

}