package com.nibula.dashboard.ui.music.vfragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.base.BaseFragment
import com.nibula.dashboard.ui.music.adapters.MyLikedAdapter
import com.nibula.dashboard.ui.music.helper.MyLikedHelper
import com.nibula.databinding.LayoutMyLikedListBinding
import com.nibula.response.artists.ArtistData

import com.nibula.response.myplaysit.MyPlaylistResponse
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.interfaces.OnLikedSong
import retrofit2.Call

class MyLikedFragment : BaseFragment(), OnLikedSong {

    companion object {
        fun newInstance() = MyLikedFragment()
    }
    lateinit var rootView: View
    lateinit var helper: MyLikedHelper
    lateinit var adapter: MyLikedAdapter
    var call: Call<MyPlaylistResponse>? = null
    var isLoading = false
    var isDataAvailable = true
    var isSearchProgress: Boolean = false
    var searchKey: String = ""
    lateinit var binding: LayoutMyLikedListBinding
     var myPlyListData= mutableListOf<ArtistData>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.view_playlist, container, false)
            binding = LayoutMyLikedListBinding.inflate(inflater, container, false)
            rootView = binding.root
            initUI()
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (CommonUtils.isLogin(requireActivity())) {
            onResume2()
            LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
                playerReceiver, IntentFilter(AppConstant.IMAGE_CLOSE)
            )
            LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
                playPauseReceiver, IntentFilter(AppConstant.PLAY_PAUSE)
            )
        }
    }

    fun onResume2() {
        helper.playListNextPage = 1
        helper.requestGetMyPlaylist(searchKey ?: "", true)
    }

    override fun onStop() {
        super.onStop()
        if (call != null && call!!.isExecuted) {
            call!!.cancel()
        }
    }

    private fun initUI() {
        helper = MyLikedHelper(this)
        // goneAllView()
        setAdapter()
        binding.swpMain.setOnRefreshListener {
            helper.playListNextPage = 1
            helper.requestGetMyPlaylist(searchKey ?: "", false)
        }

    }

    private fun goneAllView() {
        binding.tvHeader.visibility = View.GONE
        binding.rvFast.visibility = View.GONE
    }

    private fun setAdapter() {

        /*   val layoutManager = LinearLayoutManager(requireActivity())
           binding.rvFast.layoutManager = layoutManager
           adapter = MyLikedAdapter(requireContext(), navigate)
           adapter.setData(arrayListOf())
           binding.rvFast.adapter = adapter
           val decoration = FastScrollRecyclerViewItemDecoration(activity)
           binding.rvFast.addItemDecoration(decoration)
           binding.rvFast.itemAnimator = DefaultItemAnimator()*/


        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvFast.layoutManager = layoutManager
        adapter = MyLikedAdapter(requireContext(), navigate, this@MyLikedFragment)
        binding.rvFast.adapter = adapter
        binding.rvFast.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading && isDataAvailable && totalItem < (lastVisibleItem + threshold)) {
                    helper.requestGetMyPlaylist(searchKey ?: "", false)
                }
            }
        })
    }

    fun clearReleasedPageCount() {
        helper.playListNextPage = 1
    }


    private val playerReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            adapter.refreshAdapter()
            /*if (recordId == lastRecordId) {
                val id = intent?.getIntExtra(AppConstant.ID, -1) ?: -1
                val isPlaying = intent?.getBooleanExtra(AppConstant.STATUS, false) ?: false
                val fromTopButton =
                    intent?.getBooleanExtra(AppConstant.FromTopButton, false) ?: false
                selectedPosition = intent?.getIntExtra(AppConstant.SECRET_KEY, -1) ?: -1
                //updatePlayAllButton(fromTopButton, isPlaying)
                adapter.updateSongStatus(id, isPlaying)
            }*/
        }
    }


    private val playPauseReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val isPlaying = intent?.getBooleanExtra("is_playing", false) ?: false
            adapter.playPause(isPlaying)
            /*if (recordId == lastRecordId) {
                val id = intent?.getIntExtra(AppConstant.ID, -1) ?: -1
                val isPlaying = intent?.getBooleanExtra(AppConstant.STATUS, false) ?: false
                val fromTopButton =intent?.getBooleanExtra(AppConstant.FromTopButton, false) ?: false
                selectedPosition = intent?.getIntExtra(AppConstant.SECRET_KEY, -1) ?: -1
                //updatePlayAllButton(fromTopButton, isPlaying)
                adapter.updateSongStatus(id, isPlaying)
            }*/
        }
    }


    fun unregisteredBroadcast() {
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(playerReceiver)

    }

    override fun onLikedSong(recordId: String, position: Int, fileId: Int?) {

        helper.recordFavorite(true, recordId, position,fileId!!)
    }



}