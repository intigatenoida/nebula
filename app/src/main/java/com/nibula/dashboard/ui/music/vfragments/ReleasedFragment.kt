package com.nibula.dashboard.ui.music.vfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.dashboard.ui.music.adapters.ReleasesAdapter
import com.nibula.dashboard.ui.music.helper.ReleasedFragmentHelper
import com.nibula.databinding.ViewMusicReleaseBinding
import com.nibula.databinding.ViewOfferListBinding
import com.nibula.response.music.releases.ReleasesResponses
import com.nibula.utils.CommonUtils
import retrofit2.Call


class ReleasedFragment : BaseFragment() {

    companion object {
        fun newInstance() = ReleasedFragment()
    }

    lateinit var rootView: View
    var call: Call<ReleasesResponses>? = null

    lateinit var releasesAdapter: ReleasesAdapter
    lateinit var helper: ReleasedFragmentHelper

    var isDataAvailable = false
    var isLoading = true

    var isSearchProgress: Boolean = false
    var searchKey: String? = ""
    lateinit var binding: ViewMusicReleaseBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.view_music_release, container, false)
            binding=ViewMusicReleaseBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            helper.requestReleaseOfferings(searchKey ?: "", true)
        }
    }

    override fun onStop() {
        super.onStop()
        if (call != null &&
            !call!!.isCanceled
        ) {
            call!!.cancel()
        }
    }

    private fun initUI() {
        helper = ReleasedFragmentHelper(this)
        goneAllView()
        setReleasesAdapter()

        binding.tvHeader.text  = CommonUtils.setSpannableWhiteBold(requireContext(),getString(R.string.message_music_released),22,39)
        binding.swpMain.setOnRefreshListener {
            helper.releaseNextPage = 1
            helper.releaseMaxPage = 0
            helper.requestReleaseOfferings(searchKey ?: "", false)
        }
    }

    private fun goneAllView() {
        binding.tvHeader.visibility = GONE
        binding.rvOffering.visibility = GONE
    }

    private fun setReleasesAdapter() {
        releasesAdapter =
            ReleasesAdapter(requireActivity(), navigate)
        releasesAdapter.setData(mutableListOf())
        val layoutManager = GridLayoutManager(requireActivity(), 2)
        binding.rvOffering.layoutManager = layoutManager
        binding.rvOffering.adapter = releasesAdapter

        binding.rvOffering.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.requestReleaseOfferings(searchKey ?: "", false)
                }
            }
        })
    }

    fun requestSearch() {
        helper.requestReleaseOfferings(searchKey ?: "", false)
    }

    fun clearReleasedPageCount() {
        helper.releaseNextPage = 1
        helper.releaseMaxPage = 0
    }
}