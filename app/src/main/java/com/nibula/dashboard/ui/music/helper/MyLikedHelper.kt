package com.nibula.dashboard.ui.music.helper

import android.view.View
import com.nibula.DBHandler.NebulaDBHelper
import com.nibula.DBHandler.PlayListData
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ui.music.vfragments.MyLikedFragment
import com.nibula.request.RecordFavoriteRequest
import com.nibula.request.myplaylist.MyPlayListRequest
import com.nibula.response.BaseResponse
import com.nibula.response.artists.ArtistData
import com.nibula.response.myplaysit.MyPlaylistResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers


class MyLikedHelper(val fg: MyLikedFragment) : BaseHelperFragment() {

    var playListNextPage: Int = 1
    private val activityScope = CoroutineScope(Dispatchers.Main)

    fun requestGetMyPlaylist(search: String, isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (!fg.binding.swpMain.isRefreshing && isProgressDialog) {
            fg.showProcessDialog()
        }

        if (fg.call != null) {
            fg.call!!.cancel()
        }
        fg.isLoading = true

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.call = helper.getMyLikedSong(MyPlayListRequest(playListNextPage, search))
        fg.call!!.enqueue(object : CallBackManager<MyPlaylistResponse>() {

            override fun onSuccess(any: MyPlaylistResponse?, message: String) {
                if (any?.responseStatus != null && any.responseStatus == 1) {
                    dismiss(isProgressDialog)

                    if (!any.playListData.isNullOrEmpty()) {
                        fg.binding.incNoData.noData.visibility = View.GONE
                        fg.binding.incNoData.tvMessage.visibility = View.GONE
                        fg.binding.rvFast.visibility = View.VISIBLE
                        if (playListNextPage == 1) {
                            fg.myPlyListData.clear()
                            fg.adapter.clearData()
                        }

                        for (i in 0 until any.playListData!!.size) {

                            val artistList = any.playListData!![i]?.artistData
                            if (!artistList.isNullOrEmpty()) {

                                fg.myPlyListData.addAll(artistList!!)
                            }
                        }
                        fg.adapter.setData(fg.myPlyListData)
                        fg.isDataAvailable = (fg.myPlyListData.size % 10 == 0)
                        playListNextPage++
                    } else {
                        fg.binding.incNoData.noData.visibility = View.VISIBLE
                        fg.binding.incNoData.tvMessage.visibility = View.VISIBLE
                        fg.binding.incNoData.tvMessage.setText(
                            fg.requireActivity().getString(R.string.no_liked_found)
                        )
                        fg.binding.rvFast.visibility = View.GONE
                        dismiss(isProgressDialog)
                    }
                } else {
                    dismiss(isProgressDialog)
                }
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {

                if (error.errorMessage.equals("HTTP 401 Unauthorized")) {
                    CommonUtils.logOut(fg.requireActivity())
                    dismiss(isProgressDialog)
                } else {
                    dismiss(isProgressDialog)
                }
            }

        })
    }

    private fun addDataToDatabase(startWith: String, artistList: MutableList<ArtistData>) {
        for (artist in artistList) {
            if (!artist.recordId.isNullOrEmpty() && !artist.artistName.isNullOrEmpty() && !artist.artistId.isNullOrEmpty() && !artist.recordTitle.isNullOrEmpty() && !artist.recordImage.isNullOrEmpty()) {
                val playListData = PlayListData(
                )
                playListData.artistId = artist.artistId
                playListData.recordID = artist.recordId
                playListData.artistName = artist.artistName
                playListData.recordTitle = artist.recordTitle
                playListData.recordImage = artist.recordImage
                playListData.startWith = startWith
                playListData.recordType = artist.RecordType
                playListData.recordTypeId = artist.RecordTypeId
                playListData.fileId = artist.files?.get(0)?.id.toString()
                NebulaDBHelper(fg.requireContext()).InsertPlayListData(playListData)
            }

        }
    }

    private fun dismiss(isProgressDialog: Boolean) {
        fg.isSearchProgress = false
        if (fg.binding.swpMain.isRefreshing) {
            fg.binding.swpMain.isRefreshing = false
        } else if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }

    fun recordFavorite(isProgressDialog: Boolean, recordId: String, position: Int, fileId: Int) {
        if (!fg.isOnline()) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }
        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        var notifyRequest = RecordFavoriteRequest()
        notifyRequest.recordId = recordId
        notifyRequest.fileID = fileId
        notifyRequest.isSelected = false

        val call = helper.recordFavorite(notifyRequest, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {

                any?.responseStatus?.let {
                    when (it) {
                        1 -> {

                            fg.adapter.updateData(position)

                        }
                        else -> {
                            CustomToast.showToast(
                                fg.requireContext(),
                                any.responseMessage
                                    ?: fg.getString(R.string.message_something_wrong),
                                CustomToast.ToastType.FAILED
                            )
                        }
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                fg.hideProcessDialog()
            }

        })

    }

}