package com.nibula.dashboard.ui.assets.helper

import android.view.View
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ui.assets.AssetMusicFragment
import com.nibula.dashboard.ui.assets.WalletFragment
import com.nibula.request.login_request
import com.nibula.response.balance.MyBalanceResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils


class AssetsFragmentHelper(val fg: WalletFragment) : BaseHelperFragment() {


    fun getBalance(isProgressDialog: Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper = ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        val request = login_request()
        val observable = helper.getBalance(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<MyBalanceResponse>() {
            override fun onSuccess(any: MyBalanceResponse?, message: String?) {
                if (any?.response != null &&
                    any.responseStatus == 1
                ) {

                    CommonUtils.triggerWalletViewEvent(
                        any,
                        fg?.requireContext()
                            ?.let { PrefUtils.getValueFromPreference(it, PrefUtils.PHONE_NO) }!!
                            .toString()
                    )
                    fg.isDefaultAddressAvailable = any.response?.isDefaultAddressAvailable ?: false
                    fg.currentBalance = any.response?.currentBalanceAmount ?: 0.00
                    fg.manageWithdrawalUI()

                    fg.binding.tvCurrentBalance.text =
                        "${any.response!!.currencyType ?: "USD"}${
                            CommonUtils.trimDecimalToTwoPlaces(
                                fg.currentBalance
                            )
                        }"
                    val totalEarning =
                        "${any.response!!.currencyType ?: "USD"}${
                            CommonUtils.trimDecimalToTwoPlaces(
                                any.response?.totalEarningsAmount ?: 0.00
                            )
                        }"
                    fg.binding.totalEarning.text = totalEarning
                    val royaltyAmt =
                        "${any.response!!.currencyType ?: "USD"}${
                            CommonUtils.trimDecimalToTwoPlaces(
                                any.response?.royaltyAmount ?: 0.00
                            )
                        }"
                    fg.binding.royaltyAmt.text = royaltyAmt
                    val cashBackAmt =
                        "${any.response!!.currencyType ?: "USD"}${
                            CommonUtils.trimDecimalToTwoPlaces(
                                any.response?.cashBackAmount ?: 0.00
                            )
                        }"
                    fg.binding.cashbackAmt.text = cashBackAmt
                    val raisedAmt =
                        "${any.response!!.currencyType ?: "USD"}${
                            CommonUtils.trimDecimalToTwoPlaces(
                                any.response?.totalRaisedAmount?.toDouble() ?: 0.00
                            )
                        }"
                    fg.binding.raisedAmt.text = raisedAmt
                    PrefUtils.saveValueInPreference(
                        fg.requireContext(),
                        PrefUtils.MY_MONEY,
                        (any.response?.totalRaisedAmount ?: 0.0f)
                    )

                    val myMusicFg =
                        fg.adapter.getRegisteredFragment(fg.binding.tabLayout.selectedTabPosition)
                    if (myMusicFg != null &&
                        myMusicFg is AssetMusicFragment
                    ) {

                        myMusicFg.updateTotalAmount(any.response!!.currencyType)
                    }

                    if (any.response?.pendingInvestmentRequestTotalAmout.isNullOrEmpty()) {
                        fg.binding.tvPending.visibility = View.GONE
                    } else {
                        fg.binding.tvPending.visibility = View.VISIBLE
                        val color =
                            ContextCompat.getColor(
                                fg.requireActivity(),
                                R.color.green_shade_1_100per
                            )
                        fg.binding.tvPending.text =
                            CommonUtils.underlineSpannable(
                                "${any.response?.pendingInvestmentRequestTotalAmout} ${
                                    fg.getString(
                                        R.string.pending
                                    )
                                }", color
                            )
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }

        }))
    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }

}