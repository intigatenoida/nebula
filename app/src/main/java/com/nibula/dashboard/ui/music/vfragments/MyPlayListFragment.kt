package com.nibula.dashboard.ui.music.vfragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.DBHandler.NebulaDBHelper
import com.nibula.DBHandler.PlayListData
import com.nibula.base.BaseFragment
import com.nibula.dashboard.ui.music.FastScrollingAdapter
import com.nibula.dashboard.ui.music.helper.MyPlayListFragmentHelper
import com.nibula.databinding.ViewPlaylistBinding
import com.nibula.response.artists.ArtistData
import com.nibula.response.myplaysit.MyPlaylistResponse
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import retrofit2.Call

class MyPlayListFragment : BaseFragment() {

    companion object {
        fun newInstance() = MyPlayListFragment()
    }

    lateinit var rootView: View
    lateinit var helper: MyPlayListFragmentHelper
    lateinit var adapter: FastScrollingAdapter

    var call: Call<MyPlaylistResponse>? = null

    var isDataAvailable = false
    var isLoading = true
    var databaseList = ArrayList<PlayListData>()
    var isSearchProgress: Boolean = false
    var searchKey: String = ""
    lateinit var binding:ViewPlaylistBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.view_playlist, container, false)
            binding=ViewPlaylistBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (CommonUtils.isLogin(requireActivity())) {
            onResume2()
            LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
                playerReceiver,
                IntentFilter(AppConstant.IMAGE_CLOSE)
            )

            LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
                playPauseReceiver,
                IntentFilter(AppConstant.PLAY_PAUSE)
            )
        }
    }

    fun onResume2() {
        databaseList = NebulaDBHelper(requireContext()).playListAllData
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            if (databaseList.isNotEmpty() && !PrefUtils.getBooleanValue(
                    requireContext(),
                    PrefUtils.PLAYLIST_REFRESH
                )
            ) {
                requestSearch()
            } else {
                helper.requestGetMyPlaylist(searchKey ?: "", true)
            }
        } else if (databaseList.isNotEmpty()) {
            helper.updateAdapterFromDb(databaseList)
        }
    }

    override fun onStop() {
        super.onStop()
        if (call != null && call!!.isExecuted) {
            call!!.cancel()
        }
    }

    private fun initUI() {
        helper = MyPlayListFragmentHelper(this)
        // setSpannableHeader()
        goneAllView()
        setAdapter()
        binding.swpMain.setOnRefreshListener {
            helper.playListNextPage = 1
            if (searchKey.isNullOrEmpty()) {
                NebulaDBHelper(requireContext()).clearPlayListDatabase()
            }
            helper.requestGetMyPlaylist(searchKey ?: "", false)
        }

    }


    private fun goneAllView() {
        binding.tvHeader.visibility = View.GONE
        binding.rvFast.visibility = View.GONE
    }

    private fun setAdapter() {

        val layoutManager = LinearLayoutManager(requireActivity())
        binding.rvFast.layoutManager = layoutManager
        adapter = FastScrollingAdapter(requireContext(), navigate)
        adapter.setData(ArrayList<ArtistData>(), HashMap<String, Int>())
        binding.rvFast.adapter = adapter
    /*    val decoration = FastScrollRecyclerViewItemDecoration(activity)
        binding.rvFast.addItemDecoration(decoration)*/
        binding.rvFast.itemAnimator = DefaultItemAnimator()

        binding.rvFast.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.requestGetMyPlaylist(searchKey ?: "", false)
                }
            }
        })
    }

    fun requestSearch() {
        if (searchKey.isEmpty()) {
            helper.updateAdapterFromDb(databaseList)
        } else {
            searchIntoLocalDatabase()
        }
    }

    private fun searchIntoLocalDatabase() {
        val tempData = ArrayList<PlayListData>()
        for (data in databaseList) {
            if (data.artistName.toLowerCase().contains(searchKey) || data.recordTitle.toLowerCase()
                    .contains(searchKey)
            ) {
                tempData.add(data)
            }
        }
        if (tempData.isEmpty()) {
            helper.requestGetMyPlaylist(searchKey, true)
        } else {
            helper.updateAdapterFromDb(tempData)
        }

    }

    fun clearReleasedPageCount() {
        helper.playListNextPage = 1
    }


    private val playerReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            adapter.refreshAdapter()
            /*if (recordId == lastRecordId) {
                val id = intent?.getIntExtra(AppConstant.ID, -1) ?: -1
                val isPlaying = intent?.getBooleanExtra(AppConstant.STATUS, false) ?: false
                val fromTopButton =
                    intent?.getBooleanExtra(AppConstant.FromTopButton, false) ?: false
                selectedPosition = intent?.getIntExtra(AppConstant.SECRET_KEY, -1) ?: -1
                //updatePlayAllButton(fromTopButton, isPlaying)
                adapter.updateSongStatus(id, isPlaying)
            }*/
        }
    }


    private val playPauseReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val isPlaying = intent?.getBooleanExtra("is_playing", false) ?: false
            adapter.playPause(isPlaying)
            /*if (recordId == lastRecordId) {
                val id = intent?.getIntExtra(AppConstant.ID, -1) ?: -1
                val isPlaying = intent?.getBooleanExtra(AppConstant.STATUS, false) ?: false
                val fromTopButton =intent?.getBooleanExtra(AppConstant.FromTopButton, false) ?: false
                selectedPosition = intent?.getIntExtra(AppConstant.SECRET_KEY, -1) ?: -1
                //updatePlayAllButton(fromTopButton, isPlaying)
                adapter.updateSongStatus(id, isPlaying)
            }*/
        }
    }


    fun unregisteredBroadcast() {
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(playerReceiver)

    }
}