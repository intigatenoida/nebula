package com.nibula.dashboard.vmodel

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nibula.base.Event
import com.nibula.counter.TimerCounter
import java.util.*
import kotlin.collections.HashMap

class BaseActivityViewModel : ViewModel() {

    private lateinit var mStacks: HashMap<String, Stack<Fragment>>
    var mCurrentTab: String = ""
    var mCurrentTopTab: String = ""

    lateinit var manualBack: MutableLiveData<Boolean>

    val refreshAfterLogin = MutableLiveData<Event<Boolean?>>()

    private lateinit var timeUpdate: MutableLiveData<Long?>
    lateinit var timer: TimerCounter

    fun getBackStackMap(): HashMap<String, Stack<Fragment>> {
        if (!::mStacks.isInitialized) {
            mStacks = HashMap()
        }
        return mStacks
    }

    fun getBackManualBack(): LiveData<Boolean> {
        if (!::manualBack.isInitialized) {
            manualBack = MutableLiveData()
        }
        return manualBack
    }

    fun callManualBack(isBack:Boolean) {
        manualBack.value = isBack
    }

    fun refresh(events: Boolean) {
        refreshAfterLogin.value = Event(events)
    }

    fun getTimeLv(): MutableLiveData<Long?> {
        if (!::timeUpdate.isInitialized) {
            timeUpdate = MutableLiveData()
            timer = TimerCounter.newInstance(object : TimerCounter.TimerCommunicator {
                override fun onTick(millisUntilFinished: Long) {
                    updateTimer(millisUntilFinished)
                }

                override fun onFinish() {

                }
            })
            timer.initCounter()
        }
        return timeUpdate
    }


    fun updateTimer(valueMillis: Long?) {
        if (::timeUpdate.isInitialized) {
            timeUpdate.value = valueMillis
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (::timer.isInitialized) {
            timer.clear()
        }

    }
}