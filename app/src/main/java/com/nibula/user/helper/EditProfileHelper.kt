package com.nibula.user.helper

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.nibula.BuildConfig
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.customview.CustomToast
import com.nibula.response.UserProfileUploadResponse
import com.nibula.response.carificationResponse.PhoneEmailVerificationResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.uploadimage.UploadImageHelper
import com.nibula.user.edit_profile.EditProfileActivity
import com.nibula.user.profile.ProfileResponce
import com.nibula.user.sign_up.modal.CounytryModal
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class EditProfileHelper(val context: EditProfileActivity) : BaseHelperFragment() {

    private var sendFile: File? = null
    private var originalFilePath: String? = null
    private var originalUri: Uri? = null
    private var isCameraSelected = false

    var updatedUrl: String? = ""

    //Get Country Listing From API
    fun getCountryList(isProgressDialog: Boolean) {
        if (!context.isOnline()) {
            context.showToast(
                context,
                context.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        if (isProgressDialog) {
            context.showProcessDialog()
        }
        val helper = ApiClient.getClientAuth().create(ApiAuthHelper::class.java)
        val observable = helper.getCountryList()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<CounytryModal>() {
            override fun onSuccess(any: CounytryModal?, message: String?) {
                any?.let {
                    context.hideProcessDailog()
                    if (it.responseStatus == 1) {
                        it.responseCollection?.let { list ->
                            if (list.isNotEmpty()) {
                                context.updateList(list)
                            }
                        }
                    } else {
                        it.responseMessage?.let { it1 ->
                            showBottomPrompt(context.getString(R.string.message_something_wrong_1))
                        }
                    }
                }
            }

            override fun onError(error: RetroError?) {
                error?.errorMessage?.let {
                    showBottomPrompt(context.getString(R.string.message_something_wrong_1))
                }
            }
        }))
    }

    fun getProfile(isProgressDialog: Boolean) {
        if (!context.isOnline()) {
            context!!.showToast(
                context!!,
                context!!.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            context!!.showProcessDialog()
        }

        val helper = ApiClient.getClientAuth(context!!).create(ApiAuthHelper::class.java)
        val observable = helper.getProfile("")
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<ProfileResponce>() {
            override fun onSuccess(any: ProfileResponce?, message: String?) {
                if (any != null &&
                    any.responseStatus == 1 &&
                    any.userProfile != null
                ) {

                    context.updateUserEmailStatus(
                        any.userProfile!!.isUserVerified,
                        any.userProfile!!.email!!
                    )

                    context?.setUserProfileData(any.userProfile!!)

                }
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }

        }))

    }


    fun showBottomPrompt(descriptions: String) {
        val bPrompt = PNBSheetFragment(
            descriptions,
            context.getString(R.string.retry),
            context.getString(R.string.cancel),
            context
        )
        bPrompt.isCancelable = false
        bPrompt.setColor(
            ContextCompat.getColor(context, R.color.colorAccent),
            ContextCompat.getColor(context, R.color.white),
            ContextCompat.getColor(context, R.color.gray_1_51per),
            ContextCompat.getColor(context, R.color.gray_1_51per)
        )
        bPrompt.show(
            context.supportFragmentManager,
            "PositiveNegativeBottomSheetFragment"
        )
    }

    fun setImageUri(): Uri {
        val file = UploadImageHelper.tempFileReturn(
            context,
            UploadImageHelper.TEMPORARY_PROFILE_IMAGE
        )
        originalUri = FileProvider.getUriForFile(
            context,
            "${BuildConfig.APPLICATION_ID}.${context.getString(R.string.file_provider_name)}",
            file
        )
        originalFilePath = file.absolutePath
        return originalUri!!
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                AppConstant.REQUEST_UPLOAD_IMAGE -> {
                    handleImageResult(data)
                }
            }
        }
    }

    private fun handleImageResult(data: Intent?) {
        context.showProcessDialog()
        isCameraSelected = false
        val disposableObserver = object : DisposableObserver<String>() {
            override fun onComplete() {
                context.hideProcessDailog()
            }

            override fun onNext(t: String) {
                context.hideProcessDailog()
                if (originalUri == null) {
                    context.showToast(
                        context,
                        context.getString(R.string.message_upload_image),
                        CustomToast.ToastType.FAILED
                    )
                    return
                }
                //start cropping
                startCropImageActivity(originalUri!!)
            }

            override fun onError(e: Throwable) {
                context.hideProcessDailog()
            }

        }
        disposables.add(getObserver(data, disposableObserver))
    }

    private fun getObserver(
        data: Intent?,
        disposableObserver: DisposableObserver<String>
    ): DisposableObserver<String> {
        return getObservable(data)
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(disposableObserver)
    }

    private fun getObservable(data: Intent?): Observable<String> {
        return Observable.create { emitter ->
            if (data?.data != null) {
                //Photo from gallery
                val f = UploadImageHelper.copyFileFromStream(
                    context,
                    data.data!!,
                    UploadImageHelper.TEMPORARY_PROFILE_IMAGE
                )
                originalUri = Uri.fromFile(f)
                originalFilePath = f.absolutePath
            } else {
                //Photo from Camera
                isCameraSelected = true
            }

            if (!originalFilePath.isNullOrEmpty()) {
                emitter.onNext(originalFilePath ?: "")
                emitter.onComplete()
            } else {
                emitter.onError(Throwable("queryImageUrl is empty"))
            }
        }
    }

    fun startCropImageActivity(imageUri: Uri) {
        CropImage.activity(imageUri?.let { it })
            .setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setCropShape(CropImageView.CropShape.RECTANGLE)
            .setAspectRatio(1, 1)
            .start(context)
    }

    fun cropDataSection(uri: Uri?) {
        if (uri == null) {
            context.showToast(
                context,
                "Record image cropping failed",
                CustomToast.ToastType.FAILED
            )
            return
        }
        if (sendFile != null) {
            sendFile!!.delete()
        }
        val path = uri.path ?: ""
        sendFile = if (path.isNotEmpty()) {
            File(path)
        } else {
            null
        }
        if (sendFile == null) {
            context.showToast(
                context,
                context.getString(R.string.please_select_record_image),
                CustomToast.ToastType.FAILED
            )
            return
        }
        uploadImage(true)
    }

    private fun uploadImage(isProgressDialog: Boolean) {
        if (!context.isOnline()) {
            context.showToast(
                context,
                context.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            context.hideProcessDailog()
            return
        }

        if (isProgressDialog) {
            context.showProcessDialog()
        }

        val mediaType = "image/*".toMediaTypeOrNull()!!
        val requestBody = sendFile?.asRequestBody(mediaType) ?: "".toRequestBody(mediaType)
        val body =
            MultipartBody.Part.createFormData("UserImage", sendFile?.name ?: "", requestBody)

        val helper = ApiClient.getClientAuth(context).create(ApiAuthHelper::class.java)
        val observable = helper.updateImage(body)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<UserProfileUploadResponse>() {
            override fun onSuccess(any: UserProfileUploadResponse?, message: String?) {
                if (any?.responseStatus == 1) {
                    context.showToast(
                        context,
                        any.responseMessage ?: "",
                        CustomToast.ToastType.SUCCESS
                    )
                    updatedUrl = any.userImage
                    CommonUtils.loadImageWithOutCache(
                        context,
                        updatedUrl,
                        context.binding.profileImage,
                        R.drawable.ic_record_user_place_holder
                    )
                }
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }
        }))

    }

    fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            context.hideProcessDailog()
        }
    }


    fun emailVerify() {
        if (!context.isOnline()) {
            context.showToast(
                context,
                context.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            context.hideProcessDailog()
            return
        }

        context.showProcessDialog()

        val helper = ApiClient.getClientAuth(context).create(ApiAuthHelper::class.java)
        val observable = helper.getVerificationStatus(1)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<PhoneEmailVerificationResponse>() {
            override fun onSuccess(any: PhoneEmailVerificationResponse?, message: String?) {
                if (any?.responseStatus == 1) {

                    context.showToast(
                        context,
                        any.responseMessage ?: "",
                        CustomToast.ToastType.SUCCESS
                    )
                } else {
                    context.showToast(
                        context,
                        any!!.responseMessage ?: "",
                        CustomToast.ToastType.FAILED
                    )
                }

                dismiss(true)
            }

            override fun onError(error: RetroError?) {
                context.showToast(
                    context,
                    error.toString() ?: "",
                    CustomToast.ToastType.SUCCESS
                )
                dismiss(true)
            }
        }))

    }

}