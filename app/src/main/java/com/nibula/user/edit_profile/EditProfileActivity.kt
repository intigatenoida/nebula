package com.nibula.user.edit_profile

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.ActivityEditProfileBinding
import com.nibula.permissions.PermissionHelper
import com.nibula.retrofit.*
import com.nibula.uploadimage.UploadImageHelper
import com.nibula.user.helper.EditProfileHelper
import com.nibula.user.profile.ProfileResponce
import com.nibula.user.profile.UserProfile
import com.nibula.user.sign_up.adapter.CountryListAdapter
import com.nibula.user.sign_up.modal.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import com.nibula.utils.interfaces.DialogFragmentClicks
import com.theartofdev.edmodo.cropper.CropImage



class EditProfileActivity : BaseActivity(), DialogFragmentClicks, CountryListAdapter.Communicator {

    lateinit var imageUri: Uri
    lateinit var CountryList: MutableList<ResponseCollection>
    lateinit var madapter: CountryListAdapter
    lateinit var helper: EditProfileHelper
    lateinit var dialog: BottomSheetDialog
    lateinit var selectedData: ResponseCollection
    var selectedCountryCode: String = ""
    var isUserVerified: Boolean = false
    var imageChooseDialogOpen = false
    var previousLength = 0
    lateinit var binding:ActivityEditProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_edit_profile)
        binding= ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //--> Init Helper
        helper = EditProfileHelper(this@EditProfileActivity)

        helper.getProfile(true)

        binding.tvVerifyMail.setOnClickListener {

            helper.emailVerify()
        }

        //--> Country Code Click Handler
        binding.vClick.setOnClickListener {
//            if (!::CountryList.isInitialized ||
//                CountryList.isEmpty()
//            ) {
//                showToast(
//                    this@EditProfileActivity,
//                    getString(R.string.country_list_error),
//                    CustomToast.ToastType.FAILED
//                )
//                return@setOnClickListener
//            }
//            initalizeBottomSheet()

        }

        binding.edtPhoneNumber.isEnabled = false

        binding.receiveRecommendationsSwitch.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                binding.receiveRecommendationsSwitch.changeButtonColor(Color.WHITE)
            } else {
                binding.receiveRecommendationsSwitch.changeButtonColor(
                    ContextCompat.getColor(
                        this@EditProfileActivity,
                        R.color.white
                    )
                )
            }
        }
        val mySpannableString = SpannableString(getString(R.string.change_profile_photo))
        mySpannableString.setSpan(UnderlineSpan(), 0, mySpannableString.length, 0)
        binding.changePhotoTv.text = mySpannableString
        binding.headerLayout.btnCancel.setOnClickListener {
            if (!helper.updatedUrl.isNullOrEmpty()) {
                val intent = Intent()
                intent.putExtra(AppConstant.ID, helper.updatedUrl)
                setResult(Activity.RESULT_OK, intent)
            } else {
                setResult(Activity.RESULT_CANCELED)
            }
            finish()
        }

        binding.edtPhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                formatPhoneNumber(s.toString(), 10)
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int) {

                            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })




        binding.headerLayout.btnSave.setOnClickListener {
            if ( binding.edtPhoneNumber.text.toString().length < 8 ||  binding.edtPhoneNumber.text.toString().length > 12) {
                CustomToast.showToast(
                    this@EditProfileActivity,
                    getString(R.string.valid_phone_no),
                    CustomToast.ToastType.FAILED
                )
            } else if ( binding.nameEdt.text.toString().trim().length < 3) {
                CustomToast.showToast(
                    this@EditProfileActivity,
                    getString(R.string.valid_user_name),
                    CustomToast.ToastType.FAILED
                )
            } else {
                getProfile()
            }
        }
        //-->change profile picture
        binding.vPhoto.setOnClickListener {
            if (PermissionHelper.isPermissionsAllowed(
                    this,
                    PermissionHelper.permissions,
                    true,
                    AppConstant.PERMISSION_WRITE_EXTERNAL_STORAGE
                )
            ) {
                chooseImage()
              }

            //CropImage.startPickImageActivity(this)
        }


        //--> set default data
        /*  if (intent?.extras != null) {
              val userProfile: UserProfile? = intent?.extras?.getParcelable("UserData")
              edtUserName?.setText(userProfile?.userName ?: "")
              edtBio?.setText(userProfile?.bio ?: "")
              edtBio?.setSelection(edtBio.text?.toString()?.length ?: 0)
              nameEdt?.setText(userProfile?.fullName ?: "")
              edtPhoneNumber?.setText(userProfile?.phoneNumber)

              if (userProfile?.isUserVerified == true) {

                  tvVerifyMail?.visibility = View.GONE
                  edtMail?.setText(userProfile.email)

              } else {
                  edtMail?.setText(userProfile?.email)
                  tvVerifyMail?.visibility = View.VISIBLE

              }
              userProfile?.countryCode?.let { countryCode ->
                  tv_countryCodeNew.text = countryCode
                  selectedData = ResponseCollection()
                  selectedData.countryCode = countryCode
                  selectedCountryCode = countryCode
              }
              receiveRecommendationsSwitch.isChecked = userProfile?.receiveRecommendation == true
              if (userProfile?.receiveRecommendation == true) {
                  receiveRecommendationsSwitch.changeButtonColor(Color.WHITE)
              }
              if (userProfile?.userImage?.isNotEmpty() == true) {
                  CommonUtils.loadImage(
                      this@EditProfileActivity,
                      userProfile.userImage,
                      profile_image,
                      R.drawable.ic_record_user_place_holder
                  )

              }
          }*/

        hideKeyboard()

    }

    private fun formatPhoneNumber(phoneNumber: String, maxLength: Int) {

        if (previousLength == phoneNumber.length) return
        var tempPhoneNumber = phoneNumber

        if (tempPhoneNumber.startsWith("0")) {
            if (tempPhoneNumber.contains(AppConstant.PREFIX_NUMBER)) {
                tempPhoneNumber = "${tempPhoneNumber.subSequence(1, tempPhoneNumber.length)}"
            } else {
                if (tempPhoneNumber.length == 1) {
                    tempPhoneNumber = AppConstant.PREFIX_NUMBER
                } else {
                    if (tempPhoneNumber.contains(")")) {
                        tempPhoneNumber = tempPhoneNumber.replace(")", "").trim()
                        tempPhoneNumber = tempPhoneNumber
                    } else {
                        tempPhoneNumber = tempPhoneNumber.replace("(", "").replace(")", "").trim()
                        tempPhoneNumber =
                            "${AppConstant.PREFIX_NUMBER}${
                                tempPhoneNumber.subSequence(
                                    1,
                                    tempPhoneNumber.length
                                )
                            }"
                    }

                }
            }
        } else if (tempPhoneNumber.startsWith("(") && !tempPhoneNumber.contains(AppConstant.PREFIX_NUMBER)) {
            tempPhoneNumber =
                tempPhoneNumber.replace("(", "").replace(")", "").trim()
            if (tempPhoneNumber.startsWith("0")) {
                tempPhoneNumber = tempPhoneNumber.replaceFirst("0", "")
            }

        }


        var isPrefixContain = false
        if (tempPhoneNumber.contains(AppConstant.PREFIX_NUMBER)) {
            isPrefixContain = true
            tempPhoneNumber =
                tempPhoneNumber.replace(AppConstant.PREFIX_NUMBER, "")
                    .replace(AppConstant.SEPRATER, "").trim()
        }
        val firstSepraterIndex = 3
        val secondSepraterIndex = 6
        if (tempPhoneNumber.length > firstSepraterIndex) {
            tempPhoneNumber = tempPhoneNumber.replace(AppConstant.SEPRATER, "").trim()
            var tempNumber = StringBuffer()
            for (i in tempPhoneNumber.indices) {
                if (i == firstSepraterIndex || i == secondSepraterIndex) {
                    tempNumber.append(AppConstant.SEPRATER)
                }
                tempNumber.append(tempPhoneNumber[i])
            }
            tempPhoneNumber = "${
                if (isPrefixContain) {
                    AppConstant.PREFIX_NUMBER
                } else {
                    ""
                }
            }$tempNumber"
        }

        previousLength = tempPhoneNumber.length
        binding.edtPhoneNumber.setText(tempPhoneNumber)
        binding.edtPhoneNumber.setSelection( binding.edtPhoneNumber.text.toString().length)
    }

    override fun onDestroy() {
        super.onDestroy()
        helper.onDestroy()
    }

    private fun chooseImage() {
        if (imageChooseDialogOpen) return

        imageChooseDialogOpen = true
        if (!isOnline()) {
            showToast(
                this,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        UploadImageHelper.getPickImageIntent(
            this,
            helper.setImageUri()
        )?.let {
            this@EditProfileActivity.startActivityForResult(
                it,
                AppConstant.REQUEST_UPLOAD_IMAGE
            )
        }
    }

    //--> get user profile
    private fun getProfile() {
        if (!isOnline()) {


            showToast(
                this,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }

        showProcessDialog()
        val userProfile = UserProfile(
            binding.edtBio.text.toString(),
            binding.edtMail.text.toString(),
            binding.nameEdt.text.toString(),
            binding.nameEdt.text.toString(),
            "",
            false,
            "",
            binding.edtPhoneNumber.text.toString().replace("-", ""),
            binding.receiveRecommendationsSwitch.isChecked,
            "",
            binding.edtUserName.text.toString(),
            0.0,
            0,
            "0",
            isUserVerified,
            false,
            binding.nameEdt.text.toString(),
            selectedCountryCode,
            "", ""
        )
        val helper = ApiClient.getClientAuth(this).create(ApiAuthHelper::class.java)
        val call = helper.updateProfile(
            userProfile
        )
        call.enqueue(object : CallBackManager<ProfileResponce>() {
            override fun onSuccess(any: ProfileResponce?, message: String) {
                hideProcessDailog()
                if (any != null && any.responseStatus == 1) {
                    PrefUtils.saveValueInPreference(
                        this@EditProfileActivity,
                        PrefUtils.PHONE_NO,
                        "${selectedData.countryCode} -${ binding.edtPhoneNumber.text.toString()}"
                    )
                    if (!this@EditProfileActivity.helper.updatedUrl.isNullOrEmpty()) {
                        val intent = Intent()
                        intent.putExtra(AppConstant.ID, this@EditProfileActivity.helper.updatedUrl)
                        setResult(Activity.RESULT_OK, intent)
                    } else {
                        setResult(Activity.RESULT_OK)
                    }
                    finish()

                    CommonUtils.triggerUpdateProfile(
                        any?.userProfile?.userName,
                        any?.userProfile?.email,
                        any.userProfile?.fullName,
                        any.userProfile?.bio
                    )
                    showToast(
                        this@EditProfileActivity,
                        any.responseMessage ?: "",
                        CustomToast.ToastType.SUCCESS
                    )
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (!helper.updatedUrl.isNullOrEmpty()) {
            val intent = Intent()
            intent.putExtra(AppConstant.ID, helper.updatedUrl)
            setResult(Activity.RESULT_OK, intent)
        } else {
            setResult(Activity.RESULT_CANCELED)
        }
        super.onBackPressed()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        for (grantResult in grantResults) {
            var isGranted = grantResult.equals(PackageManager.PERMISSION_GRANTED)
            if (isGranted) {
                chooseImage()
                break;
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        helper.onActivityResult(requestCode, resultCode, data)
        imageChooseDialogOpen = false
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                helper.cropDataSection(result.uri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(
                    this,
                    "Cropping failed: " + result.error,
                    CustomToast.ToastType.FAILED,
                    Toast.LENGTH_LONG
                )
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    // udpate list from server
    fun updateList(list: MutableList<ResponseCollection>) {
        val data = list[0]
        if (!::selectedData.isInitialized) {
            binding.tvCountryCodeNew.text = data.countryCode
        }
        selectedData = data
        CountryList = list
    }


    //--> init Bottom sheet
/*
    private fun initalizeBottomSheet() {
        dialog = BottomSheetDialog(this, R.style.Theme_Dialog)
        dialog.setContentView(R.layout.layout_country)
        dialog.setCancelable(true)
        dialog.dismissWithAnimation = true
        dialog.setOnShowListener { dialogInterface ->
            val bottomSheetDialog = dialogInterface as BottomSheetDialog
            CommonUtils.setupFullHeight(this@EditProfileActivity, bottomSheetDialog)
        }
        //dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        dialog.edtSearchCountry.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())
            }
        })
        madapter = CountryListAdapter(this@EditProfileActivity, CountryList, this)
        val layoutManager =
            LinearLayoutManager(this@EditProfileActivity, LinearLayoutManager.VERTICAL, false)
        dialog.rcv_country.layoutManager = layoutManager
        dialog.rcv_country.adapter = madapter
        dialog.show()
    }
*/

    //-- Filter list
/*
    private fun filter(text: String) {
        val filteredList: ArrayList<ResponseCollection> = ArrayList()
        for (item in CountryList) {
            if (item.countryName?.toLowerCase()!!
                    .contains(text.toLowerCase()) || item.countryCode?.toLowerCase()!!
                    .contains(text.toLowerCase())
            ) {
                filteredList.add(item)
            }
        }
        if (filteredList.isNotEmpty()) {
            dialog.noDataFoundTv.visibility = View.GONE
            dialog.rcv_country.visibility = View.VISIBLE
            madapter.filterList(filteredList)
        } else {
            dialog.noDataFoundTv.visibility = View.VISIBLE
            dialog.noDataFoundTv.tv_message.text = getString(R.string.no_county_found)
            dialog.rcv_country.visibility = View.GONE
        }

    }
*/

    //--> On country selected from adapter
    override fun onCountrySelcted(data: ResponseCollection) {
        if (::dialog.isInitialized) {
            dialog.dismiss()
        }
        selectedData = data
        binding.tvCountryCodeNew.text = data.countryCode
        hideKeyboard()
    }

    override fun onResume() {
        super.onResume()
        if (!::CountryList.isInitialized) {
            helper.getCountryList(true)
        }
    }

    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        if (bundle == null) {
            return
        }
        when (obj) {
            is PNBSheetFragment -> {
                if (!isFinishing) {
                    helper.getCountryList(true)
                }
            }
        }
        dialog.dismiss()
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        if (bundle == null) {
            return
        }
        when (obj) {
            is PNBSheetFragment -> {
                finish()
            }
        }
        dialog.dismiss()
    }


    fun updateUserEmailStatus(isVerifiedStatus: Boolean, emailString: String) {
        if (isVerifiedStatus) {
            binding.edtMail.visibility = View.VISIBLE
            binding.tvVerifyMail.visibility = View.GONE
            isUserVerified = true

        } else {
            isUserVerified = false
            binding.tvVerifyMail.visibility = View.VISIBLE
            binding.edtMail.text = emailString


        }
    }


    fun setUserProfileData(userProfile: UserProfile) {
        val userProfile: UserProfile? = userProfile
        binding.edtUserName?.setText(userProfile?.userName ?: "")
        binding.edtBio?.setText(userProfile?.bio ?: "")
        binding.edtBio?.setSelection( binding.edtBio.text?.toString()?.length ?: 0)
        binding.nameEdt?.setText(userProfile?.fullName ?: "")
        binding.edtPhoneNumber?.setText(userProfile?.phoneNumber)

        if (userProfile?.isUserVerified == true) {

            binding.tvVerifyMail?.visibility = View.GONE
            binding.edtMail?.setText(userProfile.email)

        } else {
            binding.edtMail?.setText(userProfile?.email)
            binding.tvVerifyMail?.visibility = View.VISIBLE

        }
        userProfile?.countryCode?.let { countryCode ->
            binding.tvCountryCodeNew.text = countryCode
            selectedData = ResponseCollection()
            selectedData.countryCode = countryCode
            selectedCountryCode = countryCode
        }
        binding.receiveRecommendationsSwitch.isChecked = userProfile?.receiveRecommendation == true
        if (userProfile?.receiveRecommendation == true) {
            binding.receiveRecommendationsSwitch.changeButtonColor(Color.WHITE)
        }
        if (userProfile?.userImage?.isNotEmpty() == true) {
            CommonUtils.loadImage(
                this@EditProfileActivity,
                userProfile.userImage,
                binding.profileImage,
                R.drawable.ic_record_user_place_holder
            )

        }
    }

}


