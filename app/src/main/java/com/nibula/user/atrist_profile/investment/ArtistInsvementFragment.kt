package com.nibula.user.atrist_profile.investment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.base.BaseFragment
import com.nibula.databinding.ArtistInsvementFragmentBinding
import com.nibula.response.myinvestmentresponse.MyInvestmentResponse
import com.nibula.user.atrist_profile.helper.ArtistInvestmentHelper
import retrofit2.Call


class ArtistInsvementFragment : BaseFragment() {

    companion object {
        fun newInstance(bundle: Bundle?): ArtistInsvementFragment {
            val fg = ArtistInsvementFragment()
            fg.arguments = bundle
            return fg
        }
    }

    var artistId: String = ""

    private lateinit var viewModel: ArtistInsvementViewModel

    var call: Call<MyInvestmentResponse>? = null

    lateinit var rootView: View
    lateinit var helper: ArtistInvestmentHelper
    lateinit var adapter: ArtistInsvementAdapter
    lateinit var binding:ArtistInsvementFragmentBinding

    var isDataAvailable = false
    var isLoading = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.artist_insvement_fragment, container, false)
            binding=ArtistInsvementFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            intiUI()
        }
        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // viewModel = ViewModelProviders.of(this).get(ArtistInsvementViewModel::class.java)
        binding.rvInvestors.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            helper.getInvestmentList(artistId, true)
        }
    }

    override fun onStop() {
        super.onStop()
        if (call != null &&
            !call!!.isExecuted
        ) {
            call!!.cancel()
        }
    }

    private fun intiUI() {
        artistId = arguments?.getString("artistId") ?: ""
        helper = ArtistInvestmentHelper(this)

        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvInvestors.layoutManager = layoutManager
        adapter = ArtistInsvementAdapter(this)

        binding.rvInvestors.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.getInvestmentList(artistId, false)
                }
            }
        })
        binding.swipeInvest.setOnRefreshListener {
            helper.investmentNextPage = 1
            helper.investmentMaxPage = 0
            helper.getInvestmentList(artistId, false)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.rvInvestors.adapter = null
    }

}
