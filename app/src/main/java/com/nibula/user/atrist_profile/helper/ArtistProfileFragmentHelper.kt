package com.nibula.user.atrist_profile.helper

import android.view.View
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ConstantsNavTabs
import com.nibula.request.ArtistFollowRequest
import com.nibula.response.follow.FollowResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.user.atrist_profile.ArtistProfileFragment
import com.nibula.user.profile.ProfileResponce
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils

class ArtistProfileFragmentHelper(val fg: ArtistProfileFragment) : BaseHelperFragment() {

    var isAlreadyFollow: Boolean = false
    var artistId: String = ""

    var isSelfUser = false

    fun getProfile(isProgressDialog: Boolean , isDataUpdate : Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper = ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getProfile(artistId)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<ProfileResponce>() {
            override fun onSuccess(any: ProfileResponce?, message: String?) {
                if (any?.userProfile != null &&
                    any.responseStatus == 1
                ) {
                    if(isDataUpdate) {
                        fg.binding.tvUserName.text = any.userProfile?.userName ?: ""
                        fg.binding.tvProfession.text = any.userProfile?.bio ?: ""
                        fg.binding.edtFirstName.text = any.userProfile?.firstName ?: ""
                        val totalRaised =
                            "${any.userProfile?.currencyType ?: "$"} ${CommonUtils.trimDecimalToTwoPlaces(any.userProfile?.totalRaised ?: 0.0)}"
                        fg.binding.totalRaisedTv.text = totalRaised
                        fg.binding.releaseCountTv.text = "${any.userProfile?.totalReleased}"
                        fg.binding.verifiedUsed.visibility =
                            if (any.userProfile?.isUserVerified == true) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }
                        isAlreadyFollow = any.userProfile?.isFollowed ?: false
                        loadImage(any.userProfile?.userImage ?: "")
                    }
                    fg.binding.totalFollowersTv.text = any.userProfile?.followers ?: "0"
                    updateFollowButton(false)

                }
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }

        }))
    }

    fun loadImage(userImage: String) {
        CommonUtils.loadImageWithOutCache(
            fg.requireContext(),
            userImage,
            fg.binding.profileImage,
            R.drawable.ic_record_user_place_holder
        )
        CommonUtils.loadImageWithOutCache(
            fg.requireContext(),
            userImage,
            fg.binding.backgroundImage
        )
    }

    fun followArtistApi(isProgressDialog: Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper = ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
        val request = ArtistFollowRequest()
        request.followingTo = artistId
        request.isFollow = if (isAlreadyFollow) 0 else 1 //- 1 FOllow - 0 Unfollow
        val observable = helper.getArtistFollow(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<FollowResponse>() {
            override fun onSuccess(any: FollowResponse?, message: String?) {
                if ((any?.responseStatus ?: 0) == 1) {
                    isAlreadyFollow = !isAlreadyFollow
                    updateFollowButton(true)

                    CommonUtils.triggerFollower("","",PrefUtils.getValueFromPreference(fg.requireContext(),PrefUtils.PHONE_NO))
                    if (any?.isSelfUser == true) {
                        isSelfUser = true
                        fg.navigate.changeTab(ConstantsNavTabs.NAV_TAB_PROFILE)
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }

        }))
    }

    fun updateFollowButton(isApiCall: Boolean) {
        if(isApiCall){
            getProfile(isProgressDialog = false, isDataUpdate = false)
        }
        fg.binding.tvFollow.text = if (isAlreadyFollow) {
            fg.binding.tvFollow.setPaddingRelative(
                CommonUtils.dpToPx(fg.requireContext(), 17),
                CommonUtils.dpToPx(fg.requireContext(), 7),
                CommonUtils.dpToPx(fg.requireContext(), 17),
                CommonUtils.dpToPx(fg.requireContext(), 7)
            )
            fg.binding.tvFollow.setTextColor(
                ContextCompat.getColor(
                    fg.requireContext(),
                    R.color.colorPrimaryDark
                )
            )
            fg.binding.tvFollow.setBackgroundResource(R.drawable.diable_7_85per)
            fg.getString(R.string.following)
        } else {
            fg.binding.tvFollow.setPaddingRelative(
                CommonUtils.dpToPx(fg.requireContext(), 29),
                CommonUtils.dpToPx(fg.requireContext(), 7),
                CommonUtils.dpToPx(fg.requireContext(), 29),
                CommonUtils.dpToPx(fg.requireContext(), 7)
            )
            fg.binding.tvFollow.setTextColor(
                ContextCompat.getColor(
                    fg.requireContext(),
                    R.color.white
                )
            )
            fg.binding.tvFollow.setBackgroundResource(R.drawable.red_solid_7dp_radious)
            fg.getString(R.string.follow)
        }
    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }
}