package com.nibula.user.atrist_profile.helper

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.myinvestmentresponse.MyInvestmentResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.user.atrist_profile.investment.ArtistInsvementFragment

class ArtistInvestmentHelper(val fg:ArtistInsvementFragment):BaseHelperFragment() {

    var investmentNextPage: Int = 1
    var investmentMaxPage: Int = 0

    fun getInvestmentList(artistId:String,isProgressDialog: Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog
        ) {
            fg.showProcessDialog()
        }

        fg.isLoading = true

        val helper = ApiClient.getClientInvestor(fg.requireContext()).create(ApiAuthHelper::class.java)
        fg.call = helper.getInvestmentListing(artistId, investmentNextPage)
        fg.call!!.enqueue(object : CallBackManager<MyInvestmentResponse>() {
            override fun onSuccess(any: MyInvestmentResponse?, message: String) {
                /*Load More*/
                investmentMaxPage = ((any?.myinvestments?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = investmentNextPage < investmentMaxPage ||
                        (any?.myinvestments?.totalRecords ?: 0) > (investmentNextPage * 10)

                /*Data*/
                if (any?.myinvestments != null &&
                    any.myinvestments?.responseStatus == 1
                ) {
                    if (!any.myinvestments?.responseCollection.isNullOrEmpty()) {
                        fg.binding.rvInvestors.visibility = View.VISIBLE
                        if (investmentNextPage == 1) {
                            fg.adapter.setData(any.myinvestments?.responseCollection!!)
                        } else if (investmentNextPage > 1) {
                            fg.adapter.addData(any.myinvestments?.responseCollection!!)
                        }
                        investmentNextPage++

                    } else {
                        if (investmentNextPage == 1) {
                            fg.adapter.clearData()
                        }
                    }
                } else {
                    if (investmentNextPage == 1) {
                        fg.adapter.clearData()
                    }
                }

                dismiss(isProgressDialog)
            }

            override fun onFailure(message: String) {
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError) {
                dismiss(isProgressDialog)
            }

        })
    }

    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
        if (fg.binding.swipeInvest.isRefreshing) {
            fg.binding.swipeInvest.isRefreshing = false
        }
        fg.isLoading = false
        noData()
    }

    private fun noData() {
        if (fg.adapter.list.isNullOrEmpty()) {
            fg.binding.gdNoInvestments.visibility = View.VISIBLE
            fg.binding.rvInvestors.visibility = View.GONE
        } else {
            fg.binding.gdNoInvestments.visibility = View.GONE
            fg.binding.rvInvestors.visibility = View.VISIBLE
        }
    }


}