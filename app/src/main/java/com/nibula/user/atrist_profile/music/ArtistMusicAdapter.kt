package com.nibula.user.atrist_profile.music

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.counter.TimerCounter
import com.nibula.databinding.ArtestMusicLayoutBinding
import com.nibula.response.musicprofile.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.LifecycleViewHolder
import com.nibula.utils.TimestampTimeZoneConverter


class ArtistMusicAdapter(val fg: BaseFragment) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), TimerCounter.TimerItemRemove {

    var list: MutableList<ResponseCollection> = mutableListOf()

    var context: Context = fg.requireContext()

    var thisMillisStart: Long = fg.millisStart
    lateinit var binding:ArtestMusicLayoutBinding
    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder is ArtistMusicOfferingViewHolder) {
            //println("ArtistMusicAdapter: onViewAttachedToWindow ${holder.adapterPosition}")
            holder.onAttached()
            holder.registerTimerLiveData()
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is ArtistMusicOfferingViewHolder) {
            //println("ArtistMusicAdapter: onViewDetachedFromWindow ${holder.adapterPosition}")
            holder.removeObserver(fg.baseViewModel.getTimeLv())
            holder.onDetached()
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        if (holder is ArtistMusicOfferingViewHolder) {
            holder.onRecycled()
        }
    }

    override fun onTimerFinish(recordId: String, position: Int) {
        //println("onTimerFinish: recordId $recordId position $position")
        if (position >= 0) {
            fg.handler.postDelayed(Runnable {
                if (position < list.size) {
                    val record = list[position]
                    if (record.id.equals(recordId)) {
                        //println("onTimerFinish: MATCH recordId $recordId position $position")
                        list.removeAt(position)
                        notifyItemRemoved(position)
                        //notifyDataSetChanged()
                    } else {
                        //println("onTimerFinish: UN-MATCH record.id ${record.id} recordId $recordId position $position")
                    }
                }
            }, 1000)
        }
    }

    inner class ArtistMusicOfferingViewHolder(itemView: View) : LifecycleViewHolder(itemView) {
        var tvStatus: AppCompatTextView? = itemView.findViewById(R.id.tvStatus)

        fun registerTimerLiveData() {
            val data = if (adapterPosition > -1 && adapterPosition < list.size) {
                list[adapterPosition]
            } else {
                null
            }
            //println("ArtistMusicAdapter: registerTimerLiveData $adapterPosition")
            registerTimerLiveData(
                fg.requireContext(),
                fg.baseViewModel.getTimeLv(),
                thisMillisStart,
                tvStatus,
                data,
                this@ArtistMusicAdapter
            )
        }
    }

    inner class ArtistMusicViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setData(data: MutableList<ResponseCollection>) {
        list = data
        //setDummyData(list)
        setRemainingTime(list)
        thisMillisStart = fg.millisStart
        notifyDataSetChanged()
    }

    private fun setRemainingTime(dataList: MutableList<ResponseCollection>) {
        for (obj in dataList) {
            if (obj.recordStatusTypeId == AppConstant.initialoffering) {
                obj.remainingMillis = obj.timeLeftToRelease ?: 0
                obj.tltr = obj.timeLeftToRelease ?: 0
                obj.rId = obj.id ?: ""
            }
        }
    }

    fun addData(data: MutableList<ResponseCollection>) {
        //setDummyData(data)
        setRemainingTime(data)
        list.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        list.clear()
    }

    override fun getItemViewType(position: Int): Int {
        return if (list.isNotEmpty() &&
            list[position].recordStatusTypeId == AppConstant.initialoffering
        ) {
            1
        } else {
            2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
     /*   val view =
            LayoutInflater.from(context).inflate(R.layout.artest_music_layout, parent, false)*/

        val inflater = LayoutInflater.from(parent.context)
        binding = ArtestMusicLayoutBinding.inflate(inflater, parent, false)


        val holder = if (viewType == 1) {
            ArtistMusicOfferingViewHolder(binding.root)
        } else {
            ArtistMusicViewHolder(binding.root)
        }
        holder.itemView.setOnClickListener {
            if (holder.adapterPosition in 0..list.size) {


                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL,
                        list[holder.adapterPosition].recordImage
                    )
                    putString(
                        AppConstant.ID,
                        list[holder.adapterPosition].id
                    )
                }
                //fg.navigate.gotorequestinvest(list[holder.adapterPosition].id)
                fg.navigate.goToNewOffering(bundle)
            }
        }
        return holder
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = list[position]


        Glide
            .with(context)
            .load(data.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.cvImage.clImage)


        when (data.recordTypeId) {
            1 -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
            2 -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.song)}: ${data.recordTitle}"
            }
            else -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
        }






        when (data.recordStatusTypeId) {
            AppConstant.released -> {
                val tfLight = ResourcesCompat.getFont(context, R.font.pt_sans)
                binding.tvDate.visibility = View.VISIBLE
                binding.tvStatus.typeface = tfLight
                binding.tvStatus.text = AppConstant.AVAILABLE
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.green_shade_1_100per
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.bg_available)
                binding.tvPriceShare.text =
                    "${data.currencyType} ${CommonUtils.trimDecimalToTwoPlaces(data.totalInvestedAmount ?: 0.0)} ${context.getString(
                        R.string.raised
                    )}"
                if (data.totalNoOfInvesters != null) {
                    val shareCount = data.totalNoOfInvesters ?: 0
                    val shareString =
                        if (shareCount == 1) context.getString(R.string.investor) else context.getString(
                            R.string.investors
                        )
                    binding.tvShares.text = CommonUtils.setSpannable(context,
                        "$shareCount $shareString",
                        0,
                        shareCount.toString().length + 1
                    )
                } else {
                    binding.tvShares.text = CommonUtils.setSpannable(context,
                        "0 ${context.getString(R.string.investors)}",
                        0,
                        1
                    )
                }
            }
            AppConstant.request_submitted -> {
                val tfLight = ResourcesCompat.getFont(context, R.font.pt_sans)
                binding.tvDate.visibility = View.GONE
                binding.tvStatus.typeface = tfLight
                binding.tvStatus.text = AppConstant.pending
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background)
                binding.tvPriceShare.text =
                    "${data.currencyType} ${CommonUtils.trimDecimalToTwoPlaces(data.valueOfAShare ?: 0.0)} ${context.getString(
                        R.string.per_share
                    )}"

                binding.tvDate.visibility = VISIBLE
                binding.tvDate.text = data.offeringEndDate?.let {
                    TimestampTimeZoneConverter.convertToLocalDate(
                        it, TimestampTimeZoneConverter.UTC_TIME, "dd/MM/yyyy"
                    )
                }?.let { releaseDateString(it) }
                if (data.sharesAvailable != null) {
                    val shareCount = data.sharesAvailable ?: 0.0
                    val shareString =
                        if (shareCount <= 1) context.getString(R.string.share_available) else context.getString(
                            R.string.shares_available
                        )
                    val temp = CommonUtils.trimDecimalToTwoPlaces(shareCount)
                    binding.tvShares.text = CommonUtils.setSpannable(context,
                        "$temp $shareString",
                        0,
                        temp.length + 1
                    )
                } else {
                    binding.tvShares.text = CommonUtils.setSpannable(context,
                        "0 ${context.getString(R.string.share_available)}",
                        0,
                        1
                    )
                }
            }
            AppConstant.initialoffering -> {
                val tfBold = ResourcesCompat.getFont(context, R.font.pt_sans_bold)
                binding.tvStatus.typeface = tfBold
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.red_back_2_dp)
                /*Counter*/
                val temp = TimestampTimeZoneConverter.convertToMilliseconds(
                    data.timeLeftToRelease ?: 0
                )
                if (temp > TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                    val time =
                        TimestampTimeZoneConverter.getDaysAndHourDifference(
                            context,
                            temp
                        )
                    binding.tvStatus.visibility = View.VISIBLE
                    binding.tvStatus.text = time
                } else {
                    binding.tvStatus.visibility = View.INVISIBLE
                    if (temp > 0 &&
                        !data.isFinish
                    ) {
                        val workingTime =
                            TimestampTimeZoneConverter.getDaysAndHourDifference(
                                context,
                                data.remainingMillis
                            )
                        binding.tvStatus.text = workingTime
                        binding.tvStatus.visibility = View.VISIBLE
                    }
                }
                binding.tvPriceShare.text =
                    "$${CommonUtils.trimDecimalToTwoPlaces(data.valueOfAShare ?: 0.0)} ${context.getString(
                        R.string.per_share
                    )}"

                binding.tvDate.visibility = VISIBLE
                binding.tvDate.text = data.offeringEndDate?.let {
                    TimestampTimeZoneConverter.convertToLocalDate(
                        it, TimestampTimeZoneConverter.UTC_TIME, "dd/MM/yyyy"
                    )
                }?.let { releaseDateString(it) }
                if (data.sharesAvailable != null) {
                    val shareCount = data.sharesAvailable ?: 0.0
                    val shareString =
                        if (shareCount <= 1) context.getString(R.string.share_available) else context.getString(
                            R.string.shares_available
                        )
                    val temp = CommonUtils.trimDecimalToTwoPlaces(shareCount)
                    binding.tvShares.text = CommonUtils.setSpannable(context,
                        "$temp $shareString",
                        0,
                        temp.length + 1
                    )
                } else {
                    binding.tvShares.text = CommonUtils.setSpannable(context,
                        "0 ${context.getString(R.string.share_available)}",
                        0,
                        1
                    )
                }
            }
            AppConstant.approved_by_admin -> {
                val tfLight = ResourcesCompat.getFont(context, R.font.pt_sans)
                binding.tvDate.visibility = View.GONE

                binding.tvStatus.visibility = GONE
                binding.tvStatus.typeface = tfLight
                binding.tvStatus.text = AppConstant.pending
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background)
                binding.tvPriceShare.visibility= GONE
                binding.tvDate.visibility = GONE
                binding.tvShares.visibility = VISIBLE
                binding.tvShares.text = context.getString(R.string.recently_uploaded)
                

//                holder.itemView.tvDate.text = data.offeringEndDate?.let {
//                    TimestampTimeZoneConverter.convertToLocalDate(
//                        it, TimestampTimeZoneConverter.UTC_TIME, "dd/MM/yyyy"
//                    )
//                }?.let { releaseDateString(it) }
            }

            AppConstant.sold_out -> {

                val tfLight = ResourcesCompat.getFont(context, R.font.pt_sans)
                binding.tvDate.visibility = View.GONE

                binding.tvStatus.visibility = VISIBLE
                binding.tvStatus.text = "Sold Out"
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.gray
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.bg_sold_out)
                binding.tvPriceShare.visibility= GONE
                binding.tvDate.visibility = GONE
                binding.tvShares.visibility = VISIBLE
                binding.tvShares.text = context.getString(R.string.recently_uploaded)



//                holder.itemView.tvStatus.setBackgroundResource(R.drawable.bg_sold_out)
//                holder.itemView.tvStatus.setTextColor(Color.parseColor("#969696"))
//                holder.itemView.tvStatus.setText("Sold Out")


//                holder.itemView.tvDate.text = data.offeringEndDate?.let {
//                    TimestampTimeZoneConverter.convertToLocalDate(
//                        it, TimestampTimeZoneConverter.UTC_TIME, "dd/MM/yyyy"
//                    )
//                }?.let { releaseDateString(it) }
            }
        }
    }

    fun shareAvailable(investerVal: String): SpannableString {
        val ss1 = SpannableString("$investerVal ${context.getString(R.string.shares_available)}")
        ss1.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
            0,
            investerVal.length,
            0
        )
        return ss1
    }

    private fun releaseDateString(date: String): SpannableString {
        val ss1 = SpannableString("${context.getString(R.string.release_date)} $date")
        ss1.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
            context.getString(R.string.release_date).length + 1,
            ss1.length,
            0
        )
        return ss1
    }

    fun dateString(date: String): SpannableString {
        val ss1 = SpannableString("${context.getString(R.string.release_date)} $date")
        ss1.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
            ss1.length - date.length,
            ss1.length,
            0
        )
        return ss1
    }


}