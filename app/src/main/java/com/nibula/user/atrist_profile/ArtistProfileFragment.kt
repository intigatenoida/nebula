package com.nibula.user.atrist_profile

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.tabs.TabLayout
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.ProfileFragmentBinding
import com.nibula.user.atrist_profile.helper.ArtistProfileFragmentHelper
import com.nibula.user.login.LoginActivity
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.RequestCode


class ArtistProfileFragment : BaseFragment() {

    companion object {
        fun newInstance(bundle: Bundle?): ArtistProfileFragment {
            val fg = ArtistProfileFragment()
            fg.arguments = bundle
            return fg
        }
    }

    private lateinit var viewModel: ArtistProfileViewModel

    lateinit var rootView: View
    lateinit var helper: ArtistProfileFragmentHelper
    lateinit var adapter: ArtistProfilePagerAdapter
    var userTypeId: Int = 4
    lateinit var binding:ProfileFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.profile_fragment, container, false)
            binding= ProfileFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
        }
        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(this).get(ArtistProfileViewModel::class.java)
        if (helper.isSelfUser) {
            navigate.manualBack()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        helper.onDestroy()
    }

    private fun initUI() {

        binding.clProfile.visibility = View.GONE
        binding.clEdit.visibility = View.GONE
        binding.baseHeader.root.visibility = View.GONE
        helper = ArtistProfileFragmentHelper(this)
        binding.tvThreeDots.visibility = GONE
        binding.ivBack.visibility = VISIBLE
        binding.ivBack.setOnClickListener {
            navigate.manualBack()
        }

        helper.artistId = arguments?.getString(AppConstant.ID) ?: ""
        userTypeId = arguments?.getInt(AppConstant.STATUS, AppConstant.NEBULA_ARTIST)
            ?: AppConstant.NEBULA_ARTIST
        if (userTypeId == AppConstant.NEBULA_USER) {
            binding.verifiedUsed.visibility = GONE
            binding.userProfileGroup.visibility = View.VISIBLE
            binding.tvFollow.visibility = VISIBLE
            adapter = ArtistProfilePagerAdapter(
                requireActivity().supportFragmentManager,
                navigate,
                helper.artistId, false
            )
            binding.viewPager.adapter = adapter
            binding.viewPager.offscreenPageLimit = 2
            binding.tabLayout.setupWithViewPager(binding.viewPager)
            prepareTab(false)
        } else {
            binding.userProfileGroup.visibility = View.VISIBLE
            binding.verifiedUsed.visibility = GONE
            binding.tvFollow.visibility = VISIBLE
            adapter = ArtistProfilePagerAdapter(
                requireActivity().supportFragmentManager,
                navigate,
                helper.artistId, false
            )
            binding.viewPager.adapter = adapter
            binding.viewPager.offscreenPageLimit = 2
            binding.tabLayout.setupWithViewPager(binding.viewPager)
            prepareTab(false)


        }

        binding.tvFollow.setOnClickListener {
            if (CommonUtils.isLogin(requireContext())) {
                helper.followArtistApi(true)
            } else {
                val intent = Intent(requireActivity(), LoginActivity::class.java)
                intent.putExtra(AppConstant.ISFIRSTTIME, false)
                startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
            }
        }
        binding.viewFollowers.setOnClickListener {
            val bundle = Bundle().apply {
                putString(AppConstant.ID, helper.artistId ?: "")

            }.apply {
                putString(
                    AppConstant.RECORD_ARTIST_NAME,
                    binding.edtFirstName.text.toString().trim()
                )
            }
            navigate.showFollower(bundle)
        }
        if (helper.artistId.isNullOrEmpty()) {
            navigate.manualBack()
        } else {
            helper.getProfile(isProgressDialog = false, isDataUpdate = true)
        }
    }

    private fun prepareTab(ishide: Boolean) {
        if (!ishide) {
            val musicTab = binding.tabLayout.getTabAt(1)
            addTabHere(
                musicTab,
                R.layout.layout_tab_my_music_left,
                getString(R.string.releases),
                R.font.pt_sans, Typeface.NORMAL, View.INVISIBLE
            )
        }
        val investment = binding.tabLayout.getTabAt(0)
        addTabHere(
            investment,
            R.layout.layout_tab_my_investments_right,
            "Tokens",
            R.font.pt_sans_bold, Typeface.NORMAL,
            View.VISIBLE
        )

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                changeTabAppearance2(tab, View.INVISIBLE)
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                changeTabAppearance2(tab, View.VISIBLE)
            }

        })
        wrapTabIndicatorToTitle(binding.tabLayout, 0, 0)

    }

    private fun addTabHere(
        tab: TabLayout.Tab?,
        layoutId: Int,
        title: String,
        font: Int,
        textStyle: Int,
        indicatorVisibility: Int
    ) {
        if (tab == null) {
            return
        }
        val customTabView: View =
            LayoutInflater.from(requireContext()).inflate(layoutId, null)

        customTabView.findViewById<View>(R.id.v_bottom_border).visibility = indicatorVisibility
        val tabTitle = customTabView.findViewById<TextView>(R.id.tv_tab_title)
        tabTitle.text = title

        val tf = ResourcesCompat.getFont(requireContext(), font)
        tabTitle.setTypeface(tf, textStyle)

        tab.customView = customTabView
        tab.tag = title

        if (indicatorVisibility == View.VISIBLE) {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        } else {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCode.LOGIN_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK
        ) {
            baseViewModel.refresh(true)
            helper.followArtistApi(true)
        }
    }
}
