package com.nibula.user.atrist_profile.investment

import android.content.Context
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.counter.TimerCounter
import com.nibula.databinding.ArtistInvestmentLayoutBinding

import com.nibula.response.myinvestmentresponse.ResponseCollection

import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.LifecycleViewHolder
import com.nibula.utils.TimestampTimeZoneConverter


class ArtistInsvementAdapter(val fg: BaseFragment) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), TimerCounter.TimerItemRemove {

    var list = mutableListOf<ResponseCollection>()
    var context: Context = fg.requireContext()
    var thisMillisStart: Long = fg.millisStart
    lateinit var binding: ArtistInvestmentLayoutBinding


    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder is ArtistInvestmentOfferingViewHolder) {
            //println("InvestmentAdapter: onViewAttachedToWindow ${holder.adapterPosition}")
            holder.onAttached()
            holder.registerTimerLiveData()
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is ArtistInvestmentOfferingViewHolder) {
            //println("InvestmentAdapter: onViewDetachedFromWindow ${holder.adapterPosition}")
            holder.removeObserver(fg.baseViewModel.getTimeLv())
            holder.onDetached()
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        if (holder is ArtistInvestmentOfferingViewHolder) {
            holder.onRecycled()
        }
    }

    override fun onTimerFinish(recordId: String, position: Int) {
        //println("onTimerFinish: recordId $recordId position $position")
        if (position >= 0) {
            fg.handler.postDelayed(Runnable {
                if (position < list.size) {
                    val record = list[position]
                    if (record.id.equals(recordId)) {
                        //println("onTimerFinish: MATCH recordId $recordId position $position")
                        list.removeAt(position)
                        notifyItemRemoved(position)
                        //notifyDataSetChanged()
                    } else {
                        //println("onTimerFinish: UN-MATCH record.id ${record.id} recordId $recordId position $position")
                    }
                }
            }, 1000)
        }
    }

    inner class ArtistInvestmentOfferingViewHolder(itemView: View) :
        LifecycleViewHolder(itemView) {
        var tvStatus: AppCompatTextView? = itemView.findViewById(R.id.tvStatus)

        fun registerTimerLiveData() {
            val data = if (adapterPosition > -1 && adapterPosition < list.size) {
                list[adapterPosition]
            } else {
                null
            }
            //println("ArtistInsvementAdapter: registerTimerLiveData $adapterPosition")
            registerTimerLiveData(
                fg.requireContext(),
                fg.baseViewModel.getTimeLv(),
                thisMillisStart,
                tvStatus,
                data,
                this@ArtistInsvementAdapter
            )
        }
    }

    inner class ArtistInvestmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setData(data: MutableList<ResponseCollection>) {
        list = data
//        setDummyData(list)
        setRemainingTime(list)
        thisMillisStart = fg.millisStart
        notifyDataSetChanged()
    }

    private fun setRemainingTime(dataList: MutableList<ResponseCollection>) {
        for (obj in dataList) {
            if (obj.recordStatusTypeId == AppConstant.initialoffering) {
                obj.remainingMillis = obj.timeLeftToRelease ?: 0
                obj.tltr = obj.timeLeftToRelease ?: 0
                obj.rId = obj.id ?: ""
            }
        }
    }

    fun addData(data: MutableList<ResponseCollection>) {
//        setDummyData(data)
        setRemainingTime(data)
        list.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        list.clear()
    }

    override fun getItemViewType(position: Int): Int {
        return if (list.isNotEmpty() &&
            (list[position].recordStatusTypeId == AppConstant.initialoffering)
        ) {
            1
        } else {
            2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       /* val view =
            LayoutInflater.from(context).inflate(R.layout.artist_investment_layout, parent, false)*/


        val inflater = LayoutInflater.from(parent.context)
        binding = ArtistInvestmentLayoutBinding.inflate(inflater, parent, false)
        val view = binding.root

        val holder = if (viewType == 1) {
            ArtistInvestmentOfferingViewHolder(view)
        } else {
            ArtistInvestmentViewHolder(view)
        }

        holder.itemView.setOnClickListener {
            if (holder.adapterPosition in 0..list.size) {

                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL,
                        list[holder.adapterPosition].recordImage
                    )
                    putString(
                        AppConstant.ID,
                        list[holder.adapterPosition].id
                    )
                }
                //fg.navigate.gotorequestinvest(list[holder.adapterPosition].id)
                fg.navigate.goToNewOffering(bundle)
            }
        }

        return holder
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = list[position]


        Glide
            .with(context)
            .load(data.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.layoutImage.clImage)

        when (data.recordTypeId) {
            1 -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
            2 -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.single)}: ${data.recordTitle}"
            }
            else -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
        }

        binding.artistName.text =
            "${context.getString(R.string.artist)}: ${data.artistsName}"

        val inv = data.totalNoOfInvesters ?: 0
        val temp = "$inv "
        val spn1 = SpannableStringBuilder()
        spn1.append(
            CommonUtils.setSpannable(
                context,
                "$inv ",
                0,
                temp.length
            )
        )
        if (inv <= 1) {
            spn1.append("${context.getString(R.string.investor_)}")
        } else {
            spn1.append("${context.getString(R.string.investors)}")
        }
        binding.tvInvestors.text = spn1

        val raised = CommonUtils.trimDecimalToTwoPlaces(data.totalInvestedAmount ?: 0.0)
        binding.tvRaised.text =
            "${data.currencyType} $raised ${context.getString(R.string.raised)}"

        when (data.recordStatusTypeId) {
            AppConstant.released -> {
                val tfLight = ResourcesCompat.getFont(context, R.font.pt_sans)
                binding.tvStatus.typeface = tfLight
                binding.tvStatus.text = AppConstant.Released
                binding.tvBuyPrice.visibility = View.GONE
                binding.tvBuyPrice.text =
                    "${context.getString(R.string.buy)} ${data.currencyType} ${
                        CommonUtils.trimDecimalToTwoPlaces(
                            data.priceAfterRelease
                        )
                    }"
                binding.tvBuyPrice.setOnClickListener {

                    val bundle = Bundle().apply {
                        putString(
                            AppConstant.RECORD_IMAGE_URL,
                            data.recordImage
                        )
                        putString(
                            AppConstant.ID,
                            data.id
                        )
                    }
                    fg.navigate.goToNewOffering(bundle)
                }
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.red_shade_1_100per
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.back_background)
                binding.tvStatus.setPaddingRelative(dpToPx(0), 0, dpToPx(0), 0)
            }
            AppConstant.request_submitted -> {
                binding.tvBuyPrice.visibility = View.GONE
                val tfLight = ResourcesCompat.getFont(context, R.font.pt_sans)
                binding.tvStatus.typeface = tfLight
                binding.tvStatus.text = AppConstant.pending
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background)
                binding.tvStatus.setPaddingRelative(dpToPx(17), 0, dpToPx(17), 0)
            }

            AppConstant.initialoffering -> {
                binding.tvBuyPrice.visibility = View.GONE
                val tfBold = ResourcesCompat.getFont(context, R.font.pt_sans_bold)
                binding.tvStatus.typeface = tfBold
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.red_back_2_dp)
                /*Counter*/
                val temp = TimestampTimeZoneConverter.convertToMilliseconds(
                    data.timeLeftToRelease ?: 0
                )
                if (temp > TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                    val time =
                        TimestampTimeZoneConverter.getDaysAndHourDifference(
                            context,
                            temp
                        )
                    binding.tvStatus.visibility = View.VISIBLE
                    binding.tvStatus.text = time
                } else {
                    binding.tvStatus.visibility = View.INVISIBLE
                    if (temp > 0 &&
                        !data.isFinish
                    ) {
                        val workingTime =
                            TimestampTimeZoneConverter.getDaysAndHourDifference(
                                context,
                                data.remainingMillis
                            )
                        binding.tvStatus.text = workingTime
                        binding.tvStatus.visibility = View.VISIBLE
                    }
                }
                binding.tvStatus.setPaddingRelative(dpToPx(7), 0, dpToPx(7), 0)
            }

            else -> {
                binding.tvBuyPrice.visibility = View.GONE
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.text = AppConstant.pending
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background)
                binding.tvStatus.setPaddingRelative(dpToPx(17), 0, dpToPx(17), 0)
            }
        }

        binding.tvInvestmentStatus.visibility = View.GONE

        when (data.investmentStatusId) {

            AppConstant.PENDING -> {
//                holder.itemView.tv_investment_status.visibility = View.GONE
//                holder.itemView.tvStatus.text = AppConstant.pending
//                holder.itemView.tvStatus.setTextColor(
//                    ContextCompat.getColor(
//                        context,
//                        R.color.white
//                    )
//                )
//                holder.itemView.tvStatus.background =
//                    ContextCompat.getDrawable(context, R.drawable.panding_background)
            }

            AppConstant.APPROVED -> {
//                holder.itemView.tv_investment_status.visibility = View.VISIBLE
//                holder.itemView.tv_investment_status.text = "${data.investmentStatus}"
//                holder.itemView.tv_investment_status.setTextColor(
//                    ContextCompat.getColor(
//                        context,
//                        R.color.green_shade_1_100per
//                    )
//                )
            }

            AppConstant.REJECTED -> {
//                holder.itemView.tv_investment_status.visibility = View.VISIBLE
//                holder.itemView.tv_investment_status.text = "${data.investmentStatus}"
//                holder.itemView.tv_investment_status.setTextColor(
//                    ContextCompat.getColor(
//                        context,
//                        R.color.red_shade_1_100per
//                    )
//                )
            }

            AppConstant.INVESTED, AppConstant.ALL_READY_PURCHASED, AppConstant.SELF_OWNED -> {
                binding.tvBuyPrice.visibility = View.GONE
//                holder.itemView.tv_investment_status.visibility = View.GONE
                //holder.itemView.tv_investment_status.setTextColor(ContextCompat.getColor(context,R.color.green_shade_1_100per))
            }

            else -> {
//                holder.itemView.tv_investment_status.visibility = View.GONE
            }
        }

    }

    fun investersString(investerVal: String): SpannableString {
        val ss1 = SpannableString("$investerVal ${context.getString(R.string.investors)}")
        ss1.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
            0,
            ss1.length - 10,
            0
        )
        return ss1
    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    fun setDummyData(dataList: MutableList<ResponseCollection>) {
        for (obj in dataList) {
            if (obj.recordStatusTypeId == AppConstant.initialoffering) {
                obj.timeLeftToRelease = 60 * 2
            }
        }
    }

}