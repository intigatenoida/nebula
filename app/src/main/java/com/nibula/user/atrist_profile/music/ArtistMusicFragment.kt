package com.nibula.user.atrist_profile.music

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.ArtistMusicFragmentBinding
import com.nibula.response.musicprofile.ProfileMusicReponse
import com.nibula.user.atrist_profile.helper.ArtistProfileMusicHelper
import retrofit2.Call


class ArtistMusicFragment : BaseFragment() {

    companion object {
        fun newInstance(bundle: Bundle?): ArtistMusicFragment {
            val fg = ArtistMusicFragment()
            fg.arguments = bundle
            return fg
        }
    }

    var artistId: String = ""

    private lateinit var viewModel: ArtistMusicViewModel
    lateinit var binding:ArtistMusicFragmentBinding

    lateinit var rootView: View
    lateinit var adapter: ArtistMusicAdapter

    private lateinit var helper: ArtistProfileMusicHelper

    var call: Call<ProfileMusicReponse>? = null

    var isDataAvailable = false
    var isLoading = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
           // rootView = inflater.inflate(R.layout.artist_music_fragment, container, false)
            binding= ArtistMusicFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            helper.getMusicListing(artistId,true)
        }
    }

    override fun onStop() {
        super.onStop()
        if (call != null &&
            !call!!.isExecuted
        ) {
            call!!.cancel()
        }
    }

    private fun initUI() {
        artistId = arguments?.getString("artistId") ?: ""
        helper = ArtistProfileMusicHelper(this)
        adapter = ArtistMusicAdapter(this)
        val layoutManager = GridLayoutManager(context, 2)
        binding.musicRv.layoutManager = layoutManager
        binding.musicRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.getMusicListing(artistId,false)
                }
            }
        })

        binding.swipeMusic.setOnRefreshListener {
            helper.musicNextPage = 1
            helper.musicMaxPage = 0
            helper.getMusicListing(artistId,false)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(this).get(MusicViewModel::class.java)
        binding.musicRv.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.musicRv.adapter = null
    }

}
