package com.nibula.user.atrist_profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nibula.request_to_buy.navigate
import com.nibula.user.atrist_profile.investment.ArtistInsvementFragment
import com.nibula.user.atrist_profile.music.ArtistMusicFragment
import com.nibula.utils.SmartFragmentStatePagerAdapter

class ArtistProfilePagerAdapter(fn: FragmentManager, val navigate: navigate, val artistId: String = "", val isHide : Boolean) :
    SmartFragmentStatePagerAdapter(fn) {

    override fun getItem(position: Int): Fragment {
        val bundle = Bundle()
        bundle.putString("artistId",artistId)
        return when (position) {
            1 -> {
                ArtistMusicFragment.newInstance(bundle)
            }
            else -> {
                ArtistInsvementFragment.newInstance(bundle)
            }
        }
    }

    override fun getCount(): Int {
        return if(isHide) 1 else 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}