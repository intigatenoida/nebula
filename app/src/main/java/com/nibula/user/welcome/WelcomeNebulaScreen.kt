package com.nibula.user.welcome

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.dashboard.DashboardActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class WelcomeNebulaScreen : BaseActivity() {
    val activityScope = CoroutineScope(Dispatchers.Main)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_up_done_welcome_layout)
        activityScope.launch {
            if (isOnline()) {
               delay(2000)
               /* val intent =
                    Intent(this@WelcomeNebulaScreen, WalletConnectActivity::class.java)
                startActivity(intent)
                finish()*/
               /*startActivity(
                    Intent(
                        this@WelcomeNebulaScreen,
                        DashboardActivity::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
                finish()*/
            } else {
                showAlert(
                    "Internet connection",
                    "No internet connection detected on the device. Please check the connection and try again."
                )
            }
        }
    }

    private fun showAlert(titles: String, descriptions: String) {
        val builder = AlertDialog.Builder(this@WelcomeNebulaScreen)
        builder.setTitle(titles)
        builder.setMessage(descriptions)
        builder.setCancelable(false)
        builder.setPositiveButton(
            "Retry"
        ) { dialog, _ ->
            if (!isFinishing) {

                startActivity(
                    Intent(
                        this@WelcomeNebulaScreen,
                        DashboardActivity::class.java
                    )
                )
               // startNavigation()
                dialog.dismiss()
            }
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, _ ->
            dialog.cancel()
            finish()
        }
        val alert = builder.create()
        try {
            alert.show()
        } catch (e: Exception) {
        }

    }

}