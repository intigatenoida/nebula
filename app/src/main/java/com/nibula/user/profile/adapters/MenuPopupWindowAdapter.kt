package com.nibula.user.profile.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.widget.AppCompatTextView
import com.nibula.R


class MenuPopupWindowAdapter() : BaseAdapter() {

    private var context: Context? = null
    private var layoutInflater: LayoutInflater? = null
    private var data: ArrayList<String>? = null
    private var layoutResource: Int = 0

    constructor(context: Context, layout: Int, data: ArrayList<String>) : this() {
        this.context = context
        this.data = data
        this.layoutInflater = LayoutInflater.from(context)
        this.layoutResource = layout
    }

    class CustomMenuItemViewHolder(view: View) {
        var tvTitle: AppCompatTextView = view.findViewById(R.id.tv_title)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val vHolder: CustomMenuItemViewHolder?
        val view: View? =
            convertView ?: layoutInflater!!.inflate(layoutResource, parent, false)

        vHolder = if (view!!.tag == null) {
            CustomMenuItemViewHolder(view)
        } else {
            view.tag as CustomMenuItemViewHolder
        }

        vHolder.tvTitle.text = data!![position]

        return view
    }

    override fun getItem(position: Int): Any {
        return data!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data!!.size
    }


}