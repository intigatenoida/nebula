package com.nibula.user.profile.music

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.ProfileMusicFragmentBinding
import com.nibula.response.musicprofile.ProfileMusicReponse
import com.nibula.upload_music.ui.uploadrecord.helper.RecordDataHelper
import com.nibula.user.profile.helper.ProfileMusicFragmentHelper

import retrofit2.Call

class ProfileMusicFragment : BaseFragment() {

    companion object {
        fun newInstance() = ProfileMusicFragment()
    }

    lateinit var rootView: View
    lateinit var profileMusicAdapter: ProfileMusicAdapter

    private lateinit var helper: ProfileMusicFragmentHelper
    lateinit var binding:ProfileMusicFragmentBinding

    var call: Call<ProfileMusicReponse>? = null

    var isDataAvailable = false
    var isLoading = true

    lateinit var recordDataHelper: RecordDataHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
           // rootView = inflater.inflate(R.layout.profile_music_fragment, container, false)
            binding= ProfileMusicFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
        }
        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.musicRv.adapter = profileMusicAdapter
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        helper.musicNextPage = 1
        helper.getMusicListing("", true)
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            helper.getMusicListing("", true)
        }
    }

    override fun onStop() {
        super.onStop()
        if (call != null &&
            !call!!.isExecuted
        ) {
            call!!.cancel()
        }
    }

    private fun initUI() {
        helper = ProfileMusicFragmentHelper(this)
        recordDataHelper = RecordDataHelper(this)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.musicRv.layoutManager = layoutManager
        profileMusicAdapter = ProfileMusicAdapter(this)
        binding.musicRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.getMusicListing("", false)
                }
            }
        })

        binding.layoutNoMusic.tvTitle.text = getString(R.string.upload_initial_offering)
        binding.layoutNoMusic.subheading.text = getString(R.string.sub_heading_inital_offering)


        binding.swipeMusic.setOnRefreshListener {
            helper.musicNextPage = 1
            helper.musicMaxPage = 0
            helper.getMusicListing("", false)
        }

        binding.layoutNoMusic.iv.setOnClickListener {
            /*startActivity(
                Intent(
                    requireContext(),
                    UploadRecordActivity::class.java
                )
            )*/
            navigate.uploadRecord()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.musicRv.adapter = null
    }


    override fun getRecordDataDetail(recordId: String) {
        recordDataHelper.getRecordIdDetail(recordId)
    }


}
