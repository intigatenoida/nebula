package com.nibula.user.profile


import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class UserProfile(
    @SerializedName("Bio")
    var bio: String?,
    @SerializedName("Email")
    var email: String?,
    @SerializedName("FirstName")
    var firstName: String?,
    @SerializedName("FullName")
    var fullName: String?,
    @SerializedName("Id")
    var id: String,
    @SerializedName("IsArtist")
    var isArtist: Boolean,
    @SerializedName("LastName")
    var lastName: String?,
    @SerializedName("PhoneNumber")
    var phoneNumber: String,
    @SerializedName("ReceiveRecommendation")
    var receiveRecommendation: Boolean,
    @SerializedName("UserImage")
    var userImage: String,
    @SerializedName("UserName")
    var userName: String?,
    @SerializedName("TotalRaised")
    var totalRaised: Double,
    @SerializedName("TotalReleased")
    var totalReleased: Int,
    @SerializedName("Followers")
    var followers: String,
    @SerializedName("IsVerified")
    var isUserVerified: Boolean,
    @SerializedName("IsFollowed")
    var isFollowed: Boolean,
    @SerializedName("Name")
    var name: String?,
    @SerializedName("CountryCode")
    var countryCode: String?,
    @SerializedName("CurrencyType")
    var currencyType: String?,


    @SerializedName("CryptoWalletAddress")
    var cryptoWalletAddress: String?
) : Parcelable