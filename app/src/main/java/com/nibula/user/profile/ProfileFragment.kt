package com.nibula.user.profile

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ListAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.ListPopupWindow
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.tabs.TabLayout
import com.nibula.DBHandler.NebulaDBHelper
import com.nibula.R
import com.nibula.base.BaseApplication
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ConstantsNavTabs
import com.nibula.databinding.ProfileFragmentBinding
import com.nibula.permissions.PermissionHelper
import com.nibula.uploadimage.UploadImageHelper
import com.nibula.user.edit_profile.EditProfileActivity
import com.nibula.user.login.LoginActivity
import com.nibula.user.profile.adapters.MenuPopupWindowAdapter
import com.nibula.user.profile.helper.ProfileFragmentHelper
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import com.nibula.utils.RequestCode
import com.theartofdev.edmodo.cropper.CropImage

class ProfileFragment : BaseFragment() {
    var imageChooseDialogOpen = false
    private var originalFilePath: String? = null
    lateinit var binding:ProfileFragmentBinding

    companion object {
        fun newInstance() = ProfileFragment()
    }

    private lateinit var viewModel: ProfileViewModel
    lateinit var rootView: View
    lateinit var helper: ProfileFragmentHelper
    lateinit var adapter: ProfilePagerAdapter
    var walletAddress: String = ""
    var listPopUp: ListPopupWindow? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
           // rootView = inflater.inflate(R.layout.profile_fragment, container, false)
            binding= ProfileFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
            helper.getProfile(true)
            binding.tvWalletAddress.setOnClickListener {
                copyClipBoard(
                    walletAddress
                )
            }
            binding.tvAddWalletAddress.setOnClickListener {
/*
                startActivity(Intent(requireActivity(), WalletConnectActivity::class.java))
*/
            }
            triggerProfileViewEvent()
            binding.baseHeader.ivNotification.setOnClickListener {
                loginNotifyCheck()
            }
            binding.baseHeader.imgSearch.setOnClickListener {
                navigate.onClickSearch()
            }
            binding.baseHeader.ivUpload.setOnClickListener {
                loginUploadCheck()
            }
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
    }
    override fun onDestroy() {
        super.onDestroy()
        helper.onDestroy()
    }
    private fun initUI() {
        walletAddress
        PrefUtils.getValueFromPreference(requireActivity(), PrefUtils.CONNECTED_WALLET_ADDRESS)
        if (walletAddress.isEmpty()) {
            binding.cl.visibility = View.GONE
        }
        if (walletAddress.isNotEmpty()) {

            walletAddress?.let {
                val subString = walletAddress.substring(0, 15)
                binding.tvWalletAddress.text = "$subString..."
            }
        }

        helper = ProfileFragmentHelper(this)
        adapter = ProfilePagerAdapter(requireActivity().supportFragmentManager, navigate = navigate)
        binding.viewPager.adapter = adapter
        binding.viewPager.offscreenPageLimit = 2
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        prepareTab()
//      rootView.viewPager.currentItem = 2
        binding.profileImage.setOnClickListener {
            selectImage()
        }
        binding.editProfileTV.text =
            CommonUtils.underlineSpannable(
                getString(R.string.edit_profile),
                ContextCompat.getColor(requireContext(), R.color.colorAccent),
                true
            )
        binding.tvEditProfile.setOnClickListener {
            val intent = Intent(requireContext(), EditProfileActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable("UserData", helper.userProfile)
            intent.putExtras(bundle)
            startActivityForResult(intent, 1000)
        }

        binding.tvDashboard.setOnClickListener {
            navigate.openDashboard()
        }

        binding.tvWallet.setOnClickListener {
            navigate.openWalletScreen()
        }


        binding.tvPlaylist.setOnClickListener {
            navigate.openMusicScreen()
        }

        binding.tvUploadSong.setOnClickListener {
            navigate.openUploadSong()
        }
        binding.viewFollowers.setOnClickListener {
            val bundle = Bundle().apply {
                putString(AppConstant.ID, helper.userProfile?.id ?: "")
                putString(AppConstant.RECORD_ARTIST_NAME, helper.userProfile?.firstName ?: "")
            }
            navigate.showFollower(bundle)
        }
        setPopupMenu()
    }

    private fun selectImage() {
        if (PermissionHelper.isPermissionsAllowed(
                requireActivity(),
                PermissionHelper.permissions,
                true,
                AppConstant.PERMISSION_WRITE_EXTERNAL_STORAGE
            )
        ) {
            chooseImage()
        }
    }


    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }


    private fun chooseImage() {
        if (imageChooseDialogOpen) return

        imageChooseDialogOpen = true
        if (!isOnline()) {
            showToast(
                requireContext(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        startActivityForResult(
            UploadImageHelper.getPickImageIntent(
                requireContext(),
                helper.setImageUri()
            ),
            AppConstant.REQUEST_UPLOAD_IMAGE
        )
    }

    private fun setPopupMenu() {
        listPopUp = ListPopupWindow(requireContext())
        val data = ArrayList<String>()
        data.add(requireContext().getString(R.string.log_out))
        val listPopupWindowAdapter =
            MenuPopupWindowAdapter(requireContext(), R.layout.layout_list_menu_item, data)
        listPopUp!!.anchorView = binding.tvThreeDots
        listPopUp!!.isModal = true
        listPopUp!!.setAdapter(listPopupWindowAdapter)
        listPopUp!!.setContentWidth(measureContentWidth(listPopupWindowAdapter))
        listPopUp!!.setDropDownGravity(Gravity.END)
        listPopUp!!.horizontalOffset =
            -requireContext().resources.getDimension(R.dimen.dimen_10dp).toInt()
        listPopUp!!.setOnItemClickListener { parent, view, position, id ->
            when (data[position]) {
                requireContext().getString(R.string.log_out) -> {
                    listPopUp!!.dismiss()
                    //edit
                    //logOut()
                    helper.expireToken(
                        PrefUtils.getValueFromPreference(
                            requireActivity(),
                            PrefUtils.TOKEN
                        )
                    )
                }

            }
        }
        binding.tvThreeDots.setOnClickListener {
            listPopUp!!.show()
        }

    }

    private fun measureContentWidth(listAdapter: ListAdapter): Int {
        var mMeasureParent: ViewGroup? = null
        var maxWidth = 0
        var itemView: View? = null
        var itemType = 0
        val adapter: ListAdapter = listAdapter
        val widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        val heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        val count: Int = adapter.count
        for (i in 0 until count) {
            val positionType: Int = adapter.getItemViewType(i)
            if (positionType != itemType) {
                itemType = positionType
                itemView = null
            }
            if (mMeasureParent == null) {
                mMeasureParent = FrameLayout(requireContext())
            }
            itemView = adapter.getView(i, itemView, mMeasureParent)
            itemView.measure(widthMeasureSpec, heightMeasureSpec)
            val itemWidth = itemView.measuredWidth
            if (itemWidth > maxWidth) {
                maxWidth = itemWidth
            }
        }
        return maxWidth
    }

    private fun prepareTab() {
        val musicTab = binding.tabLayout.getTabAt(1)
        addTabHere(
            musicTab,
            R.layout.layout_tab_my_music_left,
            getString(R.string.my_releases),
            R.font.pt_sans, Typeface.NORMAL, View.INVISIBLE
        )
        val investment = binding.tabLayout.getTabAt(0)
        addTabHere(
            investment,
            R.layout.layout_tab_my_investments_right,
            getString(R.string.my_investments_new),
            R.font.pt_sans_bold, Typeface.NORMAL,
            View.VISIBLE
        )

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                changeTabAppearanceForProfile(tab, View.INVISIBLE)
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                changeTabAppearanceForProfile(tab, View.VISIBLE)
            }

        })
        wrapTabIndicatorToTitle(binding.tabLayout, 0, 0)
    }

    private fun addTabHere(
        tab: TabLayout.Tab?,
        layoutId: Int,
        title: String,
        font: Int,
        textStyle: Int,
        indicatorVisibility: Int
    ) {
        if (tab == null) {
            return
        }
        val customTabView: View = LayoutInflater.from(requireContext()).inflate(layoutId, null)
       // customTabView.findViewById<View>(R.id.v_bottom_border).visibility = indicatorVisibility
        val tabTitle = customTabView.findViewById<TextView>(R.id.tv_tab_title)
        tabTitle.text = title
        val tf = ResourcesCompat.getFont(requireContext(), font)
        tabTitle.setTypeface(tf, textStyle)
        tab.customView = customTabView
        tab.tag = title
        if (indicatorVisibility == View.VISIBLE) {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        } else {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        helper.onActivityResult(requestCode, resultCode, data)
        imageChooseDialogOpen = false
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == AppCompatActivity.RESULT_OK) {
                helper.cropDataSection(result.uri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(
                    requireContext(),
                    "Cropping failed: " + result.error,
                    CustomToast.ToastType.FAILED,
                    Toast.LENGTH_LONG
                )
            }
        } else if (resultCode == Activity.RESULT_OK &&
            requestCode == 1000
        ) {
            helper.getProfile(false)
            if (data != null) {
                helper.loadImage(
                    if (data.hasExtra(AppConstant.ID)) {
                        data?.getStringExtra(AppConstant.ID)!!

                    } else {
                        ""
                    }
                )
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    fun logOut() {
        showToast(
            requireContext(),
            "You've logged out.",
            CustomToast.ToastType.SUCCESS,
            Toast.LENGTH_LONG
        )
        PrefUtils.logout(requireContext())
        NebulaDBHelper(requireContext()).clearDataBase()
        navigate.clearLogOut()
        navigate.changeTab(ConstantsNavTabs.NAV_TAB_HOME)
        val intent = Intent(requireActivity(), LoginActivity::class.java)
        intent.putExtra(AppConstant.ISFIRSTTIME, false)
        startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
    }

    fun triggerProfileViewEvent() {
        val firebaseAnalytics = BaseApplication.firebaseAnalytics
        val bundle = Bundle()
        bundle.putString(
            "user_profile_name",
            PrefUtils.getValueFromPreference(requireContext(), PrefUtils.NAME)
        )
        firebaseAnalytics.logEvent("my_profile_view", bundle)
    }


    fun copyClipBoard(copyText: String) {
        val clipboard: ClipboardManager? =
            requireActivity()!!.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager?
        val clip = ClipData.newPlainText("label", copyText)
        clipboard?.setPrimaryClip(clip)
        Toast.makeText(requireActivity(), "Copied", Toast.LENGTH_SHORT).show()
    }


}
