package com.nibula.user.profile.investments

import android.content.Context
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.counter.TimerCounter
import com.nibula.databinding.ProfileInvestmentLayoutBinding
import com.nibula.databinding.ProfileInvestmentLayoutNewBinding
import com.nibula.response.myinvestmentresponse.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.LifecycleViewHolder
import com.nibula.utils.TimestampTimeZoneConverter

import kotlin.math.roundToInt

class InvestmentAdapter(val fg: BaseFragment, private val listener: OnPrivacyChangeListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), TimerCounter.TimerItemRemove {

    public interface OnPrivacyChangeListener {
        fun onChangePrivacyStatus(currentStatus: Int, id: String, recordId: String)
        fun showErrorMessage()
    }


    var list: MutableList<ResponseCollection> = mutableListOf()
    var context: Context = fg.requireContext()
    var thisMillisStart: Long = fg.millisStart
    lateinit var binding: ProfileInvestmentLayoutNewBinding

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder is InvestmentOfferingViewHolder) {
            //println("InvestmentAdapter: onViewAttachedToWindow ${holder.adapterPosition}")
            holder.onAttached()
            holder.registerTimerLiveData()
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is InvestmentOfferingViewHolder) {
            //println("InvestmentAdapter: onViewDetachedFromWindow ${holder.adapterPosition}")
            holder.removeObserver(fg.baseViewModel.getTimeLv())
            holder.onDetached()
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        // clearGlide(holder)
        if (holder is InvestmentOfferingViewHolder) {
            holder.onRecycled()
        }
    }

    override fun onTimerFinish(recordId: String, position: Int) {
        //println("onTimerFinish: recordId $recordId position $position")
        if (position >= 0) {
            fg.handler.postDelayed(Runnable {
                if (position < list.size) {
                    val record = list[position]
                    if (record.id.equals(recordId)) {
                        // println("onTimerFinish: MATCH recordId $recordId position $position")
                        list.removeAt(position)
                        notifyItemRemoved(position)
                        //notifyDataSetChanged()
                    } else {
                        //println("onTimerFinish: UN-MATCH record.id ${record.id} recordId $recordId position $position")
                    }
                }
            }, 1000)
        }
    }


    inner class InvestmentOfferingViewHolder(itemView: View) : LifecycleViewHolder(itemView) {
        var tvStatus: AppCompatTextView? = itemView.findViewById(R.id.tvStatus)

        fun registerTimerLiveData() {
            val data = if (adapterPosition > -1 && adapterPosition < list.size) {
                list[adapterPosition]
            } else {
                null
            }
            //println("InvestmentAdapter: registerTimerLiveData $adapterPosition")
            registerTimerLiveData(
                fg.requireContext(),
                fg.baseViewModel.getTimeLv(),
                thisMillisStart,
                tvStatus,
                data,
                this@InvestmentAdapter
            )
        }
    }

    class InvestmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setData(data: MutableList<ResponseCollection>) {
        list = data
        //setDummyData(list)
        setRemainingTime(list)
        thisMillisStart = fg.millisStart
        notifyDataSetChanged()
    }

    private fun setRemainingTime(dataList: MutableList<ResponseCollection>) {
        for (obj in dataList) {
            if (obj.recordStatusTypeId == AppConstant.initialoffering && obj.investmentStatusId != AppConstant.PENDING) {
                obj.remainingMillis = obj.timeLeftToRelease ?: 0
                obj.tltr = obj.timeLeftToRelease ?: 0
                obj.rId = obj.id ?: ""
            }
        }
    }

    fun addData(data: MutableList<ResponseCollection>) {
        //setDummyData(data)
        setRemainingTime(data)
        list.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        list.clear()
    }

    override fun getItemViewType(position: Int): Int {
        return if (list.isNotEmpty() && (list[position].recordStatusTypeId == AppConstant.initialoffering && list[position].investmentStatusId != AppConstant.PENDING)) {
            1
        } else {
            2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        /* val view =
             LayoutInflater.from(context).inflate(R.layout.profile_investment_layout, parent, false)*/

        val inflater = LayoutInflater.from(parent.context)
        binding = ProfileInvestmentLayoutNewBinding.inflate(inflater, parent, false)

        val holder = if (viewType == 1) {
            InvestmentOfferingViewHolder(binding.root)
        } else {
            InvestmentViewHolder(binding.root)
        }
/*
        holder.itemView.setOnClickListener {
            if (holder.adapterPosition in 0..list.size) {
                fg.navigate.gotorequestinvest(list[holder.adapterPosition].recordId)
            }
        }*/
        holder.itemView.setOnClickListener {
            if (holder.adapterPosition in 0..list.size) {
                val bundle = Bundle().apply {
                    putString(
                        AppConstant.RECORD_IMAGE_URL, list[holder.adapterPosition].recordImage
                    )
                    putString(
                        AppConstant.ID, list[holder.adapterPosition].recordId
                    )
                }
                fg.navigate.goToNewOffering(bundle)
            }
        }


        return holder;
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = list[position]


        data?.uploadingPerson.let {
            binding.tvArtistName.text = it
        }

        data?.recordTitle.let {
            binding.tvRecordTitle.text = it
        }

        val totalToken = data.TotalShares
        val totalOwnedToken = data.totalOwnedTokens

        val totalTokenOwned = "$totalOwnedToken/$totalToken"

        binding.tvTokenOwned.text = totalTokenOwned
        Glide
            .with(context)
            .load(data.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.clImage)
/*
        binding.publicPrivate.visibility = GONE
        if (data.investmentStatusId == 4) {
            binding.publicPrivate.visibility = GONE
            if (data.showInvestorsToPublic!!) {
                binding.publicPrivate.isClickable = true
                binding.publicPrivate.isEnabled = true
                if (data.showToPublic) {
                    binding.publicPrivate.setImageResource(R.drawable.ic_eye_red)
                } else {
                    binding.publicPrivate.setImageResource(R.drawable.ic_eye_gray)
                }
            } else {
                binding.publicPrivate.setImageResource(R.drawable.ic_eye_gray)
                binding.publicPrivate.isClickable = false
                binding.publicPrivate.isEnabled = false
            }
        }
        binding.publicPrivate.setOnClickListener {
            if (data.investmentStatusId == 4) {
                listener.onChangePrivacyStatus(
                    if (data.showToPublic) {
                        1
                    } else {
                        0
                    }, (data.investmentId ?: 1).toString(), data.id
                )
            } else {
                listener.showErrorMessage()
            }
        }
        Glide
            .with(context)
            .load(data.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.layoutImage.clImage)


        when (data.recordTypeId) {
            1 -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
            2 -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.single)}: ${data.recordTitle}"
            }
            else -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
        }

       binding.artistName.text =
            "${context.getString(R.string.artist)}: ${data.uploadingPerson}"


        val inv = data.totalNoOfInvesters ?: 0
        val temp = "$inv "
        val spn1 = SpannableStringBuilder()
        spn1.append(
            CommonUtils.setSpannable(
                context,
                "$inv ",
                0,
                temp.length
            )
        )
        if (inv <= 1) {
            spn1.append("${context.getString(R.string.investor_)}")
        } else {
            spn1.append("${context.getString(R.string.investors)}")
        }
        binding.tvInvestors.text = spn1

        val raised = CommonUtils.trimDecimalToTwoPlaces(data.totalPaidAmount ?: 0.0)
        binding.tvRaised.text =
            "${data.currencySymbol}$raised ${context.getString(R.string.raised)}"

        when (data.recordStatusTypeId) {
            AppConstant.released -> {
                binding.tvInvestmentStatus.visibility = GONE
                binding.tvStatus.text = AppConstant.Released
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.red_shade_1_100per
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.white_2)
            }

            AppConstant.request_submitted -> {
                binding.tvStatus.text = AppConstant.pending
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background)
            }

            AppConstant.initialoffering -> {
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.red_back_2_dp)

                */
/*Counter*//*

                val temp = TimestampTimeZoneConverter.convertToMilliseconds(
                    data.timeLeftToRelease ?: 0
                )
                if (temp > TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                    val time =
                        TimestampTimeZoneConverter.getDaysAndHourDifference(
                            context,
                            temp
                        )
                    binding.tvStatus.visibility = View.VISIBLE
                    binding.tvStatus.text = time
                } else {
                    binding.tvStatus.visibility = View.INVISIBLE
                    if (temp > 0 &&
                        !data.isFinish
                    ) {
                        val workingTime =
                            TimestampTimeZoneConverter.getDaysAndHourDifference(
                                context,
                                data.remainingMillis
                            )
                        binding.tvStatus.text = workingTime
                        binding.tvStatus.visibility = View.VISIBLE
                    }
                }

                when (data.investmentStatusId) {

                    AppConstant.PENDING -> {
                        binding.tvInvestmentStatus.visibility = GONE
                       binding.tvInvestmentStatus.text = "${data.investmentStatus}"
                        binding.tvInvestmentStatus.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.gray_shade_1_100per
                            )
                        )

                    }

                    AppConstant.APPROVED -> {
                      binding.tvInvestmentStatus.visibility = View.GONE
                        binding.tvInvestmentStatus.text = "${data.investmentStatus}"
                        binding.tvInvestmentStatus.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.green_shade_1_100per
                            )
                        )
                    }

                    AppConstant.REJECTED -> {
                        binding.tvInvestmentStatus.visibility = View.GONE
                        binding.tvInvestmentStatus.text = "${data.investmentStatus}"
                        binding.tvInvestmentStatus.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.red_shade_1_100per
                            )
                        )
                    }

                    AppConstant.INVESTED -> {
                    binding.tvInvestmentStatus.visibility = View.GONE
                        binding.tvInvestmentStatus.text = "${data.investmentStatus}"
                        binding.tvInvestmentStatus.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.green_shade_1_100per
                            )
                        )
                    }

                    else -> {
                        binding.tvInvestmentStatus.visibility = View.GONE
                    }
                }

            }
            */
/* else -> {
                 holder.itemView.tvStatus.setTextColor(
                     ContextCompat.getColor(
                         context,
                         R.color.white
                     )
                 )
                 holder.itemView.tvStatus.text = AppConstant.pending
                 holder.itemView.tvStatus.background =
                     ContextCompat.getDrawable(context, R.drawable.panding_background)
             }*//*

        }
*/


//        when (data.investmentStatusId) {
//
//            AppConstant.PENDING -> {
//                holder.itemView.tv_investment_status.visibility = GONE
//                holder.itemView.tv_investment_status.text = AppConstant.pending
//                holder.itemView.tv_investment_status.setTextColor(
//                    ContextCompat.getColor(
//                        context,
//                        R.color.white
//                    )
//                )
////                holder.itemView.tvStatus.background =
////                    ContextCompat.getDrawable(context, R.drawable.panding_background)
//            }
//
//            AppConstant.APPROVED -> {
//                holder.itemView.tv_investment_status.visibility = VISIBLE
//                holder.itemView.tv_investment_status.text = "${data.investmentStatus}"
//                holder.itemView.tv_investment_status.setTextColor(
//                    ContextCompat.getColor(
//                        context,
//                        R.color.green_shade_1_100per
//                    )
//                )
//            }
//
//            AppConstant.REJECTED -> {
//                holder.itemView.tv_investment_status.visibility = VISIBLE
//                holder.itemView.tv_investment_status.text = "${data.investmentStatus}"
//                holder.itemView.tv_investment_status.setTextColor(
//                    ContextCompat.getColor(
//                        context,
//                        R.color.red_shade_1_100per
//                    )
//                )
//            }
//
//            AppConstant.INVESTED -> {
//                holder.itemView.tv_investment_status.visibility = GONE
//                //holder.itemView.tv_investment_status.setTextColor(ContextCompat.getColor(context,R.color.green_shade_1_100per))
//            }
//
//            else -> {
//                holder.itemView.tv_investment_status.visibility = GONE
//            }
//        }

    }

    fun dpToPx(dp: Int): Int {
        val displayMetrics: DisplayMetrics = context.resources.displayMetrics
        return (dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }


    fun updateItem(id: String) {
        for (i in 0 until list.size) {
            if (list[i].id == id) {
                list[i].showToPublic = !list[i].showToPublic
                notifyItemChanged(i)
                break
            }
        }

    }

}