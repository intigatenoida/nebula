package com.nibula.user.profile

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nibula.request_to_buy.navigate
import com.nibula.user.profile.investments.ProfileInvestmentFragment
import com.nibula.user.profile.music.ProfileMusicFragment
import com.nibula.utils.SmartFragmentStatePagerAdapter

class ProfilePagerAdapter(fn: FragmentManager, val navigate: navigate) :
    SmartFragmentStatePagerAdapter(fn) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            1 -> {
                ProfileMusicFragment.newInstance()
            }
            else -> {
                ProfileInvestmentFragment.newInstance()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}