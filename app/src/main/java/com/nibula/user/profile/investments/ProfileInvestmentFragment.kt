package com.nibula.user.profile.investments

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.bottomsheets.PrivacyErrorMessage
import com.nibula.customview.CustomToast
import com.nibula.databinding.ProfileInvestmentFragmentBinding
import com.nibula.response.myinvestmentresponse.MyInvestmentResponse
import com.nibula.user.profile.helper.ProfileInvestmentFragmentHelper
import com.nibula.utils.interfaces.DialogFragmentClicks

import retrofit2.Call


class ProfileInvestmentFragment : BaseFragment(), InvestmentAdapter.OnPrivacyChangeListener {

    companion object {
        fun newInstance() = ProfileInvestmentFragment()
    }

    private lateinit var viewModel: InvestmentViewModel
    lateinit var binding:ProfileInvestmentFragmentBinding

    lateinit var rootView: View
    lateinit var investmentAdapter: InvestmentAdapter
    private lateinit var helper: ProfileInvestmentFragmentHelper

    var call: Call<MyInvestmentResponse>? = null

    var isDataAvailable = false
    var isLoading = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.profile_investment_fragment, container, false)
            binding=ProfileInvestmentFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
            intiUI()
        }
        return rootView
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(this).get(InvestmentViewModel::class.java)
        binding.rvInvestors.adapter = investmentAdapter
    }

    override fun onResume() {
        super.onResume()
        if (call == null ||
            (call!!.isCanceled && !call!!.isExecuted)
        ) {
            //helper.getInvestmentListing("", true)

            helper.myInvestmentTokenList(true)
        }
    }
    override fun onStop() {
        super.onStop()
        if (call != null &&
            !call!!.isExecuted
        ) {
            call!!.cancel()
        }
    }

    private fun intiUI() {
        helper = ProfileInvestmentFragmentHelper(this)

        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvInvestors.layoutManager = layoutManager
        investmentAdapter = InvestmentAdapter(this, this)
        binding.rvInvestors.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    //helper.getInvestmentListing("", false)

                    helper.myInvestmentTokenList(true)
                }
            }
        })

        binding.swipeInvest.setOnRefreshListener {
            helper.investmentNextPage = 1
            helper.investmentMaxPage = 0
            helper.myInvestmentTokenList(true)
            //helper.getInvestmentListing("", false)
        }
    }
    override fun onDestroyView() {
       // binding?.musicRv?.adapter = null
        super.onDestroyView()
    }

    override fun onChangePrivacyStatus(currentStatus: Int, id: String, recordId: String) {
    }


    override fun showErrorMessage() {
        val privacyErrorDialog = PrivacyErrorMessage()
        privacyErrorDialog.show(requireActivity().supportFragmentManager, "PrivacyErrorMessage")
    }


}
