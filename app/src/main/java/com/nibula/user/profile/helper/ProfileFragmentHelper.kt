package com.nibula.user.profile.helper

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.core.content.FileProvider
import com.nibula.BuildConfig
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.customview.CustomToast.showToast
import com.nibula.request.LogOutRequest
import com.nibula.response.BaseResponse
import com.nibula.response.UserProfileUploadResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.uploadimage.UploadImageHelper
import com.nibula.user.profile.ProfileFragment
import com.nibula.user.profile.ProfileResponce
import com.nibula.user.profile.UserProfile
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class ProfileFragmentHelper(val fg: ProfileFragment) : BaseHelperFragment() {
    private var isCameraSelected = false
    var userProfile: UserProfile? = null
    private var originalFilePath: String? = null
    private var originalUri: Uri? = null
    private var sendFile: File? = null

    fun getProfile(isProgressDialog: Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss(isProgressDialog)
            return
        }

        if (isProgressDialog) {
            fg.showProcessDialog()
        }

        val helper = ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.getProfile("")
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<ProfileResponce>() {
            override fun onSuccess(any: ProfileResponce?, message: String?) {
                if (any != null &&
                    any.responseStatus == 1 &&
                    any.userProfile != null
                ) {
                    userProfile = any.userProfile
                    fg.binding.tvUserName.text = any.userProfile!!.userName
                    fg.binding.tvProfession.text = any.userProfile!!.bio

                    fg.binding.totalRaisedTv.text =
                        "${any.userProfile!!.currencyType ?: ""}${
                            CommonUtils.trimDecimalToTwoPlaces(
                                any.userProfile!!.totalRaised
                            )
                        }"
                    fg.binding.releaseCountTv.text = "${any.userProfile!!.totalReleased}"
                    fg.binding.totalFollowersTv.text = any.userProfile!!.followers
                    fg.binding.edtFirstName.text = any.userProfile!!.firstName

/*
                    any.userProfile?.cryptoWalletAddress?.let {

                        if (it.isNotEmpty()) {
                            fg.rootView.cl.visibility = View.VISIBLE
                            it?.let {
                                val subString = it.substring(0, 15)
                                fg.rootView.tvWalletAddress.text = "$subString..."
                                fg.walletAddress = it
                            }

                            val walletAddress =
                                PrefUtils.getValueFromPreference(
                                    fg.requireContext(),
                                    PrefUtils.CONNECTED_WALLET_ADDRESS
                                )
                            if (walletAddress.isEmpty()) {

                                PrefUtils.saveValueInPreference(
                                    fg.requireActivity(),
                                    PrefUtils.CONNECTED_WALLET_ADDRESS,
                                    it
                                )
                            } else {


                            }


                        }
                    }
*/
                    fg.binding.verifiedUsed.visibility = if (any.userProfile!!.isUserVerified) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                    if (any.userProfile!!.userImage.isNotEmpty()) {
                        PrefUtils.saveValueInPreference(
                            fg.requireContext(),
                            PrefUtils.USER_IMAGE,
                            any.userProfile!!.userImage
                        )
                        loadImage(any.userProfile!!.userImage ?: "")
                    }
                }
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                if (error!!.errorMessage.equals("HTTP 401 Unauthorized")) {
                    CommonUtils.logOut(fg.requireActivity())
                    dismiss(isProgressDialog)
                } else {
                    dismiss(isProgressDialog)
                }
            }

        }))

    }

    fun loadImage(userImage: String) {
        PrefUtils.saveValueInPreference(fg.requireContext(), PrefUtils.USER_IMAGE, userImage)
        CommonUtils.loadImageWithOutCache(
            fg.requireContext(),
            userImage,
            fg.binding.profileImage,
            R.drawable.ic_record_user_place_holder
        )
        CommonUtils.loadImageWithOutCache(
            fg.requireContext(),
            userImage,
            fg.binding.backgroundImage
        )
    }


    private fun dismiss(isProgressDialog: Boolean) {
        if (isProgressDialog) {
            fg.hideProcessDialog()
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                AppConstant.REQUEST_UPLOAD_IMAGE -> {
                    handleImageResult(data)
                }
            }
        }
    }

    private fun handleImageResult(data: Intent?) {
        fg.showProcessDialog()
        isCameraSelected = false
        val disposableObserver = object : DisposableObserver<String>() {
            override fun onComplete() {
                fg.hideProcessDialog()
            }

            override fun onNext(t: String) {
                fg.hideProcessDialog()
                if (originalUri == null) {
                    fg.showToast(
                        fg.requireContext(),
                        fg.getString(R.string.message_upload_image),
                        CustomToast.ToastType.FAILED
                    )
                    return
                }
                //start cropping
                startCropImageActivity(originalUri!!)
            }

            override fun onError(e: Throwable) {
                fg.hideProcessDialog()
            }

        }
        disposables.add(getObserver(data, disposableObserver))
    }


    private fun getObserver(
        data: Intent?,
        disposableObserver: DisposableObserver<String>
    ): DisposableObserver<String> {
        return getObservable(data)
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(disposableObserver)
    }

    private fun getObservable(data: Intent?): Observable<String> {
        return Observable.create { emitter ->
            if (data?.data != null) {
                //Photo from gallery
                val f = UploadImageHelper.copyFileFromStream(
                    fg.requireContext(),
                    data.data!!,
                    UploadImageHelper.TEMPORARY_PROFILE_IMAGE
                )
                originalUri = Uri.fromFile(f)
                originalFilePath = f.absolutePath
            } else {
                //Photo from Camera
                isCameraSelected = true
            }

            if (!originalFilePath.isNullOrEmpty()) {
                emitter.onNext(originalFilePath ?: "")
                emitter.onComplete()
            } else {
                emitter.onError(Throwable("queryImageUrl is empty"))
            }
        }
    }

    fun startCropImageActivity(imageUri: Uri) {
        CropImage.activity(imageUri)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setCropShape(CropImageView.CropShape.RECTANGLE)
            .setAspectRatio(1, 1)
            .start(fg.requireActivity())
    }

    fun setImageUri(): Uri {
        val file = UploadImageHelper.tempFileReturn(
            fg.requireContext(),
            UploadImageHelper.TEMPORARY_PROFILE_IMAGE
        )
        originalUri = FileProvider.getUriForFile(
            fg.requireContext(),
            "${BuildConfig.APPLICATION_ID}.${fg.getString(R.string.file_provider_name)}",
            file
        )
        originalFilePath = file.absolutePath
        return originalUri!!
    }


    fun cropDataSection(uri: Uri?) {
        if (uri == null) {
            fg.showToast(
                fg.requireContext(),
                "Record image cropping failed",
                CustomToast.ToastType.FAILED
            )
            return
        }
        if (sendFile != null) {
            sendFile!!.delete()
        }
        val path = uri.path ?: ""
        sendFile = if (path.isNotEmpty()) {
            File(path)
        } else {
            null
        }
        if (sendFile == null) {
            fg.showToast(
                fg.requireContext(),
                fg.getString(R.string.please_select_record_image),
                CustomToast.ToastType.FAILED
            )
            return
        }
        uploadImage(true)
    }

    private fun uploadImage(isProgressDialog: Boolean) {
        if (!fg.isOnline()) {
            showToast(
                fg.requireContext(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideProcessDialog()
            return
        }

        if (isProgressDialog) {
            fg.hideProcessDialog()
        }

        val mediaType = "image/*".toMediaTypeOrNull()!!
        val requestBody = sendFile?.asRequestBody(mediaType) ?: "".toRequestBody(mediaType)
        val body =
            MultipartBody.Part.createFormData("UserImage", sendFile?.name ?: "", requestBody)

        val helper = ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
        val observable = helper.updateImage(body)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<UserProfileUploadResponse>() {
            override fun onSuccess(any: UserProfileUploadResponse?, message: String?) {
                if (any?.responseStatus == 1) {
                    showToast(
                        fg.requireContext(),
                        any.responseMessage ?: "",
                        CustomToast.ToastType.SUCCESS
                    )
                    userProfile!!.userImage = any.userImage
                    CommonUtils.loadImageWithOutCache(
                        fg.requireContext(),
                        any.userImage,
                        fg.binding.profileImage,
                        R.drawable.ic_record_user_place_holder
                    )
                    CommonUtils.loadImageWithOutCache(
                        fg.requireContext(),
                        any.userImage,
                        fg.binding.backgroundImage,
                        R.drawable.ic_record_user_place_holder
                    )

                }
                dismiss(isProgressDialog)
            }

            override fun onError(error: RetroError?) {
                dismiss(isProgressDialog)
            }
        }))

    }

    fun expireToken(token: String) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        fg.showProcessDialog()
        val helper = ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
        val request = LogOutRequest(token)
        val observable = helper.logOut(request)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String?) {
                val response = any as BaseResponse
                if (response.responseStatus ?: 0 == 1) {

                    fg.hideProcessDialog()
                    CommonUtils.triggerLogOutEvent(
                        PrefUtils.getValueFromPreference(
                            fg.requireContext(),
                            PrefUtils.PHONE_NO
                        )
                    )
                    fg.logOut()
                }
            }

            override fun onError(error: RetroError?) {
                fg.logOut()
                fg.hideProcessDialog()
            }
        }))
    }

}