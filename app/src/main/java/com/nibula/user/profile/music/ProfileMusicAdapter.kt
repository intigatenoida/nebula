package com.nibula.user.profile.music

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.counter.TimerCounter
import com.nibula.databinding.ProfileMusicLayoutBinding
import com.nibula.response.musicprofile.ResponseCollection
import com.nibula.response.saveddraft.SavedDraftRecordData
import com.nibula.utils.*

import kotlin.math.roundToInt

class ProfileMusicAdapter(val fg: BaseFragment) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), TimerCounter.TimerItemRemove {

    var list: MutableList<ResponseCollection> = mutableListOf()

    var context: Context = fg.requireContext()

    var thisMillisStart: Long = fg.millisStart
    lateinit var binding:ProfileMusicLayoutBinding

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder is ProfileOfferingViewHolder) {
            println("ProfileMusicAdapter: onViewAttachedToWindow ${holder.adapterPosition}")
            holder.onAttached()
            holder.registerTimerLiveData()
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is ProfileOfferingViewHolder) {
            println("ProfileMusicAdapter: onViewDetachedFromWindow ${holder.adapterPosition}")
            holder.removeObserver(fg.baseViewModel.getTimeLv())
            holder.onDetached()
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        // clearGlide(holder)
        if (holder is ProfileOfferingViewHolder) {
            holder.onRecycled()
        }
    }

    override fun onTimerFinish(recordId: String, position: Int) {
        //println("onTimerFinish: recordId $recordId position $position")
        if (position >= 0) {
            fg.handler.postDelayed(Runnable {
                if (position < list.size) {
                    val record = list[position]
                    if (record.id.equals(recordId)) {
                        //println("onTimerFinish: MATCH recordId $recordId position $position")
                        list.removeAt(position)
                        notifyItemRemoved(position)
                        //notifyDataSetChanged()
                    } else {
                        //println("onTimerFinish: UN-MATCH record.id ${record.id} recordId $recordId position $position")
                    }
                }
            }, 1000)
        }
    }

    inner class ProfileOfferingViewHolder(itemView: View) : LifecycleViewHolder(itemView) {
        var tvStatus: AppCompatTextView? = itemView.findViewById(R.id.tvStatus)

        fun registerTimerLiveData() {
            val data = if (adapterPosition > -1 && adapterPosition < list.size) {
                list[adapterPosition]
            } else {
                null
            }
            //println("ProfileMusicAdapter: registerTimerLiveData $adapterPosition")
            registerTimerLiveData(
                fg.requireContext(),
                fg.baseViewModel.getTimeLv(),
                thisMillisStart,
                tvStatus,
                data,
                this@ProfileMusicAdapter
            )
        }
    }

    inner class ProfileViewHolder(itemView: View) : LifecycleViewHolder(itemView)

    fun setData(data: MutableList<ResponseCollection>) {
        list = data
        //setDummyData(list)
        setRemainingTime(list)
        thisMillisStart = fg.millisStart
        notifyDataSetChanged()
    }

    private fun setRemainingTime(dataList: MutableList<ResponseCollection>) {
        for (obj in dataList) {
            if (obj.recordStatusTypeId == AppConstant.initialoffering) {
                obj.remainingMillis = obj.timeLeftToRelease ?: 0
                obj.tltr = obj.timeLeftToRelease ?: 0
                obj.rId = obj.id ?: ""
            }
        }
    }

    fun addData(data: MutableList<ResponseCollection>) {
        //setDummyData(data)
        setRemainingTime(data)
        list.addAll(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        list.clear()
    }

    override fun getItemViewType(position: Int): Int {
        return if (list.isNotEmpty() &&
            list[position].recordStatusTypeId == AppConstant.initialoffering
        ) {
            1
        } else {
            2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       /* val view =
            LayoutInflater.from(context).inflate(R.layout.profile_music_layout, parent, false)*/


        val inflater = LayoutInflater.from(parent.context)
        binding = ProfileMusicLayoutBinding.inflate(inflater, parent, false)


        val holder = if (viewType == 1) {
            ProfileOfferingViewHolder(binding.root)
        } else {
            ProfileViewHolder(binding.root)
        }
        /*holder.itemView.setOnClickListener {
            if (holder.adapterPosition in 0..list.size) {
                fg.navigate.gotorequestinvest(list[holder.adapterPosition].id)
            }
        }*/
        return holder
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = list[position]
       binding.rejectedMsg.visibility = View.GONE
        binding.tvInvestors.visibility = View.VISIBLE
        binding.tvRaised.visibility = View.VISIBLE
        binding.layoutImage.clImage.borderWidth = 0f
        Glide
            .with(context)
            .load(data.recordImage)
            .placeholder(R.drawable.nebula_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.IMMEDIATE)
            .into(binding.layoutImage.clImage)


        when (data.recordTypeId) {
            1 -> {
               binding.tvAlbumTitle.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
            2 -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.single)}: ${data.recordTitle}"
            }
            else -> {
                binding.tvAlbumTitle.text =
                    "${context.getString(R.string.album)}: ${data.recordTitle}"
            }
        }

        binding.artistName.text =
            "${context.getString(R.string.artist)}: ${data.artistsName}"

        val inv = data.totalNoOfInvesters ?: 0
        val temp = "$inv "
        val spn1 = SpannableStringBuilder()
        spn1.append(
            CommonUtils.setSpannable(
                context,
                "$inv ",
                0,
                temp.length
            )
        )
        if (inv == 1) {
            spn1.append("${context.getString(R.string.investor_)}")
        } else {
            spn1.append("${context.getString(R.string.investors)}")
        }
        binding.tvInvestors.text = spn1

        val raised = CommonUtils.trimDecimalToTwoPlaces(data.totalInvestedAmount ?: 0.0)
        binding.tvRaised.text =
            "${data.currencyType}$raised ${context.getString(R.string.raised)}"

        when (data.recordStatusTypeId) {
            AppConstant.no_investment -> {
//                holder.itemView.setOnClickListener {
//                    fg.navigate.gotorequestinvest(data.id)
//                }
                binding.tvStatus.setPadding(dpToPx(5), 0, dpToPx(5), 0)
                binding.tvDate.visibility = View.GONE
                binding.tvStatus.text = data.recordStatusType
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.setPadding(dpToPx(17), 0, dpToPx(17), 0)
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background)

            }

            AppConstant.approved_by_admin -> {
                holder.itemView.setOnClickListener {

                    val bundle = Bundle().apply {
                        putString(
                            AppConstant.RECORD_IMAGE_URL,
                            data.recordImage
                        )
                        putString(
                            AppConstant.ID,
                           data.id
                        )
                    }
                    fg.navigate.goToNewOffering(bundle)
/*
                    fg.navigate.gotorequestinvest(data.id)
*/
                }

                binding.tvStatus.setPadding(dpToPx(5), 0, dpToPx(5), 0)
                binding.tvDate.visibility = View.GONE
                binding.tvStatus.text = data.recordStatusType
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.setPadding(dpToPx(17), 0, dpToPx(17), 0)
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background)

            }
            AppConstant.rejected -> {
/*
                holder.itemView.setOnClickListener {
                    val savedDraftRecordData = getSavedDraftRecordData(data)
                    if (data.screenType == 0) {
                        fg.startActivityForResult(
                            Intent(
                                fg.requireContext(),
                                UploadRecordActivity::class.java
                            )
                                .putExtra(AppConstant.RECORD_DATA, savedDraftRecordData),
                            AppConstant.REQUEST_SAVE_DRAFTS_REFRESH
                        )
                    } else {
                        val sendIntent = Intent(fg.requireContext(), EditSongActivity::class.java)
                        sendIntent.putExtra(AppConstant.RECORD_DATA, savedDraftRecordData)
                        fg.startActivityForResult(
                            sendIntent,
                            RequestCode.REQUEST_EDIT_SONG_SCREEN
                        )
                    }

                }
*/
                binding.tvStatus.setPadding(dpToPx(5), 0, dpToPx(5), 0)
                binding.tvDate.visibility = View.GONE
                binding.tvStatus.text = data.recordStatusType
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.error_text_color
                    )
                )
                binding.tvStatus.setPadding(dpToPx(17), 0, dpToPx(17), 0)
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.bg_rejected)

                binding.rejectedMsg.visibility = View.VISIBLE
                binding.rejectedMsg.text = data.rejectionReason

            }

            AppConstant.released -> {

                holder.itemView.setOnClickListener {

                    if (holder.adapterPosition in 0..list.size) {
                        val bundle = Bundle().apply {
                            putString(
                                AppConstant.RECORD_IMAGE_URL,
                                list[holder.adapterPosition].recordImage
                            )
                            putString(
                                AppConstant.ID,
                                list[holder.adapterPosition].id
                            )
                        }
                        fg.navigate.goToNewOffering(bundle)
                    }

                 //   fg.navigate.gotorequestinvest(data.id)
                }

                binding.tvStatus.setPadding(dpToPx(5), 0, dpToPx(5), 0)
                binding.tvDate.visibility = View.GONE
                binding.tvStatus.text = AppConstant.AVAILABLE
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.green_shade_1_100per
                    )
                )
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.bg_available)

            }

            AppConstant.request_submitted -> {
                holder.itemView.setOnClickListener {
                   // fg.navigate.gotorequestinvest(data.id)


                    val bundle = Bundle().apply {
                        putString(
                            AppConstant.RECORD_IMAGE_URL,
                            data.recordImage
                        )
                        putString(
                            AppConstant.ID,
                            data.id
                        )
                    }
                    //fg.navigate.gotorequestinvest(list[holder.adapterPosition].id)
                    fg.navigate.goToNewOffering(bundle)


                }
                binding.tvStatus.text = AppConstant.pending
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                binding.tvStatus.setPadding(dpToPx(17), 0, dpToPx(17), 0)
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background)

                binding.tvDate.visibility = View.VISIBLE
                binding.tvDate.text = data.offeringEndDate?.let {
                    TimestampTimeZoneConverter.convertToLocalDate(
                        it, TimestampTimeZoneConverter.UTC_TIME, "dd/MM/yyyy"
                    )
                }?.let { releaseDateString(it) }
            }

            AppConstant.sold_out -> {
                binding.tvStatus.setPadding(dpToPx(5), 0, dpToPx(5), 0)
                binding.tvDate.visibility = View.GONE
                binding.tvStatus.text = data.recordStatusType
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.panding_background_7dp)
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
            }

            AppConstant.draft -> {//  for draft
                holder.itemView.setOnClickListener {
                    fg.getRecordDataDetail(data.id.toString())

                }
                binding.tvDate.visibility = View.GONE
                binding.tvStatus.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.black
                    )
                )
                binding.tvStatus.setPadding(dpToPx(21), 0, dpToPx(21), 0)
                binding.tvStatus.text = AppConstant.Draft
                binding.tvStatus.background =
                    ContextCompat.getDrawable(context, R.drawable.draft_background)
                binding.tvInvestors.visibility = View.GONE
                binding.tvRaised.visibility = View.GONE


            }


        }

    }

    private fun getSavedDraftRecordData(data: ResponseCollection): SavedDraftRecordData {
        return SavedDraftRecordData(
            data.amountAvailableToInvest,
            data.approveEachInvestor,
            data.artistsId,
            data.artistsName,
            data.createdOn,
            data.currencyType,
            data.description,
            data.genreType,
            data.enreTypeIds,
            data.id,
            data.investmentAmountNeeded,
            data.labels,
            data.offeringEndDate,
            data.offeringStartDate,
            data.pAdvisoryType,
            data.pAdvisoryTypeId,
            data.performanceCopyrightNo,
            data.priceAfterRelease,
            data.pricePerRoyaltyShare,
            data.showInvestorsToPublic,
            data.recordImage,
            data.recordStatusType,
            data.recordStatusTypeId,
            data.recordTitle,
            data.recordType,
            data.recordTypeId,
            data.recordingCopyrightNo,
            data.releaseDate,
            data.screenType,
            data.sharesAvailable?.toInt(),
            data.timeLeftToRelease,
            data.totalInvestedAmount,
            data.totalNoOfInvesters,
            data.totalRecords,
            data.valueOfAShare,
            data.previewDurationInSec,
            data.currencyTypeId,
            data.copyrightSocietyRecording,
            data.copyrightSocietyWork,
            data.iSCRCode,
            data.iSWCCode,
            data.messageToCoOwners,
            data.oneOffPaymentForMusicStreamAccess,
            data.pricePerRoyaltyShareRecording?.toDouble(),
            data.pricePerRoyaltyShareWork?.toDouble(),
            data.societyNameRecording,
            data.societyNameWork,
            0.0
        )
    }

    private fun releaseDateString(date: String): SpannableString {
        val ss1 = SpannableString("${context.getString(R.string.release_date)} $date")
        ss1.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
            context.getString(R.string.release_date).length + 1,
            ss1.length,
            0
        )
        return ss1
    }

    fun dpToPx(dp: Int): Int {
        val displayMetrics: DisplayMetrics = context.resources.displayMetrics
        return (dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }

}