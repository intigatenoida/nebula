package com.nibula.user.profile


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ProfileResponce(
    @SerializedName("UserProfile")
    var userProfile: UserProfile? = null,
    @SerializedName("ResponseMessage")
    var responseMessage: String? = null,
    @SerializedName("ResponseStatus")
    var responseStatus: Int? = null
)