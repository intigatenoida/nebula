package com.nibula.user.sign_up

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.safetynet.SafetyNet
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.hbb20.CountryCodePicker
import com.nibula.R
import com.nibula.TermsConditionActivity
import com.nibula.base.BaseActivity
import com.nibula.base.OtpVerificationDialog
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.customview.CustomToast
import com.nibula.dashboard.DashboardActivity
import com.nibula.databinding.SupportAllDeviceCreateAccountActivityBinding
import com.nibula.fcm.UpdateDeviceInfoReq
import com.nibula.request.RegisterRequest
import com.nibula.request.VerifyOtpCreateAccount
import com.nibula.response.BaseResponse
import com.nibula.response.registerResponse.RegisterResponse
import com.nibula.response.signup.SignupResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.user.forget_password.OTPActivity
import com.nibula.user.login.LoginActivity
import com.nibula.user.sign_up.adapter.CountryListAdapter
import com.nibula.user.sign_up.helper.CreateAccountHelper
import com.nibula.user.sign_up.modal.ResponseCollection
import com.nibula.user.welcome.WelcomeNebulaScreen
import com.nibula.utils.AppConstant
import com.nibula.utils.AppConstant.Companion.SEPRATER
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import com.nibula.utils.interfaces.DialogFragmentClicks

import java.util.regex.Pattern


class CreateAccountActivity : BaseActivity(), CountryListAdapter.Communicator,
    DialogFragmentClicks {
    val FIRST_NAME = 1
    val LAST_NAME = 2
    var previousLength = 0
    lateinit var CountryList: MutableList<ResponseCollection>
    lateinit var madapter: CountryListAdapter
    lateinit var helper: CreateAccountHelper
    lateinit var dialog: BottomSheetDialog
    val VERIFY_OTP = 1010
    var userId: String = ""
    lateinit var selectedData: ResponseCollection
    lateinit var countryCodePicker: CountryCodePicker
    var isPhoneNumberIsValid: Boolean = false
    lateinit var binding: SupportAllDeviceCreateAccountActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.support_all_device_create_account_activity)
        binding = SupportAllDeviceCreateAccountActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        countryCodePicker = findViewById(R.id.ccpLogin)
        countryCodePicker?.setOnCountryChangeListener {

            val selectedCountryCode = countryCodePicker?.selectedCountryCode
        }
        countryCodePicker.setPhoneNumberValidityChangeListener {
            isPhoneNumberIsValid = it

        }
        countryCodePicker.registerCarrierNumberEditText(binding.edtPhoneNumberValue)
        CommonUtils.triggerSignUpInitiatedEvent()
        showSoftKeyboard(binding.edtFirstName)
        //--> Init Helper
        helper =
            CreateAccountHelper(this@CreateAccountActivity)
        //helper.getCountryList(true)
        binding.edtPhoneNumberValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString().length == 1 && s.toString().startsWith("0")) {
                    s.clear()
                }
            }
        })
        findViewById<AppCompatTextView>(R.id.nebulaHeadingTv).setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.nebu.la"))
            startActivity(browserIntent)
        }
        //--> Country Code Click Handler
        binding.edtPhoneNumber.setOnClickListener {
            if (!::CountryList.isInitialized ||
                CountryList.isEmpty()
            ) {

                binding.txtError.visibility = View.VISIBLE
                binding.txtError.text = getString(R.string.country_list_error)
                /*
                  showToast(
                      this@CreateAccountActivity,
                      getString(R.string.country_list_error),
                      CustomToast.ToastType.FAILED
                  )*/
                return@setOnClickListener
            }

            // initalizeBottomSheet()
        }
        val ss1 = SpannableString(getString(R.string.login))
        ss1.setSpan(
            UnderlineSpan(), 0,
            ss1.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        binding.tvLogin.text = ss1

        binding.tvLogin.setOnClickListener {
            if (intent.hasExtra(AppConstant.IS_START_FROM_GUIDE)) {
                val intent = Intent(this, LoginActivity::class.java)
                intent.putExtra(AppConstant.ISFIRSTTIME, true)
                startActivity(intent)
                finish()
            } else {
                if (if (intent.hasExtra(AppConstant.IS_FROM_LOGIN)) {
                        intent.getBooleanExtra(AppConstant.IS_FROM_LOGIN, false)
                    } else {
                        false
                    }
                ) {
                    setResult(Activity.RESULT_FIRST_USER)
                    finish()
                }
            }
        }
        binding.skipTxt.setOnClickListener {

            CommonUtils.triggerSignUpSkip()

            if (intent.hasExtra(AppConstant.IS_START_FROM_GUIDE)) {

                val intent = Intent(this, DashboardActivity::class.java)
                startActivity(intent)
                finish()
            } else if (!PrefUtils.getBooleanValue(
                    this@CreateAccountActivity,
                    PrefUtils.IS_USER_SKIP_FROM_LOGIN
                )
            ) {
                val intent = Intent(this, DashboardActivity::class.java)
                startActivity(intent)
                finish()

            } else if (if (intent.hasExtra(AppConstant.IS_FROM_LOGIN)) {
                    intent.getBooleanExtra(AppConstant.IS_FROM_LOGIN, false)
                } else {
                    false
                }
            ) {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }
        binding.edtUserName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isNotEmpty() && !s.toString().startsWith("@", true)) {
                    binding.edtUserName.setText("@${s.toString()}")
                    binding.edtUserName.setSelection(binding.edtUserName.text.toString().length)
                }
            }

        })
        binding.edtUserName.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus && binding.edtUserName.text.toString().trim().isEmpty()) {
                binding.edtUserName.setText("@")
                binding.edtUserName.setSelection(1)
            } else if (!hasFocus && binding.edtUserName.text.toString().trim().isEmpty()) {
                binding.edtUserName.setText("")
            }
        }


        binding.edtPhoneNumberValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                formatPhoneNumber(s.toString(), 10)
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        checkedSpanable()
        hideKeyboard()
    }

    private fun formatPhoneNumber(phoneNumber: String, maxLength: Int) {

        if (previousLength == phoneNumber.length) return
        var tempPhoneNumber = phoneNumber

        if (tempPhoneNumber.startsWith("0")) {
            if (tempPhoneNumber.contains(AppConstant.PREFIX_NUMBER)) {
                tempPhoneNumber = "${tempPhoneNumber.subSequence(1, tempPhoneNumber.length)}"
            } else {
                if (tempPhoneNumber.length == 1) {
                    tempPhoneNumber = AppConstant.PREFIX_NUMBER
                } else {
                    if (tempPhoneNumber.contains(")")) {
                        tempPhoneNumber = tempPhoneNumber.replace(")", "").trim()
                        tempPhoneNumber = tempPhoneNumber
                    } else {
                        tempPhoneNumber = tempPhoneNumber.replace("(", "").replace(")", "").trim()
                        tempPhoneNumber =
                            "${AppConstant.PREFIX_NUMBER}${
                                tempPhoneNumber.subSequence(
                                    1,
                                    tempPhoneNumber.length
                                )
                            }"
                    }

                }
            }
        } else if (tempPhoneNumber.startsWith("(") && !tempPhoneNumber.contains(AppConstant.PREFIX_NUMBER)) {
            tempPhoneNumber =
                tempPhoneNumber.replace("(", "").replace(")", "").trim()
            if (tempPhoneNumber.startsWith("0")) {
                tempPhoneNumber = tempPhoneNumber.replaceFirst("0", "")
            }
        }

        var isPrefixContain = false
        if (tempPhoneNumber.contains(AppConstant.PREFIX_NUMBER)) {
            isPrefixContain = true
            tempPhoneNumber =
                tempPhoneNumber.replace(AppConstant.PREFIX_NUMBER, "").replace(SEPRATER, "").trim()
        }
        val firstSepraterIndex = 3
        val secondSepraterIndex = 6
        if (tempPhoneNumber.length > firstSepraterIndex) {
            tempPhoneNumber = tempPhoneNumber.replace(SEPRATER, "").trim()
            var tempNumber = StringBuffer()
            for (i in tempPhoneNumber.indices) {
                if (i == firstSepraterIndex || i == secondSepraterIndex) {
                    tempNumber.append(SEPRATER)
                }
                tempNumber.append(tempPhoneNumber[i])
            }
            tempPhoneNumber = "${
                if (isPrefixContain) {
                    AppConstant.PREFIX_NUMBER
                } else {
                    ""
                }
            }$tempNumber"
        }

        previousLength = tempPhoneNumber.length
        binding.edtPhoneNumberValue.setText(tempPhoneNumber)
        binding.edtPhoneNumberValue.setSelection(binding.edtPhoneNumberValue.text.toString().length)
    }

    private fun checkedSpanable() {
        val span1: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                startActivity(
                    Intent(
                        this@CreateAccountActivity,
                        TermsConditionActivity::class.java
                    ).putExtra("VALUE", 2)
                )
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                ds.color = ContextCompat.getColor(this@CreateAccountActivity, R.color.gray)
            }

        }

        val span2: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                startActivity(
                    Intent(
                        this@CreateAccountActivity,
                        TermsConditionActivity::class.java
                    ).putExtra("VALUE", 1)
                )

            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                ds.color = ContextCompat.getColor(this@CreateAccountActivity, R.color.gray)
            }
        }

        val str =
            SpannableString(getString(R.string.by_signing_up_you_agree_to_our_t_c_and_privacy_policy))
        str.setSpan(span1, 39, str.length, 0)
        str.setSpan(span2, 31, 34, 0)
        binding.cbPrivacyTv.text = str
        binding.cbPrivacyTv.movementMethod = LinkMovementMethod.getInstance()
        binding.btnSignUp.setOnClickListener {

        /*    if (!isValidate()) {
                return@setOnClickListener
            }
            hideKeyboard()*/

           createCaptcha()
        }
    }

    private fun checkNumber() {

    }

    private fun registerUser(captchaToken:String) {
        if (!isOnline()) {
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.internet_connectivity)
            /*showToast(
                  this@CreateAccountActivity,
                  getString(R.string.internet_connectivity),
                  CustomToast.ToastType.NETWORK
              )*/
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClientAuth(this).create(ApiAuthHelper::class.java)
        val name = binding.edtFirstName.text.toString()
        val request = RegisterRequest()
        request.Email = binding.edtEmail.text.toString()
        request.name = name.trim()
        request.FirstName = getName(name, FIRST_NAME)
        request.LastName = binding.edtLastName.text.toString().trim()
        request.UserName = getUserName(binding.edtUserName.text.toString())
        request.PhoneNumber = getUnformatedNumber(binding.edtPhoneNumberValue.text.toString())
        request.Password = binding.edtPassword.text.toString()
        request.userId = userId
        request.deviceTypeId = AppConstant.DEVICE_TYPE_ID
        request.captchaToken=captchaToken
        /* selectedData.countryCode?.let {
             request.countryCode = it
         }*/
        countryCodePicker.selectedCountryCodeWithPlus.let {
            request.countryCode = it
        }
        val call = helper.register(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<RegisterResponse>() {
            override fun onSuccess(res: RegisterResponse?, message: String) {
                hideProcessDailog()
                if (res?.responseStatus != null &&
                    res.responseStatus == 1
                ) {
                    //  verifyOtp(res)
                    CommonUtils.triggerSendOtpEvent(res, request)
                    otpVerificationExecution(res, request)
                }

                if (res?.responseStatus != null &&
                    res.responseStatus == 0
                ) {
                    //  verifyOtp(res)
                    binding.txtError.visibility = View.VISIBLE
                    binding.txtError.text = res.responseMessage
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                if (message.isNotEmpty()) {

                    binding.txtError.visibility = View.VISIBLE
                    binding.txtError.text = message

                    // showToast(this@CreateAccountActivity, message, CustomToast.ToastType.FAILED)
                }
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
                binding.txtError.visibility = View.VISIBLE
                binding.txtError.text = error.errorMessage
                /* showToast(
                     this@CreateAccountActivity,
                     error.errorMessage,
                     CustomToast.ToastType.FAILED
                 )*/
            }

        })
    }

    private fun verifyOtp(res: RegisterResponse) {
        userId = res.userId
        val intentOTP = Intent(this@CreateAccountActivity, OTPActivity::class.java)
        intentOTP.putExtra(AppConstant.EMAIL, binding.edtEmail.text.toString())
        intentOTP.putExtra(AppConstant.ID, res.userId)
        intentOTP.putExtra(AppConstant.REQUEST_ID, res.requestCode)
        /* intentOTP.putExtra(
             AppConstant.IS_FROM_LOGIN,
             intent.getBooleanExtra(AppConstant.IS_FROM_LOGIN, false)
         )*/
        intentOTP.putExtra(
            AppConstant.IS_FROM_LOGIN,
            true
        )
        startActivityForResult(intentOTP, VERIFY_OTP)
    }

    private fun getUserName(userName: String): String {
        var temp = userName
        if (temp.startsWith("@", true)) {
            temp = temp.replaceFirst("@", "", true)
        }
        return temp
    }

    private fun getName(name: String, type: Int): String {
        val temp = StringBuilder()
        val names = name.trim().split(" ")
        if (names.isNotEmpty()) {
            when (type) {
                FIRST_NAME -> {
                    temp.append(names[0].trim())
                }
                LAST_NAME -> {
                    for (i in names.indices) {
                        if (i > 0 && names[i] != null && names[i].isNotEmpty()) {
                            temp.append("${names[i]} ")
                        }
                    }
                }
            }
        }
        return temp.trim().toString()
    }

    private fun isValidate(): Boolean {
        binding.txtError.visibility = View.GONE
        if (binding.edtFirstName.text?.toString()?.length ?: 0 < 3) {
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.please_enter_first_name)

            return false
        }
        if (binding.edtLastName.text?.toString()?.length ?: 0 < 3) {

            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.enter_valid_last_name)

            return false
        }
        if (binding.edtUserName.text?.toString()?.length ?: 0 < 3) {
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.valid_user_name)

            return false
        }
        if (binding.edtEmail.text?.toString()?.length ?: 0 < 3) {

            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.empty_email_address)
            return false
        }
        if (!CommonUtils.isValidEmail(binding.edtEmail.text.toString())) {

            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.valid_email_address)
            return false
        }

        if (binding.edtPhoneNumberValue.text?.toString().isNullOrEmpty()) {

            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.empty_mobile_no)
            return false
        }
        val number = getUnformatedNumber(binding.edtPhoneNumberValue.text.toString())
        if (number.length < 8 || number.length > 12) {
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.valid_phone_no)
            return false
        }

        if (!isPhoneNumberIsValid) {
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.phone_number_not_valid)
            return false
        }

        if (binding.edtPassword.text?.toString().isNullOrEmpty()) {
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.empty_password)
            return false
        }

        if (binding.edtPassword.text.toString().length < 8) {


            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.password_valid)
            return false
        }
        if (binding.edtPassword.text.toString().length > 32) {

            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.password_max_valid)
            return false
        }

        if (!checkPasswordValidation(binding.edtPassword.text.toString().trim())) {
            return false
        }

        if (!binding.cbPrivacy.isChecked) {

            binding.txtError.visibility = View.VISIBLE

            binding.txtError.text = getString(R.string.check_tc_and_privacy_policy)

            /*    showToast(
                    this@CreateAccountActivity,
                    getString(R.string.check_tc_and_privacy_policy),
                    CustomToast.ToastType.FAILED
                )*/
            return false
        }

        return true
    }

    private fun getUnformatedNumber(number: String): String {
        return number.replace(AppConstant.PREFIX_NUMBER, "").replace(SEPRATER, "")

    }

    private fun checkPasswordValidation(password: String): Boolean {
        val spacialCharPatten: Pattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
        val upperCasePatten: Pattern = Pattern.compile("[A-Z ]")
        val lowerCasePatten: Pattern = Pattern.compile("[a-z ]")
        val digitCasePatten: Pattern = Pattern.compile("[0-9 ]")

        if (!spacialCharPatten.matcher(password).find()) {
            /*   showToast(
                   this@CreateAccountActivity,

                   getString(R.string.one_special_character),
                   CustomToast.ToastType.FAILED
               )*/
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.one_special_character)
            return false;
        }
        if (!upperCasePatten.matcher(password).find()) {
            /*showToast(
                    this@CreateAccountActivity,
                    getString(R.string.one_uppercase_letter),
                    CustomToast.ToastType.FAILED
                )*/
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.one_uppercase_letter)
            return false;
        }
        if (!lowerCasePatten.matcher(password).find()) {
            /*      showToast(
                      this@CreateAccountActivity,
                      getString(R.string.one_lowercase_character),
                      CustomToast.ToastType.FAILED
                  )*/
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.one_lowercase_character)
            return false;
        }
        if (!digitCasePatten.matcher(password).find()) {
            /*   showToast(
                   this@CreateAccountActivity,
                   getString(R.string.one_digit_character),
                   CustomToast.ToastType.FAILED
               )*/
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.one_digit_character)
            return false;
        }
        return true
    }

/*
    private fun initalizeBottomSheet() {
        dialog = BottomSheetDialog(this, R.style.Theme_Dialog)
        dialog.setContentView(R.layout.layout_country)
        dialog.setCancelable(true)
        dialog.dismissWithAnimation = true
        dialog.setOnShowListener { dialogInterface ->
            val bottomSheetDialog = dialogInterface as BottomSheetDialog
            CommonUtils.setupFullHeight(this@CreateAccountActivity, bottomSheetDialog)
        }
        //dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        dialog.findViewById(R.id.edtSearchCountry).addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())
            }
        })
        madapter = CountryListAdapter(this@CreateAccountActivity, CountryList, this)
        val layoutManager =
            LinearLayoutManager(this@CreateAccountActivity, LinearLayoutManager.VERTICAL, false)
        dialog.rcv_country.layoutManager = layoutManager
        dialog.rcv_country.adapter = madapter
        dialog.show()
    }
*/

/*
    private fun filter(text: String) {
        val filteredList: ArrayList<ResponseCollection> = ArrayList()
        for (item in CountryList) {
            if (item.countryName?.toLowerCase()!!
                    .contains(text.toLowerCase()) || item.countryCode?.toLowerCase()!!
                    .contains(text.toLowerCase())
            ) {
                filteredList.add(item)
            }
        }
        if (filteredList.isNotEmpty()) {
            dialog.noDataFoundTv.visibility = View.GONE
            dialog.rcv_country.visibility = View.VISIBLE
            madapter.filterList(filteredList)
        } else {
            dialog.noDataFoundTv.visibility = View.VISIBLE
            dialog.noDataFoundTv.tv_message.text = getString(R.string.no_county_found)
            dialog.rcv_country.visibility = View.GONE
        }

    }
*/

    //--> on Country Selected from adapter
    override fun onCountrySelcted(data: ResponseCollection) {
        if (::dialog.isInitialized) {
            dialog.dismiss()
        }
        selectedData = data
        // tv_countryCode.text = data.countryCode
        hideKeyboard()
    }

    //--> Get Data after Sucess
    fun updateList(list: MutableList<ResponseCollection>) {
        val data = list[0]
        //tv_countryCode.text = data.countryCode
        selectedData = data
        CountryList = list
    }

    override fun onResume() {
        super.onResume()
        /* if (!::CountryList.isInitialized) {
             helper.getCountryList(true)
         }*/
    }

    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        if (bundle == null) {
            return
        }
        when (obj) {
            is PNBSheetFragment -> {
                if (!isFinishing) {
                    helper.getCountryList(true)
                }
            }
        }
        dialog.dismiss()
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        if (bundle == null) {
            return
        }
        when (obj) {
            is PNBSheetFragment -> {
                finish()
            }
        }
        dialog.dismiss()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == VERIFY_OTP && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    fun otpVerificationExecution(res: RegisterResponse, request: RegisterRequest) {
        userId = res.userId
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val otpVerificationDialog = OtpVerificationDialog.getInstance(object :
            OtpVerificationDialog.OtpVerificationCodeListener {

            override fun onVerify(otpValue: String, errorTextView: TextView) {

                if (isValidateOTP(otpValue, errorTextView)) {
                    if (!isOnline()) {
                        showToast(
                            this@CreateAccountActivity,
                            getString(R.string.internet_connectivity),
                            CustomToast.ToastType.NETWORK
                        )
                        return
                    }
                    //--> Log Activity
                    showProcessDialog()
                    CommonUtils.triggerVerifyOtp(userId)
                    createAccountApi(res, errorTextView, otpValue)

                }
            }

        }, "${request.countryCode}", "${request.PhoneNumber}", userId)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.add(otpVerificationDialog, "VerificationDialog")
            .commitAllowingStateLoss()



    }

    private fun isValidateOTP(otpValue: String, errorTextView: TextView): Boolean {
        if (otpValue.trim().isEmpty()) {
            errorTextView.visibility = View.VISIBLE
            errorTextView.text = getString(R.string.please_enter_otp)
            return false
        }
        return true
    }


    private fun createAccountApi(res: RegisterResponse, errorTextView: TextView, otpValue: String) {
        val helper = ApiClient.getClientAuth().create(ApiAuthHelper::class.java)
        val request = VerifyOtpCreateAccount()
        request.userId = res!!.userId ?: ""
        //request.requestCode = res.requestCode ?: ""
        request.requestCode = res.requestCode ?: ""
        request.phoneConfirmationCode = otpValue.trim()
        val call = helper.validateOtpCreateAccount(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<SignupResponse>() {
            override fun onSuccess(any: SignupResponse?, message: String) {
                val res = any as SignupResponse
                hideProcessDailog()
                if (res.responseStatus == 1) {
                    CommonUtils.triggerSignUpEvent(res)

                    res.response?.token?.accessToken.let {
                        res.response.token?.accessToken?.let { it1 ->
                            PrefUtils.saveValueInPreference(
                                this@CreateAccountActivity,
                                PrefUtils.TOKEN,
                                it1
                            )
                        }
                    }
                    res.response.userInfo?.sub?.let {
                        PrefUtils.saveValueInPreference(
                            this@CreateAccountActivity,
                            PrefUtils.USER_ID,
                            it
                        )
                    }
                    res.response.userInfo?.givenName?.let {
                        PrefUtils.saveValueInPreference(
                            this@CreateAccountActivity,
                            PrefUtils.NAME,
                            it
                        )
                    }
                    res.response.userInfo?.email?.let {
                        PrefUtils.saveValueInPreference(
                            this@CreateAccountActivity,
                            PrefUtils.EMAIL,
                            it
                        )
                    }
                    /* PrefUtils.saveValueInPreference(
                         this@OTPActivity,
                         PrefUtils.PHONE_NO,
                         "${request.countryCode} -${request.PhoneNumber}"
                     )*/

                    res.response.token?.tokenType?.let {
                        PrefUtils.saveValueInPreference(
                            this@CreateAccountActivity,
                            PrefUtils.TOKEN_TYPE,
                            it
                        )
                    }

                    res.response.token?.expiresIn?.let {
                        PrefUtils.saveValueInPreference(
                            this@CreateAccountActivity,
                            PrefUtils.TOKEN_TIME,
                            it
                        )
                    }
                    updateDeviceToken()
                    startActivity(
                        Intent(
                            this@CreateAccountActivity,
                            WelcomeNebulaScreen::class.java
                        )
                    )
                    finish()


                } else {
                    errorTextView.visibility = View.VISIBLE

                    errorTextView.text = res.responseMessage
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                errorTextView.visibility = View.VISIBLE
                errorTextView.text = message
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun updateDeviceToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result!!.token
                Log.d("Token", token)
                val updateDeviceInfoReq = UpdateDeviceInfoReq()
                updateDeviceInfoReq.deviceNotificationID = token
                val helper =
                    ApiClient.getClientMusic(this).create(ApiAuthHelper::class.java)
                helper.saveDeviceInfo(updateDeviceInfoReq)
                    .enqueue(object : CallBackManager<BaseResponse>() {
                        override fun onSuccess(any: BaseResponse?, message: String) {
                        }

                        override fun onFailure(message: String) {
                        }

                        override fun onError(error: RetroError) {
                        }

                    })
            })
    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }



    fun createCaptcha() {
        SafetyNet.getClient(this).verifyWithRecaptcha(AppConstant.SITE_KEY)
            .addOnSuccessListener(this) { response ->
                if (!response.tokenResult?.isEmpty()!!) {
                    response.tokenResult?.let { Log.d("Token", it) }
                    registerUser(response.tokenResult.toString())
                }
            }
            .addOnFailureListener(this) { e ->
                if (e is ApiException) {

                    Log.d("Error message:", CommonStatusCodes.getStatusCodeString(e.statusCode))

                } else {
                    Log.d("Unknown error message:", e.message.toString())
                }
            }
    }


}

