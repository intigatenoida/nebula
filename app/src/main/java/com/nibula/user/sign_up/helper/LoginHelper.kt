package com.nibula.user.sign_up.helper



import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.customview.CustomToast
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.user.login.LoginActivity
import com.nibula.user.sign_up.modal.CounytryModal

class LoginHelper(val context: LoginActivity) : BaseHelperFragment() {

    //Get Country Listing From API
    fun getCountryList(isProgressDialog: Boolean) {
        if (!context.isOnline()) {
            context.showToast(
                context,
                context.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        if(isProgressDialog){
            context.showProcessDialog()
        }
        val helper = ApiClient.getClientAuth().create(ApiAuthHelper::class.java)
        val observable = helper.getCountryList()
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<CounytryModal>() {
            override fun onSuccess(any: CounytryModal?, message: String?) {
                any?.let {
                    context.hideProcessDailog()
                    if (it.responseStatus == 1) {
                        it.responseCollection?.let { list ->
                            if (list.isNotEmpty()) {
                                context.updateList(list)
                            }
                        }
                    } else {
                        it.responseMessage?.let { it1 ->
                            showBottomPrompt(context.getString(R.string.message_something_wrong_1))
                        }
                    }
                }
            }

            override fun onError(error: RetroError?) {
                error?.errorMessage?.let {
                    showBottomPrompt(context.getString(R.string.message_something_wrong_1))
                }
            }
        }))
    }

    fun showBottomPrompt(descriptions: String){
        val bPrompt = PNBSheetFragment(
            descriptions,
            context.getString(R.string.retry),
            context.getString(R.string.cancel),
            context!!
        )
        bPrompt.isCancelable = false
        bPrompt.setColor(
            ContextCompat.getColor(context, R.color.colorAccent),
            ContextCompat.getColor(context, R.color.white),
            ContextCompat.getColor(context, R.color.gray_1_51per),
            ContextCompat.getColor(context, R.color.gray_1_51per)
        )
        bPrompt.show(
            context.supportFragmentManager,
            "PositiveNegativeBottomSheetFragment"
        )
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}