package com.nibula.user.sign_up.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResponseCollection(
    @SerializedName("CountryCode")
    var countryCode: String? = null,
    @SerializedName("CountryName")
    var countryName: String? = null,
    @SerializedName("CountryShortName")
    var countryShortName: String? = null,
    @SerializedName("Id")
    var id: Int? = null
)