package com.nibula.user.sign_up.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemCountryBinding
import com.nibula.user.sign_up.modal.ResponseCollection
import java.util.*

class CountryListAdapter(
    val context: Context,
    val mcountryList: MutableList<ResponseCollection>,
    val communicator: Communicator
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    class CountryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private var countryList: MutableList<ResponseCollection> = mcountryList
lateinit var binding:ItemCountryBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
      /*  val view =
            LayoutInflater.from(context).inflate(R.layout.item_country, parent, false);
        return CountryViewHolder(view)*/
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemCountryBinding.inflate(inflater, parent, false)
        val holder = CountryViewHolder(binding.root)
        return holder
    }

    override fun getItemCount() = countryList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CountryViewHolder) {
            val data = countryList[position]
            data.countryName?.let { longName ->
                binding.tvCountryName.text = longName
                data.countryShortName?.let {
                    binding.tvCountryName.text = "$longName ($it)"
                }
            }
            data.countryCode?.let {
                binding.tvCountryCode.text = it
            }

            holder.itemView.setOnClickListener {
                communicator.onCountrySelcted(data)
            }
        }
    }

    fun filterList(filteredList: ArrayList<ResponseCollection>) {
        countryList = filteredList
        notifyDataSetChanged()
    }

    interface Communicator {
        fun onCountrySelcted(data: ResponseCollection)
    }
}