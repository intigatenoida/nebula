package com.nibula.user.sign_up.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class CounytryModal(
    @SerializedName("ResponseCollection")
    var responseCollection: MutableList<ResponseCollection>? = null
):BaseResponse()