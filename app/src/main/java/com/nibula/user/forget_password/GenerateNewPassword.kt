package com.nibula.user.forget_password

import android.app.Activity
import android.os.Bundle
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.customview.CustomToast
import com.nibula.databinding.ActivityNewPasswordBinding
import com.nibula.request.ResetPasswordRequest
import com.nibula.response.BaseResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.AppConstant
import java.util.regex.Pattern


class GenerateNewPassword : BaseActivity() {
    lateinit var binding:ActivityNewPasswordBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_new_password)
        binding= ActivityNewPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnLogin.setOnClickListener {
            if (checkPasswordValidation(
                    binding.edtPass.text.toString(),
                    binding.edtPasswordConfirm.text.toString()
                )
            ) resetPassword()
        }
    }


    private fun checkPasswordValidation(password: String, confirmPassword: String): Boolean {

        if (password.trim().length < 8) {
            showToast(this, "Password must be at least 8 character", CustomToast.ToastType.FAILED)
            return false;
        }
        val spacialCharPatten: Pattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
        val upperCasePatten: Pattern = Pattern.compile("[A-Z ]")
        val lowerCasePatten: Pattern = Pattern.compile("[a-z ]")
        val digitCasePatten: Pattern = Pattern.compile("[0-9 ]")

        if (!spacialCharPatten.matcher(password).find()) {
            showToast(
                this@GenerateNewPassword,
                "Password must have at least one special character",
                CustomToast.ToastType.FAILED
            )
            return false;
        }
        if (!upperCasePatten.matcher(password).find()) {
            showToast(
                this@GenerateNewPassword,
                "Password must have at least one uppercase letter",
                CustomToast.ToastType.FAILED
            )
            return false;
        }
        if (!lowerCasePatten.matcher(password).find()) {
            showToast(
                this@GenerateNewPassword,
                "Password must have at least one lowercase letter",
                CustomToast.ToastType.FAILED
            )
            return false;
        }
        if (!digitCasePatten.matcher(password).find()) {
            showToast(
                this@GenerateNewPassword,
                "Password must have at least one digit character",
                CustomToast.ToastType.FAILED
            )
            return false;
        }

        if (confirmPassword.trim().length < 8) {
            showToast(
                this@GenerateNewPassword,
                "Password must be at least 8 character",
                CustomToast.ToastType.FAILED
            )
            return false;
        }

        if (!spacialCharPatten.matcher(confirmPassword).find()) {
            showToast(
                this@GenerateNewPassword,
                "Confirm Passwords must have at least one special character.",
                CustomToast.ToastType.FAILED
            )
            return false;
        }
        if (!upperCasePatten.matcher(confirmPassword).find()) {
            showToast(
                this@GenerateNewPassword,
                "Confirm Password must have at least one uppercase character",
                CustomToast.ToastType.FAILED
            )
            return false;
        }
        if (!lowerCasePatten.matcher(confirmPassword).find()) {
            showToast(
                this@GenerateNewPassword,
                "Confirm Password must have at least one lowercase character",
                CustomToast.ToastType.FAILED
            )
            return false;
        }
        if (!digitCasePatten.matcher(confirmPassword).find()) {
            showToast(
                this@GenerateNewPassword,
                "Confirm Password must have at least one digit character !!",
                CustomToast.ToastType.FAILED
            )
            return false;
        }

        if (password != confirmPassword) {
            showToast(
                this@GenerateNewPassword,
                "New Password and Confirm Password should be same",
                CustomToast.ToastType.FAILED
            )
            return false
        }
        return true

    }


    private fun resetPassword() {
        if (!isOnline()) {
            showToast(
                this,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        bindLog(this.localClassName)

        showProcessDialog()
        val helper = ApiClient.getClientAuth(this).create(ApiAuthHelper::class.java)
        val request = ResetPasswordRequest()

        if (intent.getStringExtra(AppConstant.EMAIL)!!.isEmpty()) {
            request.counntryCode = intent.getStringExtra(AppConstant.COUNTRYCODE)!!
            request.mobileNumber = intent.getStringExtra(AppConstant.PHONENUMBER)!!
            request.token = intent.getStringExtra(AppConstant.ID) ?: ""
            request.password = binding.edtPasswordConfirm.text.toString().trim()
        } else {
            request.token = intent.getStringExtra(AppConstant.ID) ?: ""
            request.password = binding.edtPasswordConfirm.text.toString().trim()
            request.emailId = intent.getStringExtra(AppConstant.EMAIL) ?: ""

        }


        val call = helper.resetPassword(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                val response = any as BaseResponse
                hideProcessDailog()
                showToast(
                    this@GenerateNewPassword,
                    response.responseMessage ?: "",
                    CustomToast.ToastType.SUCCESS
                )
                if (response.responseStatus == 1) {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                showToast(this@GenerateNewPassword, message, CustomToast.ToastType.FAILED)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }
}