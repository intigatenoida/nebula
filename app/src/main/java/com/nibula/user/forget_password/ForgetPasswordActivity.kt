package com.nibula.user.forget_password

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.*
import android.text.style.UnderlineSpan
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hbb20.CountryCodePicker
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.databinding.ActivityForgetPasswordBinding
import com.nibula.request.ForgetPasswordRequest
import com.nibula.response.BaseResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.user.sign_up.adapter.CountryListAdapter
import com.nibula.user.sign_up.helper.ForgetPasswordHelper
import com.nibula.user.sign_up.modal.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.interfaces.DialogFragmentClicks

import java.util.ArrayList


class ForgetPasswordActivity : BaseActivity(),
    DialogFragmentClicks {
    var isPhoneNumberIsValid: Boolean = false

    private val requestCodeOTP = 1000
    lateinit var CountryList: MutableList<ResponseCollection>
    lateinit var madapter: CountryListAdapter
    lateinit var dialog: BottomSheetDialog
    lateinit var selectedData: ResponseCollection
    lateinit var helper: ForgetPasswordHelper
    lateinit var countryCodePicker: CountryCodePicker
    lateinit var binding:ActivityForgetPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_forget_password)
        binding= ActivityForgetPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        checkPhoneNumberAndCountryCode()


        binding.edtPhoneNumberValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString().length == 1 && s.toString().startsWith("0")) {
                    s.clear()
                }
            }
        })
        helper =
            ForgetPasswordHelper(this@ForgetPasswordActivity)
        val ss1 = SpannableString(getString(R.string.click_here_to_login))
        ss1.setSpan(UnderlineSpan(), 0, ss1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.txtLogin.text = ss1
        binding.txtLogin.setOnClickListener {
            /* val intent = Intent(this, LoginActivity::class.java)
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or  Intent.FLAG_ACTIVITY_NEW_TASK
             startActivity(intent)*/
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        binding.btnResetPass.setOnClickListener {
            if (isValidate()) {
                generateOtp()
            }
        }

        binding.edtPhoneNumber.setOnClickListener {
            if (!::CountryList.isInitialized ||
                CountryList.isEmpty()
            ) {

                binding.txtError.visibility = View.VISIBLE
                binding.txtError.text = getString(R.string.country_list_error)
                /*
                  showToast(
                      this@CreateAccountActivity,
                      getString(R.string.country_list_error),
                      CustomToast.ToastType.FAILED
                  )*/
                return@setOnClickListener
            }

            //initalizeBottomSheet()
        }
    }

    private fun isValidate(): Boolean {

        if (binding.edtPhoneNumber.visibility == View.INVISIBLE) {
            if (TextUtils.isEmpty(binding.edtEmail.text.toString())) {

                binding.txtError.visibility = View.VISIBLE

                binding.txtError.text = getString(R.string.email_empty)
                /*  showToast(
                      this@ForgetPasswordActivity,
                      getString(R.string.email_empty),
                      CustomToast.ToastType.FAILED
                  )*/
                return false
            }
            if (!CommonUtils.isValidEmail(binding.edtEmail.text.toString())) {
                binding.txtError.visibility = View.VISIBLE

                binding.txtError.text = getString(R.string.valid_email)

                // showToast(this, getString(R.string.valid_email), CustomToast.ToastType.FAILED)
                return false
            }
        } else {
            if (TextUtils.isEmpty(binding.edtPhoneNumberValue.text.toString())) {

                binding.txtError.visibility = View.VISIBLE

                binding.txtError.text = getString(R.string.empty_mobile_no)

                return false
            } else if (binding.edtPhoneNumberValue.text.toString().length < 8 || binding.edtPhoneNumberValue.text.toString().length > 12) {
                binding.txtError.visibility = View.VISIBLE

                binding.txtError.text = getString(R.string.valid_phone_no)
                return false

            } else if (!isPhoneNumberIsValid) {
                binding.txtError.visibility = View.VISIBLE
                binding.txtError.text = getString(R.string.phone_number_not_valid)
                return false
            } else {

            }
        }
        return true
    }

    private fun generateOtp() {
        if (!isOnline()) {

            binding.txtError.visibility = View.VISIBLE

            binding.txtError.text = getString(R.string.internet_connectivity)

            /* showToast(
                 this,
                 getString(R.string.internet_connectivity),
                 CustomToast.ToastType.NETWORK
             )*/
            return
        }
        //--> Log Activity
        bindLog(this.localClassName)

        showProcessDialog()
        val helper = ApiClient.getClientAuth(this).create(ApiAuthHelper::class.java)
        val request = ForgetPasswordRequest()
        if (binding.edtPhoneNumber.isVisible) {
            request.mobileNumber = binding.edtPhoneNumberValue.text.toString()
            //request.counntryCode = selectedData.countryCode!!
            request.counntryCode = countryCodePicker?.selectedCountryCodeWithPlus!!
        } else {
            request.emailId = binding.edtEmail.text.toString().trim()
        }

        val call = helper.forgetPassword(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                val response = any as BaseResponse
                if (response?.responseStatus == 1) {
                    val intent = Intent(this@ForgetPasswordActivity, OTPActivity::class.java)
                    intent.putExtra(AppConstant.EMAIL, request.emailId)
                    intent.putExtra(AppConstant.PHONENUMBER, request.mobileNumber)
                    //intent.putExtra(AppConstant.COUNTRYCODE, request.counntryCode)
                    intent.putExtra(AppConstant.COUNTRYCODE, countryCodePicker?.selectedCountryCodeWithPlus)
                    startActivityForResult(intent, requestCodeOTP)
                } else if (response?.responseStatus == 4) {
                    binding.edtPhoneNumber.visibility = View.VISIBLE
                    binding.edtEmail.visibility = View.INVISIBLE
                    binding.txtError.visibility = View.VISIBLE
                    binding.txtError.text = response?.responseMessage ?: ""
                } else {
                    binding.txtError.visibility = View.VISIBLE
                    binding.txtError.text = response?.responseMessage ?: ""
/*
                    showToast(
                        this@ForgetPasswordActivity,
                        response?.responseMessage ?: "",
                        CustomToast.ToastType.FAILED
                    )*/
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()

                binding.txtError.visibility = View.VISIBLE

                binding.txtError.text = message

                // showToast(this@ForgetPasswordActivity, message, CustomToast.ToastType.FAILED)
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }


    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestCodeOTP && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }


    private fun getUnformatedNumber(number: String): String {
        return number.replace(AppConstant.PREFIX_NUMBER, "").replace(AppConstant.SEPRATER, "")

    }

    //--> on Country Selected from adapter
/*
    override fun onCountrySelcted(data: ResponseCollection) {
        if (::dialog.isInitialized) {
            dialog.dismiss()
        }
        selectedData = data
        //tv_countryCode.text = data.countryCode
        hideKeyboard()
    }

    //--> Get Data after Sucess
    fun updateList(list: MutableList<ResponseCollection>) {
        val data = list[0]
       // tv_countryCode.text = data.countryCode
        selectedData = data
        CountryList = list
    }
*/

    /* override fun onResume() {
         super.onResume()
         if (!::CountryList.isInitialized) {
             helper.getCountryList(true)
         }
     }*/

  /*  override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        if (bundle == null) {
            return
        }
        when (obj) {
            is PNBSheetFragment -> {
                if (!isFinishing) {
                    helper.getCountryList(true)
                }
            }
        }
        dialog.dismiss()
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        if (bundle == null) {
            return
        }
        when (obj) {
            is PNBSheetFragment -> {
                finish()
            }
        }
        dialog.dismiss()
    }*/


/*
    private fun initalizeBottomSheet() {
        dialog = BottomSheetDialog(this, R.style.Theme_Dialog)
        dialog.setContentView(R.layout.layout_country)
        dialog.setCancelable(true)
        dialog.dismissWithAnimation = true
        dialog.setOnShowListener { dialogInterface ->
            val bottomSheetDialog = dialogInterface as BottomSheetDialog
            CommonUtils.setupFullHeight(this@ForgetPasswordActivity, bottomSheetDialog)
        }
        //dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        dialog.edtSearchCountry.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {

            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())
            }
        })
        */
/*  madapter = CountryListAdapter(this@ForgetPasswordActivity, CountryList, this)
          val layoutManager =
              LinearLayoutManager(this@ForgetPasswordActivity, LinearLayoutManager.VERTICAL, false)
          dialog.rcv_country.layoutManager = layoutManager
          dialog.rcv_country.adapter = madapter
          dialog.show()*//*

    }
*/

/*
    private fun filter(text: String) {
        val filteredList: ArrayList<ResponseCollection> = ArrayList()
        for (item in CountryList) {
            if (item.countryName?.toLowerCase()!!
                    .contains(text.toLowerCase()) || item.countryCode?.toLowerCase()!!
                    .contains(text.toLowerCase())
            ) {
                filteredList.add(item)
            }
        }
        if (filteredList.isNotEmpty()) {
            dialog.noDataFoundTv.visibility = View.GONE
            dialog.rcv_country.visibility = View.VISIBLE
            madapter.filterList(filteredList)
        } else {
            dialog.noDataFoundTv.visibility = View.VISIBLE
            dialog.noDataFoundTv.tv_message.text = getString(R.string.no_county_found)
            dialog.rcv_country.visibility = View.GONE
        }

    }
*/

    fun checkPhoneNumberAndCountryCode() {
        countryCodePicker = findViewById(R.id.ccpLogin)
        countryCodePicker?.setOnCountryChangeListener {

            val selectedCountryCode = countryCodePicker?.selectedCountryCode
        }
        countryCodePicker.setPhoneNumberValidityChangeListener {
            (it).also { isPhoneNumberIsValid = it }

        }
        countryCodePicker.registerCarrierNumberEditText(binding.edtPhoneNumberValue)
    }

    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
    }
}