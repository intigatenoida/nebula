package com.nibula.user.forget_password

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.customview.CustomToast
import com.nibula.dashboard.DashboardActivity
import com.nibula.databinding.ActivityOtpBinding
import com.nibula.request.EmailValidationRequest
import com.nibula.request.ForgetPasswordRequest
import com.nibula.request.VerifyOtpCreateAccount
import com.nibula.response.BaseResponse
import com.nibula.response.VerifyOtpResponse
import com.nibula.response.signup.SignupResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.user.welcome.WelcomeNebulaScreen
import com.nibula.utils.AppConstant
import com.nibula.utils.PrefUtils


class OTPActivity : BaseActivity() {
    var email: String? = null
    var phoneNumber: String? = null
    var countryCode: String? = null
    var userID: String? = null
    var requestId: String? = null
    var fromSignup = false
    var countDownTimer: CountDownTimer? = null
    val totalTime: Long = 60 * 1000
    val timeInterval: Long = 1000
    lateinit var binding:ActivityOtpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_otp)
        binding=ActivityOtpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        email = intent.getStringExtra(AppConstant.EMAIL)
        phoneNumber = intent.getStringExtra(AppConstant.PHONENUMBER)
        countryCode = intent.getStringExtra(AppConstant.COUNTRYCODE)
        userID = intent.getStringExtra(AppConstant.ID)
        requestId = intent.getStringExtra(AppConstant.REQUEST_ID)
        fromSignup = intent.getBooleanExtra(AppConstant.IS_FROM_LOGIN, false)
        binding.btnLogin.setOnClickListener {
            if (isValidate()) {
                binding.errorMsg.text = ""
                validateOTP()
            }
        }
        binding.resendOtp.setOnClickListener {
            binding.errorMsg.text = ""
            resendOTP()
        }
        binding.backArrow.setOnClickListener { onBackPressed() }
        setSnappableEmailText()
        setSnappableTimerText()
        if (fromSignup) {
            binding.txtTitle2.text = getString(R.string.msg_create_account_otp)
        } else {
            binding.txtTitle2.text = getString(R.string.msg_forget_password_otp)
        }
    }

    private fun startCountDownTimer() {
        binding.resendOtp.visibility = View.VISIBLE
        countDownTimer?.cancel()
        countDownTimer = object : CountDownTimer(totalTime, timeInterval) {
            override fun onFinish() {
                setSnappableTimerText()
                binding.resendOtp.visibility = View.INVISIBLE
            }
            override fun onTick(millisUntilFinished: Long) {
                val minute = (millisUntilFinished / 1000) / 60
                val seconds = (millisUntilFinished / 1000) % 60
                binding.txtResendOtp.text = "${getString(R.string.resend_code_in)} $minute:$seconds"
            }
        }.start()
    }

    private fun setSnappableTimerText() {
        val str = getString(R.string.did_not_get_the_otp_resend_otp)
        val spannableString =
            SpannableStringBuilder(str)

        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                resendOTP()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
                ds.color = ContextCompat.getColor(this@OTPActivity, R.color.colorAccent)
            }
        }

        spannableString.setSpan(
            clickableSpan,
            str.indexOf("?") + 2,
            spannableString.length,
            SpannableString.SPAN_INCLUSIVE_INCLUSIVE
        )
        binding.txtResendOtp.text = spannableString
        binding.txtResendOtp.movementMethod = LinkMovementMethod.getInstance()
    }


    private fun setSnappableEmailText() {
        val index = binding.txtTitle.text.toString().indexOf("#")
        if (email!!.isEmpty()) {
            val str = binding.txtTitle.text.toString().replace("#", "${countryCode}-${phoneNumber}")
            binding.txtTitle.text = str
        } else {
            val str = binding.txtTitle.text.toString().replace("#", email!!)
            binding.txtTitle.text = str
        }
//        val spannableString =
//            SpannableStringBuilder(str)
//
//        val clickableSpan: ClickableSpan = object : ClickableSpan() {
//            override fun onClick(textView: View) {
//                finish()
//            }
//
//            override fun updateDrawState(ds: TextPaint) {
//                super.updateDrawState(ds)
//                ds.isUnderlineText = true
//                ds.color = ContextCompat.getColor(this@OTPActivity, R.color.colorAccent)
//            }
//        }
//
//        spannableString.setSpan(
//            clickableSpan,
//            (index + 1 + (email?.length ?: 0)),
//            spannableString.length,
//            SpannableString.SPAN_INCLUSIVE_INCLUSIVE
//        )
//        txtTitle.text = spannableString
//        txtTitle.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun validateOTP() {
        if (!isOnline()) {
            showToast(
                this,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        //--> Log Activity
        bindLog(this.localClassName)

        showProcessDialog()
        if (fromSignup) {
            createAccountApi()
        } else {
            forgetPasswordApi()
        }

    }

    private fun createAccountApi() {
        val helper = ApiClient.getClientAuth().create(ApiAuthHelper::class.java)
        val request = VerifyOtpCreateAccount()
        request.userId = userID ?: ""
        request.requestCode = requestId ?: ""
        request.phoneConfirmationCode = binding.edtOTP.text.toString().trim()
        val call = helper.validateOtpCreateAccount(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<SignupResponse>() {
            override fun onSuccess(any: SignupResponse?, message: String) {
                val res = any as SignupResponse
                hideProcessDailog()
                if (res.responseStatus == 1) {


                    PrefUtils.saveValueInPreference(
                        this@OTPActivity,
                        PrefUtils.TOKEN,
                        res.response.token.accessToken
                    )

                    PrefUtils.saveValueInPreference(
                        this@OTPActivity,
                        PrefUtils.USER_ID,
                        res.response.userInfo.sub
                    )
                    PrefUtils.saveValueInPreference(
                        this@OTPActivity,
                        PrefUtils.NAME,
                        res.response.userInfo.givenName
                    )
                    PrefUtils.saveValueInPreference(
                        this@OTPActivity,
                        PrefUtils.EMAIL,
                        res.response.userInfo.email
                    )
                    /* PrefUtils.saveValueInPreference(
                         this@OTPActivity,
                         PrefUtils.PHONE_NO,
                         "${request.countryCode} -${request.PhoneNumber}"
                     )*/

                    PrefUtils.saveValueInPreference(
                        this@OTPActivity,
                        PrefUtils.TOKEN_TYPE,
                        res.response.token.tokenType
                    )

                    PrefUtils.saveValueInPreference(
                        this@OTPActivity,
                        PrefUtils.TOKEN_TIME,
                        res.response.token.expiresIn
                    )
                    if (fromSignup) {
                        /* setResult(Activity.RESULT_OK)
                         finish()*/
                        startActivity(
                            Intent(
                                this@OTPActivity,
                                WelcomeNebulaScreen::class.java
                            )
                        )
                        finish()
                    } else {
                        startActivity(
                            Intent(
                                this@OTPActivity,
                                DashboardActivity::class.java
                            )
                        )
                        finish()
                    }
                } else {
                    binding.errorMsg.text = res.responseMessage
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                binding.errorMsg.text = message
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun forgetPasswordApi() {
        val helper = ApiClient.getClientAuth(this).create(ApiAuthHelper::class.java)
        val request = EmailValidationRequest()
        if (email!!.isEmpty()) {
            request.mobileNumber = phoneNumber!!
            request.counntryCode = countryCode!!
            request.otp = binding.edtOTP.text.toString().trim()

        } else {
            request.emailId = email!!
            request.otp = binding.edtOTP.text.toString().trim()
        }

        val call = helper.validateOtp(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<VerifyOtpResponse>() {
            override fun onSuccess(any: VerifyOtpResponse?, message: String) {
                val response = any as VerifyOtpResponse
                hideProcessDailog()
                if (response.responseStatus == 1) {
                    val intent = Intent(this@OTPActivity, GenerateNewPassword::class.java)
                    intent.putExtra(AppConstant.EMAIL, request.emailId)
                    intent.putExtra(AppConstant.PHONENUMBER, phoneNumber)
                    intent.putExtra(AppConstant.COUNTRYCODE, countryCode)
                    intent.putExtra(AppConstant.ID, response.token)
                    startActivityForResult(intent, 1000)
                } else {
                    binding.errorMsg.text = response.responseMessage
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                binding.errorMsg.text = message
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }


    private fun isValidate(): Boolean {
        if (binding.edtOTP.text.toString().trim().isEmpty()) {
            binding.errorMsg.text = getString(R.string.please_enter_otp)
            return false
        }
        return true
    }


    private fun resendOTP() {
        if (!isOnline()) {
            showToast(
                this,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        //--> Log Activity
        if (fromSignup) {
            resendOTPCreateAccount()
        } else {
            resendOTPForgetPassword()
        }
    }

    private fun resendOTPCreateAccount() {

        showProcessDialog()
        val helper = ApiClient.getClientAuth(this).create(ApiAuthHelper::class.java)
        val request = EmailValidationRequest()
        request.emailId = email!!
        request.otp = binding.edtOTP.text.toString().trim()

        val call = helper.resendMobileVerificationOtp(userID!!)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                val response = any as BaseResponse
                hideProcessDailog()
                if (response.responseStatus == 1) {
                    response.responseMessage?.let {
                        showToast(
                            this@OTPActivity,
                            it,
                            CustomToast.ToastType.SUCCESS
                        )
                    }
//                    val intent = Intent(this@OTPActivity, GenerateNewPassword::class.java)
//                    intent.putExtra(AppConstant.EMAIL, request.emailId)
//                    intent.putExtra(AppConstant.ID, response.token)
//                    startActivityForResult(intent, 1000)
                } else {
                    binding.errorMsg.text = response.responseMessage
                }
                hideProcessDailog()
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                binding.errorMsg.text = message
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    private fun resendOTPForgetPassword() {
        bindLog(this.localClassName)

        showProcessDialog()
        val helper = ApiClient.getClientAuth(this).create(ApiAuthHelper::class.java)
        val request = ForgetPasswordRequest()
        if (email!!.isEmpty()) {
            request.mobileNumber = phoneNumber!!
            request.counntryCode = countryCode!!
        } else {
            request.emailId = email!!

        }

        val call = helper.resendOtp(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                hideProcessDailog()
                val response = any as BaseResponse
                if (response.responseStatus == 1) {
//                    startCountDownTimer()
                    showToast(
                        this@OTPActivity,
                        response.responseMessage ?: "",
                        CustomToast.ToastType.SUCCESS
                    )
                } else {
                    binding.errorMsg.text = response.responseMessage ?: ""
                }
            }

            override fun onFailure(message: String) {
                hideProcessDailog()
                binding.errorMsg.text = message
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }

        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
        finish()
    }


    override fun onDestroy() {
        countDownTimer?.cancel()
        super.onDestroy()
    }
}