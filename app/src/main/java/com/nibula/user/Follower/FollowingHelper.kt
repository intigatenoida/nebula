package com.nibula.user.Follower

import android.view.View
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.customview.CustomToast
import com.nibula.response.followerResponse.FollowerResponse
import com.nibula.response.followerResponse.User
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError

class FollowingHelper(val fg: FollowingFragment) : BaseHelperFragment() {

    var investmentNextPage: Int = 1
    var investmentMaxPage: Int = 0
    fun getFollower(clearData: Boolean) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            dismiss()
            return
        }

        if (!fg.binding.followerRefresh.isRefreshing
        ) {
//            fg.showProcessDialog()
        }
        fg.isLoading = true

        val helper =
            ApiClient.getClientAuth(fg.requireContext()).create(ApiAuthHelper::class.java)
        val call = helper.getFollowerFollowing(
            investmentNextPage,
            2,
            fg.binding.edSearchFollowers.text.toString(),fg.artistId
        )
        call.enqueue(object : CallBackManager<FollowerResponse>() {
            override fun onSuccess(any: FollowerResponse?, message: String) {
                /*Load More*/
                //fg.hideKeyboard()

                fg.updateTotalCount(any?.totalRecords ?: 0)
                investmentMaxPage = ((any?.totalRecords ?: 0) / 10.0).toInt()
                fg.isDataAvailable = investmentNextPage < investmentMaxPage ||
                        (any?.totalRecords ?: 0) > (investmentNextPage * 10)

//                 Data
                if (any != null &&
                    any.responseStatus == 1
                ) {
                    if (!any.users.isNullOrEmpty()) {
                        fg.binding.rcyFollowers.visibility = View.VISIBLE
                        if (clearData) {
                            fg.run { followerAdapter.setData(any.users as ArrayList<User>) }
                        } else if (investmentNextPage > 1) {
                            with(fg) { followerAdapter.addData(any.users as ArrayList<User>) }
                        }
                        investmentNextPage++

                    } else {
                        if (investmentNextPage == 1) {
                            fg.followerAdapter.clearData()
                        }
                    }
                } else {
                    if (investmentNextPage == 1) {
                        fg.followerAdapter.clearData()
                    }
                }

                dismiss()
            }

            override fun onFailure(message: String) {
                dismiss()
            }

            override fun onError(error: RetroError) {
                dismiss()
            }

        })
    }

    private fun dismiss() {
        fg.isLoading = false
        if (fg.binding.followerRefresh.isRefreshing) {
            fg.binding.followerRefresh.isRefreshing = false
        } else {
            fg.hideProcessDialog()
        }

        noData()
    }

    private fun noData() {
        if (fg.followerAdapter.dataList.isNullOrEmpty()) {
            fg.binding.noDataFoundTv.visibility = View.VISIBLE
            fg.binding.noDataFoundTv.text=fg.requireContext().getString(R.string.no_following_found)
            fg.binding.rcyFollowers.visibility = View.GONE
        } else {
            fg.binding.noDataFoundTv.visibility = View.GONE
            fg.binding.rcyFollowers.visibility = View.VISIBLE
        }
    }
}