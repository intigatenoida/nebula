package com.nibula.user.Follower.adapter
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.databinding.ItemFollowersBinding
import com.nibula.response.followerResponse.User
import com.nibula.utils.CommonUtils
class FollowerAdapter(
    val context: Context,
    val followtext: String,
    val listener: openProfileDetailListener
) :
    RecyclerView.Adapter<FollowerAdapter.ViewHolder>() {

    interface openProfileDetailListener {
        fun openProfile(userId: String, type: Int)
    }

    val dataList = ArrayList<User>()
    lateinit var binding:ItemFollowersBinding

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemFollowersBinding.inflate(inflater, parent, false)
        val holder = ViewHolder(binding.root)
        return holder

    }
       /* LayoutInflater.from(context).inflate(
            R.layout.item_followers, parent, false
        )*/

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CommonUtils.loadImage(context, dataList[position].userImage, binding.userImage)
      binding.txtName.text = dataList[position].name
        binding.txtType.text = followtext
        binding.followContainer.setOnClickListener {
            listener.openProfile(dataList[position].userId ?: "", 4)//user Type Id 4 for artist
        }

    }

    fun addData(list: ArrayList<User>) {
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    fun setData(list: ArrayList<User>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    fun clearData() {
        dataList.clear()
        notifyDataSetChanged()
    }

}