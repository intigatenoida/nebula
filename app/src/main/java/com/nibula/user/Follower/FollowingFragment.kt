package com.nibula.user.Follower

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentFollowerBinding
import com.nibula.user.Follower.adapter.FollowerAdapter
import com.nibula.utils.AppConstant


class FollowingFragment: BaseFragment(), FollowerAdapter.openProfileDetailListener {

    lateinit var followerAdapter: FollowerAdapter
    lateinit var rootView: View
    var isDataAvailable = false
    var isLoading = false
    lateinit var helper:FollowingHelper
    var artistId=""
    lateinit var binding:FragmentFollowerBinding
    companion object {
//        private lateinit var instance: FollowingFragment

        fun getInstance(artistId: String)=FollowingFragment().apply {
            arguments=Bundle().apply {
                putString(AppConstant.ID,artistId)
            }
        }/*: FollowingFragment {
            if (::instance.isInitialized.not()) {
                instance = FollowingFragment()
            }
            return instance*/
//        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       //rootView = inflater.inflate(R.layout.fragment_follower, container, false)
        binding= FragmentFollowerBinding.inflate(inflater,container,false)
        rootView=binding.root
        initUI()
        initHelper()
        return rootView
    }

    private fun initHelper() {
        helper=FollowingHelper(this)
        helper.getFollower(true)
    }

    private fun initUI() {
        artistId=arguments?.getString(AppConstant.ID)?:""
        binding.edSearchFollowers.hint=getString(R.string.search_following)
        val layoutManager =
            LinearLayoutManager(requireContext())
        binding.rcyFollowers.layoutManager = layoutManager
    followerAdapter =
            FollowerAdapter(requireContext(),getString(R.string.following),this)
        binding.rcyFollowers.adapter = followerAdapter
        binding.rcyFollowers.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                val threshold = 5
                if (!isLoading &&
                    isDataAvailable &&
                    totalItem < (lastVisibleItem + threshold)
                ) {
                    helper.getFollower(false)
                }
            }
        })

        binding.edSearchFollowers.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().length > 2) {
                    helper.investmentNextPage = 1
                    helper.getFollower(true)
                } else if (s.toString().isEmpty()) {
                    helper.investmentNextPage = 1
                    helper.getFollower(true)
                }
            }

        })

        binding.followerRefresh.setOnRefreshListener {
            helper.investmentNextPage = 1
            helper.getFollower(true)
        }
    }

    override fun openProfile(userId: String, type: Int) {
        navigate.openTopArtist(userId, type)
    }

    fun updateTotalCount(count: Int) {
        if (parentFragment is FollowerFollowingFragment){
            (parentFragment as FollowerFollowingFragment).updateFollowingCount(count)
        }
    }
}