package com.nibula.user.Follower

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.tabs.TabLayout
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentFollowerFollowingBinding
import com.nibula.user.Follower.adapter.FollowerFollowingPagerAdapter
import com.nibula.utils.AppConstant


class FollowerFollowingFragment : BaseFragment() {

    private lateinit var adapter: FollowerFollowingPagerAdapter
    lateinit var rootView: View
    private var followerCount = -1
    private var followingCount = -1
    lateinit var binding:FragmentFollowerFollowingBinding

    companion object {
        fun getInstance() = FollowerFollowingFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //rootView = inflater.inflate(R.layout.fragment_follower_following, container, false)
        binding= FragmentFollowerFollowingBinding.inflate(inflater,container,false)
        rootView=binding.root
        intiUI()

        return rootView
    }

    private fun intiUI() {
        binding.layoutTbMain.tvTitle.text = arguments?.getString(AppConstant.RECORD_ARTIST_NAME)
            ?: "${getString(R.string.app_name)}"
        binding.layoutTbMain.imgHome.setOnClickListener { navigate.manualBack() }

val artistId=arguments?.getString(AppConstant.ID)
    ?: ""
        adapter = FollowerFollowingPagerAdapter(
            childFragmentManager,
             navigate,artistId
        )
        binding.viewPager.adapter = adapter

        binding.viewPager.offscreenPageLimit = 2
        binding.viewPager.isSaveEnabled=false
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        prepareTab()

    }


    private fun prepareTab() {
        val follower = binding.tabLayout.getTabAt(0)
        addTabHere(
            follower,
            R.layout.layout_tab_follwer,
            getString(R.string.followers),
            R.font.pt_sans, Typeface.NORMAL, View.VISIBLE
        )
        val following = binding.tabLayout.getTabAt(1)
        addTabHere(
            following,
            R.layout.layout_tab_follwer,
            getString(R.string.following),
            R.font.pt_sans, Typeface.NORMAL,
            View.INVISIBLE
        )

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                changeTabAppearance3(tab, View.INVISIBLE)
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                changeTabAppearance3(tab, View.VISIBLE)
            }

        })
        wrapTabIndicatorToTitle(binding.tabLayout, 0, 0)
    }

    fun updateFollowerCount(count: Int) {
        if (count == 0) return

        followerCount = count
        val tab = binding.tabLayout.getTabAt(0)
        tab?.let {
            val tabTitle = tab?.customView!!.findViewById<AppCompatTextView>(R.id.tv_tab_title)
            tabTitle.text = "$followerCount ${getString(R.string.followers)}"
        }
    }


    fun updateFollowingCount(count: Int) {
        if (count == 0) return

        followingCount = count
        val tab = binding.tabLayout.getTabAt(1)
        tab?.let {
            val tabTitle = tab?.customView!!.findViewById<AppCompatTextView>(R.id.tv_tab_title)
            tabTitle.text = "$followingCount ${getString(R.string.following)}"
        }
    }


    private fun addTabHere(
        tab: TabLayout.Tab?,
        layoutId: Int,
        title: String,
        font: Int,
        textStyle: Int,
        indicatorVisibility: Int
    ) {
        if (tab == null) {
            return
        }
        val customTabView: View =
            LayoutInflater.from(requireContext()).inflate(layoutId, null)

        customTabView.findViewById<View>(R.id.v_bottom_border).visibility = indicatorVisibility
        val tabTitle = customTabView.findViewById<TextView>(R.id.tv_tab_title)
        tabTitle.text = title

        val tf = ResourcesCompat.getFont(requireContext(), font)
        tabTitle.setTypeface(tf, textStyle)

        tab.customView = customTabView
        tab.tag = title

        tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        if (indicatorVisibility == View.VISIBLE) {
            tabTitle.alpha = 1f
        } else {
            tabTitle.alpha = 0.4f
        }
    }
}