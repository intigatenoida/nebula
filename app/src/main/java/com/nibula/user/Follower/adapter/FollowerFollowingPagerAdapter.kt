package com.nibula.user.Follower.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nibula.request_to_buy.navigate
import com.nibula.user.Follower.FollowerFragment
import com.nibula.user.Follower.FollowingFragment
import com.nibula.utils.SmartFragmentStatePagerAdapter

class FollowerFollowingPagerAdapter(fn: FragmentManager, val navigate: navigate,val artistId:String) :
    SmartFragmentStatePagerAdapter(fn) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                FollowerFragment.getInstance(artistId)
            }
            else -> {
                FollowingFragment.getInstance(artistId)
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }

}