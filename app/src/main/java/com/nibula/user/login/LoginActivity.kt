package com.nibula.user.login

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView.OnEditorActionListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.iid.FirebaseInstanceId
import com.hbb20.CountryCodePicker
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.bottomsheets.PNBSheetFragment
import com.nibula.dashboard.DashboardActivity
import com.nibula.databinding.SupportAllDeviceLoginActivityBinding
import com.nibula.fcm.UpdateDeviceInfoReq
import com.nibula.request.login_request
import com.nibula.response.BaseResponse
import com.nibula.response.login.LoginResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.user.forget_password.ForgetPasswordActivity
import com.nibula.user.sign_up.CreateAccountActivity
import com.nibula.user.sign_up.adapter.CountryListAdapter
import com.nibula.user.sign_up.helper.LoginHelper
import com.nibula.user.sign_up.modal.ResponseCollection
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import com.nibula.utils.RequestCode.LOGIN_TO_SIGNUP
import com.nibula.utils.interfaces.DialogFragmentClicks

import java.util.*


class LoginActivity : BaseActivity(), CountryListAdapter.Communicator, DialogFragmentClicks {

    var isFirstTime = false
    lateinit var CountryList: MutableList<ResponseCollection>
    lateinit var madapter: CountryListAdapter
    lateinit var dialog: BottomSheetDialog
    lateinit var selectedData: ResponseCollection
    lateinit var helper: LoginHelper
    lateinit var countryCodePicker: CountryCodePicker
    var isPhoneNumberIsValid: Boolean = false
    lateinit var binding:SupportAllDeviceLoginActivityBinding

    companion object {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.support_all_device_login_activity)
        binding= SupportAllDeviceLoginActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //checkAppUpdaeAvailability()
        countryCodePicker = findViewById(R.id.ccpLogin)
        //showFullScreenCountryPicker()
        countryCodePicker?.setOnCountryChangeListener {

            val selectedCountryCode = countryCodePicker?.selectedCountryCode
        }
        countryCodePicker.setPhoneNumberValidityChangeListener {
            (it).also { isPhoneNumberIsValid = it }

        }
        countryCodePicker.registerCarrierNumberEditText(binding.edtPhoneNumberValue)
        showSoftKeyboard(binding.edtPhoneNumberValue)
        binding.skipTxt.setOnClickListener {
            CommonUtils.triggerSignUpSkip()
        }
        helper =
            LoginHelper(this@LoginActivity)

        binding.edtPhoneNumberValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString().length == 1 && s.toString().startsWith("0")) {
                    s.clear()
                }
            }
        })

/*
        ccpLogin.setOnClickListener {
            if (!::CountryList.isInitialized ||
                CountryList.isEmpty()
            ) {
                txtError.visibility = View.VISIBLE
                txtError.text = getString(R.string.country_list_error)
                */
/*
                  showToast(
                      this@CreateAccountActivity,
                      getString(R.string.country_list_error),
                      CustomToast.ToastType.FAILED
                  )*//*

                return@setOnClickListener
            }

            initalizeBottomSheet()
        }
*/
        isFirstTime = if (intent.hasExtra(AppConstant.ISFIRSTTIME)) {
            intent.getBooleanExtra(AppConstant.ISFIRSTTIME, false)
        } else {
            false
        }
        val ss1 = SpannableString(getString(R.string.create_new_accounts))
        ss1.setSpan(UnderlineSpan(), 0, ss1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.createAccountTV.text = ss1
        binding.createAccountTV.setOnClickListener {
            startActivityForResult(
                Intent(
                    this,
                    CreateAccountActivity::class.java
                ).putExtra(AppConstant.IS_FROM_LOGIN, true), LOGIN_TO_SIGNUP
            )
        }
        binding.forgotPasswordTv.setOnClickListener {
            startActivityForResult(
                Intent(this@LoginActivity, ForgetPasswordActivity::class.java),
                1000
            )
        }
        binding.btnLogin.setOnClickListener {
            if (!isValidate()) {
                return@setOnClickListener
            }
            loginApi()
        }
        //--> Skip
        binding.skipTxt.setOnClickListener {
            if (isFirstTime) {
                PrefUtils.saveValueInPreference(
                    this@LoginActivity,
                    PrefUtils.IS_USER_SKIP_FROM_LOGIN,
                    true
                )
                startActivity(Intent(this@LoginActivity, DashboardActivity::class.java))
                finish()
            } else {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }

        //--> Done Click listener
        binding.edtPassword.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (!isValidate()) {
                    return@OnEditorActionListener false
                }
                loginApi()
                handled = true
            }
            handled
        })
        hideKeyboard()
    }

    //--> validating Login Fields
    private fun isValidate(): Boolean {
/*        if (TextUtils.isEmpty(edtEmail.text.toString())) {

            txtError.visibility = View.VISIBLE
            txtError.text = getString(R.string.email_empty)

            *//* CustomToast.showToast(
                 this@LoginActivity,
                 getString(R.string.email_empty),
                 CustomToast.ToastType.FAILED
             )*//*
            return false
        }*/
//        if (!CommonUtils.isValidEmail(edtEmail.text.toString())) {
//            showToast(
//                this@LoginActivity,
//                getString(R.string.valid_email),
//                CustomToast.ToastType.FAILED
//            )
//            return false
//        }

        hideKeyboard()

        if (binding.edtPhoneNumberValue.text?.toString().isNullOrEmpty()) {

            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.empty_mobile_no)
            return false
        }

        val number = getUnformatedNumber(binding.edtPhoneNumberValue.text.toString())
        if (number.length < 8 || number.length > 12) {
            /*  showToast(
                this@CreateAccountActivity,
                getString(R.string.valid_phone_no),
                CustomToast.ToastType.FAILED
            )*/

            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.valid_phone_no)
            return false
        }
        if (!isPhoneNumberIsValid) {

            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.phone_number_not_valid)
            return false
        }

        if (binding.edtPassword.text.isNullOrBlank()) {
            binding.txtError.visibility = View.VISIBLE
            binding.txtError.text = getString(R.string.password_empty)
/*
            showToast(
                this@LoginActivity,
                getString(R.string.password_empty),
                CustomToast.ToastType.FAILED
            )*/
            return false
        }

        return true
    }


    //--> No need to put on login
    /* private fun checkPasswordValidation(password: String): Boolean {
         val spacialCharPatten: Pattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
         val upperCasePatten: Pattern = Pattern.compile("[A-Z ]")
         val lowerCasePatten: Pattern = Pattern.compile("[a-z ]")
         val digitCasePatten: Pattern = Pattern.compile("[0-9 ]")

         if (!spacialCharPatten.matcher(password).find()) {
             showToast("Passwords must have at least one special character.");
             return false;
         }
         if (!upperCasePatten.matcher(password).find()) {
             showToast("Password must have at least one uppercase character !!");
             return false;
         }
         if (!lowerCasePatten.matcher(password).find()) {
             showToast("Password must have at least one lowercase character !!");
             return false;
         }
         if (!digitCasePatten.matcher(password).find()) {
             showToast("Password must have at least one digit character !!");
             return false;
         }
         return true

     }*/


    private fun sendRegistrationToServer(token: String?) {
        val updateDeviceInfoReq = UpdateDeviceInfoReq()
        updateDeviceInfoReq.deviceNotificationID = token!!
        val helper =
            ApiClient.getClientMusic(this).create(ApiAuthHelper::class.java)
        helper.saveDeviceInfo(updateDeviceInfoReq)
            .enqueue(object : CallBackManager<BaseResponse>() {
                override fun onSuccess(any: BaseResponse?, message: String) {

                }

                override fun onFailure(message: String) {

                }

                override fun onError(error: RetroError) {
                }

            })
    }


    //--> API Calling
    private fun loginApi() {
        if (!isOnline()) {

            binding.txtError.visibility = View.VISIBLE

            binding.txtError.text = getString(R.string.internet_connectivity)

            /*showToast(
                this@LoginActivity,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )*/
            hideProcessDailog()
            return
        }
        //--> Log Activity
        bindLog(this.localClassName)
        hideKeyboard()
        showProcessDialog()
        val helper = ApiClient.getClientAuth(this).create(ApiAuthHelper::class.java)
        val request = login_request()
        var userName = binding.edtPhoneNumberValue.text.toString()
        // var phoneNumber = edtPhoneNumberValue.text.toString()
        request.password = binding.edtPassword.text.toString()
        //request.counntryCode = selectedData.countryCode.toString()
        request.counntryCode = countryCodePicker?.selectedCountryCodeWithPlus.toString()
        /*  if (userName[0] == '@') {                    fg.hideKeyboard()

              userName = userName.replaceFirst("@", "")
          }*/
        request.username = userName

        //--> Set static
        /*request.password="Anil@1234"
        request.username="anilg"*/

        val call = helper.login(request, AppConstant.CONTENT_TYPE)
        call.enqueue(object : CallBackManager<LoginResponse>() {
            override fun onSuccess(any: LoginResponse?, message: String) {
                updateDeviceToken()
                hideProcessDailog()
                if (any?.responseStatus != null && any.responseStatus == 1) {

                    CommonUtils.triggerloginEvent(any)
                    val mToken = PrefUtils.getValueFromPreference(
                        this@LoginActivity,

                        PrefUtils.NOTIFICATION_TOKEN
                    )
                    if (mToken.isNotEmpty()) {
                        sendRegistrationToServer(mToken)
                    }
                    any.token.let {
                        PrefUtils.saveValueInPreference(
                            this@LoginActivity,
                            PrefUtils.TOKEN,
                            it.accessToken ?: ""
                        )

                        PrefUtils.saveValueInPreference(
                            this@LoginActivity,
                            PrefUtils.TOKEN_TYPE,
                            it.tokenType ?: ""
                        )

                        PrefUtils.saveValueInPreference(
                            this@LoginActivity,
                            PrefUtils.TOKEN_TIME,
                            it.expiresIn ?: 0
                        )
                    }

                    any.token.let {
                        it?.blockchain_wallet_address?.let { it1 ->
                            PrefUtils.saveValueInPreference(
                                this@LoginActivity,
                                PrefUtils.CONNECTED_WALLET_ADDRESS,
                                it1
                            )
                        }

                    }

                    any.userInfo.let {
                        PrefUtils.saveValueInPreference(
                            this@LoginActivity,
                            PrefUtils.IS_EMAIL_VALID,
                            it.verifiedEmail ?: "".toLowerCase(Locale.ROOT) == "true"
                        )
                        PrefUtils.saveValueInPreference(
                            this@LoginActivity,
                            PrefUtils.IS_PHONE_VALID,
                            it.verifiedPhone ?: "".toLowerCase(Locale.ROOT) == "true"
                        )

                        PrefUtils.saveValueInPreference(
                            this@LoginActivity,
                            PrefUtils.USER_ID,
                            it.sub ?: ""
                        )
                        PrefUtils.saveValueInPreference(
                            this@LoginActivity,
                            PrefUtils.NAME,
                            it.givenName ?: ""
                        )
                    }


                    if (isFirstTime) {
                        startActivity(Intent(this@LoginActivity, DashboardActivity::class.java))
                        finish()
                    } else {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                } else {
                    any?.responseMessage?.let {
                        binding.txtError.visibility = View.VISIBLE

                        binding.txtError.text = it

                        //showToast(this@LoginActivity, it, CustomToast.ToastType.FAILED)
                    }
                }
            }

            override fun onFailure(message: String) {
                if (message.isNotEmpty()) {
                    binding.txtError.visibility = View.VISIBLE
                    binding.txtError.text = message
                    // showToast(this@LoginActivity, message, CustomToast.ToastType.FAILED)
                }
                hideProcessDailog()
            }

            override fun onError(error: RetroError) {
                hideProcessDailog()
            }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LOGIN_TO_SIGNUP && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        } else if (requestCode == LOGIN_TO_SIGNUP && resultCode == Activity.RESULT_CANCELED) {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
    }

    private fun updateDeviceToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result!!.token
                Log.d("Token", token)
                val updateDeviceInfoReq = UpdateDeviceInfoReq()
                updateDeviceInfoReq.deviceNotificationID = token
                val helper =
                    ApiClient.getClientMusic(this).create(ApiAuthHelper::class.java)
                helper.saveDeviceInfo(updateDeviceInfoReq)
                    .enqueue(object : CallBackManager<BaseResponse>() {
                        override fun onSuccess(any: BaseResponse?, message: String) {
                        }

                        override fun onFailure(message: String) {
                        }

                        override fun onError(error: RetroError) {
                        }

                    })
            })
    }


    private fun getUnformatedNumber(number: String): String {
        return number.replace(AppConstant.PREFIX_NUMBER, "").replace(AppConstant.SEPRATER, "")

    }

    //--> on Country Selected from adapter
    override fun onCountrySelcted(data: ResponseCollection) {
        if (::dialog.isInitialized) {
            dialog.dismiss()
        }
        selectedData = data
        //tv_countryCode.text = data.countryCode
        hideKeyboard()
    }

    //--> Get Data after Sucess
    fun updateList(list: MutableList<ResponseCollection>) {
        val data = list[0]
        //tv_countryCode.text = data.countryCode
        selectedData = data
        CountryList = list
    }

    override fun onResume() {
        super.onResume()
        /*if (!::CountryList.isInitialized) {
            helper.getCountryList(true)
        }*/
    }

    override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        if (bundle == null) {
            return
        }
        when (obj) {
            is PNBSheetFragment -> {
                if (!isFinishing) {
                    helper.getCountryList(true)
                }
            }
        }
        dialog.dismiss()
    }

    override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
        if (bundle == null) {
            return
        }
        when (obj) {
            is PNBSheetFragment -> {
                finish()
            }
        }
        dialog.dismiss()
    }


/*
    private fun initalizeBottomSheet() {
        dialog = BottomSheetDialog(this, R.style.Theme_Dialog)
        dialog.setContentView(R.layout.layout_country)
        dialog.setCancelable(true)
        dialog.dismissWithAnimation = true
        dialog.setOnShowListener { dialogInterface ->
            val bottomSheetDialog = dialogInterface as BottomSheetDialog
            CommonUtils.setupFullHeight(this@LoginActivity, bottomSheetDialog)
        }
        //dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        dialog.edtSearchCountry.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {

            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())
            }
        })
        madapter = CountryListAdapter(this@LoginActivity, CountryList, this)
        val layoutManager =
            LinearLayoutManager(this@LoginActivity, LinearLayoutManager.VERTICAL, false)
        dialog.rcv_country.layoutManager = layoutManager
        dialog.rcv_country.adapter = madapter
        dialog.show()
    }
*/

/*
    private fun filter(text: String) {
        val filteredList: ArrayList<ResponseCollection> = ArrayList()
        for (item in CountryList) {
            if (item.countryName?.toLowerCase()!!
                    .contains(text.toLowerCase()) || item.countryCode?.toLowerCase()!!
                    .contains(text.toLowerCase())
            ) {
                filteredList.add(item)
            }
        }
        if (filteredList.isNotEmpty()) {
            dialog.noDataFoundTv.visibility = View.GONE
            dialog.rcv_country.visibility = View.VISIBLE
            madapter.filterList(filteredList)
        } else {
            dialog.noDataFoundTv.visibility = View.VISIBLE
            dialog.noDataFoundTv.tv_message.text = getString(R.string.no_county_found)
            dialog.rcv_country.visibility = View.GONE
        }

    }
*/


    /*
        private fun checkAppUpdaeAvailability() {
            val appUpdateManager = create(this@LoginActivity)

    // Returns an intent object that you use to check for an update.
            val appUpdateInfoTask = appUpdateManager.appUpdateInfo

    // Checks that the platform will allow the specified type of update.
            appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
                ) {
                    // Request the update.
                }
            }
        }
    */
    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    private fun showFullScreenCountryPicker() {
        val layoutParams = countryCodePicker.layoutParams
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        countryCodePicker.layoutParams = layoutParams
    }
}
