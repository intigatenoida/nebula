package com.nibula.prompt

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nibula.R
import com.nibula.bottomsheets.BaseBottomSheetFragment
import com.nibula.databinding.PromptCustomBinding
import com.nibula.utils.interfaces.DialogFragmentClicks


class CustomPrompt : BasePrompt() {

    var promptId:Int = 0

    lateinit var dgFrgListener: DialogFragmentClicks
    lateinit var message: String
    lateinit var positiveText: String
    lateinit var negativeText: String
    lateinit var binding:PromptCustomBinding

    companion object {
        fun getInstance(
            promptId:Int,
            message: String,
            positiveText: String,
            negativeText: String,
            dgFrgListener: DialogFragmentClicks
        ): CustomPrompt {
            val customPrompt = CustomPrompt()
            customPrompt.promptId = promptId
            customPrompt.dgFrgListener = dgFrgListener
            customPrompt.message = message
            customPrompt.positiveText = positiveText
            customPrompt.negativeText = negativeText
            return customPrompt
        }

        const val PROMPT_ID:String = "promptId"
    }

    override fun onStart() {
        setPromptDimensions(0.9f, ConstraintLayout.LayoutParams.WRAP_CONTENT.toFloat())
        super.onStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =PromptCustomBinding.inflate(inflater,container,false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = false
        binding.tvTitle.text = message

        if (positiveText.isNotEmpty()) {
            binding.btOk.text = positiveText
            binding.btOk.setOnClickListener {
                val b = Bundle()
                b.putInt(BaseBottomSheetFragment.PROMPT_ID, promptId)
                dgFrgListener.onPositiveButtonClick(this, b,dialog!!)
            }
        } else {
            binding.btOk.visibility = View.GONE
        }

        if (negativeText.isNotEmpty()) {
            binding.btCancel.text = negativeText
            binding.btCancel.setOnClickListener {
                val b = Bundle()
                b.putInt(BaseBottomSheetFragment.PROMPT_ID, promptId)
                dgFrgListener.onNegativeButtonClick(this, b,dialog!!)
            }
        } else {
            binding.btCancel.visibility = View.GONE
        }
    }
}