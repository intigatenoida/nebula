package com.nibula.records.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemReferralBuyerBinding
import com.nibula.databinding.ItemReferralInvestorBinding
import com.nibula.databinding.ItemReferralsInvestorHeaderBinding
import com.nibula.utils.CommonUtils


class ReferralsAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ReferralsInvestViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ReferralsInvestHeaderViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ReferralsBuyerViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    private lateinit var layoutInflater: LayoutInflater

    private var context: Context? = null

    private var isInvestorAdapter: Boolean = false
    lateinit var headerBinding: ItemReferralsInvestorHeaderBinding
    lateinit var binding: ItemReferralInvestorBinding
    lateinit var buyingBinding: ItemReferralBuyerBinding

    companion object {

        const val VIEW_TYPE_BUYER_REFERRAL = 1

        const val VIEW_TYPE_INVESTOR_REFERRAL = 2

        const val VIEW_TYPE_INVESTOR_HEADER_REFERRAL = 3

    }

    constructor(context: Context, isInvestorAdapter: Boolean) : this() {
        this.context = context
        this.isInvestorAdapter = isInvestorAdapter
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (isInvestorAdapter) {
            if (viewType == VIEW_TYPE_INVESTOR_HEADER_REFERRAL) {
                /* val view =
                     layoutInflater.inflate(R.layout.item_referrals_investor_header, parent, false)
                 ReferralsInvestHeaderViewHolder(view)*/

                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemReferralsInvestorHeaderBinding.inflate(inflater, parent, false)

                val holder = ReferralsInvestHeaderViewHolder(binding.root)
                return holder
            } else {
                /* val view = layoutInflater.inflate(R.layout.item_referral_investor, parent, false)
                 ReferralsInvestViewHolder(view)*/
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemReferralInvestorBinding.inflate(inflater, parent, false)

                val holder = ReferralsInvestViewHolder(binding.root)
                return holder
            }
        } else {
            /*   val view = layoutInflater.inflate(R.layout.item_referral_buyer, parent, false)
               ReferralsBuyerViewHolder(view)*/


            val inflater = LayoutInflater.from(parent.context)
            buyingBinding = ItemReferralBuyerBinding.inflate(inflater, parent, false)

            val holder = ReferralsBuyerViewHolder(buyingBinding.root)
            return holder
        }
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun getItemViewType(position: Int): Int {
        if (isInvestorAdapter) {
            //Investor
            if (position == 0) {
                return VIEW_TYPE_INVESTOR_HEADER_REFERRAL
            } else if (position == 3) {
                return VIEW_TYPE_INVESTOR_HEADER_REFERRAL
            } else {
                return VIEW_TYPE_INVESTOR_REFERRAL
            }
        } else {
            //buyer
            return VIEW_TYPE_BUYER_REFERRAL
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (isInvestorAdapter) {
            if (holder is ReferralsInvestHeaderViewHolder) {
                //ReferralsInvestHeaderViewHolder
                if (position == 0) {
                    headerBinding.tvHeader.text = "Recommended by"
                } else if (position == 3) {
                    headerBinding.tvHeader.text =
                        CommonUtils.redWhiteSpannable(context!!, "283", "Investors so far")
                }

            } else {
                //ReferralsInvestViewHolder
                val itemHolder = holder as ReferralsInvestViewHolder
                binding.tvUserName.text = "Snoop Dogg"

                if (position == 1) {
                    binding.tvStatus.text = ""
                    binding.imgUser.setImageResource(R.drawable.nebula_placeholder)
                } else {
                    binding.tvStatus.text = "Invested"
                    binding.imgUser.setImageResource(R.drawable.ic_record_user_place_holder)
                }
            }
        } else {
            //ReferralsBuyerViewHolder
            val itemHolder = holder as ReferralsBuyerViewHolder
            buyingBinding.tvUserName.text = "Fuad Hawit"
            if (position == 1) {
                buyingBinding.imgUser.setImageResource(R.drawable.nebula_placeholder)
            } else {
                buyingBinding.imgUser.setImageResource(R.drawable.ic_record_user_place_holder)
            }

            buyingBinding.tvBuyNow.text = "Buy \$10.99"
        }
    }

}