package com.nibula.records.invest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.databinding.FragmentRecordInvestRefferralBinding
import com.nibula.records.adapters.ReferralsAdapter
import com.nibula.utils.CommonUtils


class RecordInvestReferralFragment : BaseFragment() {

    companion object {
        fun newInstance() = RecordInvestReferralFragment()
    }

    private lateinit var viewModel: RecordInvestRefferralViewModel

    private lateinit var rootView: View
    private lateinit var adapter: ReferralsAdapter
    lateinit var binding:FragmentRecordInvestRefferralBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.fragment_record_invest_refferral, container, false)
            binding= FragmentRecordInvestRefferralBinding.inflate(inflater,container,false)
            rootView=binding.root
            initUI()
        }
        return rootView
    }

    private fun initUI() {

        binding.tvTime.text = "15D 23H"
        binding.tvName.text = "Forgotten Rebels"
        binding.tvShare.text = "\$1000 per share"
//        rootView.tv_song.text = "Vato"
//        rootView.tv_artist.text = "Snoop Dogg"

        binding.tvAlbum.text = CommonUtils.redWhiteSpannable(requireContext(),"Album:", "This Ain’t Hollywood")
        binding.tvInvestors.text = CommonUtils.redWhiteSpannable(requireContext(),"283", "investors")

        binding.tvShareAvailable.text = CommonUtils.redWhiteSpannable(requireContext(),"17", "Shares Available")

        binding.tvReleaseDate.text = CommonUtils.whiteRedSpannable(requireContext(),"5 March 2020", "Release Date:")

//        rootView.layout_bottom.setOnClickListener {
//            val mPlay = SongPlayBottomSheetFragment(this)
//            mPlay.isCancelable = true
//            mPlay.show(childFragmentManager, "SongPlayBottomSheetFragment")
//        }

//        rootView.img_share.visibility = View.GONE

        setAdapter()

    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvInvestors.layoutManager = layoutManager

        adapter = ReferralsAdapter(requireContext(),true)
        binding.rvInvestors.adapter = adapter

    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(this).get(RecordInvestRefferralViewModel::class.java)
    }

}
