package com.nibula.ui.termscondition

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nibula.R

class TermsConditionFragment : Fragment() {

    companion object {
        fun newInstance() = TermsConditionFragment()
    }
    private lateinit var viewModel: TermsConditionViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.terms_condition_fragment, container, false)
    }
    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TermsConditionViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
