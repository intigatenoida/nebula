package com.nibula.transcation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.ActivityTranscationHistoryBinding
import com.nibula.response.HistoryResponse.HistoryResponse
import com.nibula.response.HistoryResponse.ResponseCollection
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.CommonUtils


class TranscationHistoryFragment : BaseFragment() {
    companion object {
        fun newInstance() = TranscationHistoryFragment()
    }

    private lateinit var adapter: TranscationHistoryAdapter
    private lateinit var root: View
    private lateinit var array: ArrayList<ResponseCollection>
    var currentPage = 1
    val itemPerPage = 10
    lateinit var binding:ActivityTranscationHistoryBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //root = inflater.inflate(R.layout.activity_transcation_history, null)
        binding=ActivityTranscationHistoryBinding.inflate(inflater,container,false)
        root=binding.root
        array = ArrayList()
        binding.header.tvTitle.text = getString(R.string.t_history)

        binding.baseHeader.ivNotification.setOnClickListener {
            loginNotifyCheck()
        }

        binding.baseHeader.imgSearch.setOnClickListener {
            navigate.onClickSearch()
        }

        binding.baseHeader.ivUpload.setOnClickListener {
            loginUploadCheck()
        }

        binding.header.ivBack.setOnClickListener {
            navigate.manualBack()
            //activity?.supportFragmentManager?.popBackStack()
        }

        binding.swipeTransaction.setOnRefreshListener {
            currentPage = 1
            getTransactionHistory()
        }

        adapter = TranscationHistoryAdapter(requireActivity(), array)
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.rvTranscationHistory.layoutManager = layoutManager
        binding.rvTranscationHistory.adapter = adapter

        binding.rvTranscationHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager.findLastCompletelyVisibleItemPosition() == ((itemPerPage * currentPage) - 1)) {
                    currentPage++
                    getTransactionHistory()
                }
            }
        })

        getTransactionHistory()

        return root
    }

    private fun getTransactionHistory() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        if (!binding.swipeTransaction.isRefreshing) {//
            showProcessDialog()
        }
        val helper = ApiClient.getClientMusic(requireContext()).create(ApiAuthHelper::class.java)
        val call = helper.getTransactionHistory(currentPage)
        call.enqueue(object : CallBackManager<HistoryResponse>() {
            override fun onFailure(message: String) {
                hideProcessDialog()
                hideRefreshLayout()
                if (array.isEmpty()) {
                    binding.noDataFoundTv.root.visibility = View.VISIBLE
                    binding.noDataFoundTv.tvMessage.text = getString(R.string.no_history_available)
                    binding.rvTranscationHistory.visibility = View.GONE
                }
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
                hideRefreshLayout()
                if (error.kind == RetroError.Kind.API_CALL_CANCEL) {

                } else {

                }
            }

            override fun onSuccess(any: HistoryResponse?, message: String) {
                hideProcessDialog()
                val res = any as HistoryResponse
                if (binding.swipeTransaction.isRefreshing) {
                    array.clear()
                }
                if (res.responseStatus == 1) {
                    array.addAll(res.responseCollection)
                }
                if (array.isEmpty()) {
                    binding.noDataFoundTv.root.visibility = View.VISIBLE
                    binding.noDataFoundTv.tvMessage.text = getString(R.string.no_history_available)
                    binding.rvTranscationHistory.visibility = View.GONE
                } else {
                    binding.noDataFoundTv.root.visibility = View.GONE
                    binding.rvTranscationHistory.visibility = View.VISIBLE
                }
                adapter.notifyDataSetChanged()
                hideRefreshLayout()
            }
        })
    }

    private fun hideRefreshLayout() {
        if (binding.swipeTransaction.isRefreshing) {
            binding.swipeTransaction.isRefreshing = false
        }
    }

    override fun onResume() {
        super.onResume()
        if (binding.baseHeader.imgNotificationCount.visibility != View.VISIBLE) {
            CommonUtils.setNotificationDot(requireContext(), binding.baseHeader.imgNotificationCount)
        }
    }

}