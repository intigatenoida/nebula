package com.nibula.transcation

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nibula.R
import com.nibula.databinding.ItemTranscationHistoryBinding
import com.nibula.response.HistoryResponse.ResponseCollection
import com.nibula.utils.CommonUtils

class TranscationHistoryAdapter(val context: Context, val array: ArrayList<ResponseCollection>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

lateinit var binding:ItemTranscationHistoryBinding
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        binding = ItemTranscationHistoryBinding.inflate(inflater, parent, false)
        val holder = ViewHolder(binding.root)
        return holder
       /* return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_transcation_history,
                parent,
                false
            )
        )*/
    }

    override fun getItemCount(): Int {
        return array.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.d("OnTransactionHistory", "OnBindViewHolder"+position)

        val data = array[position]
        setIcon(data.balanceTypeId, binding.iv)
        binding.tvTitle.text = data.description

     binding.txtDate.text = "${CommonUtils.dateFormatterTransaction(data.actionTakenOn)}"
        if (data.balanceTypeId == 1 || data.balanceTypeId == 7 || data.balanceTypeId == 6) {
            binding.txtAmount.text =
                "${data.currencyTypeSymbol ?: "$"} ${CommonUtils.trimDecimalToTwoPlaces(data.debitAmount)}"
        } else {
            binding.txtAmount.text =
                "${data.currencyTypeSymbol ?: "$"} ${CommonUtils.trimDecimalToTwoPlaces(data.creditAmount)}"
        }
    }

    private fun setIcon(type: Int, imageVieW: ImageView) {
        imageVieW.let {  }
        imageVieW.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                when (type) {
                    1 -> {
                        R.drawable.ic_ivested
                    }
                    2 -> {
                        R.drawable.ic_cashback
                    }
                    3 -> {
                        R.drawable.ic_copyright
                    }
                    4 -> {
                        R.drawable.ic_fund_raised
                    }
                    5 -> {
                        R.drawable.ic_album_sold
                    }
                    else -> {
                        R.drawable.ic_album_purchased
                    }
                }
            )
        )
    }

}

