package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class DeleteReferralRequest(
    @SerializedName("ChatId")
    var chatId: String = "",
    @SerializedName("RecordId")
    var recordId: String = ""
)