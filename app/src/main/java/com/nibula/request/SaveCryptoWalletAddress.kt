package com.nibula.request


import com.google.gson.annotations.SerializedName

data class SaveCryptoWalletAddress(
    @SerializedName("CryptoWalletAddress")
    var cryptoWalletAddress: String = "",
    @SerializedName("WalletTypeId")
    var walletTypeId: Int = 0
)