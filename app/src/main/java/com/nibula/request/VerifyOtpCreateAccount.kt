package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class VerifyOtpCreateAccount(
    @SerializedName("PhoneConfirmationCode")
    var phoneConfirmationCode: String = "",
    @SerializedName("RequestCode")
    var requestCode: String = "",
    @SerializedName("UserId")
    var userId: String = ""
)