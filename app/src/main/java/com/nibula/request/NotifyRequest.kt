package com.nibula.request

import com.google.gson.annotations.SerializedName

data class NotifyRequest(
    @SerializedName("RecordName")
    var recordName: String="",
    @SerializedName("Id")
    var recordId: String="",
    @SerializedName("EmailId")
    var email: String="",

    @SerializedName("FullName")
    var fullName: String="",

    @SerializedName("LastName")
    var lastName: String="",
    @SerializedName("FirstName")
    var firstName: String=""


)
