package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ChangeRequiestStatus(
    @SerializedName("Id")
    var id: String = "",
    @SerializedName("ShowToPublic")
    var showToPublic: String = ""
)