package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class VerifyEmailPhoneRequest(
    @SerializedName("Action")
    var action: Int = 0,
    @SerializedName("EmailConfirmationCode")
    var emailConfirmationCode: Int = 0,
    @SerializedName("PhoneConfirmationCode")
    var phoneConfirmationCode: Int = 0
)