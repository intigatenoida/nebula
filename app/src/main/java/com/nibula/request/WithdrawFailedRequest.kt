package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class WithdrawFailedRequest(
    @SerializedName("CurrencyTypeId")
    var currencyTypeId: Int = 0,
    @SerializedName("ReferenceNo")
    var referenceNo: String = "",
    @SerializedName("Remark")
    var remark: String = "",
    @SerializedName("RequestedAmount")
    var requestedAmount: String = "",
    @SerializedName("TransactionNo")
    var transactionNo: String = "",
    @SerializedName("WithdrawalFundRequestId")
    var withdrawalFundRequestId: Int = 0
)