package com.nibula.request
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
@Keep
data class UpdateNotificationStatus(
    @SerializedName("Id")
    var notificaationId: String = ""
)