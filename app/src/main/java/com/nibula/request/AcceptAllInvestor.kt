package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class AcceptAllInvestor(
    @SerializedName("Id")
    var id: String
)