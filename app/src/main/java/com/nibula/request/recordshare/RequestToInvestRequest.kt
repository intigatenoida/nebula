package com.nibula.request.recordshare

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class RequestToInvestRequest(

    @SerializedName("Id")
    var id:Int? =null,
    @SerializedName("RecordId")
    var recordId:String? =null,
    @SerializedName("RequestedShares")
    var requestedShares:Int? =null,
    @SerializedName("TotalRequestedAmount")
    var totalRequestedAmount:Double? =null,
    @SerializedName("ReferralCode")
    var referralCode:String? =null

)