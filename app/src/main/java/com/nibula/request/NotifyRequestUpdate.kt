package com.nibula.request

import com.google.gson.annotations.SerializedName

data class NotifyRequestUpdate(

    @SerializedName("RecordId")
    var recordId: String=""
)
