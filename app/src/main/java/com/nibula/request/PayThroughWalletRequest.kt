package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class PayThroughWalletRequest(
    @SerializedName("RequestToken")
    var requestToken: String = "",
    @SerializedName("Type")
    var type: Int = 0,
    @SerializedName("WalletAmount")
    var walletAmount: String = ""
)