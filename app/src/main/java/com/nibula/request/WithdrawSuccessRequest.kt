package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class WithdrawSuccessRequest(
    @SerializedName("CurrencyTypeId")
    var currencyTypeId: Int = 0,
    @SerializedName("ReferenceNo")
    var referenceNo:  Int = 0,
    @SerializedName("Remark")
    var remark: String = "sucessful request for first payment",
    @SerializedName("RequestedAmount")
    var requestedAmount: String = "",
    @SerializedName("TransactionNo")
    var transactionNo: Int = 0,
    @SerializedName("WithdrawalFundRequestId")
    var withdrawalFundRequestId: Int = 0
)