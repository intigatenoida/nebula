package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class BankMakeDefaultRequest(
    @SerializedName("UserBankAccountsId")
    var userBankAccountsId: Int = 0
)