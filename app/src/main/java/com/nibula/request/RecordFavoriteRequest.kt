package com.nibula.request

import com.google.gson.annotations.SerializedName

data class RecordFavoriteRequest(
    @SerializedName("RecordId")
    var recordId: String = "",
    @SerializedName("IsSelected")
    var isSelected: Boolean =false,

    @SerializedName("FileId")
    var fileID: Int = 0

)
