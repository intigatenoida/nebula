package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ResetPasswordRequest(
    @SerializedName("EmailId")
    var emailId: String = "",
    @SerializedName("Password")
    var password: String = "",
    @SerializedName("token")
    var token: String = "",

    @SerializedName("MobileNo")
var mobileNumber: String = "",
@SerializedName("CountryCode")
var counntryCode: String = ""
)