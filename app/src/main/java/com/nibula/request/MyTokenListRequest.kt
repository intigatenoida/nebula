package com.nibula.request

import com.google.gson.annotations.SerializedName

data class MyTokenListRequest(
    @SerializedName("CurrentPage")
    var currentPage: Int = 0,
    @SerializedName("RecordsPerPage")
    var recordsPerPage: Int = 0
)



