package com.nibula.request

import com.google.gson.annotations.SerializedName

data class RegisterRequest(

    @SerializedName("Name")
    var name: String = "",
    @SerializedName("FirstName")
    var FirstName: String? = null,
    @SerializedName("LastName")
    var LastName: String? = null,
    @SerializedName("UserName")
    var UserName: String = "",
    @SerializedName("Email")
    var Email: String = "",
    @SerializedName("PhoneNumber")
    var PhoneNumber: String = "",
    @SerializedName("Password")
    var Password: String = "",
    @SerializedName("CountryCode")
    var countryCode: String? = null,
    @SerializedName("UserId")
    var userId: String = "",
    @SerializedName("DeviceTypeId")
    var deviceTypeId: String = "",
    @SerializedName("CaptchaToken")
    var captchaToken: String = ""


)