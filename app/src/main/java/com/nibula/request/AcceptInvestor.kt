package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class AcceptInvestor(
    @SerializedName("Id")
    var id: Int,
    @SerializedName("InvestmentRequestStatusId")
    var investmentRequestStatusId: Int
)