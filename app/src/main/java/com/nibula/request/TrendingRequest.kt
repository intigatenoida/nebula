package com.nibula.request


import com.google.gson.annotations.SerializedName

data class TrendingRequest(
    @SerializedName("currentPage")
    var currentPage: Int = 0,
    @SerializedName("recordsPerPage")
    var recordsPerPage: Int = 0,
    @SerializedName("Search")
    var search: String = ""
)

