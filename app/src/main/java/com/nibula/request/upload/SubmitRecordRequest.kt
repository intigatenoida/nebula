package com.nibula.request.upload

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName


@Keep
data class SubmitRecordRequest(
	@SerializedName("RecordsId")
	val recordsId: String?,
	@SerializedName("RecordFilesId")
	val recordFilesId: String?,
	@SerializedName("TimeZone")
	val timeZone: String?
)