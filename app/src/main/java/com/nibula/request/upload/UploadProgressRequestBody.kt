package com.nibula.request.upload

import android.os.Handler
import android.os.Looper
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.RetroError
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream


class UploadProgressRequestBody() : RequestBody() {
	
	lateinit var mFile: File
	lateinit var content_type: String
	
	private lateinit var mListener: UploadCallbacks
	private var currentProgress: Int = 0
	
	companion object {
		
		const val RUNNING = "Running"
		
		const val COMPLETED = "Completed"
		
		const val RETRY = "Retry"
		
		const val STOP = "Stop"
		
		const val API_SUCCESS = "apiSuccess"
		
	}
	
	constructor(
		file: File,
		content_type: String,
		mListener: UploadCallbacks
	) : this() {
		this.mFile = file
		this.content_type = content_type
		this.mListener = mListener
	}
	
	override fun contentType(): MediaType? {
		return content_type.toMediaTypeOrNull()
	}
	
	override fun contentLength(): Long {
		return mFile.length()
	}
	
	override fun writeTo(sink: BufferedSink) {
		val fileLength = mFile.length()
		val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
		var mInput: FileInputStream? = null
		var uploaded: Long = 0
		val handler = Handler(Looper.getMainLooper())
		try {
			var read: Int
			mInput = FileInputStream(mFile)
			while (mInput.read(buffer).also { read = it } != -1) {
				uploaded += read.toLong()
				sink.write(buffer, 0, read)
				handler.post(ProgressUpdater(uploaded, fileLength))
			}
		} catch (e: Exception) {
			handler.post(UploadError(e))
			mInput = null
		} finally {
			mInput?.close()
		}
	}
	
	inner class ProgressUpdater(
		private val mUploaded: Long,
		private val mTotal: Long
	) :
			Runnable {
		override fun run() {
			val progress = (100 * mUploaded / mTotal).toInt()
			if (currentProgress > 0 && currentProgress != progress) {
				if (progress == 100) {
					mListener.onProgressUpdate(COMPLETED, progress)
				} else {
					mListener.onProgressUpdate(RUNNING, progress)
				}
			}
			currentProgress = progress
		}
	}
	
	inner class UploadError(
		private val e: Exception
	) : Runnable {
		override fun run() {
			mListener.onError(
				RETRY, RetroError(
					RetroError.Kind.NETWORK,
					e.message,
					-999
				)
			)
		}
	}
	
	interface UploadCallbacks {
		fun onProgressUpdate(status: String, progress: Int)
		fun onFinish(status: String, response: UploadRecordFileResponse)
		fun onError(status: String, error: RetroError)
	}
}
