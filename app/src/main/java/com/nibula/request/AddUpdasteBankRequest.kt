package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class AddUpdasteBankRequest(
    @SerializedName("AccountHolderName")
    var accountHolderName: String,
    @SerializedName("BankAccountNumber")
    var bankAccountNumber: String,
    @SerializedName("BankId")
    var bankId: Int,
    @SerializedName("IFSCCode")
    var iFSCCode: String,
    @SerializedName("UserBankAccountsId")
    var userBankAccountsId: Int
)