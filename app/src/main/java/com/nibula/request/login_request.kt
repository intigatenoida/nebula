package com.nibula.request


import com.google.gson.annotations.SerializedName

data class login_request(
    @SerializedName("Password")
    var password: String = "",
    @SerializedName("Username")
    var username: String = "",
    @SerializedName("CountryCode")
    var counntryCode: String = ""
)