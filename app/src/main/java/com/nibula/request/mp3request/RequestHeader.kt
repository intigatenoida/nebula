package com.nibula.request.mp3request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class RequestHeader(
    @SerializedName("DeviceIP")
    var deviceIP: String = ""
)