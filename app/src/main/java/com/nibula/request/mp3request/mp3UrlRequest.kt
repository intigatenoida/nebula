package com.nibula.request.mp3request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class mp3UrlRequest(
    @SerializedName("FileId")
    var fileId: Int = 0,
    @SerializedName("RequestHeader")
    var requestHeader: RequestHeader = RequestHeader()
)