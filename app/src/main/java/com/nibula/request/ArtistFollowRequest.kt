package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ArtistFollowRequest(
    @SerializedName("FollowingTo")
    var followingTo: String = "",
    @SerializedName("IsFollow")
    var isFollow: Int = 0
)