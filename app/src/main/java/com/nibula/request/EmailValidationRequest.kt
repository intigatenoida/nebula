package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class EmailValidationRequest(
    @SerializedName("EmailId")
    var emailId: String = "",
    @SerializedName("Otp")
    var otp: String = "",
    @SerializedName("MobileNo")
    var mobileNumber: String = "",
    @SerializedName("CountryCode")
    var counntryCode: String = ""
)