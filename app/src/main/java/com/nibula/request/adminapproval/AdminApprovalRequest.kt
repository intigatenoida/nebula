package com.nibula.request.adminapproval

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
class AdminApprovalRequest(

    @SerializedName("RecordId")
    val recordId: String = ""

)
