package com.nibula.request
import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class LogOutRequest(
    @SerializedName("FirebaseToken")
    var recordId: String? = ""
)