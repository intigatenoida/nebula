package com.nibula.request


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class ForgetPasswordRequest(
    @SerializedName("EmailId")
    var emailId: String = "",
    @SerializedName("MobileNo")
    var mobileNumber: String = "",
    @SerializedName("CountryCode")
    var counntryCode: String = ""
)