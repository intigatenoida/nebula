package com.nibula.request.myplaylist

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class MyPlayListRequest(
    @SerializedName("currentPage")
    var currentPage:Int?,
    @SerializedName("Search")
    var search:String?
)