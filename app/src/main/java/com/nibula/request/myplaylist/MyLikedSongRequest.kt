package com.nibula.request.myplaylist

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class MyLikedSongRequest(
    @SerializedName("currentPage")
    var currentPage:Int?,
    @SerializedName("recordsPerPage")
    var recordsPerPage:Int?
)
