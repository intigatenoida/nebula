package com.nibula.request


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class SharingRequest(
    @SerializedName("RecordId")
    var recordId: String,
    @SerializedName("SharedTo")
    var sharedTo: String,
    @SerializedName("RecordName")
    var recordName: String,
    @SerializedName("IsRelease")
    var isRelease: Int,
    @SerializedName("OfferingEndDate")
    var OfferingEndDate: String
)