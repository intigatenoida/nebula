package com.nibula.request


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class WithdrawRequest(
    @SerializedName("AvailableAmount")
    var availableAmount: String = "",
    @SerializedName("CurrencyTypeId")
    var currencyTypeId: Int = 0,
    @SerializedName("UserBankAccountId")
    var UserBankAccountId: Int = 0,
    @SerializedName("RequestedAmount")
    var requestedAmount: String = ""
)