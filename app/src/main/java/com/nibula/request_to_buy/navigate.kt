package com.nibula.request_to_buy

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.nibula.base.ForceUpdateDialog
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.response.details.AlbumDetailResponse
import com.nibula.response.details.File
import com.nibula.response.details.Response

interface navigate {

    fun gotoFinal(data: Response?, shareCount: Int)

    fun finishActivity()

    fun gotorequestinvest(albumId: String?)

    fun goToNewOffering(bundle: Bundle)

    fun goToCertificatesScreen(albumId: String?)
    fun goToViewCollectorsScreen(albumId: String?)

    fun goToProfileScreen()

    fun onClickNotification()

    fun onClickSearch()

    fun onClickBuy()


    fun buyFinal()

    fun finishBuyReleaseFragment()

    fun uploadRecord()


    fun openTopArtist(id: String, userTypeId: Int)

    fun manualBack(levelBack: Int = 0)

    fun goToPaymentSuccess(
        paymentSuccess: Boolean,
        record: Response,
        totalAmount: Double,
        shares: Int,
        investmentId: Int = 0
    )

    fun pushFragments(fg: Fragment)

    fun changeTopTab(changeTab: String)

    fun openInvestorScreen(recordDetail: AlbumDetailResponse)

    fun openCostBreakDown(costBreakdownResponse: CostBreakdownResponse.Response)

    fun openSavedDraftsList()

    fun selectedShareScreen(bundle: Bundle)

    fun goToBuyNow(bundle: Bundle)

    fun changeTab(tab: String)

    fun clearLogOut()

    fun onClickShare(recordId: String, title: String, isRelease: Int, date: String)

    fun openBankList(bundle: Bundle)

    fun addBank(bundle: Bundle?)

    fun showForceUpdateDialog(listener: ForceUpdateDialog.ForceUpdateListener)

    fun showFollower(artistId: Bundle)

    fun openChat(bundle: Bundle)


    fun openPendingFragment(recordId: String)
    fun onTokenPurchased(recordId: String)

    fun openDashboard()

    fun openWalletScreen()

fun openUploadSong()
    fun openMusicScreen()
    fun playSong(
        songIdList: MutableList<File>,
        selectedIndex: Int,
        fromTop: Boolean,
        recordTitleStr: String,
        recordId: String,
        isReleased: Int,
        date: String,
        isRecordChanged: Boolean = false)

    fun pauseMusic()
}