package com.nibula.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nibula.utils.AppConstant

@Entity(tableName = AppConstant.PLAYLIST_TABLE_NAME)
class PlayListTable(

    @PrimaryKey()
    @ColumnInfo(name = "RecordId")
    val RecordId: String,
    @ColumnInfo()
    var ArtistName: String,

    @ColumnInfo()
    var ArtistId: String,

    @ColumnInfo()
    var RecordTitle: String,

    @ColumnInfo()
    var startWith: String,

    @ColumnInfo()
    var RecordImage: String
)
