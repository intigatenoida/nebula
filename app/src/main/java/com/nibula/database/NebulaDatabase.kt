//package com.nibula.database
//
//import android.content.Context
//import androidx.room.Database
//import androidx.room.Room
//import androidx.room.RoomDatabase
//
//@Database(
//    entities = [RecentTable::class, PlayListTable::class],
//    version = 1,
//    exportSchema = false
//)
//public abstract class NebulaDatabase : RoomDatabase() {
//
//    abstract fun recentTableDao(): RecentTableDao
//    abstract fun playListDao(): PlayListDao
//
//
//    companion object {
//        private var instance: NebulaDatabase? = null
//        fun getInstance(context: Context): NebulaDatabase {
//            if (instance == null) {
//                synchronized(this) {
//                    instance = Room.databaseBuilder(
//                        context.applicationContext,
//                        NebulaDatabase::class.java,
//                        "NebulaDatabase"
//                    ).build()
//                }
//            }
//            return instance!!
//        }
//
//    }
//
//}