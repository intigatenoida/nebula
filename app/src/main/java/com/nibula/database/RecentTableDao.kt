package com.nibula.database

import androidx.room.*
import com.nibula.utils.AppConstant

@Dao
interface RecentTableDao {

    @Query("Select * from " + AppConstant.RECENT_TABLE_NAME)
    fun getAllData(): List<RecentTable>


    @Insert()
    fun insert(recentData: RecentTable)

    @Delete()
    fun delete(recentData: RecentTable)

    @Update()
    fun update(recentData: RecentTable)

    @Query("DELETE from " + AppConstant.RECENT_TABLE_NAME)
    fun deleteAll()


}