package com.nibula.database

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HowtoRoomWork : AppCompatActivity() {

    val activityScope = CoroutineScope(Dispatchers.IO)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setData()
    }

//    private fun getValueFromDatabase(): ArrayList<RecentTable> {
//        val dataList = ArrayList<RecentTable>()
//                activityScope.launch {
//            dataList.addAll(NebulaDatabase.getInstance(this@HowtoRoomWork).recentTableDao(().getAllData))
//        }
//        return dataList
//    }

    private fun setData() {
        for (i in 0..10) {
            val tableData = RecentTable(null, "", "name $i", "")
            activityScope.launch {
//           NebulaDatabase.getInstance(this@HowtoRoomWork).recentTableDao().insert(tableData)
            }
        }
    }
}