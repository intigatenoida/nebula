package com.nibula.database

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nibula.utils.AppConstant

@Entity(tableName = AppConstant.RECENT_TABLE_NAME)
data class RecentTable(

    @PrimaryKey(autoGenerate = true)
    @NonNull
    val id: Int?=null,

    @ColumnInfo()
    var itemName: String,

    @ColumnInfo()
    var itemPath: String,

    @ColumnInfo()
    var iconPath: String


)