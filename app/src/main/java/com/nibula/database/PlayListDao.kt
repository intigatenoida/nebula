package com.nibula.database

import androidx.room.*
import com.nibula.utils.AppConstant


@Dao
interface PlayListDao {

    @Query("Select * from " + AppConstant.PLAYLIST_TABLE_NAME)
    fun getAllData(): List<PlayListTable>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(recentData: PlayListTable)

    @Delete()
    fun delete(recentData: PlayListTable)

    @Update()
    fun update(recentData: PlayListTable)

    @Query("DELETE from " + AppConstant.PLAYLIST_TABLE_NAME)
    fun deleteAll()

    @Query("SELECT * FROM " + AppConstant.PLAYLIST_TABLE_NAME + " WHERE RecordId = :recordId")
    fun getItemByRecordId(recordId: String): PlayListTable?

}