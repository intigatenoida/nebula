/*
package com.nibula.fileEncDec

import android.content.Context
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.nibula.utils.AppConstant
import java.io.FileInputStream

class NebulaFileManager {
	
	//file name should be song id
	
	public interface FileDownloaderListener {
		fun onDownloadComplete()
		fun onError()
	}
	
	companion object {
		private fun startFileDownload(
			context: Context,
			url: String,
			fileName: String,
			listener: FileDownloaderListener
		) {
			PRDownloader.download(url, FileUtils.getDirPath(context), fileName).build()
				.start(object : OnDownloadListener {
					override fun onDownloadComplete() {
						encodeDownloadedFile(context, fileName, listener)
					}
					
					override fun onError(error: Error?) {
						listener.onError()
					}
					
				})
			
		}
		
		private fun encodeDownloadedFile(
			context: Context,
			fileName: String,
			listener: FileDownloaderListener
		) {
			try {
				val fileData = FileUtils.readFile(
					FileUtils.getFilePath(
						context,
						fileName */
/*+ AppConstant.POSTFIX*//*

					)
				)
				val encodedBytes = EncryptDecryptUtils.encode(
					EncryptDecryptUtils.getInstance(context).secretKey,
					fileData
				)
				FileUtils.saveFile(
					encodedBytes,
					FileUtils.getFilePath(context, fileName + AppConstant.POSTFIX)
				)
				listener.onDownloadComplete()
			} catch (e: Exception) {
				listener.onError()
			}
			finally {
				FileUtils.deleteDownloadedFile(context, fileName)
			}
		}
		
		private fun decodeFile(context: Context, fileName: String): ByteArray? {
			try {
				val fileData = FileUtils.readFile(
					FileUtils.getFilePath(
						context,
						fileName + AppConstant.POSTFIX
					)
				)
				return EncryptDecryptUtils.decode(
					EncryptDecryptUtils.getInstance(context).secretKey,
					fileData
				)
			} catch (e: java.lang.Exception) {
			
			}
			return null
		}
		
		private fun getFileForPlayer(context: Context, fileName: String) {
				FileUtils.getTempFileDescriptor(context, decodeFile(context, fileName))
		}
	}
}*/
