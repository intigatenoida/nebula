package com.nibula.finalsubmitrecord

import android.content.Intent
import android.os.Bundle
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.finalsubmitrecord.ui.main.FinalSubmitRecordFragment
import com.nibula.utils.RequestCode

class FinalSubmitRecordActivity : BaseActivity() {


    lateinit var fg: FinalSubmitRecordFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.final_submit_record_activity)
        fg = FinalSubmitRecordFragment.newInstance(intent?.extras)
        setFragment(fg, false)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCode.UPLOAD_SUCCESS_REQUEST_CODE) {
            fg.closeActivity()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}