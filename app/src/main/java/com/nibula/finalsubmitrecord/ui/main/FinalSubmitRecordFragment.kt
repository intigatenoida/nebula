package com.nibula.finalsubmitrecord.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nibula.R
import com.nibula.base.BaseFragment
import com.nibula.customview.CustomToast
import com.nibula.databinding.FinalSubmitRecordFragmentBinding
import com.nibula.finalsubmitrecord.ui.modal.FinalUploadScreenResponse
import com.nibula.request.upload.SubmitRecordRequest
import com.nibula.response.BaseResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.*

import java.util.*

class FinalSubmitRecordFragment : BaseFragment() {

    companion object {
        fun newInstance(extras: Bundle?) = FinalSubmitRecordFragment().apply {
            arguments = extras
        }
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var rootView: View
    private var recordId: String = ""
    private var submittedFieldsId = ""
    lateinit var binding:FinalSubmitRecordFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::rootView.isInitialized) {
            //rootView = inflater.inflate(R.layout.final_submit_record_fragment, container, false)
            binding=FinalSubmitRecordFragmentBinding.inflate(inflater,container,false)
            rootView=binding.root
        }
        initView()
        return rootView
    }

    private fun initView() {
        recordId = arguments?.getString(AppConstant.RECORD_ID, "").toString()
        submittedFieldsId = arguments?.getString(AppConstant.RECORD_ID_FIELDS, "").toString()
        getRecordDetails()

    }

    private fun getRecordDetails() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        showProcessDialog()
        val helper = ApiClient.getClientMusic(requireContext()).create(ApiAuthHelper::class.java)
        val call = helper.requestRecordDetailForFinalSubmit(recordId)
        call.enqueue(object : CallBackManager<FinalUploadScreenResponse>() {
            override fun onSuccess(any: FinalUploadScreenResponse?, message: String) {
                hideProcessDialog()
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    updateData(any?.response)
                } else if (any?.responseStatus != null &&
                    any.responseStatus == 0
                ) {
                    showToast(
                        context,
                        any.responseMessage!!,
                        CustomToast.ToastType.FAILED
                    )
                } else {
                    showToast(
                        context,
                        getString(R.string.message_something_wrong),
                        CustomToast.ToastType.FAILED
                    )
                }
            }

            override fun onFailure(message: String) {
                hideProcessDialog()
                showToast(
                    context,
                    getString(R.string.message_something_wrong),
                    CustomToast.ToastType.FAILED
                )
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
                showToast(
                    context,
                    getString(R.string.message_something_wrong),
                    CustomToast.ToastType.FAILED
                )
            }

        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        binding.tvSubmit.setOnClickListener {
            submitRecord()
        }

        binding.imgBack.setOnClickListener {
            activity?.setResult(Activity.RESULT_CANCELED)
            activity?.finish()
        }
    }

    fun submitRecord() {
        if (!isOnline()) {
            showToast(
                requireActivity(),
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }

        showProcessDialog()
        val calender = Calendar.getInstance()
        val localTime = calender.timeZone.displayName
        val helper = ApiClient.getClientMusic(requireContext()).create(ApiAuthHelper::class.java)
        val call = helper.requestSubmitRecord(
            SubmitRecordRequest(
                recordId,
                submittedFieldsId, localTime
            )
        )

        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                if (any?.responseStatus != null &&
                    any.responseStatus == 1
                ) {
                    hideProcessDialog()
                    showToast(// remove the success message
                        requireContext(),
                        any.responseMessage ?: "",
                        CustomToast.ToastType.SUCCESS
                    )
                    submitSuccess()
                    /*   println("Submitted record: $recordId")
                       val bPrompt = PNBSheetFragment(
                           getString(R.string.message_admin_approval),
                           getString(R.string.yes),
                           getString(R.string.no),
                           fg,
                           SpannableStringBuilder(""),
                           false,
                           3
                       )
                       bPrompt.isCancelable = true
                       bPrompt.setColor(
                           ContextCompat.getColor(requireContext(), R.color.colorAccent),
                           ContextCompat.getColor(requireContext(), R.color.white),
                           ContextCompat.getColor(requireContext(), R.color.gray_1_51per),
                           ContextCompat.getColor(requireContext(), R.color.gray_1_51per)
                       )
                       bPrompt.show(
                           childFragmentManager,
                           "PositiveNegativeBottomSheetFragment"
                       )*/
                } else if (any?.responseStatus != null &&
                    any.responseStatus == 0
                ) {
                    hideProcessDialog()
                    showToast(
                        context,
                        any.responseMessage!!,
                        CustomToast.ToastType.FAILED
                    )
                } else {
                    hideProcessDialog()
                    showToast(
                        context,
                        getString(R.string.message_something_wrong),
                        CustomToast.ToastType.FAILED
                    )
                }

            }

            override fun onFailure(message: String) {
                hideProcessDialog()
                showToast(
                    context,
                    getString(R.string.message_something_wrong),
                    CustomToast.ToastType.FAILED
                )
            }

            override fun onError(error: RetroError) {
                hideProcessDialog()
                showToast(
                    context,
                    getString(R.string.message_something_wrong),
                    CustomToast.ToastType.FAILED
                )
            }

        })

    }

    private fun submitSuccess() {
       /* startActivityForResult(
            Intent(requireContext(), UploadSuccessActivity::class.java),
            RequestCode.UPLOAD_SUCCESS_REQUEST_CODE
        )*/
    }

    fun updateData(data: FinalUploadScreenResponse.Response?) {
        with(rootView) {
            val userImage = PrefUtils.getValueFromPreference(requireContext(), PrefUtils.USER_IMAGE)
            CommonUtils.loadImageWithOutCache(
                requireContext(),
                userImage,
                binding.iv1,
                R.drawable.ic_record_user_place_holder
            )
            //:: Timer Remaining
            data?.offeringStartDate?.let {
                if (it != null) {
                    binding.relesedTime.text = TimestampTimeZoneConverter.convertToLocalDate(
                        it,
                        TimestampTimeZoneConverter.UTC_TIME
                    )
                }
            }
            Glide
                .with(context)
                .load(data!!.recordImage)
                .placeholder(R.drawable.nebula_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.IMMEDIATE)
                .into(binding.cvImage.clImage)

            binding.tvSongTitle.text = PrefUtils.getValueFromPreference(requireContext(), PrefUtils.NAME)

            //:: Album Title
            data?.recordType?.let { type ->
                data.recordTitle?.let { title ->
                    binding.tvAlbumTitle.text = "$type: $title"
                    binding.tvAlbumTitle.text = CommonUtils.setSpannable(
                        requireContext(),
                        binding.tvAlbumTitle.text as String,
                        0,
                        binding.tvAlbumTitle.text.split(":")[0].length + 1
                    )
                }
            }
            //:: Release Date ( calculated from offering End Date according to User TimeZone)
            data?.releaseDate?.let { endDate ->
                (getString(R.string.released_with_colon_new) + " " + TimestampTimeZoneConverter.convertToLocalDate(
                    endDate,
                    TimestampTimeZoneConverter.UTC_TIME
                )).also {
                    binding.releaseDateTv.text = it
                }
            }

            //:: Genere Type
            data?.genreType?.let { genereType ->
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    binding.tvHashTag.text = Html.fromHtml(genereType, Html.FROM_HTML_MODE_LEGACY)
                } else {
                    binding.tvHashTag.text = Html.fromHtml(genereType)
                }
            }
            //price
            ("${data?.currencyType}" + data?.priceForUnlimitedStream?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPriceUStream.text = it }
            ("${data?.currencyType}" + data?.pricePerRoyaltyShareWork?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also {
                binding.tvPriceWork.text = it
            }
            ("${data?.currencyType}" + data?.pricePerRoyaltyShareRecording?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPriceRecording.text = it }
            ("${data?.currencyType}" + data?.initialOfferingPerShare?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also {
                binding.tvPriceInitialOffering.text = it
            }

            //--spann
            binding.tvPriceNote.text=getPriceSpannableText(data)

            ("${data?.currencyType}" + data?.percentSharesPrice20?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPrice20.text = it }
            ("${data?.currencyType}" + data?.percentSharesPrice80?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPrice20.text = it }
            ("${data?.currencyType}" + data?.percentSharesPrice100?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }).also { binding.tvPrice100.text = it }

            binding.tvPriceNoteKeepOwing.text = getOwingText(data)
            binding.tvPriceNoteOwing.text = getOwing()


        }
    }

    private fun getOwing(): Spannable {
        val str =
            "Owning 20% collectively, your co-owners will earn with you each time the music airs, performs or streams."
        val spannable = SpannableString(str)

        val typeface = ResourcesCompat.getFont(requireContext(), R.font.pt_sans_bold)
        if (typeface != null) {
            spannable.setSpan(
                StyleSpan(typeface.style),
                7,
                11,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
        }
        return spannable
    }

    private fun getOwingText(data: FinalUploadScreenResponse.Response?): Spannable {
        val str = "You keep owning 80% of your music rights now valued at ${data?.currencyType} ${
            data?.percentSharesPrice80?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }
        }"

        val spannable = SpannableString(str)

        val typeface = ResourcesCompat.getFont(requireContext()!!, R.font.pt_sans_bold)

        val index2 = 55
        val last12 =
            index2 + data?.currencyType!!.length + "${data?.percentSharesPrice80!!}".length + 1

        spannable.setSpan(
            ForegroundColorSpan(
                ContextCompat.getColor(
                    requireContext()!!,
                    R.color.green_shade_1_100per
                )
            ),
            16,
            19,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        spannable.setSpan(
            typeface?.let { StyleSpan(it.style) },
            index2,
            last12,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        return spannable
    }

    private fun getPriceSpannableText(data: FinalUploadScreenResponse.Response?): SpannableString {
        val strToSpann = "By selling ${data?.sharesCount} shares worth ${data?.currencyType} ${
            data?.initialOfferingPerShare?.let {
                CommonUtils.trimDecimalToTwoPlaces(
                    it
                )
            }
        } each you can make " +
                "${data?.currencyType} ${
                    data?.percentSharesPrice20?.let {
                        CommonUtils.trimDecimalToTwoPlaces(
                            it
                        )
                    }
                } in total."
        val spannable = SpannableString(strToSpann)

        val typeface = ResourcesCompat.getFont(requireContext()!!, R.font.pt_sans_bold)

        val index1 = 11
        val last1 = index1 + "${data?.sharesCount!!}".length + 2

        val index2 = last1 + 13
        val last12 =
            index2 + data?.currencyType!!.length + "${data?.initialOfferingPerShare!!}".length + 2

        val index3 = last12 + 19
        val last3 =
            index3 + data?.currencyType!!.length + "${data?.percentSharesPrice20!!}".length + 3

        spannable.setSpan(
            StyleSpan(typeface!!.style),
            index1,
            last1,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )

        spannable.setSpan(
            StyleSpan(typeface.style),
            index2,
            last12,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        spannable.setSpan(
            StyleSpan(typeface!!.style),
            index3,
            last3,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        return spannable
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCode.UPLOAD_SUCCESS_REQUEST_CODE) {
            closeActivity()
        }
    }

    fun closeActivity() {
        val sendIntent = Intent()
        sendIntent.putExtra(
            "refresh", true
        )
        requireActivity().setResult(Activity.RESULT_OK, sendIntent)
        requireActivity().finish()
    }

}