package com.nibula.upload_music.ui.uploadrecord.helper

import android.content.Intent
import android.os.Bundle
import com.nibula.R
import com.nibula.base.BaseHelperFragment
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.customview.CustomToast
import com.nibula.request.mp3request.mp3UrlRequest
import com.nibula.response.mp3Response.mp3Response
import com.nibula.response.uploadrecord.UploadRecordFileResponse
import com.nibula.retrofit.*
import com.nibula.upload_music.ui.uploadrecord.activity.CostBreakDownActivity
import com.nibula.upload_music.ui.uploadrecord.adapter.UploadSongViewPageAdapter
import com.nibula.upload_music.ui.uploadrecord.fragment.FinalSubmitFragment
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call

class FinalSubmitHelper(
    val fg: FinalSubmitFragment,
    var viewPagerListener: UploadSongViewPageAdapter.Communicator
) : BaseHelperFragment() {
    lateinit var call: Call<UploadRecordFileResponse>
    fun finalSubmit() {
        fg.showProcessDialog()
        fg.showProcessDialog()
        var recordId: String = ""
        var recordFiledId: String = ""
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordId?.let {
            recordId = it
        }
        fg.uploadRecordDataViewModel.uploadRecordDataModel.recordFiledId?.let {
            recordFiledId = it
        }
        val helper =
            ApiClient.getClientMusic2(fg.requireContext()).create(ApiAuthHelper::class.java)
        call = helper.finalSubmitStep(
            recordId.toRequestBody("text/plain".toMediaTypeOrNull())
        )
        call.enqueue(object : CallBackManager<UploadRecordFileResponse>() {
            override fun onSuccess(any: UploadRecordFileResponse?, message: String) {
                if (any?.responseStatus != null) {
                    fg.hideProcessDialog()
                    if (any.responseStatus == 1) {
                        viewPagerListener.onSuccess()

                    } else {
                        viewPagerListener.failed(any?.responseMessage!!)
                    }
                } else {
                    fg.hideProcessDialog()
                }
            }

            override fun onFailure(message: String) {
                fg.hideProcessDialog()

            }

            override fun onError(error: RetroError) {
                if (error.kind != RetroError.Kind.API_CALL_CANCEL) {
                    fg.hideProcessDialog()

                }
            }

        })

    }
    fun getSongUrlFinalStep(songId: Int, listener: OnSongClick) {
        if (!fg.isOnline()) {
            CustomToast.showToast(
                fg.requireActivity(),
                fg.requireActivity().getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        val helper: ApiAuthHelper?

        helper = ApiClient.getClientMusic().create(ApiAuthHelper::class.java)

        val request = mp3UrlRequest()
        request.fileId = songId
        request.requestHeader.deviceIP = "106.201.10.241"  //TODO remove hard code IP
        val call = helper.getSongUrl(request)
        call.enqueue(object : CallBackManager<mp3Response>() {
            override fun onSuccess(any: mp3Response?, message: String) {
                val response = any as mp3Response
                if (response.responseStatus != 1 || response.response?.songFilePath.isNullOrEmpty()) {
                    response.responseMessage?.let {
                        CustomToast.showToast(
                            fg.requireActivity(),
                            it,
                            CustomToast.ToastType.FAILED
                        )
                    }
                    return
                }

                response?.response?.let {
                    val songUrl = it.songFilePath
                    fg.songFilePathUrl = songUrl
                    listener.onSongClick(songUrl)

                }
            }

            override fun onFailure(message: String) {
                CustomToast.showToast(
                    fg.requireActivity(),
                    message,
                    CustomToast.ToastType.NETWORK
                )
            }

            override fun onError(error: RetroError) {
                fg.requireActivity()
            }

        })

    }
    fun getCostBreakDown(albumId: String?, shares: Int = 1) {
        if (!fg.isOnline(fg.requireActivity())) {
            fg.showToast(
                fg.requireActivity(),
                fg.getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            fg.hideProcessDialog()
            return
        }


        val helper: ApiAuthHelper?
        if (CommonUtils.isLogin(fg.requireContext())) {
            helper =
                ApiClient.getClientMusic(fg.requireContext()).create(ApiAuthHelper::class.java)
        } else {
            helper = ApiClient.getClientMusic().create(ApiAuthHelper::class.java)
        }

        val observable = helper.getCostBreakDown(albumId ?: "", shares)
        disposables.add(sendApiRequest(observable)!!.subscribeWith(object :
            CallbackWrapper<CostBreakdownResponse>() {
            override fun onSuccess(any: CostBreakdownResponse?, message: String?) {
                if (any?.response != null) {
                    if (any.responseStatus == 1) {
                        val res = any.response
                        if (res != null) {
                            fg.costBreakdownResponse = res
                            val bundle = Bundle()
                            bundle.putParcelable(AppConstant.COST_BREAKDOWN, fg.costBreakdownResponse)
                            val intent =
                                Intent(fg.requireActivity(), CostBreakDownActivity::class.java)
                            intent.putExtras(bundle)
                            fg.startActivity(intent)
                            // fg.navigate.openCostBreakDown(fg.costBreakdownResponse)
                        }
                    } else {
                        CustomToast.showToast(
                            fg.requireContext(),
                            any.responseMessage
                                ?: fg.getString(R.string.message_something_wrong),
                            CustomToast.ToastType.FAILED
                        )
                    }
                } else {
                    CustomToast.showToast(
                        fg.requireContext(),
                        fg.getString(R.string.message_something_wrong),
                        CustomToast.ToastType.FAILED
                    )
                }

            }

            override fun onError(error: RetroError?) {
                CustomToast.showToast(
                    fg.requireContext(),
                    error?.errorMessage ?: "",
                    CustomToast.ToastType.FAILED
                )
            }
        }))
    }
    interface OnSongClick {
        fun onSongClick(path: String)
    }

}