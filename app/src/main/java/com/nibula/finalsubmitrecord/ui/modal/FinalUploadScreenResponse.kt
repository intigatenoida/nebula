package com.nibula.finalsubmitrecord.ui.modal


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.nibula.response.BaseResponse

@Keep
data class FinalUploadScreenResponse(
    @SerializedName("Response")
    var response: Response? = Response()
):BaseResponse() {
    @Keep
    data class Response(
        @SerializedName("CurrencyType")
        var currencyType: String? = "",
        @SerializedName("CurrencyTypeAbbreviation")
        var currencyTypeAbbreviation: String? = "",
        @SerializedName("GenreType")
        var genreType: String? = "",
        @SerializedName("Id")
        var id: String? = "",
        @SerializedName("InitialOfferingPerShare")
        var initialOfferingPerShare: Double? = 0.0,
        @SerializedName("OfferingStartDate")
        var offeringStartDate: String? = "",
        @SerializedName("_20PercentSharesPrice")
        var percentSharesPrice20: Double? = 0.0,
        @SerializedName("_80PercentSharesPrice")
        var percentSharesPrice80: Double? = 0.0,
        @SerializedName("_100PercentSharesPrice")
        var percentSharesPrice100: Double? = 0.0,
        @SerializedName("PriceForUnlimitedStream")
        var priceForUnlimitedStream: Double? = 0.0,
        @SerializedName("PricePerRoyaltyShareRecording")
        var pricePerRoyaltyShareRecording: Double? = 0.0,
        @SerializedName("PricePerRoyaltyShareWork")
        var pricePerRoyaltyShareWork: Double? = 0.0,
        @SerializedName("RecordImage")
        var recordImage: String? = "",
        @SerializedName("RecordStatusType")
        var recordStatusType: String? = "",
        @SerializedName("RecordStatusTypeId")
        var recordStatusTypeId: Int? = 0,
        @SerializedName("RecordTitle")
        var recordTitle: String? = "",
        @SerializedName("RecordType")
        var recordType: String? = "",
        @SerializedName("RecordTypeId")
        var recordTypeId: Int? = 0,
        @SerializedName("ReleaseDate")
        var releaseDate: String? = "",
        @SerializedName("SharesCount")
        var sharesCount: Int? = 0
    )
}