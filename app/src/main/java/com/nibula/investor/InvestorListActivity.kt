package com.nibula.investor

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nibula.base.BaseActivity
import com.nibula.databinding.ActivityInvestorBinding
import com.nibula.utils.CommonUtils


class InvestorListActivity : BaseActivity() {
    lateinit var binding:ActivityInvestorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
/*
        setContentView(R.layout.activity_investor)
*/

        binding=ActivityInvestorBinding.inflate(layoutInflater)
        binding.header.tvTitle.text=CommonUtils.setSpannable(this@InvestorListActivity,binding.header.tvTitle.text.toString(),0,binding.header.tvTitle.text.toString().split(":")[0].length)
       // binding.tvTitle.text=CommonUtils.setSpannable(this@InvestorListActivity,tvTitle.text.toString(),0,tvTitle.text.toString().split(":")[0].length)
        val adapter=InvestorAdapter(this)
        val layoutManager=LinearLayoutManager(this,RecyclerView.VERTICAL,false)
        binding.rvInvestor.layoutManager=layoutManager
        binding.rvInvestor.adapter=adapter

    }

}