package com.nibula.purchage_details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.nibula.R
import com.nibula.purchage_details.ui.purchagedetails.PurchageDetailsFragment

class PurchageDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.purchage_details_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, PurchageDetailsFragment.newInstance())
                .commitNow()
        }
    }
}
