package com.nibula.purchage_details.ui.purchagedetails

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nibula.R

class PurchageDetailsFragment : Fragment() {

    companion object {
        fun newInstance() = PurchageDetailsFragment()
    }

    private lateinit var viewModel: PurchageDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.purchage_details_fragment, container, false)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PurchageDetailsViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
