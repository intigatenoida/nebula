package com.nibula.utils

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils

/**
 * Created by pankajk on .
 *  All rights reserved by Intigate Technologies.
 */
class PrefUtils {
    companion object {

        const val USER_ID: String = "USER_ID"
        const val NAME: String = "NAME"
        const val TOKEN: String = "TOKEN"
        const val TOKEN_TYPE: String = "TOKEN_TYPE"
        const val TOKEN_TIME: String = "TOKEN_TIME"
        const val USER_IMAGE ="userImage"
        const val NOTIFICATION_TOKEN ="NOTIFICATION_TOKEN"
        const val EMAIL: String = "EMAIL"
        const val PHONE_NO: String = "PHONENO"
        const val APP_VERSION: String = "APP_VERSION"
        const val FORCE_UPDATE: String = "FORCE_UPDATE"
        const val IMAGE: String = "IMAGE"
        const val IS_GUIDE_PAGE_ENABLE: String = "IS_GUIDE_PAGE_ENABLE"
        const val IS_USER_SKIP_FROM_LOGIN = "IS_USER_SKIP_FROM_LOGIN"
        const val WALK_THROUGH_REPONSE = "WALK_THROUGH_REPONSE"
        const val KEY = "KEY"
        const val CONNECTED_WALLET_ADDRESS = "wallet_address"
        const val CONNECTED_WALLET_ACCOUNT = "wallet_account"
        const val SELECTED_SESSION_TOPIC = "selectedSessionTopic"
        const val USER_IN = "user_in"
        const val MY_MONEY = "MY_MONEY"
        const val notificationCount = "notificationCount"
        const val referralCount = "referralCount"
        const val PLAYLIST_REFRESH = "PLAYLIST_REFRESH"
        const val BANKLIST_REFRESH = "BANKLIST_REFRESH"
        const val SELECTED_BANK_ID = "SELECTED_BANK_ID"
        const val IS_EMAIL_VALID: String = "IS_EMAIL_VALID"
        const val IS_PHONE_VALID: String = "IS_PHONE_VALID"
        const val SONG_ID: String = "SONG_ID"
        const val PLAYING_DURATION: String = "PLAYING_DURATION"


        private fun initSharedPreference(context: Context): SharedPreferences {
            return context.getSharedPreferences("APPLICATION_PREF", Context.MODE_PRIVATE)
        }

        private fun initEditor(context: Context): SharedPreferences.Editor {
            return initSharedPreference(context).edit()
        }

        fun saveValueInPreference(context: Context, key: String, value: String) {
            initEditor(context).putString(key, value).commit()
        }

        fun saveValueInPreference(context: Context, key: String, value: Int) {
            initEditor(context).putInt(key, value).commit()
        }

        fun saveValueInPreference(context: Context, key: String, value: Float) {
            initEditor(context).putFloat(key, value).commit()
        }

        fun saveValueInPreference(context: Context, key: String, value: Boolean) {
            initEditor(context).putBoolean(key, value).commit()
        }


        fun getValueFromPreference(context: Context, key: String): String {
            var value = initSharedPreference(context).getString(key, "")!!
            if (TextUtils.isEmpty(value) || value.equals("null")) {
                value = ""
            }
            return value
        }

        fun getIntValue(context: Context, key: String): Int {
            val value = initSharedPreference(context).getInt(key, 0)
            if (value != null && value > 0) {
                return value
            }
            return 0
        }

        fun getFloatValue(context: Context, key: String): Float {
            val value = initSharedPreference(context).getFloat(key, 0.0f)
            if (value != null && value > 0) {
                return value
            }
            return 0.0f
        }

        fun getBooleanValue(context: Context, key: String): Boolean {
            val value = initSharedPreference(context).getBoolean(key, false)
            if (value != null) {
                return value
            }
            return false
        }

        fun logout(context: Context) {
            val walkThrough = getValueFromPreference(
                context,
                IS_GUIDE_PAGE_ENABLE
            )
            val settings = context.getSharedPreferences("APPLICATION_PREF", Context.MODE_PRIVATE)
            settings.edit().clear().commit()
            saveValueInPreference(context, IS_GUIDE_PAGE_ENABLE, walkThrough)



        }

    }
}