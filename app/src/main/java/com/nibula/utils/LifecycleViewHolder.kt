package com.nibula.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.nibula.counter.TimerCounter
import com.nibula.counter.TimerParams
import com.nibula.response.artists.FreaturedArtistResponse

abstract class LifecycleViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView), LifecycleOwner {

    private var lifecycleRegistry: LifecycleRegistry

    init {
        lifecycleRegistry = createLifeCycle()
    }

    private fun createLifeCycle(): LifecycleRegistry {
        val lifecycle = LifecycleRegistry(this)
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
        return lifecycle
    }

    @CallSuper
    open fun onAttached() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    @CallSuper
    open fun onDetached() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    }

    @CallSuper
    open fun onRecycled() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        lifecycleRegistry = createLifeCycle()
    }

    override fun getLifecycle(): Lifecycle = lifecycleRegistry

    fun removeObserver(timeUpdate: MutableLiveData<Long?>) {
        timeUpdate.removeObservers(this)
    }

    fun registerTimerLiveData(
        context: Context,
        timeUpdate: MutableLiveData<Long?>,
        thisMillisStart: Long,
        tvTimer: AppCompatTextView?,
        data: TimerParams?,
        callback: TimerCounter.TimerItemRemove
    ) {
        timeUpdate.observe(
            this,
            Observer {
                if (it != null) {
                    if (tvTimer != null && adapterPosition > -1 && data != null) {
                        val millisUntilFinished = it ?: 0
                        val actualMillis = TimestampTimeZoneConverter.convertToMilliseconds(
                            data.tltr ?: 0
                        )
                        val temp =
                            actualMillis - (thisMillisStart - millisUntilFinished)
                        //if (temp in 1..TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                        //view Holder record is lie inside 1.5 days window
                        if (!data.isFinish &&
                            temp > 0
                        ) {
                            val time =
                                TimestampTimeZoneConverter.getDaysAndHourDifference(
                                    context,
                                    temp
                                )

                            /*  val dayHourDifference = TimestampTimeZoneConverter.getDaysAndHours(
                                  context,
                                  temp
                              )*/
                            if (data.topLabel.isNullOrEmpty() ||
                                data.topLabel!! != time.toString() ||
                                tvTimer.text.isNullOrEmpty()
                            ) {
                                tvTimer.visibility = View.VISIBLE
                                tvTimer.text = time
                                data.topLabel = time.toString()
                                data.remainingMillis = temp
                            }
                        } else {
                            tvTimer.visibility = View.INVISIBLE
                        }
                    }
                }
            })
    }


    fun registerTimerLiveDataNew(
        context: Context,
        timeUpdate: MutableLiveData<Long?>,
        thisMillisStart: Long,
        tvTimer: AppCompatTextView?,
        data: TimerParams?,
        callback: TimerCounter.TimerItemRemove
    ) {
        timeUpdate.observe(
            this,
            Observer {
                if (it != null) {
                    if (tvTimer != null && adapterPosition > -1 && data != null) {
                        val millisUntilFinished = it ?: 0
                        val actualMillis = TimestampTimeZoneConverter.convertToMilliseconds(
                            data.tltr ?: 0
                        )
                        val temp =
                            actualMillis - (thisMillisStart - millisUntilFinished)
//                        if (temp in 1..TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                        //view Holder record is lie inside 1.5 days window
                        if (!data.isFinish &&
                            temp > 0
                        ) {
                            val time =
                                TimestampTimeZoneConverter.getDaysAndHourDifference(
                                    context,
                                    temp
                                )

                            val dayHourDifference = TimestampTimeZoneConverter.getDaysAndHours(
                                context,
                                temp
                            )
                            if (data.topLabel.isNullOrEmpty() ||
                                data.topLabel!! != time.toString() ||
                                tvTimer.text.isNullOrEmpty()
                            ) {
                                tvTimer.visibility = View.VISIBLE
                                tvTimer.text = dayHourDifference
                                data.topLabel = time.toString()
                                data.remainingMillis = temp
                            }
                        } else {
                            tvTimer.visibility = View.INVISIBLE
                        }
                    }
                }
            })
    }

    fun registerTimerLiveDataForFreaturedItem(
        context: Context,
        timeUpdate: MutableLiveData<Long?>,
        thisMillisStart: Long,
        tvDays: AppCompatTextView?, tvhours: AppCompatTextView?, tvMinutes: AppCompatTextView?,
        data: FreaturedArtistResponse?,
        callback: TimerCounter.TimerItemRemove
    ) {
        timeUpdate.observe(
            this,
            Observer {
                if (it != null) {

                    if (data?.RecordStatusTypeId.equals("2")) {
                        if (tvDays != null && adapterPosition > -1 && data != null) {
                            val millisUntilFinished = it ?: 0
                            val actualMillis = TimestampTimeZoneConverter.convertToMilliseconds(
                                data.tltr ?: 0
                            )
                            val temp =
                                actualMillis - (thisMillisStart - millisUntilFinished)
//                        if (temp in 1..TimerCounter.MAX_DAY_COUNTER_LIMIT) {
                            //view Holder record is lie inside 1.5 days window
                            if (!data.isFinish &&
                                temp > 0
                            ) {
                                val time =
                                    TimestampTimeZoneConverter.getDaysAndHourDifferenceNew(
                                        context,
                                        temp, tvDays, tvhours, tvMinutes
                                    )
                                if (data.topLabel.isNullOrEmpty() ||
                                    data.topLabel!! != time.toString() ||
                                    tvDays.text.isNullOrEmpty()
                                ) {

                                    data.topLabel = time.toString()
                                    data.remainingMillis = temp
                                }
                            } else {
                                //tvTimer.visibility = View.INVISIBLE
                            }
                        }
                    }

                }
            })
    }
}