package com.nibula.utils;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Base64;

public class JwtDecoder {


    public void decodeToken(Context mContext) throws JSONException {
        String jwtToken = PrefUtils.Companion.getValueFromPreference(mContext, PrefUtils.TOKEN);
        String[] split_string = jwtToken.split("\\.");
        String base64EncodedBody = split_string[1];

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Base64.Decoder decoder = Base64.getUrlDecoder();
            String body = new String(decoder.decode(base64EncodedBody));
            JSONObject jsonObject = new JSONObject(body);
            if (jsonObject != null) {
                if (jsonObject.has("verifiedEmail")) {
                    PrefUtils.Companion.saveValueInPreference(mContext, PrefUtils.IS_EMAIL_VALID, jsonObject.getBoolean("verifiedEmail"));
                }
                if (jsonObject.has("verifiedPhone")) {
                    PrefUtils.Companion.saveValueInPreference(mContext, PrefUtils.IS_PHONE_VALID, jsonObject.getBoolean("verifiedPhone"));
                }
            }
        }
    }
}
