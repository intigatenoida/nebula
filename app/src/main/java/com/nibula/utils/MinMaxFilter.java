package com.nibula.utils;

import android.text.InputFilter;
import android.text.Spanned;

public class MinMaxFilter implements InputFilter {

    private Double mIntMin, mIntMax;

    public MinMaxFilter(int minValue, int maxValue) {
        this.mIntMin = Double.valueOf(minValue);
        this.mIntMax = Double.valueOf(maxValue);
    }

    public MinMaxFilter(String minValue, String maxValue) {
        this.mIntMin = Double.valueOf(minValue);
        this.mIntMax = Double.valueOf(maxValue);
    }

    public MinMaxFilter(Double minValue, Double maxValue) {
        this.mIntMin = minValue;
        this.mIntMax = maxValue;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            Double input = Double.valueOf(dest.toString() + source.toString());
            if (isInRange(mIntMin, mIntMax, input))
                return null;
        } catch (NumberFormatException nfe) {
        }
        return "";
    }

    private boolean isInRange(Double a, Double b, Double c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
