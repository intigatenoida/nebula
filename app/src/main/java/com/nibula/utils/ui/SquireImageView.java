package com.nibula.utils.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.makeramen.roundedimageview.RoundedImageView;

public class SquireImageView extends RoundedImageView {
    public SquireImageView(Context context) {
        super(context);
    }

    public SquireImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquireImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int width, int height) {
        super.onMeasure(width, height);
        int measuredWidth = getMeasuredWidth();
            setMeasuredDimension(measuredWidth, measuredWidth);

    }
}
