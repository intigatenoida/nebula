package com.nibula.utils.ui;

import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.text.style.SuperscriptSpan;

public class TopAlignSuperscriptSpan extends MetricAffectingSpan {
    double ratio = 0.5;

    public TopAlignSuperscriptSpan() {
    }

    public TopAlignSuperscriptSpan(double ratio) {
        this.ratio = ratio;
    }

    @Override
    public void updateDrawState(TextPaint paint) {
        paint.baselineShift += (int) (paint.ascent() * ratio);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        paint.baselineShift += (int) (paint.ascent() * ratio);
    }
}