package com.nibula.utils

import android.os.SystemClock
import android.view.View
import java.util.*

/**
 * Created by Shubham Kumar Saini

 * Rejects clicks that are too close together in time.
 */
abstract class DebouncedOnClickListener(private val minimumIntervalMillis: Long) :
    View.OnClickListener {
    private val lastClickMap: MutableMap<View, Long>

    abstract fun onDebouncedClick(v: View?)
    override fun onClick(clickedView: View) {
        val previousClickTimestamp = lastClickMap[clickedView]
        val currentTimestamp = SystemClock.uptimeMillis()
        lastClickMap[clickedView] = currentTimestamp
        if (previousClickTimestamp == null || Math.abs(currentTimestamp - previousClickTimestamp) > minimumIntervalMillis) {
            onDebouncedClick(clickedView)
        }
    }

    init {
        lastClickMap = WeakHashMap()
    }
}