package com.nibula.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.nibula.R

class SpinnerAdapterBank() : BaseAdapter() {

    private var layoutInflater: LayoutInflater? = null
    private var data: MutableList<String>? = null
    private var context: Context? = null

    constructor(context: Context, data: MutableList<String>) : this() {
        this.data = data
        this.context = context
        this.layoutInflater = LayoutInflater.from(context)
    }

    fun setData(data: MutableList<String>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun clear() {
        this.data!!.clear()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (convertView == null) {
            view = layoutInflater!!.inflate(R.layout.layout_item_spinner_bank, parent, false)
        }
        val tvTitle = view!!.findViewById<AppCompatTextView>(R.id.text1)
        tvTitle.text = data!![position]
        //view.setPadding(getDp(4), getDp(10), getDp(10), getDp(10))
        if (position != 0) {
            tvTitle.setTextColor(ContextCompat.getColor(context!!, R.color.white))
        } else {
            tvTitle.setTextColor(ContextCompat.getColor(context!!, R.color.gray_1_50per))
        }
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (convertView == null) {
            view = layoutInflater!!.inflate(R.layout.layout_item_spinner_drop, parent, false)
        }
        val tvTitle = view!!.findViewById<AppCompatTextView>(R.id.text1)
        tvTitle.text = data!![position]
        if (position != 0) {
            tvTitle.setTextColor(ContextCompat.getColor(context!!, R.color.white))
            //view.setPadding(getDp(15), 0, getDp(15), getDp(15))
        } else {
            tvTitle.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
            //view.setPadding(getDp(15), getDp(15), getDp(15), getDp(15))
        }
        return view
    }


    override fun getItem(position: Int): Any {
        return data!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data!!.size
    }

    private fun getDp(size: Int): Int {
        val scale: Float = context!!.resources.displayMetrics.density
        return (size * scale + 0.5f).toInt()
    }

    override fun isEnabled(position: Int): Boolean {
        return position != 0
    }
}