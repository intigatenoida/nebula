package com.nibula.utils

object RequestCode {
    const val LOGIN_REQUEST_CODE = 100
    const val ARTIST_REQUEST_CODE = 100
    const val LOGIN_REQUEST_CODE_DETAILS = 101
    const val LOGIN_TO_SIGNUP = 103
    //-- TO check when opening assest
    const val LOGIN_REQUEST_CODE_ASSESTS = 102
    const val LOGIN_REQUEST_CODE_MUSIC = 500
    const val REQUEST_SONG_FILES_SELECTION = 104
    const val REQUEST_RECORD_SUBMIT = 105
    const val REQUEST_CHANGE_LOCAL_RECORD = 106
    const val REQUEST_EDIT_SONG_SCREEN = 107
    const val LOGIN_REQUEST_SONG_PLAY = 108
    const val PAYPAL_REQUEST_CODE = 109
    const val LOGIN_REQUEST_CODE_2 = 110
    const val REQUEST_SONG_FILES_SELECTION_ADD = 111
    const val LOGIN_REQUEST_CODE_NOTIFY = 112
    const val LOGIN_REQUEST_CODE_UPLOAD = 113
    const val STRIP_REQUEST_CODE = 114
    const val UPLOAD_SUCCESS_REQUEST_CODE = 115
    const val LOGIN_REQUEST_CODE_SHARE = 116
    const val PAYPAL_BRAINTREE_CODE = 117
    const val PAYPAL_SUCCESS = 900
    const val PAYPAL_WEB_VIEW_RESULT_CODE = 200
    const val PAYPAL_FAILED = 800

}