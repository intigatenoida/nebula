package com.nibula.utils;

import static android.app.NotificationManager.IMPORTANCE_HIGH;
import static androidx.core.app.NotificationCompat.PRIORITY_MAX;
import static com.google.android.exoplayer2.C.CONTENT_TYPE_MUSIC;
import static com.google.android.exoplayer2.C.USAGE_MEDIA;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import androidx.annotation.Nullable;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;

import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.nibula.R;
import com.nibula.dashboard.DescriptionAdapter;
import com.nibula.response.details.File;
import java.util.List;

public class AudioPlayerService extends Service implements PlayerNotificationManager.NotificationListener, AudioManager.OnAudioFocusChangeListener {
    private final IBinder mBinder = new LocalBinder();
    private ExoPlayer player;

    private PlayerNotificationManager playerNotificationManager;
    private boolean didPlayerPauseFromAudioFocus = false;
    private MediaSessionCompat mediaSession;

    @Override
    public void onCreate() {
        super.onCreate();
        player = new ExoPlayer.Builder(getApplicationContext()).build();

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(USAGE_MEDIA)
                .setContentType(CONTENT_TYPE_MUSIC)
                .build();
        player.setAudioAttributes(audioAttributes, true);
    }

    @Override
    public void onDestroy() {
        stopSelf();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stopForeground(STOP_FOREGROUND_REMOVE);
        }
        releasePlayer();
        Log.d("playeranr", "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onNotificationPosted(int notificationId, Notification notification, boolean ongoing) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            startForeground(notificationId, notification);
        }

    }

    @Override
    public void onNotificationCancelled(int notificationId, boolean dismissedByUser) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stopForeground(Service.STOP_FOREGROUND_REMOVE);
        }
        stopSelf();
        Log.d("playeranr", "onNotificationCancelled");
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
            if (playerNotificationManager != null) {
                playerNotificationManager.setPlayer(null);
            }
            playerNotificationManager = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public ExoPlayer getplayerInstance() {
        if (player == null) {
            startPlayer();
        }
        Log.d("playeranr", "getplayerInstance");
        return player;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        releasePlayer();

        if (player == null) {
            startPlayer();
        }
        Log.d("playeranr", "onStartCommand");
        return START_NOT_STICKY;
    }

    private void startPlayer() {
        AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        final Context context = this;
        //player = ExoPlayerFactory.newSimpleInstance(context);
        player.setRepeatMode(Player.REPEAT_MODE_OFF);

    }


    public void prepareSong(String songUrl, List<File> songTitle, String recordImagePath, String recordId, int position) {
        if (player == null) {
            startPlayer();
        }
        Log.d("recordId", recordId);
        ConcatenatingMediaSource concatenatingMediaSource = new ConcatenatingMediaSource();
        ProgressiveMediaSource progressiveMediaSource;
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "exo-demo"));
        for (File songDetails : songTitle) {
            Uri uri;
//          uri = Uri.parse("https://nebula-store.s3.eu-central-1.amazonaws.com/media/records/mediaconvert/output/20200824125529994.trailer_cli.m3u8");
            uri = Uri.parse(songUrl);
            if (uri.toString().contains(".m3u8")) {
                HlsMediaSource ns = new HlsMediaSource.Factory(dataSourceFactory).setTag(songDetails).createMediaSource(uri);
                concatenatingMediaSource.addMediaSource(ns);
            } else {
                progressiveMediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory).setTag(songDetails)
                        .createMediaSource(uri);
                concatenatingMediaSource.addMediaSource(progressiveMediaSource);
            }
        }
        player.prepare(concatenatingMediaSource);

        playerNotificationManager = new PlayerNotificationManager.Builder(this, 444, getString(R.string.default_notification_channel_id))
                .setChannelImportance(IMPORTANCE_HIGH)
                .setSmallIconResourceId(R.drawable.ic_nebula_new)
                .setChannelDescriptionResourceId(R.string.app_name)
                .setChannelNameResourceId(R.string.app_name)
                .setMediaDescriptionAdapter(new DescriptionAdapter(this, songTitle, recordImagePath, recordId))
                .build();

        playerNotificationManager.setPlayer(player);
        playerNotificationManager.setPriority(PRIORITY_MAX);
        playerNotificationManager.setUseRewindAction(false);
        playerNotificationManager.setUsePlayPauseActions(true);
        playerNotificationManager.setUseFastForwardAction(true);

        player.seekTo(position, C.TIME_UNSET);
        player.setPlayWhenReady(true);

        mediaSession = new MediaSessionCompat(this, "rtesfd");
        mediaSession.setActive(true);
        playerNotificationManager.setMediaSessionToken(mediaSession.getSessionToken());
        try {
            MediaSessionConnector mediaSessionConnector = new MediaSessionConnector(mediaSession);

            mediaSessionConnector.setQueueNavigator(new TimelineQueueNavigator(mediaSession) {
                @Override
                public MediaDescriptionCompat getMediaDescription(Player player, int windowIndex) {

                    return new MediaDescriptionCompat.Builder()
                            .setMediaId("" + songTitle.get(windowIndex).getId())
                            .setTitle(songTitle.get(windowIndex).getArtistName())
                            .setDescription(songTitle.get(windowIndex).getSongTitle())
                            .build();
                }
            });
            mediaSessionConnector.setPlayer(player);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("playeranr", "onUnbind");
        stopSelf();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stopForeground(STOP_FOREGROUND_REMOVE);
        }
        releasePlayer();
        return super.onUnbind(intent);
    }

    public void stopPlayer() {
        player.setPlayWhenReady(false);
        player.stop();
        player.seekTo(0);
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        if (isPlaying() && focusChange != 1) {
            pausePlayer();
            didPlayerPauseFromAudioFocus = true;
        } else {
            if (didPlayerPauseFromAudioFocus) {
                resumePlayer();
                didPlayerPauseFromAudioFocus = false;
            }
        }
    }

    public class LocalBinder extends Binder {
        public AudioPlayerService getService() {
            return AudioPlayerService.this;
        }
    }

    private void pausePlayer() {
        if (player == null) {
            return;
        }
        player.setPlayWhenReady(false);
        player.getPlaybackState();
    }

    private void resumePlayer() {
        if (player == null) {
            return;
        }
        player.setPlayWhenReady(true);
        player.getPlaybackState();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stopForeground(STOP_FOREGROUND_REMOVE);
        }
        releasePlayer();
        Log.d("playeranr", "onTaskRemoved");
        super.onTaskRemoved(rootIntent);

    }
    private boolean isPlaying() {
        if (player == null) {
            return false;
        }
        return player != null
                && player.getPlaybackState() != Player.STATE_ENDED
                && player.getPlaybackState() != Player.STATE_IDLE
                && player.getPlayWhenReady();
    }
}
