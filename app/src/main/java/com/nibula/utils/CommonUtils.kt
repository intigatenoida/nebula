package com.nibula.utils

import android.app.Activity
import android.app.Dialog
import android.content.ContentUris
import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.Settings
import android.text.*
import android.text.style.AbsoluteSizeSpan
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nibula.R
import com.nibula.base.BaseApplication
import com.nibula.permissions.PermissionHelper
import com.nibula.request.RegisterRequest
import com.nibula.response.balance.MyBalanceResponse
import com.nibula.response.login.LoginResponse
import com.nibula.response.registerResponse.RegisterResponse
import com.nibula.response.signup.SignupResponse
import com.nibula.retrofit.ApiClient
import com.nibula.utils.ui.TopAlignSuperscriptSpan
import java.math.BigInteger
import java.math.RoundingMode
import java.net.InetAddress
import java.nio.ByteOrder
import java.text.DateFormatSymbols
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

open class CommonUtils {

    companion object {

        var DEBUG: Boolean = true

        fun isLogin(context: Context): Boolean {
            return PrefUtils.getValueFromPreference(context, PrefUtils.USER_ID).isNotEmpty()
        }

        val THUMBNAIL = 0.03f
        fun setSpannable(
            context: Context,
            text: String,
            startIndex: Int,
            lastIndex: Int
        ): Spannable {
            val spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE
            )

            return spannable
        }

        //--> For Success Screen
        fun setSpannableRed(
            context: Context?,
            text: String?,
            recordType: String?,
            sharesPurchased: Int,
            textValue: String
        ): Spannable {
            val spannable = SpannableString(textValue)
            val startIndex = 32 + recordType!!.length + 3
            val lastIndex = 32 + recordType.length + text!!.length + 4
            val secondIndex = 32 + recordType.length + text.length + 14
            val secondlastindex =
                32 + recordType.length + text.length + 14 + sharesPurchased.toString().length

            spannable.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.colorAccent)),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE
            )

            spannable.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.colorAccent)),
                secondIndex,
                secondlastindex,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE
            )

            spannable.setSpan(
                AbsoluteSizeSpan(16, true), startIndex, lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            spannable.setSpan(
                AbsoluteSizeSpan(20, true), secondIndex, secondlastindex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            val typeface = ResourcesCompat.getFont(context!!, R.font.pt_sans_bold)
            spannable.setSpan(
                StyleSpan(typeface!!.style),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            spannable.setSpan(
                StyleSpan(typeface.style),
                secondIndex,
                secondlastindex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannable
        }
        //--> red color with 20 font size bold
        fun setRedSpannableWithTextSizeTwenty(
            context: Context,
            text: String,
            startIndex: Int,
            lastIndex: Int
        ): Spannable {
            val spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannable.setSpan(
                AbsoluteSizeSpan(20, true), startIndex, lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            val typeface = ResourcesCompat.getFont(context, R.font.pt_sans_bold)
            spannable.setSpan(
                StyleSpan(typeface!!.style),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            return spannable
        }

        //--> red color with 16 font size bold
        fun setRedSpannableWithTextSizeSixteen(
            context: Context,
            text: String,
            startIndex: Int,
            lastIndex: Int
        ): Spannable {
            val spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannable.setSpan(
                AbsoluteSizeSpan(16, true), startIndex, lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            val typeface = ResourcesCompat.getFont(context, R.font.pt_sans_bold)
            spannable.setSpan(
                StyleSpan(typeface!!.style),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            return spannable
        }

        fun setSpannablewithsize(
            context: Context,
            text: String,
            startIndex: Int,
            lastIndex: Int
        ): Spannable {
            val spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE
            )
            spannable.setSpan(
                AbsoluteSizeSpan(15, true), startIndex, lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            return spannable
        }


        fun setSpannableGreen(
            context: Context,
            text: String,
            startIndex: Int,
            lastIndex: Int
        ): Spannable {
            val spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context, R.color.green_shade_1_100per)),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE
            )

            return spannable
        }

        fun setSpannableWhite(text: String, startIndex: Int, lastIndex: Int): Spannable {
            val spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(Color.parseColor("#FFFFFF")), startIndex, lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            return spannable
        }

        fun setSpannableWhiteBold(
            context: Context,
            text: String,
            startIndex: Int,
            lastIndex: Int
        ): Spannable {
            val spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(Color.parseColor("#FFFFFF")), startIndex, lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            val typeface = ResourcesCompat.getFont(context, R.font.pt_sans_bold)
            spannable.setSpan(
                StyleSpan(typeface!!.style),
                startIndex,
                lastIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            return spannable
        }

        fun redWhiteSpannable(
            context: Context,
            redInfo: String,
            whiteInfo: String
        ): SpannableStringBuilder {
            val spannableString =
                SpannableStringBuilder("$redInfo $whiteInfo")
            spannableString.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.colorAccent
                    )
                ), 0, redInfo.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannableString
        }

        fun redWhiteSpannablewithSizeFifteen(
            context: Context,
            redInfo: String,
            whiteInfo: String
        ): SpannableStringBuilder {
            val spannableString =
                SpannableStringBuilder("$redInfo $whiteInfo")
            spannableString.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.colorAccent
                    )
                ), 0, redInfo.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannableString.setSpan(
                AbsoluteSizeSpan(15, true), 0, redInfo.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannableString
        }

        fun redGraySpannable(
            context: Context,
            redInfo: String,
            whiteInfo: String
        ): SpannableStringBuilder {
            val spannableString =
                SpannableStringBuilder("$redInfo $whiteInfo")
            spannableString.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.colorAccent
                    )
                ), 0, redInfo.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannableString.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.gray
                    )
                ), redInfo.length, spannableString.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannableString
        }

        fun redWhiteSpannable(
            context: Context,
            redInfo: String,
            whiteInfo: String,
            redFont: Typeface?,
            whiteFont: Typeface?
        ): SpannableStringBuilder {
            val spannableString =
                SpannableStringBuilder("$redInfo $whiteInfo")
            spannableString.setSpan(redFont, 0, redInfo.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            spannableString.setSpan(
                whiteFont,
                redInfo.length,
                spannableString.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannableString.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.colorAccent
                    )
                ), 0, redInfo.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannableString
        }

        fun whiteRedSpannable(
            context: Context,
            redInfo: String,
            whiteInfo: String,
            redFont: Typeface?,
            whiteFont: Typeface?
        ): SpannableStringBuilder {
            val spannableString =
                SpannableStringBuilder("$whiteInfo $redInfo")

            spannableString.setSpan(
                whiteFont,
                0,
                whiteInfo.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannableString.setSpan(
                redFont,
                whiteInfo.length,
                spannableString.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            spannableString.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.colorAccent
                    )
                ), whiteInfo.length, spannableString.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannableString
        }

        fun whiteRedSpannable(
            context: Context,
            redInfo: String,
            whiteInfo: String
        ): SpannableStringBuilder {
            val spannableString =
                SpannableStringBuilder("$whiteInfo $redInfo")
            spannableString.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.colorAccent
                    )
                ), whiteInfo.length, spannableString.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannableString
        }

        fun underlineSpannable(
            info: String,
            mColor: Int,
            isUnderlineText: Boolean = true
        ): SpannableStringBuilder {
            val spannableString =
                SpannableStringBuilder("$info")

            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                override fun onClick(textView: View) {

                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = isUnderlineText
                    ds.color = mColor
                }
            }

            spannableString.setSpan(
                clickableSpan,
                0,
                spannableString.length,
                SpannableString.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannableString
        }


        fun isValidEmail(email: String): Boolean {
            val emailPattern =
                "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
            val pattern = Pattern.compile(emailPattern)
            val matcher = pattern.matcher(email)
            return matcher.matches()
        }

        fun setSizeSpannable(text: String): Spannable {
            val spannable = SpannableString(text)
            val splitStr = text.split("")
            if (splitStr.size == 1) {
                spannable.setSpan(
                    AbsoluteSizeSpan(13, true), 2, splitStr[0].length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )
            } else {
                spannable.setSpan(
                    AbsoluteSizeSpan(13, true), splitStr[0].length + 3, text.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )
            }
            return spannable
        }

        fun getRemainingTimeHeader(
            context: Context,
            count: String,
            type: String
        ): SpannableStringBuilder {
            val temp = count.toString()
            val spannable = SpannableStringBuilder("$temp$type")
            val tfBold = ResourcesCompat.getFont(context, R.font.pt_sans_bold)
            val tfLight = ResourcesCompat.getFont(context, R.font.pt_sans)
            spannable.setSpan(
                StyleSpan(tfBold!!.style),
                0,
                temp.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannable.setSpan(
                AbsoluteSizeSpan(16, true),
                0,
                temp.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannable.setSpan(
                StyleSpan(tfLight!!.style),
                temp.length,
                spannable.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannable.setSpan(
                AbsoluteSizeSpan(13, true),
                temp.length,
                spannable.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannable
        }


        fun getWalletDialogSpannable(
            context: Context,
            count: String,
            type: String
        ): SpannableStringBuilder {
            val temp = count.toString()
            val spannable = SpannableStringBuilder("$temp$type")


            spannable.setSpan(
                AbsoluteSizeSpan(16, true),
                0,
                temp.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannable.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                ), 0,
                temp.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            spannable.setSpan(
                AbsoluteSizeSpan(21, true),
                temp.length,
                spannable.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )

            spannable.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.colorAccent
                    )
                ), temp.length,
                spannable.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            return spannable
        }

        fun sharePlainText(context: Context, info: String) {
            //We have to get the get the data text from server but use static for now
            val shareText = context.getString(R.string.text_share)
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name))
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "$shareText $info")
            context.startActivity(
                Intent.createChooser(
                    sharingIntent,
                    context.getString(R.string.app_name)
                )
            )
        }

        fun isDebug(): Boolean {
            //return BuildConfig.DEBUG
            return DEBUG
            //return false
        }

        fun toggelDebug(context: Context) {
            DEBUG = !DEBUG
//            Toast.makeText(context, "$DEBUG", Toast.LENGTH_LONG).show()
            ApiClient.rA = true
            ApiClient.rM = true
            ApiClient.rI = true
            ApiClient.rA1 = true
            ApiClient.rM1 = true
            ApiClient.rI1 = true
            ApiClient.rM2 = true
        }


        fun loadImage(context: Context, url: String?, imageView: ImageView) {
            loadImage(context, url, imageView, R.drawable.nebula_placeholder)
        }


        fun loadImageWithOutCache(context: Context, url: String?, imageView: ImageView) {
            loadImageWithOutCache(context, url, imageView, R.drawable.nebula_placeholder)
        }


        fun loadImage(context: Context, url: String?, imageView: ImageView, drawable: Int) {
            if (url.isNullOrEmpty()) {
                imageView.setImageDrawable(ContextCompat.getDrawable(context, drawable))
            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(context, drawable))
                val tempUrl = url.trim()
                Log.e("*******************", tempUrl)
                val options: RequestOptions = RequestOptions()
                    .placeholder(drawable)
                    .error(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)


                Glide.with(context).load(tempUrl).apply(options).into(imageView)
            }
        }

        fun loadImageNoThumbnail(
            context: Context,
            url: String?,
            imageView: ImageView,
            drawable: Int
        ) {
            if (url.isNullOrEmpty()) {
                imageView.setImageDrawable(ContextCompat.getDrawable(context, drawable))
            } else {
                val tempUrl = url.trim()
                Log.e("*******************", tempUrl)
                val options: RequestOptions = RequestOptions()
                    .placeholder(drawable)
                    .error(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                Glide.with(context).load(tempUrl).apply(options)
                    .into(imageView)
            }
        }


        @JvmStatic
        fun loadAsBitmap(
            context: Context,
            img: ImageView?,
            img1: ImageView?,
            img2: ImageView?,
            url: String?,
            drawable: Int
        ) {
            if (url.isNullOrEmpty()) {
                img?.setImageResource(drawable)
                img1?.setImageResource(drawable)
                img2?.setImageResource(drawable)
            } else {
                val options: RequestOptions = RequestOptions()
                    .placeholder(drawable)
                    .error(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
//
//                if (img != null) {
////                    Glide.with(context).load(url).thumbnail(THUMBNAIL).apply(options).into(img)
//                    Glide.with(context).load(url).apply(options).into(img)
//                }
//
//                if (img1 != null) {
////                    Glide.with(context).load(url).thumbnail(THUMBNAIL).apply(options).into(img1)
//                    Glide.with(context).load(url).apply(options).into(img1)
//                }
//
//                if (img2 != null) {
////                    Glide.with(context).load(url).thumbnail(THUMBNAIL).apply(options).into(img2)
//                    Glide.with(context).load(url).apply(options).into(img2)
//                }

                Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .apply(options)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            img?.setImageBitmap(resource)
                            if (img1 != null) {
                                img1.setImageBitmap(resource)
                                img2?.setImageBitmap(resource)
                            }

                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            img?.setImageResource(drawable)
                            img1?.setImageResource(drawable)
                            img2?.setImageResource(drawable)
                            super.onLoadFailed(errorDrawable)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {

                        }

                    })
            }
        }

        @JvmStatic
        fun loadAsBitmap(
            context: Context,
            img: ImageView?,
            img1: ImageView?,
            img2: ImageView?,
            url: String?,
            drawable: Int, view: View
        ) {
            if (url.isNullOrEmpty()) {
                img?.setImageResource(drawable)
                img1?.setImageResource(drawable)
                img2?.setImageResource(drawable)
            } else {
                val options: RequestOptions = RequestOptions()
                    .placeholder(drawable)
                    .error(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)


                Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .apply(options)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            img?.setImageBitmap(resource)
                            if (img1 != null) {
                                img1.setImageBitmap(resource)
                                img2?.setImageBitmap(resource)
                                view.visibility = View.VISIBLE
                            }

                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            img?.setImageResource(drawable)
                            img1?.setImageResource(drawable)
                            img2?.setImageResource(drawable)
                            if (img1 != null) {
                                view.visibility = View.VISIBLE
                            }
                            super.onLoadFailed(errorDrawable)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {

                        }

                    })
            }
        }

        @JvmStatic
        fun loadAsBitmapRecent(
            context: Context,
            img: ImageView?,
            img1: ImageView?,
            img2: ImageView?,
            url: String?,
            drawable: Int
        ) {
            if (url.isNullOrEmpty()) {
                img?.setImageResource(drawable)
                img1?.setImageResource(drawable)
                img2?.setImageResource(drawable)
            } else {
                val options: RequestOptions = RequestOptions()
                    .placeholder(drawable)
                    .error(drawable)
                    .override(200, 200)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.IMMEDIATE)
                Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .apply(options)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            img?.setImageBitmap(resource)
                            if (img1 != null) {
                                img1.setImageBitmap(
                                    Bitmap.createScaledBitmap(
                                        resource,
                                        20,
                                        20,
                                        false
                                    )
                                )
                                img2?.setImageBitmap(
                                    Bitmap.createScaledBitmap(
                                        resource,
                                        40,
                                        40,
                                        false
                                    )
                                )
                            }

                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            img?.setImageResource(drawable)
                            img1?.setImageResource(drawable)
                            img2?.setImageResource(drawable)
                            super.onLoadFailed(errorDrawable)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {

                        }

                    })
            }
        }

        fun loadImageWithOutCache(
            context: Context,
            url: String?,
            imageView: ImageView,
            drawable: Int
        ) {
            if (url.isNullOrEmpty()) {
                imageView.setImageDrawable(ContextCompat.getDrawable(context, drawable))
            } else {
                val tempUrl = url.trim()
                Log.e("*******************", tempUrl)
                val options: RequestOptions = RequestOptions()
                    .placeholder(drawable)
                    .error(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .priority(Priority.HIGH)
                    .skipMemoryCache(true)


                Glide.with(context).load(tempUrl).thumbnail(THUMBNAIL).apply(options)

                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
//							Log.e("Error********", e?.message)
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }
                    })
                    .into(imageView)
            }
        }


        //--> Format to two decimal places ( Round off removed)
        fun trimDecimalToTwoPlaces(value: Double?): String {

            if (value.toString().isEmpty() || value.toString() == null)
                return ""
//            val valueString = String.format("%.2f", value)
//            return valueString

            if (value!! < 0) {
                return "0.00"
            }

            val symbol= DecimalFormatSymbols(Locale.US)
            val df = DecimalFormat("#,###,###.00",symbol)
//                df.roundingMode = RoundingMode.DOWN

            val price = df.format(value)
            return if (value <= 1) trimDecimalToTwo(price.toDouble()) else price
        }

        fun trimDecimalToFourPlaces(value: Double): String {
//            val valueString = String.format("%.2f", value)
//            return valueString

            if (value < 0) {
                return "0.0000"
            }
            val symbol= DecimalFormatSymbols(Locale.US)

            val df = DecimalFormat("#,###,###.0000",symbol)
//                df.roundingMode = RoundingMode.DOWN

            val price = df.format(value)
            return if (value <= 1) trimDecimalForFourDecimal(price.toDouble()) else price
        }


        //--> Format to two decimal places ( Round off removed)
        fun trimDecimalToThreePlacesNew(value: Double): String {
//            return valueString
            val symbol= DecimalFormatSymbols(Locale.US)

            val df = DecimalFormat("#,###,###.000",symbol)
            df.roundingMode = RoundingMode.DOWN

            val price = df.format(value)
            return if (value <= 1) trimThreePlaces(price.toDouble()) else price
        }

        private fun trimDecimalToTwo(value: Double): String {
            val valueString = String.format("%.2f", value)
            return valueString
        }


        private fun trimThreePlaces(value: Double): String {
            val valueString = String.format("%.3f", value)
            return valueString
        }

        private fun trimDecimalForFourDecimal(value: Double): String {
            val valueString = String.format("%.4f", value)
            return valueString
        }

        //--> Format to two decimal places ( Round off removed)
        fun trimDecimalToTwoPlacesWithoutComma(value: Double): String {
//            val valueString = String.format("%.2f", value)
//            return valueString
            val symbol= DecimalFormatSymbols(Locale.US)

            val df = DecimalFormat("#######.00",symbol)
            return if (value == 0.0) return "0.00" else df.format(value)
        }


        fun trimDecimalToThreePlaces(value: Double): String {
            val symbol= DecimalFormatSymbols(Locale.US)

            val df = DecimalFormat("#,###,###.###",symbol)
            return df.format(value)
        }

        //convert to minute from sec
        fun convertToMinute(sec: Int): String {
            val tempMin = sec / 60
            return if (tempMin > 0) {
                val secRemain = sec - (tempMin * 60)
                "$tempMin:${
                    if (secRemain < 10) {
                        "0$secRemain"
                    } else secRemain
                }"
            } else
                "$tempMin:$sec"
        }

        fun convertPlayerDuration(sec: String, duration: Long): String {
            var currentTiming = 0
            val durationTiming = TimeUnit.MILLISECONDS.toSeconds(duration)
            val currentTimeArray = sec.split(":")
            if (currentTimeArray.size == 1) {
                currentTiming = currentTimeArray[0].toInt()
            } else {
                currentTiming = currentTimeArray[0].toInt() * 60
                currentTiming += currentTimeArray[1].toInt()
            }
            val remainSec = (durationTiming - currentTiming).toInt()

            return when (remainSec) {
                0 -> {
                    "00:00"
                }
                -1 -> {
                    "00:00"
                }
                else -> {
                    val tempMin = remainSec / 60
                    val minStr = if (tempMin < 10) {
                        "0$tempMin"
                    } else {
                        "$tempMin"
                    }
                    return if (tempMin > 100) {
                        "00:00"
                    } else if (tempMin > 0) {
                        val secRemain = remainSec - (tempMin * 60)
                        var secRemainStr = if (secRemain < 10) {
                            "0:$secRemain"
                        } else {
                            "$secRemain"
                        }
                        "$minStr:$secRemainStr"
                    } else
                        "$minStr:${
                            if (remainSec < 10) {
                                "0$remainSec"
                            } else {
                                "$remainSec"
                            }
                        }"

                }
//                    convertToMinute(remainSec)
            }
        }


        fun dateFormatterTransaction(endtime: String): String {
            return try {
                var tempStr = endtime.replace("T", " ", true)
                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault())
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val date = sdf.parse(tempStr)
                val df = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                df.format(date)
            } catch (unParsable: java.lang.Exception) {
                endtime
            }
        }


        fun dateFormatterNotification(endtime: String): String {
            return try {
                var tempStr = endtime.replace("T", " ", true)
                val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val date = sdf.parse(tempStr)
//				val currentData = Calendar.getInstance(Locale.getDefault())
//				val dateStr =
//					"${currentData.get(Calendar.DATE)}-${currentData.get(Calendar.MONTH)+1}-${currentData.get(
//						Calendar.YEAR
//					)}"
//				val currentFormattedDate = sdf.parse(dateStr)
//				if (date == currentFormattedDate) {
//					"New"
//				} else {

                val df = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
                df.format(date)
//				}
            } catch (unParsable: java.lang.Exception) {
                endtime
            }
        }


        fun isDateToday(endtime: String): Boolean {
            return try {
                var tempStr = endtime.replace("T", " ", true)
                val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val date = sdf.parse(tempStr)
                val currentData = Calendar.getInstance(Locale.getDefault())
                val dateStr =
                    "${currentData.get(Calendar.DATE)}-${currentData.get(Calendar.MONTH) + 1}-${
                        currentData.get(
                            Calendar.YEAR
                        )
                    }"
                val currentFormattedDate = sdf.parse(dateStr)
                date == currentFormattedDate
            } catch (unParsable: java.lang.Exception) {
                false
            }
        }

        fun getFormattedNumber(number: Long): String {
            return if (number < 10) {
                "0$number"
            } else {
                "$number"
            }
        }

        fun roundRectangleBoarder(
            context: Context,
            imageView: AppCompatImageView,
            url: String,
            drawableId: Int,
            radius: Int,
            marign: Int,
            colorHex: String,
            boarderInt: Int = 0
        ) {
            if (url.isNullOrEmpty()) {
                imageView.setImageDrawable(ContextCompat.getDrawable(context, drawableId))
            } else {
                val tempUrl = url.trim()
                val options: RequestOptions = RequestOptions()
                    .placeholder(drawableId)
                    .error(drawableId)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .priority(Priority.HIGH)

                Glide.with(context)
                    .load(tempUrl)
                    .apply(options)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            e?.message?.let { Log.e("Error********", it) }
                            imageView.setImageDrawable(
                                ContextCompat.getDrawable(
                                    context,
                                    drawableId
                                )
                            )
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }
                    })
                    .apply(
                        RequestOptions.bitmapTransform(
                            RoundedCornersTransformation(
                                context,
                                radius,
                                1,
                                "#00000000",
                                0
                            )
                        )
                    )
                    .into(imageView)
            }
        }


        //--> Show log
        fun showLog(tag: String?, message: String) {
            try {
                Log.e(tag, "" + message)
            } catch (e: java.lang.Exception) {
            }
        }

        fun openSettingDialog(context: Context) {

            val builder = AlertDialog.Builder(context)
            //Uncomment the below code to Set the message and title from the strings.xml file
            //Uncomment the below code to Set the message and title from the strings.xml file
            builder.setMessage("Permission is required for the further process, Please provide the permisson")
                .setTitle("Permission Dialog")
                .setCancelable(false)
                .setPositiveButton("Yes",
                    DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri: Uri = Uri.fromParts("package", context.getPackageName(), null)
                        intent.data = uri
                        (context as Activity).startActivityForResult(
                            intent,
                            PermissionHelper.SETTING_CODE
                        )

                    })
                .setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, id ->
                        //  Action for 'NO' Button
                        dialog.cancel()

                    })
            //Creating dialog box
            //Creating dialog box
            val alert: AlertDialog = builder.create()
            //Setting the title manually
            //Setting the title manually
            alert.show()
            alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#000000"))
            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#000000"))

        }


        fun getRealPathFromUri(context: Context, uri: Uri): String? {
            // DocumentProvider
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(
                    context,
                    uri
                )
            ) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]

                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    }
                } else if (isDownloadsDocument(uri)) {

                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )

                    return getDataColumn(context, contentUri, null, null)
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]

                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } /*else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }*/

                    val selection = "_id=?"
                    val selectionArgs = arrayOf(split[1])

                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }// MediaProvider
                // DownloadsProvider
            } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

                // Return the remote address
                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                    context,
                    uri,
                    null,
                    null
                )

            } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
                return uri.path
            }// File

            return null
        }

        private fun getDataColumn(
            context: Context, uri: Uri?, selection: String?,
            selectionArgs: Array<String>?
        ): String? {

            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(column)

            try {
                cursor =
                    context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
                if (cursor != null && cursor.moveToFirst()) {
                    val index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }

        private fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        private fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri.authority
        }

        private fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri.authority
        }

        private fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content" == uri.authority
        }

        fun stringToDouble(info: String?): Double {
            if (info.isNullOrEmpty()) {
                return 0.0
            }
            return info.toDouble()
        }

        private fun getIP(context: Context): String {

            val conManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = conManager.allNetworks
            var ipAddress = ""
            for (netInfo in networkInfo) {
                val networkCapability = conManager.getNetworkCapabilities(netInfo)
                if (networkCapability?.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) == true) {
                    ipAddress = getWifiIPAddress(context)
                } else if (networkCapability?.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) == true) {
                    ipAddress = getWifiIPAddress(context)
                }
            }
            return ipAddress
        }

        private fun getWifiIPAddress(context: Context): String {
            val wifiMgr = context.getSystemService(WIFI_SERVICE) as WifiManager
            val wifiInfo = wifiMgr.connectionInfo
            var ip: Int = wifiInfo?.ipAddress ?: 0
            if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN) {
                ip = Integer.reverseBytes(ip)
            }
            val ipByteArray = BigInteger.valueOf(ip.toLong()).toByteArray()
            return InetAddress.getByAddress(ipByteArray).hostAddress
        }

        private fun getDeviceWifi(context: Context): String {

            return ""
        }


        fun calculateDialogDimensions(
            activity: Activity?,
            dialog: Dialog?,
            width: Float,
            height: Float
        ) {
            if (activity == null || dialog?.window == null) {
                return
            }

            val displayMetrics = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            val displayWidth = displayMetrics.widthPixels
            val displayHeight = displayMetrics.heightPixels
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window!!.attributes)
            // Set the alert dialog window width and height
// Set alert dialog width equal to screen width 90%
// int dialogWindowWidth = (int) (displayWidth * 0.9f);
// Set alert dialog height equal to screen height 90%
// int dialogWindowHeight = (int) (displayHeight * 0.9f);
// Set the width and height for the layout parameters
// This will bet the width and height of alert dialog
            if (width != ConstraintLayout.LayoutParams.WRAP_CONTENT.toFloat() && width != ConstraintLayout.LayoutParams.MATCH_PARENT.toFloat()) { // Set alert dialog width equal to screen width as ratio using width like 0.7f to 70%
                layoutParams.width = (displayWidth * width).toInt()
            } else {
                layoutParams.width = width.toInt()
            }
            if (height != ConstraintLayout.LayoutParams.WRAP_CONTENT.toFloat() && height != ConstraintLayout.LayoutParams.MATCH_PARENT.toFloat()) { // Set alert dialog height equal to screen height as ratio using height like 0.7f to 70%
                layoutParams.height = (displayHeight * height).toInt()
            } else {
                layoutParams.height = height.toInt()
            }
            // Apply the newly created layout parameters to the alert dialog window
            dialog.window!!.attributes = layoutParams
            dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        }

        fun removeDecimalPlaces(temp: Double): String {
            return String.format("%.0f", temp)
        }

        fun removeOneDecimalPlaces(temp: Double): String {
            return String.format("%.1f", temp)
        }


        fun setTwoDecimalPlaces(temp: Double): String {
            val symbol= DecimalFormatSymbols(Locale.US)

            val df = DecimalFormat("#,###,###.00",symbol)
            return df.format(temp)
        }

        fun setNotificationDot(context: Context, textView: TextView) {
            if (PrefUtils.getIntValue(context, PrefUtils.notificationCount) > 0 ||
                PrefUtils.getIntValue(context, PrefUtils.referralCount) > 0
            ) {
                textView.visibility = View.VISIBLE
            } else {
                textView.visibility = View.GONE
            }
        }

        fun dpToPx(context: Context, dp: Int): Int {
            val displayMetrics = context.resources.displayMetrics
            return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        }

        fun dpToPx2(context: Context, dp: Float): Float {
            return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                context.resources.displayMetrics
            )
        }


        fun setFormattedNumber(number: Int): String {
            return if (number in 0..9) {
                "0$number"
            } else {
                "$number"
            }
        }


        fun setReferralSpan(
            context: Context,
            isRead: Boolean,
            text: String,
            textValue: String
        ): Spannable {
            val spannable = SpannableString(text + " " + textValue)
            val startIndex = 0
            val lastIndex = text.length


            val secondIndex = lastIndex + 1
            val secondlastindex = secondIndex + textValue.length
            if (!isRead) {

                spannable.run {
                    setSpan(
                        ForegroundColorSpan(ContextCompat.getColor(context, R.color.white)),
                        startIndex,
                        lastIndex,
                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
                    )

                    setSpan(
                        ForegroundColorSpan(ContextCompat.getColor(context, R.color.gray)),
                        secondIndex,
                        secondlastindex,
                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
                    )

                    setSpan(
                        AbsoluteSizeSpan(15, true), startIndex, lastIndex,
                        Spannable.SPAN_INCLUSIVE_INCLUSIVE
                    )

                    setSpan(
                        AbsoluteSizeSpan(13, true), secondIndex, secondlastindex,
                        Spannable.SPAN_INCLUSIVE_INCLUSIVE
                    )
//                    setSpan(
//                        RelativeSizeSpan(0.5f),
//                        secondIndex,
//                        secondlastindex,
//                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
//                    )
                    setSpan(
                        TopAlignSuperscriptSpan(0.1),
                        secondIndex,
                        secondlastindex,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }

                val typeface = ResourcesCompat.getFont(context!!, R.font.pt_sans_bold)
                spannable.setSpan(
                    StyleSpan(typeface!!.style),
                    startIndex,
                    lastIndex,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )

                val typeface1 = ResourcesCompat.getFont(context!!, R.font.pt_sans)
                spannable.setSpan(
                    StyleSpan(typeface1!!.style),
                    secondIndex,
                    secondlastindex,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )


            } else {


                spannable.setSpan(
                    ForegroundColorSpan(ContextCompat.getColor(context, R.color.gray)),
                    startIndex,
                    secondlastindex,
                    Spannable.SPAN_INCLUSIVE_EXCLUSIVE
                )

                spannable.setSpan(
                    AbsoluteSizeSpan(15, true), startIndex, lastIndex,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )

                spannable.setSpan(
                    AbsoluteSizeSpan(13, true), secondIndex, secondlastindex,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )


                val typeface1 = ResourcesCompat.getFont(context!!, R.font.pt_sans)
                spannable.setSpan(
                    StyleSpan(typeface1!!.style),
                    startIndex,
                    secondlastindex,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )
            }
            return spannable
        }

        /*Bottom Sheet Full*/
        fun setupFullHeight(activity: AppCompatActivity, bottomSheetDialog: BottomSheetDialog) {
            val bottomSheet =
                bottomSheetDialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
            val behavior: BottomSheetBehavior<*> =
                BottomSheetBehavior.from<FrameLayout?>(bottomSheet!!)
            val layoutParams = bottomSheet.layoutParams
            val windowHeight = getWindowHeight(activity)
            if (layoutParams != null) {
                layoutParams.height = windowHeight
            }
            bottomSheet.layoutParams = layoutParams
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        fun getWindowHeight(activity: AppCompatActivity): Int {
            // Calculate window height for fullscreen use
            val displayMetrics = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.heightPixels
        }

        fun isValidContextForGlide(context: Context?): Boolean {
            if (context == null) {
                return false
            }
            if (context is AppCompatActivity) {
                val activity = context
                if (activity.isDestroyed || activity.isFinishing) {
                    return false
                }
            }
            return true
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun convertDateTimeToMilliSeconds(dateString: String) {

            val date = dateString
            val formatter: DateTimeFormatter =
                DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
            val localDate: LocalDateTime = LocalDateTime.parse(date, formatter)
            val timeInMilliseconds: Long =
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    localDate.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli()
                } else {
                    TODO("VERSION.SDK_INT < O")
                }
        }

        fun chatDateFormatter(context: Context, dateStr: String): String {
            return try {
                var tempStr = dateStr.replace("T", " ", true)
                val readerFormatter =
                    SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault())
                val todayFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                // CREATE DateFormatSymbols WITH ALL SYMBOLS FROM (DEFAULT) Locale

                todayFormatter.timeZone = TimeZone.getTimeZone("UTC")

                val todayStr = getTodayDate()
                if ((todayFormatter.parse(tempStr.split(" ")[0]) == todayFormatter.parse(todayStr))) {
                    context.getString(R.string.today)
                } else {
                    val symbols = DateFormatSymbols(Locale.getDefault())
                    symbols.amPmStrings = arrayOf("AM", "PM")
                    readerFormatter.timeZone = TimeZone.getTimeZone("UTC")
                    val date = readerFormatter.parse(tempStr)
                    val df = SimpleDateFormat("EEE, MMM d", Locale.getDefault())
                    df.dateFormatSymbols = symbols
                    df.format(date)
                }
            } catch (unParsable: java.lang.Exception) {
                dateStr
            }
        }

        fun getTodayDate(): String {
            val cal = Calendar.getInstance(Locale.getDefault())
                .apply { timeZone = TimeZone.getTimeZone("UTC") }
            return "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DAY_OF_MONTH)}"
        }

        fun chatTimeFormatter(dateStr: String): String {
            return try {
                val tempStr = dateStr.replace("T", " ", true)
                val readerFormatter =
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\"", Locale.getDefault())
                val symbols = DateFormatSymbols(Locale.getDefault())
                // OVERRIDE SOME symbols WHILE RETAINING OTHERS
                symbols.amPmStrings = arrayOf("AM", "PM")
                readerFormatter.timeZone = TimeZone.getTimeZone("UTC")
                val date = readerFormatter.parse(tempStr)
                val df = SimpleDateFormat("hh:mm a", Locale.getDefault())
                df.dateFormatSymbols = symbols
                df.format(date)

            } catch (unParsable: java.lang.Exception) {
                try {
                    val tempStr = dateStr.replace("T", " ", true)
                    val readerFormatter =
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                    val symbols = DateFormatSymbols(Locale.getDefault())
                    // OVERRIDE SOME symbols WHILE RETAINING OTHERS
                    symbols.amPmStrings = arrayOf("AM", "PM")
                    readerFormatter.timeZone = TimeZone.getTimeZone("UTC")
                    val date = readerFormatter.parse(tempStr)
                    val df = SimpleDateFormat("hh:mm a", Locale.getDefault())
                    df.dateFormatSymbols = symbols
                    df.format(date)
                } catch (e: Exception) {
                    dateStr
                }

            }
        }


        fun chatTimeFormatter(): String {
            return try {
                val symbols = DateFormatSymbols(Locale.getDefault())
                symbols.amPmStrings = arrayOf("AM", "PM")
                val df = SimpleDateFormat("hh:mm a", Locale.getDefault())
                df.dateFormatSymbols = symbols
                df.format(Date())

            } catch (unParsable: java.lang.Exception) {
                ""
            }
        }

        // Event Triggering

        fun triggerSignUpInitiatedEvent() {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("signup", "Sign Up Initiated")
            mFirebaseAnalytics.logEvent("sign_up_initiated", bundle)
        }


        fun triggerSendOtpEvent(res: RegisterResponse, registerRequest: RegisterRequest) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("user_id", res?.userId)
            bundle.putString("phone_number", registerRequest?.PhoneNumber)
            bundle.putString("country_code", registerRequest?.countryCode)
            mFirebaseAnalytics.logEvent("send_otp", bundle)
        }


        fun triggerVerifyOtp(userid: String) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("user_id", userid)
            mFirebaseAnalytics.logEvent("verify_otp", bundle)
        }


        fun triggerSignUpEvent(response: SignupResponse) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("email", response.response.userInfo?.email)
            bundle.putString("user_name", response.response.userInfo?.givenName)
            bundle.putString("phone_number", response.response.userInfo?.verifiedPhone)
            mFirebaseAnalytics.logEvent("sign_up", bundle)
        }


        fun triggerSignUpSkip() {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            mFirebaseAnalytics.logEvent("sign_up_skip", null)
        }

        fun triggerloginEvent(loginResponse: LoginResponse) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("user_name", loginResponse.userInfo.givenName)
            bundle.putString("phone_number", loginResponse.userInfo.givenName)
            mFirebaseAnalytics.logEvent("login", bundle)
        }


        fun triggerLogOutEvent(phoneNumber: String) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("phone_number", phoneNumber)
            mFirebaseAnalytics.logEvent("logout", bundle)
        }


        fun trigerOpenDropEvent(songTitle: String) {
            val firebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("drop_name", songTitle)
            firebaseAnalytics.logEvent("open_drop", bundle)
        }


        fun triggerNotifyEvent(isLogin: Boolean) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("login_state", isLogin.toString())
            mFirebaseAnalytics.logEvent("notify_me_button", bundle)
        }


        fun triggerUpdateProfile(
            userName: String?,
            userEmail: String?,
            fullName: String?,
            bio: String?
        ) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("user_name", userName)
            bundle.putString("user_email", userEmail)
            bundle.putString("full_name", fullName)
            bundle.putString("bio", bio)
            mFirebaseAnalytics.logEvent("edit_profile", bundle)
        }




        fun triggerSearchEvent(
           searchText:String?
        ) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("search_query", searchText)

            mFirebaseAnalytics.logEvent("search", bundle)
        }


        fun triggerMusicFragment(myPlayListCount: String) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("my_play_list_count", myPlayListCount)
            mFirebaseAnalytics.logEvent("my_playlist_view", bundle)
        }


        fun triggerWalletViewEvent(walletResponse: MyBalanceResponse, phoneNumber: String) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            mFirebaseAnalytics.setAnalyticsCollectionEnabled(true)
            val bundle = Bundle()
            bundle.putString(
                "wallet_current_amount",
                walletResponse.response?.currentBalanceAmount.toString()
            )
            bundle.putString("currency_type", walletResponse.response?.currencyType.toString())
            bundle.putString("phone_number", phoneNumber)
            mFirebaseAnalytics.logEvent("wallet_view", bundle)
        }

        fun triggerFollower(followerName: String, followingName: String, phoneNumber: String) {
            val mFirebaseAnalytics = BaseApplication.firebaseAnalytics
            mFirebaseAnalytics.setAnalyticsCollectionEnabled(true)
            val bundle = Bundle()
            bundle.putString("following_name", followerName)
            bundle.putString("follower_name", followingName)
            bundle.putString("phone_number", phoneNumber)
            mFirebaseAnalytics.logEvent("wallet_view", bundle)
        }

        @JvmStatic
        fun logOut(context: Context) {
            /* navigate = context as navigate
             PrefUtils.logout(context)
             NebulaDBHelper(context).clearDataBase()
             navigate.clearLogOut()
             // navigate.changeTab(ConstantsNavTabs.NAV_TAB_HOME)
             val intent = Intent(BaseApplication.appInstance(), LoginActivity::class.java)
             intent.putExtra(AppConstant.ISFIRSTTIME, false)
             context.startActivity(intent)*/
        }
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }


}

