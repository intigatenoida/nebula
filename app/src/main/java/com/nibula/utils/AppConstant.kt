package com.nibula.utils

class AppConstant {
    companion object {
        const val TIME_TO_WAIT: Long = 100
        const val TIME_TO_WAIT_NEXT: Long = 1000
        const val URI_LIST = "uriList"
        const val POSITION = "position"
        const val RECORD_IMAGE_URL = "recordImageUrl"
        const val RECORD_ARTIST_NAME = "recordArtistName"
        const val RECORD_TYPE_ID = "recordTypeId"
        const val RECORD_TITLE = "recordTitle"
        const val RECORD_STATUS = "RECORD_STATUS"
        const val RELEASED_DATE = "RELEASED_DATE"
        const val RECORD_ID = "recordId"
        const val DEVICE_TYPE_ID = "2"
        const val CHAT_ID = "chatId"
        const val partner_ID = "partnerId"
        const val RECORD_DATA = "recordData"
        const val CONTENT_TYPE = "application/json"
        const val ID = "ID"
        const val COLOR_HEXA = "recordImageColor"
        const val RECORD_ID_FIELDS="recordIdFields"
        const val PRICE_PER_ROYALTY_WORK = "pricePerRoyaltyWork"
        const val PRICE_PER_ROYALTY_RECORDING = "pricePerRoyaltyRecording"
        const val COPY_RIGHT_SOCIETY_WORK_ID = "copyRightSocietyWorkId"
        const val COPY_RIGHT_SOCIETY_RECORDING_ID = "copyRightSocietyRecordingId"
        const val ISWC_CODE ="iswcCode"
        const val ISCR_CODe = "iscrCode"
        const val IMAGE_RESIZE_HOME="?width=200&height=200&quality=10"
        const val COPY_RIGHT_SOCIETY_WORK = "copyRightSocietyWork"
        const val COPY_RIGHT_SOCIETY_RECORDING = "copyRightSocietyRecording"
        const val CURRENCY_TYPE_ID ="currencyTypeId"
        const val STATUS = "STATUS"
        const val EMAIL = "EMAIL"
        const val PHONENUMBER = "PHONENUMBER"
        const val COUNTRYCODE = "COUNTRYCODE"
        const val INVESTMENT_STATUS_TYPE = "investmentStatusType"

        const val IS_FRESH_RECORD = "isFreshRecord"
        const val ISFIRSTTIME = "IS_FIRST_TIME"
        const val IS_FROM_LOGIN = "IS_FROM_LOGIN"
        const val IS_START_FROM_GUIDE = "IS_START_FROM_GUIDE"

        const val RECORD_DETAIL = "RECORD_DETAIL"
        const val COST_BREAKDOWN ="COST_BREAKDOWN"
        const val SHARES_PURCHASED = "SHARES_PURCHASED"
        const val SITE_KEY = "6LfpE1smAAAAAHYwQb9K5agXAl0PlYlDv6ljIYeZ"



        const val CONNECTED_WALLET_ACCOUNT = "wallet_account"
        const val SELECTED_SESSION_TOPIC = "selectedSessionTopic"
        const val WALLET_URI = "wallet_uri"
        /*Request Code*/
        const val REQUEST_CODE_SIGN_UP = 10045
        const val REQUEST_SAVE_DRAFTS_REFRESH = 10046
        const val REQUEST_CODE_FINAL_UPLOAD = 10099
        const val PAYMENT_SUCCCESS_BROADCAST_ACTION = "payment_success"


       /*
        0 = Draft
        1 = Submitted
        8 = Approved By Admin
        2 = Initial Offering
        3 = Released
        9 = Stopped Due To No Investment
        */

//new Record Status
      /*0        Draft
        1        Submitted
        2        Approved
        3        Rejected
        4        Resubmitted
        5        Blocked
        6        Launched
        7        Sold Out
        8        Closed
        9        Removed
        10       Stop Displaying on Application*/
        //-->    RecordType
        const val draft = 0
        const val request_submitted = 1
        const val initialoffering = 20
        const val released = 6
        const val sold_out = 7
        const val no_investment = 9
        const val approved_by_admin = 2
        const val rejected = 3
        const val pending = "Pending"
        const val Draft = "Draft"
        const val Released = "Released"
        const val AVAILABLE = "Available"
        const val PERMISSION_WRITE_EXTERNAL_STORAGE: Int = 333
        const val REQUEST_UPLOAD_IMAGE: Int = 4444
        const val RECENT_TABLE_NAME = "RECENT_TABLE_NAME"
        const val RECENT_DELETE_TABLE_NAME = "RECENT_DELETE_TABLE_NAME"
        const val PLAYLIST_TABLE_NAME = "PLAYLIST"
        const val CURRENCY_USD = 1
        const val CURRENCY_EUR = 2
        const val CURRENCY_GBP = 3
        const val CURRENCY_CHF = 4
        // for file save and encryption
        const val TEMP_FILE_NAME = "MultipleImageView"
        const val POSTFIX = "Nebula"
        const val FILE_EXT = ".mp3"
        const val DIR_NAME = "Audio"
        const val OUTPUT_KEY_LENGTH = 256
        // Algorithm
        const val CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding"
        const val KEY_SPEC_ALGORITHM = "AES"
        const val PROVIDER = "BC"
        const val SECRET_KEY = "SECRET_KEY"
        const val PLAYER_INTENT_FILTER = "PLAYER_INTENT_FILTER"
        const val IMAGE_CLOSE = "IMAGE_CLOSE"
        const val PLAY_PAUSE = "PLAY_PAUSE"
        //:: Investor Record Status
        const val InvestNow = 0
        const val RequestToInvest = 1
        const val RequestPending = 2
        const val RequestApproved = 3
        const val RequestRejected = 4
        const val AlreadyInvested = 5
        const val PayNow = 6
        const val AlreadyPurchased = 7
        const val SelfOwned = 8
        const val PENDING = 1
        const val APPROVED = 2
        const val REJECTED = 3
        const val INVESTED = 4
        const val ALL_READY_PURCHASED = 5
        const val SELF_OWNED = 6
        const val FromTopButton = "FromTopButton"


        const val InvestmentRequest = 1
        const val SharesSold = 2
        const val RecordSubmitted = 38
        const val NewOffering = 3
        const val PurchasedRecord = 4
        const val ConvertedToReleased = 5

        const val NewOfferingUpdatedStatus = 38
        const val ConvertedToReleasedUpdateStatus = 41
        const val Cashboack = 6
        const val Percent_cut_on_sell = 7
        const val Follow_Following = 8
        const val Approve_Reject_Investment = 9
        const val Withdraw_funds = 10
        const val Earning_on_transaction = 11
        const val Copyright_Royalties = 12
        const val Fund_raised = 13

        /* Strip Constants */
        const val CLIENT_SECRETE = "clientSecrete"

        const val TEST_PUBLISH_KEY_STRIP: String =
            "pk_test_51HAKrwI4XokV98ozx7ZlqVUBe2PlIw9HZvE9CKRvL4W8uoJ8hpiLQ3aZyVHStkyNcQllhCyVLt5krtas2mEwON5Y00bX365RnX"

        const val LIVE_PUBLISH_KEY_STRIP: String =
            "pk_live_51HAKrwI4XokV98oz1IrcAgVkrUQAEdahvrAvSh0UjFtSrnbsyg31k5kKZyY4TxFOx3L1mlFbKlzTX0zZvhKc1org00RmtfF6LV"
        /*Pay Pal Account*/
        const val CURRENCY_CODE: String = "USD"
        /*
          Client Id for it@initgate.in

         */
        const val SANDBOX_PAYPAL_CLIENT_ID: String = "ATaAOXkgXhmE6iGhzey_oWOK4AdBtqVmsf9pDfxVTsqNTGAiyjqhLumaprQ05e-ll1a6Bz5lZ4jhMHAk"
        // SANDBOX QA - > AYVvWOJhAlmmDkmuIF4r6P-05haL1L-o-K8Wcj3luKtUw8vaXLyxfX4K7QPsBcZ09YQ2MCoow85-Jcvk
        //SANDBOX Live -> ATaAOXkgXhmE6iGhzey_oWOK4AdBtqVmsf9pDfxVTsqNTGAiyjqhLumaprQ05e-ll1a6Bz5lZ4jhMHAk

//            "AbKh0W1myFAXSGKAMCA_pUC7PTbGImO2xs6r0j1k2hxu6Fp5HWZl03VsZer4HuW6wF2gSXhugX_mC8AH"
//            "AUvLekglTeriqZAk2Zo398ordOX7MtvC2-ksoJLw800RoAnfZLe1RkIMqMfslzFphP451I0vWxv5081r"
//            "AfOgmUXsXyp8wz4HkTNh-iBm76vxaOOiYit7Scg1IuRs6q4OCUx9ilOrRoTqnsBzhAmn10EEe03zgwwa"
//            "AZKM2Wf3jS1S4ijnO3YCI9glIQsPzTX297RF3WssupOvNUCLLXRLzLTOVpW2LRkMqadZnfPK_HJYqA5W" // Example

        const val NEBULA_USER = 3
        const val NEBULA_ARTIST = 4
        const val BANK_ID = "BANK_ID"
       /*const val TERMS_CONDITION = "http://share.nebula.ch/CmsPages/Index?tid=2"
        const val PRIVACY_POLICY = "http://share.nebula.ch/CmsPages/Index?tid=3"
*/
        const val TERMS_CONDITION = "https://nebu.la/legal/terms"
        const val PRIVACY_POLICY = "https://nebu.la/legal/privacy-policy/"
        /* Change Privacy Policy link to https://www.nebu.la/legal/privacy-policy/
        Change Terms and Conditions link to https://www.nebu.la/legal/terms
*/
        const val BANK_DETAIL = "BANK_DETAIL"
        const val BANK_ACCOUNT_NUMBER = "BANK_ACCOUNT_NUMBER"
        const val BANK_ACCOUNT_NAME = "BANK_ACCOUNT_NAME"
        const val BANK_IFSC = "BANK_IFSC"
        const val FROM_BANK_LIST = "FROM_BANK_LIST"
        const val REFRESH_PAGE = "REFRESH_PAGE"
        const val AMOUNT = "AMOUNT"

        const val ACTION_TYPE_EMAIL = 1
        const val ACTION_TYPE_PHONE = 2
        const val ACTION_TYPE_EMAIL_AND_PHONE = 3
        const val ALREDY_VERIFIED = 1
        const val CODE_SEND = 2
        const val INVALID_OTP = 0

        const val FROM_NOTIFICATION = 1
        const val CHAT_TYPE_TEXT = 1
        const val ADD_BANK = "ADD_BANK"

        const val PREFIX_NUMBER = "(0)"
        const val SEPRATER = "-"
        const val REQUEST_ID = "REQUESTID"

        const val DETAILS_VIEW = "DETAILS_VIEW"
        const val CHAT_VIEW = "CHAT_VIEW"
        const val WALLET_VIEW = "WALLET_VIEW"
        const val NORMAL_VIEW = "NORMAL_VIEW"
        const val REJECTED_VIEW = "REJECTED_VIEW"
        const val PENDING_REQUESTS = "PENDING_REQUESTS"
        const val TOKEN_PURCHASE = "PURCHASE_TOKEN"
    }
}