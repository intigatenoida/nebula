package com.nibula.utils


import android.content.Context
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.util.Log
import android.view.View.GONE
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.FragmentActivity
import com.nibula.R
import com.nibula.request_to_invest.timerListiner
import com.nibula.utils.CommonUtils.Companion.getRemainingTimeHeader
import com.nibula.utils.CommonUtils.Companion.showLog
import java.lang.StringBuilder
import java.sql.Timestamp
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


@Suppress("JAVA_CLASS_ON_COMPANION")
class TimestampTimeZoneConverter {

    companion object {

        private var countDownTimer: CountDownTimer? = null
        const val UTC_TIME = "UTC"

        // Server date formats
        val SERVER_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val SERVER_DATE_FORMAT_WITHOUT_TIME =
            SimpleDateFormat("yyyy-MM-dd'T'00:00:00", Locale.getDefault())

        //--> Opens: 5/08/2020
        val SERVER_DATE_FORMAT_dd_MM_YYYY = "dd/MM/yyyy"

        val SERVER_DATE_FORMAT_WT_MS =
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())

        // Convert Timestamp from UTC TimeZone to Required TimeZone Timestamp
        fun convertFromUTCTimeZone(
            inputTimestamp: Timestamp?,
            requiredTimeZone: String?
        ): Timestamp? {
            return inputTimestamp?.let {
                requiredTimeZone?.let { it1 ->
                    convertTimestampTimeZone(
                        it,
                        UTC_TIME,
                        it1
                    )
                }
            }
        }

        // Convert a given TimeZone Timestamp to UTC TimeZone
        fun convertToUTCTimeZone(
            inputTimestamp: Timestamp?,
            currentTimeZone: String?
        ): Timestamp? {
            return inputTimestamp?.let {
                currentTimeZone?.let { it1 ->
                    convertTimestampTimeZone(
                        it,
                        it1,
                        UTC_TIME
                    )
                }
            }
        }

        // Main Function to Convert Timestamp TimeZones
        private fun convertTimestampTimeZone(
            inputTimestamp: Timestamp, fromTimeZone: String,
            toTimeZone: String
        ): Timestamp? {
            SERVER_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone(fromTimeZone))
            val date: Date
            try {
                date = SERVER_DATE_FORMAT.parse(inputTimestamp.toString())
                SERVER_DATE_FORMAT.timeZone = TimeZone.getTimeZone(toTimeZone)
                return Timestamp.valueOf(SERVER_DATE_FORMAT.format(date))
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return inputTimestamp
        }


        //--> Convert to local Date
        fun convertToLocalDate(
            date: String?,
            fromTimeZone: String,
            targetFormater: String = "dd MMMM yyyy"
        ): String {
            if (date == null || TextUtils.isEmpty(date)) {
                return ""
            }
            var ss = ""
            try {
                val sdf = SERVER_DATE_FORMAT
                sdf.timeZone = TimeZone.getTimeZone(fromTimeZone)
                val currDate = sdf.parse(date)
                val df = SimpleDateFormat(targetFormater, Locale.getDefault())
                df.timeZone = Calendar.getInstance().timeZone
                ss = df.format(currDate)
            } catch (e: Exception) {
                ss = ""
            }
            return ss
        }

        fun getLocalCalenderObj(
            date: String,
            fromTimeZone: String
        ): Calendar? {
            val sdf = SERVER_DATE_FORMAT
            sdf.timeZone = TimeZone.getTimeZone(fromTimeZone)
            val currDate = sdf.parse(date)
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = currDate.time
            calendar.timeZone = TimeZone.getDefault()
            return calendar
        }

        //--> get current Time Zone
        fun getLocalTimeZone(): TimeZone {
            return TimeZone.getDefault()
        }

        //--> Convert date to timeStamp
        fun convertDateToLocalTimeStamp(date: String, fromTimeZone: String): Long {
            val sdf = SERVER_DATE_FORMAT
            sdf.timeZone = TimeZone.getTimeZone(fromTimeZone)
            val mdate = sdf.parse(date)
//            sdf.timeZone = Calendar.getInstance().timeZone
            return mdate.time
        }

        /**
         * Timer for release
         *
         * @param serverTime
         * @param timeRemainig
         * @param textView
         * @return
         */
        @JvmStatic
        public fun setTimer(
            timeRemainig: Long,
            textView: TextView,
            mtimerListiner: timerListiner?
        ): CountDownTimer? {
            Log.d("Timer_values", "Server 3 :  \n Remainig : $timeRemainig")
            val countDownTimer: CountDownTimer = object : CountDownTimer(timeRemainig, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    textView.text = getDaysAndHourDifference(textView.context, millisUntilFinished)
                    showLog("Timer", "$timeRemainig")
                }

                override fun onFinish() {
                    mtimerListiner?.onTimerFinish()
                }
            }
            countDownTimer.start()
            return countDownTimer
        }


        //--> Time Differences
        fun getDaysAndHourDifference(
            context: Context,
            timeInMilliSecond: Long
        ): SpannableStringBuilder {
            var spanText: SpannableStringBuilder = SpannableStringBuilder()
//                getRemainingTimeHeader(context, CommonUtils.getFormattedNumber(0), "D")
            try {
                var msDiff = timeInMilliSecond
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60
                val hoursInMilli = minutesInMilli * 60
                val daysInMilli = hoursInMilli * 24

                val elapsedDays: Long = msDiff / daysInMilli
                msDiff %= daysInMilli

                val elapsedHours: Long = msDiff / hoursInMilli
                msDiff %= hoursInMilli

                val elapsedMinutes: Long = msDiff / minutesInMilli
                msDiff %= minutesInMilli

                val elapsedSeconds: Long = msDiff / secondsInMilli

                if (elapsedDays > 0) {
                    //D H
                    spanText = getRemainingTimeHeader(
                        context,
                        CommonUtils.getFormattedNumber(elapsedDays),
                        "D"
                    )
                } else {
                    spanText.clear()
                }

                if (elapsedDays > 0) {
                    if (elapsedHours > 0) {
                        spanText.append(" ")
                        spanText.append(
                            getRemainingTimeHeader(
                                context,
                                CommonUtils.getFormattedNumber(elapsedHours),
                                "H"
                            )
                        )

                    }
                } else {
                    if (elapsedHours > 0) {
                        spanText.clear()
                        spanText.append(" ")
                        spanText.append(
                            getRemainingTimeHeader(
                                context,
                                CommonUtils.getFormattedNumber(elapsedHours),
                                "H"
                            )
                        )

                    }
                }

                if (elapsedMinutes > 0) {
                    spanText.append(" ")
                    spanText.append(
                        getRemainingTimeHeader(
                            context,
                            CommonUtils.getFormattedNumber(elapsedMinutes),
                            "M"
                        )
                    )
                } else {
                    spanText.append(" ")
                    spanText.append(
                        getRemainingTimeHeader(
                            context,
                            CommonUtils.getFormattedNumber(0),
                            "M"
                        )
                    )
                }
                if (elapsedSeconds > 0) {
                    spanText.append(" ")
                    spanText.append(
                        getRemainingTimeHeader(
                            context,
                            CommonUtils.getFormattedNumber(elapsedSeconds),
                            "S"
                        )
                    )
                } else {
                    spanText.append(" ")
                    spanText.append(
                        getRemainingTimeHeader(
                            context,
                            CommonUtils.getFormattedNumber(0),
                            "S"
                        )
                    )

                }
                /* else {
                     if (elapsedHours > 0) {
                         //H M
                         spanText = getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedHours), "H")
                         if (elapsedMinutes > 0) {
                             spanText.append(" ")
                             spanText.append(getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedMinutes), "M"))
                         }else{
                             spanText.append(" ")
                             spanText.append(getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(0), "M"))
                             if (elapsedSeconds > 0) {
                                 spanText.append(" ")
                                 spanText.append(getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedSeconds), "S"))
                             }
                         }
                     } else {
                         if (elapsedMinutes > 0) {
                             //M S
                             spanText = getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedMinutes), "M")
                             if (elapsedSeconds > 0) {
                                 spanText.append(" ")
                                 spanText.append(getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedSeconds), "S"))
                             }
                         } else {
                             spanText = if (elapsedSeconds > 0) {
                                 //S
                                 getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedSeconds), "S")
                             } else {
                                 getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(0), "S")
                             }
                         }
                     }
                 }*/
            } catch (e: Exception) {

            } finally {
                return spanText
            }

        }


        fun getTimerDifference(
            daysTextView: AppCompatTextView,
            hourTextView: AppCompatTextView,
            minTextView: AppCompatTextView,
            secTextView: AppCompatTextView,

            timeInMilliSecond: Long
        ) {
            try {
                var msDiff = timeInMilliSecond
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60
                val hoursInMilli = minutesInMilli * 60
                val daysInMilli = hoursInMilli * 24

                val elapsedDays: Long = msDiff / daysInMilli
                msDiff %= daysInMilli

                val elapsedHours: Long = msDiff / hoursInMilli
                msDiff %= hoursInMilli

                val elapsedMinutes: Long = msDiff / minutesInMilli
                msDiff %= minutesInMilli

                val elapsedSeconds: Long = msDiff / secondsInMilli

                if (elapsedDays > 0) {
                    // Set Elapsed days
                    daysTextView.text = elapsedDays?.toString()

                } else {
                    // Set Zero day
                    daysTextView.text = "00"

                }

                if (elapsedDays > 0) {
                    if (elapsedHours > 0) {
                        hourTextView.text = elapsedHours.toString()
                        // Set Elapsed Hour

                    }
                } else {
                    if (elapsedHours > 0) {
                        // Set Elapsed Hour
                        hourTextView.text = elapsedHours.toString()

                    }
                }

                if (elapsedMinutes > 0) {
                    // Set Elapsed Minutes
                    minTextView.text = elapsedMinutes.toString()

                } else {
                    // Set Elapsed Zero Minutes
                    minTextView.text = "00"
                }
                if (elapsedSeconds > 0) {
                    // Set Elapsed Second
                    secTextView.text = elapsedSeconds.toString()


                } else {
                    secTextView.text = "00"

                    // Set Elapsed Seconds

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        //--> Time Differences
        fun getDaysAndHourDifferenceNew(
            context: Context,
            timeInMilliSecond: Long
        ): SpannableStringBuilder {
            var spanText: SpannableStringBuilder = SpannableStringBuilder()
//                getRemainingTimeHeader(context, CommonUtils.getFormattedNumber(0), "D")
            try {
                var msDiff = timeInMilliSecond
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60
                val hoursInMilli = minutesInMilli * 60
                val daysInMilli = hoursInMilli * 24

                val elapsedDays: Long = msDiff / daysInMilli
                msDiff %= daysInMilli

                val elapsedHours: Long = msDiff / hoursInMilli
                msDiff %= hoursInMilli

                val elapsedMinutes: Long = msDiff / minutesInMilli
                msDiff %= minutesInMilli

                val elapsedSeconds: Long = msDiff / secondsInMilli

                if (elapsedDays > 0) {
                    //D H
                    spanText = getRemainingTimeHeader(
                        context,
                        CommonUtils.getFormattedNumber(elapsedDays),
                        "D"
                    )
                } else {
                    spanText.clear()
                }

                if (elapsedDays > 0) {
                    if (elapsedHours > 0) {
                        spanText.append(" ")
                        spanText.append(
                            getRemainingTimeHeader(
                                context,
                                CommonUtils.getFormattedNumber(elapsedHours),
                                "H"
                            )
                        )

                    }
                } else {
                    if (elapsedHours > 0) {
                        spanText.clear()
                        spanText.append(" ")
                        spanText.append(
                            getRemainingTimeHeader(
                                context,
                                CommonUtils.getFormattedNumber(elapsedHours),
                                "H"
                            )
                        )

                    }
                }

                if (elapsedMinutes > 0) {
                    spanText.append(" ")
                    spanText.append(
                        getRemainingTimeHeader(
                            context,
                            CommonUtils.getFormattedNumber(elapsedMinutes),
                            "M"
                        )
                    )
                } else {
                    spanText.append(" ")
                    spanText.append(
                        getRemainingTimeHeader(
                            context,
                            CommonUtils.getFormattedNumber(0),
                            "M"
                        )
                    )
                }

                /* else {
                     if (elapsedHours > 0) {
                         //H M
                         spanText = getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedHours), "H")
                         if (elapsedMinutes > 0) {
                             spanText.append(" ")
                             spanText.append(getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedMinutes), "M"))
                         }else{
                             spanText.append(" ")
                             spanText.append(getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(0), "M"))
                             if (elapsedSeconds > 0) {
                                 spanText.append(" ")
                                 spanText.append(getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedSeconds), "S"))
                             }
                         }
                     } else {
                         if (elapsedMinutes > 0) {
                             //M S
                             spanText = getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedMinutes), "M")
                             if (elapsedSeconds > 0) {
                                 spanText.append(" ")
                                 spanText.append(getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedSeconds), "S"))
                             }
                         } else {
                             spanText = if (elapsedSeconds > 0) {
                                 //S
                                 getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(elapsedSeconds), "S")
                             } else {
                                 getRemainingTimeHeader(context,CommonUtils.getFormattedNumber(0), "S")
                             }
                         }
                     }
                 }*/
            } catch (e: Exception) {

            } finally {
                return spanText
            }

        }

        //--> Time Differences
        fun getDaysAndHours(
            context: Context,
            timeInMilliSecond: Long
        ): String {

            try {
                var msDiff = timeInMilliSecond
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60
                val hoursInMilli = minutesInMilli * 60
                val daysInMilli = hoursInMilli * 24

                val elapsedDays: Long = msDiff / daysInMilli
                msDiff %= daysInMilli

                val elapsedHours: Long = msDiff / hoursInMilli
                msDiff %= hoursInMilli

                return "$elapsedDays" + " days" + " $elapsedHours" + " hours"
            } catch (e: Exception) {

            }
            return ""
        }


        //--> Time Differences
        fun getDaysAndHourDifferenceNew(
            context: Context,
            timeInMilliSecond: Long,
            tvDays: AppCompatTextView?,
            tvHours: AppCompatTextView?,
            tvMinutes: AppCompatTextView?
        ) {
            try {
                var msDiff = timeInMilliSecond
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60
                val hoursInMilli = minutesInMilli * 60
                val daysInMilli = hoursInMilli * 24

                val elapsedDays: Long = msDiff / daysInMilli
                msDiff %= daysInMilli

                val elapsedHours: Long = msDiff / hoursInMilli
                msDiff %= hoursInMilli

                val elapsedMinutes: Long = msDiff / minutesInMilli
                msDiff %= minutesInMilli

                tvDays?.text = elapsedDays.toString()
                tvHours?.text = elapsedHours.toString()
                tvMinutes?.text = elapsedMinutes.toString()
                val elapsedSeconds: Long = msDiff / secondsInMilli


            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        /**
         * Stop Timer
         *
         * @param countDownTimer
         */
        fun stopTimer() {
            if (countDownTimer != null) {
                countDownTimer!!.cancel()
                countDownTimer = null
            }
        }


        /**
         * Get Time Stamp For Number Of hours
         *
         * @param hours
         * @return
         */
        public fun convertToMilliseconds(sec: Long): Long {
            return java.lang.Long.valueOf(sec * 1000.toLong())
        }


        //--> Update Timer on TextView
        fun setTimerTextView(
            secondsLeft: Long,
            textView: AppCompatTextView,
            mtimerListiner: timerListiner?
        ): CountDownTimer? {
            //--> Show Log
            showLog(this.javaClass.simpleName, "$secondsLeft")

            val timer = convertToMilliseconds(secondsLeft)
            when {
                //---> Case when album released  time is + 1.5 days from current Day
                (secondsLeft) > 1 -> {
                    //(secondsLeft / 3600) > (24*100) -> {
                    //---> setting Timer Value
                    textView.text = timer?.let { getDaysAndHourDifference(textView.context, it) }
                }

                (secondsLeft) < 1 -> {
                    textView.visibility = GONE
                }
                else -> countDownTimer = timer?.let { setTimer(it, textView, mtimerListiner) }
            }
            return countDownTimer
        }


        fun setTimerTextViewNewOffering(
            secondsLeft: Long,
            daysTextView: AppCompatTextView,
            hourTextView: AppCompatTextView,
            minTextView: AppCompatTextView,
            secTextView: AppCompatTextView,
            mtimerListiner: timerListiner?
        ): CountDownTimer? {
            //--> Show Log
            showLog(this.javaClass.simpleName, "$secondsLeft")

            val timer = convertToMilliseconds(secondsLeft)
            when {
                //---> Case when album released  time is + 1.5 days from current Day
                (secondsLeft) > 1 -> {
                    //(secondsLeft / 3600) > (24*100) -> {
                    //---> setting Timer Value
                    timer?.let {
                        getTimerDifference(
                            daysTextView,
                            hourTextView,
                            minTextView,
                            secTextView,
                            it
                        )
                    }
                }

                (secondsLeft) < 1 -> {
                    // textView.visibility = GONE
                }
/*
                else -> countDownTimer = timer?.let { setTimer(it, textView, mtimerListiner) }
*/
            }
            return countDownTimer
        }


        //--> Convert to Days only
        fun convertEndDateToCounterDate(
            context: FragmentActivity,
            endDate: String
        ): Spannable? {
            var spannable: Spannable? = null
            try {
                val sdf = SERVER_DATE_FORMAT
                sdf.timeZone = TimeZone.getTimeZone(UTC_TIME)
                val date = sdf.parse(endDate)
                sdf.timeZone = Calendar.getInstance().timeZone
                val currTime = System.currentTimeMillis()
                val endTime = date.time
                if (currTime > endTime) {
                    val temp = "0 ${context.getString(R.string.days_to_go)}"
                    spannable = CommonUtils.setSpannablewithsize(context, temp, 0, 1)
                } else {
                    var temp = "0 Days to go"
                    val msDiff = endTime - currTime
                    val day = TimeUnit.MILLISECONDS.toDays(msDiff)
                    when {
                        day == 0L -> {
                            temp = "0 ${context.getString(R.string.days_to_go)}"
                            spannable = CommonUtils.setSpannablewithsize(context, temp, 0, 1)
                        }
                        1 <= day && day <= 9 -> {
                            temp = "$day ${context.getString(R.string.days_to_go)}"
                            spannable = CommonUtils.setSpannablewithsize(context, temp, 0, 1)
                        }
                        day > 9 -> {
                            temp = "$day ${context.getString(R.string.days_to_go)}"
                            spannable = CommonUtils.setSpannablewithsize(context, temp, 0, 2)
                        }
                    }
                }
            } catch (e: Exception) {

            }
            return spannable
        }

        //Please dont change strict guidelines
        //-- Convert date to UTC date
        fun convertToUTCDate(dates: Long): String {
            val stamp = Timestamp(dates)
            val sdf = SERVER_DATE_FORMAT
            val date = Date(stamp.time)
            sdf.timeZone = Calendar.getInstance().timeZone
            val convrttedDate = sdf.format(date)
            val currDate = sdf.parse(convrttedDate)
            sdf.timeZone = TimeZone.getTimeZone(UTC_TIME)
            return sdf.format(currDate!!)
        }


        fun convertToLocaleTimeZone(dates: Long): String {
            val stamp = Timestamp(dates)
            val sdf = SERVER_DATE_FORMAT
            val date = Date(stamp.time)
            sdf.timeZone = Calendar.getInstance().timeZone
            val convrttedDate = sdf.format(date)
            val currDate = sdf.parse(convrttedDate)
            sdf.timeZone = TimeZone.getDefault()
            return sdf.format(currDate!!)
        }
    }


}