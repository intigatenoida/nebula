package com.google.android.material.bottomsheet;


import android.view.View
import androidx.annotation.VisibleForTesting
import androidx.viewpager.widget.ViewPager
import java.lang.ref.WeakReference

open class BottomSheetBehaviorFix<V : View> : BottomSheetBehavior<V>(),
    ViewPager.OnPageChangeListener {

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        val container = viewRef?.get() ?: return
        nestedScrollingChildRef = WeakReference(findScrollingChild(container))
    }

    @VisibleForTesting
    protected override fun findScrollingChild(view: View?): View? {
        return if (view is ViewPager) {
            view.focusedChild?.let { findScrollingChild(it) }
        } else {
            super.findScrollingChild(view)
        }
    }
//    @VisibleForTesting
//    override fun findScrollingChild(view: View): View? {
//        return if (view is ViewPager) {
//            view.focusedChild?.let { findScrollingChild(it) }
//        } else {
//            super.findScrollingChild(view)
//        }
//    }
}