package com.nibula.utils

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.base.BaseActivity
import com.nibula.databinding.FragmentUploadStepBinding

class UserAccessActivity : BaseActivity() {
    lateinit var binding:FragmentUploadStepBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.statusBarColor = ContextCompat.getColor(this,R.color.colorAccent)
        binding=FragmentUploadStepBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setContentView(R.layout.fragment_upload_step)
        val otp = intent?.getStringExtra(AppConstant.ID) ?: ""
        if (otp.isNotEmpty() && otp.length == 4) {
            binding.text1.text = otp[0].toString()
            binding.text2.text = otp[1].toString()
            binding.text3.text = otp[2].toString()
            binding.text4.text = otp[3].toString()
        }

        binding.done.setOnClickListener {
            onBackPressed()
        }

    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        finish()
    }
}