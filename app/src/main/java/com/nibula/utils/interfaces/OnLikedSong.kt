package com.nibula.utils.interfaces

interface OnLikedSong {

    fun onLikedSong(recordId: String, position: Int, fileId: Int?)
}