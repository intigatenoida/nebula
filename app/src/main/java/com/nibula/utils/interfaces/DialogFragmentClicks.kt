package com.nibula.utils.interfaces


import android.app.Dialog
import android.os.Bundle

interface DialogFragmentClicks {
    fun onPositiveButtonClick(obj:Any,bundle: Bundle?,dialog: Dialog)
    fun onNegativeButtonClick(obj:Any,bundle: Bundle?,dialog: Dialog)
}