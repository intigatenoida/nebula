package com.nibula.utils.interfaces


import android.app.Dialog
import android.os.Bundle

interface CancelUplaodClicks {
    fun onCancelUpload(obj: Any, bundle: Bundle?, dialog: Dialog)
    fun onContinue(obj: Any, bundle: Bundle?, dialog: Dialog)
}