package com.nibula.base.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class PlaySongEntity(
    @SerializedName("SongId")
    @PrimaryKey var songId: Int,
    @SerializedName("ListeningTimeInSeconds")
    var timeInSecond: Int,
    @SerializedName("DeviceId")
    var deviceId: String
)
