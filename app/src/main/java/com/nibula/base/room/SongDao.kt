package com.nibula.base.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface SongDao {
    @Query("SELECT * FROM playsongentity")
    fun getAll(): List<PlaySongEntity>

    @Query("SELECT * FROM playsongentity where songId=:songId")
    fun getRow(songId: Int): PlaySongEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(products: PlaySongEntity)

    @Query("Update playsongentity set timeInSecond=timeInSecond + :time where songId=:songId ")
    fun updateTime(time: Int, songId: Int)

}