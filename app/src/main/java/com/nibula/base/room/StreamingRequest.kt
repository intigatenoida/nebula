package com.nibula.base.room

import com.google.gson.annotations.SerializedName

class StreamingRequest {
    @SerializedName("JsonRequest")
    public var jsonRequest = listOf<PlaySongEntity>()
}