package com.nibula.base

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.text.Layout
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AlignmentSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.nibula.R
import com.nibula.addBank.fragment.AddBankFragment
import com.nibula.addBank.fragment.BankListFragment
import com.nibula.base.room.NebulaDatabase
import com.nibula.base.room.PlaySongEntity
import com.nibula.costbreakdown.fragment.CostBreakDownFragment
import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.customview.CustomToast
import com.nibula.dashboard.ConstantsNavTabs
import com.nibula.dashboard.ConstantsNavTabs.NAV_TAB_HOME
import com.nibula.dashboard.DashboardActivity
import com.nibula.dashboard.ui.home.HomeFragment
import com.nibula.dashboard.vmodel.BaseActivityViewModel
import com.nibula.investorcertificates.fragment.InvestorsCertificatesFragment
import com.nibula.investorcertificates.fragment.ViewCollectorsFragment
import com.nibula.pending_request.PendingInvestorFragment
import com.nibula.pending_request.PendingRequestFragment
import com.nibula.request_to_buy.BuyFinalFragment
import com.nibula.request_to_buy.RequestToBuyFragment
import com.nibula.request_to_buy.navigate
import com.nibula.request_to_invest.*
import com.nibula.response.BaseResponse
import com.nibula.response.details.AlbumDetailResponse
import com.nibula.response.details.File
import com.nibula.response.details.Response
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.search.ui.SearchFragment
import com.nibula.share.ui.ShareBottomSheetDialog
import com.nibula.share.ui.ShareFragment
import com.nibula.upload_music.ui.savedrafts.SavedDraftsActivity
import com.nibula.user.Follower.FollowerFollowingFragment
import com.nibula.user.atrist_profile.ArtistProfileFragment
import com.nibula.user.login.LoginActivity
import com.nibula.useractivity.ui.ChatFragment
import com.nibula.useractivity.ui.UserActivityFragment
import com.nibula.utils.AppConstant
import com.nibula.utils.PrefUtils
import com.nibula.utils.RequestCode
import com.nibula.utils.interfaces.DialogFragmentClicks
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("Registered")
open class BaseActivity() : AppCompatActivity(), navigate {
    private lateinit var progress: ViewGroup
    private var loadingView: View? = null

    fun setFragment(fragment: Fragment, isAddToBackStack: Boolean) {
        val backStateName: String = fragment::class.java.name
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.maincontainer, fragment)
        if (isAddToBackStack) {
            ft.addToBackStack(backStateName)
        }
        ft.commit()
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun disableAutofill() {
        window.decorView.importantForAutofill = View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS
    }

    fun replaceFragment(
        fragment: Fragment,
        id: Int,
        tag: String,
        isAddToBackStack: Boolean = false
    ) {
        val tran = supportFragmentManager.beginTransaction()
        tran.replace(id, fragment)
        if (isAddToBackStack) {
            tran.addToBackStack(tag)
        }
        tran.commit()
        manageBottomNavigation(fragment)
    }

    fun showToast(
        context: Context?,
        msg: String,
        type: CustomToast.ToastType,
        duration: Int = Toast.LENGTH_SHORT
    ) {
        if (msg.isNotEmpty()) {
            CustomToast.showToast(
                context, msg, type, duration
            )
        }
    }

    fun showToastCenter(msg: String) {
        val centeredText: Spannable = SpannableString(msg)
        centeredText.setSpan(
            AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
            0, if (msg.isNotEmpty()) {
                msg.length - 1
            } else {
                0
            },
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        Toast.makeText(this, centeredText, Toast.LENGTH_SHORT).show()
    }

    //--> After Album Detail - > Shares purchase Screen -> Success
    override fun goToPaymentSuccess(
        paymentSuccess: Boolean,
        record: Response,
        totalAmount: Double,
        shares: Int,
        investmentId: Int
    ) {
        val bundle = Bundle()
        bundle.putBoolean("paymentSuccess", paymentSuccess)
        bundle.putParcelable("record", record)
        bundle.putDouble("totalAmount", totalAmount)
        bundle.putInt("shares", shares)
        bundle.putInt("investmentId", investmentId)
        pushFragments(
            viewModel.mCurrentTab,
            InvestmentSuccesful.newInstance(bundle),
            true
        )
        // setFragment(RequestFinalFragment(), true, R.id.containerFragment)
    }

    override fun pushFragments(fg: Fragment) {
        pushFragments(viewModel.mCurrentTab, fg, true)
    }

    override fun changeTopTab(changeTab: String) {

    }

    override fun openInvestorScreen(recordDetail: AlbumDetailResponse) {
        val bundle = Bundle()
        bundle.putParcelable(AppConstant.RECORD_DETAIL, recordDetail)
        val fragment = PendingInvestorFragment.newInstance()
        fragment.arguments = bundle
        pushFragments(viewModel.mCurrentTab, fragment, true)

//        setFragment(PendingInvestorFragment(), true, R.id.containerFragment)
    }

    override fun openCostBreakDown(costBreakdownResponse: CostBreakdownResponse.Response) {
        val bundle = Bundle()
        bundle.putParcelable(AppConstant.COST_BREAKDOWN, costBreakdownResponse)
        val fragment = CostBreakDownFragment.newInstance()
        fragment.arguments = bundle
        pushFragments(viewModel.mCurrentTab, fragment, true)
    }

    override fun openSavedDraftsList() {
        startActivity(
            Intent(
                this,
                SavedDraftsActivity::class.java
            )
        )
    }

    override fun selectedShareScreen(bundle: Bundle) {
        TODO("Not yet implemented")
    }

    override fun goToBuyNow(bundle: Bundle) {
        TODO("Not yet implemented")
    }

    override fun openDashboard() {

    }

    override fun openWalletScreen() {

    }

    override fun openMusicScreen() {

    }


    override fun openUploadSong() {

    }

    //::Purchase share
/*
    override fun selectedShareScreen(bundle: Bundle) {
        val fg = RequestToInvestPriceInfoFragment.newInstance(bundle)
        pushFragments(viewModel.mCurrentTab, fg, true)
    }

    //::Buy Now
    override fun goToBuyNow(bundle: Bundle) {
        val fg = RequestToInvestPriceInfoFragment.newInstance(bundle)
        pushFragments(viewModel.mCurrentTab, fg, true)
    }
*/

    override fun changeTab(tab: String) {

    }

    override fun clearLogOut() {
        mStacks[ConstantsNavTabs.NAV_TAB_HOME]?.clear()
        mStacks[ConstantsNavTabs.NAV_TAB_MUSIC]?.clear()
        mStacks[ConstantsNavTabs.NAV_TAB_ASSETS]?.clear()
        mStacks[ConstantsNavTabs.NAV_TAB_PROFILE]?.clear()
    }

    //--> After Successfull Investment
    override fun gotoFinal(data: Response?, shareCount: Int) {
        val fragment = RequestFinalFragment.newInstance().apply {
            arguments = Bundle().apply {
                putParcelable(AppConstant.RECORD_DETAIL, data) // Record Detail
                putInt(AppConstant.SHARES_PURCHASED, shareCount) // Shares purchased
            }
        }
        pushFragments(viewModel.mCurrentTab, fragment, true)
    }

    override fun finishActivity() {
    }

    override fun gotorequestinvest(albumId: String?) {
    }

    /*   override fun finishActivity() {
           supportFragmentManager.popBackStack(
               RecordDetailFragmentNew::class.java.name,
               FragmentManager.POP_BACK_STACK_INCLUSIVE
           )
       }
   */
    override fun finishBuyReleaseFragment() {
        supportFragmentManager.popBackStack(
            RequestToBuyFragment::class.java.name,
            FragmentManager.POP_BACK_STACK_INCLUSIVE
        )
    }

/*    override fun gotorequestinvest(albumId: String?) {
        val bundle = Bundle()
        bundle.putString(AppConstant.ID, albumId)
        bundle.putBoolean("isShowBottom", false)
        pushFragments(viewModel.mCurrentTab, RecordDetailFragmentNew.newInstance(bundle), true)
        //pushFragments(viewModel.mCurrentTab, OfferingFragment.newInstance(bundle), true)
    }*/

    override fun goToNewOffering(bundle: Bundle) {
        if (isOnline()) {
            bundle.putBoolean("isShowBottom", false)
            // pushFragments(viewModel.mCurrentTab, RecordDetailFragmentNew.newInstance(bundle), true)
             pushFragments(viewModel.mCurrentTab, OfferingFragment.newInstance(bundle), true)
            // pushFragments(viewModel.mCurrentTab, NewOfferingFragment.newInstance(bundle), true)

           /* findNavController(R.id.nav_host_fragment_activity_main).navigate(
               R.id.action_navigation_home_to_offeringFragment,
                bundle
            )
*/
            //findNavController(R.id.nav_host_fragment_activity_main).navigate(R.id.action_navigation_home_to_offeringFragment)


        /*    val action = HomeFragmentDirections.actionNavigationHomeToOfferingFragment()
             findNavController(R.id.nav_host_fragment_activity_main).navigate(action)*/
        }

    }


    override fun goToCertificatesScreen(albumId: String?) {
        val bundle = Bundle()
        bundle.putString(AppConstant.ID, albumId)
        pushFragments(
            viewModel.mCurrentTab,
            InvestorsCertificatesFragment.newInstance(bundle),
            true
        )
    }

    override fun goToViewCollectorsScreen(recordId: String?) {
        val bundle = Bundle()
        bundle.putString(AppConstant.RECORD_ID, recordId)
        pushFragments(
            viewModel.mCurrentTab,
            ViewCollectorsFragment.newInstance(bundle),
            true
        )
    }

    override fun goToProfileScreen() {

    }

    override fun onClickNotification() {
//        replaceFragment(UserActivityFragment.newInstance(), R.id.containerFragment, "")
        pushFragments(viewModel.mCurrentTab, UserActivityFragment.newInstance(), true)
    }

    override fun onClickSearch() {
//      replaceFragment(SearchFragment.newInstance(), R.id.containerFragment, "")
        pushFragments(viewModel.mCurrentTab, SearchFragment.newInstance(), true)
    }

    override fun onClickBuy() {
        pushFragments(viewModel.mCurrentTab, RequestToBuyFragment.newInstance(), true)
        //setFragment(RequestToBuyFragment(), true, R.id.containerFragment)
    }

    //success screen after paid for album
    override fun buyFinal() {
        pushFragments(viewModel.mCurrentTab, BuyFinalFragment.newInstance(), true)
        //setFragment(BuyFinalFragment(), true, R.id.containerFragment)
    }

    override fun openTopArtist(id: String, userTypeId: Int) {
        val bundle = Bundle()
        bundle.putString(AppConstant.ID, id)
        bundle.putInt(AppConstant.STATUS, userTypeId)
        pushFragments(viewModel.mCurrentTab, ArtistProfileFragment.newInstance(bundle), true)
        //setFragment(ArtistProfileFragment(), true, R.id.containerFragment)
    }

    override fun openChat(bundle: Bundle) {
        pushFragments(viewModel.mCurrentTab, ChatFragment.newInstance(bundle), true)
    }

    override fun openPendingFragment(recordId: String) {

        val fragment = PendingRequestFragment.newInstance().apply {
            arguments = Bundle().apply {
                putString(AppConstant.ID, recordId)
            }
        }
        pushFragments(fragment)
    }

    override fun onTokenPurchased(recordId: String) {
        val bundle = Bundle()
        bundle.putString(AppConstant.RECORD_ID, recordId)
        pushFragments(
            viewModel.mCurrentTab,
            ViewCollectorsFragment.newInstance(bundle),
            true
        )
    }

    override fun playSong(
        songIdList: MutableList<File>,
        selectedIndex: Int,
        fromTop: Boolean,
        recordTitleStr: String,
        recordId: String,
        isReleased: Int,
        date: String,
        isRecordChanged: Boolean
    ) {

    }

    override fun pauseMusic() {

    }


    override fun manualBack(levelBack: Int) {
        popFragments(levelBack)
    }

    override fun uploadRecord() {

    }

    lateinit var mStacks: HashMap<String, Stack<Fragment>>
    lateinit var viewModel: BaseActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //disableAutofill();
        viewModel = ViewModelProvider(this).get(BaseActivityViewModel::class.java)

        viewModel.getBackManualBack().observe(this, Observer<Boolean> { t ->
            if (t == true) {
                popFragments()
            }
        })
    }

    fun pushFragments(
        tag: String?,
        fragment: Fragment,
        shouldAdd: Boolean,
        shouldAnimate: Boolean = true
    ) {
        if (shouldAdd) mStacks[tag]!!.push(fragment)
        val manager = supportFragmentManager
        val ft: FragmentTransaction = manager.beginTransaction()
        if (shouldAnimate) {
            /*     //ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right)
                 ft.setCustomAnimations(
                     R.anim.slide_in_left,  // Enter animation
                     R.anim.slide_out_right,  // Exit animation
                     R.anim.slide_in_left,  // Pop enter animation
                     R.anim.slide_out_right  // Pop exit animation
                 )*/
        }
        ft.replace(R.id.containerFragment, fragment)
        ft.setReorderingAllowed(true)

        ft.commit()
        manageBottomNavigation(fragment)
    }

    private fun manageBottomNavigation(fragment: Fragment) {
        if (fragment !is ChatFragment) {

            DashboardActivity.activityDashboardBinding.navView.visibility = View.VISIBLE

        } else {
            DashboardActivity.activityDashboardBinding.navView.visibility = View.GONE
        }
    }

    fun popFragments(levelBack: Int = 0) {
        Log.d("FragmentPOP", "POP")
        val currentTabStack = mStacks[viewModel.mCurrentTab] ?: return
        val fragment: Fragment?
        if (levelBack == 0) {
            fragment = currentTabStack.elementAt(currentTabStack.size - 2)
            currentTabStack.pop()
        } else {
            fragment = currentTabStack.elementAt(currentTabStack.size - (levelBack + 1))
            for (i in 1..levelBack) {
                currentTabStack.pop()
            }
        }
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()
/*
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right)
*/

        ft.replace(R.id.containerFragment, fragment)
        ft.commit()
        manageBottomNavigation(fragment)
    }

    fun isSearchOnTop(): Boolean {
        return (mStacks[viewModel.mCurrentTab]?.lastElement() is SearchFragment)
    }

    fun getProfileTopFragment(): Fragment? {
        return (mStacks[viewModel.mCurrentTab]?.lastElement())
    }

    fun isUserActivityOnTop(): Boolean {
        return (mStacks[viewModel.mCurrentTab]?.lastElement() is UserActivityFragment)
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (::mStacks.isInitialized) {

            if ((mStacks[viewModel.mCurrentTab]?.lastElement() as BaseFragment?)?.onBackPressed() === false) {
                if (mStacks[viewModel.mCurrentTab]!!.size == 1 && viewModel.mCurrentTab == NAV_TAB_HOME) {
                    super.onBackPressed() // or call finish..
                    finish()
                } else if (mStacks[viewModel.mCurrentTab]!!.size == 1 && viewModel.mCurrentTab != NAV_TAB_HOME) {
                    viewModel.mCurrentTab = NAV_TAB_HOME
                    DashboardActivity.activityDashboardBinding.navView.selectedItemId = R.id.navigation_home
                    pushFragments(viewModel.mCurrentTab, HomeFragment(), false)
                } else {
                    popFragments()
                }
            } else {
                //do nothing.. fragment already handled back button press.
            }
        } else {
            super.onBackPressed()
        }
        /*if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
        } else {
            supportFragmentManager.popBackStack()
        }*/
    }
/*

    fun showProcessDialog() {
//        val fragmentTransaction = supportFragmentManager.beginTransaction()
//        val prev = supportFragmentManager.findFragmentByTag("ProcessDialog")
//        if (prev != null) {
//            val df = prev as ProgressDialog
//            df.dismissAllowingStateLoss()
////            fragmentTransaction.remove(prev)
//        }
//        fragmentTransaction.addToBackStack(null)
//        dialogFragment = ProgressDialog.getInstance()
//        fragmentTransaction.add(dialogFragment, "ProcessDialog").commitAllowingStateLoss()
//        dialogFragment.show(fragmentTransaction, "ProcessDialog")

        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (loadingView == null) {
            loadingView = inflater.inflate(R.layout.process_loader, null)
        }
        if (!::progress.isInitialized) {
            progress = window.decorView.findViewById(android.R.id.content)

        }
        hideProgressDialog()

        progress.addView(loadingView)

    }

    private fun hideProgressDialog() {
        if (::progress.isInitialized && loadingView != null*/
/* && progress.childCount > 0*//*
) {
            progress.removeView(loadingView)
        }
    }
*/



    fun showProcessDialog() {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (loadingView == null) {
            loadingView = inflater.inflate(R.layout.process_loader, null)
        }

        if (!::progress.isInitialized) {
            try {
                progress = this.window?.decorView?.findViewById(android.R.id.content)!!
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        hideProgressDialog()
        progress.addView(loadingView)
    }

    fun hideProgressDialog() {
        if (::progress.isInitialized && loadingView != null) {
            progress.removeView(loadingView)
        }
    }


    override fun showForceUpdateDialog(listener: ForceUpdateDialog.ForceUpdateListener) {
//        val fragmentTransaction = supportFragmentManager.beginTransaction()
//        val prev = supportFragmentManager.findFragmentByTag("UpdateDialog")
//        if (prev != null) {
//            val df = prev as ProgressDialog
//            df.dismissAllowingStateLoss()
////            fragmentTransaction.remove(prev)
//        }
//        fragmentTransaction.addToBackStack(null)
//        dialogFragment = ForceUpdateDialog.getInstance(listener)
//        fragmentTransaction.add(dialogFragment, "UpdateDialog").commitAllowingStateLoss()
    }

    override fun showFollower(artistBundle: Bundle) {
        val fragment = FollowerFollowingFragment.getInstance().apply {
            arguments = artistBundle
        }
        pushFragments(viewModel.mCurrentTab, fragment, true)
    }

    fun hideProcessDailog() {
        if (::progress.isInitialized && loadingView != null/* && progress.childCount > 0*/) {
            progress.removeView(loadingView)
        }

//        val fragmentTransaction = supportFragmentManager.beginTransaction()
//        val prev = supportFragmentManager.findFragmentByTag("ProcessDialog")
//        if (prev != null) {
//            val df = prev as ProgressDialog
//            df.dismissAllowingStateLoss()
////            fragmentTransaction.remove(prev)
//        }
//        if (::dialogFragment.isInitialized) {
//            dialogFragment.dismiss()
//
//        }
    }

    open fun isOnline(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    open fun bindLog(className: String) {
        Log.d(className, "Inside $className")
    }

    override fun onClickShare(recordId: String, title: String, isRelease: Int, date: String) {

        val dialog = ShareBottomSheetDialog(object : DialogFragmentClicks {
            override fun onPositiveButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
                if (PrefUtils.getValueFromPreference(this@BaseActivity, PrefUtils.USER_ID)
                        .isEmpty()
                ) {
                    val intent = Intent(this@BaseActivity, LoginActivity::class.java)
                    intent.putExtra(AppConstant.ISFIRSTTIME, false)
                    startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
                    return
                }
                val fg = ShareFragment()
                val bundle1 = Bundle()
                bundle1.putString(AppConstant.ID, recordId)
                bundle1.putInt(AppConstant.RECORD_STATUS, isRelease)
                bundle1.putString(AppConstant.RECORD_TITLE, title)
                bundle1.putString(AppConstant.RELEASED_DATE, date)
                fg.arguments = bundle1
                pushFragments(viewModel.mCurrentTab, fg, true)
                dialog.dismiss()
            }

            override fun onNegativeButtonClick(obj: Any, bundle: Bundle?, dialog: Dialog) {
//                if (PrefUtils.getValueFromPreference(this@BaseActivity, PrefUtils.USER_ID)
//                        .isEmpty()
//                ) {
//                    val intent = Intent(this@BaseActivity, LoginActivity::class.java)
//                    intent.putExtra(AppConstant.ISFIRSTTIME, false)
//                    startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE)
//                    return
//                }
                val sharingHelper = SharingHelper()
                sharingHelper.getExternalShareUrl(recordId, this@BaseActivity)
                dialog.dismiss()
//                checkAccessCode()
            }
        })

        dialog.show(supportFragmentManager, "ShareDialogFragment")
    }

    override fun openBankList(bundle: Bundle) {
        val fg = BankListFragment.getInstance()
        fg.arguments = bundle
        pushFragments(viewModel.mCurrentTab, fg, true)
    }

    override fun addBank(bundle: Bundle?) {
        val fg = AddBankFragment.newInstance()
        bundle?.let { fg.arguments = bundle }
        pushFragments(viewModel.mCurrentTab, fg, true)
    }

    fun hideKeyboard() {
        if (currentFocus == null) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            return
        }
        val inputMethodManager = getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            currentFocus!!.windowToken, 0
        )
    }

    fun pushStrimingData() {
        try {
            val data = NebulaDatabase.getDatabase(this).songDao().getAll()
            if (data.isNotEmpty()) {
                val newdata: ArrayList<PlaySongEntity> = arrayListOf()
                data.forEach {
                    if (it.timeInSecond > 0) {
                        newdata.add(it)
                    }
                }

                if (newdata.isNullOrEmpty()) {
                    return
                }
                val jsonText = Gson().toJson(newdata)
                ApiClient.getClientMusic(this).create(ApiAuthHelper::class.java).apply {
                    saveSongStreamingTime(
                        jsonText
                    )
                        .enqueue(object : CallBackManager<BaseResponse>() {
                            override fun onSuccess(any: BaseResponse?, message: String) {
                                if (any != null && any.responseStatus == 1) {
                                    NebulaDatabase.getDatabase(this@BaseActivity).clearAllTables()
                                }
                            }

                            override fun onFailure(message: String) {
                            }

                            override fun onError(error: RetroError) {
                            }
                        })
                }
            }
        } catch (e: Exception) {

        }


        4576 + 4254 + 10000 + 50000 + 35000
    }
}