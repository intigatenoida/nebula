package com.nibula.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.view.View.*
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.nibula.R
import com.nibula.customview.CustomToast
import com.nibula.dashboard.vmodel.BaseActivityViewModel
import com.nibula.request_to_buy.navigate
import com.nibula.user.login.LoginActivity
import com.nibula.utils.AppConstant
import com.nibula.utils.CommonUtils
import com.nibula.utils.RequestCode


open class BaseFragment : Fragment() {

    //lateinit var playerBottomSheet: BottomSheetDialog

    lateinit var mContext: Context

    lateinit var navigate: navigate
    lateinit var baseViewModel: BaseActivityViewModel

    lateinit var apiBaseModel: BaseViewModel

    lateinit var handler: Handler
    var millisStart: Long = 0L

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigate = context as navigate
        mContext = context
    }

    open fun isOnline(context: Context): Boolean {
        val cm =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun showProcessDialog() {
        if (::mContext.isInitialized) {
            (mContext as BaseActivity).showProcessDialog()
        }
    }

    fun hideProcessDialog() {
        if (::mContext.isInitialized) {
            (mContext as BaseActivity).hideProcessDailog()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handler = Handler()
        Log.d("classNAme", this.javaClass.name)
        baseViewModel = ViewModelProvider(requireActivity()).get(BaseActivityViewModel::class.java)
        apiBaseModel = ViewModelProvider(requireActivity()).get(BaseViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseViewModel.getTimeLv().observe(viewLifecycleOwner, Observer {
            //println("BaseFragment onViewCreated : ${it ?: 0}")
            millisStart = it ?: 0
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacksAndMessages(null)
    }

    open fun addTab(
        tab: TabLayout.Tab?,
        title: String,
        font: Int,
        textStyle: Int,
        indicatorVisibility: Int,
        count: Int
    ) {
        if (tab == null) {
            return
        }
        val customTabView: View =
            LayoutInflater.from(requireContext()).inflate(R.layout.layout_tab_1, null)

        customTabView.findViewById<View>(R.id.v_bottom_border).visibility = indicatorVisibility
        val tabTitle = customTabView.findViewById<TextView>(R.id.tv_tab_title)
        tabTitle.text = " " + title

        val tf = ResourcesCompat.getFont(requireContext(), font)
        tabTitle.setTypeface(tf, textStyle)

        tab.customView = customTabView
        tab.tag = title
        /*   val tvNotifyCount = customTabView.findViewById<TextView>(R.id.tv_notify_count)
             if (count <= 0) {
                 tvNotifyCount.visibility = GONE
             } else {
                 tvNotifyCount.visibility = VISIBLE
                 tvNotifyCount.text = if (count > 9) {
                     "9+"
                 } else {
                     "$count"
                 }
             }*/
        if (indicatorVisibility == VISIBLE) {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            //tvNotifyCount.visibility = GONE
        } else {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
            //tvNotifyCount.visibility = VISIBLE
        }
    }

    open fun addTab2(
        tab: TabLayout.Tab?,
        title: String,
        font: Int,
        textStyle: Int,
        indicatorVisibility: Int
    ) {
        if (tab == null) {
            return
        }
        val customTabView: View =
            LayoutInflater.from(requireContext()).inflate(R.layout.layout_tab_2, null)

        customTabView.findViewById<View>(R.id.v_bottom_border).visibility = indicatorVisibility
        val tabTitle = customTabView.findViewById<TextView>(R.id.tv_tab_title)
        tabTitle.text = title

        val tf = ResourcesCompat.getFont(requireContext(), font)
        tabTitle.setTypeface(tf, textStyle)

        tab.customView = customTabView
        tab.tag = title

        if (indicatorVisibility == VISIBLE) {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        } else {
            tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
        }
    }

    fun changeTabAppearance(
        p0: TabLayout.Tab?,
        visible: Int,
        count: Int
    ) {
        if (p0?.customView == null) {
            return
        }
        val tabTitle = p0.customView!!.findViewById<AppCompatTextView>(R.id.tv_tab_title)
        p0.customView!!.findViewById<View>(R.id.v_bottom_border).visibility = visible
        /*  val tvNotifyCount = p0.customView!!.findViewById<TextView>(R.id.tv_notify_count)
          if (count > 0) {
              tvNotifyCount.visibility = VISIBLE
              tvNotifyCount.text = if (count > 9) {
                  "9+"
              } else {
                  "$count"
              }
          } else {
              tvNotifyCount.visibility = View.GONE
          }*/
        when (visible) {
            VISIBLE -> {
                val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans_bold)
                tabTitle.setTypeface(tf, Typeface.NORMAL)
                tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                //tvNotifyCount.visibility = GONE
            }
            GONE -> {
                val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans)
                tabTitle.setTypeface(tf, Typeface.NORMAL)
                tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
                //tvNotifyCount.visibility = VISIBLE
            }
        }
    }

    fun changeTabAppearance2(p0: TabLayout.Tab?, visible: Int) {
        if (p0?.customView == null) {
            return
        }
        val tabTitle = p0.customView!!.findViewById<AppCompatTextView>(R.id.tv_tab_title)
        p0.customView!!.findViewById<View>(R.id.v_bottom_border).visibility = visible
        when (visible) {
            VISIBLE -> {
                val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans_bold)
                tabTitle.setTypeface(tf, Typeface.NORMAL)
                tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            }
            INVISIBLE, GONE -> {
                val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans)
                tabTitle.setTypeface(tf, Typeface.NORMAL)
                tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
            }
        }
    }

    fun changeTabAppearanceNew(p0: TabLayout.Tab?, visible: Int) {
        if (p0?.customView == null) {
            return
        }
        val tabTitle = p0.customView!!.findViewById<AppCompatTextView>(R.id.tv_tab_title)
        when (visible) {
            VISIBLE -> {
                val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans_bold)
                tabTitle.setTypeface(tf, Typeface.NORMAL)
                tabTitle.setBackgroundResource(R.drawable.bg_blue_shape)

                tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            }
            INVISIBLE, GONE -> {
                val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans)
                tabTitle.setTypeface(tf, Typeface.NORMAL)
                tabTitle.setBackgroundResource(0)

                tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
            }
        }
    }
    fun changeTabAppearanceForProfile(p0: TabLayout.Tab?, visible: Int) {
        if (p0?.customView == null) {
            return
        }
        val tabTitle = p0.customView!!.findViewById<AppCompatTextView>(R.id.tv_tab_title)
        when (visible) {
            VISIBLE -> {
                val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans_bold)
                tabTitle.setTypeface(tf, Typeface.NORMAL)
                tabTitle.setBackgroundResource(R.drawable.bg_dark_gray_shape)

                tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            }
            INVISIBLE, GONE -> {
                val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans)
                tabTitle.setTypeface(tf, Typeface.NORMAL)
                tabTitle.setBackgroundResource(0)

                tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
            }
        }
    }

    fun changeTabAppearance3(p0: TabLayout.Tab?, visible: Int) {
        if (p0?.customView == null) {
            return
        }
        val tabTitle = p0.customView!!.findViewById<AppCompatTextView>(R.id.tv_tab_title)
        p0.customView!!.findViewById<View>(R.id.v_bottom_border).visibility = visible
        val tf = ResourcesCompat.getFont(requireContext(), R.font.pt_sans)
        tabTitle.setTypeface(tf, Typeface.NORMAL)
        tabTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        when (visible) {
            VISIBLE -> {
                tabTitle.alpha = 1f
            }
            INVISIBLE, GONE -> {
                tabTitle.alpha = 0.4f
            }
        }
    }

    fun wrapTabIndicatorToTitle(
        tabLayout: TabLayout,
        externalMargin: Int,
        internalMargin: Int
    ) {
        val tabStrip = tabLayout.getChildAt(0)
        if (tabStrip is ViewGroup) {
            val childCount = tabStrip.childCount
            for (i in 0 until childCount) {
                val tabView = tabStrip.getChildAt(i)
                //set minimum width to 0 for instead for small texts, indicator is not wrapped as expected
                tabView.minimumWidth = 0
                // set padding to 0 for wrapping indicator as title
                tabView.setPadding(0, tabView.paddingTop, 0, tabView.paddingBottom)
                // setting custom margin between tabs
                if (tabView.layoutParams is ViewGroup.MarginLayoutParams) {
                    val layoutParams =
                        tabView.layoutParams as ViewGroup.MarginLayoutParams
                    if (i == 0) { // left
                        settingMargin(layoutParams, externalMargin, internalMargin)
                    } else if (i == childCount - 1) { // right
                        settingMargin(layoutParams, internalMargin, externalMargin)
                    } else { // internaFl
                        settingMargin(layoutParams, internalMargin, internalMargin)
                    }
                }
            }
            tabLayout.requestLayout()
        }
    }

    fun settingMargin(
        layoutParams: ViewGroup.MarginLayoutParams,
        start: Int,
        end: Int
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            layoutParams.marginStart = start
            layoutParams.marginEnd = end
            layoutParams.leftMargin = start
            layoutParams.rightMargin = end
        } else {
            layoutParams.leftMargin = start
            layoutParams.rightMargin = end
        }
    }

    fun setFragment(fragment: Fragment, id: Int, isAddToBackStack: Boolean) {
        val backStateName: String = fragment::class.java.name
        val fm = requireActivity().supportFragmentManager
        val ft = fm.beginTransaction()
        ft.replace(id, fragment)
        if (isAddToBackStack) {
            ft.addToBackStack(backStateName)
        }
        ft.commit()
    }

    fun setFragment(fragment: Fragment, enterAnim: Int, exitAnim: Int, tag: String) {
        val fm = requireActivity().supportFragmentManager
        val ft = fm.beginTransaction()
        ft.setCustomAnimations(enterAnim, exitAnim)
        ft.replace(R.id.containerFragment, fragment)
        ft.addToBackStack(tag)
        ft.commit()
    }

    fun setFragment(fragment: Fragment, tag: String) {
        val fm = requireActivity().supportFragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.containerFragment, fragment)
        ft.addToBackStack(tag)
        ft.commit()
    }

    fun showToast(
        context: Context?,
        msg: String,
        type: CustomToast.ToastType,
        duration: Int = Toast.LENGTH_SHORT
    ) {
        if (msg.isNotEmpty()) {
            CustomToast.showToast(
                context, msg, type, duration
            )
        }
    }

    fun hideKeyboard() {
        if (requireActivity()?.currentFocus == null) {
            requireActivity()?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            return
        }
        val inputMethodManager = requireActivity().getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity()?.currentFocus!!.windowToken, 0
        )
    }


    fun Window.getSoftInputMode(): Int {
        return attributes.softInputMode
    }

    open fun onBackPressed(): Boolean {
        return false
    }

    open fun isOnline(): Boolean {
        val cm =
            requireContext()?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }


    fun loginNotifyCheck() {
        if (CommonUtils.isLogin(requireActivity())) {
            navigate.onClickNotification()
        } else {
            val intent = Intent(requireActivity(), LoginActivity::class.java)
            intent.putExtra(AppConstant.ISFIRSTTIME, false)
            this@BaseFragment.startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE_NOTIFY)
        }
    }

    fun loginUploadCheck() {
        if (CommonUtils.isLogin(requireContext())) {
            navigate.uploadRecord()
        } else {
            val intent = Intent(requireContext(), LoginActivity::class.java)
            intent.putExtra(AppConstant.ISFIRSTTIME, false)
            this@BaseFragment.startActivityForResult(intent, RequestCode.LOGIN_REQUEST_CODE_UPLOAD)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RequestCode.LOGIN_REQUEST_CODE_NOTIFY -> {
                if (resultCode == Activity.RESULT_OK) {
                    baseViewModel.refresh(true)
                    navigate.onClickNotification()
                }
            }

            RequestCode.LOGIN_REQUEST_CODE_UPLOAD -> {
                if (resultCode == Activity.RESULT_OK) {
                    baseViewModel.refresh(true)
                    navigate.uploadRecord()
                }
            }
        }
    }

    open fun getRecordDataDetail(recordId: String) {

    }

    open fun refreshHomeScreen() {

    }
}