package com.nibula.base
/*
import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics
import com.nibula.R
import com.walletconnect.android.Core
import com.walletconnect.android.CoreClient
import com.walletconnect.android.relay.ConnectionType
import com.walletconnect.sign.client.Sign
import com.walletconnect.sign.client.SignClient
import timber.log.Timber

class BaseApplication : Application() {

    companion object {
        lateinit var firebaseAnalytics: FirebaseAnalytics
        private lateinit var app: BaseApplication
        fun appInstance() = app
    }

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    override fun onCreate() {
        super.onCreate()
        app = this
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        */
/* // Required initialization logic here!
        val projectId = "e33c836025414510b9efa4b11224ab85" //Get Project ID at https://cloud.walletconnect.com/
        val relayUrl = "relay.walletconnect.com"
        val serverUrl = "wss://$relayUrl?projectId=$projectId"
        val connectionType = ConnectionType.AUTOMATIC // or ConnectionType.MANUAL

        val appMetaData = Core.Model.AppMetaData(
            name = "Plurality",
            description = "Kotlin App",
            url = "Plurality url",
            icons = listOf("https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png?alt=media"),
            redirect = getString(R.string.deep_link_url)  // Custom Redirect URI
        )

        CoreClient.initialize(
            relayServerUrl = serverUrl,
            connectionType = connectionType,
            application = this,
            metaData = appMetaData
        ) { error ->
            Timber.tag("CoreClient_Initialization_Error").e(error.throwable.stackTraceToString())
        }

        val init = Sign.Params.Init(core = CoreClient)
        SignClient.initialize(init) { error ->
            Timber.tag("SignClient_Initialization_Error").e(error.throwable.stackTraceToString())
        }
    }*//*
    }
}
*/
import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics
import com.nibula.R
import com.walletconnect.android.Core
import com.walletconnect.android.CoreClient
import com.walletconnect.android.relay.ConnectionType
import com.walletconnect.sign.client.Sign
import com.walletconnect.sign.client.SignClient
import timber.log.Timber

class BaseApplication : Application() {
    companion object {
        lateinit var firebaseAnalytics: FirebaseAnalytics
        private lateinit var app: BaseApplication
        fun appInstance() = app
    }
    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    override fun onCreate() {
        super.onCreate()
        app = this
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        // Required initialization logic here!
        val projectId = "e33c836025414510b9efa4b11224ab85" //Get Project ID at https://cloud.walletconnect.com/
        val relayUrl = "relay.walletconnect.com"
        val serverUrl = "wss://$relayUrl?projectId=$projectId"
        val connectionType = ConnectionType.AUTOMATIC // or ConnectionType.MANUAL

        val appMetaData = Core.Model.AppMetaData(
            name = "Nebula",
            description = "Nebula App",
            url = "Nebula url",
            icons = listOf("https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png?alt=media"),
            redirect = getString(R.string.deep_link_url)  // Custom Redirect URI
        )

        CoreClient.initialize(
            relayServerUrl = serverUrl,
            connectionType = connectionType,
            application = this,
            metaData = appMetaData
        ) {
                error -> Timber.e(error.throwable.stackTraceToString())
          }


        val init = Sign.Params.Init(core = CoreClient)
        SignClient.initialize(init) { error ->
            Timber.e(error.throwable.stackTraceToString())
        }
    }

}


