package com.nibula.base

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.google.gson.GsonBuilder
import com.nibula.R
import com.nibula.customview.CustomToast
import com.nibula.dashboard.DashboardActivity
import com.nibula.guide.WalkThroughActivity
import com.nibula.response.AppVersionResponse
import com.nibula.response.walkthrought.WalkThroughResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : BaseActivity() {
    val activityScope = CoroutineScope(Dispatchers.Main)
//    val executor = ContextCompat.getMainExecutor(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        setContentView(R.layout.activity_splash)
        startNavigation()

    }

    private fun getWalkThroughTagLines() {
        val updatedResponse = "[\n" +
                "    {\n" +
                "      \"Id\": 1,\n" +
                "      \"Title\": \"PRE-RELEASES\\\\nFOR MUSIC INVESTORS\",\n" +
                "      \"Description\": \"Discover hit songs before they take off.\\\\nInvest by buying music and receive\\\\nroyalties.\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Id\": 2,\n" +
                "      \"Title\": \"CASH BACKS FOR SONGS IN\\\\nTHE MAKING\",\n" +
                "      \"Description\": \"When music is created, a story is born.\\\\nShare your co-owned songs and earn cash back.\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Id\": 3,\n" +
                "      \"Title\": \" CREATE THE\\\\nSOUNDTRACK TO YOUR LIFE\",\n" +
                "      \"Description\": \"Pick your rhythm. Choose your mood.\\\\nMusic is valuable, so are you.\\\\nStart investing in music today.\"\n" +
                "    }\n" +
                "  ]"

        val updatedResponsev2 = "[\n" +
                "    {\n" +
                "      \"Id\": 1,\n" +
                "      \"Title\": \"\",\n" +
                "      \"Description\":\"Purchase music royalities from\\\\ntalented artists, and earn.\\\\nroyality payouts.\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Id\": 2,\n" +
                "      \"Title\": \"\",\n" +
                "      \"Description\":\"Artists and supporters can\\\\nshare in the cultural and\\\\nfinancial profits of music.\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Id\": 3,\n" +
                "      \"Title\": \"\",\n" +
                "      \"Description\": \"Co-Own Music\"\n" +
                "    }\n" +
                "  ]"


// Title and description changed according to the new sketch (NEBULA NEW DESIGN 17 DEC 2021 Deepak)
        val updatedResponsev3 = "[\n" +
                "    {\n" +
                "      \"Id\": 1,\n" +
                "      \"Title\": \"PURCHASE ROYALTIES\\\\nCO-OWN MUSIC\",\n" +
                "      \"Description\":\"Purchase royalty tokens of music you\\\\nlove and earn royalty payouts.\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Id\": 2,\n" +
                "      \"Title\": \"ARTISTS & FANS\\\\n EARN TOGETHER\",\n" +
                "      \"Description\":\"Earn money every time the record\\\\nairs on the radio, performs on\\\\nstage, or streams online.\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Id\": 3,\n" +
                "      \"Title\": \"SHARE IN THE SUCCESS\",\n" +
                "      \"Description\": \"we support artists by giving 20% \\\\ncommission to all those supporting \\\\nyour sales.\"\n" +
                "    }\n" +
                "  ]"
        PrefUtils.saveValueInPreference(
            this@SplashActivity,
            PrefUtils.WALK_THROUGH_REPONSE,
            updatedResponsev3
        )

        if (!isOnline()) {
            showToast(
                this@SplashActivity,
                getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )
            return
        }
        val helper = ApiClient.getClientMusic(this@SplashActivity).create(ApiAuthHelper::class.java)
        val call = helper.getWalkThroughtTagLines()
        call.enqueue(object : CallBackManager<WalkThroughResponse>() {
            override fun onSuccess(any: WalkThroughResponse?, message: String) {
                val response = any as WalkThroughResponse
                val intent =
                    Intent(this@SplashActivity, WalkThroughActivity::class.java)
                startActivity(intent)
                finish()
                when (response.responseStatus ?: 0) {
                    1 -> {
                        val updatedResponse =
                            GsonBuilder().disableHtmlEscaping().create().toJson(
                                response.responseCollection
                            )
                        PrefUtils.saveValueInPreference(
                            this@SplashActivity,
                            PrefUtils.WALK_THROUGH_REPONSE,
                            updatedResponse
                        )
                        val intent =
                            Intent(this@SplashActivity, WalkThroughActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    0 -> {
                        response.responseMessage?.let {
                            showToast(
                                this@SplashActivity,
                                it,
                                CustomToast.ToastType.FAILED
                            )
                        }
                    }
                    else -> {

                    }
                }
            }

            override fun onFailure(message: String) {
                showAlert("", message)
//                showToast(this@SplashActivity,message,CustomToast.ToastType.FAILED)
            }

            override fun onError(error: RetroError) {
                showAlert("", error.errorMessage)
//                showToast(this@SplashActivity,error.errorMessage,CustomToast.ToastType.FAILED)
            }
        })
    }

    private fun startNavigation() {
        activityScope.launch {
            if (isOnline()) {
                delay(2000)
                getForceUpdateStatus()
            } else {
                showAlert(
                    "Internet connection",
                    "No internet connection detected on the device. Please check the connection and try again."
                )
            }
        }
    }

    private fun getForceUpdateStatus() {
        val helper =
            ApiClient.getClientMusic().create(ApiAuthHelper::class.java)
        val call = helper.getAppVersion()
        call.enqueue(object : CallBackManager<AppVersionResponse>() {
            override fun onSuccess(any: AppVersionResponse?, message: String) {
                PrefUtils.saveValueInPreference(
                    this@SplashActivity,
                    PrefUtils.APP_VERSION,
                    any?.androidVersion ?: "0.0"
                )
                PrefUtils.saveValueInPreference(
                    this@SplashActivity,
                    PrefUtils.FORCE_UPDATE,
                    any?.forceUpdate ?: false
                )
                checkWalkThrough()
            }

            override fun onFailure(message: String) {
                checkWalkThrough()
            }

            override fun onError(error: RetroError) {
                checkWalkThrough()
            }

        })
    }

    private fun checkWalkThrough() {
        if (PrefUtils.getValueFromPreference(
                this@SplashActivity,
                PrefUtils.IS_GUIDE_PAGE_ENABLE
            ).isEmpty()) {
            getWalkThroughTagLines()
        } else {
            if (CommonUtils.isLogin(this@SplashActivity)) {
//              checkBioMatric()
                val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
                startActivity(intent)
                finish()
                return
            }
//
            val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
            startActivity(intent)
            finish()
        }


    }

    private fun showAlert(titles: String, descriptions: String) {
        val builder = AlertDialog.Builder(this@SplashActivity)
        builder.setTitle(titles)
        builder.setMessage(descriptions)
        builder.setCancelable(false)
        builder.setPositiveButton(
            "Retry"
        ) { dialog, _ ->
            if (!isFinishing) {
                startNavigation()
                dialog.dismiss()
            }
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, _ ->
            dialog.cancel()
            finish()
        }
        val alert = builder.create()
        try {
            alert.show()
        } catch (e: Exception) {
        }

    }

    /*   private fun checkBioMatric() {
           val biometricManager = BiometricManager.from(this)
           when (biometricManager.canAuthenticate()) {
               BiometricManager.BIOMETRIC_SUCCESS ->
                   authUser(ContextCompat.getMainExecutor(this))
               BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                   Toast.makeText(
                       this,
                       "getString(R.string.error_msg_no_biometric_hardware)",
                       Toast.LENGTH_LONG
                   ).show()
               BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                   val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
                   startActivity(intent)
                   finish()
               }
               BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                   val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
                   startActivity(intent)
                   finish()
               }

           }
       }

       private fun authUser(executor: Executor) {
           val promptInfo = BiometricPrompt.PromptInfo.Builder()
               // 2
               .setTitle("Enter phone lock patter, PIN, password or fingerprint")
               // 3
               .setSubtitle(getString(R.string.title_Assets))
               // 4
               .setDescription("Unlock Nebula")
               // 5
               .setDeviceCredentialAllowed(true)
               // 6
               .build()
           val biometricPrompt = BiometricPrompt(this, executor,
               object : BiometricPrompt.AuthenticationCallback() {
                   // 2
                   override fun onAuthenticationSucceeded(
                       result: BiometricPrompt.AuthenticationResult
                   ) {
                       super.onAuthenticationSucceeded(result)
                       val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
                       startActivity(intent)
                       finish()
                   }

                   // 3
                   override fun onAuthenticationError(
                       errorCode: Int, errString: CharSequence
                   ) {
                       super.onAuthenticationError(errorCode, errString)
                       Toast.makeText(
                           applicationContext,
                           " getString(R.string.error_msg_auth_error, errString)",
                           Toast.LENGTH_SHORT
                       ).show()
                   }

                   // 4
                   override fun onAuthenticationFailed() {
                       super.onAuthenticationFailed()
                       Toast.makeText(
                           applicationContext,
                           " getString(R.string.error_msg_auth_failed)",
                           Toast.LENGTH_SHORT
                       ).show()

                   }
               })
           biometricPrompt.authenticate(promptInfo)
       }*/


}
