package com.nibula.base

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

open class BaseHelperFragment {

    val disposables = CompositeDisposable()

    fun <T> addToQueue(observable: Observable<T>): Observable<T>? {
        return observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun <T> sendApiRequest(observable: Observable<T>): Observable<T>? {
        return addToQueue(observable = observable)

    }
    open fun onDestroy() {
        if (!disposables.isDisposed) {
            disposables.clear()
        }
    }
}