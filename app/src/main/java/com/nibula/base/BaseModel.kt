package com.nibula.base

import com.nibula.retrofit.RetroError

data class BaseModel<T>(
    var t: T?,
    var isError: Boolean,
    var error: RetroError?
)




