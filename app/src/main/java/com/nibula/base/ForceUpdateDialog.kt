package com.nibula.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nibula.R
import com.nibula.utils.PrefUtils

class ForceUpdateDialog : BaseDialogFragment() {

    lateinit var mContext: Context

      interface ForceUpdateListener {
        fun onUpdateClick()
        fun closeApp()
        }

    companion object {
        private lateinit var listener: ForceUpdateListener
        fun getInstance(listener: ForceUpdateListener): ForceUpdateDialog {
            this.listener = listener
            return ForceUpdateDialog()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.dialog_force_update, container, false)
        rootView.findViewById<TextView>(R.id.update).setOnClickListener {
            listener?.onUpdateClick()
            dismiss()
        }

        rootView.findViewById<TextView>(R.id.cancel).setOnClickListener {
            if (PrefUtils.getBooleanValue(requireContext(), PrefUtils.FORCE_UPDATE)) {
                listener?.closeApp()
            }
            dismiss()
        }

        return rootView
    }
}