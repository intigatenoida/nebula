package com.nibula.base

import com.nibula.R
import com.nibula.customview.CustomToast
import com.nibula.request.adminapproval.AdminApprovalRequest
import com.nibula.response.externalShareResponse.ExternalShareResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import com.nibula.utils.CommonUtils
import io.reactivex.disposables.CompositeDisposable

class SharingHelper :BaseHelperFragment() {
    fun getExternalShareUrl(recordId: String,baseActivity: BaseActivity) {
        if (!baseActivity.isOnline()) {
            baseActivity. showToast(
                baseActivity,
                baseActivity. getString(R.string.internet_connectivity),
                CustomToast.ToastType.NETWORK
            )

            return
        }

        baseActivity. showProcessDialog()
        val disposables = CompositeDisposable()
        val request = AdminApprovalRequest(recordId)
        val helper =
            ApiClient.getClientMusic(baseActivity).create(ApiAuthHelper::class.java)
        val observable = helper.getExternalShareUrl(request)

        disposables.add(addToQueue(observable)!!.subscribeWith(object :
            CallbackWrapper<ExternalShareResponse>() {

            override fun onSuccess(any: ExternalShareResponse?, message: String?) {
                baseActivity.hideProcessDailog()
                if (any?.responseStatus == 1 && any.response.url.isEmpty().not()) {
                    CommonUtils.sharePlainText(baseActivity, any.response.url)
                }
            }

            override fun onError(error: RetroError?) {
                baseActivity.hideProcessDailog()
            }

        }))

    }
}