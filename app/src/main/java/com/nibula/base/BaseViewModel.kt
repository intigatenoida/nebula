package com.nibula.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nibula.retrofit.CallbackWrapper
import com.nibula.retrofit.RetroError
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
open class BaseViewModel : ViewModel() {
    fun <T> getCallBacks(observer: Observable<T>?, disposable: CompositeDisposable): MutableLiveData<BaseModel<T>> {
        val apiCallbacks: MutableLiveData<BaseModel<T>> = MutableLiveData()
        if (observer == null) {
            return apiCallbacks
        }
        val callbacks = object : CallbackWrapper<T>() {
            override fun onSuccess(any: T?, message: String?) {
                apiCallbacks.value = BaseModel<T>(any, false, RetroError(RetroError.Kind.NONE, "", -999))
            }

            override fun onError(error: RetroError?) {
                apiCallbacks.value = BaseModel<T>(null, true, error)
            }
        }
        disposable.add(addToQueue(observer)!!.subscribeWith(callbacks))
        return apiCallbacks
    }
    fun <T> addToQueue(observable: Observable<T>): Observable<T>? {
        return observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}