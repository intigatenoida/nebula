package com.nibula.base

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.nibula.R
import com.nibula.databinding.NewVerifyOtpDialogBinding
import com.nibula.request.EmailValidationRequest
import com.nibula.response.BaseResponse
import com.nibula.retrofit.ApiAuthHelper
import com.nibula.retrofit.ApiClient
import com.nibula.retrofit.CallBackManager
import com.nibula.retrofit.RetroError

class OtpVerificationDialog : BaseDialogFragment() {

    lateinit var mContext: Context
    var countDownTimer: CountDownTimer? = null
    val totalTime: Long = 30 * 1000
    val timeInterval: Long = 1000
    lateinit var rootView: View
    private lateinit var newVeriftOtpDialogBinding: NewVerifyOtpDialogBinding

    interface OtpVerificationCodeListener {
        fun onVerify(otpValue: String, errorTextView: TextView)
    }

    companion object {
        private lateinit var listener: OtpVerificationCodeListener
        lateinit var countryCode: String
        lateinit var phoneNumber: String
        lateinit var userId: String
        fun getInstance(
            listener: OtpVerificationCodeListener,
            countryCode: String,
            phoneNumber: String,
            userId: String
        ): OtpVerificationDialog {
            this.listener = listener
            this.countryCode = countryCode
            this.phoneNumber = phoneNumber
            this.userId = userId
            return OtpVerificationDialog()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        newVeriftOtpDialogBinding = NewVerifyOtpDialogBinding.inflate(inflater, container, false)
        rootView = newVeriftOtpDialogBinding.root
        //rootView = inflater.inflate(R.layout.new_verify_otp_dialog, container, false)
        dialog?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        newVeriftOtpDialogBinding.edtOTPEmail.setOtpCompletionListener {

            if (it.length == 4) {
                newVeriftOtpDialogBinding.btnVerify.isEnabled
                listener?.onVerify(newVeriftOtpDialogBinding.edtOTPEmail.text.toString(), newVeriftOtpDialogBinding.txtErrorEmail)

            }
        }

        val ss1 = SpannableString(getString(R.string.resend))
        ss1.setSpan(
            UnderlineSpan(), 0, ss1.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        newVeriftOtpDialogBinding.resendOtp.text = ss1

        newVeriftOtpDialogBinding.txtEmailPhone.text = "To " + "${countryCode}-${phoneNumber}"
        newVeriftOtpDialogBinding.btnVerify.setOnClickListener {
            listener?.onVerify(newVeriftOtpDialogBinding.edtOTPEmail.text.toString(), newVeriftOtpDialogBinding.txtErrorEmail)
        }

        newVeriftOtpDialogBinding.resendOtp.isClickable = false
        startCountDownTimer()

        newVeriftOtpDialogBinding.resendOtp.setOnClickListener {
            resendOTPCreateAccount()
        }

        rootView.findViewById<ImageView>(R.id.imgClose).setOnClickListener {
            dismiss()
        }

        return rootView
    }

    fun startCountDownTimer() {
        newVeriftOtpDialogBinding.tvTimer.visibility = View.VISIBLE
        newVeriftOtpDialogBinding.resendOtp.visibility = View.VISIBLE
        countDownTimer?.cancel()
        countDownTimer = object : CountDownTimer(totalTime, timeInterval) {
            override fun onFinish() {
                newVeriftOtpDialogBinding.resendOtp.isClickable = true
                newVeriftOtpDialogBinding.tvTimer.visibility = View.INVISIBLE
            }

            override fun onTick(millisUntilFinished: Long) {
                val minute = (millisUntilFinished / 1000) / 60
                val seconds = (millisUntilFinished / 1000) % 60
                if (seconds < 10) {
                    newVeriftOtpDialogBinding.tvTimer.text = "$minute:0$seconds"

                } else {
                    newVeriftOtpDialogBinding.tvTimer.text = "$minute:$seconds"

                }
                newVeriftOtpDialogBinding.resendOtp.isClickable = false

            }

        }.start()
    }

    private fun resendOTPCreateAccount() {
        val helper = ApiClient.getClientAuth(requireContext()).create(ApiAuthHelper::class.java)
        val request = EmailValidationRequest()
        request.otp = ""
        val call = helper.resendMobileVerificationOtp(userId)
        call.enqueue(object : CallBackManager<BaseResponse>() {
            override fun onSuccess(any: BaseResponse?, message: String) {
                val response = any as BaseResponse
                if (response.responseStatus == 1) {
                    response.responseMessage?.let {

                        startCountDownTimer()
                        newVeriftOtpDialogBinding.txtErrorEmail.visibility = View.VISIBLE
                        newVeriftOtpDialogBinding.tvTimer.visibility = View.VISIBLE
                        newVeriftOtpDialogBinding.txtErrorEmail.setTextColor(
                            ContextCompat.getColor(
                                requireContext(), R.color.green_shade_1_100per
                            )
                        )
                        newVeriftOtpDialogBinding.txtErrorEmail.text = response.responseMessage


                    }
//                    val intent = Intent(this@OTPActivity, GenerateNewPassword::class.java)
//                    intent.putExtra(AppConstant.EMAIL, request.emailId)
//                    intent.putExtra(AppConstant.ID, response.token)
//                    startActivityForResult(intent, 1000)
                } else {
                    newVeriftOtpDialogBinding.txtErrorEmail.visibility = View.VISIBLE
                    newVeriftOtpDialogBinding.txtErrorEmail.text = response.responseMessage
                }
            }

            override fun onFailure(message: String) {
                newVeriftOtpDialogBinding.txtErrorEmail.visibility = View.VISIBLE
                newVeriftOtpDialogBinding.txtErrorEmail.text = message
            }

            override fun onError(error: RetroError) {
            }

        })
    }

}