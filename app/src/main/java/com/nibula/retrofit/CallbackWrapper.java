package com.nibula.retrofit;

import android.util.Log;


import java.net.UnknownHostException;

import io.reactivex.observers.DisposableObserver;


public abstract class CallbackWrapper<T> extends DisposableObserver<T> {

    protected abstract void onSuccess(T any, String message);

    protected abstract void onError(RetroError error);

    @Override
    public void onNext(final T t) {

        onSuccess(t, "");
    }

    @Override
    public void onError(Throwable e) {
        Log.d("Okhttp", e.getLocalizedMessage());
        if (e.getMessage().equals("HTTP 401 Unauthorized")) {
            onError(new RetroError(RetroError.Kind.HTTP, "HTTP 401 Unauthorized", -999));

        } else if (e instanceof UnknownHostException) {
            onError(new RetroError(RetroError.Kind.NETWORK, "Unable to connect to server.", -999));
        } else {
            onError(
                    new RetroError(
                            RetroError.Kind.UNEXPECTED,
                            "Unable to connect to server.\nTry after sometime.",
                            -999
                    )
            );
        }
    }

    @Override
    public void onComplete() {

    }


}