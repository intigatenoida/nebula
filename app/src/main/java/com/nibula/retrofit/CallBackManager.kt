package com.nibula.retrofit;

import android.os.Bundle
import android.util.Log
import com.nibula.base.BaseApplication
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException

abstract class CallBackManager<T> : Callback<T> {
    override fun onFailure(call: Call<T>, t: Throwable) {
        Log.d("onFailure", call.request().toString() + "   " + t.getLocalizedMessage())
        if (call.isCanceled) {
            val firebaseAnalytics = BaseApplication.firebaseAnalytics
            val bundle = Bundle()
            bundle.putString("api_request", call.request().toString())
            bundle.putString("message", t.getLocalizedMessage())
            firebaseAnalytics.logEvent("Android_Api_Failure", bundle)
            onError(
                RetroError(
                    RetroError.Kind.API_CALL_CANCEL,
                    "Unable to connect to server.",
                    -999
                )
            )
        } else {

            if (t.message == "HTTP 401 Unauthorized") {
                onError(RetroError(RetroError.Kind.HTTP, "HTTP 401 Unauthorized", -999))
            } else if (t is UnknownHostException) {
                val firebaseAnalytics = BaseApplication.firebaseAnalytics
                val bundle = Bundle()
                bundle.putString("api_request", call.request().toString())
                bundle.putString("message", "Unknown Host Exception")
                firebaseAnalytics.logEvent("Android_Api_Failure", bundle)
                onError(RetroError(RetroError.Kind.NETWORK, "Unable to connect to server.", -999))
            } else {

                val firebaseAnalytics = BaseApplication.firebaseAnalytics
                val bundle = Bundle()
                bundle.putString("api_request", call.request().toString())
                bundle.putString(
                    "message", "Unable to connect to server.\n" +
                            "Try after sometime."
                )
                firebaseAnalytics.logEvent("Android_Api_Failure", bundle)

                onError(
                    RetroError(
                        RetroError.Kind.UNEXPECTED,
                        "Unable to connect to server.\nTry after sometime.",
                        -999
                    )
                )
            }
        }
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            onSuccess(response.body(), "")
        } else {
            val temp = response.errorBody()?.string()
            try {
                val arrayJson = JSONArray(temp)
                val obj: JSONObject = arrayJson[0] as JSONObject
                if (obj.has("Description")) {
                    onFailure(obj.get("Description").toString())
                } else if (obj.has("description")) {
                    onFailure(obj.get("description").toString())
                } else {
                    onFailure(response.message())
                }
            } catch (e: Exception) {
                onFailure(response.message())
            }
        }
    }

    abstract fun onSuccess(any: T?, message: String)
    abstract fun onFailure(message: String)
    abstract fun onError(error: RetroError)
}