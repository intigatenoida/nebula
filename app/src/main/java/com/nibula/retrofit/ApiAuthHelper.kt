package com.nibula.retrofit

import com.nibula.costbreakdown.modal.CostBreakdownResponse
import com.nibula.fcm.UpdateDeviceInfoReq
import com.nibula.finalsubmitrecord.ui.modal.FinalUploadScreenResponse
import com.nibula.investorcertificates.modals.InvestorCertificateDetailResponse
import com.nibula.investorcertificates.modals.InvestorsCertificatesResponse
import com.nibula.investorcertificates.modals.RecordIdRequest
import com.nibula.request.*
import com.nibula.request.adminapproval.AdminApprovalRequest
import com.nibula.request.mp3request.mp3UrlRequest
import com.nibula.request.myplaylist.MyLikedSongRequest
import com.nibula.request.myplaylist.MyPlayListRequest
import com.nibula.request.recordshare.RequestToInvestRequest
import com.nibula.request.upload.SubmitRecordRequest
import com.nibula.request_to_invest.modal.*
import com.nibula.request_to_invest.modal.recomded_by.RecomendedByRes
import com.nibula.response.*
import com.nibula.response.HistoryResponse.HistoryResponse
import com.nibula.response.HowItWork.HowItWorkResponse
import com.nibula.response.artists.ArtistListResponse
import com.nibula.response.balance.MyBalanceResponse
import com.nibula.response.bankDetailListResponse.BankDetailListResponse
import com.nibula.response.bankList.BankListResponse
import com.nibula.response.carificationResponse.PhoneEmailVerificationResponse
import com.nibula.response.chatHistoryResponse.ChatHistoryResponse
import com.nibula.response.chatRecordResponse.ChatRecordResponse
import com.nibula.response.cryptowalletresponse.SaveCryptoWalletResponse
import com.nibula.response.defaultListResponse.DefaultUserResponse
import com.nibula.response.details.AlbumDetailResponse
import com.nibula.response.details.RecordDetailResponse
import com.nibula.response.details.investments.InvestorList
import com.nibula.response.externalShareResponse.ExternalShareResponse
import com.nibula.response.follow.FollowResponse
import com.nibula.response.followerResponse.FollowerResponse
import com.nibula.response.globalsearchResponse.GlobalSearchResponse
import com.nibula.response.home.recentuploaded.HomeScreenCommonModel
import com.nibula.response.home.recentuploaded.RecentUploadedResponse
import com.nibula.response.investerResponse.InvesterResponse
import com.nibula.response.login.LoginResponse
import com.nibula.response.mp3Response.mp3Response
import com.nibula.response.music.offerings.OfferingResponses
import com.nibula.response.music.releases.ReleasesResponses
import com.nibula.response.musicprofile.ProfileMusicReponse
import com.nibula.response.mycollectorresponse.MyCollecrtorsResponse
import com.nibula.response.myinvestmentresponse.MyInvestmentResponse
import com.nibula.response.myinvestmentresponse.Myinvestments
import com.nibula.response.myplaysit.MyPlaylistResponse
import com.nibula.response.notifiaction.NotificationResponse
import com.nibula.response.recorddetail.GetInvestorStatusResponse
import com.nibula.response.recordshares.RequestToInvestResponse
import com.nibula.response.referalresponse.ReferalListResponse
import com.nibula.response.registerResponse.RegisterResponse
import com.nibula.response.saveddraft.SaveAsDraftNewDataResponse
import com.nibula.response.saveddraft.SavedDraftRecordResponse
import com.nibula.response.searchShareResponse.SearchShareResponse
import com.nibula.response.signup.SignupResponse
import com.nibula.response.topoffertopartist.FreaturedArtistlistResponse
import com.nibula.response.topoffertopartist.NewOfferTopArtistResponse
import com.nibula.response.topoffertopartist.NewTopArtistsResponse
import com.nibula.response.topoffertopartist.TopOfferTopArtistReponse
import com.nibula.response.uploadrecord.*
import com.nibula.response.userAccessCode.UserAccessCodeResponse
import com.nibula.response.walkthrought.WalkThroughResponse
import com.nibula.upload_music.ui.uploadrecord.collecting_society_res.CollectingSocietyResponse
import com.nibula.upload_music.ui.uploadrecord.currency_res.CurrencyRes
import com.nibula.user.profile.ProfileResponce
import com.nibula.user.profile.UserProfile
import com.nibula.user.sign_up.modal.CounytryModal
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiAuthHelper {

    //--> Walk Through
    @GET("api/music/walkthrough")
    fun getWalkThroughtTagLines(): Call<WalkThroughResponse>

    @POST("api/account/authentication")
    fun login(
        @Body request: login_request,
        @Header("Content-Type") contentType: String
    ): Call<LoginResponse>

    @POST("api/TrendingOffering")
    fun trendingOffering(
        @Body request: TrendingRequest,
        @Header("Content-Type") contentType: String
    ): Call<LoginResponse>

    /*@GET("/api/trends-offering-top-artists")
     fun trendingOfferingTopArtist(@Header("Content-Type") contentType: String): Observable<TopOfferTopArtistReponse>
    */
    //New
    @GET("/api/music/getTrendingList")
    fun trendingOfferingTopArtist(@Header("Content-Type") contentType: String): Observable<TopOfferTopArtistReponse>

    @GET("/api/music/TopArtists")
    fun getTopArtistsNew(@Header("Content-Type") contentType: String): Observable<NewTopArtistsResponse>

    @GET("api/music/getListForHomePage")
    fun getHomeScreenData(@Header("Content-Type") contentType: String): Observable<HomeScreenCommonModel>

    @POST("/api/music/getTrendingList")
    fun getTrendingOfferingListNew(
        @Body request: TrendingRequest,
        @Header("Content-Type") contentType: String
    ): Call<NewOfferTopArtistResponse>

    @POST("/api/music/TopArtists")
    fun getTopArtistsNew(
        @Body request: TrendingRequest,
        @Header("Content-Type") contentType: String
    ): Call<NewTopArtistsResponse>

    //https://devauth.nebu.la/api/Account/SaveUserCryptoWalletAddress
    @POST("/api/Account/SaveUserCryptoWalletAddress")
    fun saveUserCryptoWalletAddress(
        @Body request: SaveCryptoWalletAddress,
        @Header("Content-Type") contentType: String
    ): Call<SaveCryptoWalletResponse>


    @POST("/api/music/getFeaturedList")
    fun freaturedArtistList(
        @Body request: TrendingRequest,
        @Header("Content-Type") contentType: String
    ): Call<FreaturedArtistlistResponse>

    @POST("api/account/register")
    fun register(
        @Body request: RegisterRequest,
        @Header("Content-Type") contentType: String
    ): Call<RegisterResponse>

    @GET("/api/account/ResendMobileVerificationOtp")
    fun resendMobileVerificationOtp(
        @Query("UserId") userId: String
    ): Call<BaseResponse>

    //--> Recent Uploaded with pagination
    @GET("api/music/getNewOffering")
    fun recentUpload(
        @Query("currentPage") pageNo: Int,
        @Query("recordsPerPage") recordPerPage: Int,
        @Query("search") search: String
    ): Observable<RecentUploadedResponse>

    //https://gateway1.nebula.ch/api/music/getNewOffering?currentPage=1&recordsPerPage=12&Search=
    /*//--> Recent Uploaded
    @GET("api/music/RecentlyUploaded")
    fun recentUpload(): Call<RecentUploadedResponse>*/
    //--> Get Record Details
    @GET("api/record-detail-view")
    fun getRecordDetails(
        @Query("RecordId") albumid: String,
        @Query("ForceUpdateRecordStatus") forceUpdateRecordStatus: Boolean
    ): Observable<AlbumDetailResponse>

    //--> Get Record Details New Created By Lokesh 05 July 2022
    @GET("api/music/GetDetailsOfRecords")
    fun getRecordDetailNew(@Query("RecordId") albumid: String): Observable<RecordDetailResponse>

    //--> Get Record Details
    @GET("api/music/GetRecordsCostBreakdown")
    fun getCostBreakDown(
        @Query("RecordId") albumid: String,
        @Query("Quantity") shares: Int
    ): Observable<CostBreakdownResponse>

    @GET("/api/offerings")
    fun requestOfferings(
        @Query("key") sKey: String,
        @Query("currentPage") pageNo: Int,
        @Query("Search") search: String
    ): Call<OfferingResponses>

    @GET("/api/music/Released")
    fun requestReleases(
        @Query("currentPage") pageNo: Int,
        @Query("Search") search: String
    ): Call<ReleasesResponses>

    @POST("api/account/sendforgotpasswordotp")
    fun forgetPassword(
        @Body request: ForgetPasswordRequest,
        @Header("Content-Type") contentType: String
    ): Call<BaseResponse>

    @POST("api/searchnshare/UpdateNotificationIsRead")
    fun updateNotificationStatus(
        @Body request: UpdateNotificationStatus,
        @Header("Content-Type") contentType: String
    ): Call<BaseResponse>

    @POST("api/account/resendforgotpasswordotp")
    fun resendOtp(
        @Body request: ForgetPasswordRequest,
        @Header("Content-Type") contentType: String
    ): Call<BaseResponse>

    @POST("api/account/verifyforgotpasswordotp")
    fun validateOtp(
        @Body request: EmailValidationRequest,
        @Header("Content-Type") contentType: String
    ): Call<VerifyOtpResponse>

    @POST("api/account/CompleteRegistration")
    fun validateOtpCreateAccount(
        @Body request: VerifyOtpCreateAccount,
        @Header("Content-Type") contentType: String
    ): Call<SignupResponse>

    @POST("api/account/resetforgotpassword")
    fun resetPassword(
        @Body request: ResetPasswordRequest,
        @Header("Content-Type") contentType: String
    ): Call<BaseResponse>

    @GET("/api/music/Genre")
    fun requestGenere(): Observable<GenereResponse>

    @GET("api/music/getCurrencyType")
    fun getCurrencyType(): Observable<CurrencyRes>

    @GET("/api/music/ParentalAdvisoryType")
    fun requestParentalAdvisory(): Observable<GenereResponse>

//Not used ( Commented by @Shubham)
//    @POST("api/music/AddUpdateRecords")
//    @Multipart
//    fun requestRecordSavedDraft(
//        @Part("Id") id: RequestBody,
//        @Part("RecordTypeId") recordTypeId: RequestBody,
//        @Part("RecordTitle") recordTitle: RequestBody,
//        @Part("Description") description: RequestBody,
//        @Part("GenreTypeId") genreTypeId: RequestBody,
//        @Part("PAdvisoryTypeId") pAdvisoryTypeId: RequestBody,
//        @Part("Price") price: RequestBody,
//        @Part("Labels") labels: RequestBody,
//        @Part("PerformanceCopyrightNo") performanceCopyrightNo: RequestBody,
//        @Part("RecordingCopyrightNo") recordingCopyrightNo: RequestBody,
//        @Part("InvestmentAmountNeeded") investmentAmountNeeded: RequestBody,
//        @Part("PricePerRoyaltyShare") maxRoyaltyPerInvestor: RequestBody,
//        @Part("ShowInvestorsToPublic") showInvestorsToPublic: RequestBody,
//        @Part("ApproveEachInvestor") approveEachInvestor: RequestBody,
//        @Part("OfferingStartDate") offeringStartDate: RequestBody,
//        @Part("OfferingEndDate") offeringEndDate: RequestBody,
//        @Part("CurrencyTypeId") currencyTypeId: RequestBody,
//        @Part("PreviewDurationInSec") previewDurationInSec: RequestBody,
//        @Part("TimeZone") timeZone: RequestBody,
//        @Part image: MultipartBody.Part
//    ): Call<UploadRecordResponse>

    @POST("api/music/AddUpdateRecords")
    @Multipart
    fun requestRecordSavedDraft1(
        @Part("Id") id: RequestBody,
        @Part("RecordTypeId") recordTypeId: RequestBody,
        @Part("RecordTitle") recordTitle: RequestBody,
        @Part("MessageToCoOwners") messageToCoOwners: RequestBody,
        @Part("GenreTypeId") genreTypeId: RequestBody,
        @Part("CurrencyTypeId") currencyTypeId: RequestBody,
        @Part("CopyrightSocietyWork") copyrightSocietyWork: RequestBody,
        @Part("CopyrightSocietyRecording") copyrightSocietyRecording: RequestBody,
        @Part("OneOffPayment4MusicStreamAccess") oneOffPayment4MusicStreamAccess: RequestBody,
        @Part("ISWCCodeWork") iSWCCodeWork: RequestBody,
        @Part("ISCRCodeRecording") iSCRCodeRecording: RequestBody,
        @Part("PricePerRoyaltyShareWork") pricePerRoyaltyShareWork: RequestBody,
        @Part("PricePerRoyaltyShareRecording") pricePerRoyaltyShareRecording: RequestBody,
        @Part("NumberOfShares") numberOfShares: RequestBody,
        @Part("ShowInvestorsToPublic") showInvestorsToPublic: RequestBody,
        @Part("ApproveEachInvestor") approveEachInvestor: RequestBody,
        @Part("OfferingStartDate") offeringStartDate: RequestBody,
        @Part("OfferingEndDate") offeringEndDate: RequestBody,
        @Part("OfferingStartUTCDate") offeringStartUTCDate: RequestBody,
        @Part("OfferingEndUTCDate") offeringEndUTCDate: RequestBody,
        @Part("PreviewDurationInSec") previewDurationInSec: RequestBody,
        @Part("TimeZone") timeZone: RequestBody,
        @Part("IsSaveAsDraft") isSaveAsDraft: RequestBody,
        @Part image: MultipartBody.Part
    ): Call<UploadRecordResponse>


    //--> Recent Uploaded with pagination
    @GET("api/investors-for-record")
    fun getInvestorList(
        @Query("recordid") recordId: String,
        @Query("CurrentPage") currentpage: Int
    ): Call<InvestorList>

    @GET("api/Account/getuserprofile")
    fun getProfile(@Query("ArtistId") artistid: String): Observable<ProfileResponce>

    @POST("api/Account/updateuserprofile")
    fun updateProfile(
        @Body userProfile: UserProfile
    ): Call<ProfileResponce>

    @POST("/api/Account/updateprofilepicture")
    @Multipart
    fun updateImage(@Part image: MultipartBody.Part?): Observable<UserProfileUploadResponse>

    @GET("/api/music/Records")
    fun requestSavedDrafts(
        @Query("currentPage") pageNo: Int
    ): Call<SavedDraftRecordResponse>

    @DELETE("/api/music/Records/{id}")
    fun requestDeleteSavedDrafts(@Path("id") id: String): Call<BaseResponse>

    //delete remove bank account
    @DELETE("/api/invest/DeleteUserAccounts/{id}")
    fun deleteUserAccounts(@Path("id") id: Int): Observable<BaseResponse>

    //--> Music --> Profile
    @GET("api/user-uploaded-music")
    fun getMusicListing(
        @Query("ArtistId") artistid: String,
        @Query("currentPage") currentpage: Int
    ): Call<ProfileMusicReponse>


    @GET("api/user-investments")
    fun getInvestmentListing(
        @Query("ArtistId") artistid: String,
        @Query("currentPage") currentpage: Int
    ): Call<MyInvestmentResponse>


    @POST("api/invest/MyTokensList")
    fun getMyTokenlist(
        @Body request: MyTokenListRequest,
        @Header("Content-Type") contentType: String
    ): Call<Myinvestments>

    @POST("api/music/NotifyMe")
    fun notifyRecord(
        @Body request: NotifyRequestUpdate,
        @Header("Content-Type") contentType: String
    ): Call<BaseResponse>


    @POST("api/searchnshare/NotifyMeViaEmail")
    fun notifyRecordNew(
        @Body request: NotifyRequest,
        @Header("Content-Type") contentType: String
    ): Call<BaseResponse>

    @POST("api/music/SaveFavouriteSong")
    fun recordFavorite(
        @Body request: RecordFavoriteRequest,
        @Header("Content-Type") contentType: String
    ): Call<BaseResponse>

    @POST("api/invest/GetCollectorsByRecordId")
    fun getViewCollectorsList(
        @Body request: MyViewCollectorsRequest,
        @Header("Content-Type") contentType: String
    ): Call<MyCollecrtorsResponse>

    @POST("api/music/UpdateRecordsImage")
    @Multipart
    fun requestRecordImageUpload(
        @Part("RecordsId") id: RequestBody,
        @Part image: MultipartBody.Part
    ): Call<RecordImageResponse>

    @GET("api/record-files")
    fun requestRecordFiles(
        @Query("RecordId") recordId: String
    ): Call<RecordGetFilesResponse>

    @GET("api/GetRecommendationListOfARecord")
    fun getrecommondedBy(
        @Query("RecordId") recordId: String
    ): Observable<RecomendedByRes>

    @GET("api/UserInformation/getartists")
    fun requestArtistList(
        @Query("currentPage") currentPage: Int,
        @Query("Search") query: String
    ): Call<ArtistListResponse>

    @POST("api/music/AddUpdateRecordFiles")
    @Multipart
    fun requestSavedRecordFile(
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFileId: RequestBody,
        @Part("Artists") artists: RequestBody,
        @Part("SongTitle") songTitle: RequestBody,
        @Part("RecordDurationInSeconds") recordDurationInSeconds: RequestBody,
        @Part("CopyrightSocietyWork") copyrightSocietyWork: RequestBody,
        @Part("CopyrightSocietyRecording") copyrightSocietyRecording: RequestBody,
        @Part("ISWCCode") iSWCCode: RequestBody,
        @Part("ISCRCode") iSCRCode: RequestBody,
        @Part songFile: MultipartBody.Part?

    ): Call<UploadRecordFileResponse>


    //@Lokesh make new API Step 0
    @POST("api/music/MusicUploadGetRecordAndFileId")
    fun getRecordIdAndFileId(@Header("Content-Type") contentType: String): Call<GetRecordIdResponse>


    //@Lokesh make new API Step 1
    @POST("api/music/MusicUploadSaveSongFileAndCover")
    @Multipart
    fun requestSavedRecordFileNew(
        @Part("StepNo") stepNo: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordDurationInSeconds") recordDurationInSeconds: RequestBody,
        @Part("IsSongCoverChanged") isSongCoverChanged: RequestBody,
        @Part("IsSongFileChanged") issSongFileChanged: RequestBody,
        @Part songFile: MultipartBody.Part?,
        @Part songCoverImage: MultipartBody.Part?
    ): Call<UploadRecordFileResponse>

    //@Lokesh make new API Step 2

    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun requestSavedRecordSongTitle(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("SongTitle") songTitle: RequestBody
    ): Call<UploadRecordFileResponse>

    //Step 3
    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun addArtists(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("Artists") songTitle: RequestBody
    ): Call<UploadRecordFileResponse>

    //Step 4
    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun addGenreRequest(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("GenreTypeId") songTitle: RequestBody
    ): Call<UploadRecordFileResponse>

    //Step 5
    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun addSongDurationRequest(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("IsSongPreviouslyReleased") isSongPreviouslyReleased: RequestBody,
        @Part("PreviewDurationInSec") previewDurationInSec: RequestBody,
        @Part("MessageToCollectors") messageToCollectors: RequestBody
    ): Call<UploadRecordFileResponse>

    //Step 6
    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun raisedMoneyRequest(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("InvestmentAmountNeeded") investmentAmountNeeded: RequestBody,
        @Part("CurrencyTypeId") currencyTypeId: RequestBody
    ): Call<UploadRecordFileResponse>


    //Step 7
    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun royaltyPercentageREquest(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("ArtistOwnershipPercentage") isSongPreviouslyReleased: RequestBody,
        @Part("PercentageToSell") percentageToSell: RequestBody

    ): Call<UploadRecordFileResponse>

    /*StepNo:8
    RecordId:7f29695c-78e3-4937-8eb0-6b3a7c5e6aca
    RecordFileId:653
    NoOfToken:200
    PricePerToken:0.50
    RoyalityOwnershipPercentage:40.00
    RoyalityOwnershipPercentagePerToken:0.04
    Price:100.00
*/


    //Step 8
    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun tokenSellRequest(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("NoOfToken") numberOfToken: RequestBody,

        @Part("PricePerToken") pricePerToken: RequestBody,

        @Part("RoyalityOwnershipPercentagePerToken") royalityPercentage: RequestBody,


        @Part("ArtistOwnershipPercentage") royalityOwnershipPercentage: RequestBody,

        @Part("InvestmentAmountNeeded") price: RequestBody
    ): Call<UploadRecordFileResponse>

    //Step 9
    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun requestToAddCopyRightCode(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("ISRCCode") numberOfToken: RequestBody,
        @Part("ISWCCode") pricePerToken: RequestBody,
        @Part("CopyrightSocietyWork") copyrightSocietyWork: RequestBody,
        @Part("Distributor") distributor: RequestBody,
        @Part("CountryName") countryName: RequestBody

    ): Call<UploadRecordFileResponse>

    //Step 10
    @POST("api/music/MusicUploadSaveSongDetails")
    @Multipart
    fun requestToLaunchDate(
        @Part("StepNo") stepNumer: RequestBody,
        @Part("RecordId") recordId: RequestBody,
        @Part("RecordFileId") recordFieldID: RequestBody,
        @Part("LaunchDate") numberOfToken: RequestBody,
        @Part("LaunchDateInUTC") dateInUTC: RequestBody,
        @Part("TimeZone") pricePerToken: RequestBody,
        @Part("AutoApproveInvestments") approveEachInvestor: RequestBody
    ): Call<UploadRecordFileResponse>


    @POST("api/music/MusicUploadFinalSubmit")
    @Multipart
    fun finalSubmitStep(
        @Part("RecordId") recordId: RequestBody
    ): Call<UploadRecordFileResponse>

    @GET("api/music/record-upload-details-preview")
    fun getRecordIdDetail(
        @Query("RecordId") recordId: String
    ): Call<SaveAsDraftNewDataResponse>

    @POST("api/music/MusicUploadDiscardChanges")
    @Multipart
    fun discardUploading(
        @Part("RecordId") recordId: RequestBody
    ): Call<UploadRecordFileResponse>


    @DELETE("api/music/RecordFiles/{pid}")
    fun requestDeleteRecordFile(
        @Path("pid") pid: Int
    ): Call<BaseResponse>

    @POST("api/music/SubmitRecords")
    fun requestSubmitRecord(
        @Body request: SubmitRecordRequest
    ): Call<BaseResponse>

    @GET("/api/music/GetUploadRecord3rdScreen")
    fun requestRecordDetailForFinalSubmit(
        @Query("RecordId") recordId: String
    ): Call<FinalUploadScreenResponse>

    @POST("api/invest/MyBalance")
    fun getBalance(
        @Body request: login_request
    ): Observable<MyBalanceResponse>

    @POST("api/music/S3PreSignedUrl")
    fun getSongUrl(@Body request: mp3UrlRequest): Call<mp3Response>

    @GET("api/MyStatusForRecord")
    fun requestGetInvestorStatus(
        @Query("RecordId") recordId: String,
        @Query("ApproveEachInvestor") isApproveInvestor: Boolean
    ): Observable<GetInvestorStatusResponse>

    @POST("api/invest/InvestmentRequest")
    fun requestShareRequestToInvest(
        @Body request: RequestToInvestRequest
    ): Call<RequestToInvestResponse>


    @GET("api/pending-request-for-artist")
    fun getAllInvestorList(
        @Query("RecordId") recordId: String,
        @Query("currentPage") pageNo: Int
    ): Call<InvesterResponse>

    @POST("api/invest/AcceptAllInvestmentRequest")
    fun acceptAllInvestment(
        @Body request: AcceptAllInvestor
    ): Call<BaseResponse>

    @POST("api/invest/AcceptRejectInvestmentRequest")
    fun acceptInvestment(
        @Body request: AcceptInvestor
    ): Call<AcceptInvestorResponse>

    @POST("api/music/ApproveSubmittedRecords")
    fun requestAdminApproval(
        @Body request: AdminApprovalRequest
    ): Call<BaseResponse>

    //transaction history
    @GET("api/invest/TransactionHistory")
    fun getTransactionHistory(@Query("currentPage") pageNo: Int): Call<HistoryResponse>

    // to Follow and unfollow the artist
    @POST("api/follower/followrequest")
    fun getArtistFollow(
        @Body request: ArtistFollowRequest
    ): Observable<FollowResponse>

    //--> TO Get The Details After approval of request to invest
    @GET("api/invest/MyApprovedInvestmentRequestDetail")
    fun getInvestorApprovalShares(@Query("RecordId") recordId: String): Observable<InvestmentDetailAfterApproval>

    //--> Investor initial payment begin for shares purchase
    @POST("api/invest/InvestmentPaymentBegin")
    fun getInitialPaymentInfo(@Body initialPaymentInfoRequest: InitialPaymentInfoRequest): Observable<InvestmentBegins>

    //--> Investor payment Success
    @POST("api/invest/InvestmentPaymentSuccessful")
    fun sendPaymentSuccessInfo(@Body investorPaymentRequest: InvestorPaymentRequest): Observable<InvestmentSuccessResponse>

    //--> Investor payment Failed
    @POST("api/invest/InvestmentPaymentFailed")
    fun sendPaymentFailedInfo(@Body investorPaymentRequest: InvestorPaymentRequest): Observable<BaseResponse>

    //--> Buy Now initial payment begin for shares purchase
    @POST("api/invest/PurchaseRecordBegin")
    fun getBuyNowInitialPaymentInfo(@Body initialPaymentInfoRequest: InitialPaymentInfoRequest): Observable<InvestmentBegins>

    //--> Buy Now payment Success
    @POST("api/invest/PurchaseRecordSuccessful")
    fun sendBuyNowPaymentSuccessInfo(@Body investorPaymentRequest: InvestorPaymentRequest): Observable<BaseResponse>

    //--> Buy Now payment Failed
    @POST("api/invest/PurchaseRecordFailed")
    fun sendBuyNowPaymentFailedInfo(@Body investorPaymentRequest: InvestorPaymentRequest): Observable<BaseResponse>

    @POST("api/music/GetMyPlaylist")
    fun requestGetMyPlaylist(@Body request: MyPlayListRequest): Call<MyPlaylistResponse>


    @POST("api/music/GetFavouriteSong")
    fun getMyLikedSong(@Body request: MyPlayListRequest): Call<MyPlaylistResponse>

    @GET("api/searchnshare/Notification")
    fun requestNotificationsList(@Query("currentPage") pageNo: Int): Observable<NotificationResponse>

    @GET("api/searchnshare/GetNotificationCount")
    fun getNotificationCount(): Observable<NotificationResponse>

    @GET("api/Account/GetUserCryptoWalletAddress")
    fun getCryptoWalletAddress(): Observable<SaveCryptoWalletResponse>

    @GET("api/searchnshare/GetReferralCount")
    fun getReferralCount(): Observable<NotificationResponse>

    @GET("api/searchnshare/HowItWork")
    fun getHowItWork(): Observable<HowItWorkResponse>

    @GET("api/user-dafault-sharing")
    fun getDefaultSearch(): Observable<DefaultUserResponse>

    @POST("api/searchnshare/SharingRequest")
    fun saveSharing(@Body sharingRequest: SharingRequest): Observable<BaseResponse>

    @GET("api/UserInformation/getsearchuser")
    fun searchSharing(
        @Query("CurrentPage") pageNo: Int,
        @Query("Search") search: String
    ): Observable<SearchShareResponse>

    @GET("api/getreferralslist")
    fun getReferalList(@Query("CurrentPage") pageNo: Int): Observable<ReferalListResponse>

    //--> Country List
    @GET("api/UserInformation/getcountries")
    fun getCountryList(): Observable<CounytryModal>


    @POST("api/searchnshare/DeleteReferralRecommendation")
    fun deleteReferral(@Body deleteReferralRequest: DeleteReferralRequest): Observable<BaseResponse>


    // http://18.158.70.107:5001/api/searchnshare/Search?requestType=4&search_text=T&currentPage=1

    /*Old
      @GET("api/Search")
      fun globalSearch(
          @Query("requestType") requestType: Int,
          @Query("search_text") search_text: String,
          @Query("currentPage") currentPage: Int
      ): Observable<GlobalSearchResponse>*/
//New Search Api
    @GET("api/searchnshare/Search")
    fun globalSearch(
        @Query("requestType") requestType: Int,
        @Query("search_text") search_text: String,
        @Query("currentPage") currentPage: Int
    ): Observable<GlobalSearchResponse>


    @POST("api/searchnshare/GetRecordExternalSharingPath")
    fun getExternalShareUrl(@Body request: AdminApprovalRequest): Observable<ExternalShareResponse>

    @POST("api/invest/WithdrawalFundRequest")
    fun withdrawAmount(@Body request: WithdrawRequest): Observable<BaseResponse>

    @POST("api/invest/WithdrawalFundCompleted")
    fun withdrawAmountComplete(@Body request: WithdrawSuccessRequest): Observable<BaseResponse>

    @POST("api/invest/WithdrawalRequestFailed")
    fun withdrawAmountFailed(@Body request: WithdrawFailedRequest): Observable<BaseResponse>

    @GET("api/invest/GetUserBankAccounts")
    fun getBankList(
        @Query("currentPage") currentPage: Int
    ): Observable<BankDetailListResponse>

    @POST("api/invest/AddUpdateUserBankAccounts")
    fun saveUpdateAccountNumber(@Body request: AddUpdasteBankRequest): Observable<SaveBankResponse>

    @GET(" api/invest/GetBanks")
    fun getBankList(): Observable<BankListResponse>

    @POST("api/invest/MakeBankAccountsDefault")
    fun updateBankDefault(@Body request: BankMakeDefaultRequest): Observable<BaseResponse>

    @POST("api/invest/PayThroughWallet")
    fun payThroughWallet(@Body request: PayThroughWalletRequest): Observable<BaseResponse>

    @GET("api/appversion/version")
    fun getAppVersion(): Call<AppVersionResponse>

    @GET("api/account/RequestEmailPhoneVerification")
    fun getVerificationStatus(@Query("action") currentPage: Int): Observable<PhoneEmailVerificationResponse>

    @POST("api/account/CompleteEmailPhoneVerification")
    fun verifyEmailPhone(@Body request: VerifyEmailPhoneRequest): Observable<PhoneEmailVerificationResponse>

    @GET("api/Follower/getfollowerfollowing")
    fun getFollowerFollowing(
        @Query("currentPage") currentPage: Int,
        @Query("type") type: Int,
        @Query("search") search: String,
        @Query("userId") userId: String
    ): Call<FollowerResponse>

    @POST("api/invest/HideNShowInvestors")
    fun changeInvestmentStatus(@Body changeRequestStatus: ChangeRequiestStatus): Call<BaseResponse>

    @POST("api/searchnshare/SaveDeviceInfo")
    fun saveDeviceInfo(@Body changeRequestStatus: UpdateDeviceInfoReq): Call<BaseResponse>

    @GET("api/gwgetchatdetails")
    fun getChatHistory(
        @Query("ChatId") chatId: Int,
        @Query("recordId") recordId: String,
        @Query("currentPage") pageNumber: Int,
        @Query("TotalAvRecords") totalRecord: Int
    ): Call<ChatHistoryResponse>

    @GET("api/recommandedrecordslistfromfriend")
    fun getChatRecord(
        @Query("ChatId") chatId: Int
    ): Call<ChatRecordResponse>

    @FormUrlEncoded
    @POST("api/music/SaveSongStreamingTime")
    fun saveSongStreamingTime(
        @Field("JsonRequest") strimgingRequest: String
    ): Call<BaseResponse>

    @POST("api/account/logout")
    fun logOut(
        @Body logOutRequest: LogOutRequest
    ): Observable<BaseResponse>


    @GET("api/account/getuseraccesscode")
    fun getAccessCode(): Call<UserAccessCodeResponse>

    @GET("/api/music/CopyrightSociety")
    fun requestCopyrightSociety(
        @Query("CurrencyTypeId") currencyTypeId: Int
    ): Observable<CollectingSocietyResponse>

    @POST("/api/invest/InvestmentsCertificatesList")
    fun requestCertificatesDetails(@Body recordIdRequest: RecordIdRequest): Observable<InvestorsCertificatesResponse>


    @GET("/api/invest/GetCertificate")
    fun requestCertificate(
        @Query("UserId") userId: String
    ): Observable<InvestorsCertificatesResponse>

    @POST("/api/invest/BraintreeCheckout")
    fun braintreeCheckout(@Body braintreeCheckoutRequest: BraintreeCheckoutRequest): Observable<BraintreeCheckoutResponse>



    @GET("api/invest/InvestmentDetailById")
    fun getCertificateDetail(
        @Query("InvestmentId") investmentId: Int
    ): Call<InvestorCertificateDetailResponse>


    @GET("api/invest/GetCertificatepdf")
    fun generatePdfFile(
        @Query("InvestmentId") investmentId: Int
    ): Call<InvestorCertificateDetailResponse>
}