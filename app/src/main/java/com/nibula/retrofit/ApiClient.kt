package com.nibula.retrofit

import android.content.Context
import com.nibula.utils.CommonUtils
import com.nibula.utils.PrefUtils
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {
    companion object {
        /*
         LIVE
         private const val BASE_URL_INVESTOR = "https://gateway2.nebula.ch/V2/"
         const val BASE_URL_MUSIC = "https://gateway1.nebula.ch/V2/"
         private const val BASE_URL_AUTH = "https://auth.nebula.ch"
         const val CHAT_BASE_URL = "https://schat.nebula.ch/Production/chatHub"
         DEV
         private const val BASE_URL_INVESTOR = "http://18.158.70.107:5002/"
         const val BASE_URL_MUSIC = "http://18.158.70.107:5001/"
         // const val BASE_URL_MUSIC = "http://18.158.70.107:4002/"
         // private const val BASE_URL_AUTH = "http://18.158.70.107:3000/"
         private const val BASE_URL_AUTH = "http://18.158.70.107:4000/"
         const val CHAT_BASE_URL = "http://schat.nebula.ch/chatHub"

         Staging
         private const val BASE_URL_INVESTOR = "https://stagegateway2.nebu.la/"
         const val BASE_URL_MUSIC = " https://stagegateway1.nebu.la/"
         // const val BASE_URL_MUSIC = "http://18.158.70.107:4002/"
         // private const val BASE_URL_AUTH = "http://18.158.70.107:3000/"
         private const val BASE_URL_AUTH = "https://stageauth.nebu.la/"
         const val CHAT_BASE_URL = "https://stageschat.nebu.la/chatHub"
         */
       /*
        https://stagegateway2.nebu.la/
        https://stagegateway1.nebu.la/
        https://stageauth.nebu.la/
        https://stageschat.nebu.la/chatHub
       */
        private const val BASE_URL_INVESTOR = "https://stagegateway2.nebu.la/"
        const val BASE_URL_MUSIC = "https://stagegateway1.nebu.la/"
        // const val BASE_URL_MUSIC = "http://18.158.70.107:4002/"
        // private const val BASE_URL_AUTH = "http://18.158.70.107:3000/"
        private const val BASE_URL_AUTH = "https://stageauth.nebu.la/"
        const val CHAT_BASE_URL = "https://stageschat.nebu.la/chatHub"

            /*private const val BASE_URL_INVESTOR = "http://18.158.70.107:5002/"
            const val BASE_URL_MUSIC = "http://18.158.70.107:5001/"
            // const val BASE_URL_MUSIC = "http://18.158.70.107:4002/"
            // private const val BASE_URL_AUTH = "http://18.158.70.107:3000/"
            private const val BASE_URL_AUTH = "http://18.158.70.107:4000/"
            const val CHAT_BASE_URL = "http://schat.nebula.ch/chatHub"*/

           /*private const val BASE_URL_INVESTOR = "https://devgateway2.nebu.la/"
            const val BASE_URL_MUSIC = "https://devgateway1.nebu.la/"
            private const val BASE_URL_AUTH = "https://devauth.nebu.la/"
            const val CHAT_BASE_URL = "https://schat.nebula.ch/chatHub/"*/

        // Route Changed At 2 June 2022
        // https://gateway1.nebula.ch/V2/
        // https://gateway2.nebula.ch/V2/
        //--> Domain based mapping for production

        // Production
       /* private const val BASE_URL_INVESTOR = "https://gateway2.nebula.ch/V2/"
        const val BASE_URL_MUSIC = "https://gateway1.nebula.ch/V2/"
        private const val BASE_URL_AUTH = "https://auth.nebula.ch"
        const val CHAT_BASE_URL = "https://schat.nebula.ch/Production/chatHub"*/
        //https://gateway2.nebula.ch for 5012
        //LIVE
        /*
        private const val BASE_URL_INVESTOR = "http://18.158.70.107:5012/"
        const val BASE_URL_MUSIC = "http://18.158.70.107:5011/"
        private const val BASE_URL_AUTH = "http://18.158.70.107:3010/"
        const val CHAT_BASE_URL="http://schat.nebula.ch/Production/chatHub"*/
        /*
        Server Side
        BASE_URL_INVESTOR = "http://18.158.70.107:5012/";
        BASE_URL_MUSIC = "http://18.158.70.10-7:5011/";
        BASE_URL_AUTH = "http://18.158.70.107:3010/";
        CHAT_BASE_URL="http://schat.nebula.ch/Production/chatHub";
        */
        /*
        private const val BASE_URL_IP = "http://www.ip-api.com/"
        */
        private lateinit var retrofitAuth: Retrofit
        private lateinit var retrofitMusic: Retrofit
        private lateinit var retrofitInvestor: Retrofit
        private lateinit var retrofitAuth1: Retrofit
        private lateinit var retrofitMusic1: Retrofit
        private lateinit var retrofitInvestor1: Retrofit
        private lateinit var retrofitMusic2: Retrofit

        /*private lateinit var retrofitIp: Retrofit*/
        var rA: Boolean = false
        var rM: Boolean = false
        var rI: Boolean = false
        var rA1: Boolean = false
        var rM1: Boolean = false
        var rI1: Boolean = false
        var rM2: Boolean = false
        private const val REQUEST_TIMEOUT = 30L
        private const val REQUEST_UPLOAD_TIMEOUT = 10L

        //--> http://18.158.70.107:3000
        fun getClientAuth(context: Context): Retrofit {
            if (!::retrofitAuth1.isInitialized || rA1) {
                rA = false
                try {
                    val httpClient = OkHttpClient.Builder()
                        .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                    httpClient.addInterceptor { chain ->
                        val token = PrefUtils.getValueFromPreference(context, PrefUtils.TOKEN) ?: ""
                        val tokenType =
                            PrefUtils.getValueFromPreference(context, PrefUtils.TOKEN_TYPE) ?: ""
                        val original: Request = chain.request()
                        val requestBuilder: Request = original.newBuilder()
                            .header("AUTHORIZATION", "$tokenType $token")
                            .method(original.method, original.body)
                            .build()
                        chain.proceed(requestBuilder)
                    }
                    if (CommonUtils.isDebug()) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.apply {
                            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        }
                        httpClient.addInterceptor(loggingInterceptor)
                    }
                    retrofitAuth = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .baseUrl(BASE_URL_AUTH).client(httpClient.build()).build()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            return retrofitAuth
        }

        fun getClientAuth(): Retrofit {
            if (!::retrofitAuth1.isInitialized || rA1) {
                rA1 = false
                try {
                    val httpClient = OkHttpClient.Builder()
                        .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                    if (CommonUtils.isDebug()) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.apply {
                            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        }
                        httpClient.addInterceptor(loggingInterceptor)
                    }
                    retrofitAuth1 = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .baseUrl(BASE_URL_AUTH).client(httpClient.build()).build()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            return retrofitAuth1
        }

        //--> http://18.158.70.107:5002
        fun getClientInvestor(context: Context): Retrofit {
            if (!::retrofitInvestor.isInitialized || rI) {
                rI = false
                try {
                    val httpClient = OkHttpClient.Builder()
                        .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                    httpClient.addInterceptor { chain ->
                        val token = PrefUtils.getValueFromPreference(context, PrefUtils.TOKEN) ?: ""
                        val tokenType =
                            PrefUtils.getValueFromPreference(context, PrefUtils.TOKEN_TYPE) ?: ""
                        val original: Request = chain.request()
                        val requestBuilder: Request = original.newBuilder()
                            .header("AUTHORIZATION", "$tokenType $token")
                            .method(original.method, original.body)
                            .build()
                        chain.proceed(requestBuilder)
                    }
                    if (CommonUtils.isDebug()) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.apply {
                            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        }
                        httpClient.addInterceptor(loggingInterceptor)
                    }
                    retrofitInvestor = Retrofit.Builder()
                        .baseUrl(BASE_URL_INVESTOR)
                        .client(httpClient.build())
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()

                } catch (e: java.lang.Exception) {
                }
            }
            return retrofitInvestor
        }

        fun getClientInvestor(): Retrofit {
            if (!::retrofitInvestor1.isInitialized || rI1) {
                rI1 = false
                try {
                    val httpClient = OkHttpClient.Builder()
                        .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                    if (CommonUtils.isDebug()) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.apply {
                            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        }
                        httpClient.addInterceptor(loggingInterceptor)
                    }

                    retrofitInvestor1 = Retrofit.Builder()
                        .baseUrl(BASE_URL_INVESTOR)
                        .client(httpClient.build())
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()

                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            return retrofitInvestor1
        }

        //--> http://18.158.70.107:5001/api/music/getCurrencyType
        fun getClientMusic(context: Context): Retrofit {
            if (!::retrofitMusic.isInitialized || rM) {
                rM = false
                try {
                    val httpClient = OkHttpClient.Builder()
                        .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)

                    httpClient.addInterceptor { chain ->
                        val token = PrefUtils.getValueFromPreference(context, PrefUtils.TOKEN) ?: ""
                        val tokenType =
                            PrefUtils.getValueFromPreference(context, PrefUtils.TOKEN_TYPE) ?: ""
                        val original: Request = chain.request()
                        val requestBuilder: Request = original.newBuilder()
                            .header("AUTHORIZATION", "$tokenType $token")
                            .method(original.method, original.body)
                            .build()
                        chain.proceed(requestBuilder)
                    }

                    if (CommonUtils.isDebug()) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.apply {
                            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        }
                        httpClient.addInterceptor(loggingInterceptor)
                    }

                    retrofitMusic = Retrofit.Builder()
                        .baseUrl(BASE_URL_MUSIC)
                        .client(httpClient.build())
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            return retrofitMusic
        }

        fun getClientMusic(): Retrofit {
            if (!::retrofitMusic1.isInitialized || rM1) {
                rM1 = false
                try {
                    val httpClient = OkHttpClient.Builder()
                        .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)

                    if (CommonUtils.isDebug()) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.apply {
                            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        }
                        httpClient.addInterceptor(loggingInterceptor)
                    }

                    retrofitMusic1 = Retrofit.Builder()
                        .baseUrl(BASE_URL_MUSIC)
                        .client(httpClient.build())
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            return retrofitMusic1
        }

        //For Uploading RecordFiles
        //--> http://18.158.70.107:5001

        fun getClientMusic2(context: Context): Retrofit {
            if (!::retrofitMusic2.isInitialized || rM2) {
                rM2 = false
                try {
                    val httpClient = OkHttpClient.Builder()
                        .connectTimeout(REQUEST_UPLOAD_TIMEOUT, TimeUnit.MINUTES)
                        .writeTimeout(REQUEST_UPLOAD_TIMEOUT, TimeUnit.MINUTES)
                        .readTimeout(REQUEST_UPLOAD_TIMEOUT, TimeUnit.MINUTES)

                    httpClient.addInterceptor { chain ->
                        val token = PrefUtils.getValueFromPreference(context, PrefUtils.TOKEN)
                        val tokenType =
                            PrefUtils.getValueFromPreference(context, PrefUtils.TOKEN_TYPE)
                        val original: Request = chain.request()
                        val requestBuilder: Request = original.newBuilder()
                            .header("AUTHORIZATION", "$tokenType $token")
                            .method(original.method, original.body)
                            .build()
                        chain.proceed(requestBuilder)
                    }

                    if (CommonUtils.isDebug()) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.apply {
                            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        }
                        httpClient.addInterceptor(loggingInterceptor)
                    }

                    retrofitMusic2 = Retrofit.Builder()
                        .baseUrl(BASE_URL_MUSIC)
                        .client(httpClient.build())
                        .addConverterFactory(GsonConverterFactory.create())
                        /*.addCallAdapterFactory(RxJava2CallAdapterFactory.create())*/
                        .build()
                } catch (e: java.lang.Exception) {

                }
            }
            return retrofitMusic2
        }

    }
}